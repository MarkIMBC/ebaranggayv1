var fs = require('fs');

fs.readFile('./dist/vetcloud-angularv11/index.html', 'utf-8', function (err, data) {

  if (err) throw err;

  var dateParameter = Math.floor(Math.random() * 100000);;
  var newValue = data;

  var i;
  for (i = 0; i < 100; i++) {

    newValue = newValue.replace('.css"', `.css?v=${dateParameter}"`);
    newValue = newValue.replace('.js"', `.js?v=${dateParameter}"`);
  }

  console.log(newValue);

  fs.writeFile('./dist/vetcloud-angularv11/index.html', newValue, 'utf-8', function (err, data) {
    if (err) throw err;
    console.log('Done!');
  })
})
