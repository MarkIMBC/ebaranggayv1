import { Injectable, Input, Directive } from '@angular/core';
import * as CryptoJS from 'crypto-js';

export const CrypterKey: string = 'sql123$%^abc123$';
// 8080808080808080
@Injectable({
  providedIn: 'root',
})
export class CrypterService {

  encrypt(str: string, isEncrypted?: boolean): string {
    if (isEncrypted == true) return str;
    var key = CryptoJS.enc.Utf8.parse(CrypterKey);
    var iv = CryptoJS.enc.Utf8.parse(CrypterKey);

    let encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(str), key, {
      keySize: 128 / 2,
      iv: iv,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7,
    });
    return encrypted.toString();
  }

  decrypt(str: string): string {
    if (str == null) return '';
    if (str.length == 0) return '';

    var key = CryptoJS.enc.Utf8.parse(CrypterKey);
    var iv = CryptoJS.enc.Utf8.parse(CrypterKey);

    const plainText = CryptoJS.AES.decrypt(str, key, {
      keySize: 128 / 2,
      iv: iv,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7,
    });

    return plainText.toString(CryptoJS.enc.Utf8);
  }
}
