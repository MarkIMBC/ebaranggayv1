import { Router } from '@angular/router';

import { Injectable } from "@angular/core";
import { CrypterService } from './crypter.service';

@Injectable({
  providedIn: "root"
})
export class GeneralfxService {

  static isNullOrUndefined<T>(obj: T | null | undefined): obj is null | undefined {
    return typeof obj === 'undefined' || obj === null;
  }

  static findIndexByKeyValue(_array: any[], key: string, value: any) {
    for (var i = 0; i < _array.length; i++) {
      if (_array[i][key] == value) {
        return i;
      }
    }

    return -1;
  }

  static roundOffDecimal(value: number, decimalPlaces?: number): number {

    if (GeneralfxService.isNullOrUndefined(decimalPlaces)) decimalPlaces = 2;
    if (GeneralfxService.isNullOrUndefined(value)) value = 0;

    var strresult = value.toFixed(decimalPlaces);

    return parseFloat(strresult);
  }

  static customNavigate(router: Router, cs: CrypterService, routelink: any, config: any){

    var configJSON = JSON.stringify(config);

    configJSON = cs.encrypt(configJSON);
    if (config != null && config != null) routelink.push(configJSON);

    router.navigate(routelink);
  }
}
