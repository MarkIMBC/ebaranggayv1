import { Component, Input, OnInit, TemplateRef } from '@angular/core';

@Component({
  selector: 'content-header',
  templateUrl: './content-header.component.html',
  styleUrls: ['./content-header.component.less']
})
export class ContentHeaderComponent implements OnInit {

  @Input() customTitleTemplate!: TemplateRef<any>;

  @Input()
  title: string = ''

  constructor() { }

  ngOnInit(): void {
  }

}
