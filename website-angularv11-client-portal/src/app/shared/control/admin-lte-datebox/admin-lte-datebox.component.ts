import { Component, forwardRef, Input, OnInit } from '@angular/core';
import * as moment from 'moment';
import { AdminLTEBaseControlComponent } from '../admin-lte-base-control/admin-lte-base-control.component';

@Component({
  selector: 'admin-lte-datebox',
  templateUrl: './admin-lte-datebox.component.html',
  styleUrls: [
    './../admin-lte-base-control/admin-lte-base-control.component.less',
    './admin-lte-datebox.component.less'
  ],
  providers: [{ provide: AdminLTEBaseControlComponent, useExisting: forwardRef(() => AdminLTEDateboxComponent) }]
})
export class AdminLTEDateboxComponent extends AdminLTEBaseControlComponent{

  @Input() format: string = 'MM-dd-yyyy';

  @Input() set initialValue(val: any) {

    if(val == null || val == '') val = null;

    if(val != null){

      this.value = moment(val).format('yyyy-MM-DD');
    }else{

      this.value = null;
    }

  }
}
