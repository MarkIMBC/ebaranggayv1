import { PagingOption } from 'src/shared/APP_HELPER';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.less']
})
export class PaginationComponent implements OnInit {

  private _TotalPageNumber: number = 0;
  pagesCount: number[] = []

  @Input() set TotalPageNumber(value: number) {

    this._TotalPageNumber = value;

    this.pagesCount = [];

    for (let i = 1; i <= this._TotalPageNumber ; i++) {

      this.pagesCount.push(i);
    }
  }

  @Output() pageNumber_OnClick: EventEmitter<any> = new EventEmitter();

  _pageNumber_OnClick(e: number){

      this.pageNumber_OnClick.emit({
        page: e
      });
  }

  constructor() { }

  ngOnInit(): void {


  }

}
