import { Component, OnInit, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { UserAuthenticationService, TokenSessionFields } from 'src/app/core/UserAuthentication.service';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { ToastService } from 'src/app/shared/toast.service';
import { ClientCredit, FilingStatusEnum, IControlModelArg, IFormValidation, ReceiveInventory } from 'src/shared/APP_HELPER';

@Component({
  selector: 'adjust-credit-dialog',
  templateUrl: './adjust-credit-dialog.component.html',
  styleUrls: ['./adjust-credit-dialog.component.less']
})
export class AdjustCreditDialogComponent implements OnInit {

  @ViewChild('modalDialog') modalDialog: ModalComponent | undefined;

  loading: boolean = false;
  IsDeposit: boolean = true;
  currentUser: TokenSessionFields = new TokenSessionFields();
  CurrentObject: any = {};
  Caption: string = 'Deposit';

  private _ID_Client: number = 0;

  constructor(private ds: DataService, private userAuth: UserAuthenticationService,
    public toastService: ToastService, private cs: CrypterService) { }

  async ngOnInit(): Promise<void> {

    this.currentUser = this.userAuth.getDecodedToken();
  }

  async show(ID_Client: number): Promise<any> {

    var modalDialog = this.modalDialog;

    this._ID_Client = ID_Client;
    this.loading = true;

    var promise = new Promise<any>(async (resolve, reject) => {

      if (modalDialog == undefined) {

        reject('no modal instantiate..');
      } else {

        this.loading = false;
        await modalDialog.open();
        resolve(null);
      }
    });

    return promise;
  }

  btnDepositWithdraw_onClick() {

    this.IsDeposit = this.IsDeposit != true;

    this.Caption = (this.IsDeposit ? 'Deposit' : 'Withdraw')
  }

  CreditAmount_onModelChanged(e: IControlModelArg) {

    this.CurrentObject.CreditAmount = e.value;
  }

  Comment_onModelChanged(e: IControlModelArg) {

    this.CurrentObject.Comment = e.value;
  }

  private async validateRecord() {

    var isValid = true;
    var validationsAppForm: IFormValidation[] = [];

    if (this.IsDeposit) {

      if (this.CurrentObject.CreditAmount == 0) {

        validationsAppForm.push({
          message: 'Credit Amount is required.'
        });
      }
    }


    if (this.IsDeposit != true) {

      if (this.CurrentObject.CreditAmount < 1) {

        validationsAppForm.push({
          message: 'Credit Amount is must be greater than zero.'
        });
      }
    }

    if(this.CurrentObject.Comment.length == 0){

      validationsAppForm.push({
        message: 'Note is required.'
      });
    }

    if (validationsAppForm.length > 0) {

      this.toastService.warning(validationsAppForm[0].message);
      isValid = false;
    }

    return isValid;
  }

  async btnAdjust_onClick() {

    var isvalid;

    if (!this.CurrentObject['CreditAmount']) this.CurrentObject.CreditAmount = 0;
    if (!this.CurrentObject['Comment']) this.CurrentObject.Comment = '';

    this.CurrentObject.CreditAmount = Math.abs(this.CurrentObject.CreditAmount);

    isvalid = await this.validateRecord();
    if (!isvalid) return;


    this.loading = true;
    await this.doAdjustClientCredits()

    if (this.modalDialog != undefined) {

      this.modalDialog.close();
    }
  }

  async doAdjustClientCredits(): Promise<any> {

    var clientCredits: ClientCredit[] = [];
    var creditAmount: number = this.CurrentObject.CreditAmount;

    creditAmount = this.IsDeposit ? this.CurrentObject.CreditAmount : 0 - this.CurrentObject.CreditAmount;

    clientCredits.push({
      ID_Client: this._ID_Client,
      Date: moment().format("MM-d-YY HH:mm:ss"),
      CreditAmount: creditAmount,
      Code: `${this.Caption}  Credit`,
      Comment: this.CurrentObject.Comment
    });

    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pDoAdjustClientCredits",
        {
          ClientCredits: clientCredits,
          ID_UserSession: this.currentUser.ID_Session
        },
        {
          isReturnObject: true,
        }
      );

      if (obj.Success) {
        this.toastService.success(
          `${this.Caption}  successfully.`,
          `Credit`
        );
        res(obj);
      } else {
        this.toastService.danger(
          obj.message,
          `Failed to ${this.Caption}  Credit`
        );
        rej(obj);
      }
    });
  }

  btnClose_onClick() {

    if (this.modalDialog == undefined) return;
    this.modalDialog.close();
  }

}
