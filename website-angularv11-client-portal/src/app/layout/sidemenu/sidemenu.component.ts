import { DataService } from './../../core/data.service';
import { TokenSessionFields, UserAuthenticationService } from './../../core/UserAuthentication.service';
import { SystemService } from './../../core/system.service';
import { Component, OnInit } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { Subscription } from 'rxjs';

@Component({
  selector: 'sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.less']
})
export class SidemenuComponent implements OnInit {

  MenuItems: AdminLTEMenuItem[] = [
    {
      label: "Doctor's Schedule",
      icon: 'fa fa-calendar',
      isOpen: false,
      visible: true,
      routerLink: 'ScheduleList',
    },
    {
      label: 'Admission',
      icon: 'fas fa-clinic-medical',
      isOpen: false,
      visible: true,
      items: [
        {
          label: 'Client',
          icon: 'fas fa-user-tie',
          isOpen: false,
          routerLink: 'Client',
          visible: true,
        },
        {
          label: 'Patient',
          icon: 'fas fa-paw',
          isOpen: false,
          routerLink: 'Patient',
          visible: true,
        },
        {
          label: 'SOAP',
          icon: 'fas fa-receipt',
          isOpen: false,
          routerLink: 'Patient_SOAP',
          visible: true,
        },
        {
          label: 'Confinement',
          icon: 'fas fa-bed',
          isOpen: false,
          routerLink: 'ConfinementList',
          visible: true,
        },
      ]
    },
    {
      label: 'Inventory',
      icon: 'fas fa-boxes',
      isOpen: false,
      visible: true,
      items: [
        {
          label: 'Item',
          isOpen: false,
          routerLink: 'ItemInventoriable',
          visible: true,
        },
        {
          label: 'Service',
          isOpen: false,
          routerLink: 'ItemService',
          visible: true,
        }
        // {
        //   label: 'Purchase Order',
        //   isOpen: false,
        //   visible: true,
        // },
        // {
        //   label: 'Receiving Report',
        //   isOpen: false,
        //   visible: true,
        // },
        // {
        //   label: 'Supplier',
        //   isOpen: false,
        //   routerLink: 'SupplierList',
        //   visible: true,
        // },
      ]
    },
    {
      label: 'Sales',
      icon: 'fas fa-money-check',
      isOpen: false,
      visible: true,
      items: [
        {
          label: 'Billing Invoice',
          icon: 'fas fa-file-invoice',
          isOpen: false,
          routerLink: 'BillingInvoice',
          visible: true,
        },
        // {
        //   label: 'Payment Transaction',
        //   isOpen: false,
        //   icon: 'fas fa-receipt',
        //   visible: true,
        // },
      ]
    },
    {
      label: 'Human Resource',
      icon: 'fas fa-users',
      isOpen: false,
      visible: true,
      items: [
        {
          label: 'Employee',
          isOpen: false,
          visible: true,
          routerLink: 'Employee',
        },
      ]
    },
    {
      label: 'Reports',
      icon: 'fas fa-file-alt',
      isOpen: false,
      visible: true,
      items: [
        {
          label: 'Billing Invoice Aging Report',
          isOpen: false,
          visible: true,
          routerLink: 'ReportBillingInvoiceAging',
        },
        {
          label: 'Billing Invoice Paid List',
          isOpen: false,
          visible: true,
          routerLink: 'ReportBillingInvoicePaidList',
        },
        {
          label: 'Inventory Summary',
          isOpen: false,
          visible: true,
          routerLink: 'ReportInventorySummary',
        },
        {
          label: 'Inventory Detail',
          isOpen: false,
          visible: true,
          routerLink: 'ReportInventoryDetail',
        },
        {
          label: 'Invoice Items And Services',
          isOpen: false,
          visible: true,
          routerLink: 'ReportInvoiceItemsAndServices',
        },
        {
          label: 'Payment Transaction Detail',
          isOpen: false,
          visible: true,
          routerLink: 'ReportPaymentTransactionDetail'
        },
        {
          label: 'Sales Income',
          isOpen: false,
          visible: true,
          routerLink: 'ReportSalesIncome',
        },
        // {
        //   label: 'Inventory Summary',
        //   isOpen: false,
        //   visible: true,
        // },
        // {
        //   label: 'Inventory Detail Report',
        //   isOpen: false,
        //   visible: true,
        // },
      ]
    },
  ];

  companyName: string = '';
  currentUser: TokenSessionFields = new TokenSessionFields();

  message: string = '';
  subscription: Subscription | undefined;

  constructor(
    private userAuth: UserAuthenticationService,
    private ds: DataService,
    private systemService: SystemService
  ) {

  }

  async ngOnInit(): Promise<void> {



    this.currentUser = this.userAuth.getDecodedToken();
    if (this.currentUser.ID_User == undefined || this.currentUser.ID_User == null) {

      this.MenuItems = [];
      return;
    }


    this.ds.execSP(
      "pGetCompany",
      {
        ID: this.currentUser.ID_Company,
        ID_Session: this.currentUser.ID_UserSession
      },
      { isReturnObject: true }
    ).then((obj) => {

      this.companyName = obj.Name;
    });
  }

  ngOnDestroy() {

  }

}
