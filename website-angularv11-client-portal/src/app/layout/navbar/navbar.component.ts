import { UserAuthenticationService } from './../../core/UserAuthentication.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.less']
})
export class NavbarComponent implements OnInit {

  constructor(private userAuth: UserAuthenticationService) { }

  ngOnInit(): void {
  }

  menuItem_onClick() {

    localStorage.clear();
    window.location.href = './Login';
  }

}
