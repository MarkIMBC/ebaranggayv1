import { Component } from '@angular/core';
import * as moment from 'moment';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { FilterCriteriaType, IFilterFormValue, PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { ReportComponent } from '../report/report.component';

@Component({
  selector: 'app-report-inventory-detail',
  templateUrl: './report-inventory-detail.component.html',
  styleUrls: ['./report-inventory-detail.component.less']
})
export class ReportInventoryDetailComponent extends ReportComponent {

  configOptions: any = {
    ReportName: 'INVENTORYDETAILREPORT'
  }

  protected getFilterValues(filterValues: IFilterFormValue[]): IFilterFormValue[] {

    if (this.CurrentObject['Name_Item']) {

      filterValues.push({
        dataField: "Name_Item",
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: this.CurrentObject['Name_Item']
      });
    }

    return filterValues;
  }

}
