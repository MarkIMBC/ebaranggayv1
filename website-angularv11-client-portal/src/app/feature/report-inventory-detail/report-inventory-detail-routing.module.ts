import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportInventoryDetailComponent } from './report-inventory-detail.component';

const routes: Routes = [{ path: '', component: ReportInventoryDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportInventoryDetailRoutingModule { }
