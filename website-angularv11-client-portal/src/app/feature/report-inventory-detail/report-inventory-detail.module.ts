import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportInventoryDetailRoutingModule } from './report-inventory-detail-routing.module';
import { ReportInventoryDetailComponent } from './report-inventory-detail.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ReportInventoryDetailComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportInventoryDetailRoutingModule
  ]
})
export class ReportInventoryDetailModule { }
