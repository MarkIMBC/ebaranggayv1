import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportPOSSummaryComponent } from './report-pos-summary.component';

const routes: Routes = [{ path: '', component: ReportPOSSummaryComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportPOSSummaryRoutingModule { }
