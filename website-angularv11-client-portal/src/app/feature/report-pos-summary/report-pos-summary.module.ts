import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportPOSSummaryRoutingModule } from './report-pos-summary-routing.module';
import { ReportPOSSummaryComponent } from './report-pos-summary.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ReportPOSSummaryComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportPOSSummaryRoutingModule
  ]
})
export class ReportPOSSummaryModule { }
