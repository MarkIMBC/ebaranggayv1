import { PatientSOAPTableComponent } from './patient-soap-table/patient-soap-table.component';
import { Component, ViewChild } from '@angular/core';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { FilterCriteriaType, IFormValidation, PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { BaseDetailViewComponent } from '../base-detail-view/base-detail-view.component';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { APP_REPORTVIEW } from 'src/shared/APP_MODELS';

@Component({
  selector: 'app-patient-detail',
  templateUrl: './patient-detail.component.html',
  styleUrls: [
    './../base-detail-view/base-detail-view.component.less',
    './patient-detail.component.less'
  ]
})
export class PatientDetailComponent extends BaseDetailViewComponent {

  @ViewChild('patientsoaptable') patientsoaptable: PatientSOAPTableComponent | undefined;

  ModelName: string = 'Patient'
  headerTitle: string = 'Patient'
  displayMember: string = "Name";
  BreedSpecieList: string[] = []

  async loadBreedSpecieList(): Promise<void> {

    var sql = this.cs.encrypt(
      `SELECT
              ID,
              Name
        FROM dbo.tBreedSpecie
    `);

    var _BreedSpecieList:string[] = [];

    var objs = await this.ds.query<any>(sql);

    this.BreedSpecieList = [];
    objs.forEach(function (obj) {

      _BreedSpecieList.push(obj.Name);
    });

    this.BreedSpecieList = _BreedSpecieList;
  }

  loadRightDrowDownMenu() {

    this.rightDropDownItems = [];

    if (this.CurrentObject.Name_Client != null) {

      this.rightDropDownItems.push({
        label: `Goto ${this.CurrentObject.Name_Client}`,
        visible: true,
        name: 'viewclient'
      })
    }
  }

  rightDropDown_onMainButtonClick() {

  }

  rightDropDown_onMenuItemButtonClick(event: any) {

    if (event.item.name == 'viewclient') {

      this.customNavigate(['Client', this._Code_Company, this.CurrentObject.ID_Client], {});
    }
  }

  loadInitMenuItem() {

    this.menuItems.push(this._menuItem_New);

    if(this.CurrentObject.IsActive != true) return;

    this.menuItems.push(this._menuItem_Save);
  }

  loadMenuItems() {

  }

  async menubar_OnClick(e: any) {

    if (e.item.name == APP_REPORTVIEW.ACKNOWLEDGEMENTREPORT) {

      this.customNavigate(['Report'], {
        ReportName: 'ACKNOWLEDGEMENTREPORT',
        filterValues: [
          {
            dataField: "ID",
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID
          }
        ]
      });
    }else if (e.item.name == APP_REPORTVIEW.ADMISSIONREPORT) {

      this.customNavigate(['Report'], {
        ReportName: 'ADMISSIONREPORT',
        filterValues: [
          {
            dataField: "ID",
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID
          }
        ]
      });
    }else if (e.item.name == APP_REPORTVIEW.AGREEMENTFORCONFINEMENT) {

      this.customNavigate(['Report'], {
        ReportName: 'AGREEMENTFORCONFINEMENT',
        filterValues: [
          {
            dataField: "ID",
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID
          }
        ]
      });
    }else if (e.item.name == APP_REPORTVIEW.CONCENTTOOPERATION) {

      this.customNavigate(['Report'], {
        ReportName: 'CONCENTTOOPERATION',
        filterValues: [
          {
            dataField: "ID",
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID
          }
        ]
      });
    }else if (e.item.name == APP_REPORTVIEW.EUTHANASIAAUTHORIZATION) {

      this.customNavigate(['Report'], {
        ReportName: 'EUTHANASIAAUTHORIZATION',
        filterValues: [
          {
            dataField: "ID",
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID
          }
        ]
      });
    }else if (e.item.name == APP_REPORTVIEW.VETERINARYHEALTHCLINIC) {

      this.customNavigate(['Report'], {
        ReportName: 'VETERINARYHEALTHCLINIC',
        filterValues: [
          {
            dataField: "ID",
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID
          }
        ]
      });
    }
  }

  ID_Gender_LookupboxOption: AdminLTEDataLookupboxOption = {

    listviewOption: {
      sql: 'SELECT ID, Name FROM tGender',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        }
      ]
    }
  }

  ID_Client_LookupboxOption: AdminLTEDataLookupboxOption = {

    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        }
      ]
    }
  }

  DetailView_onLoad() {

    var listViewOptionID_Client = this.ID_Client_LookupboxOption.listviewOption;

    if (listViewOptionID_Client != undefined) {
      listViewOptionID_Client.sql = `/*encryptsqlstart*/
                                      SELECT
                                            ID,
                                            Name
                                      FROM dbo.tClient
                                      WHERE
                                            ID_Company = ${this.currentUser.ID_Company}
                                      /*encryptsqlend*/`;

    }

    if (this.patientsoaptable != undefined) {

      this.patientsoaptable.load(this.CurrentObject.ID);
    }

    this.loadBreedSpecieList();
  }

  async _menuItem_New_onClick() {

    var routeLink = ['Patient', this._Code_Company, -1];
    this.customNavigate(routeLink, this.configOptions);
  }

  protected redirectAfterSaved() {

    var routeLink = ['Patient', this._Code_Company, this.CurrentObject.ID];
    this.customNavigate(routeLink, this.configOptions);
  }

  async validation(): Promise<IFormValidation[]> {

    var validations: IFormValidation[] = [];

    if (this.CurrentObject.Name == null) this.CurrentObject.Name = "";

    if (this.CurrentObject.Name.length == 0) {
      validations.push({
        message: `Name is required.`,
      });
    }

    return Promise.resolve(validations);
  }

  protected pGetRecordOptions(): any {

    var options: any = {};
    var configKeys = ['ID_Client'];

    configKeys.forEach((key) => {

      if (this.configOptions[key] != undefined && this.configOptions[key] != null) {

        options[key] = this.configOptions[key]
      }
    });

    return options;
  }

}
