import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/core/UserAuthentication.service';
import { ToastService } from 'src/app/shared/toast.service';
import { FilingStatusEnum } from 'src/shared/APP_HELPER';

@Component({
  selector: 'patient-soap-table',
  templateUrl: './patient-soap-table.component.html',
  styleUrls: ['./patient-soap-table.component.less']
})
export class PatientSOAPTableComponent implements OnInit {

  dataSource: any[] = [];
  loading: boolean = false;
  currentUser: TokenSessionFields = new TokenSessionFields();

  @Output() onCancel: EventEmitter<any> = new EventEmitter<any>();

  private ID_Patient: number = 0;

  constructor(
    private ds: DataService,
    private cs: CrypterService,
    private userAuth: UserAuthenticationService,
    private toastService: ToastService,
    private router: Router
  ) {

  }

  async ngOnInit(): Promise<void> {

    this.currentUser = this.userAuth.getDecodedToken();
  }

  load(ID_Patient: number) {

    this.ID_Patient = ID_Patient;

    var sql = `
        /*encryptsqlstart*/
        SELECT  ID,
            Date,
            Code,
            Name_SOAPType,
            History,
            Name_FilingStatus
        FROM dbo.vPatient_SOAP
        WHERE
          ID_Patient = ${this.ID_Patient} AND
          ID_FilingStatus IN (${FilingStatusEnum.Filed}, ${FilingStatusEnum.Approved})
        /*encryptsqlend*/
    `;

    sql = this.cs.encrypt(sql);
    this.ds.query<any>(sql)
      .then((obj) => {

        this.dataSource = obj;
      });
  }

  onRowClick(rowData: any) {

    var routeLink = ['Patient_SOAP', rowData.ID];

    GeneralfxService.customNavigate(this.router, this.cs, routeLink, {})
  }

}
