import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientSOAPDetailComponent } from './patient-soap-detail.component';

const routes: Routes = [{ path: '', component: PatientSOAPDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientSOAPDetailRoutingModule { }
