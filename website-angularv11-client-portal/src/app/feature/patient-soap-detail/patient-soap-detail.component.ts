import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { ItemTypeEnum, FilingStatusEnum, PaymentMethodEnum, FilterCriteriaType, IFile, IFormValidation, Patient_SOAP_Prescription_DTO, Patient_SOAP_Plan_DTO } from './../../../shared/APP_HELPER';
import { SQLListDialogComponent } from './../../shared/control/sql-list-dialog/sql-list-dialog.component';
import { CurrentObjectOnValueChangeArg } from './../base-detail-view/base-detail-view.component';
import { Component, ViewChild } from '@angular/core';
import { AdminLTEDataLookupboxComponent, AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum, TaxSchemeEnum } from 'src/shared/APP_HELPER';
import { BaseDetailViewComponent } from '../base-detail-view/base-detail-view.component';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { LaboratoryImagesComponent } from './laboratory-images/laboratory-images.component';
import { Patient_SOAP_Plan } from 'src/shared/APP_MODELS';

@Component({
  selector: 'app-patient-invoice-detail',
  templateUrl: './patient-soap-detail.component.html',
  styleUrls: [
    './../base-detail-view/base-detail-view.component.less',
    './patient-soap-detail.component.less'
  ]
})
export class PatientSOAPDetailComponent extends BaseDetailViewComponent {

  @ViewChild('ID_Patient_Lookupbox') ID_Patient_Lookupbox: AdminLTEDataLookupboxComponent | undefined
  @ViewChild('laboratoryimages') laboratoryimages: LaboratoryImagesComponent | undefined

  ModelName: string = 'Patient_SOAP';
  headerTitle: string = 'SOAP';
  CaseTypeList: string[] = [];

  FILINGSTATUS_FILED: FilingStatusEnum = FilingStatusEnum.Filed;

  async loadCaseTypeList(): Promise<void> {

    this.CaseTypeList = [];

    var sql = this.cs.encrypt(
      `SELECT
              ID,
              Name
        FROM dbo.tCaseType
    `);

    var _CaseTypeList: string[] = [];

    var objs = await this.ds.query<any>(sql);

    objs.forEach(function (obj) {

      _CaseTypeList.push(obj.Name);
    });

    this.CaseTypeList = _CaseTypeList;
  }

  loadRightDrowDownMenu() {

    this.rightDropDownItems = [];

    if (this.CurrentObject.Name_Client != null) {

      this.rightDropDownItems.push({
        label: `Goto ${this.CurrentObject.Name_Client}`,
        visible: true,
        name: 'viewclient'
      })
    }

    if (this.CurrentObject.Name_Patient != null) {

      this.rightDropDownItems.push({
        label: `Goto ${this.CurrentObject.Name_Patient}`,
        visible: true,
        name: 'viewpatient'
      })
    }

    if (this.CurrentObject.ID > 0 && !this.CurrentObject.ID_Patient_Confinement) {

      this.rightDropDownItems.push({
        label: 'Create Billing Invoice',
        visible: true,
        name: 'createbillinginvoice'
      })
    }


    if (this.CurrentObject.ID > 0 && this.CurrentObject.ID_Patient_Confinement) {

      this.rightDropDownItems.push({
        label: 'Confine Record',
        visible: true,
        name: 'confinerecord'
      })
    }

  }

  rightDropDown_onMainButtonClick() {

  }

  rightDropDown_onMenuItemButtonClick(event: any) {

    if (event.item.name == 'viewclient') {

      this.customNavigate(['Client', this.CurrentObject.ID_Client], {})
    } else if (event.item.name == 'viewpatient') {

      this.customNavigate(['Patient', this.CurrentObject.ID_Patient], {})
    } else if (event.item.name == 'createbillinginvoice') {

      this.customNavigate(['BillingInvoice', -1], {
        ID_Client: this.CurrentObject.ID_Client,
        ID_Patient: this.CurrentObject.ID_Patient,
        AttendingPhysician_ID_Employee: this.CurrentObject.AttendingPhysician_ID_Employee,
        ID_Patient_SOAP: this.CurrentObject.ID,
      });
    } else if (event.item.name == 'confinerecord') {

      this.customNavigate(['Confinement', this.CurrentObject.ID_Patient_Confinement], {});
    }
  }

  loadInitMenuItem() {

    if (this.CurrentObject.ID_Patient_Confinement) {

      this.loadInitMenuItem_Patient_Confinement();
      return;
    }

    this.menuItems.push(this._menuItem_New);

    if (this.CurrentObject.ID_FilingStatus != FilingStatusEnum.Cancelled) {

      this.menuItems.push(this._menuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {

      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  loadInitMenuItem_Patient_Confinement() {
    if (
      this.CurrentObject.Patient_Confinement_ID_FilingStatus == FilingStatusEnum.Confined
    ) {

      this.addMenuItem(this._menuItem_New);
    }

    if ( this.CurrentObject.ID_FilingStatus != FilingStatusEnum.Cancelled) {

      this.addMenuItem(this._menuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {
      this.addMenuItem(this._menuItem_Refresh);
    }
  }

  _menuItem_CreateConfinement: AdminLTEMenuItem = {
    label: 'Create Confinement',
    icon: 'fa-bed fas',
    class: 'text-primary',
    visible: true,
  }

  _menuItem_Approve: AdminLTEMenuItem = {
    label: 'Approve',
    icon: 'fa fa-file',
    class: 'text-primary',
    visible: true,
  }

  _menuItem_Done: AdminLTEMenuItem = {
    label: 'Done',
    icon: 'fa fa-file',
    class: 'text-primary',
    visible: true,
  }

  _menuItem_Cancel: AdminLTEMenuItem = {
    label: 'Cancel',
    icon: 'fa fa-times',
    class: 'text-danger',
    visible: true,
  }

  _menuItem_Report: AdminLTEMenuItem = {
    label: 'Report',
    icon: 'far fa-file',
    class: 'text-primary',
    visible: true,
    items: [
      {
        label: 'SOAP Report',
        name: 'soapreport',
        icon: 'far fa-file',
        class: 'text-primary',
        visible: true,
      },
      {
        label: 'Prescription',
        name: 'prescriptionreport',
        icon: 'far fa-file',
        class: 'text-primary',
        visible: true,
      },
    ]
  }

  loadMenuItems() {

    if (this.CurrentObject.ID < 1) return;

    if (this.CurrentObject.ID_Patient_Confinement) {

      this.loadMenuItems_Patient_Confinement();
      return;
    }

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {

      this.addMenuItem(this._menuItem_Done);
      this.addMenuItem(this._menuItem_Cancel);
      this.addMenuItem(this._menuItem_CreateConfinement);

    } else if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Approved) {

      this.addMenuItem(this._menuItem_Done);
      this.addMenuItem(this._menuItem_Cancel);
      this.addMenuItem(this._menuItem_CreateConfinement);

    }

    this.addMenuItem(this._menuItem_Report);

  }

  loadMenuItems_Patient_Confinement() {

    if (this.CurrentObject.ID < 1) return;

    if (
      this.CurrentObject.ID_FilingStatus_Patient_Confinement == FilingStatusEnum.Confined ||
      this.CurrentObject.ID_FilingStatus_Patient_Confinement == FilingStatusEnum.Discharged
    ) {

      if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
        this.addMenuItem(this._menuItem_Done);
        this.addMenuItem(this._menuItem_Cancel);
      } else if (
        this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Approved
      ) {

        this.addMenuItem(this._menuItem_Done);
        this.addMenuItem(this._menuItem_Cancel);
      }
    }

    this.addMenuItem(this._menuItem_Report);
  }

  async menubar_OnClick(e: any) {

    if (e.item.label == 'Approve') {

      this.doApprove();
    } else if (e.item.label == "Done") {

      this.doDone();
    } else if (e.item.label == "Cancel") {

      this.doCancel();
    } else if (e.item.label == "Create Confinement") {

      this.doCreateConfinement();
    } else if (e.item.name == 'soapreport') {

      this.customNavigate(['Report'], {
        ReportName: 'PatientSOAP',
        filterValues: [
          {
            dataField: "ID",
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID
          }
        ]
      });
    } else if (e.item.name == 'prescriptionreport') {

      this.customNavigate(['Report'], {
        ReportName: 'PatientSOAPPrescription',
        filterValues: [
          {
            dataField: "ID",
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID
          }
        ]
      });
    }
  }

  async doApprove() {

    if (this.isDirty) {

      this.toastService.warning(`Please save first before approving ${this.CurrentObject.Code}.`);
      return;
    }

    this.loading = true;

    this.ds.execSP(
      "pApprovePatient_SOAP",
      {
        IDs_Patient_SOAP: [this.CurrentObject.ID],
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      { isReturnObject: true }
    ).then(async (obj) => {

      if (obj.Success) {

        await this.loadRecord();
        this.toastService.success(`${this.CurrentObject.Code} has been approved successfully.`);
      } else {

        this.toastService.danger(obj.message);
      }

      this.loading = false;
    }).catch(() => {

      this.toastService.danger(`Unable to approve ${this.CurrentObject.Code}.`);
      this.loading = false;
    });
  }

  async doDone() {

    if (this.isDirty) {

      this.toastService.warning(`Please save first before done ${this.CurrentObject.Code}.`);
      return;
    }

    this.loading = true;

    this.ds.execSP(
      "pDonePatient_SOAP",
      {
        IDs_Patient_SOAP: [this.CurrentObject.ID],
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      { isReturnObject: true }
    ).then(async (obj) => {

      if (obj.Success) {

        await this.loadRecord();
        this.toastService.success(`${this.CurrentObject.Code} has been done successfully.`);
      } else {

        this.toastService.danger(obj.message);
      }

      this.loading = false;
    }).catch(() => {

      this.toastService.danger(`Unable to done ${this.CurrentObject.Code}.`);
      this.loading = false;
    });
  }

  async doCreateConfinement() {

    this.customNavigate(['Confinement', -1], {
      ID_Patient_SOAP: this.CurrentObject.ID,
    });
  }

  doCancel() {

    if (this.isDirty) {

      this.toastService.warning(`Please save first before canceling ${this.CurrentObject.Code}.`);
      return;
    }

    this.loading = true;

    this.ds.execSP(
      "pCancelPatient_SOAP",
      {
        IDs_Patient_SOAP: [this.CurrentObject.ID],
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      { isReturnObject: true }
    ).then(async (obj) => {

      if (obj.Success) {

        await this.loadRecord();
        this.toastService.success(`${this.CurrentObject.Code} has been cancelled successfully.`);
      } else {

        this.toastService.danger(obj.message);
      }

      this.loading = false;
    }).catch(() => {

      this.toastService.danger(`Unable to cancel ${this.CurrentObject.Code}.`);
      this.loading = false;
    });
  }

  ID_SOAPType_LookupboxOption: AdminLTEDataLookupboxOption = {

    listviewOption: {
      sql: 'SELECT ID, Name FROM tSOAPType',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        }
      ]
    }
  }

  AttendingPhysician_ID_Employee_LookupboxOption: AdminLTEDataLookupboxOption = {

    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'Name_Position',
          caption: 'Position',
          propertyType: PropertyTypeEnum.String,
        }
      ]
    }
  }

  ID_Client_LookupboxOption: AdminLTEDataLookupboxOption = {

    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        }
      ]
    }
  }

  ID_Patient_LookupboxOption: AdminLTEDataLookupboxOption = {

    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        }
      ]
    }
  }

  DetailView_onLoad() {

    var listViewOptionAttendingPhysician_ID_Employee = this.AttendingPhysician_ID_Employee_LookupboxOption.listviewOption;
    var listViewOptionID_Client = this.ID_Client_LookupboxOption.listviewOption;

    if (this.CurrentObject.Patient_SOAP_Prescription == null) {

      this.CurrentObject.Patient_SOAP_Prescription = [];
    }

    if (this.CurrentObject.Patient_SOAP_Plan == null) {

      this.CurrentObject.Patient_SOAP_Plan = [];
    }

    if (this.CurrentObject.Patient_SOAP_Prescription == null) {

      this.CurrentObject.Patient_SOAP_Prescription = [];
    }

    if (this.CurrentObject.LabImages == null) {

      this.CurrentObject.LabImages = [];
    }

    if (listViewOptionAttendingPhysician_ID_Employee != undefined) {
      listViewOptionAttendingPhysician_ID_Employee.sql = `/*encryptsqlstart*/
                                                          SELECT
                                                                ID,
                                                                Name,
                                                                Name_Position
                                                          FROM dbo.vAttendingVeterinarian
                                                          WHERE
                                                                ID_Company = ${this.currentUser.ID_Company}
                                                          /*encryptsqlend*/`;
    }

    if (listViewOptionID_Client != undefined) {
      listViewOptionID_Client.sql = `/*encryptsqlstart*/
                                      SELECT
                                            ID,
                                            Name
                                      FROM dbo.tClient
                                      WHERE
                                            ID_Company = ${this.currentUser.ID_Company}
                                      /*encryptsqlend*/`;

      var listViewOptionID_Patient = this.ID_Patient_LookupboxOption.listviewOption;
      if (listViewOptionID_Patient == undefined) return;

      listViewOptionID_Patient.sql = `/*encryptsqlstart*/
                              SELECT
                                    ID,
                                    Name,
                                    Name_Client
                              FROM dbo.vPatient
                              WHERE
                                    ID_Company = ${this.currentUser.ID_Company} AND
                                    ID_Client = ${this.CurrentObject.ID_Client}
                              /*encryptsqlend*/`;

    }

    this.loadCaseTypeList();
  }

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {

    if (e.name == 'ID_Client') {

      var listViewOptionID_Patient = this.ID_Patient_LookupboxOption.listviewOption;
      if (listViewOptionID_Patient == undefined) return;

      listViewOptionID_Patient.sql = `/*encryptsqlstart*/
                              SELECT
                                    ID,
                                    Name,
                                    Name_Client
                              FROM dbo.vPatient
                              WHERE
                                    ID_Company = ${this.currentUser.ID_Company} AND
                                    ID_Client = ${this.CurrentObject.ID_Client}
                              /*encryptsqlend*/`;

      if (this.ID_Patient_Lookupbox == undefined) return;
      this.ID_Patient_Lookupbox?.removeValue();
    }
  }

  btnClinicalExaminationFillTemplate_onClick() {

    this.CurrentObject.ClinicalExamination = this.CurrentObject.ClinicalExaminationTemplate;
  }

  btnDiagnosisFillTemplate_onClick() {

    this.CurrentObject.Diagnosis = this.CurrentObject.DiagnosisTemplate;
  }

  async validation(): Promise<IFormValidation[]> {

    var validations: IFormValidation[] = [];

    var ID_Client = this.CurrentObject.ID_Client;
    var ID_Patient = this.CurrentObject.ID_Patient;

    if (this.CurrentObject.ID_Client == undefined || this.CurrentObject.ID_Client == null) ID_Client = 0;
    if (this.CurrentObject.ID_Patient == undefined || this.CurrentObject.ID_Patient == null) ID_Patient = 0;

    if (ID_Client == 0) {
      validations.push({
        message: "Client is required.",
      });
    }

    if (ID_Patient == 0) {
      validations.push({
        message: "Patient is required.",
      });
    }

    this.CurrentObject.Patient_SOAP_Plan.forEach((soapPlan: Patient_SOAP_Plan_DTO) => {

      if (!soapPlan.DateReturn) {

        validations.push({
          message: `Date Return is required for ${soapPlan.Name_Item} (Plan).`,
        });
      }
    });

    this.CurrentObject.Patient_SOAP_Prescription.forEach((itemService: { Quantity: number | null; Name_Item: any; }) => {

      if (itemService.Quantity == null || itemService.Quantity == 0) {

        validations.push({
          message: `Quantity is required for ${itemService.Name_Item} (Prescription).`,
        });
      }
    });

    return Promise.resolve(validations);
  }

  protected pGetRecordOptions(): any {

    var options: any = {};
    var configKeys = ['ID_Client', 'ID_Patient', 'ID_SOAPType', 'ID_Patient_Confinement'];

    configKeys.forEach((key) => {

      if (this.configOptions[key] != undefined && this.configOptions[key] != null) {

        options[key] = this.configOptions[key]
      }
    });

    return options;
  }

  getFiles(): IFile[] {

    var currentFiles: IFile[] = []

    if (this.laboratoryimages) {

      currentFiles = this.laboratoryimages.getFiles();
    }

    this.adminLTEControls.forEach((i: any) => {

      if (i["file"]) {
        currentFiles.push({
          file: i["file"],
          dataField: i.name,
          isImage: true,
        });
      }
    });

    return currentFiles;
  }
}
