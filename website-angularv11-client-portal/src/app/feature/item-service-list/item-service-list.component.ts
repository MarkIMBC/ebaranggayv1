import { Item_DTO } from './../../../shared/APP_HELPER';
import { BaseListViewComponent } from '../base-list-view/base-list-view.component';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { Item } from 'src/shared/APP_MODELS';

@Component({
  selector: 'app-item-service-list',
  templateUrl: './item-service-list.component.html',
  styleUrls: [
    './../base-list-view/base-list-view.component.less',
    './item-service-list.component.less'
  ],
})
export class ItemServiceListComponent extends BaseListViewComponent {

  headerTitle: string = 'Service';

  CurrentObject: any = {
    Name: '',
    Name_ItemCategory: ''
  }

  dataSource: Item_DTO[] = []

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {


        return true;
      }
    },
    {
      label: 'ItemService',
      isActive: true,
      visible: true,
      command: () => {


        return true;
      }
    }
  ]

  async ListView_Onload() {

    await this.loadRecords()
  }

  async loadRecords() {

    var sql = "";
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `/*encryptsqlstart*/
            SELECT
              ID,
              Name,
              ISNULL(UnitPrice, 0) UnitPrice
            FROM vItemService
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company} AND
              IsActive = 1
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {

    var filterString = "";

    if (this.CurrentObject.Name.length > 0) {

      filterString += `Name LIKE '%${this.CurrentObject.Name}%'`;
    }

    return filterString;
  }

  Row_OnClick(rowData: Item) {

    this.customNavigate(['ItemService', rowData.ID], {});
  }

  menuItem_New_onClick() {

    this.customNavigate(['ItemService', -1], {});
  }
}
