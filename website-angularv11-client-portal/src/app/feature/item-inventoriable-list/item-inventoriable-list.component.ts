import { Item } from './../../../shared/APP_MODELS';
import { Item_DTO } from './../../../shared/APP_HELPER';
import { BaseListViewComponent } from '../base-list-view/base-list-view.component';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';

@Component({
  selector: 'app-item-inventoriable-list',
  templateUrl: './item-inventoriable-list.component.html',
  styleUrls: [
    './../base-list-view/base-list-view.component.less',
    './item-inventoriable-list.component.less'
  ],
})
export class ItemInventoriableListComponent extends BaseListViewComponent {

  headerTitle: string = 'Item';

  CurrentObject: any = {
    Name: '',
    Name_ItemCategory: ''
  }

  dataSource: Item_DTO[] = []

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {


        return true;
      }
    },
    {
      label: 'ItemInventoriable',
      visible: true,
      isActive: true,
      command: () => {


        return true;
      }
    }
  ]

  async ListView_Onload() {

    await this.loadRecords()
  }

  async loadRecords() {

    var sql = "";
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `/*encryptsqlstart*/
            SELECT
                ID,
                Name,
                RemainingBeforeExpired,
                RemainingDays,
                Name_InventoryStatus,
                ID_InventoryStatus,
                CurrentInventoryCount
            FROM vItemInventoriable_ListvIew
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {

    var filterString = "";

    if (this.CurrentObject.Name.length > 0) {

      filterString += `Name LIKE '%${this.CurrentObject.Name}%'`;
    }

    return filterString;
  }

  Row_OnClick(rowData: Item) {

    this.customNavigate(['ItemInventoriable', rowData.ID], {});
  }

  menuItem_New_onClick() {

    this.customNavigate(['ItemInventoriable', -1], {});
  }
}
