import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';

import { ItemInventoriableListRoutingModule } from './item-inventoriable-list-routing.module';
import { ItemInventoriableListComponent } from './item-inventoriable-list.component';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';

@NgModule({
  declarations: [ItemInventoriableListComponent],
  imports: [
    CommonModule,
    SharedModule,
    ItemInventoriableListRoutingModule
  ],
  providers: [
    DataService,
    CrypterService,
  ],
})
export class ItemInventoriableListModule { }
