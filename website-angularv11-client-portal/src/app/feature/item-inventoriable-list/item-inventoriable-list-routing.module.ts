import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ItemInventoriableListComponent } from './item-inventoriable-list.component';

const routes: Routes = [{ path: '', component: ItemInventoriableListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemInventoriableListRoutingModule { }
