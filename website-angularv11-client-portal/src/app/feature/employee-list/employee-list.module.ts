import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeListRoutingModule } from './employee-list-routing.module';
import { EmployeeListComponent } from './employee-list.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [EmployeeListComponent],
  imports: [
    CommonModule,
    SharedModule,
    EmployeeListRoutingModule
  ]
})
export class EmployeeListModule { }
