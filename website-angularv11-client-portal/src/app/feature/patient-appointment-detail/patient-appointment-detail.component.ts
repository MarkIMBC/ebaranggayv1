import { ItemTypeEnum } from './../../../shared/APP_HELPER';
import { Component, ViewChild } from '@angular/core';
import { AdminLTEDataLookupboxComponent, AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { BaseDetailViewComponent, CurrentObjectOnValueChangeArg } from '../base-detail-view/base-detail-view.component';

@Component({
  selector: 'app-patient-appointment-detail',
  templateUrl: './patient-appointment-detail.component.html',
  styleUrls: [
    './../base-detail-view/base-detail-view.component.less',
    './patient-appointment-detail.component.less'
  ]
})
export class PatientAppointmentDetailComponent extends BaseDetailViewComponent {

  @ViewChild('ID_Patient_Lookupbox') ID_Patient_Lookupbox: AdminLTEDataLookupboxComponent | undefined

  ModelName: string = 'PatientAppointment'
  headerTitle: string = 'Patient Appointment'

  routerFeatureName: string = 'PatientAppointment';
  displayMember: string = "Name_Client";

  ID_SOAPType_LookupboxOption: AdminLTEDataLookupboxOption = {

    listviewOption: {
      sql: `SELECT ID, Name FROM tSOAPType`,
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        }
      ]
    }
  }

  ID_Client_LookupboxOption: AdminLTEDataLookupboxOption = {

    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        }
      ]
    }
  }

  ID_Patient_LookupboxOption: AdminLTEDataLookupboxOption = {

    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        }
      ]
    }
  }

  loadRightDrowDownMenu() {

    this.rightDropDownItems = [];


    if(this.CurrentObject.ID_Client > 0){

      this.rightDropDownItems.push({
        label: `Goto ${this.CurrentObject.Name_Client} info`,
        visible: true,
        name: 'viewclient'
      })
    }

    if(this.CurrentObject.ID_Patient > 0){

      this.rightDropDownItems.push({
        label: `Goto ${this.CurrentObject.Name_Patient} info`,
        visible: true,
        name: 'viewpatient'
      })
    }

    this.rightDropDownItems.push({
      label: 'Create SOAP',
      visible: true,
      name: 'createsoap'
    })

    this.rightDropDownItems.push({
      label: 'Create Billing Invoice',
      visible: true,
      name: 'createbillinginvoice'
    })
  }


  rightDropDown_onMainButtonClick() {

  }

  rightDropDown_onMenuItemButtonClick(event: any) {

    if (event.item.name == 'viewclient') {

      this.customNavigate(['Client', this.CurrentObject.ID_Client], {})
    } else if (event.item.name == 'viewpatient') {

      this.customNavigate(['Patient', this.CurrentObject.ID_Patient], {})
    } else if (event.item.name == 'createsoap') {

      this.customNavigate(['Patient_SOAP', -1], {
        ID_Client: this.CurrentObject.ID_Client,
        ID_Patient: this.CurrentObject.ID_Patient,
        ID_SOAPType: this.CurrentObject.ID_SOAPType,
      })
    } else if (event.item.name == 'createbillinginvoice') {

      this.customNavigate(['BillingInvoice', -1], {
        ID_Client: this.CurrentObject.ID_Client,
        ID_Patient: this.CurrentObject.ID_Patient,
        ID_SOAPType: this.CurrentObject.ID_SOAPType,
      })
    }
  }

  DetailView_onLoad() {

    var listViewOptionID_Client = this.ID_Client_LookupboxOption.listviewOption;

    if (listViewOptionID_Client != undefined) {
      listViewOptionID_Client.sql = `/*encryptsqlstart*/
                                      SELECT
                                            ID,
                                            Name
                                      FROM dbo.tClient
                                      WHERE
                                            ID_Company = ${this.currentUser.ID_Company}
                                      /*encryptsqlend*/`;

      var listViewOptionID_Patient = this.ID_Patient_LookupboxOption.listviewOption;
      if (listViewOptionID_Patient == undefined) return;

      listViewOptionID_Patient.sql = `/*encryptsqlstart*/
                              SELECT
                                    ID,
                                    Name,
                                    Name_Client
                              FROM dbo.vPatient
                              WHERE
                                    ID_Company = ${this.currentUser.ID_Company} AND
                                    ID_Client = ${this.CurrentObject.ID_Client}
                              /*encryptsqlend*/`;

    }

  }

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {

    if (e.name == 'ID_Client') {

      var listViewOptionID_Patient = this.ID_Patient_LookupboxOption.listviewOption;
      if (listViewOptionID_Patient == undefined) return;

      listViewOptionID_Patient.sql = `/*encryptsqlstart*/
                              SELECT
                                    ID,
                                    Name,
                                    Name_Client
                              FROM dbo.vPatient
                              WHERE
                                    ID_Company = ${this.currentUser.ID_Company} AND
                                    ID_Client = ${this.CurrentObject.ID_Client}
                              /*encryptsqlend*/`;

      if (this.ID_Patient_Lookupbox == undefined) return;
      this.ID_Patient_Lookupbox?.removeValue();
    }
  }


}
