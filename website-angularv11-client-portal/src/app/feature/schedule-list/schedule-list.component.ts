import { GeneralfxService } from './../../core/generalfx.service';
import { IControlModelArg, Patient_SOAP_DTO } from './../../../shared/APP_HELPER';
import { APP_MODEL, BreedSpecie } from './../../../shared/APP_MODELS';
import { Component, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { BaseListViewComponent } from '../base-list-view/base-list-view.component';
import * as moment from 'moment';

@Component({
  selector: 'app-schedule-list',
  templateUrl: './schedule-list.component.html',
  styleUrls: [
    './../base-list-view/base-list-view.component.less',
    './schedule-list.component.less'
  ],
})
export class ScheduleListComponent extends BaseListViewComponent {

  headerTitle: string = 'Schedule';

  CurrentObject: any = {

    Date: null
  }

  dataSource: any[] = []

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {


        return true;
      }
    },
    {
      label: 'SOAP',
      isActive: true,
      visible: true,
      command: () => {


        return true;
      }
    }
  ]

  async ListView_Onload() {

    this.CurrentObject.Date = moment().format('yyyy-MM-DD');

    await this.loadRecords()
  }

  async loadRecords() {

    var sql = "";
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    this.OrderByString = 'DateStart DESC'

    sql = `/*encryptsqlstart*/
            SELECT
                Oid_Model,
                ID_CurrentObject,
                ReferenceCode,
                Paticular,
                Description,
                DateStart,
                DateEnd
            FROM dbo.vAppointmentEvent
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {

    var filterString = "";

    if (this.CurrentObject.Date != null) {

      filterString += `Convert(Date, DateStart) = '${this.CurrentObject.Date}'`;

    }

    if (this.CurrentObject.Code != null) {

      if(filterString.length > 0)  filterString += ' AND '
      filterString += `ReferenceCode LIKE '%${this.CurrentObject.Code}%'`;
    }

    return filterString;
  }

  Row_OnClick(rowData: any) {

    if (rowData.Oid_Model == APP_MODEL.PATIENT_SOAP.toLowerCase()) {

      this.customNavigate(['Patient_SOAP', rowData.ID_CurrentObject], {});
    }else if (rowData.Oid_Model == APP_MODEL.PATIENTAPPOINTMENT.toLowerCase()) {

      this.customNavigate(['PatientAppointment', rowData.ID_CurrentObject], {});
    }
  }

  menuItem_New_onClick() {

    this.customNavigate(['PatientAppointment', -1], {});
  }

  Date_onModelChanged(e: IControlModelArg){

    this.control_onModelChanged(e);

    this.pagingOption.PageNumber = 1;
    this.loadRecords();
  }
}
