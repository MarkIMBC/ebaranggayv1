import { IUserSession, UserAuthenticationService } from './../../core/UserAuthentication.service';
import { IControlModelArg } from './../../../shared/APP_HELPER';
import { Component, ElementRef, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/core/data.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { PagingOption } from 'src/shared/APP_HELPER';
import { TokenSessionFields } from 'src/app/core/UserAuthentication.service';
import { CrypterService } from 'src/app/core/crypter.service';

@Component({
  selector: 'app-base-list-view',
  templateUrl: './base-list-view.component.html',
})
export class BaseListViewComponent implements OnInit {

  protected ds: DataService;

  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    protected elRef: ElementRef,
    private _ds: DataService,
    private userAuth: UserAuthenticationService,
    protected cs: CrypterService
  ) {
    this.ds = _ds;

    this.gridHeight = '0px'
  }

  currentUser: TokenSessionFields = new TokenSessionFields();

  dataSource: any[] = [];

  headerTitle: string = '';

  OrderByString: string = ''

  breadCrumbItems: AdminLTEMenuItem[] = [];

  hideFilter: boolean = true;

  cardBodyHeight: string = '400px';
  gridHeight: string = '400px';

  pagingOption: PagingOption = new PagingOption();
  loading: boolean = false;
  CurrentObject: any = {};

  TotalRecordCount: number = 0;
  LoadedRecordCount: number = 0;

  items: AdminLTEMenuItem[] = [
    {
      label: 'New',
      icon: 'fas fa-file',
      visible: true,
      command: () => {


        return true;
      }
    },
    {
      label: 'Refresh',
      icon: 'fas fa-sync',
      visible: true,
      command: () => {

        this.loadRecords();
        return true;
      }
    },
  ]

  menubar_OnClick(e: any) {

    var menuItem: AdminLTEMenuItem = e.item;

    if (menuItem.label == "Refresh") {

      this.loadRecords();
    } else if (menuItem.label == "New") {

      this.menuItem_New_onClick();
    }
  }

  menuItem_New_onClick() {


  }

  findIndexByKeyValue(_array: any, key: any, value: any) {
    for (var i = 0; i < _array.length; i++) {
      if (_array[i][key] == value) {
        return i;
      }
    }

    return -1;
  }

  _Code_Company: string = '';

  async ngOnInit(): Promise<void> {

    this._Code_Company = this.route.snapshot.params["Code_Company"];
    if (this._Code_Company) await this.userAuth.InitReceptionPortal(this._Code_Company);

    this.currentUser = this.userAuth.getDecodedToken();
    if (this.currentUser.ID_User == undefined || this.currentUser.ID_User == null) {

      this.userAuth.LoginLastAccount().then((res) => {

        this.currentUser = this.userAuth.getDecodedToken();

        window.location.reload();
      }).catch((err) => {

        window.location.href = './Login'
        return;
      });
    }

    window.onresize = () => {

      setTimeout(() => {

        this.resize();

      }, 100);
    };

    this.ListView_Onload();
  }
  async ngAfterViewInit() {
    setTimeout(() => {

      this.resize();

      this.ListView_OnAfterView();
    }, 2000);
  }

  btnHideFilter_OnClick() {

    this.hideFilter = this.hideFilter == false;
  }

  resize() {

    var height = window.innerHeight;

    var cardBodyHeight = 0;
    var gridHeight = 0;

    console.log('inner height', height);

    cardBodyHeight = height - 318;
    gridHeight = height - 275;

    if (cardBodyHeight < 350) {

      this.hideFilter = true;
      cardBodyHeight = 400;
      gridHeight = 400;
    }


    this.cardBodyHeight = cardBodyHeight.toString() + 'px';
    this.gridHeight = gridHeight.toString() + 'px';
  }

  async getRecordPaging(query: string) {
    var columnName: string = '';


    this.loading = true;

    var obj = await this.ds.execSP(
      'pGetRecordPaging',
      {
        sql: query,
        PageNumber: this.pagingOption.PageNumber,
        DisplayCount: this.pagingOption.DisplayCount,
        OrderByString: columnName.length > 0 ? columnName : this.OrderByString,
      },
      {
        isReturnObject: true,
      }
    );

    this.loading = false;

    if (obj == null || obj == true || obj == undefined) {
      obj = {};
      obj.Records = [];
      obj.TotalRecord = 0;
      obj.TotalPageNumber = 0;
    }

    if (obj.Records == null || obj.Records == undefined) obj.Records = [];

    this.pagingOption.TotalRecord = obj.TotalRecord;
    this.pagingOption.TotalPageNumber = obj.TotalPageNumber;
    this.dataSource = obj.Records;

    this.TotalRecordCount = obj.TotalRecord;

    this.LoadedRecordCount =
      this.dataSource.length < this.pagingOption.DisplayCount
        ? this.dataSource.length
        : this.pagingOption.DisplayCount;
    this.LoadedRecordCount =
      this.pagingOption.DisplayCount * (this.pagingOption.PageNumber - 1) +
      this.dataSource.length;
  }

  loadRecords() { }

  btRefresh_OnClick() {
    this.loadRecords();
  }

  onPageChange(event: any) {

    this.pagingOption.PageNumber = event.page;

    this.loadRecords();
  }

  onBtnClearClick(event: any) {

    this.pagingOption.PageNumber = 1;

    this.loadRecords();
  }

  onBtnSearchClick(event: any) {

    this.pagingOption.PageNumber = 1;

    this.loadRecords();
  }

  getFilterString(): string {

    var filterString = "";

    return filterString;
  }

  ListView_Onload() { }

  ListView_OnAfterView() { }

  tableHeader_onClick(columnName: string) {

    if (this.OrderByString.length == 0 || this.OrderByString != columnName) {

      this.OrderByString = this.OrderByString.search(" DESC") < 0 ? columnName : '';
    } else {

      this.OrderByString = columnName + ' DESC';
    }

    this.loadRecords();
  }

  onColumnHeaderClick(columnName: string) {


  }

  Row_OnClick(rowData: any) {

    this.router.navigate(['Client', 'Detail']);
  }

  control_onModelChanged(e: IControlModelArg) {

    this.CurrentObject[e.name] = e.value;
  }

  control_onSelectedItemChanged(e: IControlModelArg) {

    this.pagingOption.PageNumber = 1;

    this.CurrentObject[e.name] = e.value;
    this.loadRecords();
  }

  control_onEnterKeyUp(e: IControlModelArg) {

    this.pagingOption.PageNumber = 1;

    this.CurrentObject[e.name] = e.value;
    this.loadRecords();
  }

  customNavigate(routelink: any, config?: any) {

    var configJSON = JSON.stringify(config);

    configJSON = this.cs.encrypt(configJSON);
    if (config != null && config != null) routelink.push(configJSON);

    this.router.navigate(routelink);
  }
}
