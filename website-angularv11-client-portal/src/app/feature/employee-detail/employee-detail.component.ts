import { IFormValidation, ItemTypeEnum, Patient_SOAP_Plan_DTO } from './../../../shared/APP_HELPER';
import { Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { BaseDetailViewComponent, CurrentObjectOnValueChangeArg } from '../base-detail-view/base-detail-view.component';
import { AdminLTEBaseControlComponent } from 'src/app/shared/control/admin-lte-base-control/admin-lte-base-control.component';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: [
    './../base-detail-view/base-detail-view.component.less',
    './employee-detail.component.less'
  ]
})
export class EmployeeDetailComponent extends BaseDetailViewComponent {

  ModelName: string = 'Employee'
  displayMember: string = "Name";

  ID_Position_LookupboxOption: AdminLTEDataLookupboxOption = {

    listviewOption: {
      sql: 'SELECT ID, Name FROM tPosition',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        }
      ]
    }
  }

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {

    this.CurrentObject.Name = this.CurrentObject['LastName']+ ', ' + this.CurrentObject['FirstName'] + ' ' + this.CurrentObject['MiddleName'];
  }

  async validation(): Promise<IFormValidation[]> {

    var validations: IFormValidation[] = [];

    var LastName = this.CurrentObject.LastName;
    var FirstName = this.CurrentObject.FirstName;
    var MiddleName = this.CurrentObject.MiddleName;
    var ID_Position = this.CurrentObject.ID_Position;

    if (LastName == undefined || LastName == null) LastName = '';
    if (FirstName == undefined || FirstName == null) FirstName = '';
    if (MiddleName == undefined || MiddleName == null) MiddleName = '';
    if (ID_Position == undefined || ID_Position == null) ID_Position = 0;

    if (LastName.length == 0) {
      validations.push({
        message: "Last Name is required.",
      });
    }

    if (FirstName.length == 0) {
      validations.push({
        message: "First Name is required.",
      });
    }

    if (ID_Position == 0) {
      validations.push({
        message: "Position is required.",
      });
    }

    return Promise.resolve(validations);
  }

}
