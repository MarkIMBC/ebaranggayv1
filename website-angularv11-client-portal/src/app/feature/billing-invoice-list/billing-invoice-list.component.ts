import { FilingStatusEnum } from './../../../shared/APP_HELPER';
import { Component, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { BillingInvoice_DTO } from 'src/shared/APP_HELPER';
import { BaseListViewComponent } from '../base-list-view/base-list-view.component';

@Component({
  selector: 'app-billing-invoice-list',
  templateUrl: './billing-invoice-list.component.html',
  styleUrls: [
    './../base-list-view/base-list-view.component.less',
    './billing-invoice-list.component.less'
  ],
})
export class BillingInvoiceListComponent extends BaseListViewComponent {

  headerTitle: string = 'Billing Invoice';

  CurrentObject: any = {
    Code: '',
    Name_Client: '',
    Name_Patient: '',
    AttendingPhysician_Name_Employee: '',
    Name_FilingStatus: ''
  }

  dataSource: BillingInvoice_DTO[] = []

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {


        return true;
      }
    },
    {
      label: 'Billing Invoice',
      isActive: true,
      visible: true,
      command: () => {


        return true;
      }
    }
  ]

  FilingStatus_Approved: FilingStatusEnum = FilingStatusEnum.Approved;

  async ListView_Onload() {

    await this.loadRecords()
  }

  async loadRecords() {

    var sql = "";
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `/*encryptsqlstart*/
          SELECT
            ID,
            Date,
            Code,
            Name_Client,
            Name_Patient,
            AttendingPhysician_Name_Employee,
            TotalAmount,
            ID_FilingStatus,
            Status
        FROM dbo.vBillingInvoice
        /*encryptsqlend*/
        WHERE
          ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {

    var filterString = "";

    if (this.CurrentObject.Code.length > 0) {

      filterString += `Code LIKE '%${this.CurrentObject.Code}%'`;
    }

    if (this.CurrentObject.Name_Client.length > 0) {

      if(filterString.length > 0)  filterString += ' AND '
      filterString += `Name_Client LIKE '%${this.CurrentObject.Name_Client}%'`;
    }

    if (this.CurrentObject.Name_Patient.length > 0) {

      if(filterString.length > 0)  filterString += ' AND '
      filterString += `Name_Patient LIKE '%${this.CurrentObject.Name_Patient}%'`;
    }

    if (this.CurrentObject.AttendingPhysician_Name_Employee.length > 0) {

      if(filterString.length > 0)  filterString += ' AND '
      filterString += `AttendingPhysician_Name_Employee LIKE '%${this.CurrentObject.AttendingPhysician_Name_Employee}%'`;
    }

    if (this.CurrentObject.Name_FilingStatus.length > 0) {

      if(filterString.length > 0)  filterString += ' AND '
      filterString += `(Name_FilingStatus LIKE '%${this.CurrentObject.Name_FilingStatus}%' OR Payment_Name_FilingStatus LIKE '%${this.CurrentObject.Name_FilingStatus}%')`;
    }

    return filterString;
  }

  Row_OnClick(rowData: BillingInvoice_DTO) {

    this.customNavigate(['BillingInvoice', rowData.ID], {});
  }

  menuItem_New_onClick() {

    this.customNavigate(['BillingInvoice', -1], {});
  }
}
