import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportPaymentTransactionDetailComponent } from './report-payment-transaction-detail.component';

const routes: Routes = [{ path: '', component: ReportPaymentTransactionDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportPaymentTransactionDetailRoutingModule { }
