import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportPaymentTransactionDetailRoutingModule } from './report-payment-transaction-detail-routing.module';
import { ReportPaymentTransactionDetailComponent } from './report-payment-transaction-detail.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ReportPaymentTransactionDetailComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportPaymentTransactionDetailRoutingModule
  ]
})
export class ReportPaymentTransactionDetailModule { }
