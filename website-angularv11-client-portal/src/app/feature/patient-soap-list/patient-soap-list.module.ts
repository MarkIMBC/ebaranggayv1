import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';

import { PatientSOAPListRoutingModule } from './patient-soap-list-routing.module';
import { PatientSOAPListComponent } from './patient-soap-list.component';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';

@NgModule({
  declarations: [PatientSOAPListComponent],
  imports: [
    CommonModule,
    SharedModule,
    PatientSOAPListRoutingModule
  ],
  providers: [
    DataService,
    CrypterService,
  ],
})
export class PatientSOAPListModule { }
