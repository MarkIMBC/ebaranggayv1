import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientSOAPListComponent } from './patient-soap-list.component';

const routes: Routes = [{ path: '', component: PatientSOAPListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientSOAPListRoutingModule { }
