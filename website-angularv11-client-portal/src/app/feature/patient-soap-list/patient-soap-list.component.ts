import { Patient_SOAP_DTO } from './../../../shared/APP_HELPER';
import { BreedSpecie } from './../../../shared/APP_MODELS';
import { Component, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { BaseListViewComponent } from '../base-list-view/base-list-view.component';

@Component({
  selector: 'app-patient-soap-list',
  templateUrl: './patient-soap-list.component.html',
  styleUrls: [
    './../base-list-view/base-list-view.component.less',
    './patient-soap-list.component.less'
  ],
})
export class PatientSOAPListComponent extends BaseListViewComponent {

  headerTitle: string = 'SOAP';

  CurrentObject: any = {

    Code: '',
    Name_SOAPType: '',
    Name_Client: '',
    Name_Patient: '',
    Name_FilingStatus: '',
  }

  dataSource: Patient_SOAP_DTO[] = []

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {


        return true;
      }
    },
    {
      label: 'SOAP',
      isActive: true,
      visible: true,
      command: () => {


        return true;
      }
    }
  ]

  async ListView_Onload() {

    await this.loadRecords()
  }

  async loadRecords() {

    var sql = "";
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `/*encryptsqlstart*/
            SELECT
                ID,
                Date,
                Code,
                Name_SOAPType,
                Name_Client,
                Name_Patient,
                Name_FilingStatus
            FROM dbo.vPatient_SOAP
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {

    var filterString = "";


    if (this.CurrentObject.Code.length > 0) {

      filterString += `Code LIKE '%${this.CurrentObject.Code}%'`;
    }

    if (this.CurrentObject.Name_SOAPType.length > 0) {

      if(filterString.length > 0)  filterString += ' AND '
      filterString += `Name_SOAPType LIKE '%${this.CurrentObject.Name_SOAPType}%'`;
    }

    if (this.CurrentObject.Name_Client.length > 0) {

      if(filterString.length > 0)  filterString += ' AND '
      filterString += `Name_Client LIKE '%${this.CurrentObject.Name_Client}%'`;
    }

    if (this.CurrentObject.Name_Patient.length > 0) {

      if(filterString.length > 0)  filterString += ' AND '
      filterString += `Name_Patient LIKE '%${this.CurrentObject.Name_Patient}%'`;
    }

    if (this.CurrentObject.Name_FilingStatus.length > 0) {

      if(filterString.length > 0)  filterString += ' AND '
      filterString += `Name_FilingStatus LIKE '%${this.CurrentObject.Name_FilingStatus}%'`;
    }

    return filterString;
  }

  Row_OnClick(rowData: Patient_SOAP_DTO) {

    this.customNavigate(['Patient_SOAP', rowData.ID], {});
  }

  menuItem_New_onClick() {

    this.customNavigate(['Patient_SOAP', -1], {});
  }
}
