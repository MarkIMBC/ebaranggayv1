import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { FilterCriteriaType, IFilterFormValue, PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { ReportComponent } from '../report/report.component';

@Component({
  selector: 'app-report-billing-invoice-paid-list',
  templateUrl: './report-billing-invoice-paid-list.component.html',
  styleUrls: ['./report-billing-invoice-paid-list.component.less']
})
export class ReportBillingInvoicePaidListComponent extends ReportComponent {

  configOptions: any = {
    ReportName: 'BILLINGINVOICEPAIDLISTREPORT'
  }

  AttendingPhysician_ID_Employee_LookupboxOption: AdminLTEDataLookupboxOption = {

    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'Name_Position',
          caption: 'Position',
          propertyType: PropertyTypeEnum.String,
        }
      ]
    }
  }

  onLoad() {

    var listViewOptionAttendingPhysician_ID_Employee = this.AttendingPhysician_ID_Employee_LookupboxOption.listviewOption;

    if (listViewOptionAttendingPhysician_ID_Employee != undefined) {
      listViewOptionAttendingPhysician_ID_Employee.sql = `/*encryptsqlstart*/
                                                          SELECT
                                                                ID,
                                                                Name,
                                                                Name_Position
                                                          FROM dbo.vAttendingVeterinarian
                                                          WHERE
                                                                ID_Company = ${this.currentUser.ID_Company}
                                                          /*encryptsqlend*/`;
    }
  }

  protected getFilterValues(filterValues: IFilterFormValue[]): IFilterFormValue[] {

    if (this.CurrentObject['DateStart_BillingInvoice']) {

      var value = this.CurrentObject['DateStart_BillingInvoice'];

      if (this.CurrentObject['DateEnd_BillingInvoice']) {

        var valueEnd = this.CurrentObject['DateEnd_BillingInvoice'];

        filterValues.push({
          dataField: "Date_BillingInvoice",
          filterCriteriaType: FilterCriteriaType.Between,
          propertyType: PropertyTypeEnum.Date,
          value: [
            value,
            valueEnd
          ]
        });
      } else {

        filterValues.push({
          dataField: "Date_BillingInvoice",
          filterCriteriaType: FilterCriteriaType.Equal,
          propertyType: PropertyTypeEnum.Date,
          value: [
            value,
            value
          ]
        });
      }
    } else {

      filterValues.push({
        dataField: "Date_BillingInvoice",
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Date,
        value: [
          moment().format("YYYY-MM-DD"),
          moment().format("YYYY-MM-DD")
        ]
      });
    }

    if (this.CurrentObject['DateStart_PaymentTransaction']) {

      var value = this.CurrentObject['DateStart_PaymentTransaction'];

      if (this.CurrentObject['DateEnd_PaymentTransaction']) {

        var valueEnd = this.CurrentObject['DateEnd_PaymentTransaction'];

        filterValues.push({
          dataField: "Date_PaymentTransaction",
          filterCriteriaType: FilterCriteriaType.Between,
          propertyType: PropertyTypeEnum.Date,
          value: [
            value,
            valueEnd
          ]
        });
      } else {

        filterValues.push({
          dataField: "Date_PaymentTransaction",
          filterCriteriaType: FilterCriteriaType.Equal,
          propertyType: PropertyTypeEnum.Date,
          value: [
            value,
            value
          ]
        });
      }
    }

    if (this.CurrentObject['AttendingPhysician_ID_Employee']) {

      filterValues.push({
        dataField: "AttendingPhysician_ID_Employee",
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Int,
        value: this.CurrentObject['AttendingPhysician_ID_Employee']
      });
    }

    if (this.CurrentObject['Name_Client']) {

      filterValues.push({
        dataField: "Name_Client",
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: this.CurrentObject['Name_Client']
      });
    }

    if (this.CurrentObject['Name_Patient']) {

      filterValues.push({
        dataField: "Name_Patient",
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: this.CurrentObject['Name_Patient']
      });
    }

    var index = -1;
    var caption = '';

    index = GeneralfxService.findIndexByKeyValue(filterValues, 'dataField', 'Date_BillingInvoice');
    if (index > -1) {

      var formFilter = filterValues[index];
      var dateStart = new Date(formFilter.value[0]);
      var dateEnd = new Date(formFilter.value[1]);

      caption += `Bill Date From ${moment(dateStart).format("MM/DD/YYYY")} To ${moment(dateEnd).format("MM/DD/YYYY")}<br/>`;
    }

    if (this.CurrentObject['AttendingPhysician_Name_Employee']) {

      if (this.CurrentObject.AttendingPhysician_Name_Employee.length) {

        caption += `Attending Vet: ${this.CurrentObject.AttendingPhysician_Name_Employee}<br>`;
      }
    }

    if (caption.length > 0) {

      filterValues.push({
        dataField: "Header_CustomCaption",
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.String,
        value: caption,
      });
    }

    return filterValues;
  }

}
