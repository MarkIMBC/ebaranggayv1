import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfinementItemsServicesTableComponent } from './confinement-items-services-table.component';

describe('ConfinementItemsServicesTableComponent', () => {
  let component: ConfinementItemsServicesTableComponent;
  let fixture: ComponentFixture<ConfinementItemsServicesTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfinementItemsServicesTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfinementItemsServicesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
