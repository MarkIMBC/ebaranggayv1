import { TokenSessionFields, UserAuthenticationService } from './../../core/UserAuthentication.service';
import { Component, OnInit } from '@angular/core';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { ToastService } from 'src/app/shared/toast.service';

@Component({
  selector: 'app-starter',
  templateUrl: './starter.component.html',
  styleUrls: ['./starter.component.less']
})
export class StarterComponent implements OnInit {

  cardBodyHeight: string = '100px'
  isVisible: boolean = false;

  company: any = {
    ImageHeaderLocationFilenamePath: '',
    Name: ''
  };

  currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(
    private ds: DataService,
    private userAuth: UserAuthenticationService) { }

  ngOnInit(): void {

    this.currentUser = this.userAuth.getDecodedToken();

    this.loadCompany();
  }

  async ngAfterViewInit() {
    setTimeout(() => {

      this.resize();
      this.isVisible = true;
    }, 2000);
  }

  loadCompany() {

    this.ds.execSP(
      "pGetCompany",
      {
        ID: this.currentUser.ID_Company,
        ID_Session: this.currentUser.ID_UserSession
      },
      { isReturnObject: true }
    ).then((obj) => {

      this.company = obj;
      console.log(obj);
    });
  }

  resize() {

    var height = window.innerHeight;
    var cardBodyHeight = 0;

    cardBodyHeight = height - 100;

    if(cardBodyHeight < 350){

      cardBodyHeight = 400;
    }

    this.cardBodyHeight = cardBodyHeight.toString() + 'px'
  }

}
