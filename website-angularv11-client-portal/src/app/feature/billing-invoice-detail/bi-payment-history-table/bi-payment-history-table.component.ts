import { ToastService } from './../../../shared/toast.service';
import { TokenSessionFields } from 'src/app/core/UserAuthentication.service';
import { UserAuthenticationService } from './../../../core/UserAuthentication.service';
import { DataService } from './../../../core/data.service';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FilingStatusEnum } from 'src/shared/APP_HELPER';
import { CrypterService } from 'src/app/core/crypter.service';

@Component({
  selector: 'bi-payment-history-table',
  templateUrl: './bi-payment-history-table.component.html',
  styleUrls: ['./bi-payment-history-table.component.less']
})
export class BIPaymentHistoryTableComponent implements OnInit {

  dataSource: any[] = [];
  loading: boolean = false;
  currentUser: TokenSessionFields = new TokenSessionFields();

  @Output() onCancel: EventEmitter<any> = new EventEmitter<any>();

  private ID: number = 0;

  constructor(
    private ds: DataService,
    private cs: CrypterService,
    private userAuth: UserAuthenticationService,
    private toastService: ToastService
  ) {

  }

  async ngOnInit(): Promise<void> {

    this.currentUser = this.userAuth.getDecodedToken();
  }

  load(billingInvoiceID: number) {

    this.ID = billingInvoiceID;

    var sql = `SELECT
                    ID,
                    Date,
                    Code,
                    PayableAmount,
                    PaymentAmount,
                    Name_PaymentMethod,
                    RemainingAmount,
                    ChangeAmount,
                    Name_FilingStatus
                FROM dbo.vPaymentTransaction
                WHERE
                  ID_BillingInvoice = ${billingInvoiceID} AND
                  ID_FilingStatus IN (${FilingStatusEnum.Approved})
    `;

    sql = this.cs.encrypt(sql);
    this.ds.query<any>(sql)
      .then((obj) => {

        console.log(obj);
        this.dataSource = obj;
      });

  }

  btnPrint_onClick(rowData: any) {

    this.PrintDialog(rowData.ID)
  }

  async btnCancel_onClick(ptRecord: any) {

    await this.cancelPTRecord(ptRecord);

    this.load(this.ID)
  }

  async cancelPTRecord(ptRecord: any) {

    this.loading = true;

    return new Promise<any>(async (resolve, reject) => {

      this.ds.execSP(
        "pCancelPaymentTransaction",
        {
          IDs_PaymentTransaction: [ptRecord.ID],
          ID_UserSession: this.currentUser.ID_UserSession
        },
        { isReturnObject: true }
      ).then((obj) => {


        if (obj['Success']) {

          this.toastService.success(`${ptRecord.Code} has been successfully cancelled.`);
        } else {

          this.toastService.danger(obj.message);
        }

        this.loading = false;
        resolve(obj);

        this.onCancel.emit();

      }).catch((obj) => {


        this.loading = false;

        this.toastService.danger(`Unable to cancel ${ptRecord.Code}. Please try again...`);

        reject('Saving Failed....');
      });

    });
  }

  async PrintDialog(ID_PaymentTransaction: number) {

    var obj = await this.ds.execSP(
      "pGetPaymentTransactionPrintReceiptLayout",
      {
        ID_PaymentTransaction: ID_PaymentTransaction
      },
      {
        isReturnObject: true,
      }
    );

    var w = 400;
    var h = 800;

    const dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : window.screenX;
    const dualScreenTop = window.screenTop !== undefined ? window.screenTop : window.screenY;

    const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    const systemZoom = width / window.screen.availWidth;
    const left = (width - w) / 2 / systemZoom + dualScreenLeft
    const top = (height - h) / 2 / systemZoom + dualScreenTop

    var mywindow: any = window.open('', 'PRINT', `
      scrollbars=yes,
      width=${w / systemZoom},
      height=${h / systemZoom},
      top=10,
      left=${left}
    `);

    mywindow.document.write(`

      <html>
        <head>
          <title>${obj.title}</title>
          <style>${obj.style}</style>
        </head>
        <body>
          ${obj.content}
        </body>
      </html>
    `);

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.onload = function () {

      var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

      if (obj.isAutoPrint) {

        setTimeout(async () => {

          mywindow.print();
          if (!isMobile) mywindow.close();
        }, obj.millisecondDelay);
      }
    };

    return true;
  }
}
