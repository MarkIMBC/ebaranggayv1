import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BillingInvoiceDetailComponent } from './billing-invoice-detail.component';

const routes: Routes = [{ path: '', component: BillingInvoiceDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillingInvoiceDetailRoutingModule { }
