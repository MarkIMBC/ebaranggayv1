import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ItemInventoriableDetailComponent } from './item-inventoriable-detail.component';

const routes: Routes = [{ path: '', component: ItemInventoriableDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemInventoriableDetailRoutingModule { }
