import { ClientBillingInvoiceTableComponent } from './client-billing-invoice-table/client-billing-invoice-table.component';
import { ClientPatientTableComponent } from './client-patient-table/client-patient-table.component';
import { Component, ViewChild } from '@angular/core';
import { BaseDetailViewComponent } from '../base-detail-view/base-detail-view.component';
import { AdjustCreditDialogComponent } from '../../shared/adjust-credit-dialog/adjust-credit-dialog.component';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { IFormValidation } from 'src/shared/APP_HELPER';

@Component({
  selector: 'app-client-detail',
  templateUrl: './client-detail.component.html',
  styleUrls: [
    './../base-detail-view/base-detail-view.component.less',
    './client-detail.component.less'
  ]
})
export class ClientDetailComponent extends BaseDetailViewComponent {

  @ViewChild('clientpatienttable') clientpatienttable: ClientPatientTableComponent | undefined;
  @ViewChild('clientbillinginvoicetable') clientbillinginvoicetable: ClientBillingInvoiceTableComponent | undefined;
  @ViewChild('adjustcreditdialog') adjustcreditdialog: AdjustCreditDialogComponent | undefined;


  ModelName: string = 'Client'
  headerTitle: string = 'Client'
  displayMember: string = "Name";

  _menuItem_AdjustCredit: AdminLTEMenuItem = {
    label: 'Adjust Credit',
    icon: 'fa fa-save',
    class: 'text-info',
    visible: true
  }

  loadRightDrowDownMenu() {

    this.rightDropDownItems = [];

    this.rightDropDownItems.push({
      label: 'Home',
      visible: true,
      name: 'Home'
    });
  }

  async menubar_OnClick(e: any) {

    if (e.item.label == 'Adjust Credit') {

      var ID_Client = this.CurrentObject.ID;

      if (this.adjustcreditdialog) {

        await this.adjustcreditdialog.show(ID_Client);
        this.loadRecord()
      }
    }
  }

  rightDropDown_onMainButtonClick() {

  }

  rightDropDown_onMenuItemButtonClick(event: any) {

    if (event.item.name == 'Home') {

      this.router.navigate(['ReceptionPortal', this._Code_Company]);
    }
  }

  loadInitMenuItem() {

    this.menuItems.push(this._menuItem_Save);

    if (this.CurrentObject.ID > 0) {

      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  loadMenuItems() {

  }

  DetailView_onLoad() {

    if (this.clientpatienttable != undefined) {

      this.clientpatienttable.load(this.CurrentObject.ID);
    }

    if (this.clientbillinginvoicetable != undefined) {

      this.clientbillinginvoicetable.load(this.CurrentObject.ID);
    }
  }

  async validation(): Promise<IFormValidation[]> {

    var validations: IFormValidation[] = [];

    if (this.CurrentObject.Name == null) this.CurrentObject.Name = "";

    if (this.CurrentObject.Name.length == 0) {
      validations.push({
        message: `Name is required.`,
      });
    }

    return Promise.resolve(validations);
  }

  protected redirectAfterSaved() {

    var routeLink = ['Client', this._Code_Company, this.CurrentObject.ID];
    this.customNavigate(routeLink, {});
  }

}
