import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportSalesIncomeRoutingModule } from './report-sales-income-routing.module';
import { ReportSalesIncomeComponent } from './report-sales-income.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ReportSalesIncomeComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportSalesIncomeRoutingModule
  ]
})
export class ReportSalesIncomeModule { }
