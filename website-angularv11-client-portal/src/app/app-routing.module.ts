import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './defaultpage/page-not-found/page-not-found.component';

const routes: Routes = [

  { path: '', component: LoginComponent},

  { path: 'Login', component: LoginComponent},

  { path: 'Starter', loadChildren: () => import('./feature/starter/starter.module').then(m => m.StarterModule) },

  { path: 'Client', loadChildren: () => import('./feature/client-list/client-list.module').then(m => m.ClientModule) },

  { path: 'Client/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/client-detail/client-detail.module').then(m => m.ClientDetailModule) },

  { path: 'Client/:Code_Company/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/client-detail/client-detail.module').then(m => m.ClientDetailModule) },

  { path: 'Patient', loadChildren: () => import('./feature/patient-list/patient-list.module').then(m => m.PatientListModule) },

  { path: 'Patient/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/patient-detail/patient-detail.module').then(m => m.PatientDetailModule) },

  { path: 'Patient/:Code_Company/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/patient-detail/patient-detail.module').then(m => m.PatientDetailModule) },

  { path: 'ItemService', loadChildren: () => import('./feature/item-service-list/item-service-list.module').then(m => m.ItemServiceListModule) },

  { path: 'ItemService/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/item-service-detail/item-service-detail.module').then(m => m.ItemServiceDetailModule) },

  { path: 'ItemInventoriable', loadChildren: () => import('./feature/item-inventoriable-list/item-inventoriable-list.module').then(m => m.ItemInventoriableListModule) },

  { path: 'ItemInventoriable/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/item-inventoriable-detail/item-inventoriable-detail.module').then(m => m.ItemInventoriableDetailModule) },

  { path: 'SupplierList', loadChildren: () => import('./feature/supplier-list/supplier-list.module').then(m => m.SupplierListModule) },

  { path: 'Patient_SOAP', loadChildren: () => import('./feature/patient-soap-list/patient-soap-list.module').then(m => m.PatientSOAPListModule) },

  { path: 'Patient_SOAP/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/patient-soap-detail/patient-soap-detail.module').then(m => m.PatientSoapDetailModule) },

  { path: 'PatientAppointment/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/patient-appointment-detail/patient-appointment-detail.module').then(m => m.PatientAppointmentDetailModule) },

  { path: 'BillingInvoice', loadChildren: () => import('./feature/billing-invoice-list/billing-invoice-list.module').then(m => m.BillingInvoiceListModule) },

  { path: 'BillingInvoice/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/billing-invoice-detail/billing-invoice-detail.module').then(m => m.BillingInvoiceDetailModule) },

  { path: 'PatientSoapDetail', loadChildren: () => import('./feature/patient-soap-detail/patient-soap-detail.module').then(m => m.PatientSoapDetailModule) },

  { path: 'ClientDetail', loadChildren: () => import('./feature/client-detail/client-detail.module').then(m => m.ClientDetailModule) },

  { path: 'PatientDetail', loadChildren: () => import('./feature/patient-detail/patient-detail.module').then(m => m.PatientDetailModule) },

  { path: 'ItemInventoriableDetail', loadChildren: () => import('./feature/item-inventoriable-detail/item-inventoriable-detail.module').then(m => m.ItemInventoriableDetailModule) },

  { path: 'ItemServiceDetail', loadChildren: () => import('./feature/item-service-detail/item-service-detail.module').then(m => m.ItemServiceDetailModule) },

  { path: 'ScheduleList', loadChildren: () => import('./feature/schedule-list/schedule-list.module').then(m => m.ScheduleListModule) },

  { path: 'Employee/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/employee-detail/employee-detail.module').then(m => m.EmployeeDetailModule) },

  { path: 'Employee', loadChildren: () => import('./feature/employee-list/employee-list.module').then(m => m.EmployeeListModule) },

  { path: 'PageNotFound', component: PageNotFoundComponent },

  { path: 'Report/:configOptions', loadChildren: () => import('./feature/report/report.module').then(m => m.ReportModule) },

  { path: 'PatientAppointmentDetail', loadChildren: () => import('./feature/patient-appointment-detail/patient-appointment-detail.module').then(m => m.PatientAppointmentDetailModule) },

  { path: 'CompanyDetail', loadChildren: () => import('./feature/company-detail/company-detail.module').then(m => m.CompanyDetailModule) },

  { path: 'ReportSalesIncome', loadChildren: () => import('./feature/report-sales-income/report-sales-income.module').then(m => m.ReportSalesIncomeModule) },

  { path: 'ReportBillingInvoicePaidList', loadChildren: () => import('./feature/report-billing-invoice-paid-list/report-billing-invoice-paid-list.module').then(m => m.ReportBillingInvoicePaidListModule) },

  { path: 'ReportBillingInvoiceAging', loadChildren: () => import('./feature/report-billing-invoice-aging/report-billing-invoice-aging.module').then(m => m.ReportBillingInvoiceAgingModule) },

  { path: 'ReportInvoiceItemsAndServices', loadChildren: () => import('./feature/report-pos-summary/report-pos-summary.module').then(m => m.ReportPOSSummaryModule) },

  { path: 'ReportPaymentTransactionDetail', loadChildren: () => import('./feature/report-payment-transaction-detail/report-payment-transaction-detail.module').then(m => m.ReportPaymentTransactionDetailModule) },

  { path: 'ReportInventorySummary', loadChildren: () => import('./feature/report-inventory-summary/report-inventory-summary.module').then(m => m.ReportInventorySummaryModule) },

  { path: 'ReportInventoryDetail', loadChildren: () => import('./feature/report-inventory-detail/report-inventory-detail.module').then(m => m.ReportInventoryDetailModule) },

  { path: 'EmployeeDetail', loadChildren: () => import('./feature/employee-detail/employee-detail.module').then(m => m.EmployeeDetailModule) },

  { path: 'EmployeeList', loadChildren: () => import('./feature/employee-list/employee-list.module').then(m => m.EmployeeListModule) },

  { path: 'ConfinementList', loadChildren: () => import('./feature/patient-confinement-list/patient-confinement-list.module').then(m => m.PatientConfinementListModule) },

  { path: 'Confinement/:ID_CurrentObject/:configOptions', loadChildren: () => import('./feature/patient-confinement-detail/patient-confinement-detail.module').then(m => m.PatientConfinementDetailModule) },

  { path: 'ReceptionPortal/:Code_Company', loadChildren: () => import('./feature/reception-portal/reception-portal.module').then(m => m.ReceptionPortalModule) },

  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [
      RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
