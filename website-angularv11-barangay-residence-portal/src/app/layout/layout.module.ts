import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ControlSidebarComponent } from './control-sidebar/control-sidebar.component';
import { MainFooterComponent } from './main-footer/main-footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidemenuComponent } from './sidemenu/sidemenu.component';


@NgModule({
  declarations: [
    NavbarComponent,
    SidemenuComponent,
    ControlSidebarComponent,
    MainFooterComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    SidemenuComponent,
    NavbarComponent,
    ControlSidebarComponent,
    MainFooterComponent
  ]
})
export class LayoutModule { }
