import { Component, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';
import { AdminLTEMenuItem } from '../AdminLTEMenuItem';

@Component({
  selector: 'menubar',
  templateUrl: './menubar.component.html',
  styleUrls: ['./menubar.component.less']
})
export class MenubarComponent implements OnInit {

  @Input() contentTemplate!: TemplateRef<any>;
  @Input() items: AdminLTEMenuItem[] = [];

  @Output() onClick: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {

    this.items.forEach((item) => {

      if(item.class == undefined || item.class == null) item.class = '';
      if(item.visible == undefined || item.visible == null) item.visible = true;
    });
  }

  menuItemOnClick(item: AdminLTEMenuItem){

    this.onClick.emit({
      item: item
    });
  }

  ngAfterViewInit() : void{


  }

}
