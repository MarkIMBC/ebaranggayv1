import { AdminLTEMenuItem } from './../AdminLTEMenuItem';
import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'drop-down-button',
  templateUrl: './drop-down-button.component.html',
  styleUrls: ['./drop-down-button.component.less']
})
export class DropDownButtonComponent implements OnInit {

  @Input() title: string = '';
  @Input() showMainButton: boolean = true;
  @Input() isDropdownRight: boolean = false;

  @Input() items: AdminLTEMenuItem[] = []


  @Output() onMainButtonClick: EventEmitter<any> = new EventEmitter<any>();
  @Output() onMenuItemButtonClick: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  btnMain_onClick() {

    this.onMainButtonClick.emit();
  }

  menuItemm_onClick(menuItem: any) {

    this.onMenuItemButtonClick.emit({
      item: menuItem
    });
  }

}

