import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'card-refresh',
  templateUrl: './card-refresh.component.html',
  styleUrls: ['./card-refresh.component.less']
})
export class CardRefreshComponent implements OnInit {

  @Input()
  visible: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

}
