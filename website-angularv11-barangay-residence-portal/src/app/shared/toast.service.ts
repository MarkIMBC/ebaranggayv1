import { Injectable, TemplateRef } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ToastService {
  toasts: any[] = [];

  success(textOrTpl: string | TemplateRef<any>, options: any = {}){

    this.show(textOrTpl, { classname: 'bg-success text-light', delay: 5000 });
  }

  danger(textOrTpl: string | TemplateRef<any>, options: any = {}){

    this.show(textOrTpl, { classname: 'bg-danger text-light', delay: 5000 });
  }

  warning(textOrTpl: string | TemplateRef<any>, options: any = {}){

    this.show(textOrTpl, { classname: 'bg-warning text-light', delay: 5000 });
  }

  info(textOrTpl: string | TemplateRef<any>, options: any = {}){

    this.show(textOrTpl, { classname: 'bg-info text-light', delay: 5000 });
  }

  show(textOrTpl: string | TemplateRef<any>, options: any = {}) {
    this.toasts.push({ textOrTpl, ...options });
  }

  remove(toast: any) {
    this.toasts = this.toasts.filter(t => t !== toast);
  }
}
