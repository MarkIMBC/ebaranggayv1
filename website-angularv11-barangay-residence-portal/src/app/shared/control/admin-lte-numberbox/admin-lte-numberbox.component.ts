import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { AdminLTEBaseControlComponent } from '../admin-lte-base-control/admin-lte-base-control.component';
import * as $ from 'jquery';

@Component({
  selector: 'admin-lte-numberbox',
  templateUrl: './admin-lte-numberbox.component.html',
  styleUrls: [
    './../admin-lte-base-control/admin-lte-base-control.component.less',
    './admin-lte-numberbox.component.less'
  ],
  providers: [{ provide: AdminLTEBaseControlComponent, useExisting: forwardRef(() => AdminLTENumberboxComponent) }]
})
export class AdminLTENumberboxComponent extends AdminLTEBaseControlComponent {

  isShowDisplayView: boolean = true;

  @Input() format: string = '1.2-5';
  @Input() min: number = 0;

  @Input() set initialValue(val: any) {

    if (val == null || val == '') val = 0;
    this.value = val;
  }

  focusOut() {

    this.isShowDisplayView = true;
  }

  focusIn() {

    this.isShowDisplayView = false;
  }

  onClick() {

    $('#' + this.id + '-input').focus();
    $('#' + this.id + '-input').select();
  }
}
