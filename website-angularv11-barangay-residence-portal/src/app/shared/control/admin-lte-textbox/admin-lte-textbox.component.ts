import { AdminLTEBaseControlComponent } from './../admin-lte-base-control/admin-lte-base-control.component';
import { IControlModelArg } from './../../../../shared/APP_HELPER';
import { Component, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'admin-lte-texbox',
  templateUrl: './admin-lte-textbox.component.html',
  styleUrls: ['./admin-lte-textbox.component.less'],
  providers: [{ provide: AdminLTEBaseControlComponent, useExisting: forwardRef(() => AdminLTETextboxComponent) }]
})
export class AdminLTETextboxComponent extends AdminLTEBaseControlComponent {



}
