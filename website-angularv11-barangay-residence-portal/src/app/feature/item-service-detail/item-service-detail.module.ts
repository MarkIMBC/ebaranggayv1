import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemServiceDetailRoutingModule } from './item-service-detail-routing.module';
import { ItemServiceDetailComponent } from './item-service-detail.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ItemServiceDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    ItemServiceDetailRoutingModule
  ]
})
export class ItemServiceDetailModule { }
