import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ItemServiceDetailComponent } from './item-service-detail.component';

const routes: Routes = [{ path: '', component: ItemServiceDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemServiceDetailRoutingModule { }
