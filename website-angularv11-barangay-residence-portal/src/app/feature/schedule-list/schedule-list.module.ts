import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScheduleListRoutingModule } from './schedule-list-routing.module';
import { ScheduleListComponent } from './schedule-list.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ScheduleListComponent],
  imports: [
    CommonModule,
    SharedModule,
    ScheduleListRoutingModule
  ]
})
export class ScheduleListModule { }
