import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReceptionPortalComponent } from './reception-portal.component';

const routes: Routes = [{ path: '', component: ReceptionPortalComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceptionPortalRoutingModule { }
