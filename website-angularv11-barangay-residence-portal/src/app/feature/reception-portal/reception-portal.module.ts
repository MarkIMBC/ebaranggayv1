import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { ReceptionPortalRoutingModule } from './reception-portal-routing.module';
import { ReceptionPortalComponent } from './reception-portal.component';

@NgModule({
  declarations: [ReceptionPortalComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReceptionPortalRoutingModule
  ],
  providers: [
    DataService,
    CrypterService,
  ],
})
export class ReceptionPortalModule { }
