import { Client_DTO, Patient_SOAP_DTO } from './../../../shared/APP_HELPER';
import { BreedSpecie } from './../../../shared/APP_MODELS';
import { Component, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { BaseListViewComponent } from '../base-list-view/base-list-view.component';

@Component({
  selector: 'app-reception-portal',
  templateUrl: './reception-portal.component.html',
  styleUrls: [
    './../base-list-view/base-list-view.component.less',
    './reception-portal.component.less'
  ],
})
export class ReceptionPortalComponent extends BaseListViewComponent {

  headerTitle: string = 'Client';

  items: AdminLTEMenuItem[] = [
    {
      label: 'Refresh',
      icon: 'fas fa-sync',
      visible: true,
      command: () => {

        this.loadRecords();
        return true;
      }
    },
  ]

  CurrentObject: any = {
    Name: '',
    ContactNumbers: '',
    SearchTag: ''
  }

  dataSource: Client_DTO[] = []

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {


        return true;
      }
    },
    {
      label: 'Client',
      isActive: true,
      visible: true,
      command: () => {


        return true;
      }
    }
  ]

  async ListView_Onload() {

    await this.loadRecords()
  }

  async loadRecords() {

    var sql = "";
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `/*encryptsqlstart*/
            SELECT  ID,
                    Code,
                    Name,
                    ContactNumbers,
                    DateCreated
            FROM dbo.vClient_ListView
            /*encryptsqlend*/
            WHERE
            ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {

    var filterString = "";

    if (this.CurrentObject.SearchTag == null) this.CurrentObject.SearchTag = ""
    if (this.CurrentObject.Code == null) this.CurrentObject.Code = ""
    if (this.CurrentObject.Name == null) this.CurrentObject.Name = ""
    if (this.CurrentObject.ContactNumber == null) this.CurrentObject.ContactNumber = ""

    if (this.CurrentObject.SearchTag.length > 0) {

      filterString += `(Code LIKE '%${this.CurrentObject.SearchTag}%' OR Name LIKE '%${this.CurrentObject.SearchTag}%')`;
    }

    if (this.CurrentObject.Code.length > 0) {
      if (filterString.length > 0) filterString += ' AND '
      filterString += `Code LIKE '%${this.CurrentObject.Code}%'`;
    }

    if (this.CurrentObject.Name.length > 0) {

      if (filterString.length > 0) filterString += ' AND '
      filterString += `Name LIKE '%${this.CurrentObject.Name}%'`;
    }

    if (this.CurrentObject.ContactNumber.length > 0) {

      if (filterString.length > 0) filterString += ' AND '
      filterString += `ContactNumbers LIKE '%${this.CurrentObject.ContactNumber}%'`;
    }


    return filterString;
  }

  Row_OnClick(rowData: Client_DTO) {

    this.customNavigate(['Client', this._Code_Company, rowData.ID], {});
  }

  menuItem_New_onClick() {

    this.customNavigate(['Client', this._Code_Company, -1], {});
  }

  btnCreateClientInfo_onClick() {

    this.customNavigate(['Client', this._Code_Company, -1], {});
  }
}
