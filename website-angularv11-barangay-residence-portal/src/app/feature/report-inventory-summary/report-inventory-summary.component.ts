import { Component } from '@angular/core';
import * as moment from 'moment';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { FilterCriteriaType, IFilterFormValue, PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { ReportComponent } from '../report/report.component';

@Component({
  selector: 'app-report-inventory-summary',
  templateUrl: './report-inventory-summary.component.html',
  styleUrls: ['./report-inventory-summary.component.less']
})
export class ReportInventorySummaryComponent extends ReportComponent {

  configOptions: any = {
    ReportName: 'INVENTORYSUMMARYREPORT'
  }

  onLoad() {

  }

  protected getFilterValues(filterValues: IFilterFormValue[]): IFilterFormValue[] {

    if (this.CurrentObject['Name_Item']) {

      filterValues.push({
        dataField: "Name_Item",
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: this.CurrentObject['Name_Item']
      });
    }

    return filterValues;
  }

}
