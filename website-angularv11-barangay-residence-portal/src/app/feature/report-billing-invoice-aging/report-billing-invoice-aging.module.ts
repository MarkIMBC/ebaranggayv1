import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportBillingInvoiceAgingRoutingModule } from './report-billing-invoice-aging-routing.module';
import { ReportBillingInvoiceAgingComponent } from './report-billing-invoice-aging.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ReportBillingInvoiceAgingComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportBillingInvoiceAgingRoutingModule
  ]
})
export class ReportBillingInvoiceAgingModule { }
