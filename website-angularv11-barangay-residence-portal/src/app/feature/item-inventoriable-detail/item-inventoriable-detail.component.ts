import { AdjustInventoryDialogComponent } from './adjust-inventory-dialog/adjust-inventory-dialog.component';
import { ItemTypeEnum, Item_DTO } from './../../../shared/APP_HELPER';
import { Component, ViewChild } from '@angular/core';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { BaseDetailViewComponent } from '../base-detail-view/base-detail-view.component';

@Component({
  selector: 'app-item-inventoriable-detail',
  templateUrl: './item-inventoriable-detail.component.html',
  styleUrls: [
    './../base-detail-view/base-detail-view.component.less',
    './item-inventoriable-detail.component.less'
  ]
})
export class ItemInventoriableDetailComponent extends BaseDetailViewComponent {

  @ViewChild('adjustinventorydialog') adjustinventorydialog: AdjustInventoryDialogComponent | undefined;

  ModelName: string = 'Item'
  headerTitle: string = 'Item'

  routerFeatureName: string = 'ItemInventoriable';
  displayMember: string = "Name";

  ID_ItemCategory_LookupboxOption: AdminLTEDataLookupboxOption = {

    listviewOption: {
      sql: `SELECT ID, Name FROM tItemCategory WHERE ID_ItemType = ${ItemTypeEnum.Inventoriable}`,
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        }
      ]
    }
  }

  loadInitMenuItem() {

    this.menuItems.push(this._menuItem_New);

    if(this.CurrentObject.IsActive != true) return;

    this.menuItems.push(this._menuItem_Save);

    if (this.CurrentObject.ID > 0) {

      this.menuItems.push(this._menuItem_Refresh);
      this.menuItems.push(this._menuItem_Delete);
    }
  }

  loadMenuItems() {

    if(this.CurrentObject.IsActive != true) return;

    var menuAdjustInventory = {
      label: 'Adjust Inventory',
      icon: 'fas fa-cubes',
      class: 'text-primary',
      visible: true,
    }

    if (this.CurrentObject.ID > 0) {

      this.addMenuItem(menuAdjustInventory);
    }
  }

  async menubar_OnClick(e: any) {

    if (e.item.label == 'Adjust Inventory') {

      if(this.adjustinventorydialog != undefined){

        await this.adjustinventorydialog.show(this.CurrentObject.ID);
        this.loadRecord();
      }
    }
  }

  DetailView_onLoad() {

    this.CurrentObject.ID_ItemType = ItemTypeEnum.Inventoriable;
  }

  public CurrentObject_onBeforeSaving() {

    var item: any = this.CurrentObject;
    var fieldNames: string[] = ['UnitCost','UnitPrice','MinInventoryCount','MaxInventoryCount'];

    fieldNames.forEach(fieldName => {

      if(!item[fieldName]) item[fieldName] = 0;
      item[fieldName] = Math.abs(item[fieldName]);
    });
  }
}
