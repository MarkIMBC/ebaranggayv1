import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BillingInvoiceListComponent } from './billing-invoice-list.component';

const routes: Routes = [{ path: '', component: BillingInvoiceListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillingInvoiceListRoutingModule { }
