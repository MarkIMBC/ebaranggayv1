import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StarterRoutingModule } from './starter-routing.module';
import { StarterComponent } from './starter.component';


@NgModule({
  declarations: [StarterComponent],
  imports: [
    CommonModule,
    SharedModule,
    StarterRoutingModule
  ]
})
export class StarterModule { }
