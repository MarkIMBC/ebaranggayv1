import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { FilterCriteriaType, IFilterFormValue, PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { ReportComponent } from '../report/report.component';

@Component({
  selector: 'app-report-pos-summary',
  templateUrl: './report-pos-summary.component.html',
  styleUrls: ['./report-pos-summary.component.less']
})
export class ReportPOSSummaryComponent extends ReportComponent {

  configOptions: any = {
    ReportName: 'POSSUMMARY'
  }

  onLoad() {

  }

  protected getFilterValues(filterValues: IFilterFormValue[]): IFilterFormValue[] {

    if (this.CurrentObject['DateStart_BillingInvoice']) {

      var value = this.CurrentObject['DateStart_BillingInvoice'];

      if (this.CurrentObject['DateEnd_BillingInvoice']) {

        var valueEnd = this.CurrentObject['DateEnd_BillingInvoice'];

        filterValues.push({
          dataField: "Date_QPARAM",
          filterCriteriaType: FilterCriteriaType.Between,
          propertyType: PropertyTypeEnum.Date,
          value: [
            value,
            valueEnd
          ]
        });
      } else {

        filterValues.push({
          dataField: "Date_QPARAM",
          filterCriteriaType: FilterCriteriaType.Equal,
          propertyType: PropertyTypeEnum.Date,
          value: [
            value,
            value
          ]
        });
      }
    } else {

      filterValues.push({
        dataField: "Date_QPARAM",
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Date,
        value: [
          moment().format("YYYY-MM-DD"),
          moment().format("YYYY-MM-DD")
        ]
      });
    }

    var index = -1;
    var caption = '';

    index = GeneralfxService.findIndexByKeyValue(filterValues, 'dataField', 'Date_BillingInvoice');
    if (index > -1) {

      var formFilter = filterValues[index];
      var dateStart = new Date(formFilter.value[0]);
      var dateEnd = new Date(formFilter.value[1]);

      caption += `Bill Date From ${moment(dateStart).format("MM/DD/YYYY")} To ${moment(dateEnd).format("MM/DD/YYYY")}<br/>`;
    }

    if (caption.length > 0) {

      filterValues.push({
        dataField: "Header_CustomCaption",
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.String,
        value: caption,
      });
    }

    return filterValues;
  }

}
