import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientConfinementListComponent } from './patient-confinement-list.component';

const routes: Routes = [{ path: '', component: PatientConfinementListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientConfinementListRoutingModule { }
