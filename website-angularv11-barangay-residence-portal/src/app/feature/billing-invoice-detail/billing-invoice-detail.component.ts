import { BIPaymentHistoryTableComponent } from './bi-payment-history-table/bi-payment-history-table.component';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { BillingPaymentDialogComponent } from './billing-payment-dialog/billing-payment-dialog.component';
import { ItemTypeEnum, FilingStatusEnum, PaymentMethodEnum, FilterCriteriaType, IFormValidation, AlignEnum } from './../../../shared/APP_HELPER';
import { SQLListDialogComponent } from './../../shared/control/sql-list-dialog/sql-list-dialog.component';
import { CurrentObjectOnValueChangeArg } from './../base-detail-view/base-detail-view.component';
import { Component, ViewChild } from '@angular/core';
import { AdminLTEDataLookupboxComponent, AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum, TaxSchemeEnum, BillingInvoice_Detail_DTO } from 'src/shared/APP_HELPER';
import { BaseDetailViewComponent } from '../base-detail-view/base-detail-view.component';
import { GeneralfxService } from 'src/app/core/generalfx.service';

@Component({
  selector: 'app-billing-invoice-detail',
  templateUrl: './billing-invoice-detail.component.html',
  styleUrls: [
    './../base-detail-view/base-detail-view.component.less',
    './billing-invoice-detail.component.less'
  ]
})
export class BillingInvoiceDetailComponent extends BaseDetailViewComponent {

  @ViewChild('bipaymenthistorytable') bipaymenthistorytable: BIPaymentHistoryTableComponent | undefined;
  @ViewChild('biPaymentDialog') biPaymentDialog: BillingPaymentDialogComponent | undefined;

  @ViewChild('ID_Patient_Lookupbox') ID_Patient_Lookupbox: AdminLTEDataLookupboxComponent | undefined

  ModelName: string = 'BillingInvoice'
  headerTitle: string = 'Billing Invoice'

  FILINGSTATUS_FILED: FilingStatusEnum = FilingStatusEnum.Filed;

  loadRightDrowDownMenu() {

    this.rightDropDownItems = [];

    if (this.CurrentObject.Name_Client != null) {

      this.rightDropDownItems.push({
        label: `Goto ${this.CurrentObject.Name_Client} Info.`,
        visible: true,
        name: 'viewclient'
      });
    }

    if (this.CurrentObject.Name_Patient != null) {

      this.rightDropDownItems.push({
        label: `Goto ${this.CurrentObject.Name_Patient} Info.`,
        visible: true,
        name: 'viewpatient'
      })
    }
  }

  rightDropDown_onMainButtonClick() {

  }

  rightDropDown_onMenuItemButtonClick(event: any) {

    if (event.item.name == 'viewclient') {

      this.customNavigate(['Client', this.CurrentObject.ID_Client], {})
    } else if (event.item.name == 'viewpatient') {

      this.customNavigate(['Patient', this.CurrentObject.ID_Patient], {})
    }
  }

  loadInitMenuItem() {

    this.menuItems.push(this._menuItem_New);

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {

      this.menuItems.push(this._menuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {

      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  loadMenuItems() {

    var menuItemApprove = {
      label: 'Approve',
      icon: 'fa fa-file',
      class: 'text-primary',
      visible: true,
    }

    var menuItemCancel = {
      label: 'Cancel',
      icon: 'fa fa-times',
      class: 'text-danger',
      visible: true,
    }

    var menuItemPayment: AdminLTEMenuItem = {
      label: 'Payment',
      icon: 'far fa-money-bill-alt',
      class: 'text-primary',
      visible: true,
      items: [
        {
          label: 'Cash',
          name: 'Cash',
          icon: 'far fa-money-bill-alt',
          class: 'text-primary',
          visible: true,
        },
        {
          label: 'G Cash',
          name: 'GCash',
          icon: 'far fa-money-bill-alt',
          class: 'text-primary',
          visible: true,
        },
        {
          label: 'Debit / Credit',
          name: 'DebitCredit',
          icon: 'far fa-credit-card',
          class: 'text-primary',
          visible: true,
        },
        {
          label: 'Check',
          name: 'Check',
          icon: 'fas fa-money-check',
          class: 'text-primary',
          visible: true,
        },
      ]
    }

    var menuReport: AdminLTEMenuItem = {
      label: 'Report',
      icon: 'far fa-file',
      class: 'text-primary',
      visible: true,
      items: [
        {
          label: 'Billing Invoice Report',
          name: 'billinginvoicereport',
          icon: 'far fa-file',
          class: 'text-primary',
          visible: true,
        }
      ]
    }

    if (this.CurrentObject.ID > 0) {

      if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {

        this.addMenuItem(menuItemApprove);
        this.addMenuItem(menuItemCancel);
      } else if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Approved) {

        if (this.CurrentObject.Payment_ID_FilingStatus == FilingStatusEnum.Pending) {

          this.addMenuItem(menuItemCancel);
        }

        if (
          this.CurrentObject.Payment_ID_FilingStatus == FilingStatusEnum.Pending ||
          this.CurrentObject.Payment_ID_FilingStatus == FilingStatusEnum.PartiallyPaid
        ) {

          this.addMenuItem(menuItemPayment);
        }
      }

      this.addMenuItem(menuReport);
    }
  }

  async menubar_OnClick(e: any) {

    if (e.item.label == 'Approve') {

      this.doApprove();
    } else if (e.item.label == 'Cancel') {

      this.doCancel();
    } else if (e.item.name == 'Cash') {

      if (this.biPaymentDialog == undefined) return;

      await this.biPaymentDialog.show(PaymentMethodEnum.Cash);
      this.loadRecord();

    } else if (e.item.name == 'GCash') {

      if (this.biPaymentDialog == undefined) return;

      await this.biPaymentDialog.show(PaymentMethodEnum.GCash);
      this.loadRecord();

    } else if (e.item.name == 'DebitCredit') {

      if (this.biPaymentDialog == undefined) return;
      await this.biPaymentDialog.show(PaymentMethodEnum.DebitCredit);
      this.loadRecord();

    } else if (e.item.name == 'Check') {

      if (this.biPaymentDialog == undefined) return;
      await this.biPaymentDialog.show(PaymentMethodEnum.Check);
      this.loadRecord();
    } else if (e.item.name == 'billinginvoicereport') {

      this.customNavigate(['Report'], {
        ReportName: 'PatientBillingInvoiceReport',
        filterValues: [
          {
            dataField: "ID",
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID
          }
        ]
      });
    }
  }

  async doApprove() {

    if (this.isDirty) {

      this.toastService.warning(`Please save first before approving ${this.CurrentObject.Code}.`);
      return;
    }

    this.loading = true;

    this.ds.execSP(
      "pApproveBillingInvoice",
      {
        IDs_BillingInvoice: [this.CurrentObject.ID],
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      { isReturnObject: true }
    ).then(async (obj) => {

      if (obj.Success) {

        await this.loadRecord();
        this.toastService.success(`${this.CurrentObject.Code} has been approved successfully.`);
      } else {

        this.toastService.danger(obj.message);
      }

      this.loading = false;
    }).catch(() => {

      this.toastService.danger(`Unable to approve ${this.CurrentObject.Code}.`);
      this.loading = false;
    });
  }

  doCancel() {

    if (this.isDirty) {

      this.toastService.warning(`Please save first before canceling ${this.CurrentObject.Code}.`);
      return;
    }

    this.loading = true;

    this.ds.execSP(
      "pCancelBillingInvoice",
      {
        IDs_BillingInvoice: [this.CurrentObject.ID],
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      { isReturnObject: true }
    ).then(async (obj) => {

      if (obj.Success) {

        await this.loadRecord();
        this.toastService.success(`${this.CurrentObject.Code} has been cancelled successfully.`);
      } else {

        this.toastService.danger(obj.message);
      }

      this.loading = false;
    }).catch(() => {

      this.toastService.danger(`Unable to cancel ${this.CurrentObject.Code}.`);
      this.loading = false;
    });
  }

  AttendingPhysician_ID_Employee_LookupboxOption: AdminLTEDataLookupboxOption = {

    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'Name_Position',
          caption: 'Position',
          propertyType: PropertyTypeEnum.String,
        }
      ]
    }
  }

  ID_Client_LookupboxOption: AdminLTEDataLookupboxOption = {

    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        }
      ]
    }
  }

  ID_Patient_LookupboxOption: AdminLTEDataLookupboxOption = {

    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        }
      ]
    }
  }

  compute() {
    var subTotal = 0;
    var totalAmount = 0;
    var grossAmount = 0;
    var vatAmount = 0;
    var discountRate = this.CurrentObject.DiscountRate;
    var discountAmount = this.CurrentObject.DiscountAmount;
    var netAmount = 0;

    if (discountRate == null || discountRate == undefined) discountRate = 0;
    if (discountAmount == null || discountAmount == undefined) discountAmount = 0;

    discountRate = GeneralfxService.roundOffDecimal(discountRate);
    discountAmount = GeneralfxService.roundOffDecimal(discountAmount);

    this.CurrentObject.BillingInvoice_Detail.forEach((detail: any) => {

      if (detail.Quantity == null || detail.Quantity == undefined) detail.Quantity = 0;
      if (detail.UnitPrice == null || detail.UnitPrice == undefined) detail.UnitPrice = 0;

      detail.Amount = detail.Quantity * detail.UnitPrice;
      detail.ID_BillingInvoice = this.CurrentObject.ID;

      subTotal += detail.Amount;
    });

    totalAmount = GeneralfxService.roundOffDecimal(subTotal);

    if (this.CurrentObject.IsComputeDiscountRate) {
      discountAmount = totalAmount * (discountRate / 100);
      discountAmount = GeneralfxService.roundOffDecimal(discountAmount);
    } else {
      discountRate = (discountAmount / totalAmount) * 100;
      discountRate = GeneralfxService.roundOffDecimal(discountRate);
    }

    totalAmount = totalAmount - discountAmount;
    totalAmount = GeneralfxService.roundOffDecimal(totalAmount);

    grossAmount = totalAmount;

    vatAmount = (grossAmount / 1.12) * 0.12;
    vatAmount = GeneralfxService.roundOffDecimal(vatAmount);

    if (
      this.CurrentObject.ID_TaxScheme == TaxSchemeEnum.ZeroRated ||
      this.CurrentObject.ID_TaxScheme == 0 ||
      this.CurrentObject.ID_TaxScheme == undefined
    ) {
      vatAmount = 0;
      netAmount = grossAmount;
    } else if (this.CurrentObject.ID_TaxScheme == TaxSchemeEnum.TaxExclusive) {
      netAmount = grossAmount + vatAmount;
    } else if (this.CurrentObject.ID_TaxScheme == TaxSchemeEnum.TaxInclusive) {
      netAmount = grossAmount - vatAmount;
    }

    if (discountRate == null || discountRate == undefined) discountRate = 0;
    if (discountAmount == null || discountAmount == undefined) discountAmount = 0;

    this.CurrentObject.SubTotal = subTotal;
    this.CurrentObject.TotalAmount = totalAmount;
    this.CurrentObject.DiscountRate = discountRate;
    this.CurrentObject.DiscountAmount = discountAmount;

    this.CurrentObject.GrossAmount = grossAmount;
    this.CurrentObject.VatAmount = vatAmount;
    this.CurrentObject.NetAmount = netAmount;
  }

  DetailView_onLoad() {

    var listViewOptionAttendingPhysician_ID_Employee = this.AttendingPhysician_ID_Employee_LookupboxOption.listviewOption;
    var listViewOptionID_Client = this.ID_Client_LookupboxOption.listviewOption;

    if (this.CurrentObject.BillingInvoice_Detail == null) {

      this.CurrentObject.BillingInvoice_Detail = [];
    }

    if (listViewOptionAttendingPhysician_ID_Employee != undefined) {
      listViewOptionAttendingPhysician_ID_Employee.sql = `/*encryptsqlstart*/
                                                          SELECT
                                                                ID,
                                                                Name,
                                                                Name_Position
                                                          FROM dbo.vAttendingVeterinarian
                                                          WHERE
                                                                ID_Company = ${this.currentUser.ID_Company}
                                                          /*encryptsqlend*/`;
    }

    if (listViewOptionID_Client != undefined) {
      listViewOptionID_Client.sql = `/*encryptsqlstart*/
                                      SELECT
                                            ID,
                                            Name
                                      FROM dbo.tClient
                                      WHERE
                                            ID_Company = ${this.currentUser.ID_Company}
                                      /*encryptsqlend*/`;

      var listViewOptionID_Patient = this.ID_Patient_LookupboxOption.listviewOption;
      if (listViewOptionID_Patient == undefined) return;

      listViewOptionID_Patient.sql = `/*encryptsqlstart*/
                              SELECT
                                    ID,
                                    Name,
                                    Name_Client
                              FROM dbo.vPatient
                              WHERE
                                    ID_Company = ${this.currentUser.ID_Company} AND
                                    ID_Client = ${this.CurrentObject.ID_Client}
                              /*encryptsqlend*/`;

    }

    if (this.bipaymenthistorytable != undefined) {

      this.bipaymenthistorytable.load(this.CurrentObject.ID)
    }
  }

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {

    if (e.name == 'ID_Client') {

      var listViewOptionID_Patient = this.ID_Patient_LookupboxOption.listviewOption;
      if (listViewOptionID_Patient == undefined) return;

      listViewOptionID_Patient.sql = `/*encryptsqlstart*/
                              SELECT
                                    ID,
                                    Name,
                                    Name_Client
                              FROM dbo.vPatient
                              WHERE
                                    ID_Company = ${this.currentUser.ID_Company} AND
                                    ID_Client = ${this.CurrentObject.ID_Client}
                              /*encryptsqlend*/`;

      if (this.ID_Patient_Lookupbox == undefined) return;
      this.ID_Patient_Lookupbox?.removeValue();
    }
  }

  biDetailQuantity_onModelChange(biDetail: BillingInvoice_Detail_DTO) {

    this.compute();
  }

  biDetailUnitPrice_onModelChange(biDetail: BillingInvoice_Detail_DTO) {

    this.compute();
  }

  btnDeleteBIDetail_onClick(biDetail: BillingInvoice_Detail_DTO) {

    this.deleteDetail('BillingInvoice_Detail', biDetail)
  }

  biDetailMenuItems: AdminLTEMenuItem[] = [
    {
      label: 'Add Item',
      icon: 'fa fa-cart-plus',
      visible: true,
    },
    {
      label: 'Add Service',
      icon: 'fa fa-stethoscope',
      visible: true,
    }
  ];

  async biDetailMenuItems_OnClick(arg: any) {

    var menuItem: AdminLTEMenuItem = arg.item;


    if (menuItem.label == 'Add Item') {

      this.doAddItem()
    } else if (menuItem.label == 'Add Service') {

      this.doAddService();
    }

  }

  async doAddItem() {

    var obj: any;

    obj = await this.sqllistdialog?.open({
      sql: `/*encryptsqlstart*/
            SELECT
                  ID,
                  Name,
                  UnitPrice,
                  CurrentInventoryCount,
                  UnitCost,
                  OtherInfo_DateExpiration
            FROM dbo.vItemInventoriableForBillingLookUp
            /*encryptsqlend*/
            WHERE
                  ID_Company = ${this.currentUser.ID_Company}`,
      columns: [
        {
          name: 'Name',
          caption: 'Item',
          propertyType: PropertyTypeEnum.String
        },
        {
          name: 'UnitPrice',
          caption: 'Price',
          propertyType: PropertyTypeEnum.Decimal,
          format: '1.2',
          align: AlignEnum.right
        },
        {
          name: 'CurrentInventoryCount',
          caption: 'Inventory Count',
          propertyType: PropertyTypeEnum.Int,
          format: '1.0',
          align: AlignEnum.right
        },
      ]
    });

    obj.rows.forEach((record: any) => {
      var item = {
        ID: this.getTempID(),
        ID_Item: record.ID,
        Name_Item: record.Name,
        Quantity: 1,
        UnitCost: record.UnitCost,
        UnitPrice: record.UnitPrice,
        DateExpiration: record.OtherInfo_DateExpiration
      };

      if (record.ID_ItemType == ItemTypeEnum.Service) item.Quantity = 1;

      if (this.CurrentObject.BillingInvoice_Detail == null) this.CurrentObject.BillingInvoice_Detail = [];
      this.CurrentObject.BillingInvoice_Detail.push(item);
    });

    this.compute();
  }

  async doAddService() {

    var obj: any;
    obj = await this.sqllistdialog?.open({
      sql: `/*encryptsqlstart*/
            SELECT
              ID,
              Name,
              UnitPrice,
              UnitCost
            FROM dbo.vItemServiceLookUp
            /*encryptsqlend*/
            WHERE
                  ID_Company = ${this.currentUser.ID_Company}`,
      columns: [
        {
          name: 'Name',
          caption: 'Item',
          propertyType: PropertyTypeEnum.String
        },
        {
          name: 'UnitPrice',
          caption: 'Price',
          propertyType: PropertyTypeEnum.Decimal,
          format: '1.2',
          align: AlignEnum.right
        },
      ]
    });

    obj.rows.forEach((record: any) => {
      var item = {
        ID: this.getTempID(),
        ID_Item: record.ID,
        Name_Item: record.Name,
        Quantity: 0,
        UnitCost: record.UnitCost,
        UnitPrice: record.UnitPrice
      };

      if (record.ID_ItemType == ItemTypeEnum.Service) item.Quantity = 1;

      if (this.CurrentObject.BillingInvoice_Detail == null) this.CurrentObject.BillingInvoice_Detail = [];
      this.CurrentObject.BillingInvoice_Detail.push(item);
    });

    this.compute();
  }

  DiscountRate_onModelChange() {

    this.CurrentObject.IsComputeDiscountRate = true;
    this.compute();
  }

  DiscountAmount_onModelChange() {

    this.CurrentObject.IsComputeDiscountRate = false;
    this.compute();
  }

  bipaymenthistorytable_onCancel() {

    this.loadRecord();
  }

  protected pGetRecordOptions(): any {

    var options: any = {};
    var configKeys = ['ID_Client', 'ID_Patient', 'AttendingPhysician_ID_Employee', 'ID_Patient_SOAP', 'ID_Patient_Confinement'];

    configKeys.forEach((key) => {

      if (this.configOptions[key] != undefined && this.configOptions[key] != null) {

        options[key] = this.configOptions[key]
      }
    });

    return options;
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    this.CurrentObject.BillingInvoice_Detail.forEach((detail: { Quantity: number; Name_Item: any; }) => {
      if (detail.Quantity == 0) {
        validations.push({
          message: `Quantity is required for item ${detail.Name_Item}.`,
        });
      }
    });

    if (validations.length == 0) {

      validations = await this.validateBackend();
    }

    return Promise.resolve(validations);
  }

  public CurrentObject_onBeforeSaving() {

    this.compute();
  }

  async validateBackend(): Promise<IFormValidation[]> {

    var validations: IFormValidation[] = [];

    var validateObj = await this.ds.execSP(
      "pBillingInvoice_Validation",
      {
        ID_BillingInvoice: this.CurrentObject.ID,
        ID_Patient_Confinement: this.CurrentObject.ID_Patient_Confinement,
        ID_UserSession: this.currentUser.ID_UserSession
      },
      {
        isReturnObject: true,
      }
    );

    if (validateObj.isValid != true) {

      validations.push({ message: validateObj.message });
    }

    return validations;
  }

}
