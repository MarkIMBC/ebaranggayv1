import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from './../../../shared/AdminLTEMenuItem';
import { IControlModelArg, ItemTypeEnum, Patient_SOAP_Plan_DTO, Patient_SOAP_Prescription_DTO, PropertyTypeEnum } from './../../../../shared/APP_HELPER';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { GeneralfxService } from 'src/app/core/generalfx.service';

@Component({
  selector: 'soap-prescription',
  templateUrl: './soap-prescription.component.html',
  styleUrls: ['./soap-prescription.component.less']
})
export class SOAPPrescriptionComponent implements OnInit {

  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;

  @Input() SOAPPrescriptions: Patient_SOAP_Prescription_DTO[] = []

  tempID: number = 0;

  menuItems: AdminLTEMenuItem[] = [
    {
      label: 'Add Item',
      icon: 'fa fa-stethoscope',
      visible: true
    }
  ];

  currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(private userAuth: UserAuthenticationService) { }

  ngOnInit(): void {

    this.currentUser = this.userAuth.getDecodedToken();
  }

  async menuItems_OnClick(arg: any) {

    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.label == 'Add Item') {

      this.doAddItemInventoriable();
    }
  }

  async BtnDeleteSOAPPresciption_OnClick(SOAPPresciption: any) {

    var index = GeneralfxService.findIndexByKeyValue(
      this.SOAPPrescriptions,
      "ID",
      SOAPPresciption.ID + ""
    );

    this.SOAPPrescriptions.splice(index, 1);
  }

  async doAddItemInventoriable() {

    if (this.sqllistdialog == undefined) return;

    var obj: any;
    obj = await this.sqllistdialog.open({
      sql: `/*encryptsqlstart*/
            SELECT
              ID,
              Name,
              UnitPrice,
              CurrentInventoryCount,
              UnitCost,
              OtherInfo_DateExpiration
            FROM dbo.vItemInventoriable
            /*encryptsqlend*/
            WHERE
                  ID_Company = ${this.currentUser.ID_Company}`,
      columns: [
        {
          name: 'Name',
          caption: 'Item',
          propertyType: PropertyTypeEnum.String
        },
        {
          name: 'UnitPrice',
          caption: 'Price',
          propertyType: PropertyTypeEnum.Decimal,
          format: '1.2'
        },
      ]
    });


    this.tempID--;

    obj.rows.forEach((record: any) => {
      var item = {
        ID: this.tempID,
        ID_Item: record.ID,
        Name_Item: record.Name,
      };

      if (this.SOAPPrescriptions == null) this.SOAPPrescriptions = [];
      this.SOAPPrescriptions.push(item);
    });

  }

  SOAPPresciption_onModelChanged(SOAPPresciption: any, e: IControlModelArg) {

    SOAPPresciption[e.name] = e.value;
  }
}
