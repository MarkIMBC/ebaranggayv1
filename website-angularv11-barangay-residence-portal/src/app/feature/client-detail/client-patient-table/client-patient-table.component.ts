import { GeneralfxService } from 'src/app/core/generalfx.service';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/core/UserAuthentication.service';
import { ToastService } from 'src/app/shared/toast.service';
import { FilingStatusEnum } from 'src/shared/APP_HELPER';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';

@Component({
  selector: 'client-patient-table',
  templateUrl: './client-patient-table.component.html',
  styleUrls: ['./client-patient-table.component.less']
})
export class ClientPatientTableComponent implements OnInit {
  items: AdminLTEMenuItem[] = [
    {
      label: 'New',
      icon: 'fas fa-plus',
      visible: true,
    },
  ]

  FILINGSTATUS_WAITING: number = FilingStatusEnum.Waiting;
  FILINGSTATUS_ONGOING: number =  FilingStatusEnum.Ongoing;
  FILINGSTATUS_DONE: number =  FilingStatusEnum.Done;
  FILINGSTATUS_CANCELLED: number =  FilingStatusEnum.Cancelled;

  _Code_Company: string = '';

  dataSource: any[] = [];
  loading: boolean = false;
  currentUser: TokenSessionFields = new TokenSessionFields();

  @Output() onCancel: EventEmitter<any> = new EventEmitter<any>();

  ID_Client: number = 0;

  constructor(
    private ds: DataService,
    private cs: CrypterService,
    private userAuth: UserAuthenticationService,
    private toastService: ToastService,
    private router: Router,
    protected route: ActivatedRoute
  ) {

  }

  async ngOnInit(): Promise<void> {

    this.currentUser = this.userAuth.getDecodedToken();
  }

  async menubar_OnClick(e: any){

    if (e.item.label == 'New') {

      var config = {

        ID_Client: this.ID_Client
      };

      this._Code_Company = this.route.snapshot.params["Code_Company"];
      var routeLink = ['Patient', this._Code_Company, -1];
      GeneralfxService.customNavigate(this.router, this.cs, routeLink, config)
    }
  }

  load(ID_Client: number) {

    this.ID_Client = ID_Client;

    var sql = `SELECT
                    p.ID,
                    p.Name,
                    p.Name_Gender,
                    p.Species,
                    WaitingStatus_ID_FilingStatus,
                    WaitingStatus_Name_FilingStatus,
                    waitingList.ID ID_PatientWaitingList,
                    waitingList.Code Code_PatientWaitingList
                FROM dbo.vPatient p
                LEFT JOIN vLastestPatientWaitingList waitingList
                    ON p.ID = waitingList.ID_Patient
                WHERE
                  p.ID_Client = ${this.ID_Client} AND
                  p.IsActive = 1
    `;

    sql = this.cs.encrypt(sql);
    this.ds.query<any>(sql)
      .then((obj) => {

        this.dataSource = obj;
      });
  }

  onRowClick(rowData: any){

    this._Code_Company = this.route.snapshot.params["Code_Company"];

    var routeLink = ['Patient', this._Code_Company ,rowData.ID];

    GeneralfxService.customNavigate(this.router, this.cs, routeLink, {})
  }

  patientMenuQueueWaiting_onClick(record: any){

    this.doQueue(record);
  }

  Queue_onRowClick(record: any){

    this.doQueue(record);
  }

  Cancel_onRowClick(record: any){

    this.updateStatus(record, this.FILINGSTATUS_CANCELLED);
  }

  async doQueue(record: any): Promise<any> {
    return new Promise<any>(async (res, rej) => {

      this.loading = true;


      var obj = await this.ds.execSP(
        "pDoAddPatientToWaitingList",
        {
          IDs_Patient: [record.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
        }
      );

      if (obj.Success) {
        this.toastService.success(
          `${record.Name} has been added to waiting list.`
        );

        this.loading = false;

        this.load(this.ID_Client)
        res(obj);
      } else {

        this.toastService.danger(
          obj.message
        );

        this.loading = false;
        rej(obj);
      }
    });
  }

  async updateStatus(record: any, WaitingStatus_ID_FilingStatus: number): Promise<any> {

    return new Promise<any[]>(async (res, rej) => {

      this.loading = true;

      var obj = await this.ds.execSP(
        "pUpdatePatientToWaitingListStatus",
        {
          IDs_PatientWaitingList: [record.ID_PatientWaitingList],
          WaitingStatus_ID_FilingStatus: WaitingStatus_ID_FilingStatus,
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
        }
      );

      if (obj.Success) {

        this.toastService.success(
          `${record.Name} has been updated in waiting list.`
        );

        this.load(this.ID_Client)
        this.loading = false;
        res(obj);
      } else {
        this.toastService.danger(
          obj.message
        );

        this.loading = false;
        rej(obj);
      }
    });
  }

}
