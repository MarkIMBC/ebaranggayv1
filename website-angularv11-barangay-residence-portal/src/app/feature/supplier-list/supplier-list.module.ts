import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';

import { SupplierListRoutingModule } from './supplier-list-routing.module';
import { SupplierListComponent } from './supplier-list.component';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';

@NgModule({
  declarations: [SupplierListComponent],
  imports: [
    CommonModule,
    SharedModule,
    SupplierListRoutingModule
  ],
  providers: [
    DataService,
    CrypterService,
  ],
})
export class SupplierListModule { }
