import { BreedSpecie, Patient } from './../../../shared/APP_MODELS';
import { Component, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { Patient_DTO } from 'src/shared/APP_HELPER';
import { BaseListViewComponent } from '../base-list-view/base-list-view.component';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: [
    './../base-list-view/base-list-view.component.less',
    './patient-list.component.less'
  ],
})
export class PatientListComponent extends BaseListViewComponent {

  @ViewChild("cellContactNumberTemplate")
  cellContactNumberTemplate!: TemplateRef<any>;

  headerTitle: string = 'Patient';

  CurrentObject: any = {
    Code: '',
    Name_Client: '',
    Name: '',
    Species: '',
    Name_Gender: ''
  }

  dataSource: Patient_DTO[] = []

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {


        return true;
      }
    },
    {
      label: 'Client',
      isActive: true,
      visible: true,
      command: () => {


        return true;
      }
    }
  ]

  async ListView_Onload() {

    await this.loadRecords()
  }

  async loadRecords() {

    var sql = "";
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `/*encryptsqlstart*/
            SELECT  ID,
                    Code,
                    Name_Client,
                    Name,
                    Species,
                    Name_Gender,
                    IsDeceased,
                    DateLastVisited
            FROM dbo.vPatient
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company} AND
              ISNULL(IsActive, 0) = 1
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {

    var filterString = "";

    if (this.CurrentObject.Name == null) this.CurrentObject.Name = ""
    if (this.CurrentObject.ContactNumber == null) this.CurrentObject.ContactNumber = ""

    if (this.CurrentObject.Code.length > 0) {

      filterString += `Code LIKE '%${this.CurrentObject.Code}%'`;
    }

    if (this.CurrentObject.Name_Client.length > 0) {

      if(filterString.length > 0)  filterString += ' AND '
      filterString += `Name_Client LIKE '%${this.CurrentObject.Name_Client}%'`;
    }

    if (this.CurrentObject.Name.length > 0) {

      if(filterString.length > 0)  filterString += ' AND '
      filterString += `Name LIKE '%${this.CurrentObject.Name}%'`;
    }

    if (this.CurrentObject.Species.length > 0) {

      if(filterString.length > 0)  filterString += ' AND '
      filterString += `Species LIKE '%${this.CurrentObject.Species}%'`;
    }

    if (this.CurrentObject.Name_Gender.length > 0) {

      if(filterString.length > 0)  filterString += ' AND '
      filterString += `Name_Gender LIKE '%${this.CurrentObject.Name_Gender}%'`;
    }

    return filterString;
  }

  Row_OnClick(rowData: Patient) {

    this.customNavigate(['Patient', rowData.ID], {});
  }

  menuItem_New_onClick() {

    this.customNavigate(['Patient', -1], {});
  }
}
