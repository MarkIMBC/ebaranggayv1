import { AdminLTEMenuItem } from './../../shared/AdminLTEMenuItem';
import { UserAuthenticationService } from './../../core/UserAuthentication.service';
import { GeneralfxService } from './../../core/generalfx.service';
import {
  Component,
  ElementRef,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/core/data.service';
import { TokenSessionFields } from 'src/app/core/UserAuthentication.service';
import {
  FilingStatusEnum,
  IControlModelArg,
  IFile,
  IFormValidation,
  PagingOption,
} from 'src/shared/APP_HELPER';
import { Model } from 'src/shared/APP_MODELS';
import { ToastService } from 'src/app/shared/toast.service';
import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import { CrypterService } from 'src/app/core/crypter.service';
import { AdminLTEBaseControlComponent } from 'src/app/shared/control/admin-lte-base-control/admin-lte-base-control.component';

@Component({
  selector: 'app-base-detail-view',
  templateUrl: './base-detail-view.component.html',
  styleUrls: ['./base-detail-view.component.less'],
})
export class BaseDetailViewComponent implements OnInit {
  FILINGSTATUS_FILED: number = FilingStatusEnum.Filed;
  FILINGSTATUS_APPROVED: number = FilingStatusEnum.Approved;
  FILINGSTATUS_CANCELLED: number = FilingStatusEnum.Cancelled;
  FILINGSTATUS_CONFINED: number = FilingStatusEnum.Confined;
  FILINGSTATUS_DISCHARGE: number = FilingStatusEnum.Discharged;

  @ViewChildren(AdminLTEBaseControlComponent)
  adminLTEControls!: QueryList<AdminLTEBaseControlComponent>;

  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    protected elRef: ElementRef,
    protected ds: DataService,
    public toastService: ToastService,
    private userAuth: UserAuthenticationService,
    protected cs: CrypterService
  ) {

  }

  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;

  protected _tempID: number = 0;

  ModelName: string = '';
  currentUser: TokenSessionFields = new TokenSessionFields();
  headerTitle: string = '';
  loading: boolean = true;

  displayMember: string = 'Code';
  routerFeatureName: string = '';

  menuItems: AdminLTEMenuItem[] = [];
  protected __ID_CurrentObject: number = 0;
  model: Model = new Model();

  protected isDirty: boolean = false;

  CurrentObject: any = {};
  PreviousObject: any = {};
  CurrentFiles: IFile[] = [];

  _Code_Company: string = '';
  _ReceptionPortalGuid_Company: string = '';

  async ngOnInit(): Promise<void> {

    await this.ds.loadConfig();

    window.localStorage.clear();

    this._Code_Company = this.route.snapshot.params['Code_Company'];
    this._ReceptionPortalGuid_Company =
      this.route.snapshot.params['ReceptionPortalGuid_Company'];

    if (this._Code_Company){

      await this.userAuth.LogReceptionPortal({
        CompanyCode: this._Code_Company,
        ReceptionPortalGuid: this._ReceptionPortalGuid_Company,
      });
    }
    this.currentUser = this.userAuth.getDecodedToken();
    if (
      this.currentUser.ID_User == undefined ||
      this.currentUser.ID_User == null
    ) {
      this.userAuth.LoginLastAccount();
    }

    this.loadConfigOption();

    await this.loadCurrentModel();

    this.DetailView_Init();

    this.__ID_CurrentObject = await this._getDefault__ID_CurrentObject();
    await this.loadRecord();
  }

  ngAfterViewInit() {
    this.DetailView_AfterInit();
  }

  _menuItem_New: AdminLTEMenuItem = {
    label: 'New',
    icon: 'fa fa-file',
    class: 'text-default',
    visible: true,
  };

  configOptions: any = {};

  private loadConfigOption() {
    var configOptionsString = this.route.snapshot.params['configOptions'];

    if (configOptionsString == undefined || configOptionsString == null) return;

    configOptionsString = this.cs.decrypt(configOptionsString);

    this.configOptions = JSON.parse(configOptionsString);
  }

  async _menuItem_New_onClick() {
    this.__ID_CurrentObject = -1;
    await this.loadRecord();

    var routerFeatureName = this.model.Name;
    if (this.routerFeatureName.length > 0)
      routerFeatureName = this.routerFeatureName;

    var routeLink = [routerFeatureName, this.__ID_CurrentObject];
    this.customNavigate(routeLink, this.configOptions);
  }

  _menuItem_Save: AdminLTEMenuItem = {
    label: 'Save',
    icon: 'fa fa-save',
    class: 'text-success',
    visible: true,
  };
  async _menuItem_Save_onClick() {
    this.CurrentObject_onBeforeSaving();

    var isvalid = true;

    isvalid = await this.validateRecord();
    if (!isvalid) return;

    if (this.loading == true) return;
    this.loading = true;

    this.save()
      .then(async (r) => {
        await this.loadRecord();

        this.redirectAfterSaved();

        this.toastService.success(
          `${this.CurrentObject[this.displayMember]} is saving successfull.`
        );
        this.loading = false;
      })
      .catch((reason) => {
        this.toastService.danger(
          `${this.CurrentObject[this.displayMember]} is saving failed.`
        );
        this.loading = false;
      });
  }

  public CurrentObject_onBeforeSaving() {}

  public async validateRecord() {
    this.loading = true;

    var isValid = true;
    var validationsAppForm: IFormValidation[] = [];
    var CustomValidations: IFormValidation[] = await this.validation();

    CustomValidations.forEach((customValidation) => {
      validationsAppForm.push(customValidation);
    });

    if (validationsAppForm.length > 0) {
      this.toastService.warning(validationsAppForm[0].message);
      isValid = false;
    }

    this.loading = false;

    return isValid;
  }

  protected async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    return Promise.resolve(validations);
  }

  protected redirectAfterSaved() {
    var routerFeatureName = this.model.Name;
    if (this.routerFeatureName.length > 0)
      routerFeatureName = this.routerFeatureName;

    var routeLink = [routerFeatureName, this.__ID_CurrentObject];
    this.customNavigate(routeLink, this.configOptions);
  }

  _menuItem_Refresh: AdminLTEMenuItem = {
    label: 'Refresh',
    icon: 'fa fa-sync',
    class: 'text-info',
    visible: true,
  };

  _menuItem_Delete: AdminLTEMenuItem = {
    label: 'Delete',
    icon: 'fas fa-trash-alt',
    class: 'text-danger',
    visible: true,
  };

  async _menuItem_Refresh_onClick() {
    await this.loadRecord();
  }

  loadInitMenuItem() {
    this.menuItems.push(this._menuItem_New);
    this.menuItems.push(this._menuItem_Save);

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  addMenuItem(item: AdminLTEMenuItem) {
    this.menuItems.push(item);
  }

  protected async _getDefault__ID_CurrentObject() {
    return parseInt(this.route.snapshot.params['ID_CurrentObject']);
  }

  async loadCurrentModel() {
    this.loading = true;

    var params: any = {};
    params['name'] = this.ModelName;

    var obj = await this.ds.execSP(
      `pGetModelByName`,
      {
        name: this.ModelName,
      },
      {
        isReturnObject: true,
      }
    );

    this.model = obj;

    this.loading = false;
  }

  async loadRecord() {
    await this.CurrentObject_onBeforeLoad();

    this.loading = true;

    var params: any = this.pGetRecordOptions();

    params['ID'] = this.__ID_CurrentObject;
    params['ID_Session'] = this.currentUser.ID_UserSession;

    var obj = await this.ds.execSP(`pGet${this.model.Name}`, params, {
      isReturnObject: true,
    });

    if (obj['ID_Company'] != undefined) {
      if (obj.ID_Company != this.currentUser.ID_Company) {
        this.router.navigate(['PageNotFound']);
        return;
      }
    }

    this.PreviousObject = JSON.parse(JSON.stringify(obj));
    this.CurrentObject = JSON.parse(JSON.stringify(obj));

    this.menuItems = [];

    this.loadInitMenuItem();
    this.loadMenuItems();
    this.loadRightDrowDownMenu();

    this.loading = false;
    this.isDirty = false;

    this.DetailView_onLoad();
  }

  CurrentObject_onBeforeLoad() {}

  protected pGetRecordOptions(): any {
    return {};
  }

  loadMenuItems() {}

  DetailView_Init() {}

  DetailView_AfterInit() {}

  DetailView_onLoad() {}

  initmenubar_OnClick(e: any) {
    if (e.item.label == 'New') {
      this._menuItem_New_onClick();
    } else if (e.item.label == 'Save') {
      this._menuItem_Save_onClick();
    } else if (e.item.label == 'Refresh') {
      this._menuItem_Refresh_onClick();
    } else if (e.item.label == 'Delete') {
      this.doDelete();
    }

    this.menubar_OnClick(e);
  }

  menubar_OnClick(e: any) {}

  compute() {}

  deleteDetail(detailName: string, obj: any) {
    var index = GeneralfxService.findIndexByKeyValue(
      this.CurrentObject[detailName],
      'ID',
      obj.ID + ''
    );

    this.CurrentObject.BillingInvoice_Detail.splice(index, 1);

    this.compute();
  }

  doDelete() {
    if (this.isDirty) {
      this.toastService.warning(
        `Please save first before delete ${
          this.CurrentObject[this.displayMember]
        }.`
      );
      return;
    }

    //if(confirm(`Would you like to delete ${this.CurrentObject[this.displayMember]}?`) != true) return;

    this.loading = true;

    this.CurrentObject.IsActive = 0;

    this.save()
      .then(async (r) => {
        await this.loadRecord();

        this.redirectAfterSaved();

        this.toastService.success(
          `${this.CurrentObject[this.displayMember]} is deleted successfully.`
        );
        this.loading = false;
      })
      .catch((reason) => {
        this.toastService.danger(
          `${this.CurrentObject[this.displayMember]} is deleting failed.`
        );
        this.loading = false;
      });
  }

  control_onModelChanged(e: IControlModelArg) {
    this.CurrentObject[e.name] = e.value;

    if (e.displayName != undefined) {
      this.CurrentObject[e.displayName] = e.displayValue;
    }

    var arg: CurrentObjectOnValueChangeArg = {
      name: e.name,
      value: e.value,
    };

    this.CurrentObject_onValueChange(arg);

    this.isDirty = true;
  }

  getTempID(): number {
    this._tempID -= 1;
    return this._tempID;
  }

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {}

  getFiles(): IFile[] {
    var currentFiles: IFile[] = [];

    this.adminLTEControls.forEach((i: any) => {
      if (i['file']) {
        currentFiles.push({
          file: i['file'],
          dataField: i.name,
          isImage: true,
        });
      }
    });

    return currentFiles;
  }

  public async save(): Promise<any> {
    var CurrentObject = this.CurrentObject;
    var PreviousObject = this.PreviousObject;
    var model = this.model;
    var currentUser = this.currentUser;
    var currentFiles: IFile[] = [];

    currentFiles = this.getFiles();

    if (
      CurrentObject['ID_Company'] == null &&
      CurrentObject['ID_Company'] == undefined &&
      CurrentObject.ID < 1
    ) {
      CurrentObject.ID_Company = currentUser.ID_Company;
    }

    var promise = new Promise<any>(async (resolve, reject) => {
      this.ds
        .saveObject(
          model.Oid,
          CurrentObject,
          PreviousObject,
          currentFiles,
          currentUser
        )
        .then((r) => {
          if (r.key != undefined) {
            var id = (r.key + '').replace("'", '');
            var ID_CurrentObject = parseInt(id);
            this.__ID_CurrentObject = ID_CurrentObject;

            resolve(r);
          } else {
            reject('Saving Failed....');
          }
        })
        .catch((reason) => {
          reject(reason);
        });
    });

    return promise;
  }

  rightDropDownItems: AdminLTEMenuItem[] = [];

  loadRightDrowDownMenu() {}

  rightDropDown_onMainButtonClick() {}

  rightDropDown_onMenuItemButtonClick(event: any) {}

  customNavigate(routelink: any, config?: any) {
    var configJSON = JSON.stringify(config);

    configJSON = this.cs.encrypt(configJSON);
    if (config != null && config != null) routelink.push(configJSON);

    this.router.navigate(routelink);
  }
}

export class CurrentObjectOnValueChangeArg {
  name: string = '';
  value: any;
}
