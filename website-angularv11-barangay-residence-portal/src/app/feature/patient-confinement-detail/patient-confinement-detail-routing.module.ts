import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientConfinementDetailComponent } from './patient-confinement-detail.component';

const routes: Routes = [{ path: '', component: PatientConfinementDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientConfinementDetailRoutingModule { }
