import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from './../../../shared/AdminLTEMenuItem';
import { FilingStatusEnum, IControlModelArg, ItemTypeEnum, Patient_Confinement_ItemsServices_DTO, PropertyTypeEnum } from './../../../../shared/APP_HELPER';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { GeneralfxService } from 'src/app/core/generalfx.service';

@Component({
  selector: 'confinement-items-services-table',
  templateUrl: './confinement-items-services-table.component.html',
  styleUrls: ['./confinement-items-services-table.component.less']
})
export class ConfinementItemsServicesTableComponent implements OnInit {

  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;

  @Input() itemsServices: Patient_Confinement_ItemsServices_DTO[] = []

  @Output() onModelChanged: EventEmitter<any> = new EventEmitter<any>();
  @Output() onMenuItemClick: EventEmitter<any> = new EventEmitter<any>();

  tempID: number = 0;

  menuItems: AdminLTEMenuItem[] = [];

  loadMenuByCurrentObject(CurrentObject: any) {

    var menuItem_AddService = {
      label: 'Add Service',
      icon: 'fa fa-stethoscope',
      visible: true
    }

    var menuItem_AddItem = {
      label: 'Add Item',
      icon: 'fas fa-cubes',
      visible: true
    }
    var menuItem_CreateBillingInvoice = {
      label: 'Create Invoice',
      icon: 'fas fa-file',
      visible: true
    }
    var menuItem_viewBillingInvoice = {
      label: 'View Invoice',
      icon: 'fas fa-file',
      visible: true
    }

    this.menuItems = [];

    if (CurrentObject.ID_FilingStatus == FilingStatusEnum.Confined) {

      this.menuItems.push(menuItem_AddItem);
      this.menuItems.push(menuItem_AddService);
    }

    if (CurrentObject.ID_BillingInvoice) {

      this.menuItems.push(menuItem_viewBillingInvoice);
    } else {

      if (CurrentObject.ID_FilingStatus == FilingStatusEnum.Discharged) {

        this.menuItems.push(menuItem_CreateBillingInvoice);
      }
    }
  }

  currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(private userAuth: UserAuthenticationService) { }

  ngOnInit(): void {

    this.currentUser = this.userAuth.getDecodedToken();
  }

  async menuItems_OnClick(arg: any) {

    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.label == 'Add Service') {

      this.doAddService();
    } else if (menuItem.label == 'Add Item') {

      this.doAddItem();
    }

    this.onMenuItemClick.emit(arg);
  }

  async BtnDeleteSOAPPlan_OnClick(itemsService: any) {

    var index = GeneralfxService.findIndexByKeyValue(
      this.itemsServices,
      "ID",
      itemsService.ID + ""
    );

    this.itemsServices.splice(index, 1);
  }

  async doAddService() {

    if (this.sqllistdialog == undefined) return;

    var obj: any;
    obj = await this.sqllistdialog.open({
      sql: `/*encryptsqlstart*/
            SELECT
              ID,
              Name,
              UnitPrice,
              CurrentInventoryCount,
              UnitCost,
              OtherInfo_DateExpiration
            FROM dbo.vItemService
            /*encryptsqlend*/
            WHERE
                  ID_Company = ${this.currentUser.ID_Company}`,
      columns: [
        {
          name: 'Name',
          caption: 'Item',
          propertyType: PropertyTypeEnum.String
        },
        {
          name: 'UnitPrice',
          caption: 'Price',
          propertyType: PropertyTypeEnum.Decimal,
          format: '1.2'
        },
      ]
    });

    this.tempID--;

    obj.rows.forEach((record: any) => {

      var item = {
        ID: this.tempID,
        ID_Item: record.ID,
        Name_Item: record.Name,
        Quantity: 0,
        UnitCost: record.UnitCost,
        UnitPrice: record.UnitPrice
      };

      if (record.ID_ItemType == ItemTypeEnum.Service) item.Quantity = 1;

      if (this.itemsServices == null) this.itemsServices = [];
      this.itemsServices.push(item);
    });

  }

  async doAddItem() {

    if (this.sqllistdialog == undefined) return;

    var obj: any;
    obj = await this.sqllistdialog.open({
      sql: `SELECT
                  ID,
                  Name,
                  UnitPrice,
                  FormattedCurrentInventoryCount,
                  RemainingBeforeExpired
            FROM dbo.vItemInventoriableForBillingLookUp
            WHERE
              ID_Company = ${this.currentUser.ID_Company}
      `,
      columns: [
        {
          name: 'Name',
          caption: 'Item',
          propertyType: PropertyTypeEnum.String
        },
        {
          name: 'UnitPrice',
          caption: 'Price',
          propertyType: PropertyTypeEnum.Decimal,
          format: '1.2'
        },
      ]
    });

    this.tempID--;

    obj.rows.forEach((record: any) => {

      var item = {
        ID: this.tempID,
        ID_Item: record.ID,
        Name_Item: record.Name,
        Quantity: 0,
        UnitCost: record.UnitCost,
        UnitPrice: record.UnitPrice
      };

      if (record.ID_ItemType == ItemTypeEnum.Service) item.Quantity = 1;

      if (this.itemsServices == null) this.itemsServices = [];
      this.itemsServices.push(item);
    });

  }

  itemsService_onModelChanged(itemsService: any, e: IControlModelArg) {

    itemsService[e.name] = e.value;

    this.onModelChanged.emit();
  }
}
