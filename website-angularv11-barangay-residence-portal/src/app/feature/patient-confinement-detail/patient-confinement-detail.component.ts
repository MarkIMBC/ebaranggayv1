import { FilingStatusEnum, IFormValidation, ItemTypeEnum, Patient_Confinement_ItemsServices_DTO } from './../../../shared/APP_HELPER';
import { Component, ViewChild } from '@angular/core';
import { AdminLTEDataLookupboxComponent, AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { BaseDetailViewComponent, CurrentObjectOnValueChangeArg } from '../base-detail-view/base-detail-view.component';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { ConfinementSOAPListComponent } from './confinement-soap-list/confinement-soap-list.component';
import { ConfinementItemsServicesTableComponent } from './confinement-items-services-table/confinement-items-services-table.component';
import { AdjustCreditDialogComponent } from '../../shared/adjust-credit-dialog/adjust-credit-dialog.component';

@Component({
  selector: 'app-patient-confinement-detail',
  templateUrl: './patient-confinement-detail.component.html',
  styleUrls: [
    './../base-detail-view/base-detail-view.component.less',
    './patient-confinement-detail.component.less'
  ]
})
export class PatientConfinementDetailComponent extends BaseDetailViewComponent {

  @ViewChild("confinementsoaplist")
  confinementsoaptable: ConfinementSOAPListComponent | undefined;

  @ViewChild("confinementitemsservicestable")
  confinementitemsservicestable: ConfinementItemsServicesTableComponent | undefined;

  @ViewChild('adjustcreditdialog')
  adjustcreditdialog: AdjustCreditDialogComponent | undefined;

  @ViewChild('ID_Patient_Lookupbox') ID_Patient_Lookupbox: AdminLTEDataLookupboxComponent | undefined

  ModelName: string = 'Patient_Confinement'
  headerTitle: string = 'Confinement'

  routerFeatureName: string = 'Confinement';
  displayMember: string = "Code";

  ID_Client_LookupboxOption: AdminLTEDataLookupboxOption = {

    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        }
      ]
    }
  }

  ID_Patient_LookupboxOption: AdminLTEDataLookupboxOption = {

    listviewOption: {
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        }
      ]
    }
  }

  loadInitMenuItem() {

    this.menuItems.push(this._menuItem_New);

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Confined) {

      this.menuItems.push(this._menuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {

      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  _menuItem_AdjustCredit: AdminLTEMenuItem = {
    label: 'Adjust Credit',
    icon: 'fa fa-save',
    class: 'text-info',
    visible: true
  }

  loadMenuItems() {

    var menuItemDischarge = {
      label: 'Discharge',
      icon: 'fa fa-file',
      class: 'text-primary',
      visible: true,
    }

    var menuItemCancel = {
      label: 'Cancel',
      icon: 'fa fa-times',
      class: 'text-danger',
      visible: true,
    }

    if (this.CurrentObject.ID > 0) {

      if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Confined) {

        this.addMenuItem(menuItemDischarge);
        this.addMenuItem(menuItemCancel);
      }

      this.addMenuItem(this._menuItem_AdjustCredit);
    }
  }

  async menubar_OnClick(e: any) {

    if (e.item.label == 'Discharge') {

      this.doDischarge();
    } else if (e.item.label == 'Cancel') {

      this.doCancel();
    } else if (e.item.label == 'Adjust Credit') {

      var ID_Client = this.CurrentObject.ID_Client;
      if (this.adjustcreditdialog) {

        await this.adjustcreditdialog.show(ID_Client);
        this.loadRecord()
      }
    }
  }

  async doDischarge() {

    if (this.isDirty) {

      this.toastService.warning(`Please save first before discharging ${this.CurrentObject.Code}.`);
      return;
    }

    this.loading = true;

    this.ds.execSP(
      "pDischargePatient_Confinement",
      {
        IDs_Patient_Confinement: [this.CurrentObject.ID],
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      { isReturnObject: true }
    ).then(async (obj) => {

      if (obj.Success) {

        await this.loadRecord();
        this.toastService.success(`${this.CurrentObject.Code} has been discharged successfully.`);
      } else {

        this.toastService.danger(obj.message);
      }

      this.loading = false;
    }).catch(() => {

      this.toastService.danger(`Unable to discharge ${this.CurrentObject.Code}.`);
      this.loading = false;
    });
  }

  async doCancel() {

    if (this.isDirty) {

      this.toastService.warning(`Please save first before cancel ${this.CurrentObject.Code}.`);
      return;
    }

    this.loading = true;

    this.ds.execSP(
      "pCancelPatient_Confinement",
      {
        IDs_Patient_Confinement: [this.CurrentObject.ID],
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      { isReturnObject: true }
    ).then(async (obj) => {

      if (obj.Success) {

        await this.loadRecord();
        this.toastService.success(`${this.CurrentObject.Code} has been canceled successfully.`);
      } else {

        this.toastService.danger(obj.message);
      }

      this.loading = false;
    }).catch(() => {

      this.toastService.danger(`Unable to cancel ${this.CurrentObject.Code}.`);
      this.loading = false;
    });
  }

  DetailView_onLoad() {

    var listViewOptionID_Client = this.ID_Client_LookupboxOption.listviewOption;

    if (this.CurrentObject.Patient_Confinement_ItemsServices == null) {

      this.CurrentObject.Patient_Confinement_ItemsServices = [];
    }

    if (listViewOptionID_Client != undefined) {
      listViewOptionID_Client.sql = `/*encryptsqlstart*/
                                      SELECT
                                            ID,
                                            Name
                                      FROM dbo.tClient
                                      WHERE
                                            ID_Company = ${this.currentUser.ID_Company}
                                      /*encryptsqlend*/`;

      var listViewOptionID_Patient = this.ID_Patient_LookupboxOption.listviewOption;
      if (listViewOptionID_Patient == undefined) return;

      listViewOptionID_Patient.sql = `/*encryptsqlstart*/
                              SELECT
                                    ID,
                                    Name,
                                    Name_Client
                              FROM dbo.vPatient
                              WHERE
                                    ID_Company = ${this.currentUser.ID_Company} AND
                                    ID_Client = ${this.CurrentObject.ID_Client}
                              /*encryptsqlend*/`;

    }

    if (this.confinementsoaptable) {

      this.confinementsoaptable.loadMenuByCurrentObject(this.CurrentObject);
      this.confinementsoaptable.load(this.CurrentObject.ID)
    }

    if (this.confinementitemsservicestable) {

      this.confinementitemsservicestable.loadMenuByCurrentObject(this.CurrentObject);
    }
  }

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {

    if (e.name == 'ID_Client') {

      var listViewOptionID_Patient = this.ID_Patient_LookupboxOption.listviewOption;
      if (listViewOptionID_Patient == undefined) return;

      listViewOptionID_Patient.sql = `/*encryptsqlstart*/
                              SELECT
                                    ID,
                                    Name,
                                    Name_Client
                              FROM dbo.vPatient
                              WHERE
                                    ID_Company = ${this.currentUser.ID_Company} AND
                                    ID_Client = ${this.CurrentObject.ID_Client}
                              /*encryptsqlend*/`;

      if (this.ID_Patient_Lookupbox == undefined) return;
      this.ID_Patient_Lookupbox?.removeValue();
    }
  }

  compute() {

    var totalAmount: number = 0;

    this.CurrentObject.Patient_Confinement_ItemsServices.forEach((obj: Patient_Confinement_ItemsServices_DTO) => {

      var Quantity = obj.Quantity;
      var UnitPrice = obj.UnitPrice;
      var amount = 0;

      if (Quantity == undefined || Quantity == null) Quantity = 0;
      if (UnitPrice == undefined || UnitPrice == null) UnitPrice = 0;

      amount = Quantity * UnitPrice;
      obj.Amount = amount;

      totalAmount += amount;
    });

    this.CurrentObject.SubTotal = totalAmount;
    this.CurrentObject.TotalAmount = totalAmount;
  }

  async validation(): Promise<IFormValidation[]> {

    var validations: IFormValidation[] = [];

    var ID_Client = this.CurrentObject.ID_Client;
    var ID_Patient = this.CurrentObject.ID_Patient;

    if (this.CurrentObject.ID_Client == undefined || this.CurrentObject.ID_Client == null) ID_Client = 0;
    if (this.CurrentObject.ID_Patient == undefined || this.CurrentObject.ID_Patient == null) ID_Patient = 0;

    if (ID_Client == 0) {
      validations.push({
        message: "Client is required.",
      });
    }

    if (ID_Patient == 0) {
      validations.push({
        message: "Patient is required.",
      });
    }

    this.CurrentObject.Patient_Confinement_ItemsServices.forEach((itemService: { Quantity: number | null; Name_Item: any; }) => {

      if (itemService.Quantity == null || itemService.Quantity == 0) {

        validations.push({
          message: `Quantity is required for ${itemService.Name_Item}.`,
        });
      }
    });

    if (validations.length == 0) {

      var validateObj = await this.ds.execSP(
        "pPatient_Confinement_Validation",
        {
          ID_Patient_Confinement: this.CurrentObject.ID,
          ID_Patient: this.CurrentObject.ID_Patient,
          ID_UserSession: this.currentUser.ID_UserSession
        },
        {
          isReturnObject: true,
        }
      );

      if (validateObj.isValid != true) {

        validations.push({ message: validateObj.message });
      }
    }

    return Promise.resolve(validations);
  }

  confinementitemsservicestable_onModelChanged() {

    this.compute();
  }

  confinementitemsservicestable_onMenuItemClick(arg: any) {

    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.label == 'Create Invoice') {

      this.doCreateInvoice();
    } else if (menuItem.label == 'View Invoice') {

      this.doViewInvoice();
    }
  }

  async doCreateInvoice() {

    this.customNavigate(['BillingInvoice', -1], {
      ID_Patient_Confinement: this.CurrentObject.ID,
    });
  }

  async doViewInvoice() {

    this.customNavigate(['BillingInvoice', this.CurrentObject.ID_BillingInvoice], {});
  }

  protected pGetRecordOptions(): any {

    var options: any = {};
    var configKeys = ['ID_Patient_SOAP'];

    configKeys.forEach((key) => {

      if (this.configOptions[key] != undefined && this.configOptions[key] != null) {

        options[key] = this.configOptions[key]
      }
    });

    return options;
  }
}
