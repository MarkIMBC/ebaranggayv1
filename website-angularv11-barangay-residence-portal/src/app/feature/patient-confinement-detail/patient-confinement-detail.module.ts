import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PatientConfinementDetailRoutingModule } from './patient-confinement-detail-routing.module';
import { PatientConfinementDetailComponent } from './patient-confinement-detail.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ConfinementSOAPListComponent } from './confinement-soap-list/confinement-soap-list.component';
import { ConfinementItemsServicesTableComponent } from './confinement-items-services-table/confinement-items-services-table.component';


@NgModule({
  declarations: [PatientConfinementDetailComponent, ConfinementSOAPListComponent, ConfinementItemsServicesTableComponent],
  imports: [
    CommonModule,
    SharedModule,
    PatientConfinementDetailRoutingModule
  ]
})
export class PatientConfinementDetailModule { }
