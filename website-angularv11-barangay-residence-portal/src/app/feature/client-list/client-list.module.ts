import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { ClientRoutingModule } from './client-list-routing.module';
import { ClientListComponent } from './client-list.component';
import { DataService } from 'src/app/core/data.service';
import { CrypterService } from 'src/app/core/crypter.service';


@NgModule({
  declarations: [ClientListComponent],
  imports: [
    CommonModule,
    SharedModule,
    ClientRoutingModule
  ],
  providers: [
    DataService,
    CrypterService,
  ],
})
export class ClientModule { }
