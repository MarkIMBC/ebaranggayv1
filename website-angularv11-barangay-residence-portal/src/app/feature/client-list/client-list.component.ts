import { BaseListViewComponent } from '../base-list-view/base-list-view.component';
import { Component } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { Client_DTO } from 'src/shared/APP_HELPER';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: [
    './../base-list-view/base-list-view.component.less',
    './client-list.component.less'
  ],
})
export class ClientListComponent extends BaseListViewComponent {

  headerTitle: string = 'Client';

  CurrentObject: any = {
    Name: '',
    ContactNumbers: ''
  }

  dataSource: Client_DTO[] = []

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {


        return true;
      }
    },
    {
      label: 'Client',
      isActive: true,
      visible: true,
      command: () => {


        return true;
      }
    }
  ]

  async ListView_Onload() {

    await this.loadRecords()
  }

  async loadRecords() {

    var sql = "";
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `/*encryptsqlstart*/
            SELECT  ID,
                    Name,
                    ContactNumbers,
                    DateCreated
            FROM dbo.vClient_ListView
            /*encryptsqlend*/
            WHERE
            ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {

    var filterString = "";

    if (this.CurrentObject.Name == null) this.CurrentObject.Name = ""
    if (this.CurrentObject.ContactNumber == null) this.CurrentObject.ContactNumber = ""

    if (this.CurrentObject.Name.length > 0) {

      filterString += `Name LIKE '%${this.CurrentObject.Name}%'`;
    }

    if (this.CurrentObject.ContactNumber.length > 0) {

      if(filterString.length > 0)  filterString += ' AND '
      filterString += `ContactNumbers LIKE '%${this.CurrentObject.ContactNumber}%'`;
    }


    return filterString;
  }

  Row_OnClick(rowData: Client_DTO) {

    this.customNavigate(['Client', rowData.ID], {});
  }

  menuItem_New_onClick() {

    this.customNavigate(['Client', -1], {});
  }
}
