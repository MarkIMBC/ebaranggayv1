import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportBillingInvoicePaidListComponent } from './report-billing-invoice-paid-list.component';

const routes: Routes = [{ path: '', component: ReportBillingInvoicePaidListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportBillingInvoicePaidListRoutingModule { }
