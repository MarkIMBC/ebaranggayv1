import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportBillingInvoicePaidListRoutingModule } from './report-billing-invoice-paid-list-routing.module';
import { ReportBillingInvoicePaidListComponent } from './report-billing-invoice-paid-list.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ReportBillingInvoicePaidListComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReportBillingInvoicePaidListRoutingModule
  ]
})
export class ReportBillingInvoicePaidListModule { }
