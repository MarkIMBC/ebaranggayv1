import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';

import { ItemServiceListRoutingModule } from './item-service-list-routing.module';
import { ItemServiceListComponent } from './item-service-list.component';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';


@NgModule({
  declarations: [ItemServiceListComponent],
  imports: [
    CommonModule,
    SharedModule,
    ItemServiceListRoutingModule
  ],
  providers: [
    DataService,
    CrypterService,
  ],
})
export class ItemServiceListModule { }
