import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ItemServiceListComponent } from './item-service-list.component';

const routes: Routes = [{ path: '', component: ItemServiceListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemServiceListRoutingModule { }
