import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndigencyCertificateFormComponent } from './indigency-certificate-form.component';

describe('IndigencyCertificateFormComponent', () => {
  let component: IndigencyCertificateFormComponent;
  let fixture: ComponentFixture<IndigencyCertificateFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndigencyCertificateFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndigencyCertificateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
