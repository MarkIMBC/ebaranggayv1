import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { ClientPatients } from '../client-registration.component';

@Component({
  selector: 'consent-agreement-dialog-box',
  templateUrl: './consent-agreement-dialog-box.component.html',
  styleUrls: ['./consent-agreement-dialog-box.component.less'],
})
export class ConsentAgreementDialogBoxComponent implements OnInit {
  @Input() Client: ClientPatients = new ClientPatients();

  @ViewChild('modalDialog') modalDialog: ModalComponent | undefined;

  IsAgree: boolean = false;

  currentUser: TokenSessionFields = new TokenSessionFields();

  ConsentFormHtMLString: string = '';

  constructor(
    private ds: DataService,
    private userAuth: UserAuthenticationService
  ) {}

  async ngOnInit(): Promise<void> {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  async show(): Promise<any> {
    var obj = await this.loadConsentFormHtMLString();

    this.ConsentFormHtMLString = obj.ConsentFormHtMLString;

    var modalDialog = this.modalDialog;

    this.IsAgree = false;

    var promise = new Promise<any>(async (resolve, reject) => {

      if (modalDialog == undefined) {

        reject('no modal instantiate..');
      } else{

        await modalDialog.open();

        if(this.IsAgree){
          resolve('');
        }else{
          reject(null);
        }
      }
    });

    return promise;
  }

  btnAgreement_onClick() {

    if (this.modalDialog == undefined) return;

    this.modalDialog.close();
    this.IsAgree = true;
  }

  btnClose_onClick() {

    if (this.modalDialog == undefined) return;
    this.modalDialog.close();
  }

  async loadConsentFormHtMLString(): Promise<any> {
    return new Promise<any>(async (res, rej) => {
      var obj = await this.ds.execSP(
        'pGetConsentFormHtMLString',
        {
          'ID_UserSession':this.currentUser.ID_UserSession
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        res(obj);
      } else {
        rej(obj);
      }
    });
  }
}
