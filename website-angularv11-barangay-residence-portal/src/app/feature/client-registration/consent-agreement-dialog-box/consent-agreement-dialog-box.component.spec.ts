import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsentAgreementDialogBoxComponent } from './consent-agreement-dialog-box.component';

describe('ConsentAgreementDialogBoxComponent', () => {
  let component: ConsentAgreementDialogBoxComponent;
  let fixture: ComponentFixture<ConsentAgreementDialogBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsentAgreementDialogBoxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsentAgreementDialogBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
