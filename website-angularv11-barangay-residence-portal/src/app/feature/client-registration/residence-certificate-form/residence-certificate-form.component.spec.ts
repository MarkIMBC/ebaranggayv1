import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidenceCertificateFormComponent } from './residence-certificate-form.component';

describe('ResidenceCertificateFormComponent', () => {
  let component: ResidenceCertificateFormComponent;
  let fixture: ComponentFixture<ResidenceCertificateFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResidenceCertificateFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidenceCertificateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
