import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import { IControlModelArg, PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { CurrentObjectOnValueChangeArg } from '../../base-detail-view/base-detail-view.component';

@Component({
  selector: 'residence-certificate-form',
  templateUrl: './residence-certificate-form.component.html',
  styleUrls: ['./residence-certificate-form.component.less'],
})
export class ResidenceCertificateFormComponent implements OnInit {
  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;
  @Input() Record: any = {};

  ID_Gender_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM tGender',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  constructor() {}

  ngOnInit(): void {}

  control_onModelChanged(e: IControlModelArg) {
    this.Record[e.name] = e.value;

    if (e.displayName != undefined) {
      this.Record[e.displayName] = e.displayValue;
    }

    var arg: CurrentObjectOnValueChangeArg = {
      name: e.name,
      value: e.value,
    };
  }

  hasValues(): boolean {

    let size = 0;
    for (let k in this.Record) {
      size++;
    }
    return size > 0;
  }

  resetFields() {
    this.Record = {};
  }
}
