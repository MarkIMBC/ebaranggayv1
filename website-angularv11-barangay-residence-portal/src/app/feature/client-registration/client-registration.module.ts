import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientRegistrationRoutingModule } from './client-registration-routing.module';
import { ClientRegistrationComponent } from './client-registration.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PatientTableComponent } from './patient-table/patient-table.component';
import { ConsentAgreementDialogBoxComponent } from './consent-agreement-dialog-box/consent-agreement-dialog-box.component';
import { ResidenceCertificateFormComponent } from './residence-certificate-form/residence-certificate-form.component';
import { BarangayBusinessClearanceFormComponent } from './barangay-business-clearance-form/barangay-business-clearance-form.component';
import { CommunityTaxCertificateFormComponent } from './community-tax-certificate-form/community-tax-certificate-form.component';
import { IndigencyCertificateFormComponent } from './indigency-certificate-form/indigency-certificate-form.component';
import { AnnouncementDisplayComponent } from './announcement-display/announcement-display.component';
import { BarangayIDRegisitrationFormComponent } from './barangay-id-regisitration-form/barangay-id-regisitration-form.component';

@NgModule({
  declarations: [
    ClientRegistrationComponent,
    PatientTableComponent,
    ConsentAgreementDialogBoxComponent,
    ResidenceCertificateFormComponent,
    BarangayBusinessClearanceFormComponent,
    CommunityTaxCertificateFormComponent,
    IndigencyCertificateFormComponent,
    AnnouncementDisplayComponent,
    BarangayIDRegisitrationFormComponent,
  ],
  imports: [CommonModule, SharedModule, ClientRegistrationRoutingModule],
})
export class ClientRegistrationModule {}
