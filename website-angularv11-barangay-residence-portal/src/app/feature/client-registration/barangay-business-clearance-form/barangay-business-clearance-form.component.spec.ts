import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BarangayBusinessClearanceFormComponent } from './barangay-business-clearance-form.component';

describe('BarangayBusinessClearanceFormComponent', () => {
  let component: BarangayBusinessClearanceFormComponent;
  let fixture: ComponentFixture<BarangayBusinessClearanceFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BarangayBusinessClearanceFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BarangayBusinessClearanceFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
