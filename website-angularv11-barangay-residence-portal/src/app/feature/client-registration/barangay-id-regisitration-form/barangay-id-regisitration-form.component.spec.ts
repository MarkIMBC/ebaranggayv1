import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BarangayIDRegisitrationFormComponent } from './barangay-id-regisitration-form.component';

describe('BarangayIDRegisitrationFormComponent', () => {
  let component: BarangayIDRegisitrationFormComponent;
  let fixture: ComponentFixture<BarangayIDRegisitrationFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BarangayIDRegisitrationFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BarangayIDRegisitrationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
