import { ResidentRequest, ResidentRequest_Certificate, ResidentRequest_BarangayBusinessClearance, ResidentRequest_CommunityTaxCertificate } from './../../../shared/APP_MODELS';
import { BarangayBusinessClearanceFormComponent } from './barangay-business-clearance-form/barangay-business-clearance-form.component';
import { CommunityTaxCertificateFormComponent } from './community-tax-certificate-form/community-tax-certificate-form.component';
import { ResidenceCertificateFormComponent } from './residence-certificate-form/residence-certificate-form.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import { IControlModelArg, IFormValidation } from 'src/shared/APP_HELPER';
import { BaseDetailViewComponent, CurrentObjectOnValueChangeArg } from '../base-detail-view/base-detail-view.component';
import { BaseListViewComponent } from '../base-list-view/base-list-view.component';
import { ConsentAgreementDialogBoxComponent } from './consent-agreement-dialog-box/consent-agreement-dialog-box.component';
import { IndigencyCertificateFormComponent } from './indigency-certificate-form/indigency-certificate-form.component';

@Component({
  selector: 'app-client-registration',
  templateUrl: './client-registration.component.html',
  styleUrls: [
    './../base-detail-view/base-detail-view.component.less',
    './client-registration.component.less',
  ],
})
export class ClientRegistrationComponent extends BaseDetailViewComponent {
  ModelName: string = 'ResidentRequest';
  headerTitle: string = 'ResidentRequest';

  displayMember: string = 'Name';

  @ViewChild('residencecertificateform') residencecertificateform: ResidenceCertificateFormComponent | undefined;
  @ViewChild('barangaybusinessclearanceform') barangaybusinessclearanceform: BarangayBusinessClearanceFormComponent | undefined;
  @ViewChild('communitytaxcertificateform') communitytaxcertificateform: CommunityTaxCertificateFormComponent | undefined;
  @ViewChild('consentagreementdialogbox') consentagreementdialogbox: ConsentAgreementDialogBoxComponent | undefined;
  @ViewChild('indigencycertificateform') indigencycertificateform: IndigencyCertificateFormComponent | undefined;

  CurrentObject: any = new ResidentRequest();

  async DetailView_Init() {
    var obj = await this.ds.execSP(
      'pGetRecordPaging',
      {
        sql: `SELECT Name FROM tCompany WHERE ID = ${this.currentUser.ID_Company}`,
        PageNumber: 1,
        DisplayCount: 1,
        OrderByString: '',
      },
      {
        isReturnObject: true,
      }
    );

    this.headerTitle = `Barangay Residence Portal (${obj.Records[0].Name})`;
  }

  loadInitMenuItem() {
    this.menuItems.push({
      label: 'Save',
      icon: 'fa fa-save',
      class: 'text-success',
      visible: true,
    });
  }

  async CurrentObject_onBeforeLoad() {
    this.__ID_CurrentObject = -1;
  }

  async validation(): Promise<IFormValidation[]> {

    var validations: IFormValidation[] = [];

    if (this.CurrentObject.Patients == null || this.CurrentObject.Patients == undefined) this.CurrentObject.Patients = [];

    if (this.CurrentObject.Name == null) this.CurrentObject.Name = "";

    if (this.CurrentObject.Name.length == 0) {
      validations.push({
        message: `Name is required.`,
      });
    }

    return Promise.resolve(validations);
  }

  control_onModelChanged(e: IControlModelArg) {
    this.CurrentObject[e.name] = e.value;

    if (e.displayName != undefined) {


    }

    var arg: CurrentObjectOnValueChangeArg = {
      name: e.name,
      value: e.value,
    };

    this.CurrentObject_onValueChange(arg);

    this.isDirty = true;
  }

  public CurrentObject_onBeforeSaving() {

    this.CurrentObject.ResidentRequest_Certificate = [];
    this.CurrentObject.ResidentRequest_BarangayBusinessClearance = [];
    this.CurrentObject.ResidentRequest_CommunityTaxCertificate = [];
    this.CurrentObject.ResidentRequest_IndigencyCertificate = [];


    if (this.residencecertificateform) {

      if (this.residencecertificateform.hasValues()) {
        this.residencecertificateform.Record.ID_Company = this.currentUser.ID_Company;
        this.residencecertificateform.Record.ID = this.getTempID();
        this.CurrentObject.ResidentRequest_Certificate.push(this.residencecertificateform.Record);
      }
    }

    if (this.barangaybusinessclearanceform) {

      if (this.barangaybusinessclearanceform.hasValues()) {
        this.barangaybusinessclearanceform.Record.ID = this.getTempID();
        this.barangaybusinessclearanceform.Record.ID_Company = this.currentUser.ID_Company;
        this.CurrentObject.ResidentRequest_BarangayBusinessClearance.push(this.barangaybusinessclearanceform.Record);
      }
    }

    if (this.communitytaxcertificateform) {

      if (this.communitytaxcertificateform.hasValues()) {
        this.communitytaxcertificateform.Record.ID = this.getTempID();
        this.communitytaxcertificateform.Record.ID_Company = this.currentUser.ID_Company;
        this.CurrentObject.ResidentRequest_CommunityTaxCertificate.push(this.communitytaxcertificateform.Record);
      }
    }

    if (this.indigencycertificateform) {

      if (this.indigencycertificateform.hasValues()) {
        this.indigencycertificateform.Record.ID = this.getTempID();
        this.indigencycertificateform.Record.ID_Company = this.currentUser.ID_Company;
        this.indigencycertificateform.Record.Name = this.CurrentObject.Name;
        this.CurrentObject.ResidentRequest_IndigencyCertificate.push(this.indigencycertificateform.Record);
      }
    }
  }

  protected redirectAfterSaved() {
    var routerFeatureName = this.model.Name;
    if (this.routerFeatureName.length > 0)
      routerFeatureName = this.routerFeatureName;

    var routeLink = [
      this._Code_Company,
      this._ReceptionPortalGuid_Company,
      'BaranggayRegistrationPortal',
    ];
    this.customNavigate(routeLink);


    if (this.residencecertificateform) {

      this.residencecertificateform.resetFields();
    }

    if (this.barangaybusinessclearanceform) {

      this.barangaybusinessclearanceform.resetFields();
    }

    if (this.communitytaxcertificateform) {

      this.communitytaxcertificateform.resetFields();
    }

    if (this.indigencycertificateform) {

      this.indigencycertificateform.resetFields();
    }
  }

  async _menuItem_Save_onClick() {
    this.CurrentObject_onBeforeSaving();

    var isvalid = true;

    isvalid = await this.validateRecord();
    if (!isvalid) return;

    if (this.loading == true) return;
    this.loading = true;

    this.save()
      .then(async (r) => {
        await this.loadRecord();

        this.redirectAfterSaved();

        this.toastService.success(
          `Request has been sent successfully.`
        );
        this.loading = false;
      })
      .catch((reason) => {
        this.toastService.danger(
          `Request has been failled. Please try again...`
        );
        this.loading = false;
      });
  }
}
export class ClientPatients {

  Name: string = '';
  Email: string = '';
  ContactNumber: string = '';
  ContactNumber2: string = '';
  Address: string = '';
  Comment: string = '';
  Patients: any[] = []
}
