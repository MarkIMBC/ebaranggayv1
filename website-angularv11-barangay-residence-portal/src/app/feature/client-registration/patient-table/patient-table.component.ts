import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import { ToastService } from 'src/app/shared/toast.service';
import { IControlModelArg, PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { CurrentObjectOnValueChangeArg } from '../../base-detail-view/base-detail-view.component';
import { ClientPatients } from '../client-registration.component';

@Component({
  selector: 'patient-table',
  templateUrl: './patient-table.component.html',
  styleUrls: ['./patient-table.component.less']
})
export class PatientTableComponent implements OnInit {

  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;

  @Input() Client: ClientPatients = new ClientPatients();

  BreedSpecieList: string[] = []

  currentUser: TokenSessionFields = new TokenSessionFields();
  tempID: number = 0;

  menuItems: AdminLTEMenuItem[] = [
    {
      label: 'Add Pet',
      icon: 'fa fa-plus',
      visible: true
    }
  ];

  constructor(
    private ds: DataService,
    private cs: CrypterService,
    private userAuth: UserAuthenticationService,
    private toastService: ToastService,
    private router: Router
  ) {

  }

  async loadBreedSpecieList(): Promise<void> {

    var sql = this.cs.encrypt(
      `SELECT
              ID,
              Name
        FROM dbo.tBreedSpecie
    `);

    var _BreedSpecieList:string[] = [];

    var objs = await this.ds.query<any>(sql);

    this.BreedSpecieList = [];
    objs.forEach(function (obj) {

      _BreedSpecieList.push(obj.Name);
    });

    this.BreedSpecieList = _BreedSpecieList;
  }

  ID_Gender_LookupboxOption: AdminLTEDataLookupboxOption = {

    listviewOption: {
      sql: 'SELECT ID, Name FROM tGender',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        }
      ]
    }
  }

  async menuItems_OnClick(arg: any) {

    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.label == 'Add Pet') {

      if(this.Client.Patients == undefined || this.Client.Patients == null) this.Client.Patients = [];

      this.Client.Patients.push({
        Name_Patient: '',
        Species_Patient: '',
        ID_Gender_Patient: 0,
        Name_Gender_Patient: ''
      });
    }
  }

  ngOnInit(): void {

    this.loadBreedSpecieList();
  }

  PatientRow_onModelChanged(patient: any, e: IControlModelArg) {
    patient[e.name] = e.value;

    if (e.displayName != undefined) {
      patient[e.displayName] = e.displayValue;
    }

    var arg: CurrentObjectOnValueChangeArg = {
      name: e.name,
      value: e.value,
    };

    // this.CurrentObject_onValueChange(arg);

    // this.isDirty = true;
  }

  async BtnDeleteSOAPPresciption_OnClick(patient: any) {

    var index = GeneralfxService.findIndexByKeyValue(
      this.Client.Patients,
      "Name_Patient",
      patient.Name + ""
    );

    this.Client.Patients.splice(index, 1);
  }
}
