import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunityTaxCertificateFormComponent } from './community-tax-certificate-form.component';

describe('CommunityTaxCertificateFormComponent', () => {
  let component: CommunityTaxCertificateFormComponent;
  let fixture: ComponentFixture<CommunityTaxCertificateFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommunityTaxCertificateFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunityTaxCertificateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
