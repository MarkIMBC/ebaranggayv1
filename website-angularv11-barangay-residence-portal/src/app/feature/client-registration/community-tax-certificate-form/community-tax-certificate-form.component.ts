import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import { IControlModelArg } from 'src/shared/APP_HELPER';
import { CurrentObjectOnValueChangeArg } from '../../base-detail-view/base-detail-view.component';

@Component({
  selector: 'community-tax-certificate-form',
  templateUrl: './community-tax-certificate-form.component.html',
  styleUrls: ['./community-tax-certificate-form.component.less'],
})
export class CommunityTaxCertificateFormComponent implements OnInit {
  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;
  @Input() Record: any = {};

  constructor() {}

  ngOnInit(): void {}

  control_onModelChanged(e: IControlModelArg) {
    this.Record[e.name] = e.value;

    if (e.displayName != undefined) {
      this.Record[e.displayName] = e.displayValue;
    }

    var arg: CurrentObjectOnValueChangeArg = {
      name: e.name,
      value: e.value,
    };
  }

  hasValues(): boolean {

    let size = 0;
    for (let k in this.Record) {
      size++;
    }
    return size > 0;
  }

  resetFields() {
    this.Record = {};
  }
}
