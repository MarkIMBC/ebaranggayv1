import { Component, OnInit } from '@angular/core';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { ToastService } from 'src/app/shared/toast.service';
import { FilingStatusEnum } from 'src/shared/APP_HELPER';

@Component({
  selector: 'announcement-display',
  templateUrl: './announcement-display.component.html',
  styleUrls: ['./announcement-display.component.less'],
})
export class AnnouncementDisplayComponent implements OnInit {
  currentUser: TokenSessionFields = new TokenSessionFields();
  dataSource: any[] = [];
  loading: boolean = false;

  DisplayRecordCount: number = 0;
  MilliSecondsDelay: number = 0;
  PageNumber: number = 1;
  TotalPageCount: number = 1;
  PageSize: number = 3;

  constructor(
    protected ds: DataService,
    public toastService: ToastService,
    private cs: CrypterService,
    private userAuth: UserAuthenticationService
  ) {}

  ngOnInit(): void {
    this.currentUser = this.userAuth.getDecodedToken();

    this.loadRecord();

    setInterval(() => {
      if (this.PageNumber >= this.TotalPageCount) {
        this.PageNumber = 1;
      } else {
        this.PageNumber++;
      }

      this.loadRecord();
    }, 5000);
  }

  loadRecord() {
    this.loading = true;

    this.ds
      .execSP(
        'pGetLatestAnnouncement',
        {
          ID_UserSession: this.currentUser.ID_UserSession,
          PageNumber: this.PageNumber,
          PageSize: this.PageSize,
        },
        { isReturnObject: true }
      )
      .then(async (obj) => {
        this.PageNumber = obj.CurrentPageNumber;
        this.TotalPageCount = obj.TotalPageCount;
        this.MilliSecondsDelay = obj.MilliSecondsDelay;

        if (obj.Records) {
          this.dataSource = obj.Records;
        }
        this.loading = false;
      })
      .catch(() => {
        this.loading = false;
      });
  }
}
