import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnouncementDisplayComponent } from './announcement-display.component';

describe('AnnoucementDisplayComponent', () => {
  let component: AnnouncementDisplayComponent;
  let fixture: ComponentFixture<AnnouncementDisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnnouncementDisplayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnouncementDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
