import { Component } from '@angular/core';
import { DataService } from './core/data.service';
import { TokenSessionFields, UserAuthenticationService } from './core/UserAuthentication.service';
import { ToastService } from './shared/toast.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {

  title = 'vetcloud-angularv11';
  hasLogin: boolean = false;

  currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(private userAuth: UserAuthenticationService, private ds: DataService, private toastService: ToastService) { }

  async ngOnInit(): Promise<void> {

    await this.ds.loadConfig();

    this.currentUser = this.userAuth.getDecodedToken();

    this.hasLogin = !(this.currentUser.ID_User == undefined || this.currentUser.ID_User == null);
  }
}
