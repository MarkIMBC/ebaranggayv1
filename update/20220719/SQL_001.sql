IF Object_Id('dbo.tResident_Business') IS NULL
  BEGIN
      EXEC _pCreateappModuleWithTable
        'tResident_Business',
        1,
        0,
        NULL

      EXEC _PaddModelProperty
        'tResident_Business',
        'Resident_BusinessOwner',
        1

      EXEC _PaddModelProperty
        'tResident_Business',
        'Address',
        1

      EXEC _PaddModelProperty
        'tResident_Business',
        'DateValid',
        5
  END

GO
IF COL_LENGTH('dbo.tResident_Business', 'RequestedBy') IS NULL
BEGIN
     EXEC _PaddModelProperty
        'tResident_Business',
        'RequestedBy',
        1
END

GO

exec _pRefreshAllViews
go

CREATE OR
ALTER VIEW vResident_Business
AS
  SELECT H.*,
         UC.Name AS CreatedBy,
         UM.Name AS LastModifiedBy
  FROM   tResident_Business H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID

GO

CREATE  OR
ALTER VIEW vResident_Business_ListvIew
AS
  SELECT ID,
         Name,
         IsActive,
         ID_Company,
         Resident_BusinessOwner Name_Owner,
         RequestedBy,
         Address,
         CreatedBy,
         LastModifiedBy
  FROM   vResident_Business
  WHERE  IsActive = 1

GO

CREATE OR
ALTER PROCEDURE pGetResident_Business @ID         INT = -1,
                                      @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_'

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL AS [_],
                           -1   AS [ID],
                           NULL AS [Code],
                           NULL AS [Name],
                           1    AS [IsActive],
                           NULL AS [ID_Company],
                           NULL AS [Comment],
                           NULL AS [DateCreated],
                           NULL AS [DateModified],
                           NULL AS [ID_CreatedBy],
                           NULL AS [ID_LastModifiedBy]) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vResident_Business H
            WHERE  H.ID = @ID
        END
  END

GO

CREATE OR
ALTER VIEW [dbo].vzResidentBusinessReport
AS
  SELECT Resident.ID                                             ID,
         company.ID                                              ID_Company,
         company.ImageLogoLocationFilenamePath                   ImageLogoLocationFilenamePath,
         company.Name                                            Name_Company,
         company.Address                                         Address_Company,
         company.ContactNumber                                   ContactNumber_Company,
         company.BarangayListSideReportHTMLString                BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                             BarangayCaptainName_Company,
         company.BarangayCaptain_Name_Employee                   BarangayCaptain_Name_Employee,
         company.BarangayCaptain_ImageSignitureSpecimen_Employee BarangayCaptain_ImageSignitureSpecimen_Employee,
         municipality.Name                                       Name_MunicipalityCompany,
         municipality.ImageLogoLocationFilenamePath              ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                                    Address_MunicipalityCompany,
         ISNULL(Resident.Name, '')                               Name_Business,
         ISNULL(Resident.Resident_BusinessOwner, '')             Name_Owner,
         Resident.DateValid,
         ISNULL(Resident.RequestedBy, '')                        RequestedBy
  FROM   vResident_Business Resident
         INNER JOIN vCompanyActive company
                 ON company.ID = Resident.ID_Company
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = Resident.ID_Company

GO

IF NOT EXISTS (SELECT 1
               FROM   _tReport
               WHERE  Name = 'ResidentBusinessReport')
  BEGIN
      exec _pCreateReportView
        'ResidentBusinessReport',
        1
  END

exec _pRefreshAllViews 
