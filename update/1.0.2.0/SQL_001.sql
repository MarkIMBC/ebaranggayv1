IF Object_Id('dbo.tResident_Family') IS NULL
  BEGIN
      EXEC _pCreateappModuleWithTable
        'tResident_Family',
        1,
        0,
        NULL

	  EXEC _PaddModelProperty
        'tResident_Family',
        'ID_Resident',
        2

      EXEC _PaddModelProperty
        'tResident_Family',
        'ID_FamilyMember',
        2

      EXEC _PaddModelProperty
        'tResident_Family',
        'ID_RelationshipType',
        2

    

      EXEC _PaddModelDetail
        'tResident_Family',
        'ID_Resident',
        'tResident',
        'tResident'
  END

GO

IF Object_Id('dbo.tFamilyRelationshipType') IS NULL
  BEGIN
      EXEC _pCreateappModuleWithTable
        'tFamilyRelationshipType',
        1,
        0,
        NULL

      INSERT INTO [dbo].[tFamilyRelationshipType]
                  ([Name],
                   [IsActive],
                   [ID_Company],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy])
      VALUES      ('Ama',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1),
                  ('Ina',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1),
                  ('Anak',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1),
                  ('Kapatid',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1),
                  ('Lola',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1),
                  ('Lolo',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1)
  END

GO

IF col_Length('dbo.tResident', 'IsPregnant') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tResident',
        'IsPregnant',
        4

      EXEC _pRefreshAllViews
  END

GO

IF col_Length('dbo.tResident', 'IsHeadFamily') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tResident',
        'IsHeadofFamily',
        4

      EXEC _pRefreshAllViews
  END

GO

Update tGender
SET    Name = 'Lalake'
WHERE  ID = 1

Update tGender
SET    Name = 'Babae'
WHERE  ID = 2

GO

CREATE OR
ALTER VIEW vResident_Listview
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid       Guid_Company
  FROM   vResident hed
        
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company

GO

CREATE OR
ALTER VIEW vResident_ProgramListview
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid       Guid_Company,
         program.ID_Program ID_Program
  FROM   vResident hed
         INNER JOIN vResident_Program program
                 on hed.ID = program.ID_Resident
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company

GO


exec _pRefreshAllViews

GO

CREATE OR ALTER VIEW [dbo].[vzResidentDetailReport]
AS
  SELECT hed.*,
		company.ImageLogoLocationFilenamePath,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END HeaderInfo_Company
  FROM   vResident hed
         LEFT JOIN dbo.vCompany company
                ON company.ID = hed.ID_Company
  WHERE  hed.IsActive = 1

GO

CREATE OR
ALTER VIEW [dbo].[vzResidentProgramReport]
AS
  SELECT hed.*,
         company.ImageLogoLocationFilenamePath,
         program.Name_Program,
         program.ID_Program,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END HeaderInfo_Company
  FROM   vResident hed
         LEFT JOIN dbo.vCompany company
                ON company.ID = hed.ID_Company
         INNER JOIN dbo.vResident_Program program
                 ON hed.ID = program.ID_Resident
  WHERE  hed.IsActive = 1

GO

CREATE OR
ALTER VIEW vResident_Family
AS
  SELECT H.*,
         rel.Name AS Name_Relationship,
		 res.Name AS Name_FamilyMember,
         UC.Name  AS CreatedBy,
         UM.Name  AS LastModifiedBy
  FROM   tResident_Family H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tFamilyRelationshipType rel
                ON rel.ID = H.ID_RelationshipType
		 LEFT JOIN tResident res
				ON res.ID=H.ID_FamilyMember

GO

ALTER PROCEDURE pGetResident @ID         INT = -1,
                             @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS Resident_Program,
             '' AS Resident_Family,
             '' AS Reports

      DECLARE @ID_User INT
      DECLARE @ID_Company INT
      DECLARE @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse,
             @ID_Company = ID_Company
      FROM   tUserSession _session
             INNER JOIN vUSer _user
                     on _session.ID_User = _user.ID
      WHERE  _session.ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL AS [_],
                           -1   AS [ID],
                           NULL AS [Code],
                           NULL AS [Name],
                           1    AS [IsActive],
                           NULL AS [ID_Company],
                           NULL AS [Comment],
                           NULL AS [DateCreated],
                           NULL AS [DateModified],
                           NULL AS [ID_CreatedBy],
                           NULL AS [ID_LastModifiedBy]) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tCompany company
                          ON company.ID = h.ID_Company
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vResident H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   vResident_Program
      WHERE  ID_Resident = @ID

      SELECT *
      FROM   vResident_Family
      WHERE  ID_Resident = @ID

      SELECT *
      FROm   vResidentDetailReportList
      order  by label
  END

GO
exec _pRefreshAllViews
Go


IF NOT EXISTS (SELECT 1
               FROM   _tReport
               WHERE  Name = 'ResidentDetailReport')
  BEGIN
      exec _pCreateReportView
        'ResidentDetailReport',
        1
  END 

IF NOT EXISTS (SELECT 1
               FROM   _tReport
               WHERE  Name = 'ResidentProgramReport')
  BEGIN
      exec _pCreateReportView
        'ResidentProgramReport',
        1
  END 
GO