ALTER VIEW villingInvoiceWalkInList
AS
  SELECT H.*,
         UC.Name AS CreatedBy,
         UM.Name AS LastModifiedBy
  FROM   tBillingInvoiceWalkInList H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID

GO

IF Object_Id('dbo.tResidentRequest') IS NULL
  BEGIN
      EXEC _pCreateappModuleWithTable
        'tLawSuit',
        1,
        0,
        NULL

      EXEC _pCreateappModuleWithTable
        'tDispute',
        1,
        0,
        NULL

      EXEC _pCreateappModuleWithTable
        'tResidentRequest',
        1,
        0,
        NULL

      EXEC _pCreateappModuleWithTable
        'tResidentRequest_Certificate',
        1,
        0,
        NULL

      EXEC _pCreateappModuleWithTable
        'tResidentRequest_BarangayBusinessClearance',
        1,
        0,
        NULL

      EXEC _pCreateappModuleWithTable
        'tResidentRequest_CommunityTaxCertificate',
        1,
        0,
        NULL

      EXEC _pCreateappModuleWithTable
        'tResidentRequest_IndigencyCertificate',
        1,
        0,
        NULL

      EXEC _PaddModelProperty
        'tResidentRequest_Certificate',
        'ID_ResidentRequest',
        2

      EXEC _PaddModelProperty
        'tResidentRequest_BarangayBusinessClearance',
        'ID_ResidentRequest',
        2

      EXEC _PaddModelProperty
        'tResidentRequest_CommunityTaxCertificate',
        'ID_ResidentRequest',
        2

      EXEC _PaddModelProperty
        'tResidentRequest_IndigencyCertificate',
        'ID_ResidentRequest',
        2

      EXEC _PaddModelDetail
        'tResidentRequest_Certificate',
        'ID_ResidentRequest',
        'tResidentRequest',
        'tResidentRequest'

      EXEC _PaddModelDetail
        'tResidentRequest_BarangayBusinessClearance',
        'ID_ResidentRequest',
        'tResidentRequest',
        'tResidentRequest'

      EXEC _PaddModelDetail
        'tResidentRequest_CommunityTaxCertificate',
        'ID_ResidentRequest',
        'tResidentRequest',
        'tResidentRequest'

      EXEC _PaddModelDetail
        'tResidentRequest_IndigencyCertificate',
        'ID_ResidentRequest',
        'tResidentRequest',
        'tResidentRequest'

      EXEC _pRefreshAllViews

      EXEC _PaddModelProperty
        'tResidentRequest',
        'ContactNumber',
        1

      EXEC _PaddModelProperty
        'tResidentRequest',
        'Address',
        1

      EXEC _PaddModelProperty
        'tResidentRequest',
        'Purpose',
        1

      EXEC _PaddModelProperty
        'tResidentRequest_Certificate',
        'DateBirth',
        5

      EXEC _PaddModelProperty
        'tResidentRequest_Certificate',
        'ID_Gender',
        2

      EXEC _PaddModelProperty
        'tResidentRequest_BarangayBusinessClearance',
        'BusinessName',
        1

      EXEC _PaddModelProperty
        'tResidentRequest_BarangayBusinessClearance',
        'RegistrationNumber',
        1

      EXEC _PaddModelProperty
        'tResidentRequest_BarangayBusinessClearance',
        'ImageCertificateofRegistration',
        1

      EXEC _PaddModelProperty
        'tResidentRequest_BarangayBusinessClearance',
        'Address',
        1

      EXEC _PaddModelProperty
        'tResidentRequest_BarangayBusinessClearance',
        'DateValidity',
        5

      EXEC _PaddModelProperty
        'tResidentRequest_CommunityTaxCertificate',
        'CompanyName',
        1

      EXEC _PaddModelProperty
        'tResidentRequest_CommunityTaxCertificate',
        'PositionName',
        1

      EXEC _PaddModelProperty
        'tResidentRequest_CommunityTaxCertificate',
        'AnnualIncomeAmount',
        3

      EXEC _PaddModelProperty
        'tResidentRequest_IndigencyCertificate',
        'ID_Gender',
        2

      EXEC _pRefreshAllViews
  END

GO

IF Object_Id('dbo.tResident') IS NULL
  BEGIN
      EXEC _pCreateappModuleWithTable
        'tResident',
        1,
        0,
        NULL
  END

GO

IF col_Length('dbo.tResident', 'ID_Gender') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tResident',
        'ID_Gender',
        2

      EXEC _pRefreshAllViews
  END

GO

IF col_Length('dbo.tResident', 'ContactNumber') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tResident',
        'ContactNumber',
        1

      EXEC _pRefreshAllViews
  END

GO

IF col_Length('dbo.tResident', 'Address') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tResident',
        'Address',
        1

      EXEC _pRefreshAllViews
  END

GO

IF col_Length('dbo.tLawSuit', 'Date') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tLawSuit',
        'Date',
        5

      EXEC _pRefreshAllViews
  END

GO

IF col_Length('dbo.tLawSuit', 'ComplainantName') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tLawSuit',
        'ComplainantName',
        1

      EXEC _pRefreshAllViews
  END

GO

IF col_Length('dbo.tLawSuit', 'ComplainantName') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tLawSuit',
        'ComplainantSMSNumber',
        1

      EXEC _pRefreshAllViews
  END

GO

IF col_Length('dbo.tLawSuit', 'RespondentName') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tLawSuit',
        'RespondentName',
        1

      EXEC _pRefreshAllViews
  END

GO

IF col_Length('dbo.tLawSuit', 'RespondentName') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tLawSuit',
        'RespondentSMSNumber',
        1

      EXEC _pRefreshAllViews
  END

GO

IF Object_Id('dbo.tProgram') IS NULL
  BEGIN
      EXEC _pCreateappModuleWithTable
        'tProgram',
        1,
        0,
        NULL

      DECLARE @Program TABLE
        (
           Name VARCHAR(MAX)
        )

      INSERT @Program
      VALUES ('4Ps'),
             ('Scholarship'),
             ('Sernior Citizen')

      INSERT INTO [dbo].[tProgram]
                  ([Name],
                   [IsActive],
                   [ID_Company],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy])
      SELECT Name,
             1,
             1,
             GetDate(),
             GetDate(),
             1,
             1
      FROM   @Program
  END

GO

IF Object_Id('dbo.tResident_Program') IS NULL
  BEGIN
      EXEC _pCreateappModuleWithTable
        'tResident_Program',
        1,
        0,
        NULL

      EXEC _PaddModelProperty
        'tResident_Program',
        'ID_Resident',
        2

      EXEC _PaddModelDetail
        'tResident_Program',
        'ID_Resident',
        'tResident',
        'tResident'

      EXEC _pRefreshAllViews
  END

GO

IF col_Length('dbo.tResident_Program', 'ID_Program') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tResident_Program',
        'ID_Program',
        2

      EXEC _pRefreshAllViews
  END

GO

CREATE OR
ALTER VIEW vResidentRequest_ListvIew
AS
  SELECT ID,
         Name,
         ContactNumber,
         Address,
         Purpose,
         ID_Company,
         IsActive,
         DateCreated,
         DateModified
  FROM   vResidentRequest

GO

CREATE OR
ALTER VIEW vLawSuit_ListvIew
AS
  SELECT hed.ID,
         ComplainantName,
         RespondentName,
         Date,
         RespondentSMSNumber,
         ComplainantSMSNumber,
         hed.Comment,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified
  FROM   vLawSuit hed

GO

CREATE OR
ALTER VIEW vDispute_ListvIew
AS
  SELECT hed.ID,
         hed.Name,
         hed.Comment,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified
  FROM   vDispute hed

GO

ALTER VIEW vResident_Program
AS
  SELECT H.*,
         UC.Name   AS CreatedBy,
         UM.Name   AS LastModifiedBy,
         prog.Name Name_Program
  FROM   tResident_Program H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tProgram prog
                ON prog.ID = h.ID_Program

GO

CREATE OR
ALTER VIEW vResidentRequest_Certificate
AS
  SELECT H.*,
         ResidentRequest.Name          Name_ResidentRequest,
         ResidentRequest.ContactNumber ContactNumber_ResidentRequest,
         gender.NAME                   Name_Gender,
         UC.NAME                       AS CreatedBy,
         UM.NAME                       AS LastModifiedBy
  FROM   tResidentRequest_Certificate H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tGender gender
                ON gender.ID = h.ID_Gender
         LEFT JOIN tResidentRequest ResidentRequest
                ON ResidentRequest.ID = h.ID_ResidentRequest

GO

CREATE OR
ALTER VIEW vResidentRequest_IndigencyCertificate
AS
  SELECT H.*,
         ResidentRequest.Name          Name_ResidentRequest,
         ResidentRequest.ContactNumber ContactNumber_ResidentRequest,
         gender.NAME                   Name_Gender,
         UC.NAME                       AS CreatedBy,
         UM.NAME                       AS LastModifiedBy
  FROM   tResidentRequest_IndigencyCertificate H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tGender gender
                ON gender.ID = h.ID_Gender
         LEFT JOIN tResidentRequest ResidentRequest
                ON ResidentRequest.ID = h.ID_ResidentRequest

GO

CREATE OR
ALTER VIEW vResidentRequest_IndigencyCertificate_ListvIew
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.ID_Company,
         hed.IsActive,
         hed.ID_ResidentRequest,
         hed.Name_ResidentRequest,
         hed.DateCreated,
         hed.DateModified
  FROM   vResidentRequest_IndigencyCertificate hed

GO

CREATE OR
ALTER VIEW vResidentRequest_Certificate_ListvIew
AS
  SELECT ID,
         Name,
         DateBirth,
         ID_Gender,
         ID_Company,
         IsActive,
         ID_ResidentRequest,
         Name_ResidentRequest,
         DateCreated,
         DateModified
  FROM   vResidentRequest_Certificate

GO

CREATE OR
ALTER VIEW vResidentRequest_BarangayBusinessClearance
AS
  SELECT H.*,
         ResidentRequest.Name          Name_ResidentRequest,
         ResidentRequest.ContactNumber ContactNumber_ResidentRequest,
         UC.NAME                       AS CreatedBy,
         UM.NAME                       AS LastModifiedBy
  FROM   tResidentRequest_BarangayBusinessClearance H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tResidentRequest ResidentRequest
                ON ResidentRequest.ID = h.ID_ResidentRequest

GO

CREATE OR
ALTER VIEW vResidentRequest_BarangayBusinessClearance_ListvIew
AS
  SELECT ID,
         Name,
         RegistrationNumber,
         ImageCertificateofRegistration,
         ID_ResidentRequest,
         Name_ResidentRequest,
         ID_Company,
         IsActive,
         DateCreated,
         DateModified
  FROM   vResidentRequest_BarangayBusinessClearance

GO

CREATE OR
ALTER VIEW vResidentRequest_CommunityTaxCertificate
AS
  SELECT H.*,
         ResidentRequest.Name Name_ResidentRequest,
         UC.NAME              AS CreatedBy,
         UM.NAME              AS LastModifiedBy
  FROM   tResidentRequest_CommunityTaxCertificate H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tResidentRequest ResidentRequest
                ON ResidentRequest.ID = h.ID_ResidentRequest

GO

CREATE OR
ALTER VIEW vResidentRequest_CommunityTaxCertificate_ListvIew
AS
  SELECT ID,
         Name,
         CompanyName,
         PositionName,
         AnnualIncomeAmount,
         ID_ResidentRequest,
         Name_ResidentRequest,
         ID_Company,
         IsActive,
         DateCreated,
         DateModified
  FROM   vResidentRequest_CommunityTaxCertificate

GO

CREATE OR
ALTER VIEW vResidentRequestTransaction
AS
  SELECT hed.DateCreated,
         hed.ID_Company,
         hed.ID_ResidentRequest,
         hed.ID,
         hed.Name,
         _model.Oid     Oid_Model,
         _model.Caption Caption_Model
  FROM   vResidentRequest_Certificate_ListvIew hed,
         _tModel _model
  WHERE  _model.TableName = 'tResidentRequest_Certificate'
  UNION ALL
  SELECT hed.DateCreated,
         hed.ID_Company,
         hed.ID_ResidentRequest,
         hed.ID,
         hed.Name,
         _model.Oid     Oid_Model,
         _model.Caption Caption_Model
  FROM   vResidentRequest_CommunityTaxCertificate_ListvIew hed,
         _tModel _model
  WHERE  _model.TableName = 'tResidentRequest_CommunityTaxCertificate'
  UNION ALL
  SELECT hed.DateCreated,
         hed.ID_Company,
         hed.ID_ResidentRequest,
         hed.ID,
         hed.Name,
         _model.Oid     Oid_Model,
         _model.Caption Caption_Model
  FROM   vResidentRequest_BarangayBusinessClearance_ListvIew hed,
         _tModel _model
  WHERE  _model.TableName = 'tResidentRequest_BarangayBusinessClearance'
  UNION ALL
  SELECT hed.DateCreated,
         hed.ID_Company,
         hed.ID_ResidentRequest,
         hed.ID,
         hed.Name,
         _model.Oid     Oid_Model,
         _model.Caption Caption_Model
  FROM   vResidentRequest_IndigencyCertificate_ListvIew hed,
         _tModel _model
  WHERE  _model.TableName = 'tResidentRequest_IndigencyCertificate'

GO

CREATE OR
ALTER VIEW vResidentRequestTransaction_ListvIew
AS
  SELECT hed.*,
         reRed.Name    Name_ResidentRequest,
         reRed.Address Address_ResidentRequest,
         reRed.Purpose Purpose_ResidentRequest
  FROM   vResidentRequestTransaction hed
         INNER JOIN vResidentRequest reRed
                 ON hed.ID_ResidentRequest = reRed.ID

GO

CREATE OR
ALTER VIEW [dbo].vzResidentRequestBarangayBusinessClearance
AS
  SELECT bz.Name               NAME,
         bz.ID                 ID,
         bz.Code,
         bz.BusinessName,
         bz.Address,
         bz.RegistrationNumber,
         bz.Name_ResidentRequest,
         bz.DateValidity,
         company.ID            ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.Name          Name_Company,
         company.Address       Address_Company,
         company.ContactNumber ContactNumber_Company
  FROM   vResidentRequest_BarangayBusinessClearance bz
         INNER JOIN vCompany company
                 ON company.ID = bz.ID_Company

GO

CREATE  OR
ALTER VIEW [dbo].vzResidentRequestCertificate
AS
  SELECT Resident.Name         NAME,
         Resident.ID           ID,
         company.ID            ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.Name          Name_Company,
         company.Address       Address_Company,
         company.ContactNumber ContactNumber_Company
  FROM   vResidentRequest_Certificate Resident
         INNER JOIN vCompany company
                 ON company.ID = Resident.ID_Company

GO

CREATE  OR
ALTER VIEW [dbo].vzResidentRequestCertificateIndigency
AS
  SELECT Resident.Name         NAME,
         Resident.ID           ID,
         company.ID            ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.Name          Name_Company,
         company.Address       Address_Company,
         company.ContactNumber ContactNumber_Company
  FROM   vResidentRequest_IndigencyCertificate Resident
         INNER JOIN vCompany company
                 ON company.ID = Resident.ID_Company

GO

CREATE  OR
ALTER VIEW [dbo].vzBarangayComplaintReport
AS
  SELECT lawSuit.ID,
         lawSuit.ComplainantName,
         lawSuit.RespondentName,
         lawSuit.Date,
         lawSuit.RespondentSMSNumber,
         lawSuit.ComplainantSMSNumber,
         lawSuit.Comment,
         company.ID            ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.Name          Name_Company,
         company.Address       Address_Company,
         company.ContactNumber ContactNumber_Company
  FROM   vLawSuit lawSuit
         INNER JOIN vCompany company
                 ON company.ID = lawSuit.ID_Company

GO

CREATE OR
ALTER VIEW vResident
AS
  SELECT H.*,
         UC.Name     AS CreatedBy,
         UM.Name     AS LastModifiedBy,
         gender.Name Name_Gender
  FROM   tResident H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tGender gender
                ON gender.ID = h.ID_Gender

GO

CREATE OR
ALTER VIEW vResident_ListvIew
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified
  FROM   vResident hed

GO

CREATE OR
ALTER PROCEDURE pGetResidentRequest @ID         INT = -1,
                                    @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_'

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL AS [_],
                           -1   AS [ID],
                           NULL AS [Code],
                           NULL AS [Name],
                           1    AS [IsActive],
                           NULL AS [ID_Company],
                           NULL AS [Comment],
                           NULL AS [DateCreated],
                           NULL AS [DateModified],
                           NULL AS [ID_CreatedBy],
                           NULL AS [ID_LastModifiedBy]) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vResidentRequest H
            WHERE  H.ID = @ID
        END
  END

GO

CREATE OR
ALTER PROCEDURE pGetResidentRequest_Certificate @ID                 INT = -1,
                                                @ID_ResidentRequest INT = NULL,
                                                @ID_Session         INT = NULL
AS
  BEGIN
      SELECT '_'

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL                AS [_],
                           -1                  AS [ID],
                           NULL                AS [Code],
                           NULL                AS [Name],
                           1                   AS [IsActive],
                           NULL                AS [ID_Company],
                           NULL                AS [Comment],
                           NULL                AS [DateCreated],
                           NULL                AS [DateModified],
                           NULL                AS [ID_CreatedBy],
                           NULL                AS [ID_LastModifiedBy],
                           @ID_ResidentRequest AS ID_ResidentRequest) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tResidentRequest ResidentRequest
                          ON ResidentRequest.ID = h.ID_ResidentRequest
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vResidentRequest_Certificate H
            WHERE  H.ID = @ID
        END
  END

GO

CREATE OR
ALTER PROCEDURE pGetResident @ID         INT = -1,
                             @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS Resident_Program

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL AS [_],
                           -1   AS [ID],
                           NULL AS [Code],
                           NULL AS [Name],
                           1    AS [IsActive],
                           NULL AS [ID_Company],
                           NULL AS [Comment],
                           NULL AS [DateCreated],
                           NULL AS [DateModified],
                           NULL AS [ID_CreatedBy],
                           NULL AS [ID_LastModifiedBy]) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vResident H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   vResident_Program
      WHERE  ID_Resident = @ID
  END

GO

ALTER PROCEDURE pGetLawsuit @ID         INT = -1,
                            @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_'

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL      AS [_],
                           -1        AS [ID],
                           NULL      AS [Code],
                           NULL      AS [Name],
                           GetDate() AS [Date],
                           1         AS [IsActive],
                           NULL      AS [ID_Company],
                           NULL      AS [Comment],
                           NULL      AS [DateCreated],
                           NULL      AS [DateModified],
                           NULL      AS [ID_CreatedBy],
                           NULL      AS [ID_LastModifiedBy]) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vLawSuit H
            WHERE  H.ID = @ID
        END
  END

GO

IF(SELECT Count(*)
   FROM   _tReport
   WHERE  Name = 'ResidentRequestCertificate') = 0
  BEGIN
      EXEC _pCreateReportView
        'ResidentRequestCertificate',
        1
  END

go

IF(SELECT Count(*)
   FROM   _tReport
   WHERE  Name = 'ResidentRequestBarangayBusinessClearance') = 0
  BEGIN
      EXEC _pCreateReportView
        'ResidentRequestBarangayBusinessClearance',
        1
  END

go

IF(SELECT Count(*)
   FROM   _tReport
   WHERE  Name = 'ResidentRequestCertificateIndigency') = 0
  BEGIN
      EXEC _pCreateReportView
        'ResidentRequestCertificateIndigency',
        1
  END

go

IF(SELECT Count(*)
   FROM   _tReport
   WHERE  Name = 'BarangayComplaintReport') = 0
  BEGIN
      EXEC _pCreateReportView
        'BarangayComplaintReport',
        1
  END

go

UPDATE _tModel
SET    Caption = 'Residence Certificate'
WHERE  TableName = 'tResidentRequest_Certificate'

UPDATE _tModel
SET    Caption = 'Community Tax Certificate'
WHERE  TableName = 'tResidentRequest_CommunityTaxCertificate'

UPDATE _tModel
SET    Caption = 'Barangay Business Clearance'
WHERE  TableName = 'tResidentRequest_BarangayBusinessClearance'

UPDATE _tModel
SET    Caption = 'Indigency Certificate'
WHERE  TableName = 'tResidentRequest_IndigencyCertificate'

GO 
