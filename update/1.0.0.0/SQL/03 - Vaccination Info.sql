IF COL_LENGTH('tResident', 'IsVaccinated') IS NULL
  BEGIN
      Exec _pAddModelProperty
        'tResident',
        'IsVaccinated',
        4

      exec _pRefreshAllViews
  END

GO

IF COL_LENGTH('tResident', 'IsVaccinated') IS NULL
  BEGIN
      Exec _pAddModelProperty
        'tResident',
        'DateLastVaccination',
        5

      exec _pRefreshAllViews
  END

GO

ALTER VIEW vResident_ListvIew
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated
  FROM   vResident hed

GO

SELECT 0       ID,
       'Lahat' Name
UNION ALL
SELECT 1             ID,
       'Nabakunahan' Name
UNION ALL
SELECT 2                      ID,
       'Hindi pa nabakunahan' Name 
