IF Object_Id('dbo.tSMSLogs') IS NULL
  BEGIN
      EXEC _pCreateappModuleWithTable
        'tSMSLogs',
        1,
        0,
        NULL
  END

GO


IF col_Length('dbo.tSMSLogs', 'ID_CurrentObject') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tSMSLogs',
        'ID_CurrentObject',
        1

      EXEC _pRefreshAllViews
  END

GO

IF col_Length('dbo.tSMSLogs', 'SMSMessage') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tSMSLogs',
        'SMSMessage',
        1

      EXEC _pRefreshAllViews
  END

GO






