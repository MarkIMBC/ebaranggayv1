IF OBJECT_ID('dbo.tAnnouncement', N'U') IS NULL
  BEGIN
      exec _pCreateAppModuleWithTable
        'tAnnouncement',
        1,
        NULL,
        NULL

      ALTER TABLE tAnnouncement
        ALTER COLUMN Name VARCHAR(8000);

      exec _pRefreshAllViews
  END

GO

IF COL_LENGTH('tAnnouncement', 'DateStart') IS NULL
  BEGIN
      Exec _pAddModelProperty
        'tAnnouncement',
        'DateStart',
        5

      exec _pRefreshAllViews
  END

GO

IF COL_LENGTH('tAnnouncement', 'DateEnd') IS NULL
  BEGIN
      Exec _pAddModelProperty
        'tAnnouncement',
        'DateEnd',
        5

      exec _pRefreshAllViews
  END

GO

CREATE   OR
ALTER VIEW vAnnouncement
AS
  SELECT H.*,
         UC.NAME AS CreatedBy,
         UM.NAME AS LastModifiedBy
  FROM   tAnnouncement H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID

GO

CREATE     OR
ALTER VIEW vAnnouncement_ListvIew
AS
  SELECT ID,
         Name,
         DateStart,
         DateEnd,
         ID_Company,
         IsActive,
         DateCreated,
         DateModified
  FROM   vAnnouncement
  WHERE  IsActive = 1

GO

CREATE OR
ALTER FUNCTION [dbo].GetDateFormatSince(@TempDate DATETIME)
RETURNS NVARCHAR(200)
AS
  BEGIN
      DECLARE @TimeFormat NVARCHAR(200)
      DECLARE @second INT
      DECLARE @Minutes INT
      DECLARE @Hours INT
      DECLARE @Days INT
      DECLARE @Months INT
      DECLARE @Years INT
      DECLARE @Date DATETIME = GETDATE()

      SET @Years = DATEDIFF(yy, @TempDate, @Date)
      SET @Months = DATEDIFF(mm, @TempDate, @Date)
      SET @Days = DATEDIFF(dd, @TempDate, @Date)
      SET @Hours = DATEDIFF(hh, @TempDate, @Date)
      SET @Minutes = DATEDIFF(mi, @TempDate, @Date)
      SET @Minutes = @Minutes - ( @Hours * 60 )
      SET @second = DATEDIFF(ss, @TempDate, @Date)
      SET @second = @second - ( @Minutes * 60 )

      IF @Years = 0
         AND @Months = 0
         AND @Days = 0
         AND @Hours = 0
         AND @Minutes = 0
         AND @second >= 0
        BEGIN
            SET @TimeFormat = 'a second ago'
        END
      else IF @Years = 0
         AND @Months = 0
         AND @Days = 0
         AND @Hours = 0
         AND @Minutes = 1
        BEGIN
            SET @TimeFormat = 'a minute ago'
        END
      else IF @Years = 0
         AND @Months = 0
         AND @Days = 0
         AND @Hours = 0
         AND @Minutes > 0
        BEGIN
            SET @TimeFormat = CONVERT(VARCHAR, @Minutes) + ' minutes ago'
        END
      ELSE IF @Years = 0
         AND @Months = 0
         AND @Days = 0
         AND @Hours = 1
        BEGIN
            SET @TimeFormat = 'a hour ago'
        END
      ELSE IF @Years = 0
         AND @Months = 0
         AND @Days = 0
         AND @Hours > 1
        BEGIN
            SET @TimeFormat = CONVERT(VARCHAR, @Hours) + ' hours ago'
        END
      ELSE IF @Years = 0
         AND @Months = 0
         AND @Days = 1
        BEGIN
            SET @TimeFormat = 'a day ago'
        END
      ELSE IF @Years = 0
         AND @Months = 0
         AND @Days > 1
        BEGIN
            SET @TimeFormat = CONVERT(VARCHAR, @Days) + ' days ago'
        END
      ELSE IF @Months = 1
        BEGIN
            SET @TimeFormat = 'a month ago'
        END
      ELSE IF @Months BETWEEN 2 and 11
        BEGIN
            SET @TimeFormat = CONVERT(VARCHAR, @Months) + ' months ago'
        END
      ELSE IF @Months = 12
        BEGIN
            SET @TimeFormat = 'a year ago'
        END
      ELSE IF @Months > 12
        BEGIN
            SET @TimeFormat = CONVERT(VARCHAR, @Years) + ' year ago'
        END

      RETURN @TimeFormat;
  END

GO

CREATE OR
ALTER PROCEDURE pGetAnnouncement @ID         INT = -1,
                                 @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_'

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL                                                        AS [_],
                           -1                                                          AS [ID],
                           NULL                                                        AS [Code],
                           NULL                                                        AS [Name],
                           1                                                           AS [IsActive],
                           NULL                                                        AS [ID_Company],
                           NULL                                                        AS [Comment],
                           NULL                                                        AS [DateCreated],
                           NULL                                                        AS [DateModified],
                           NULL                                                        AS [ID_CreatedBy],
                           NULL                                                        AS [ID_LastModifiedBy],
                           CONVERT(DateTIME, FORMAT(GETDATE(), 'yyyy-MM-dd HH:mm:00')) AS [DateStart],
                           CONVERT(DateTIME, FORMAT(GETDATE(), 'yyyy-MM-dd HH:00:00')) AS [DateEnd]) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vAnnouncement H
            WHERE  H.ID = @ID
        END
  END

GO

CREATE OR
ALTER PROC pGetLatestAnnouncement(@ID_UserSession INT,
                                  @PageNumber     INT = 1,
                                  @PageSize       INT = 100)
AS
  BEGIN
      DECLARE @ID_User      INT,
              @ID_Warehouse INT
      DECLARE @ID_Company INT
      DEClare @Success BIT = 1
      DECLARE @RecordCount INT = 0
      DECLARE @DisplayCount INT = 0
      DECLARE @TotalPageCount INT = 0

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_UserSession

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      DECLARE @table table
        (
           ID               INT,
           Name             VARCHAR(MAX),
           DateStart        DateTime,
           DateEnd          DateTime,
           LastUpdateString VARCHAR(MAX)
        )

      SELECT @RecordCount = Count(*)
      FROm   vAnnouncement
      WHERE  ID_Company = @ID_Company
             AND CONVERT(Date, GETDATE()) BETWEEN CONVERT(Date, DateStart) AND Convert(Date, DateEnd)
             AND IsActive = 1

      SET @TotalPageCount = ( @RecordCount / @PageSize ) + 1
      SET @TotalPageCount = CASE
                              WHEN @RecordCount = 0 THEN 0
                              ELSE @TotalPageCount
                            END

      INSERT INTO @table
                  (ID,
                   Name,
                   DateStart,
                   DateEnd,
                   LastUpdateString)
      SELECT ID,
             Name,
             DateStart,
             DateEnd,
             dbo.GetDateFormatSince(DateStart) LastUpdateString
      FROm   vAnnouncement
      WHERE  ID_Company = @ID_Company
             AND CONVERT(Date, GETDATE()) BETWEEN CONVERT(Date, DateStart) AND Convert(Date, DateEnd)
             AND IsActive = 1
      ORDER  BY DATEDIFF(ss, DateStart, Getdate()) ASC
      OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY
      OPTION (RECOMPILE);

      SELECT @DisplayCount = COUNT(*)
      FROM   @table

      SELECT '_',
             '' Records

      SELECT @Success        Success,
             @RecordCount    TotalRecordCount,
             @DisplayCount   DisplayRecordCount,
             @PageNumber     CurrentPageNumber,
             @TotalPageCount TotalPageCount,
             2000            MilliSecondsDelay

      SELECT *
      FROM   @table
  END

GO

exec pGetLatestAnnouncement
  60945,
  1,
  3

SELECT *,
       dbo.GetDateFormatSince(DateStart) LastUpdateString,
       DATEDIFF(yy, DateStart, Getdate()),
       DATEDIFF(mm, DateStart, Getdate())
FROM   vAnnouncement
ORDER  BY DATEDIFF(ss, DateStart, Getdate()) ASC 
