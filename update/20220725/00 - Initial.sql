go

IF OBJECT_ID (N'dbo.tSoloParent', N'U') IS NULL
  BeGIN
      Exec _pCreateAppModuleWithTable
        'tSoloParent',
        1,
        NULL,
        NULL
  END

GO

IF OBJECT_ID (N'dbo.tCertificationOfNoIncome', N'U') IS NULL
  BeGIN
      Exec _pCreateAppModuleWithTable
        'tCertificationOfNoIncome',
        1,
        NULL,
        NULL
  END

GO

if( COL_LENGTH ('tCertificationOfNoIncome', 'Date') IS NULL )
  BEGIN
      EXEC _pAddModelProperty
        'tCertificationOfNoIncome',
        'Date',
        5
  END

GO

if( COL_LENGTH ('tCertificationOfNoIncome', 'Age') IS NULL )
  BEGIN
      EXEC _pAddModelProperty
        'tCertificationOfNoIncome',
        'Age',
        1
  END

GO

if( COL_LENGTH ('tResident', 'IsOFW') IS NULL )
  BEGIN
      EXEC _pAddModelProperty
        'tResident',
        'IsOFW',
        4
  END

GO

if( COL_LENGTH ('tResident', 'IsPWD') IS NULL )
  BEGIN
      EXEC _pAddModelProperty
        'tResident',
        'IsPWD',
        4
  END

GO

if( COL_LENGTH ('tResident', 'Citizenship') IS NULL )
  BEGIN
      EXEC _pAddModelProperty
        'tCertificationOfNoIncome',
        'Citizenship',
        1
  END

GO

if( COL_LENGTH ('tSoloParent', 'Date') IS NULL )
  BEGIN
      EXEC _pAddModelProperty
        'tSoloParent',
        'Date',
        5
  END

GO

if( COL_LENGTH ('tSoloParent', 'ID_RelationshipType') IS NULL )
  BEGIN
      EXEC _pAddModelProperty
        'tSoloParent',
        'ID_RelationshipType',
        2
  END

GO

if( COL_LENGTH ('tSoloParent', 'Age') IS NULL )
  BEGIN
      EXEC _pAddModelProperty
        'tSoloParent',
        'Age',
        1
  END

GO

if( COL_LENGTH ('tSoloParent', 'ParentName') IS NULL )
  BEGIN
      EXEC _pAddModelProperty
        'tSoloParent',
        'ParentName',
        1
  END

GO

if( COL_LENGTH ('tSoloParent', 'ParentAge') IS NULL )
  BEGIN
      EXEC _pAddModelProperty
        'tSoloParent',
        'ParentAge',
        1
  END

GO

if( COL_LENGTH ('tSoloParent', 'Purpose') IS NULL )
  BEGIN
      EXEC _pAddModelProperty
        'tSoloParent',
        'Purpose',
        1
  END

GO

if( COL_LENGTH ('tSoloParent', 'Reason') IS NULL )
  BEGIN
      EXEC _pAddModelProperty
        'tSoloParent',
        'Reason',
        1
  END

GO

if( COL_LENGTH ('tBarangayClearance', 'Requestor') IS NULL )
  BEGIN
      EXEC _pAddModelProperty
        'tBarangayClearance',
        'Requestor',
        1
  END

GO

if( COL_LENGTH ('tResident', 'isSoloParent') IS NULL )
  BEGIN
      EXEC _pAddModelProperty
        'tResident',
        'isSoloParent',
        4
  END

GO

if( COL_LENGTH ('tCertificateOfResidency', 'ImageProfilePicFilename') IS NULL )
  BEGIN
      EXEC _pAddModelProperty
        'tCertificateOfResidency',
        'ImageProfilePicFilename',
        1
  END

GO

if( COL_LENGTH ('tCertificateOfResidency', 'Purpose') IS NULL )
  BEGIN
      EXEC _pAddModelProperty
        'tCertificateOfResidency',
        'Purpose',
        1
  END

GO

GO

if( COL_LENGTH ('tResident_Business', 'BusinessName') IS NULL )
  BEGIN
      EXEC _pAddModelProperty
        'tResident_Business',
        'BusinessName',
        1
  END

GO

if( COL_LENGTH ('tResident_Business', 'ContactNumber') IS NULL )
  BEGIN
      EXEC _pAddModelProperty
        'tResident_Business',
        'ContactNumber',
        1
  END

GO

if( COL_LENGTH ('tGender', 'TranslatedEngName') IS NULL )
  BEGIN
      ALTER TABLE tGender
        Add TranslatedEngName VARCHAR(250);
  END

GO

CREATE OR
ALTER FUNCTION dbo.fGetModelReportMenuItemsByCompany(@ID_Company INT,
                                                     @Name_Model VARCHAR(MAX))
RETURNS @table TABLE(
  label       varchar(MAX),
  Name_Report varchar(MAX),
  name        varchar(MAX))
as
  BEGIN
      INSERT @table
      select Name_ModelReport            label,
             Name_Report,
             'ModelReport-' + Oid_Report name
      FROM   vModelReport
      WHERE  ID_Company = @ID_Company
             AND Name_Model = @Name_Model

      IF(SELECT COUNT(*)
         FROM   @table) = 0
        BEGIN
            INSERT @table
            select Name_ModelReport            label,
                   Name_Report,
                   'ModelReport-' + Oid_Report name
            FROM   vModelReport
            WHERE  ID_Company = 1
                   AND Name_Model = @Name_Model
        END

      RETURN
  end

GO

EXEC _pRefreshAllViews

GO

CREATE OR
ALTER VIEW [dbo].[vCompany]
AS
  SELECT H.*,
         UC.Name                           AS CreatedBy,
         UM.Name                           AS LastModifiedBy,
         dbo.fGetAPILink() + '/Content/Image/'
         + H.ImageHeaderFilename           ImageHeaderLocationFilenamePath,
         dbo.fGetAPILink() + '/Content/Thumbnail/'
         + H.ImageHeaderFilename           ImageHeaderThumbNameLocationFilenamePath,
         dbo.fGetAPILink() + '/Content/Image/'
         + H.ImageLogoFilename             ImageLogoLocationFilenamePath,
         dbo.fGetAPILink() + '/Content/Thumbnail/'
         + H.ImageLogoFilename             ImageLogoThumbNameLocationFilenamePath,
         dbo.fGetAPILink() + '/' + h.Code + '/'
         + h.ReceptionPortalGuid
         + '/BaranggayRegistrationPortal'  BarangayPortalLink,
         employee.Name                     BarangayCaptain_Name_Employee,
         ISNULL(employee.LastName, '')     BarangayCaptain_LastName_Employee,
         ISNULL(employee.FirstName, '')    BarangayCaptain_FirstName_Employee,
         ISNULL(employee.MiddleName, '')   BarangayCaptain_MiddleName_Employee,
         dbo.fGetAPILink() + '/Content/Thumbnail/'
         + employee.ImageSignitureSpecimen BarangayCaptain_ImageSignitureSpecimen_Employee
  FROM   tCompany H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN vEmployee employee
                ON H.BarangayCaptain_ID_Employee = employee.ID

GO

CREATE OR
ALTER View vModelReport
as
  SELECT modelReport.ID,
         modelReport.ID_Company,
         company.Name     Name_Company,
         Oid_Model,
         model.Name       Name_Model,
         Oid_Report,
         report.Name      Name_Report,
         modelReport.Name Name_ModelReport,
         company.Guid     Guid_Company
  FROm   _tModelReport modelReport
         LEFT JOIN _tModel model
                ON modelReport.Oid_Model = model.Oid
         LEFT JOIN _tReport report
                ON modelReport.Oid_Report = report.Oid
         LEFT JOIN tCompany company
                ON modelReport.ID_Company = company.ID

GO

GO


CREATE OR
ALTER VIEW vResident_Listview_Voter
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid Guid_Company
  FROM   vResident hed
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  where  hed.IsActive = 1
         and hed.IsVoter = 1

GO

CREATE OR
ALTER VIew vResidentAge
as
  select ID                                                  ID_Resident,
         FLOOR(DATEDIFF(DAY, DateBirth, GETDATE()) / 365.25) Age,
         DateBirth
  FROM   tResident

GO

CREATE OR
ALTER VIEW vResident_Listview_SeniorCitizen
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid Guid_Company,
         redAge.Age
  FROM   vResident hed
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
         LEFT JOIN vResidentAge redAge
                on redAge.ID_Resident = hed.ID
  WHERE  redAge.Age >= 60

GO

CREATE OR
ALTER VIEW vResident_Listview_SoloParent
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid Guid_Company
  FROM   vResident hed
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  hed.IsActive = 1
         and hed.isSoloParent = 1

GO

CREATE OR
ALTER VIEW vResident_Listview_OFW
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid Guid_Company
  FROM   vResident hed
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  Where  hed.IsActive = 1
         and hed.isOFW = 1

GO

CREATE OR
ALTER VIEW vResident_Listview_PWD
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid Guid_Company
  FROM   vResident hed
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  hed.IsActive = 1
         and hed.IsPwd = 1

GO 

CREATE  OR
ALTER VIEW vResident_SoloParent_Listview
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid Guid_Company
  FROM   vResident hed
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  hed.IsSoloParent = 1
         and hed.IsActive = 1

GO

CREATE   OR
ALTER VIEW vResidentDetailReportList
AS
  SELECT ROW_NUMBER()
           OVER(
             ORDER BY DateCreated ASC)               ID,
         ISNULL(Caption, Name)                       label,
         'far fa-file'                               icon,
         'text-prim1ary'                             class,
         1                                           visible,
         Name                                        reportname,
         'ModelReport-' + Convert(VARCHAR(MAX), Oid) name
  FROm   _tReport
  where  Name IN ( 'CLEARANCETOOPERATE', 'Pagpapatunay', 'CertificationIndegency', 'CertificationOfResidency',
                   'ClearanceToConstruct', 'CertificationForWorkandFamily', 'CertificationForNonIncome', 'CertificationOfEarnings',
                   'CerificationAgriculturalLand', 'CertificationOfCovidFreeCase', 'PagpapatunayNaNakauwiNaSaKanilangBayan', 'PagpapatunaySoloParent',
                   'Certificationof14Quarantine', 'CertificationofPAFExamination', 'CertificationofNoOffenseInKatarungangPambarangay', 'CertificationOfNoRecordForBHERTS',
                   'CertificationOfNonEmployed', 'PagpapatunayNgEvacuate', 'PagpapatunayNgHindiMakakakuhaNgSAP', 'CertificationOfResidency_HeadOfFamily',
                   'CertificationOfResidency_Permanent', 'CertificationOfPatricipationOfBusiness', 'CertificationOfNonResident', 'CertificationOfDeath',
                   'CertificationOfSeller', 'CertificationOfRTPCTestRequest', 'CertificateToTravel', 'CertificateToGoBackToPreviousResidence',
                   'CertificateOfInclusion_BHERTS', 'CertificateOfExclusion_CovidInvestigation', 'CertificateOfAcceptance', 'HealthCertificate',
                   'QuarantinePassCertification', 'CertificationOfIsolation', 'CertificateOf7DayQuarantine', 'CertificationOfEffectOfLockdown',
                   'CertificateOf14DayQuarantineCompletion', 'CertificateOfTravelDueToExam', 'ListOfExclusionNonCovidResidents', 'BARCCertificate',
                   'TanggapanNgBARCChairman', 'CertificateOfGoodMoral', 'CertificateOfSustainableLivelihoodProgram', 'BarangayClearance' )

GO

CREATE OR
ALTER VIEW vSoloParent
AS
  SELECT H.*,
         UC.Name     AS CreatedBy,
         UM.Name     AS LastModifiedBy,
         family.Name AS Name_Relationship
  FROM   tSoloParent H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tFamilyRelationshipType family
                ON H.ID_RelationshipType = family.ID

GO
GO

CREATE   OR
ALTER VIEW vResident_ProgramListview_SoloParentsCashIncentives
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid         Guid_Company,
         program.ID_Program   ID_Program,
         program.Name_Program Name_Program
  FROM   vResident hed
         INNER JOIN vResident_Program program
                 on hed.ID = program.ID_Resident
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  program.Name_Program = 'Solo Parents Cash Incentives'

GO

CREATE   OR
ALTER VIEW vResident_ProgramListview_GSIS
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid         Guid_Company,
         program.ID_Program   ID_Program,
         program.Name_Program Name_Program
  FROM   vResident hed
         INNER JOIN vResident_Program program
                 on hed.ID = program.ID_Resident
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  program.Name_Program = 'GSIS'

GO

CREATE   OR
ALTER VIEW vResident_ProgramListview_SSS
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid         Guid_Company,
         program.ID_Program   ID_Program,
         program.Name_Program Name_Program
  FROM   vResident hed
         INNER JOIN vResident_Program program
                 on hed.ID = program.ID_Resident
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  program.Name_Program = 'SSS'

GO

CREATE   OR
ALTER VIEW vResident_ProgramListview_PhiiHealth
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid         Guid_Company,
         program.ID_Program   ID_Program,
         program.Name_Program Name_Program
  FROM   vResident hed
         INNER JOIN vResident_Program program
                 on hed.ID = program.ID_Resident
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  program.Name_Program = 'PhiiHealth'

GO 

CREATE OR
ALTER VIEW vSoloParent_Listview
AS
  SELECT hed. ID,
         hed.Code,
         hed.Date,
         hed.Name,
         hed.Age,
         hed.ParentName,
         hed.ParentAge,
         hed.Purpose,
         hed.Reason,
         hed.Name_Relationship,
         hed.ID_Company,
         hed.IsActive
  FROM   vSoloParent hed

GO

CREATE          OR
ALTER VIEW [dbo].vzSoloParentFormReport
AS
  SELECT hed.ID                                                  ID,
         hed.Date                                                Date,
         hed.Code                                                Code,
         hed.Name                                                Name,
         hed.Age                                                 Age,
         hed.ParentName                                          ParentName,
         hed.ParentAge                                           ParentAge,
         hed.Purpose,
         hed.Reason,
         hed.Name_Relationship,
         company.ID                                              ID_Company,
         company.ImageLogoLocationFilenamePath                   ImageLogoLocationFilenamePath,
         company.Name                                            Name_Company,
         company.Address                                         Address_Company,
         company.ContactNumber                                   ContactNumber_Company,
         company.BarangayListSideReportHTMLString                BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                             BarangayCaptainName_Company,
         company.BarangayCaptain_Name_Employee                   BarangayCaptain_Name_Employee,
         company.BarangayCaptain_ImageSignitureSpecimen_Employee BarangayCaptain_ImageSignitureSpecimen_Employee,
         company.BarangayCaptain_LastName_Employee               BarangayCaptain_LastName_Employee,
         company.BarangayCaptain_FirstName_Employee              BarangayCaptain_FirstName_Employee,
         company.BarangayCaptain_MiddleName_Employee             BarangayCaptain_MiddleName_Employee,
         municipality.Name                                       Name_MunicipalityCompany,
         municipality.ImageLogoLocationFilenamePath              ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                                    Address_MunicipalityCompany
  FROM   vSoloParent hed
         INNER JOIN vCompanyActive company
                 ON company.ID = hed.ID_Company
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = hed.ID_Company

GO

CREATE OR
ALTER VIEW vCompanyActiveUser
as
  SELECT user_.ID_Company,
         user_.Name_Company,
         user_.UserName,
         user_.Password
  FROm   vUser user_
         inner join vCompanyActive c
                 on user_.ID_Company = c.ID

GO

CREATE          OR
ALTER VIEW [dbo].vzBarangayClearanceFormReport
AS
  SELECT hed.ID                                                  ID,
         hed.Name                                                Name,
         hed.Code                                                Code,
         hed.Date                                                Date,
         hed.Address                                             Address,
         hed.Age                                                 Age,
         hed.Name_CivilStatus                                    Name_CivilStatus,
         hed.Requestor,
         hed.Name_Gender,
         gender.TranslatedEngName                                TranslatedEngName_Gender,
         hed.Purpose,
         civilStatus.TranslatedEngName                           TranslatedEngName_CivilStatus,
         dbo.fGetAPILink() + '/Content/Image/'
         + hed.ImageProfilePicFilename                           ImageProfilePicFilenamePath_BarangayClearance,
         company.ID                                              ID_Company,
         company.ImageLogoLocationFilenamePath                   ImageLogoLocationFilenamePath,
         company.Name                                            Name_Company,
         company.Address                                         Address_Company,
         company.ContactNumber                                   ContactNumber_Company,
         company.BarangayListSideReportHTMLString                BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                             BarangayCaptainName_Company,
         company.BarangayCaptain_Name_Employee                   BarangayCaptain_Name_Employee,
         company.BarangayCaptain_ImageSignitureSpecimen_Employee BarangayCaptain_ImageSignitureSpecimen_Employee,
         company.BarangayCaptain_LastName_Employee               BarangayCaptain_LastName_Employee,
         company.BarangayCaptain_FirstName_Employee              BarangayCaptain_FirstName_Employee,
         company.BarangayCaptain_MiddleName_Employee             BarangayCaptain_MiddleName_Employee,
         municipality.Name                                       Name_MunicipalityCompany,
         municipality.ImageLogoLocationFilenamePath              ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                                    Address_MunicipalityCompany
  FROM   vBarangayClearance hed
         INNER JOIN vCompanyActive company
                 ON company.ID = hed.ID_Company
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = hed.ID_Company
         LEFT Join tCivilStatus civilStatus
                ON civilStatus.ID = hed.ID_CivilStatus
         LEFT Join tGender gender
                ON gender.ID = hed.ID_Gender

GO

GO

CREATE    OR
ALTER VIEW [dbo].vzCertificationIndigencyFormReport
AS
  SELECT hed.ID                                                  ID,
         hed.Name                                                Name,
         hed.Code                                                Code,
         hed.Date                                                Date,
         hed.Address                                             Address,
         hed.Age                                                 Age,
         hed.Citizenship,
         hed.RequestOf,
         company.ID                                              ID_Company,
         company.ImageLogoLocationFilenamePath                   ImageLogoLocationFilenamePath,
         company.Name                                            Name_Company,
         company.Address                                         Address_Company,
         company.ContactNumber                                   ContactNumber_Company,
         company.BarangayListSideReportHTMLString                BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                             BarangayCaptainName_Company,
         company.BarangayCaptain_Name_Employee                   BarangayCaptain_Name_Employee,
         company.BarangayCaptain_ImageSignitureSpecimen_Employee BarangayCaptain_ImageSignitureSpecimen_Employee,
         company.BarangayCaptain_LastName_Employee               BarangayCaptain_LastName_Employee,
         company.BarangayCaptain_FirstName_Employee              BarangayCaptain_FirstName_Employee,
         company.BarangayCaptain_MiddleName_Employee             BarangayCaptain_MiddleName_Employee,
         municipality.Name                                       Name_MunicipalityCompany,
         municipality.ImageLogoLocationFilenamePath              ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                                    Address_MunicipalityCompany
  FROM   vCertificateOfIndigency hed
         INNER JOIN vCompanyActive company
                 ON company.ID = hed.ID_Company
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = hed.ID_Company

--LEFT Join tCivilStatus civilStatus
--       ON civilStatus.ID = hed.ID_CivilStatus
--LEFT Join tGender gender
--       ON gender.ID = hed.ID_Gender
GO

CREATE       OR
ALTER VIEW [dbo].vzCertificationResidencyFormReport
AS
  SELECT hed.ID                                                  ID,
         hed.Name                                                Name,
         hed. Purpose,
         dbo.fGetAPILink() + '/Content/Image/'
         + hed.ImageProfilePicFilename                           ImageProfilePicFilenamePath_CertificateOfResidency,
         hed.Code                                                Code,
         hed.Date                                                Date,
         hed.Address                                             Address,
         hed.Age                                                 Age,
         hed.Citizenship,
         hed.Requestor,
         company.ID                                              ID_Company,
         company.ImageLogoLocationFilenamePath                   ImageLogoLocationFilenamePath,
         company.Name                                            Name_Company,
         company.Address                                         Address_Company,
         company.ContactNumber                                   ContactNumber_Company,
         company.BarangayListSideReportHTMLString                BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                             BarangayCaptainName_Company,
         company.BarangayCaptain_Name_Employee                   BarangayCaptain_Name_Employee,
         company.BarangayCaptain_LastName_Employee               BarangayCaptain_LastName_Employee,
         company.BarangayCaptain_FirstName_Employee              BarangayCaptain_FirstName_Employee,
         company.BarangayCaptain_MiddleName_Employee             BarangayCaptain_MiddleName_Employee,
         company.BarangayCaptain_ImageSignitureSpecimen_Employee BarangayCaptain_ImageSignitureSpecimen_Employee,
         municipality.Name                                       Name_MunicipalityCompany,
         municipality.ImageLogoLocationFilenamePath              ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                                    Address_MunicipalityCompany
  FROM   vCertificateOfResidency hed
         INNER JOIN vCompanyActive company
                 ON company.ID = hed.ID_Company
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = hed.ID_Company

GO

CREATE OR
ALTER VIEW dbo.vzClearanceToOperateFormReport
AS
  SELECT Resident.ID                                             ID,
         company.ID                                              ID_Company,
         company.ImageLogoLocationFilenamePath                   ImageLogoLocationFilenamePath,
         company.Name                                            Name_Company,
         company.Address                                         Address_Company,
         company.ContactNumber                                   ContactNumber_Company,
         company.BarangayListSideReportHTMLString                BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                             BarangayCaptainName_Company,
         company.BarangayCaptain_Name_Employee                   BarangayCaptain_Name_Employee,
         company.BarangayCaptain_LastName_Employee               BarangayCaptain_LastName_Employee,
         company.BarangayCaptain_FirstName_Employee              BarangayCaptain_FirstName_Employee,
         company.BarangayCaptain_MiddleName_Employee             BarangayCaptain_MiddleName_Employee,
         company.BarangayCaptain_ImageSignitureSpecimen_Employee BarangayCaptain_ImageSignitureSpecimen_Employee,
         municipality.Name                                       Name_MunicipalityCompany,
         municipality.ImageLogoLocationFilenamePath              ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                                    Address_MunicipalityCompany,
         ISNULL(Resident.Name, '')                               Name_Business,
         ISNULL(Resident.Resident_BusinessOwner, '')             Name_Owner,
         Resident.DateValid,
         ISNULL(Resident.RequestedBy, '')                        RequestedBy,
         Resident.Address,
         resident.ContactNumber
  FROM   vResident_Business Resident
         INNER JOIN vCompanyActive company
                 ON company.ID = Resident.ID_Company
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = Resident.ID_Company

GO

CREATE       OR
ALTER VIEW [dbo].vzCertificationOfNoIncomeFormReport
AS
  SELECT hed.ID                                                  ID,
         hed.Name                                                Name,
         hed.Code                                                Code,
         hed.Date                                                Date,
         hed.Age                                                 Age,
         hed.Citizenship,
         company.ID                                              ID_Company,
         company.ImageLogoLocationFilenamePath                   ImageLogoLocationFilenamePath,
         company.Name                                            Name_Company,
         company.Address                                         Address_Company,
         company.ContactNumber                                   ContactNumber_Company,
         company.BarangayListSideReportHTMLString                BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                             BarangayCaptainName_Company,
         company.BarangayCaptain_Name_Employee                   BarangayCaptain_Name_Employee,
         company.BarangayCaptain_LastName_Employee               BarangayCaptain_LastName_Employee,
         company.BarangayCaptain_FirstName_Employee              BarangayCaptain_FirstName_Employee,
         company.BarangayCaptain_MiddleName_Employee             BarangayCaptain_MiddleName_Employee,
         company.BarangayCaptain_ImageSignitureSpecimen_Employee BarangayCaptain_ImageSignitureSpecimen_Employee,
         municipality.Name                                       Name_MunicipalityCompany,
         municipality.ImageLogoLocationFilenamePath              ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                                    Address_MunicipalityCompany
  FROM   vCertificationOfNoIncome hed
         INNER JOIN vCompanyActive company
                 ON company.ID = hed.ID_Company
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = hed.ID_Company

GO

CREATE OR
ALTER VIEW vzResidentVotersListReport
AS
  SELECT hed.ID,
         hed.Name,
         hed.Name_Gender,
         hed.ContactNumber,
         company.ID                                              ID_Company,
         company.ImageLogoLocationFilenamePath                   ImageLogoLocationFilenamePath,
         company.Name                                            Name_Company,
         company.Address                                         Address_Company,
         company.ContactNumber                                   ContactNumber_Company,
         company.BarangayListSideReportHTMLString                BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                             BarangayCaptainName_Company,
         company.BarangayCaptain_Name_Employee                   BarangayCaptain_Name_Employee,
         company.BarangayCaptain_LastName_Employee               BarangayCaptain_LastName_Employee,
         company.BarangayCaptain_FirstName_Employee              BarangayCaptain_FirstName_Employee,
         company.BarangayCaptain_MiddleName_Employee             BarangayCaptain_MiddleName_Employee,
         company.BarangayCaptain_ImageSignitureSpecimen_Employee BarangayCaptain_ImageSignitureSpecimen_Employee,
         municipality.Name                                       Name_MunicipalityCompany,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' <br/>Email: ' + company.Email
             ELSE ''
           END                                                   HeaderInfo_Company,
         municipality.ImageLogoLocationFilenamePath              ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                                    Address_MunicipalityCompany
  FROM   vResident hed
         INNER JOIN vCompanyActive company
                 ON company.ID = hed.ID_Company
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = hed.ID_Company
  WHERE  hed.IsActive = 1
         and hed.IsVoter = 1

GO

CREATE OR
ALTER PROCEDURE pGetCertificationOfNoIncome @ID         INT = -1,
                                            @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS Reports

      DECLARE @ID_User      INT,
              @ID_Warehouse INT,
              @ID_Company   INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL      AS [_],
                           -1        AS [ID],
                           NULL      AS [Code],
                           NULL      AS [Name],
                           1         AS [IsActive],
                           NULL      AS [ID_Company],
                           NULL      AS [Comment],
                           NULL      AS [DateCreated],
                           NULL      AS [DateModified],
                           NULL      AS [ID_CreatedBy],
                           NULL      AS [ID_LastModifiedBy],
                           GetDate() Date) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vCertificationOfNoIncome H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   dbo.fGetModelReportMenuItemsByCompany(@ID_Company, 'CertificationOfNoIncome')
  END

GO

CREATE OR
ALTER PROC pGetDashboardBarangaySummary @ID_UserSession INT = NULL
as
  BEGIN
      DECLARE @ID_User    INT,
              @ID_Company INT
      DECLARE @ResidentCount INT = 0
      DECLARE @PermanentResidentCount INT = 0
      DECLARE @MigrateResidentCount INT = 0
      DECLARE @TrancientResidentCount INT = 0
      DECLARE @HomeOwnershipStatusOwnerCount INT = 0
      DECLARE @EducationalLevelGraduatedCount INT = 0
      DECLARE @TotalResidentMaleCount INT = 0
      DECLARE @TotalResidentFemaleCount INT = 0
      DECLARE @ResidentCountAge60Above INT = 0
      DECLARE @ResidentCountAge18to59 INT = 0
      DECLARE @ResidentCountAge10to17 INT = 0
      DECLARE @ResidentCountAge5to9 INT = 0
      DECLARE @ResidentCountAge24to84months INT = 0
      DECLARE @ResidentCountAge12to23months INT = 0
      DECLARE @ResidentCountAge0to11months INT = 0
      DECLARE @ResidentCountMale INT = 0
      DECLARE @ResidentCountFemale INT = 0
      DECLARE @ResidentCount4Ps INT = 0
      DECLARE @ResidentCountSeniorCitizen INT = 0
      DECLARE @ResidentCountScholar INT = 0
      DECLARE @VoterCount INT = 0
      DECLARE @MayAriNgBahayAtLupaID_HomeOwnershipStatus INT = 1
      DECLARE @GraduateSchool_ID_EducationalLevel INT = 6
      DECLARE @Male_ID_Gender INT = 1
      DECLARE @Female_ID_Gender INT = 2

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      SELECT @ResidentCount = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1

      SELECT @PermanentResidentCount = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             AND IsPermanent = 1

      SELECT @MigrateResidentCount = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             AND IsMigrante = 1

      SELECT @TrancientResidentCount = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             AND IsTransient = 1

      SELECT @HomeOwnershipStatusOwnerCount = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             AND IsTransient = 1
             ANd ID_HomeOwnershipStatus = @MayAriNgBahayAtLupaID_HomeOwnershipStatus

      SELECT @EducationalLevelGraduatedCount = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             ANd ID_EducationalLevel = @GraduateSchool_ID_EducationalLevel

      SELECT @ResidentCountMale = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             ANd ID_Gender = @Male_ID_Gender

      SELECT @ResidentCountFemale = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             ANd ID_Gender = @Female_ID_Gender

      SELECT @ResidentCountAge60Above = COUNT(*)
      FROm   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) >= 60

      SELECT @ResidentCountAge18to59 = COUNT(*)
      FROm   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) BETWEEN 18 AND 59

      SELECT @ResidentCountAge10to17 = COUNT(*)
      FROm   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) BETWEEN 10 AND 17

      SELECT @ResidentCountAge5to9 = COUNT(*)
      FROm   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) BETWEEN 5 AND 9

      SELECT @ResidentCountAge24to84months = COUNT(*)
      FROm   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             and DateBirth IS NOT NULL
             AND DATEDIFF(MONTH, DateBirth, GETDATE()) BETWEEN 24 AND 84

      SELECT @ResidentCountAge12to23months = COUNT(*)
      FROm   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             and DateBirth IS NOT NULL
             AND DATEDIFF(MONTH, DateBirth, GETDATE()) BETWEEN 12 AND 23

      SELECT @ResidentCountAge0to11months = COUNT(*)
      FROm   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             and DateBirth IS NOT NULL
             AND DATEDIFF(MONTH, DateBirth, GETDATE()) BETWEEN 0 AND 11

      SELECT @ResidentCount4Ps = COUNT(*)
      FROM   (SELECT DISTINCT resident.ID
              FROM   tResident resident
                     inner join tResident_Program residentprogram
                             on resident.ID = residentprogram.ID_Resident
                     Inner join tProgram program
                             on residentprogram.ID_Program = program.ID
              where  resident.ID_Company = @ID_Company
                     and resident.IsActive = 1
                     AND program.Name IN ( '4Ps' )
              GROUP  BY resident.ID) tbl

      SELECT @ResidentCountSeniorCitizen = COUNT(*)
      FROM   (SELECT DISTINCT resident.ID
              FROM   tResident resident
                     inner join tResident_Program residentprogram
                             on resident.ID = residentprogram.ID_Resident
                     Inner join tProgram program
                             on residentprogram.ID_Program = program.ID
              where  resident.ID_Company = @ID_Company
                     and resident.IsActive = 1
                     AND program.Name IN ( 'Senior Citizen' )
              GROUP  BY resident.ID) tbl

      SELECT @ResidentCountScholar = COUNT(*)
      FROM   (SELECT DISTINCT resident.ID
              FROM   tResident resident
                     inner join tResident_Program residentprogram
                             on resident.ID = residentprogram.ID_Resident
                     Inner join tProgram program
                             on residentprogram.ID_Program = program.ID
              where  resident.ID_Company = @ID_Company
                     and resident.IsActive = 1
                     AND program.Name IN ( 'Scholarship' )
              GROUP  BY resident.ID) tbl

      SELECT '_'

      SELECT ISNULL(@ResidentCount, 0)                  ResidentCount,
             ISNULL(@VoterCount, 0)                     VoterCount,
             ISNULL(@PermanentResidentCount, 0)         PermanentResidentCount,
             ISNULL(@MigrateResidentCount, 0)           MigrateResidentCount,
             ISNULL(@TrancientResidentCount, 0)         TrancientResidentCount,
             ISNULL(@HomeOwnershipStatusOwnerCount, 0)  HomeOwnershipStatusOwnerCount,
             ISNULL(@EducationalLevelGraduatedCount, 0) EducationalLevelGraduatedCount,
             ISNULL(@ResidentCountAge60Above, 0)        ResidentCountAge60Above,
             ISNULL(@ResidentCountAge18to59, 0)         ResidentCountAge18to59,
             ISNULL(@ResidentCountAge10to17, 0)         ResidentCountAge10to17,
             ISNULL(@ResidentCountAge5to9, 0)           ResidentCountAge5to9,
             ISNULL(@ResidentCountAge24to84months, 0)   ResidentCountAge24to84months,
             ISNULL(@ResidentCountAge12to23months, 0)   ResidentCountAge12to23months,
             ISNULL(@ResidentCountAge0to11months, 0)    ResidentCountAge0to11months,
             ISNULL(@ResidentCountMale, 0)              ResidentCountMale,
             ISNULL(@ResidentCountFemale, 0)            ResidentCountFemale,
             ISNULL(@ResidentCount4Ps, 0)               ResidentCount4Ps,
             ISNULL(@ResidentCountSeniorCitizen, 0)     ResidentCountSeniorCitizen,
             ISNULL(@ResidentCountScholar, 0)           ResidentCountScholar
  END

GO

CREATE OR
ALTER PROC pAddModelReportByCompanyGUID(@GUID_Company VARCHAR(MAX),
                                        @ModelName    Varchar(200),
                                        @ReportName   VARCHAR(200),
                                        @labelName    VARCHAR(200))
AS
  BEGIN
      IF(SELECT Count(*)
         FROM   vCompanyActive
         WHERE  Guid = @GUID_Company
                AND IsActive = 1) <> 1
        BEGIN ;
            THROW 51000, 'Company does not exist.', 1;
        END

      DECLARE @ID_Company INT

      SELECT @ID_Company = ID
      FROM   tCompany
      WHERE  Guid = @GUID_Company

      exec pAddModelReport
        @ID_Company,
        @ModelName,
        @ReportName,
        @labelName
  END

GO

CREATE OR
ALTER PROCEDURE pGetBarangayClearance @ID         INT = -1,
                                      @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS Reports

      DECLARE @ID_User      INT,
              @ID_Warehouse INT,
              @ID_Company   INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL      AS [_],
                           -1        AS [ID],
                           NULL      AS [Code],
                           NULL      AS [Name],
                           1         AS [IsActive],
                           NULL      AS [ID_Company],
                           NULL      AS [Comment],
                           NULL      AS [DateCreated],
                           NULL      AS [DateModified],
                           NULL      AS [ID_CreatedBy],
                           NULL      AS [ID_LastModifiedBy],
                           GETDATE() Date) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vBarangayClearance H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   dbo.fGetModelReportMenuItemsByCompany(@ID_Company, 'BarangayClearance')
  END

GO

CREATE   OR
ALTER PROCEDURE pGetCertificateOfIndigency @ID         INT = -1,
                                           @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' Reports

      DECLARE @ID_User      INT,
              @ID_Warehouse INT,
              @ID_Company   INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL      AS [_],
                           -1        AS [ID],
                           NULL      AS [Code],
                           NULL      AS [Name],
                           1         AS [IsActive],
                           NULL      AS [ID_Company],
                           NULL      AS [Comment],
                           NULL      AS [DateCreated],
                           NULL      AS [DateModified],
                           NULL      AS [ID_CreatedBy],
                           NULL      AS [ID_LastModifiedBy],
                           GETDATE() Date) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vCertificateOfIndigency H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   dbo.fGetModelReportMenuItemsByCompany(@ID_Company, 'CertificateOfIndigency')
  END

GO

CREATE  OR
ALTER PROCEDURE pGetCertificateOfResidency @ID         INT = -1,
                                           @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' Reports

      DECLARE @ID_User      INT,
              @ID_Warehouse INT,
              @ID_Company   INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL      AS [_],
                           -1        AS [ID],
                           NULL      AS [Code],
                           NULL      AS [Name],
                           1         AS [IsActive],
                           NULL      AS [ID_Company],
                           NULL      AS [Comment],
                           NULL      AS [DateCreated],
                           NULL      AS [DateModified],
                           NULL      AS [ID_CreatedBy],
                           NULL      AS [ID_LastModifiedBy],
                           GETDATE() Date) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vCertificateOfResidency H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   dbo.fGetModelReportMenuItemsByCompany(@ID_Company, 'CertificateOfResidency')
  END

GO

CREATE or
alter PROCEDURE pGetSoloParent @ID         INT = -1,
                               @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' Reports

      DECLARE @ID_User      INT,
              @ID_Warehouse INT,
              @ID_Company   INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL      AS [_],
                           -1        AS [ID],
                           NULL      AS [Code],
                           NULL      AS [Name],
                           1         AS [IsActive],
                           NULL      AS [ID_Company],
                           NULL      AS [Comment],
                           NULL      AS [DateCreated],
                           NULL      AS [DateModified],
                           NULL      AS [ID_CreatedBy],
                           NULL      AS [ID_LastModifiedBy],
                           GETDATE() Date) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vSoloParent H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   dbo.fGetModelReportMenuItemsByCompany(@ID_Company, 'SoloParent')
  END

GO

CREATE   OR
ALTER PROCEDURE pGetResident_Business @ID         INT = -1,
                                      @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' Reports

      DECLARE @ID_User      INT,
              @ID_Warehouse INT,
              @ID_Company   INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL AS [_],
                           -1   AS [ID],
                           NULL AS [Code],
                           NULL AS [Name],
                           1    AS [IsActive],
                           NULL AS [ID_Company],
                           NULL AS [Comment],
                           NULL AS [DateCreated],
                           NULL AS [DateModified],
                           NULL AS [ID_CreatedBy],
                           NULL AS [ID_LastModifiedBy]) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vResident_Business H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   dbo.fGetModelReportMenuItemsByCompany(@ID_Company, 'Resident_Business')
  END

GO

CREATE OR
ALTER PROC pDeleteModelReportByCompanyGUID(@GUID_Company VARCHAR(MAX),
                                           @ModelName    Varchar(200),
                                           @ReportName   VARCHAR(200))
AS
  BEGIN
      DECLARE @OID_Report VARCHAR(MAX) = ''
      DECLARE @OID_Model VARCHAR(MAX) = ''
      DECLARE @ID_Company INT

      SELECT @ID_Company = ID
      FROM   tCompany
      WHERE  Guid = @GUID_Company

      SELECT @OID_Report = Oid
      FROM   _tReport
      where  Name = @ReportName

      SELECT @OID_Model = Oid
      FROM   _tModel
      where  Name = @ModelName

      DELETE FROM _tModelReport
      WHERE  ID_Company = @ID_Company
             and Oid_Report = @OID_Report
  END

GO

IF NOT EXISTS (SELECT 1
               FROM   _tReport
               WHERE  Name = 'BarangayClearanceFormReport_CityOfLipaBatangas_BrgyTambo')
  BEGIN
      exec _pCreateReportView
        'BarangayClearanceFormReport_CityOfLipaBatangas_BrgyTambo',
        1
  END

GO

IF NOT EXISTS (SELECT 1
               FROM   _tReport
               WHERE  Name = 'CertificationIndigencyFormReport_CityOfLipaBatangas_BrgyTambo')
  BEGIN
      exec _pCreateReportView
        'CertificationIndigencyFormReport_CityOfLipaBatangas_BrgyTambo',
        1
  END

GO

IF NOT EXISTS (SELECT 1
               FROM   _tReport
               WHERE  Name = 'CertificateOfResidencyFormReport_CityOfLipaBatangas_BrgyTambo')
  BEGIN
      exec _pCreateReportView
        'CertificateOfResidencyFormReport_CityOfLipaBatangas_BrgyTambo',
        1
  END

GO

IF NOT EXISTS (SELECT 1
               FROM   _tReport
               WHERE  Name = 'ClearanceToOperateFormReport_CityOfLipaBatangas_BrgyTambo')
  BEGIN
      exec _pCreateReportView
        'ClearanceToOperateFormReport_CityOfLipaBatangas_BrgyTambo',
        1
  END

GO

IF NOT EXISTS (SELECT 1
               FROM   _tReport
               WHERE  Name = 'ResidentVoterListReport')
  BEGIN
      exec _pCreateReportView
        'ResidentVoterListReport',
        1
  END

GO

IF NOT EXISTS (SELECT 1
               FROM   _tReport
               WHERE  Name = 'SoloParentForm')
  BEGIN
      exec _pCreateReportView
        'SoloParentForm',
        1
  END

GO

IF NOT EXISTS (SELECT 1
               FROM   _tReport
               WHERE  Name = 'CertificationOfNoIncomeForm')
  BEGIN
      exec _pCreateReportView
        'CertificationOfNoIncomeForm',
        1
  END

GO

Update tGender
SET    TranslatedEngName = 'Male'
WHERE  ID = 1

Update tGender
SET    TranslatedEngName = 'Female'
WHERE  ID = 2

update vCompanyActive
SET    BarangayListSideReportHTMLString =
'<center><b style="color: red">SANGUNIANG PAMBARANGAY</b><br/><b>HON. JOEL I. KATIGBAK</b><br/> Barangay Chairman</br></center><br/> <b>HON. DIGNA A. BAUTISTA</b><br/> - Committee on Cooperatives and Agriculture<br/> - Committee on Women and Family,LGBT and Persons with Disability<br/> - Committee on Livelihood<br/><br/><b>HON. ARTURO I. ZARA</b><br/>- Committee on Finance, Budget and Appropriation<br/> - Committee on Rules and Privileges<br/> - Committee on Good Governance<br/> <br/><b>HON. GINA G. LUNA</b><br/>- Committee on Environmental Protection<br/> - Committee on Senior Citizen/ Urban Poor<br/> <br/><b>HON. SANDY S. ILUSTRE</b><br/> - Committee on Public Works<br/> - Committee on Trade and Industry<br/> <br/><b>HON. APOLLO VICENTE G. ORENSE</b><br/> - Committee on Information Technology<br/> - Committee on Religious & Liturgical Affairs<br/> - Committee on Cultural Affairs and Tourism<br/> - Committee on Risk Reduction<br/> <br/><b>HON. ALLAN J. SADSAD</b><br/> - Committee on Education<br/>- Committee on Health and Sanitation<br/> - Committee on Peace and Order, Public Safety and Drug Abuse<br/> <br/><b>HON.CAYETANO A. GANDEZA JR.</b><br/> - Committee on Labor and Employment<br/> - Committee on Transportation<br/> <br/><b>HON. MARY LOIS L. GANDEZA</b><br/> - SK CHAIRPERSON<br/>- Committee on Youth and Sports'
WHERE  GUID = '66A2A296-7EBD-4A81-960E-D435C9D22DF7'

update vCompanyActive
SET    Name = 'Brgy. Tambo'
WHERE  GUID = '66A2A296-7EBD-4A81-960E-D435C9D22DF7'

GO

-- Brgy. Tambo
exec pAddModelReportByCompanyGUID
  '66A2A296-7EBD-4A81-960E-D435C9D22DF7',
  'BarangayClearance',
  'BarangayClearanceFormReport_CityOfLipaBatangas_BrgyTambo',
  'Barangay Clearance'

GO

exec pAddModelReportByCompanyGUID
  '66A2A296-7EBD-4A81-960E-D435C9D22DF7',
  'CertificateOfIndigency',
  'CertificationIndigencyFormReport_CityOfLipaBatangas_BrgyTambo',
  'Certificate Of Indigency'

GO

exec pAddModelReportByCompanyGUID
  '66A2A296-7EBD-4A81-960E-D435C9D22DF7',
  'CertificateOfResidency',
  'CertificateOfResidencyFormReport_CityOfLipaBatangas_BrgyTambo',
  'Certificate Of Residency'

GO

exec pAddModelReportByCompanyGUID
  '66A2A296-7EBD-4A81-960E-D435C9D22DF7',
  'Resident_Business',
  'ClearanceToOperateFormReport_CityOfLipaBatangas_BrgyTambo',
  'Clearance To Operate'

GO

exec pAddModelReport
  1,
  'SoloParent',
  'SoloParentForm',
  'Solo Parent Form'

GO

exec pAddModelReport
  1,
  'CertificationOfNoIncome',
  'CertificationOfNoIncomeForm',
  'Certification Of No Income Form'

GO

SELECT *
FROM   vCompanyActiveUser
Order  by Name_Company

SELECT *
FROM   vModelReport
Order  by Name_Company,
          Name_Model

GO 
