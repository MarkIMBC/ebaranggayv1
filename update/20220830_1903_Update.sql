GO

CREATE   OR
ALTER FUNCTION fGetCleanedString(@Value VARCHAR(MAX))
RETURNS VARCHAR(MAX)
AS
  BEGIN
      RETURN LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(@Value, '  ', ' '), CHAR(9), ''), CHAR(10), ''), CHAR(13), '')))
  END

GO

GO

ALTER VIEW [dbo].[vzResidentDetailReport]
AS
  SELECT hed.Name,
         hed.IsActive,
         hed.ID_Company,
         hed.ID_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.Name_Gender,
         hed.Name_Company,
         hed.IsTransient,
         hed.IsPermanent,
         hed.IsMigrante,
         company.ImageLogoLocationFilenamePath,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                    HeaderInfo_Company,
         [dbo].[fGetOrdinalNumber] (GETDATE())    AS OrdinalNum,
         ISNULL(dbo.fGetIntAge(hed.DateBirth), 0) AS Age
  FROM   vResident hed
         LEFT JOIN dbo.vCompany company
                ON company.ID = hed.ID_Company 
  WHERE  hed.IsActive = 1 

GO

CREATE  OR
ALTER PROC dbo.pResident_Validation (@ID_Resident    INT,
                                     @Name           VARCHAR(MAX),
                                     @ID_UserSession INT)
AS
  BEGIN
      DECLARE @isValid BIT = 1;
      DECLARE @isWarning BIT = 0;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @ID_Company INT = 0

      SELECT @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      where  _usersession.ID = @ID_UserSession

      BEGIN TRY
	  
          DECLARE @Count_ResidentNames INT = 0
          DECLARE @Saved_Name_Resident VARCHAR( MAX) = ''

		  SELECT @Saved_Name_Resident = Name FROm tResident WHERE ID = @ID_Resident

          SELECT @Count_ResidentNames = COUNT(*)
          FROm   tResident
          where  REPLACE(dbo.fGetCleanedString(Name), ' ', '') = REPLACE(dbo.fGetCleanedString(@Name), ' ', '')
                 and ID_Company = @ID_Company
                 and IsActive = 1
                 AND ID NOT IN ( @ID_Resident )

          IF ( @Count_ResidentNames ) > 0
            BEGIN
                if( @ID_Resident > 0 AND REPLACE(dbo.fGetCleanedString(@Saved_Name_Resident), ' ', '') <> REPLACE(dbo.fGetCleanedString(@Name), ' ', '')  )
                  BEGIN
                      SET @isWarning = 1
                      SET @message = 'Resident '''
                                     + dbo.fGetCleanedString(@Name) + ''' '
                                     + CASE
                                         WHEN @Count_ResidentNames > 1 then 'have'
                                         else 'has'
                                       END
                                     + ' '
                                     + CONVERT(VARCHAR(MAX), @Count_ResidentNames)
                                     + ' already exist.';
                  END
                ELSE
                  BEGIN
                      SET @message = 'Resident Name is already exist.';
                  END;

                THROW 50001, @message, 1;
            END;

      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @isValid = 0;
      END CATCH

      SELECT '_';

      SELECT @isValid   isValid,
             @isWarning isWarning,
             @message   message;
  END

GO

exec pResident_Validation
  @ID_Resident=5265,
  @Name=N'43',
  @ID_UserSession=N'61255'

go 
 
