  
CREATE OR ALTER   
 VIEW vResidentDetailReportList  
AS  
  SELECT ROW_NUMBER()  
           OVER(  
             ORDER BY DateCreated ASC)               ID,  
         ISNULL(Caption, Name)                       label,  
         'far fa-file'                               icon,  
         'text-primary'                              class,  
         1                                           visible,  
         Name                                        reportname,  
         'ModelReport-' + Convert(VARCHAR(MAX), Oid) name  
  FROm   _tReport  
  where  Name IN (  'CLEARANCETOOPERATE', 'Pagpapatunay', 'CertificationIndegency',  
                   'CertificationOfResidency', 'ClearanceToConstruct', 'CertificationForWorkandFamily', 'CertificationForNonIncome',  
                   'CertificationOfEarnings', 'CerificationAgriculturalLand', 'CertificationOfCovidFreeCase', 'PagpapatunayNaNakauwiNaSaKanilangBayan',  
                   'PagpapatunaySoloParent', 'Certificationof14Quarantine', 'CertificationofPAFExamination', 'CertificationofNoOffenseInKatarungangPambarangay',  
                   'CertificationOfNoRecordForBHERTS', 'CertificationOfNonEmployed', 'PagpapatunayNgEvacuate', 'PagpapatunayNgHindiMakakakuhaNgSAP',  
                   'CertificationOfResidency_HeadOfFamily', 'CertificationOfResidency_Permanent', 'CertificationOfPatricipationOfBusiness', 'CertificationOfNonResident',  
                   'CertificationOfDeath', 'CertificationOfSeller', 'CertificationOfRTPCTestRequest', 'CertificateToTravel',  
                   'CertificateToGoBackToPreviousResidence', 'CertificateOfInclusion_BHERTS', 'CertificateOfExclusion_CovidInvestigation', 'CertificateOfAcceptance',  
                   'HealthCertificate', 'QuarantinePassCertification', 'CertificationOfIsolation', 'CertificateOf7DayQuarantine',  
                   'CertificationOfEffectOfLockdown', 'CertificateOf14DayQuarantineCompletion', 'CertificateOfTravelDueToExam', 'ListOfExclusionNonCovidResidents',  
                   'BARCCertificate', 'TanggapanNgBARCChairman', 'CertificateOfGoodMoral', 'CertificateOfSustainableLivelihoodProgram' )  
  

  exec _pRefreshAllViews