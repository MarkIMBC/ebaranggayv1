IF Object_Id('dbo.tBarangayClearance') IS NULL
  BEGIN
      EXEC _pCreateappModuleWithTable
        'tBarangayClearance',
        1,
        0,
        NULL
  END

GO

IF Object_Id('dbo.tCertificateOfResidency') IS NULL
  BEGIN
      EXEC _pCreateappModuleWithTable
        'tCertificateOfResidency',
        1,
        0,
        NULL
  END

GO

IF Object_Id('dbo.tCertificateOfIndigency') IS NULL
  BEGIN
      EXEC _pCreateappModuleWithTable
        'tCertificateOfIndigency',
        1,
        0,
        NULL
  END

GO

IF COL_LENGTH('dbo.tBarangayClearance', 'Date') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tBarangayClearance',
        'Date',
        5
  END

GO

IF COL_LENGTH('dbo.tBarangayClearance', 'Age') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tBarangayClearance',
        'Age',
        1
  END

GO

IF COL_LENGTH('dbo.tBarangayClearance', 'ImageProfilePicFilename') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tBarangayClearance',
        'ImageProfilePicFilename',
        1
  END

GO

IF COL_LENGTH('dbo.tBarangayClearance', 'ID_Gender') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tBarangayClearance',
        'ID_Gender',
        2
  END

GO

IF COL_LENGTH('dbo.tBarangayClearance', 'ID_CivilStatus') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tBarangayClearance',
        'ID_CivilStatus',
        2
  END

GO

IF COL_LENGTH('dbo.tBarangayClearance', 'Address') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tBarangayClearance',
        'Address',
        1
  END

GO

IF COL_LENGTH('dbo.tBarangayClearance', 'Purpose') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tBarangayClearance',
        'Purpose',
        1
  END

GO

IF COL_LENGTH('dbo.tCertificateOfIndigency', 'Date') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tCertificateOfIndigency',
        'Date',
        5
  END

GO

IF COL_LENGTH('dbo.tCertificateOfIndigency', 'Age') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tCertificateOfIndigency',
        'Age',
        1
  END

GO

IF COL_LENGTH('dbo.tCertificateOfIndigency', 'Citizenship') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tCertificateOfIndigency',
        'Citizenship',
        1
  END

GO

IF COL_LENGTH('dbo.tCertificateOfIndigency', 'Address') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tCertificateOfIndigency',
        'Address',
        1
  END

GO

IF COL_LENGTH('dbo.tCertificateOfIndigency', 'RequestOf') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tCertificateOfIndigency',
        'RequestOf',
        1
  END

GO

IF COL_LENGTH('dbo.tCertificateOfResidency', 'Age') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tCertificateOfResidency',
        'Age',
        1
  END

GO

IF COL_LENGTH('dbo.tCertificateOfResidency', 'Citizenship') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tCertificateOfResidency',
        'Citizenship',
        1
  END

GO

IF COL_LENGTH('dbo.tCertificateOfResidency', 'Address') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tCertificateOfResidency',
        'Address',
        1
  END

GO

IF COL_LENGTH('dbo.tCertificateOfResidency', 'StayInDuration') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tCertificateOfResidency',
        'StayInDuration',
        1
  END

GO

IF COL_LENGTH('dbo.tCertificateOfResidency', 'Requestor') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tCertificateOfResidency',
        'Requestor',
        1
  END

GO

IF COL_LENGTH('dbo.tCertificateOfResidency', 'Date') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tCertificateOfResidency',
        'Date',
        5
  END

GO

EXEC _pRefreshAllViews

GO

CREATE OR
ALTER VIEW vBarangayClearance
AS
  SELECT H.*,
         UC.Name          AS CreatedBy,
         UM.Name          AS LastModifiedBy,
         civilStatus.Name Name_CivilStatus,
         gender.Name      Name_Gender
  FROM   tBarangayClearance H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tCivilStatus civilStatus
                ON H.ID_CivilStatus = civilStatus.ID
         LEFT JOIN tGender gender
                ON H.ID_Gender = gender.ID

GO

GO

CREATE       Or
Alter VIEW [dbo].vzBarangayClearanceFormReport
AS
  SELECT hed.ID                                                  ID,
         hed.Name                                                Name,
         hed.Code                                                Code,
         hed.Date                                                Date,
         hed.Address                                             Address,
         hed.Age                                                 Age,
         hed.Name_CivilStatus                                    Name_CivilStatus,
         hed.Name_Gender,
         hed.Purpose,
         dbo.fGetAPILink() + '/Content/Thumbnail/'
         + hed.ImageProfilePicFilename                           ImageProfilePicFilenamePath_BarangayClearance,
         company.ID                                              ID_Company,
         company.ImageLogoLocationFilenamePath                   ImageLogoLocationFilenamePath,
         company.Name                                            Name_Company,
         company.Address                                         Address_Company,
         company.ContactNumber                                   ContactNumber_Company,
         company.BarangayListSideReportHTMLString                BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                             BarangayCaptainName_Company,
         company.BarangayCaptain_Name_Employee                   BarangayCaptain_Name_Employee,
         company.BarangayCaptain_ImageSignitureSpecimen_Employee BarangayCaptain_ImageSignitureSpecimen_Employee,
         municipality.Name                                       Name_MunicipalityCompany,
         municipality.ImageLogoLocationFilenamePath              ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                                    Address_MunicipalityCompany
  FROM   vBarangayClearance hed
         INNER JOIN vCompanyActive company
                 ON company.ID = hed.ID_Company
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = hed.ID_Company

GO

CREATE OR
ALTER VIEW vCertificateOfIndigency
AS
  SELECT H.*,
         UC.Name AS CreatedBy,
         UM.Name AS LastModifiedBy
  FROM   tCertificateOfIndigency H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID

GO

CREATE OR
ALTER VIEW vBarangayClearance_Listview
AS
  SELECT hed.ID,
         hed.Date,
         hed.Code,
         hed.Name,
         hed.Name_Gender,
         hed.Name_CivilStatus,
         hed.Purpose,
         hed.ID_Company,
         hed.IsActive
  FROM   vBarangayClearance hed

GO

CREATE OR
ALTER VIEW vCertificateOfIndigency_Listview
AS
  SELECT hed.ID,
         hed.Date,
         hed.Code,
         hed.Name,
         hed.Age,
         hed.Citizenship,
         hed.RequestOf,
         hed.ID_Company,
         hed.IsActive
  FROM   vCertificateOfIndigency hed

GO

CREATE OR
ALTER VIEW vCertificateOfResidency_Listview
AS
  SELECT hed.ID,
         hed.Date,
         hed.Code,
         hed.Name,
         hed.Age,
         hed.Citizenship,
         hed.Address,
         hed.StayInDuration,
         hed.Requestor,
         hed.ID_Company,
         hed.IsActive
  FROM   vCertificateOfResidency hed

GO

CREATE       Or
Alter VIEW [dbo].vzCertificationIndigencyFormReport
AS
  SELECT hed.ID                                                  ID,
         hed.Name                                                Name,
         hed.Code                                                Code,
         hed.Date                                                Date,
         hed.Address                                             Address,
         hed.Age                                                 Age,
         hed.Citizenship,
         hed.RequestOf,
         company.ID                                              ID_Company,
         company.ImageLogoLocationFilenamePath                   ImageLogoLocationFilenamePath,
         company.Name                                            Name_Company,
         company.Address                                         Address_Company,
         company.ContactNumber                                   ContactNumber_Company,
         company.BarangayListSideReportHTMLString                BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                             BarangayCaptainName_Company,
         company.BarangayCaptain_Name_Employee                   BarangayCaptain_Name_Employee,
         company.BarangayCaptain_ImageSignitureSpecimen_Employee BarangayCaptain_ImageSignitureSpecimen_Employee,
         municipality.Name                                       Name_MunicipalityCompany,
         municipality.ImageLogoLocationFilenamePath              ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                                    Address_MunicipalityCompany
  FROM   vCertificateOfIndigency hed
         INNER JOIN vCompanyActive company
                 ON company.ID = hed.ID_Company
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = hed.ID_Company

GO

CREATE       Or
Alter VIEW [dbo].vzCertificationResidencyFormReport
AS
  SELECT hed.ID                                                  ID,
         hed.Name                                                Name,
         hed.Code                                                Code,
         hed.Date                                                Date,
         hed.Address                                             Address,
         hed.Age                                                 Age,
         hed.Citizenship,
         hed.Requestor,
         company.ID                                              ID_Company,
         company.ImageLogoLocationFilenamePath                   ImageLogoLocationFilenamePath,
         company.Name                                            Name_Company,
         company.Address                                         Address_Company,
         company.ContactNumber                                   ContactNumber_Company,
         company.BarangayListSideReportHTMLString                BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                             BarangayCaptainName_Company,
         company.BarangayCaptain_Name_Employee                   BarangayCaptain_Name_Employee,
         company.BarangayCaptain_ImageSignitureSpecimen_Employee BarangayCaptain_ImageSignitureSpecimen_Employee,
         municipality.Name                                       Name_MunicipalityCompany,
         municipality.ImageLogoLocationFilenamePath              ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                                    Address_MunicipalityCompany
  FROM   vCertificateOfResidency hed
         INNER JOIN vCompanyActive company
                 ON company.ID = hed.ID_Company
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = hed.ID_Company

GO

GO

CREATE OR
ALTER View vModelReport
as
  SELECT modelReport.ID,
         modelReport.ID_Company,
         company.Name     Name_Company,
         Oid_Model,
         model.Name       Name_Model,
         Oid_Report,
         report.Name      Name_Report,
         modelReport.Name Name_ModelReport
  FROm   _tModelReport modelReport
         LEFT JOIN _tModel model
                ON modelReport.Oid_Model = model.Oid
         LEFT JOIN _tReport report
                ON modelReport.Oid_Report = report.Oid
         LEFT JOIN tCompany company
                ON modelReport.ID_Company = company.ID

GO

CREATE OR
ALTER PROCEDURE pGetBarangayClearance @ID         INT = -1,
                                      @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS Reports

      DECLARE @ID_User      INT,
              @ID_Warehouse INT,
              @ID_Company   INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL      AS [_],
                           -1        AS [ID],
                           NULL      AS [Code],
                           NULL      AS [Name],
                           1         AS [IsActive],
                           NULL      AS [ID_Company],
                           NULL      AS [Comment],
                           NULL      AS [DateCreated],
                           NULL      AS [DateModified],
                           NULL      AS [ID_CreatedBy],
                           NULL      AS [ID_LastModifiedBy],
                           GETDATE() Date) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vBarangayClearance H
            WHERE  H.ID = @ID
        END

      select Name_ModelReport            label,
             Name_Report,
             'ModelReport-' + Oid_Report name
      FROM   vModelReport
      WHERE  ID_Company = @ID_Company
             AND Name_Model = 'BarangayClearance'
  END

GO

CREATE OR
ALTER PROCEDURE pGetCertificateOfIndigency @ID         INT = -1,
                                           @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_'

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL      AS [_],
                           -1        AS [ID],
                           NULL      AS [Code],
                           NULL      AS [Name],
                           1         AS [IsActive],
                           NULL      AS [ID_Company],
                           NULL      AS [Comment],
                           NULL      AS [DateCreated],
                           NULL      AS [DateModified],
                           NULL      AS [ID_CreatedBy],
                           NULL      AS [ID_LastModifiedBy],
                           GETDATE() Date) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vCertificateOfIndigency H
            WHERE  H.ID = @ID
        END
  END

GO

GO

CREATE OR
ALTER PROCEDURE pGetCertificateOfResidency @ID         INT = -1,
                                           @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_'

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL      AS [_],
                           -1        AS [ID],
                           NULL      AS [Code],
                           NULL      AS [Name],
                           1         AS [IsActive],
                           NULL      AS [ID_Company],
                           NULL      AS [Comment],
                           NULL      AS [DateCreated],
                           NULL      AS [DateModified],
                           NULL      AS [ID_CreatedBy],
                           NULL      AS [ID_LastModifiedBy],
                           GETDATE() Date) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vCertificateOfResidency H
            WHERE  H.ID = @ID
        END
  END

GO

CREATE OR
ALTER PROC pAddModelReport(@ID_Company INT,
                           @ModelName  Varchar(200),
                           @ReportName VARCHAR(200),
                           @labelName  VARCHAR(200))
AS
  BEGIN
      DECLARE @Oid_Model VARCHAR(MAX)
      DECLARE @Oid_Report VARCHAR(MAX)
      DECLARE @Count_Validation INT = 0
      DECLARE @msg VARCHAR(MAX) = ''

      SELECT @Oid_Model = Oid
      FROM   _tModel
      WHERE  Name = @ModelName

      SELECT @Oid_Report = Oid
      FROM   _tReport
      WHERE  Name = @ReportName

      /* Check if Model is not exist */
      SELECT @Count_Validation = COUNT(*)
      FROM   _tModel
      WHERE  Oid = @Oid_Model;

      IF( @Count_Validation = 0 )
        BEGIN
            SET @msg = @ModelName + ' is not exist.';

            THROW 51000, @msg, 1;
        END

      /* Check if Report is not exist */
      SELECT @Count_Validation = COUNT(*)
      FROM   _tReport
      WHERE  Oid = @Oid_Report;

      IF( @Count_Validation = 0 )
        BEGIN
            SET @msg = @ReportName + ' is not exist.';

            THROW 51000, @msg, 1;
        END

      /* Check if Model and Report is already exist */
      SELECT @Count_Validation = COUNT(*)
      FROM   [_tModelReport]
      WHERE  Oid_Report = @Oid_Report
             AND Oid_Model = @Oid_Model
             AND ID_Company = @ID_Company;

      IF( @Count_Validation > 0 )
        BEGIN
            SET @msg = @ModelName + ' ' + @ReportName
                       + ' is already exist.';

            THROW 51000, @msg, 1;
        END

      INSERT INTO [dbo].[_tModelReport]
                  ([Name],
                   [IsActive],
                   [ID_Company],
                   DateCreated,
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [Oid_Model],
                   [Oid_Report])
      VALUES      (@labelName,
                   1,
                   @ID_Company,
                   GETDATE(),
                   1,
                   1,
                   @Oid_Model,
                   @Oid_Report)
  END

GO

GO

CREATE OR
ALTER PROCEDURE pGetResident @ID         INT = -1,
                             @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS Resident_Program,
             '' AS Resident_Family,
             '' AS Resident_MedicalRecord

      DECLARE @ID_User INT
      DECLARE @ID_Company INT
      DECLARE @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse,
             @ID_Company = ID_Company
      FROM   tUserSession _session
             INNER JOIN vUSer _user
                     on _session.ID_User = _user.ID
      WHERE  _session.ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL AS [_],
                           -1   AS [ID],
                           NULL AS [Code],
                           NULL AS [Name],
                           1    AS [IsActive],
                           NULL AS [ID_Company],
                           NULL AS [Comment],
                           NULL AS [DateCreated],
                           NULL AS [DateModified],
                           NULL AS [ID_CreatedBy],
                           NULL AS [ID_LastModifiedBy]) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tCompany company
                          ON company.ID = h.ID_Company
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vResident H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   vResident_Program
      WHERE  ID_Resident = @ID

      SELECT *
      FROM   vResident_Family
      WHERE  ID_Resident = @ID

      SELECT *
      FROM   vResident_MedicalRecord
      WHERE  ID_Resident = @ID
      Order  by ID DESC
  END

GO

IF NOT EXISTS (SELECT 1
               FROM   _tReport
               WHERE  Name = 'BarangayClearanceFormReport')
  BEGIN
      exec _pCreateReportView
        'BarangayClearanceFormReport',
        1
  END

GO

GO

IF NOT EXISTS (SELECT 1
               FROM   _tReport
               WHERE  Name = 'BarangayClearanceFormReport-CityOfLipaBatangas-BrgyTambo')
  BEGIN
      exec _pCreateReportView
        'BarangayClearanceFormReport-CityOfLipaBatangas-BrgyTambo',
        1
  END

GO

IF NOT EXISTS (SELECT 1
               FROM   _tReport
               WHERE  Name = 'CertificationIndigencyFormReport')
  BEGIN
      exec _pCreateReportView
        'CertificationIndigencyFormReport',
        1
  END

GO

IF NOT EXISTS (SELECT 1
               FROM   _tReport
               WHERE  Name = 'CertificationResidencyFormReport')
  BEGIN
      exec _pCreateReportView
        'CertificationResidencyFormReport',
        1
  END

GO

exec _pRefreshAllViews

GO

DECLARE @Not_Guids_Company TABLE
  (
     Guid VARCHAR(MAX)
  )

INSERT @Not_Guids_Company
VALUES('66A2A296-7EBD-4A81-960E-D435C9D22DF7')

DECLARE @ID_Company INT
DECLARE @Name_Companhy VARCHAR(MAX) = ''
DECLARE db_cursor CURSOR FOR
  SELECT ID,
         Name
  FROM   vCompanyActive
  WHERE  Guid NOT IN (SELECT Guid
                      FROM   @Not_Guids_Company)

OPEN db_cursor

FETCH NEXT FROM db_cursor INTO @ID_Company, @Name_Companhy

WHILE @@FETCH_STATUS = 0
  BEGIN
      if(SELECT COUNT(*)
         FROM   vModelReport
         WHERE  ID_Company = @ID_Company
                AND Name_Model = 'BarangayClearance') = 0
        BEGIN
            exec pAddModelReport
              @ID_Company,
              'BarangayClearance',
              'BarangayClearanceFormReport',
              'Barangay Clearance'
        END

      FETCH NEXT FROM db_cursor INTO @ID_Company, @Name_Companhy
  END

CLOSE db_cursor

DEALLOCATE db_cursor

GO 
