GO

IF COL_LENGTH('_tReport', 'Caption') IS NULL
  BEGIN
      exec _pAddModelProperty
        '_tReport',
        'Caption',
        1
  END

GO

IF COL_LENGTH('tCompany', 'BarangayListSideReportHTMLString') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tCompany',
        'BarangayListSideReportHTMLString',
        1;

      ALTER TABLE tCompany
        ALTER COLUMN BarangayListSideReportHTMLString VARCHAR(MAX);
  END

GO

IF COL_LENGTH('tCompany', 'BarangayCaptainName') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tCompany',
        'BarangayCaptainName',
        1;
  END

GO

exec _pRefreshAllViews

go

CREATE OR
ALTER VIEW vMunicipalityCompany
AS
  SELECT *
  FROM   vCompany
  WHERE  ID = 1

GO

CREATE OR
ALTER FUNCTION [dbo].[fGetBarangayListSideReportHTMLString]()
RETURNS VARCHAR(MAX)
AS
  BEGIN
      DECLARE @result VARCHAR(MAX) = ''

      /***********************************************/
      SET @result = @result + '<b>_____________________</b>'
                    + '<br/>'
      SET @result = @result + '<b>Punong Barangay</b>' + '<br/>'
                    + '<br/>'
      /***********************************************/
      SET @result = @result + '<b>_____________________</b>'
                    + '<br/>'
      SET @result = @result + '<b>Position</b>' + '<br/>'
      SET @result = @result + 'Department' + '<br/>' + '<br/>'

      /***********************************************/
      RETURN @result
  END

GO

ALTER PROCEDURE [dbo].[_pCreateReportView] @ReportName   VARCHAR(300),
                                           @Caption      VARCHAR(300),
                                           @IsNavigation BIT = 1
AS
    DECLARE @Oid_Report UNIQUEIDENTIFIER= NEWID();

    INSERT INTO dbo._tReport
                (Oid,
                 Code,
                 Name,
                 IsActive,
                 Comment,
                 DateCreated,
                 DateModified,
                 ID_CreatedBy,
                 ID_LastModifiedBy,
                 ReportPath,
                 Caption)
    VALUES      (@Oid_Report,-- Oid - uniqueidentifier  
                 NULL,-- Code - varchar  
                 @ReportName,-- Name - varchar  
                 1,-- IsActive - bit  
                 NULL,-- Comment - varchar  
                 GETDATE(),-- DateCreated - datetime  
                 GETDATE(),-- DateModified - datetime  
                 1,-- ID_CreatedBy - int  
                 NULL,-- ID_LastModifiedBy - int  
                 @ReportName,-- ReportPath - varchar  ,
                 @Caption );

    DECLARE @Oid_View UNIQUEIDENTIFIER= NEWID();

    INSERT INTO dbo._tView
                (Oid,
                 Code,
                 Name,
                 IsActive,
                 ID_ListView,
                 ID_Report,
                 ID_Dashboard,
                 ID_ViewType,
                 CustomViewPath,
                 ControllerPath,
                 Comment,
                 DateCreated,
                 ID_CreatedBy,
                 ID_LastModifiedBy,
                 DateModified,
                 ID_Model,
                 DataSource)
    VALUES      (@Oid_View,-- Oid - uniqueidentifier  
                 NULL,-- Code - varchar  
                 @ReportName + '_ReportView',-- Name - varchar  
                 1,-- IsActive - bit  
                 NULL,-- ID_ListView - uniqueidentifier  
                 @Oid_Report,-- ID_Report - uniqueidentifier  
                 NULL,-- ID_Dashboard - uniqueidentifier  
                 3,-- ID_ViewType - int  
                 NULL,-- CustomViewPath - varchar  
                 NULL,-- ControllerPath - varchar  
                 NULL,-- Comment - varchar  
                 GETDATE(),-- DateCreated - datetime  
                 1,-- ID_CreatedBy - int  
                 NULL,-- ID_LastModifiedBy - int  
                 GETDATE(),-- DateModified - datetime  
                 NULL,-- ID_Model - uniqueidentifier  
                 NULL -- DataSource - varchar  
    );

    IF( @IsNavigation = 1 )
      BEGIN
          INSERT INTO dbo._tNavigation
                      (Oid,
                       Code,
                       Name,
                       IsActive,
                       Comment,
                       ID_CreatedBy,
                       ID_LastModifiedBy,
                       DateCreated,
                       DateModified,
                       ID_View,
                       Caption,
                       Icon,
                       SeqNo,
                       ID_Parent)
          VALUES      (NEWID(),-- Oid - uniqueidentifier  
                       NULL,-- Code - varchar  
                       @ReportName + '_Navigation',-- Name - varchar  
                       1,-- IsActive - bit  
                       NULL,-- Comment - varchar  
                       1,-- ID_CreatedBy - int  
                       NULL,-- ID_LastModifiedBy - int  
                       GETDATE(),-- DateCreated - datetime  
                       GETDATE(),-- DateModified - datetime  
                       @Oid_View,-- ID_View - uniqueidentifier  
                       @ReportName,-- Caption - varchar  
                       NULL,-- Icon - varchar  
                       NULL,-- SeqNo - int  
                       '4E94C28D-7955-4726-8676-65B06A142876' -- ID_Parent - uniqueidentifier  
          );
      END;

GO

CREATE    OR
ALTER VIEW [dbo].vzResidentBarangayClearance
AS
  SELECT Resident.Name                              Name,
         Resident.ID                                ID,
         Resident.Address                           Address_Resident,
         municipality.Name                          Name_MunicipalityCompany,
         municipality.ImageLogoLocationFilenamePath ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                       Address_MunicipalityCompany,
         company.ID                                 ID_Company,
         company.ImageLogoLocationFilenamePath      ImageLogoLocationFilenamePath,
         company.Name                               Name_Company,
         company.Address                            Address_Company,
         company.ContactNumber                      ContactNumber_Company,
         company.BarangayListSideReportHTMLString   BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                BarangayCaptainName_Company
  FROM   tResident Resident
         INNER JOIN vCompanyActive company
                 ON company.ID = Resident.ID_Company
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = Resident.ID_Company

go

CREATE    OR
ALTER VIEW [dbo].vzResidentFormReport
AS
  SELECT Resident.Name                              Name,
         Resident.ID                                ID,
         Resident.Address                           Address_Resident,
         municipality.Name                          Name_MunicipalityCompany,
         municipality.ImageLogoLocationFilenamePath ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                       Address_MunicipalityCompany,
         company.ID                                 ID_Company,
         company.ImageLogoLocationFilenamePath      ImageLogoLocationFilenamePath,
         company.Name                               Name_Company,
         company.Address                            Address_Company,
         company.ContactNumber                      ContactNumber_Company,
         company.BarangayListSideReportHTMLString   BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                BarangayCaptainName_Company
  FROM   tResident Resident
         INNER JOIN vCompanyActive company
                 ON company.ID = Resident.ID_Company
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = Resident.ID_Company

go

CREATE    OR
ALTER VIEW [dbo].vzResidentClearanceToOperate
AS
  SELECT Resident.Name                              Name,
         Resident.ID                                ID,
         Resident.Address                           Address_Resident,
         municipality.Name                          Name_MunicipalityCompany,
         municipality.ImageLogoLocationFilenamePath ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                       Address_MunicipalityCompany,
         company.ID                                 ID_Company,
         company.ImageLogoLocationFilenamePath      ImageLogoLocationFilenamePath,
         company.Name                               Name_Company,
         company.Address                            Address_Company,
         company.ContactNumber                      ContactNumber_Company,
         company.BarangayListSideReportHTMLString   BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                BarangayCaptainName_Company
  FROM   tResident Resident
         INNER JOIN vCompanyActive company
                 ON company.ID = Resident.ID_Company
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = Resident.ID_Company

go

CREATE OR
ALTER VIEW vResidentDetailReportList
AS
  SELECT ROW_NUMBER()
           OVER(
             ORDER BY Oid ASC)                       ID,
         ISNULL(Caption, Name)                       label,
         'far fa-file'                               icon,
         'text-primary'                              class,
         1                                           visible,
         Name                                        reportname,
         'ModelReport-' + Convert(VARCHAR(MAX), Oid) name
  FROm   _tReport
  where  Name IN ( 'BarangayClearance', 'CLEARANCETOOPERATE', 'Pagpapatunay', 'CertificationIndegency',
                   'CertificationOfResidency', 'ClearanceToConstruct' )

GO

ALTER PROCEDURE pGetResident @ID         INT = -1,
                             @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS Resident_Program,
             '' AS Reports

      DECLARE @ID_User INT
      DECLARE @ID_Company INT
      DECLARE @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse,
             @ID_Company = ID_Company
      FROM   tUserSession _session
             INNER JOIN vUSer _user
                     on _session.ID_User = _user.ID
      WHERE  _session.ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL AS [_],
                           -1   AS [ID],
                           NULL AS [Code],
                           NULL AS [Name],
                           1    AS [IsActive],
                           NULL AS [ID_Company],
                           NULL AS [Comment],
                           NULL AS [DateCreated],
                           NULL AS [DateModified],
                           NULL AS [ID_CreatedBy],
                           NULL AS [ID_LastModifiedBy]) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tCompany company
                          ON company.ID = h.ID_Company
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vResident H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   vResident_Program
      WHERE  ID_Resident = @ID

      SELECT *
      FROm   vResidentDetailReportList
      order  by label
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'BarangayClearance') = 0
  BEGIN
      exec _pCreateReportView
        'BarangayClearance',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CLEARANCETOOPERATE') = 0
  BEGIN
      exec _pCreateReportView
        'CLEARANCETOOPERATE',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'Pagpapatunay') = 0
  BEGIN
      exec _pCreateReportView
        'Pagpapatunay',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificationIndegency') = 0
  BEGIN
      exec _pCreateReportView
        'CertificationIndegency',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificationOfResidency') = 0
  BEGIN
      exec _pCreateReportView
        'CertificationOfResidency',
        'Certification Of Residency',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'ClearanceToConstruct') = 0
  BEGIN
      exec _pCreateReportView
        'ClearanceToConstruct',
        'Clearance To Construct',
        0
  END

GO

Update _tReport
SET    Caption = 'Certification Of Residency'
WHERE  Name = 'CertificationOfResidency'

Update _tReport
SET    Caption = 'Certification Indegency'
WHERE  Name = 'CertificationIndegency'

Update _tReport
SET    Caption = 'Pagpapatunay'
WHERE  Name = 'Pagpapatunay'

Update _tReport
SET    Caption = 'Clearance To Operate'
WHERE  Name = 'CLEARANCETOOPERATE'

Update _tReport
SET    Caption = 'Barangay Clearance'
WHERE  Name = 'BarangayClearance'

Update tCompany
SET    BarangayListSideReportHTMLString = ''
WHERE  BarangayListSideReportHTMLString IS NULL

GO

UPDATE tCompany
SET    BarangayListSideReportHTMLString =
'<b>Hon. NOVILITO M. MANALO</b><br /> <b>Punong Barangay</b><br /> <br /> <br /> <b>Hon. ROLDAN A. ROSITA</b><br /> <b>Councilor</b><br /> Comm. on Health Social Welfare Services<br /> <br /> <b>Hon. ALEX V. TENORIO</b><br /> <b>Councilor</b><br /> Comm. on Public Information, Transient helpers &amp; Visitors<br /> <br /> <b>Hon. ROGER D.V CANOSA SR</b><br /> <b>Councilor</b><br /> Comm. on Peace in Order &amp; Human Rights<br /> <br /> <b>Hon. ROSENDO T. BABADILLA</b><br /> <b>Councilor</b><br /> Comm. on Education &amp; Communication<br /> <br /> <b>Hon. EMILY L. ORENSE</b><br /> <b>Councilor</b><br /> Comm. on Women &amp; Family<br /> <br /> <b>Hon. CELSO M. PUNZALAN</b><br /> <b>Councilor</b><br /> Comm. on Finance Ways &amp; Means<br /> <br /> <b>Hon. EDUARDO O. SOLIS</b><br /> <b>Councilor</b><br /> Comm. on Public Works &amp; Environmental Services<br /> <br /> <b>Hon. DIETHER C. MARALIT</b><br /> <b>SK Chairman</b><br /> Comm. on Youth &amp; Sport &amp; Culture<br /> <br /> <b>JODEE-LYN V. CACAO</b><br /> <b>Secretary</b> '
WHERE  Name = 'Brgy. Lumang Lipa'

UPDATE tCompany
SET    BarangayCaptainName = 'Hon. NOVILITO M. MANALO'
WHERE  Name = 'Brgy. Lumang Lipa' 
