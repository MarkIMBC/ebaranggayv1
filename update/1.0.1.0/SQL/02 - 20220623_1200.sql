GO

ALTER PROC pGetDashboardBarangaySummary @ID_UserSession INT = NULL
as
  BEGIN
      DECLARE @ID_User    INT,
              @ID_Company INT
      DECLARE @ResidentCount INT = 0
      DECLARE @PermanentResidentCount INT = 0
      DECLARE @MigrateResidentCount INT = 0
      DECLARE @TrancientResidentCount INT = 0
      DECLARE @HomeOwnershipStatusOwnerCount INT = 0
      DECLARE @EducationalLevelGraduatedCount INT = 0
      DECLARE @TotalResidentMaleCount INT = 0
      DECLARE @TotalResidentFemaleCount INT = 0
      DECLARE @ResidentCountAge60Above INT = 0
      DECLARE @ResidentCountAge18to59 INT = 0
      DECLARE @ResidentCountAge10to17 INT = 0
      DECLARE @ResidentCountAge5to9 INT = 0
      DECLARE @ResidentCountAge24to84months INT = 0
      DECLARE @ResidentCountAge12to23months INT = 0
      DECLARE @ResidentCountAge0to11months INT = 0
      DECLARE @ResidentCountMale INT = 0
      DECLARE @ResidentCountFemale INT = 0
      DECLARE @ResidentCount4Ps INT = 0
      DECLARE @ResidentCountSeniorCitizen INT = 0
      DECLARE @ResidentCountScholar INT = 0
      DECLARE @MayAriNgBahayAtLupaID_HomeOwnershipStatus INT = 1
      DECLARE @GraduateSchool_ID_EducationalLevel INT = 6
      DECLARE @Male_ID_Gender INT = 1
      DECLARE @Female_ID_Gender INT = 2

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      SELECT @ResidentCount = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1

      SELECT @PermanentResidentCount = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             AND IsPermanent = 1

      SELECT @MigrateResidentCount = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             AND IsMigrante = 1

      SELECT @TrancientResidentCount = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             AND IsTransient = 1

      SELECT @HomeOwnershipStatusOwnerCount = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             AND IsTransient = 1
             ANd ID_HomeOwnershipStatus = @MayAriNgBahayAtLupaID_HomeOwnershipStatus

      SELECT @EducationalLevelGraduatedCount = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             ANd ID_EducationalLevel = @GraduateSchool_ID_EducationalLevel

      SELECT @ResidentCountMale = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             ANd ID_Gender = @Male_ID_Gender

      SELECT @ResidentCountFemale = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             ANd ID_Gender = @Female_ID_Gender

      SELECT @ResidentCountAge60Above = COUNT(*)
      FROm   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) >= 60

      SELECT @ResidentCountAge18to59 = COUNT(*)
      FROm   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) BETWEEN 18 AND 59

      SELECT @ResidentCountAge10to17 = COUNT(*)
      FROm   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) BETWEEN 10 AND 17

      SELECT @ResidentCountAge5to9 = COUNT(*)
      FROm   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) BETWEEN 5 AND 9

      SELECT @ResidentCountAge24to84months = COUNT(*)
      FROm   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             and DateBirth IS NOT NULL
             AND DATEDIFF(MONTH, DateBirth, GETDATE()) BETWEEN 24 AND 84

      SELECT @ResidentCountAge12to23months = COUNT(*)
      FROm   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             and DateBirth IS NOT NULL
             AND DATEDIFF(MONTH, DateBirth, GETDATE()) BETWEEN 12 AND 23

      SELECT @ResidentCountAge0to11months = COUNT(*)
      FROm   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             and DateBirth IS NOT NULL
             AND DATEDIFF(MONTH, DateBirth, GETDATE()) BETWEEN 0 AND 11

      SELECT @ResidentCount4Ps = COUNT(*)
      FROM   (SELECT DISTINCT resident.ID
              FROM   tResident resident
                     inner join tResident_Program residentprogram
                             on resident.ID = residentprogram.ID_Resident
                     Inner join tProgram program
                             on residentprogram.ID_Program = program.ID
              where  resident.ID_Company = @ID_Company
                     and resident.IsActive = 1
                     AND program.Name IN ( '4Ps' )
              GROUP  BY resident.ID) tbl

      SELECT @ResidentCountSeniorCitizen = COUNT(*)
      FROM   (SELECT DISTINCT resident.ID
              FROM   tResident resident
                     inner join tResident_Program residentprogram
                             on resident.ID = residentprogram.ID_Resident
                     Inner join tProgram program
                             on residentprogram.ID_Program = program.ID
              where  resident.ID_Company = @ID_Company
                     and resident.IsActive = 1
                     AND program.Name IN ( 'Senior Citizen')
              GROUP  BY resident.ID) tbl

      SELECT @ResidentCountScholar = COUNT(*)
      FROM   (SELECT DISTINCT resident.ID
              FROM   tResident resident
                     inner join tResident_Program residentprogram
                             on resident.ID = residentprogram.ID_Resident
                     Inner join tProgram program
                             on residentprogram.ID_Program = program.ID
              where  resident.ID_Company = @ID_Company
                     and resident.IsActive = 1
                     AND program.Name IN ( 'Scholarship' )
              GROUP  BY resident.ID) tbl

      SELECT '_'

      SELECT ISNULL(@ResidentCount, 0)                  ResidentCount,
             ISNULL(@PermanentResidentCount, 0)         PermanentResidentCount,
             ISNULL(@MigrateResidentCount, 0)           MigrateResidentCount,
             ISNULL(@TrancientResidentCount, 0)         TrancientResidentCount,
             ISNULL(@HomeOwnershipStatusOwnerCount, 0)  HomeOwnershipStatusOwnerCount,
             ISNULL(@EducationalLevelGraduatedCount, 0) EducationalLevelGraduatedCount,
             ISNULL(@ResidentCountAge60Above, 0)        ResidentCountAge60Above,
             ISNULL(@ResidentCountAge18to59, 0)         ResidentCountAge18to59,
             ISNULL(@ResidentCountAge10to17, 0)         ResidentCountAge10to17,
             ISNULL(@ResidentCountAge5to9, 0)           ResidentCountAge5to9,
             ISNULL(@ResidentCountAge24to84months, 0)   ResidentCountAge24to84months,
             ISNULL(@ResidentCountAge12to23months, 0)   ResidentCountAge12to23months,
             ISNULL(@ResidentCountAge0to11months, 0)    ResidentCountAge0to11months,
             ISNULL(@ResidentCountMale, 0)              ResidentCountMale,
             ISNULL(@ResidentCountFemale, 0)            ResidentCountFemale,
             ISNULL(@ResidentCount4Ps, 0)               ResidentCount4Ps,
             ISNULL(@ResidentCountSeniorCitizen, 0)     ResidentCountSeniorCitizen,
             ISNULL(@ResidentCountScholar, 0)           ResidentCountScholar
  END

GO

ALTER PROC dbo.pGetDashboardBarangayMonthlyClientsDataSource (@ID_UserSession INT,
                                                              @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      DECLARE @ClientDailyCount INT = 0
      DECLARE @ClientMonthlyCount INT = 0
      DECLARE @SalesDailyAmount DECIMAL(18, 2) = 0
      DECLARE @SalesMonthlyAmount DECIMAL(18, 2) = 0

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear                          DateYear,
             'Mga Bagong Resident ' + @DateYear Label

      SELECT SUM(CASE datepart(month, DateCreated)
                   WHEN 1 THEN 1
                   ELSE 0
                 END) AS 'January',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 2 THEN 1
                   ELSE 0
                 END) AS 'February',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 3 THEN 1
                   ELSE 0
                 END) AS 'March',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 4 THEN 1
                   ELSE 0
                 END) AS 'April',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 5 THEN 1
                   ELSE 0
                 END) AS 'May',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 6 THEN 1
                   ELSE 0
                 END) AS 'June',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 7 THEN 1
                   ELSE 0
                 END) AS 'July',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 8 THEN 1
                   ELSE 0
                 END) AS 'August',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 9 THEN 1
                   ELSE 0
                 END) AS 'September',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 10 THEN 1
                   ELSE 0
                 END) AS 'October',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 11 THEN 1
                   ELSE 0
                 END) AS 'November',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 12 THEN 1
                   ELSE 0
                 END) AS 'December',
             SUM(CASE datepart(year, DateCreated)
                   WHEN 2012 THEN 1
                   ELSE 0
                 END) AS 'TOTAL',
             ID_Company
      FROM   tResident
      WHERE  IsActive = 1
             AND YEAR(DateCreated) = @DateYear
             AND ID_Company = @ID_Company
      GROUP  BY ID_Company
  END

GO

exec pGetDashboardBarangaySummary
  61099

GO

SELECT ID_User,
       ID_Company
FROM   tUserSession _usersession
       inner join vUser _user
               on _usersession.ID_User = _user.ID
WHERE  _usersession.ID = 61099

SELECT COUNT(*)
FROm   tResident
where  IsActive = 1
       and DateBirth IS NOT NULL
       AND ID_Company = 246
       and DATEDIFF(year, DateBirth, GETDATE()) >= 60

SELECT TOP 5 *
FROM   tUserSession
ORDER  BY ID DESC

SELECT *
FROM   tResident
WHERE  ID_Gender = 1
       AND ID_Company = 248 
