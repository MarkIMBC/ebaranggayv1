GO

IF COL_LENGTH('tCivilStatus', 'TranslatedEngName') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tCivilStatus',
        'TranslatedEngName',
        1
  END

GO

IF COL_LENGTH('tCompany', 'BarangayCaptain_ID_Employee') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tCompany',
        'BarangayCaptain_ID_Employee',
        2
  END

GO

IF COL_LENGTH('tEmployee', 'ImageSignitureSpecimen') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tEmployee',
        'ImageSignitureSpecimen',
        1
  END

GO

GO

CREATE OR
ALTER FUNCTION [dbo].[fGetYearAge] (@dateBirth          DATETIME,
                                    @dateDateStart      DateTime = NULL,
                                    @InvalidDateCaption VARCHAR(MAX) = NULL)
RETURNS VARCHAR(MAX)
AS
  BEGIN
      DECLARE @result VARCHAR(MAX) = ''
      DECLARE @date    DATETIME,
              @tmpdate DATETIME,
              @years   INT,
              @months  INT,
              @days    INT

      IF @dateBirth IS NULL
        RETURN ''

      IF @dateDateStart IS NULL
        SET @dateDateStart = getdate()

      SELECT @date = @dateBirth

      SELECT @tmpdate = @date

      SELECT @years = DATEDIFF(yy, @tmpdate, @dateDateStart) - CASE
                                                                 WHEN ( MONTH(@date) > MONTH(@dateDateStart) )
                                                                       OR ( MONTH(@date) = MONTH(@dateDateStart)
                                                                            AND DAY(@date) > DAY(@dateDateStart) ) THEN 1
                                                                 ELSE 0
                                                               END

      SELECT @tmpdate = DATEADD(yy, @years, @tmpdate)

      SELECT @months = DATEDIFF(m, @tmpdate, @dateDateStart) - CASE
                                                                 WHEN DAY(@date) > DAY(@dateDateStart) THEN 1
                                                                 ELSE 0
                                                               END

      SELECT @tmpdate = DATEADD(m, @months, @tmpdate)

      SELECT @days = DATEDIFF(d, @tmpdate, @dateDateStart)

      IF ( @years > 0 )
        SET @result = CONVERT(VARCHAR(MAX), @years) + ' year'
                      + CASE
                          WHEN @years > 1 THEN 's'
                          ELSE ''
                        END

      if( LEN(@result) = 0 )
        set @result = @InvalidDateCaption
      ELSE
        set @result = @result + ' old'

      RETURN @result
  END

GO

CREATE OR
ALTER VIEW vResidentDetailReportList
AS
  SELECT ROW_NUMBER()
           OVER(
             ORDER BY DateCreated ASC)               ID,
         ISNULL(Caption, Name)                       label,
         'far fa-file'                               icon,
         'text-primary'                              class,
         1                                           visible,
         Name                                        reportname,
         'ModelReport-' + Convert(VARCHAR(MAX), Oid) name
  FROm   _tReport
  where  Name IN ( 'BarangayClearance', 'CLEARANCETOOPERATE', 'Pagpapatunay', 'CertificationIndegency',
                   'CertificationOfResidency', 'ClearanceToConstruct', 'CertificationForWorkandFamily', 'CertificationForNonIncome',
                   'CertificationOfEarnings', 'CerificationAgriculturalLand', 'CertificationOfCovidFreeCase', 'PagpapatunayNaNakauwiNaSaKanilangBayan',
                   'PagpapatunaySoloParent', 'Certificationof14Quarantine', 'CertificationofPAFExamination', 'CertificationofNoOffenseInKatarungangPambarangay',
                   'CertificationOfNoRecordForBHERTS', 'CertificationOfNonEmployed', 'PagpapatunayNgEvacuate', 'PagpapatunayNgHindiMakakakuhaNgSAP',
                   'CertificationOfResidency_HeadOfFamily', 'CertificationOfResidency_Permanent', 'CertificationOfPatricipationOfBusiness', 'CertificationOfNonResident',
                   'CertificationOfDeath', 'CertificationOfSeller', 'CertificationOfRTPCTestRequest', 'CertificateToTravel',
                   'CertificateToGoBackToPreviousResidence', 'CertificateOfInclusion_BHERTS', 'CertificateOfExclusion_CovidInvestigation', 'CertificateOfAcceptance',
                   'HealthCertificate', 'QuarantinePassCertification', 'CertificationOfIsolation', 'CertificateOf7DayQuarantine',
                   'CertificationOfEffectOfLockdown', 'CertificateOf14DayQuarantineCompletion', 'CertificateOfTravelDueToExam', 'ListOfExclusionNonCovidResidents',
                   'BARCCertificate', 'TanggapanNgBARCChairman', 'CertificateOfGoodMoral', 'CertificateOfSustainableLivelihoodProgram' )

GO

ALTER VIEW [dbo].[vCompany]
AS
  SELECT H.*,
         UC.Name                           AS CreatedBy,
         UM.Name                           AS LastModifiedBy,
         dbo.fGetAPILink() + '/Content/Image/'
         + H.ImageHeaderFilename           ImageHeaderLocationFilenamePath,
         dbo.fGetAPILink() + '/Content/Thumbnail/'
         + H.ImageHeaderFilename           ImageHeaderThumbNameLocationFilenamePath,
         dbo.fGetAPILink() + '/Content/Image/'
         + H.ImageLogoFilename             ImageLogoLocationFilenamePath,
         dbo.fGetAPILink() + '/Content/Thumbnail/'
         + H.ImageLogoFilename             ImageLogoThumbNameLocationFilenamePath,
         dbo.fGetAPILink() + '/' + h.Code + '/'
         + h.ReceptionPortalGuid
         + '/BaranggayRegistrationPortal'  BarangayPortalLink,
         employee.Name                     BarangayCaptain_Name_Employee,
         dbo.fGetAPILink() + '/Content/Thumbnail/'
         + employee.ImageSignitureSpecimen BarangayCaptain_ImageSignitureSpecimen_Employee
  FROM   tCompany H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN vEmployee employee
                ON H.BarangayCaptain_ID_Employee = employee.ID

GO

CREATE    OR
ALTER VIEW [dbo].vzResidentFormReport
AS
  SELECT Resident.Name                                           Name,
         Resident.ID                                             ID,
         Resident.Address                                        Address_Resident,
         ISNULL(civilStatus.TranslatedEngName, '')               TranslatedEngName_CivilStatus,
         ISNULL(FORMAT(DateBirth, 'MMMM dd, yyyy'), '')          FormatBirthDate_Resident,
         dbo.fGetYearAge(DateBirth, GETDATE(), '')               Age_Resident,
         company.ID                                              ID_Company,
         company.ImageLogoLocationFilenamePath                   ImageLogoLocationFilenamePath,
         company.Name                                            Name_Company,
         company.Address                                         Address_Company,
         company.ContactNumber                                   ContactNumber_Company,
         company.BarangayListSideReportHTMLString                BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                             BarangayCaptainName_Company,
         Resident.Name_CivilStatus                               Name_CivilStatus,
         company.BarangayCaptain_Name_Employee                   BarangayCaptain_Name_Employee,
         company.BarangayCaptain_ImageSignitureSpecimen_Employee BarangayCaptain_ImageSignitureSpecimen_Employee,
         municipality.Name                                       Name_MunicipalityCompany,
         municipality.ImageLogoLocationFilenamePath              ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                                    Address_MunicipalityCompany,
         Resident.Name_Gender
  FROM   vResident Resident
         INNER JOIN vCompanyActive company
                 ON company.ID = Resident.ID_Company
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = Resident.ID_Company
         Left JOIN tCivilStatus civilStatus
                on Resident.ID_CivilStatus = civilStatus.ID

go

ALTER PROC dbo.pGetDashboardMunicipalityMonthlyClientsDataSource (@ID_UserSession INT,
                                                                  @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      DECLARE @ClientDailyCount INT = 0
      DECLARE @ClientMonthlyCount INT = 0
      DECLARE @SalesDailyAmount DECIMAL(18, 2) = 0
      DECLARE @SalesMonthlyAmount DECIMAL(18, 2) = 0

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear                          DateYear,
             'Mga Bagong Resident ' + @DateYear Label

      SELECT SUM(CASE datepart(month, resident. DateCreated)
                   WHEN 1 THEN 1
                   ELSE 0
                 END) AS 'January',
             SUM(CASE datepart(month, resident. DateCreated)
                   WHEN 2 THEN 1
                   ELSE 0
                 END) AS 'February',
             SUM(CASE datepart(month, resident. DateCreated)
                   WHEN 3 THEN 1
                   ELSE 0
                 END) AS 'March',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 4 THEN 1
                   ELSE 0
                 END) AS 'April',
             SUM(CASE datepart(month, resident. DateCreated)
                   WHEN 5 THEN 1
                   ELSE 0
                 END) AS 'May',
             SUM(CASE datepart(month, resident. DateCreated)
                   WHEN 6 THEN 1
                   ELSE 0
                 END) AS 'June',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 7 THEN 1
                   ELSE 0
                 END) AS 'July',
             SUM(CASE datepart(month, resident. DateCreated)
                   WHEN 8 THEN 1
                   ELSE 0
                 END) AS 'August',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 9 THEN 1
                   ELSE 0
                 END) AS 'September',
             SUM(CASE datepart(month, resident. DateCreated)
                   WHEN 10 THEN 1
                   ELSE 0
                 END) AS 'October',
             SUM(CASE datepart(month, resident. DateCreated)
                   WHEN 11 THEN 1
                   ELSE 0
                 END) AS 'November',
             SUM(CASE datepart(month, resident. DateCreated)
                   WHEN 12 THEN 1
                   ELSE 0
                 END) AS 'December',
             SUM(CASE datepart(year, resident.DateCreated)
                   WHEN 2012 THEN 1
                   ELSE 0
                 END) AS 'TOTAL'
      FROM   tResident resident
             INNER JOIN vCompanyActive company
                     on resident.ID_Company = company.ID
      WHERE  resident . IsActive = 1
             AND YEAR(resident.DateCreated) = @DateYear
             AND ID_Company > 1
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificationForWorkandFamily') = 0
  BEGIN
      exec _pCreateReportView
        'CertificationForWorkandFamily',
        'Certification (Para sa Trabaho at Pamilya)',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificationForNonIncome') = 0
  BEGIN
      exec _pCreateReportView
        'CertificationForNonIncome',
        'Certification (Para sa Walang Kita/No Income)',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificationOfEarnings') = 0
  BEGIN
      exec _pCreateReportView
        'CertificationOfEarnings',
        'Certification Of Earnings (Para sa Kita)',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CerificationAgriculturalLand') = 0
  BEGIN
      exec _pCreateReportView
        'CerificationAgriculturalLand',
        'Certification (Agricultural Land)',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificationOfCovidFreeCase') = 0
  BEGIN
      exec _pCreateReportView
        'CertificationOfCovidFreeCase',
        'Certification Of Covid Free Case',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'PagpapatunayNaNakauwiNaSaKanilangBayan') = 0
  BEGIN
      exec _pCreateReportView
        'PagpapatunayNaNakauwiNaSaKanilangBayan',
        'Pagpapatunay NaNakauwi Na Sa Kanilang Bayan',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'PagpapatunaySoloParent') = 0
  BEGIN
      exec _pCreateReportView
        'PagpapatunaySoloParent',
        'Pagpapatunay (Solo Parent)',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'Certificationof14Quarantine') = 0
  BEGIN
      exec _pCreateReportView
        'Certificationof14Quarantine',
        'Certification of 14 Quarantine',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificationofPAFExamination') = 0
  BEGIN
      exec _pCreateReportView
        'CertificationofPAFExamination',
        'Certification of PAF Examination',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificationofNoOffenseInKatarungangPambarangay') = 0
  BEGIN
      exec _pCreateReportView
        'CertificationofNoOffenseInKatarungangPambarangay',
        'Certification Of No Offense In KatarungangP ambarangay',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificationOfNoRecordForBHERTS') = 0
  BEGIN
      exec _pCreateReportView
        'CertificationOfNoRecordForBHERTS',
        'Certification Of No Record For BHERTS',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificationOfNonEmployed') = 0
  BEGIN
      exec _pCreateReportView
        'CertificationOfNonEmployed',
        'Certification Of Non Employed',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'PagpapatunayNgEvacuate') = 0
  BEGIN
      exec _pCreateReportView
        'PagpapatunayNgEvacuate',
        'Pagpapatunay Ng Evacuate',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'PagpapatunayNgHindiMakakakuhaNgSAP') = 0
  BEGIN
      exec _pCreateReportView
        'PagpapatunayNgHindiMakakakuhaNgSAP',
        'Pagpapatunay ng Hindi Makakakuha ng SAP',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificationOfResidency_HeadOfFamily') = 0
  BEGIN
      exec _pCreateReportView
        'CertificationOfResidency_HeadOfFamily',
        'Certification Of Residency (Head Of Family)',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificationOfResidency_Permanent') = 0
  BEGIN
      exec _pCreateReportView
        'CertificationOfResidency_Permanent',
        'Certification Of Residency (Permanent)',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificationOfNonResident') = 0
  BEGIN
      exec _pCreateReportView
        'CertificationOfNonResident',
        'CertificationO f Non-Resident',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificationOfPatricipationOfBusiness') = 0
  BEGIN
      exec _pCreateReportView
        'CertificationOfPatricipationOfBusiness',
        'Certification Of Patricipation Of Business)',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificationOfDeath') = 0
  BEGIN
      exec _pCreateReportView
        'CertificationOfDeath',
        'Certification Of Death',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificationOfSeller') = 0
  BEGIN
      exec _pCreateReportView
        'CertificationOfSeller',
        'Certification Of Seller',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificationOfRTPCTestRequest') = 0
  BEGIN
      exec _pCreateReportView
        'CertificationOfRTPCTestRequest',
        'Certification Of RTPC Test Request',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificateToTravel') = 0
  BEGIN
      exec _pCreateReportView
        'CertificateToTravel',
        'Certificate to Travel',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificateToGoBackToPreviousResidence') = 0
  BEGIN
      exec _pCreateReportView
        'CertificateToGoBackToPreviousResidence',
        'Certificate To Go Back To Previous Residence',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificateOfExclusion_CovidInvestigation') = 0
  BEGIN
      exec _pCreateReportView
        'CertificateOfExclusion_CovidInvestigation',
        'Certificate Of Exclusion (Covid/Investigation)',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificateOfInclusion_BHERTS') = 0
  BEGIN
      exec _pCreateReportView
        'CertificateOfInclusion_BHERTS',
        'Certificate Of Inclusion (BHERTS)',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificateOfAcceptance') = 0
  BEGIN
      exec _pCreateReportView
        'CertificateOfAcceptance',
        'Certificate Of Acceptance',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'HealthCertificate') = 0
  BEGIN
      exec _pCreateReportView
        'HealthCertificate',
        'Health Certificate',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'QuarantinePassCertification') = 0
  BEGIN
      exec _pCreateReportView
        'QuarantinePassCertification',
        'Quarantine Pass/Certification',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'QuarantinePassCertification') = 0
  BEGIN
      exec _pCreateReportView
        'QuarantinePassCertification',
        'Quarantine Pass/Certification',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificationOfIsolation') = 0
  BEGIN
      exec _pCreateReportView
        'CertificationOfIsolation',
        'Certification Of Isolation',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificateOf7DayQuarantine') = 0
  BEGIN
      exec _pCreateReportView
        'CertificateOf7DayQuarantine',
        'Certificate Of 7 Day Quarantine',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificationOfEffectOfLockdown') = 0
  BEGIN
      exec _pCreateReportView
        'CertificationOfEffectOfLockdown',
        'Certification Of Effect Of Lockdown',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificateOf14DayQuarantineCompletion') = 0
  BEGIN
      exec _pCreateReportView
        'CertificateOf14DayQuarantineCompletion',
        'Certificate Of 14 Day Quarantine Completion',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificateOfTravelDueToExam') = 0
  BEGIN
      exec _pCreateReportView
        'CertificateOfTravelDueToExam',
        'Certificate of Travel Due To Exam',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'ListOfExclusionNonCovidResidents') = 0
  BEGIN
      exec _pCreateReportView
        'ListOfExclusionNonCovidResidents',
        'List Of Exclusion Non-Covid Residents',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'BARCCertificate') = 0
  BEGIN
      exec _pCreateReportView
        'BARCCertificate',
        'BARC Certificate',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'TanggapanNgBARCChairman') = 0
  BEGIN
      exec _pCreateReportView
        'TanggapanNgBARCChairman',
        'Tanggapan ng BARC Chairman',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificateOfGoodMoral') = 0
  BEGIN
      exec _pCreateReportView
        'CertificateOfGoodMoral',
        'Certificate Of Good Moral',
        0
  END

GO

IF(SELECT COUNT(*)
   FROM   _tReport
   WHERE  Name = 'CertificateOfSustainableLivelihoodProgram') = 0
  BEGIN
      exec _pCreateReportView
        'CertificateOfSustainableLivelihoodProgram',
        'Certificate Of Sustainable Livelihood Program',
        0
  END

GO

Update tCivilStatus
SET    TranslatedEngName = 'Single'
WHERE  Name = 'Walang Asawa'

Update tCivilStatus
SET    TranslatedEngName = 'Married'
WHERE  Name = 'Kasal'

Update tCivilStatus
SET    TranslatedEngName = 'Live-in'
WHERE  Name = 'Nagsasama ng hindi kasal'

Update tCivilStatus
SET    TranslatedEngName = 'Separated'
WHERE  Name = 'Hiwalay sa asawa'

Update tCivilStatus
SET    TranslatedEngName = 'Widow'
WHERE  Name = 'Balo' 
