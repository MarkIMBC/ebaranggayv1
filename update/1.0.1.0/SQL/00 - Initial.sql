GO

CREATE    OR
ALTER VIEW vResident
AS
  SELECT H.*,
         UC.Name      AS CreatedBy,
         UM.Name      AS LastModifiedBy,
         gender.Name  Name_Gender,
         company.Name Name_Company
  FROM   tResident H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tGender gender
                ON gender.ID = h.ID_Gender
         LEFT JOIN tCompany company
                ON company.ID = h.ID_Company

GO

ALTER VIEW vResident_ListvIew
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid Guid_Company
  FROM   vResident hed
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company

GO

CREATE OR
ALTER PROC pCreateBarangayLogins(@CompanyName       VARCHAR(MAX),
                                 @CompanyCode       VARCHAR(MAX),
                                 @MaxSMSCountPerDay INT = 25)
AS
  BEGIN
      exec pCreateCompanyLogins
        @CompanyName,
        @CompanyCode,
        @MaxSMSCountPerDay

      Update tCompany
      SET    MainRouteLink = 'Barangay'
      WHERE  Code = @CompanyCode
             AND Name = @CompanyName
  END

GO

GO

IF NOT EXISTS(SELECT 1
              FROM   sys.columns
              WHERE  Name = N'MainRouteLink'
                     AND Object_ID = Object_ID(N'dbo.tCompany'))
  BEGIN
      exec _pAddModelProperty
        'tCompany',
        'MainRouteLink',
        1

      Update tCompany
      SET    MainRouteLink = 'Barangay'
      WHERE  ID > 1

      Update tCompany
      SET    MainRouteLink = 'Municipality'
      WHERE  ID = 1

      Update tCompany
      SET    IsActive = 0
      WHERE  ID > 1

      Update vUser
      SET    IsActive = company.IsActive
      FROm   vUser _user
             inner join tCompany company
                     on _user.ID_Company = company.ID;

      Update tCompany
      set    Name = 'Nasugbu, Province of Batangas'
      where  ID = 1;

      exec pCreateBarangayLogins
        'Barangay Aga',
        'Aga',
        25;

      exec pCreateBarangayLogins
        'Barangay Balaytigui',
        'Balaytigui',
        25

      exec pCreateBarangayLogins
        'Barangay Banilad',
        'Banilad',
        25

      exec pCreateBarangayLogins
        'Barangay 1',
        'brgy1',
        25

      exec pCreateBarangayLogins
        'Barangay 2',
        'brgy2',
        25

      exec pCreateBarangayLogins
        'Barangay 3',
        'brgy3',
        25
  END

GO

exec _pRefreshAllViews

GO

CREATE    OR
ALTER PROCEDURE pGetResident @ID         INT = -1,
                             @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS Resident_Program

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL         AS [_],
                           -1           AS [ID],
                           NULL         AS [Code],
                           NULL         AS [Name],
                           1            AS [IsActive],
                           NULL         AS [ID_Company],
                           NULL         AS [Comment],
                           NULL         AS [DateCreated],
                           NULL         AS [DateModified],
                           NULL         AS [ID_CreatedBy],
                           NULL         AS [ID_LastModifiedBy]) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tCompany company
                          ON company.ID = h.ID_Company
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vResident H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   vResident_Program
      WHERE  ID_Resident = @ID
  END

GO

ALTER PROC pDoUserLogin(@Username VARCHAR(300),
                        @Password VARCHAR(300))
AS
    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';
    DECLARE @ID_User INT = 0
    DECLARE @ID_UserSession INT = 0

    BEGIN TRY
        EXEC pDoUserLogin_validation
          @Username,
          @Password

        SELECT @ID_User = ID
        FROM   tUser
        WHERE  username = @Username
               AND Password = @Password
               AND IsActive = 1

        INSERT INTO [dbo].[tUserSession]
                    ([Code],
                     [Name],
                     [IsActive],
                     [Comment],
                     [ID_User],
                     [ID_Warehouse],
                     [DateCreated])
        VALUES      (NULL,
                     NULL,
                     1,
                     '',
                     @ID_User,
                     1,
                     GetDate())

        SET @ID_UserSession = @@IDENTITY
    END TRY
    BEGIN CATCH
        SET @message = ERROR_MESSAGE();
        SET @Success = 0;
    END CATCH;

    SELECT '_',
           '' CurrentUser;

    SELECT @Success Success,
           @message message;

    SELECT _user.*,
           employee.LastName,
           employee.MiddleName,
           employee.FirstName,
           @ID_UserSession ID_UserSession,
           company.dbusername,
           company.dbpassword,
           company.Code    Code_Company,
           company.MainRouteLink
    FROM   vUser _user
           INNER JOIN temployee employee
                   ON _user.ID_Employee = employee.ID
           INNER JOIN tCompany company
                   ON employee.ID_Company = company.ID
    WHERE  _user.ID = @ID_User

GO
GO

CREATE    OR
ALTER PROC dbo.pGetDashboardMonthlyRegisteredResidentDataSource (@ID_UserSession INT,
                                                                 @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_UserSession;

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      DECLARE @ClientDailyCount INT = 0
      DECLARE @ClientMonthlyCount INT = 0
      DECLARE @SalesDailyAmount DECIMAL(18, 2) = 0
      DECLARE @SalesMonthlyAmount DECIMAL(18, 2) = 0

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear                         DateYear,
             'Monthly New Registered Resident ' + @DateYear Label

      SELECT SUM(CASE datepart(month, DateCreated)
                   WHEN 1 THEN 1
                   ELSE 0
                 END) AS 'January',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 2 THEN 1
                   ELSE 0
                 END) AS 'February',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 3 THEN 1
                   ELSE 0
                 END) AS 'March',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 4 THEN 1
                   ELSE 0
                 END) AS 'April',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 5 THEN 1
                   ELSE 0
                 END) AS 'May',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 6 THEN 1
                   ELSE 0
                 END) AS 'June',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 7 THEN 1
                   ELSE 0
                 END) AS 'July',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 8 THEN 1
                   ELSE 0
                 END) AS 'August',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 9 THEN 1
                   ELSE 0
                 END) AS 'September',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 10 THEN 1
                   ELSE 0
                 END) AS 'October',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 11 THEN 1
                   ELSE 0
                 END) AS 'November',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 12 THEN 1
                   ELSE 0
                 END) AS 'December',
             SUM(CASE datepart(year, DateCreated)
                   WHEN 2012 THEN 1
                   ELSE 0
                 END) AS 'TOTAL',
             ID_Company
      FROM   tResident
      WHERE  IsActive = 1
             AND DateCreated BETWEEN @DateYear + '/01/01' AND @DateYear + '/12/31'
             AND ID_Company > 1
      GROUP  BY ID_Company
  END 
GO
GO

CREATE OR
ALTER PROC dbo.pGetDashboardBarangayRegisteredResidetDataSource (@ID_UserSession INT,
                                                                 @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_UserSession;

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      DECLARE @ClientDailyCount INT = 0
      DECLARE @ClientMonthlyCount INT = 0
      DECLARE @SalesDailyAmount DECIMAL(18, 2) = 0
      DECLARE @SalesMonthlyAmount DECIMAL(18, 2) = 0
      DECLARE @dataSource TABLE
        (
           Name  VARCHAR(MAX),
           Count INT
        )

      INSERT @dataSource
      SELECT TOP 10 resident.Name_Company,
                    Count(resident.ID_Company) as Count
      FROM   vResident resident
      WHERE  resident.ID_Company > 1
             AND YEAR(resident.DateCreated) = @DateYear
      GROUP  BY resident.ID_Company,
                resident.Name_Company
      ORDER  BY Count DESC

      IF (SELECT COUNT(*)
          FROM   @dataSource) = 0
        BEGIN
            INSERT @dataSource
            SELECT 'No Record',
                   0
        END

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear DateYear

      SELECT *
      FROM   @dataSource
  END 
GO

CREATE OR
ALTER PROC pGetBarangayList
AS
  BEGIN
      SELECT '_',
             '' DataSource;

      SELECT GetDate() CureentDate

      SELECT ID,
             Code,
             Name,
             Guid
      FROM   vCompanyActive
      WHERE  ID > 1
  END

GO

exec pGetBarangayList

SELECT *
FROM   vUser

SELECT *
FROM   vCompanyActive

go 
