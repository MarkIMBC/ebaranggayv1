GO

CREATE OR
ALTER FUNCTION [dbo].[fGetCleanedString](@Value VARCHAR(MAX))
RETURNS VARCHAR(MAX)
AS
  BEGIN
      RETURN LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(@Value, '  ', ' '), CHAR(9), ''), CHAR(10), ''), CHAR(13), '')))
  END

GO

IF EXISTS (SELECT *
           FROM   sysobjects
           WHERE  id = object_id(N'[dbo].[pGetDashboardSummary]')
                  and OBJECTPROPERTY(id, N'IsProcedure') = 1)
  BEGIN
      DROP PROCEDURE [dbo].pGetDashboardSummary
  END

GO

GO

IF EXISTS (SELECT *
           FROM   sysobjects
           WHERE  id = object_id(N'[dbo].[pGetBarangayDashboardSummary]')
                  and OBJECTPROPERTY(id, N'IsProcedure') = 1)
  BEGIN
      DROP PROCEDURE [dbo].pGetBarangayDashboardSummary
  END

GO

IF EXISTS (SELECT *
           FROM   sysobjects
           WHERE  id = object_id(N'[dbo].[pGetMonthlyDashboardClientsDataSource]')
                  and OBJECTPROPERTY(id, N'IsProcedure') = 1)
  BEGIN
      DROP PROCEDURE [dbo].pGetMonthlyDashboardClientsDataSource
  END

GO

GO

IF EXISTS (SELECT *
           FROM   sysobjects
           WHERE  id = object_id(N'[dbo].[pGetMonthlyMunicipalityDashboardClientsDataSource]')
                  and OBJECTPROPERTY(id, N'IsProcedure') = 1)
  BEGIN
      DROP PROCEDURE [dbo].pGetMonthlyMunicipalityDashboardClientsDataSource
  END

GO

IF EXISTS (SELECT *
           FROM   sysobjects
           WHERE  id = object_id(N'[dbo].[pGetDashboardResidentProgramDataSource]')
                  and OBJECTPROPERTY(id, N'IsProcedure') = 1)
  BEGIN
      DROP PROCEDURE [dbo].pGetDashboardResidentProgramDataSource
  END

GO

IF EXISTS (SELECT *
           FROM   sysobjects
           WHERE  id = object_id(N'[dbo].[pGetDashboardMunicipalitySummary]')
                  and OBJECTPROPERTY(id, N'IsProcedure') = 1)
  BEGIN
      DROP PROCEDURE [dbo].pGetDashboardMunicipalitySummary
  END

GO

IF EXISTS (SELECT *
           FROM   sysobjects
           WHERE  id = object_id(N'[dbo].[pGetDashboardMonthlyVaccinatedResidentDataSource]')
                  and OBJECTPROPERTY(id, N'IsProcedure') = 1)
  BEGIN
      DROP PROCEDURE [dbo].pGetDashboardMonthlyVaccinatedResidentDataSource
  END

GO

IF EXISTS (SELECT *
           FROM   sysobjects
           WHERE  id = object_id(N'[dbo].[pGetDashboardMonthlyRegisteredResidentDataSource]')
                  and OBJECTPROPERTY(id, N'IsProcedure') = 1)
  BEGIN
      DROP PROCEDURE [dbo].pGetDashboardMonthlyRegisteredResidentDataSource
  END

GO

GO

IF EXISTS (SELECT *
           FROM   sysobjects
           WHERE  id = object_id(N'[dbo].[pGetDashboardMonthlyClientsDataSource]')
                  and OBJECTPROPERTY(id, N'IsProcedure') = 1)
  BEGIN
      DROP PROCEDURE [dbo].pGetDashboardMonthlyClientsDataSource
  END

GO

IF EXISTS (SELECT *
           FROM   sysobjects
           WHERE  id = object_id(N'[dbo].[pGetDashboardBarangayRegisteredResidetDataSource]')
                  and OBJECTPROPERTY(id, N'IsProcedure') = 1)
  BEGIN
      DROP PROCEDURE [dbo].pGetDashboardBarangayRegisteredResidetDataSource
  END

GO

GO

IF EXISTS (SELECT *
           FROM   sysobjects
           WHERE  id = object_id(N'[dbo].[pGetDashboardMonthlyVaccinatedResidentDataSource]')
                  and OBJECTPROPERTY(id, N'IsProcedure') = 1)
  BEGIN
      DROP PROCEDURE [dbo].pGetDashboardMonthlyVaccinatedResidentDataSource
  END

GO

IF EXISTS (SELECT *
           FROM   sysobjects
           WHERE  id = object_id(N'[dbo].[pGetDashboardMonthlyClientsDataSource]')
                  and OBJECTPROPERTY(id, N'IsProcedure') = 1)
  BEGIN
      DROP PROCEDURE [dbo].pGetDashboardMonthlyClientsDataSource
  END

GO

if OBJECT_ID('dbo.tOccupationalStatus') is null
  BEGIN
      exec _pCreateAppModuleWithTable
        'tOccupationalStatus',
        1,
        NULL,
        NULL;

      INSERT INTO [dbo].tOccupationalStatus
                  ([Name],
                   [IsActive],
                   [ID_Company],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy])
      VALUES      ('Wala',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1),
                  ('Mayroon',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1),
                  ('Naghahanap',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1);
  END

GO

if OBJECT_ID('dbo.tHomeOwnershipStatus') is null
  BEGIN
      exec _pCreateAppModuleWithTable
        'tHomeOwnershipStatus',
        1,
        NULL,
        NULL;

      INSERT INTO [dbo].[tHomeOwnershipStatus]
                  ([Name],
                   [IsActive],
                   [ID_Company],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy])
      VALUES      ('May-ari ng Bahay at Lupa',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1),
                  ('Nangungupahan',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1),
                  ('Katiwala',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1),
                  ('Naninirahan ng Walang Titulo',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1);
  END

GO

if OBJECT_ID('dbo.tCivilStatus') is null
  BEGIN
      exec _pCreateAppModuleWithTable
        'tCivilStatus',
        1,
        NULL,
        NULL;

      INSERT INTO [dbo].[tCivilStatus]
                  ([Name],
                   [IsActive],
                   [ID_Company],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy])
      VALUES      ('Walang Asawa',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1),
                  ('Kasal',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1),
                  ('Nagsasama ng hindi kasal',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1),
                  ('Hiwalay sa asawa',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1),
                  ('Balo',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1);
  END

GO

if OBJECT_ID('dbo.tEducationalLevel') is null
  BEGIN
      exec _pCreateAppModuleWithTable
        'tEducationalLevel',
        1,
        NULL,
        NULL;

      INSERT INTO [dbo].tEducationalLevel
                  ([Name],
                   [IsActive],
                   [ID_Company],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy])
      VALUES      ('Walang Edukasyon',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1),
                  ('Day Care',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1),
                  ('Elementarya',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1),
                  ('K to 12',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1),
                  ('Kolehiyo',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1),
                  ('Graduate School',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1);
  END

GO

IF COL_LENGTH('tResident', 'IsPermanent') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResident',
        'IsPermanent',
        4
  END

GO

IF COL_LENGTH('tResident', 'IsMigrante') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResident',
        'IsMigrante',
        4
  END

GO

IF COL_LENGTH('tResident', 'MigrantAddress') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResident',
        'MigrantAddress',
        1
  END

GO

IF COL_LENGTH('tResident', 'ID_CivilStatus') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResident',
        'ID_CivilStatus',
        2
  END

GO

IF COL_LENGTH('tResident', 'ID_EducationalLevel') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResident',
        'ID_EducationalLevel',
        2
  END

GO

IF COL_LENGTH('tResident', 'ID_OccupationalStatus') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResident',
        'ID_OccupationalStatus',
        2
  END

GO

IF COL_LENGTH('tResident', 'Occupation') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResident',
        'Occupation',
        1
  END

GO

IF COL_LENGTH('tResident', 'ID_HomeOwnershipStatus') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResident',
        'ID_HomeOwnershipStatus',
        2
  END

GO

IF COL_LENGTH('tResident', 'IsTransient') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResident',
        'IsTransient',
        4
  END

GO

IF COL_LENGTH('tResidentRequest', 'ID_Resident') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResidentRequest',
        'ID_Resident',
        2
  END

GO

IF COL_LENGTH('tResidentRequest_CommunityTaxCertificate', 'PositionName') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResidentRequest_CommunityTaxCertificate',
        'PositionName',
        1
  END

GO

exec _pAddModelProperty
  'tResident',
  'Lastname',
  1

GO

exec _pAddModelProperty
  'tResident',
  'Firstname',
  1

GO

exec _pAddModelProperty
  'tResident',
  'Middlename',
  1

GO

exec _pAddModelProperty
  'tResident',
  'Suffix',
  1

GO

exec _pAddModelProperty
  'tResident',
  'ImportGuid',
  1

exec _pAddModelProperty
  'tResident',
  'DateBirth',
  5

GO

exec _pRefreshAllViews

exec _pRefreshAllViews

GO

ALTER VIEW vResident
AS
  SELECT H.*,
         UC.Name                  AS CreatedBy,
         UM.Name                  AS LastModifiedBy,
         gender.Name              Name_Gender,
         company.Name             Name_Company,
         civilStatus.Name         Name_CivilStatus,
         educationalLevel.Name    Name_EducationalLevel,
         homeOwnershipStatus.Name Name_HomeOwnershipStatus,
         occupationalStatus.Name  Name_OccupationalStatus
  FROM   tResident H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tGender gender
                ON gender.ID = h.ID_Gender
         LEFT JOIN tCivilStatus civilStatus
                ON civilStatus.ID = h.ID_CivilStatus
         LEFT JOIN tEducationalLevel educationalLevel
                ON educationalLevel.ID = h.ID_EducationalLevel
         LEFT JOIN tHomeOwnershipStatus homeOwnershipStatus
                ON homeOwnershipStatus.ID = h.ID_HomeOwnershipStatus
         LEFT JOIN tOccupationalStatus occupationalStatus
                ON occupationalStatus.ID = h.ID_OccupationalStatus
         LEFT JOIN tCompany company
                ON company.ID = h.ID_Company

GO

ALTER VIEW vResidentRequest_CommunityTaxCertificate
AS
  SELECT H.*,
         ResidentRequest.Name Name_ResidentRequest,
         UC.NAME              AS CreatedBy,
         UM.NAME              AS LastModifiedBy
  FROM   tResidentRequest_CommunityTaxCertificate H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tResidentRequest ResidentRequest
                ON ResidentRequest.ID = h.ID_ResidentRequest

GO

CREATE OR
ALTER PROC pGetDashboardBarangaySummary @ID_UserSession INT = NULL
as
  BEGIN
      DECLARE @ID_User    INT,
              @ID_Company INT
      DECLARE @ResidentCount INT = 0
      DECLARE @PermanentResidentCount INT = 0
      DECLARE @MigrateResidentCount INT = 0
      DECLARE @TrancientResidentCount INT = 0
      DECLARE @HomeOwnershipStatusOwnerCount INT = 0
      DECLARE @EducationalLevelGraduatedCount INT = 0
      DECLARE @TotalResidentMaleCount INT = 0
      DECLARE @TotalResidentFemaleCount INT = 0
      DECLARE @MayAriNgBahayAtLupaID_HomeOwnershipStatus INT = 1
      DECLARE @GraduateSchool_ID_EducationalLevel INT = 6
      DECLARE @Male_ID_Gender INT = 1
      DECLARE @Female_ID_Gender INT = 2

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      SELECT @ResidentCount = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1

      SELECT @PermanentResidentCount = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             AND IsPermanent = 1

      SELECT @MigrateResidentCount = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             AND IsMigrante = 1

      SELECT @TrancientResidentCount = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             AND IsTransient = 1

      SELECT @HomeOwnershipStatusOwnerCount = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             AND IsTransient = 1
             ANd ID_HomeOwnershipStatus = @MayAriNgBahayAtLupaID_HomeOwnershipStatus

      SELECT @EducationalLevelGraduatedCount = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             AND IsTransient = 1
             ANd ID_EducationalLevel = @GraduateSchool_ID_EducationalLevel

      SELECT @TotalResidentMaleCount = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             AND IsTransient = 1
             ANd ID_Gender = @Male_ID_Gender

      SELECT @TotalResidentFemaleCount = COUNT(*)
      FROM   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             AND IsTransient = 1
             ANd ID_Gender = @Female_ID_Gender

      SELECT '_'

      SELECT ISNULL(@ResidentCount, 0)                  ResidentCount,
             ISNULL(@PermanentResidentCount, 0)         PermanentResidentCount,
             ISNULL(@MigrateResidentCount, 0)           MigrateResidentCount,
             ISNULL(@TrancientResidentCount, 0)         TrancientResidentCount,
             ISNULL(@HomeOwnershipStatusOwnerCount, 0)  HomeOwnershipStatusOwnerCount,
             ISNULL(@EducationalLevelGraduatedCount, 0) EducationalLevelGraduatedCount,
             ISNULL(@TotalResidentMaleCount, 0)         TotalResidentMaleCount,
             ISNULL(@TotalResidentFemaleCount, 0)       TotalResidentFemaleCount
  END

GO

CREATE OR
ALTER PROC pGetDashboardMunicipalitySummary @ID_UserSession INT = NULL
as
  BEGIN
      DECLARE @ID_User    INT,
              @ID_Company INT
      DECLARE @TotalResidentCount INT = 0
      DECLARE @MonthlyVaccinationCount INT = 0
      DECLARE @TotalResidentMaleCount INT = 0
      DECLARE @TotalResidentFemaleCount INT = 0

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      SELECT @TotalResidentCount = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  resident.ID_Company > 1
             and resident.IsActive = 1

      SELECT @MonthlyVaccinationCount = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  resident.ID_Company > 1
             and resident.IsActive = 1
             AND IsVaccinated = 1
             AND YEAR(DateLastVaccination) = YEAR(GETDATE())
             AND MONTH(DateLastVaccination) = MONTH(GETDATE())

      SELECT @TotalResidentMaleCount = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  resident.ID_Company > 1
             and resident.IsActive = 1
             AND resident.ID_Gender = 1

      SELECT @TotalResidentFemaleCount = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  resident.ID_Company > 1
             and resident.IsActive = 1
             AND resident.ID_Gender = 2

      SELECT '_'

      SELECT ISNULL(@TotalResidentCount, 0)       TotalResidentCount,
             ISNULL(@MonthlyVaccinationCount, 0)  MonthlyVaccinationCount,
             ISNULL(@TotalResidentMaleCount, 0)   TotalResidentMaleCount,
             ISNULL(@TotalResidentFemaleCount, 0) TotalResidentFemaleCount
  END

GO

Create OR
ALTER PROC dbo.pGetDashboardBarangayMonthlyClientsDataSource (@ID_UserSession INT,
                                                              @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      DECLARE @ClientDailyCount INT = 0
      DECLARE @ClientMonthlyCount INT = 0
      DECLARE @SalesDailyAmount DECIMAL(18, 2) = 0
      DECLARE @SalesMonthlyAmount DECIMAL(18, 2) = 0

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear                          DateYear,
             'Mga Bagong Resident ' + @DateYear Label

      SELECT SUM(CASE datepart(month, DateCreated)
                   WHEN 1 THEN 1
                   ELSE 0
                 END) AS 'January',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 2 THEN 1
                   ELSE 0
                 END) AS 'February',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 3 THEN 1
                   ELSE 0
                 END) AS 'March',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 4 THEN 1
                   ELSE 0
                 END) AS 'April',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 5 THEN 1
                   ELSE 0
                 END) AS 'May',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 6 THEN 1
                   ELSE 0
                 END) AS 'June',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 7 THEN 1
                   ELSE 0
                 END) AS 'July',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 8 THEN 1
                   ELSE 0
                 END) AS 'August',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 9 THEN 1
                   ELSE 0
                 END) AS 'September',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 10 THEN 1
                   ELSE 0
                 END) AS 'October',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 11 THEN 1
                   ELSE 0
                 END) AS 'November',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 12 THEN 1
                   ELSE 0
                 END) AS 'December',
             SUM(CASE datepart(year, DateCreated)
                   WHEN 2012 THEN 1
                   ELSE 0
                 END) AS 'TOTAL',
             ID_Company
      FROM   tResident
      WHERE  IsActive = 1
             AND YEAR(DateCreated) = @DateYear
             AND ID_Company > 1
      GROUP  BY ID_Company
  END

GO

CREATE OR
ALTER PROC dbo.pGetDashboardMunicipalityMonthlyClientsDataSource (@ID_UserSession INT,
                                                                  @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      DECLARE @ClientDailyCount INT = 0
      DECLARE @ClientMonthlyCount INT = 0
      DECLARE @SalesDailyAmount DECIMAL(18, 2) = 0
      DECLARE @SalesMonthlyAmount DECIMAL(18, 2) = 0

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear                          DateYear,
             'Mga Bagong Resident ' + @DateYear Label

      SELECT SUM(CASE datepart(month, DateCreated)
                   WHEN 1 THEN 1
                   ELSE 0
                 END) AS 'January',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 2 THEN 1
                   ELSE 0
                 END) AS 'February',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 3 THEN 1
                   ELSE 0
                 END) AS 'March',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 4 THEN 1
                   ELSE 0
                 END) AS 'April',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 5 THEN 1
                   ELSE 0
                 END) AS 'May',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 6 THEN 1
                   ELSE 0
                 END) AS 'June',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 7 THEN 1
                   ELSE 0
                 END) AS 'July',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 8 THEN 1
                   ELSE 0
                 END) AS 'August',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 9 THEN 1
                   ELSE 0
                 END) AS 'September',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 10 THEN 1
                   ELSE 0
                 END) AS 'October',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 11 THEN 1
                   ELSE 0
                 END) AS 'November',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 12 THEN 1
                   ELSE 0
                 END) AS 'December',
             SUM(CASE datepart(year, DateCreated)
                   WHEN 2012 THEN 1
                   ELSE 0
                 END) AS 'TOTAL'
      FROM   tResident
      WHERE  IsActive = 1
             AND YEAR(DateCreated) = @DateYear
             AND ID_Company > 1
  END

GO

CREATE OR
ALTER PROC dbo.pGetDashboardBarangayResidentProgramDataSource (@ID_UserSession INT,
                                                               @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      DECLARE @ClientDailyCount INT = 0
      DECLARE @ClientMonthlyCount INT = 0
      DECLARE @SalesDailyAmount DECIMAL(18, 2) = 0
      DECLARE @SalesMonthlyAmount DECIMAL(18, 2) = 0
      DECLARE @dataSource TABLE
        (
           Name  VARCHAR(MAX),
           Count INT
        )

      INSERT @dataSource
      SELECT Name_Program,
             Count(*)
      FROM   vResident_Program _program
             inner join tResident resident
                     on _program.ID_Resident = resident.ID
      Where  resident.IsActive = 1
             AND resident.ID_Company = @ID_Company
      GROUP  BY Name_Program

      IF (SELECT COUNT(*)
          FROM   @dataSource) = 0
        BEGIN
            INSERT @dataSource
            SELECT 'No Record',
                   0
        END

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear DateYear

      SELECT *
      FROM   @dataSource
  END

GO

CREATE OR
ALTER PROC dbo.pGetDashboardMunicipalityMonthlyVaccinatedResidentDataSource (@ID_UserSession INT,
                                                                             @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      DECLARE @ClientDailyCount INT = 0
      DECLARE @ClientMonthlyCount INT = 0
      DECLARE @SalesDailyAmount DECIMAL(18, 2) = 0
      DECLARE @SalesMonthlyAmount DECIMAL(18, 2) = 0

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear                   DateYear,
             'Mga Bakunado ' + @DateYear Label

      SELECT SUM(CASE datepart(month, DateCreated)
                   WHEN 1 THEN 1
                   ELSE 0
                 END) AS 'January',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 2 THEN 1
                   ELSE 0
                 END) AS 'February',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 3 THEN 1
                   ELSE 0
                 END) AS 'March',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 4 THEN 1
                   ELSE 0
                 END) AS 'April',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 5 THEN 1
                   ELSE 0
                 END) AS 'May',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 6 THEN 1
                   ELSE 0
                 END) AS 'June',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 7 THEN 1
                   ELSE 0
                 END) AS 'July',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 8 THEN 1
                   ELSE 0
                 END) AS 'August',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 9 THEN 1
                   ELSE 0
                 END) AS 'September',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 10 THEN 1
                   ELSE 0
                 END) AS 'October',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 11 THEN 1
                   ELSE 0
                 END) AS 'November',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 12 THEN 1
                   ELSE 0
                 END) AS 'December',
             SUM(CASE datepart(year, DateCreated)
                   WHEN 2012 THEN 1
                   ELSE 0
                 END) AS 'TOTAL'
      FROM   tResident
      WHERE  IsActive = 1
             AND YEAR(DateCreated) = @DateYear
             AND ID_Company > 1
             and IsVaccinated = 1
  END

GO

CREATE OR
ALTER PROC dbo.pGetDashboardBarangayMonthlyVaccinatedResidentDataSource (@ID_UserSession INT,
                                                                         @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      DECLARE @ClientDailyCount INT = 0
      DECLARE @ClientMonthlyCount INT = 0
      DECLARE @SalesDailyAmount DECIMAL(18, 2) = 0
      DECLARE @SalesMonthlyAmount DECIMAL(18, 2) = 0

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear                   DateYear,
             'Mga Bakunado ' + @DateYear Label

      SELECT SUM(CASE datepart(month, DateCreated)
                   WHEN 1 THEN 1
                   ELSE 0
                 END) AS 'January',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 2 THEN 1
                   ELSE 0
                 END) AS 'February',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 3 THEN 1
                   ELSE 0
                 END) AS 'March',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 4 THEN 1
                   ELSE 0
                 END) AS 'April',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 5 THEN 1
                   ELSE 0
                 END) AS 'May',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 6 THEN 1
                   ELSE 0
                 END) AS 'June',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 7 THEN 1
                   ELSE 0
                 END) AS 'July',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 8 THEN 1
                   ELSE 0
                 END) AS 'August',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 9 THEN 1
                   ELSE 0
                 END) AS 'September',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 10 THEN 1
                   ELSE 0
                 END) AS 'October',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 11 THEN 1
                   ELSE 0
                 END) AS 'November',
             SUM(CASE datepart(month, DateCreated)
                   WHEN 12 THEN 1
                   ELSE 0
                 END) AS 'December',
             SUM(CASE datepart(year, DateCreated)
                   WHEN 2012 THEN 1
                   ELSE 0
                 END) AS 'TOTAL',
             ID_Company
      FROM   tResident
      WHERE  IsActive = 1
             AND YEAR(DateCreated) = @DateYear
             AND ID_Company = @ID_Company
             and IsVaccinated = 1
      GROUP  BY ID_Company
  END

GO

GO

Create OR
ALTER PROC dbo.pGetDashboardBarangayRegisteredResidentDataSource (@ID_UserSession INT,
                                                                  @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_UserSession;

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      DECLARE @ClientDailyCount INT = 0
      DECLARE @ClientMonthlyCount INT = 0
      DECLARE @SalesDailyAmount DECIMAL(18, 2) = 0
      DECLARE @SalesMonthlyAmount DECIMAL(18, 2) = 0
      DECLARE @dataSource TABLE
        (
           Name  VARCHAR(MAX),
           Count INT
        )

      INSERT @dataSource
      SELECT TOP 10 resident.Name_Company,
                    Count(resident.ID_Company) as Count
      FROM   vResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      WHERE  resident.ID_Company = @ID_Company
             AND YEAR(resident.DateCreated) = @DateYear
      GROUP  BY resident.ID_Company,
                resident.Name_Company
      ORDER  BY Count DESC

      IF (SELECT COUNT(*)
          FROM   @dataSource) = 0
        BEGIN
            INSERT @dataSource
            SELECT 'No Record',
                   0
        END

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear DateYear

      SELECT *
      FROM   @dataSource
  END

GO

Create OR
ALTER PROC dbo.pGetDashboardMunicipalityRegisteredResidentDataSource (@ID_UserSession INT,
                                                                      @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_UserSession;

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      DECLARE @ClientDailyCount INT = 0
      DECLARE @ClientMonthlyCount INT = 0
      DECLARE @SalesDailyAmount DECIMAL(18, 2) = 0
      DECLARE @SalesMonthlyAmount DECIMAL(18, 2) = 0
      DECLARE @dataSource TABLE
        (
           Name  VARCHAR(MAX),
           Count INT
        )

      INSERT @dataSource
      SELECT company.Name,
             Count(*) as Count
      FROM   vResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      WHERE  resident.ID_Company > 1
             AND YEAR(resident.DateCreated) = @DateYear
      GROUP  BY company.Name
      ORDER  BY Count DESC

      IF (SELECT COUNT(*)
          FROM   @dataSource) = 0
        BEGIN
            INSERT @dataSource
            SELECT 'No Record',
                   0
        END

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear DateYear

      SELECT *
      FROM   @dataSource
  END

GO

ALTER PROCEDURE pGetResidentRequest @ID                                         INT = -1,
                                    @ID_ResidentRequest_CommunityTaxCertificate INT,
                                    @ID_Resident                                INT = NULL,
                                    @ID_ResidentRequest                         INT = NULL,
                                    @ID_Session                                 INT = NULL
AS
  BEGIN
      SELECT '_',
             '' ResidentRequest_BarangayBusinessClearance,
             '' ResidentRequest_IndigencyCertificate,
             '' ResidentRequest_Certificate,
             '' ResidentRequest_CommunityTaxCertificate

      DECLARE @ID_User      INT,
              @ID_Warehouse INT
      DECLARE @Name_Resident VARCHAR(MAX) = ''
      DECLARE @ContactNumber_Resident VARCHAR(MAX) = ''
      DECLARE @Address_Resident VARCHAR(MAX) = ''
      DECLARE @ID_Gender_Resident INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF( ISNULL(@ID_Resident, 0) > 0 )
        BEGIN
            SELECT @Name_Resident = Name,
                   @ContactNumber_Resident = ContactNumber,
                   @Address_Resident = Address,
                   @ID_Gender_Resident = ID_Gender
            FROM   vResident
            WHERE  ID = @ID_Resident
        END

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL                    AS [_],
                           -1                      AS [ID],
                           NULL                    AS [Code],
                           @Name_Resident          AS [Name],
                           @ContactNumber_Resident ContactNumber,
                           @Address_Resident       Address,
                           @ID_Gender_Resident     ID_Gender,
                           1                       AS [IsActive],
                           NULL                    AS [ID_Company],
                           NULL                    AS [Comment],
                           NULL                    AS [DateCreated],
                           NULL                    AS [DateModified],
                           NULL                    AS [ID_CreatedBy],
                           NULL                    AS [ID_LastModifiedBy],
                           @ID_ResidentRequest     AS ID_ResidentRequest,
                           @ID_Resident            AS ID_Resident) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tResident resident
                          ON H.ID_Resident = resident.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vResidentRequest H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   vResidentRequest_BarangayBusinessClearance
      WHERE  ID_ResidentRequest = @ID

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT -1        ID,
                           GetDate() DateCreated) H
        END
      ELSE
        BEGIN
            SELECT *
            FROM   vResidentRequest_IndigencyCertificate
            WHERE  ID_ResidentRequest = @ID
        END

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   gender.Name Name_Gender
            FROM   (SELECT -1                  ID,
                           @ID_Gender_Resident ID_Gender) H
                   LEFT JOIN tGender gender
                          on gender.ID = h.ID_Gender
        END
      ELSE
        BEGIN
            SELECT *
            FROM   vResidentRequest_Certificate
            WHERE  ID_ResidentRequest = @ID
        END

      SELECT *
      FROM   vResidentRequest_CommunityTaxCertificate
      WHERE  ID_ResidentRequest = @ID
  END

GO

ALTER PROCEDURE pGetResidentRequest_BarangayBusinessClearance @ID          INT = -1,
                                                              @ID_Resident INT = NULL,
                                                              @ID_Session  INT = NULL
AS
  BEGIN
      SELECT '_'

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL AS [_],
                           -1   AS [ID],
                           NULL AS [Code],
                           NULL AS [Name],
                           1    AS [IsActive],
                           NULL AS [ID_Company],
                           NULL AS [Comment],
                           NULL AS [DateCreated],
                           NULL AS [DateModified],
                           NULL AS [ID_CreatedBy],
                           NULL AS [ID_LastModifiedBy]) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vResidentRequest_BarangayBusinessClearance H
            WHERE  H.ID = @ID
        END
  END

GO

ALTER PROCEDURE pGetResidentRequest_CommunityTaxCertificate @ID          INT = -1,
                                                            @ID_Resident INT = NULL,
                                                            @ID_Session  INT = NULL
AS
  BEGIN
      SELECT '_'

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL         AS [_],
                           -1           AS [ID],
                           NULL         AS [Code],
                           NULL         AS [Name],
                           1            AS [IsActive],
                           NULL         AS [ID_Company],
                           NULL         AS [Comment],
                           NULL         AS [DateCreated],
                           NULL         AS [DateModified],
                           NULL         AS [ID_CreatedBy],
                           NULL         AS [ID_LastModifiedBy],
                           @ID_Resident AS ID_Resident) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vResidentRequest_CommunityTaxCertificate H
            WHERE  H.ID = @ID
        END
  END 
