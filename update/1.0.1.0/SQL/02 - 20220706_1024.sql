GO

IF COL_LENGTH('tResident', 'ImageProfilePicFilename') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResident',
        'ImageProfilePicFilename',
        1
  END

GO

IF Object_Id('dbo.tMedicalRecordType') IS NULL
  BEGIN
      EXEC _pCreateappModuleWithTable
        'tMedicalRecordType',
        1,
        0,
        NULL;

      TRUNCATE TABLE tMedicalRecordType;

      INSERT INTO [dbo].tMedicalRecordType
                  ([Name],
                   [IsActive])
      SELECT Name,
             1
      FROM   (SELECT 'Vaccination' Name
              UNION ALL
              SELECT 'Pregnancy Diagnosis' Name
              UNION ALL
              SELECT 'Purgative' Name
              UNION ALL
              SELECT 'Others' Name) tbl;
  END

GO

IF Object_Id('dbo.tResident_MedicalRecord') IS NULL
  BEGIN
      EXEC _pCreateappModuleWithTable
        'tResident_MedicalRecord',
        1,
        0,
        NULL

      EXEC _PaddModelProperty
        'tResident_MedicalRecord',
        'ID_Resident',
        2

      EXEC _PaddModelDetail
        'tResident_MedicalRecord',
        'ID_Resident',
        'tResident',
        'tResident'
  END

GO

IF Object_Id('dbo.tResident_Family') IS NULL
  BEGIN
      EXEC _pCreateappModuleWithTable
        'tResident_Family',
        1,
        0,
        NULL

      EXEC _PaddModelProperty
        'tResident_Family',
        'ID_Resident',
        2

      EXEC _PaddModelProperty
        'tResident_Family',
        'ID_FamilyMember',
        2

      EXEC _PaddModelProperty
        'tResident_Family',
        'ID_RelationshipType',
        2

      EXEC _PaddModelDetail
        'tResident_Family',
        'ID_Resident',
        'tResident',
        'tResident'
  END

GO

IF Object_Id('dbo.tFamilyRelationshipType') IS NULL
  BEGIN
      EXEC _pCreateappModuleWithTable
        'tFamilyRelationshipType',
        1,
        0,
        NULL

      INSERT INTO [dbo].[tFamilyRelationshipType]
                  ([Name],
                   [IsActive],
                   [ID_Company],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy])
      VALUES      ('Ama',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1),
                  ('Ina',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1),
                  ('Anak',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1),
                  ('Kapatid',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1),
                  ('Lola',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1),
                  ('Lolo',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1)
  END

GO

IF col_Length('dbo.tResident', 'IsPregnant') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tResident',
        'IsPregnant',
        4
  END

GO

IF col_Length('dbo.tResident', 'IsHeadofFamily') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tResident',
        'IsHeadofFamily',
        4
  END

GO

IF col_Length('dbo.tResident_MedicalRecord', 'Date') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tResident_MedicalRecord',
        'Date',
        5
  END

GO

IF col_Length('dbo.tResident_MedicalRecord', 'ID_MedicalRecordType') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tResident_MedicalRecord',
        'ID_MedicalRecordType',
        2
  END

GO

exec _pRefreshAllViews

GO

ALTER VIEW vResident_ListvIew
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid Guid_Company
  FROM   vResident hed
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company

GO

CREATE OR
ALTER VIEW vResident_ProgramListview
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid         Guid_Company,
         program.ID_Program   ID_Program,
         program.Name_Program Name_Program
  FROM   vResident hed
         INNER JOIN vResident_Program program
                 on hed.ID = program.ID_Resident
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company

GO

CREATE    OR
ALTER VIEW [dbo].vzResidentFormReport
AS
  SELECT Resident.Name                                           Name,
         Resident.ID                                             ID,
         Resident.Address                                        Address_Resident,
         ISNULL(civilStatus.TranslatedEngName, '')               TranslatedEngName_CivilStatus,
         ISNULL(FORMAT(DateBirth, 'MMMM dd, yyyy'), '')          FormatBirthDate_Resident,
         dbo.fGetYearAge(DateBirth, GETDATE(), '')               Age_Resident,
         company.ID                                              ID_Company,
         company.ImageLogoLocationFilenamePath                   ImageLogoLocationFilenamePath,
         company.Name                                            Name_Company,
         company.Address                                         Address_Company,
         company.ContactNumber                                   ContactNumber_Company,
         company.BarangayListSideReportHTMLString                BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                             BarangayCaptainName_Company,
         Resident.Name_CivilStatus                               Name_CivilStatus,
         company.BarangayCaptain_Name_Employee                   BarangayCaptain_Name_Employee,
         company.BarangayCaptain_ImageSignitureSpecimen_Employee BarangayCaptain_ImageSignitureSpecimen_Employee,
         municipality.Name                                       Name_MunicipalityCompany,
         municipality.ImageLogoLocationFilenamePath              ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                                    Address_MunicipalityCompany,
         Resident.Name_Gender,
         dbo.fGetAPILink() + '/Content/Thumbnail/'
         + Resident.ImageProfilePicFilename                      ImageProfilePicFilename
  FROM   vResident Resident
         INNER JOIN vCompanyActive company
                 ON company.ID = Resident.ID_Company
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = Resident.ID_Company
         Left JOIN tCivilStatus civilStatus
                on Resident.ID_CivilStatus = civilStatus.ID

go

CREATE OR
ALTER VIEW [dbo].[vzResidentDetailReport]
AS
  SELECT hed.*,
         company.ImageLogoLocationFilenamePath,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END HeaderInfo_Company
  FROM   vResident hed
         LEFT JOIN dbo.vCompany company
                ON company.ID = hed.ID_Company
  WHERE  hed.IsActive = 1

GO


CREATE OR
ALTER VIEW vResidentProgramString
AS
  SELECT DIStinct progA.ID_Resident,
                  STUFF((SELECT N', ' + Name_Program
                         FROM   dbo.vResident_Program progB
                         WHERE  progB.ID_Resident = progA.ID_Resident Order by Name_Program
                         FOR XML PATH(''), TYPE).value(N'.[1]', N'nvarchar(max)'), 1, 2, N'') ProgramString
  FROM   vResident_Program progA 
GO

CREATE OR
ALTER VIEW [dbo].[vzResidentProgramReport]
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
		REPLACE(rPrgString.ProgramString, ', ', '<br/>') ProgramString,
         hed.ID_Company,
         company.Name                                                                Name_Company,
         company.ImageLogoLocationFilenamePath,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                                                       HeaderInfo_Company
  FROM   vResident hed
	     INNER JOIN vResidentProgramString rPrgString ON hed.ID = rPrgString.ID_Resident
         LEFT JOIN dbo.vCompany company
                ON company.ID = hed.ID_Company
  WHERE  hed.IsActive = 1

GO

CREATE OR
ALTER VIEW vResident_Family
AS
  SELECT H.*,
         rel.Name AS Name_Relationship,
         res.Name AS Name_FamilyMember,
         UC.Name  AS CreatedBy,
         UM.Name  AS LastModifiedBy
  FROM   tResident_Family H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tFamilyRelationshipType rel
                ON rel.ID = H.ID_RelationshipType
         LEFT JOIN tResident res
                ON res.ID = H.ID_FamilyMember

GO

GO

CREATE OR
ALTER VIEW vResident_MedicalRecord
AS
  SELECT H.*,
         UC.Name                AS CreatedBy,
         UM.Name                AS LastModifiedBy,
         medicalRecordType.Name Name_MedicalRecordType
  FROM   tResident_MedicalRecord H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tMedicalRecordType medicalRecordType
                ON H.ID_MedicalRecordType = medicalRecordType.ID

GO

ALTER PROCEDURE pGetResident @ID         INT = -1,
                             @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS Resident_Program,
             '' AS Resident_Family,
             '' AS Resident_MedicalRecord,
             '' AS Reports

      DECLARE @ID_User INT
      DECLARE @ID_Company INT
      DECLARE @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse,
             @ID_Company = ID_Company
      FROM   tUserSession _session
             INNER JOIN vUSer _user
                     on _session.ID_User = _user.ID
      WHERE  _session.ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL AS [_],
                           -1   AS [ID],
                           NULL AS [Code],
                           NULL AS [Name],
                           1    AS [IsActive],
                           NULL AS [ID_Company],
                           NULL AS [Comment],
                           NULL AS [DateCreated],
                           NULL AS [DateModified],
                           NULL AS [ID_CreatedBy],
                           NULL AS [ID_LastModifiedBy]) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tCompany company
                          ON company.ID = h.ID_Company
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vResident H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   vResident_Program
      WHERE  ID_Resident = @ID

      SELECT *
      FROM   vResident_Family
      WHERE  ID_Resident = @ID

      SELECT *
      FROM   vResident_MedicalRecord
      WHERE  ID_Resident = @ID
      Order  by ID DESC

      SELECT *
      FROm   vResidentDetailReportList
      order  by label
  END

GO

ALTER PROC pGetDashboardMunicipalitySummary @ID_UserSession INT = NULL
as
  BEGIN
      DECLARE @ID_User    INT,
              @ID_Company INT
      DECLARE @ResidentCount INT = 0
      DECLARE @PermanentResidentCount INT = 0
      DECLARE @MigrateResidentCount INT = 0
      DECLARE @TrancientResidentCount INT = 0
      DECLARE @HomeOwnershipStatusOwnerCount INT = 0
      DECLARE @EducationalLevelGraduatedCount INT = 0
      DECLARE @TotalResidentMaleCount INT = 0
      DECLARE @TotalResidentFemaleCount INT = 0
      DECLARE @ResidentCountAge60Above INT = 0
      DECLARE @ResidentCountAge18to59 INT = 0
      DECLARE @ResidentCountAge10to17 INT = 0
      DECLARE @ResidentCountAge5to9 INT = 0
      DECLARE @ResidentCountAge24to84months INT = 0
      DECLARE @ResidentCountAge12to23months INT = 0
      DECLARE @ResidentCountAge0to11months INT = 0
      DECLARE @ResidentCountMale INT = 0
      DECLARE @ResidentCountFemale INT = 0
      DECLARE @ResidentCount4Ps INT = 0
      DECLARE @ResidentCountSeniorCitizen INT = 0
      DECLARE @ResidentCountScholar INT = 0
      DECLARE @MayAriNgBahayAtLupaID_HomeOwnershipStatus INT = 1
      DECLARE @GraduateSchool_ID_EducationalLevel INT = 6
      DECLARE @Male_ID_Gender INT = 1
      DECLARE @Female_ID_Gender INT = 2

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      SELECT @ResidentCount = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1

      SELECT @PermanentResidentCount = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1
             AND IsPermanent = 1

      SELECT @MigrateResidentCount = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1
             AND IsMigrante = 1

      SELECT @TrancientResidentCount = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1
             AND IsTransient = 1

      SELECT @HomeOwnershipStatusOwnerCount = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1
             ANd ID_HomeOwnershipStatus = @MayAriNgBahayAtLupaID_HomeOwnershipStatus

      SELECT @EducationalLevelGraduatedCount = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1
             ANd ID_EducationalLevel = @GraduateSchool_ID_EducationalLevel

      SELECT @ResidentCountMale = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1
             ANd ID_Gender = @Male_ID_Gender

      SELECT @ResidentCountFemale = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1
             ANd ID_Gender = @Female_ID_Gender

      SELECT @ResidentCountAge60Above = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) >= 60

      SELECT @ResidentCountAge18to59 = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) BETWEEN 18 AND 59

      SELECT @ResidentCountAge10to17 = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) BETWEEN 10 AND 17

      SELECT @ResidentCountAge5to9 = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) BETWEEN 5 AND 9

      SELECT @ResidentCountAge24to84months = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1
             and DateBirth IS NOT NULL
             AND DATEDIFF(MONTH, DateBirth, GETDATE()) BETWEEN 24 AND 84

      SELECT @ResidentCountAge12to23months = COUNT(*)
      FROm   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             and DateBirth IS NOT NULL
             AND DATEDIFF(MONTH, DateBirth, GETDATE()) BETWEEN 12 AND 23

      SELECT @ResidentCountAge0to11months = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  resident.ID_Company > 1
             and resident.IsActive = 1
             and DateBirth IS NOT NULL
             AND DATEDIFF(MONTH, DateBirth, GETDATE()) BETWEEN 0 AND 11

      SELECT @ResidentCount4Ps = COUNT(*)
      FROM   (SELECT DISTINCT resident.ID
              FROM   tResident resident
                     inner join tResident_Program residentprogram
                             on resident.ID = residentprogram.ID_Resident
                     Inner join tProgram program
                             on residentprogram.ID_Program = program.ID
                     inner join vCompanyActive company
                             on resident.ID_Company = company.ID
              where  resident.ID_Company > 1
                     and resident.IsActive = 1
                     AND program.Name IN ( '4Ps' )
              GROUP  BY resident.ID) tbl

      SELECT @ResidentCountSeniorCitizen = COUNT(*)
      FROM   (SELECT DISTINCT resident.ID
              FROM   tResident resident
                     inner join tResident_Program residentprogram
                             on resident.ID = residentprogram.ID_Resident
                     Inner join tProgram program
                             on residentprogram.ID_Program = program.ID
                     inner join vCompanyActive company
                             on resident.ID_Company = company.ID
              where  resident.ID_Company = @ID_Company
                     and resident.IsActive > 1
                     AND program.Name IN ( 'Senior Citizen' )
              GROUP  BY resident.ID) tbl

      SELECT @ResidentCountScholar = COUNT(*)
      FROM   (SELECT DISTINCT resident.ID
              FROM   tResident resident
                     inner join tResident_Program residentprogram
                             on resident.ID = residentprogram.ID_Resident
                     Inner join tProgram program
                             on residentprogram.ID_Program = program.ID
                     inner join vCompanyActive company
                             on resident.ID_Company = company.ID
              where  resident.ID_Company > 1
                     and resident.IsActive = 1
                     AND program.Name IN ( 'Scholarship' )
              GROUP  BY resident.ID) tbl

      SELECT '_'

      SELECT ISNULL(@ResidentCount, 0)                  ResidentCount,
             ISNULL(@PermanentResidentCount, 0)         PermanentResidentCount,
             ISNULL(@MigrateResidentCount, 0)           MigrateResidentCount,
             ISNULL(@TrancientResidentCount, 0)         TrancientResidentCount,
             ISNULL(@HomeOwnershipStatusOwnerCount, 0)  HomeOwnershipStatusOwnerCount,
             ISNULL(@EducationalLevelGraduatedCount, 0) EducationalLevelGraduatedCount,
             ISNULL(@ResidentCountAge60Above, 0)        ResidentCountAge60Above,
             ISNULL(@ResidentCountAge18to59, 0)         ResidentCountAge18to59,
             ISNULL(@ResidentCountAge10to17, 0)         ResidentCountAge10to17,
             ISNULL(@ResidentCountAge5to9, 0)           ResidentCountAge5to9,
             ISNULL(@ResidentCountAge24to84months, 0)   ResidentCountAge24to84months,
             ISNULL(@ResidentCountAge12to23months, 0)   ResidentCountAge12to23months,
             ISNULL(@ResidentCountAge0to11months, 0)    ResidentCountAge0to11months,
             ISNULL(@ResidentCountMale, 0)              ResidentCountMale,
             ISNULL(@ResidentCountFemale, 0)            ResidentCountFemale,
             ISNULL(@ResidentCount4Ps, 0)               ResidentCount4Ps,
             ISNULL(@ResidentCountSeniorCitizen, 0)     ResidentCountSeniorCitizen,
             ISNULL(@ResidentCountScholar, 0)           ResidentCountScholar
  END

Go

GO

CREATE OR
ALTER PROC dbo.pGetDashboardMunicipaltyResidentProgramDataSource (@ID_UserSession INT,
                                                                  @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      DECLARE @ClientDailyCount INT = 0
      DECLARE @ClientMonthlyCount INT = 0
      DECLARE @SalesDailyAmount DECIMAL(18, 2) = 0
      DECLARE @SalesMonthlyAmount DECIMAL(18, 2) = 0
      DECLARE @dataSource TABLE
        (
           Name  VARCHAR(MAX),
           Count INT
        )

      INSERT @dataSource
      SELECT Name_Program,
             Count(*)
      FROM   vResident_Program _program
             inner join tResident resident
                     on _program.ID_Resident = resident.ID
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  resident.ID_Company > 1
             and resident.IsActive = 1
      GROUP  BY Name_Program

      IF (SELECT COUNT(*)
          FROM   @dataSource) = 0
        BEGIN
            INSERT @dataSource
            SELECT 'No Record',
                   0
        END

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear DateYear

      SELECT *
      FROM   @dataSource
  END

GO

CREATE OR
ALTER PROC dbo.pGetDashboardMunicipalityPregnancyDiagnosis (@ID_UserSession INT,
                                                            @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_UserSession;

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      DECLARE @ClientDailyCount INT = 0
      DECLARE @ClientMonthlyCount INT = 0
      DECLARE @SalesDailyAmount DECIMAL(18, 2) = 0
      DECLARE @SalesMonthlyAmount DECIMAL(18, 2) = 0
      DECLARE @record TABLE
        (
           CountYear  INT,
           CountMonth VARCHAR(MAX),
           TotalCount DECIMAL(18, 2)
        )
      DECLARE @MonthNumber INT;

      SET @MonthNumber = 1;

      WHILE @MonthNumber <= 12
        BEGIN
            INSERT @record
            SELECT @DateYear,
                   DateName(month, DateAdd(month, @MonthNumber, -1)),
                   0.00

            SET @MonthNumber= @MonthNumber + 1
        END;

      DECLARE @PregnancyDiagnosis_ID_MedicalRecord INT = 0

      Update @record
      SET    TotalCount = tbl.TotalCount
      FROM   @record record
             INNER JOIN (SELECT YEAR(medicalRecord.Date)                                          as CountYear,
                                DateName(month, DateAdd(month, MONTH(medicalRecord.Date), 0) - 1) as CountMonth,
                                COUNT(*)                                                          AS TotalCount
                         FROM   vResident resident
                                inner join vResident_MedicalRecord medicalRecord
                                        on resident.ID = medicalRecord.ID_Resident
                                inner join vCompanyActive company
                                        on resident.ID_Company = company.ID
                         WHere  resident.ID_Company > 1
                                and resident.IsActive = 1
                                AND YEAR(medicalRecord.Date) = @DateYear
                         GROUP  BY YEAR(medicalRecord.Date),
                                   MONTH(medicalRecord.Date)) tbl
                     on tbl.CountMonth = record.CountMonth
                        and tbl.CountYear = record.CountYear

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear DateYear

      SELECT *
      FROM   @record
  END

GO

exec pGetDashboardMunicipalityPregnancyDiagnosis
  @ID_UserSession=N'61120',
  @DateYear=N'2022'

IF NOT EXISTS (SELECT 1
               FROM   _tReport
               WHERE  Name = 'ResidentDetailReport')
  BEGIN
      exec _pCreateReportView
        'ResidentDetailReport',
        1
  END

IF NOT EXISTS (SELECT 1
               FROM   _tReport
               WHERE  Name = 'ResidentProgramReport')
  BEGIN
      exec _pCreateReportView
        'ResidentProgramReport',
        1
  END

GO 
