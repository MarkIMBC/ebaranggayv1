GO

CREATE    OR
ALTER VIEW vHouseholdNumber_Listview
as
  Select household.ID,
         household.Name,
         household.ID_Company,
         ISNULL(summary.Count, 0) Count,
         DateCreated,
         AssignedBHW_Name_Employee
  FROM   vHouseholdNumber household
         lEFT join vHouseholdNumberResidentHeadOfFamilyCount summary
                on household.ID = summary.ID
  WHERE  household.IsActive = 1

GO

CREATE      OR
ALTER VIEW vResident_Listview_HeadOfFamily
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid Guid_Company,
         ID_HouseholdNumber,
         AssignedBHW_Name_Employee
  FROM   vResident hed
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  hed.IsActive = 1
         AND ISNULL(hed.IsHeadofFamily, 0) = 1
         ANd ISNULL(IsDeceased, 0) = 0

GO

CReate OR
ALTER PROC _tempGenerateResident(@ID_Company INT)
as
  BEGIN
      DECLARE @IDs_Resident TABLE
        (
           RowID       INT,
           ID_Resident INT
        )
      DECLARE @currentCounter INT = 1
      DECLARE @maxCounter INT = 1

      INSERT @IDs_Resident
      SELECT ROW_NUMBER()
               OVER(
                 ORDER BY ID ASC) AS RowID,
             ID
      FROM   tResident
      WHERE  ID_Company = @ID_Company
             AND ISNULL(Code, '') = ''

      SELECT @maxCounter = COUNT(*)
      FROM   @IDs_Resident

      IF( @maxCounter > 0 )
        SELECT Name,
               @maxCounter
        FROM   tCompany
        WHERE  ID = @ID_Company

      WHILE @currentCounter <= @maxCounter
        BEGIN
            DECLARE @ID_Resident INT = 0

            SELECT @ID_Resident = ID_Resident
            FROM   @IDs_Resident
            WHERE  RowID = @currentCounter

            exec pModel_GenerateCode_By_ModelName
              'Resident',
              @ID_Resident,
              1

            SET @currentCounter = @currentCounter + 1
        END
  END

GO

/* GENERATE CODE for Resident IN ALL COMPANY */
GO

DECLARE @IDs_Company TABLE
  (
     RowID      INT,
     ID_Company INT
  )
DECLARE @currentCounter INT = 1
DECLARE @maxCounter INT = 1

INSERT @IDs_Company
SELECT ROW_NUMBER()
         OVER(
           ORDER BY ID ASC) AS RowID,
       ID
FROM   tCompany

SELECT @maxCounter = COUNT(*)
FROM   @IDs_Company

WHILE @currentCounter <= @maxCounter
  BEGIN
      DECLARE @ID_Company INT = 0

      SELECT @ID_Company = ID_Company
      FROM   @IDs_Company
      WHERE  RowID = @currentCounter

      exec _tempGenerateResident
        @ID_Company

      SET @currentCounter = @currentCounter + 1
  END

GO

DROP PROC _tempGenerateResident

GO

GO

SELECT *
FROM   tResident

GO

GO

CReate OR
ALTER PROC _tempGenerateCertificateOfResidency(@ID_Company INT)
as
  BEGIN
      DECLARE @IDs_CertificateOfResidency TABLE
        (
           RowID                     INT,
           ID_CertificateOfResidency INT
        )
      DECLARE @currentCounter INT = 1
      DECLARE @maxCounter INT = 1

      INSERT @IDs_CertificateOfResidency
      SELECT ROW_NUMBER()
               OVER(
                 ORDER BY ID ASC) AS RowID,
             ID
      FROM   tCertificateOfResidency
      WHERE  ID_Company = @ID_Company
             AND ISNULL(Code, '') = ''

      SELECT @maxCounter = COUNT(*)
      FROM   @IDs_CertificateOfResidency

      IF( @maxCounter > 0 )
        SELECT Name,
               @maxCounter
        FROM   tCompany
        WHERE  ID = @ID_Company

      WHILE @currentCounter <= @maxCounter
        BEGIN
            DECLARE @ID_CertificateOfResidency INT = 0

            SELECT @ID_CertificateOfResidency = ID_CertificateOfResidency
            FROM   @IDs_CertificateOfResidency
            WHERE  RowID = @currentCounter

            exec pModel_GenerateCode_By_ModelName
              'CertificateOfResidency',
              @ID_CertificateOfResidency,
              1

            SET @currentCounter = @currentCounter + 1
        END
  END

GO

/* GENERATE CODE for CertificateOfResidency IN ALL COMPANY */
GO

DECLARE @IDs_Company TABLE
  (
     RowID      INT,
     ID_Company INT
  )
DECLARE @currentCounter INT = 1
DECLARE @maxCounter INT = 1

INSERT @IDs_Company
SELECT ROW_NUMBER()
         OVER(
           ORDER BY ID ASC) AS RowID,
       ID
FROM   tCompany

SELECT @maxCounter = COUNT(*)
FROM   @IDs_Company

WHILE @currentCounter <= @maxCounter
  BEGIN
      DECLARE @ID_Company INT = 0

      SELECT @ID_Company = ID_Company
      FROM   @IDs_Company
      WHERE  RowID = @currentCounter

      exec _tempGenerateCertificateOfResidency
        @ID_Company

      SET @currentCounter = @currentCounter + 1
  END

GO

DROP PROC _tempGenerateCertificateOfResidency

GO

GO

SELECT *
FROM   tCertificateOfResidency

GO

CReate OR
ALTER PROC _tempGenerateBarangayClearance(@ID_Company INT)
as
  BEGIN
      DECLARE @IDs_BarangayClearance TABLE
        (
           RowID                INT,
           ID_BarangayClearance INT
        )
      DECLARE @currentCounter INT = 1
      DECLARE @maxCounter INT = 1

      INSERT @IDs_BarangayClearance
      SELECT ROW_NUMBER()
               OVER(
                 ORDER BY ID ASC) AS RowID,
             ID
      FROM   tBarangayClearance
      WHERE  ID_Company = @ID_Company
             AND ISNULL(Code, '') = ''

      SELECT @maxCounter = COUNT(*)
      FROM   @IDs_BarangayClearance

      IF( @maxCounter > 0 )
        SELECT Name,
               @maxCounter
        FROM   tCompany
        WHERE  ID = @ID_Company

      WHILE @currentCounter <= @maxCounter
        BEGIN
            DECLARE @ID_BarangayClearance INT = 0

            SELECT @ID_BarangayClearance = ID_BarangayClearance
            FROM   @IDs_BarangayClearance
            WHERE  RowID = @currentCounter

            exec pModel_GenerateCode_By_ModelName
              'BarangayClearance',
              @ID_BarangayClearance,
              1

            SET @currentCounter = @currentCounter + 1
        END
  END

GO

/* GENERATE CODE for BarangayClearance IN ALL COMPANY */
GO

DECLARE @IDs_Company TABLE
  (
     RowID      INT,
     ID_Company INT
  )
DECLARE @currentCounter INT = 1
DECLARE @maxCounter INT = 1

INSERT @IDs_Company
SELECT ROW_NUMBER()
         OVER(
           ORDER BY ID ASC) AS RowID,
       ID
FROM   tCompany

SELECT @maxCounter = COUNT(*)
FROM   @IDs_Company

WHILE @currentCounter <= @maxCounter
  BEGIN
      DECLARE @ID_Company INT = 0

      SELECT @ID_Company = ID_Company
      FROM   @IDs_Company
      WHERE  RowID = @currentCounter

      exec _tempGenerateBarangayClearance
        @ID_Company

      SET @currentCounter = @currentCounter + 1
  END

GO

DROP PROC _tempGenerateBarangayClearance

GO

GO

SELECT *
FROM   tBarangayClearance

GO

CReate OR
ALTER PROC _tempGenerateSoloParent(@ID_Company INT)
as
  BEGIN
      DECLARE @IDs_SoloParent TABLE
        (
           RowID         INT,
           ID_SoloParent INT
        )
      DECLARE @currentCounter INT = 1
      DECLARE @maxCounter INT = 1

      INSERT @IDs_SoloParent
      SELECT ROW_NUMBER()
               OVER(
                 ORDER BY ID ASC) AS RowID,
             ID
      FROM   tSoloParent
      WHERE  ID_Company = @ID_Company
             AND ISNULL(Code, '') = ''

      SELECT @maxCounter = COUNT(*)
      FROM   @IDs_SoloParent

      IF( @maxCounter > 0 )
        SELECT Name,
               @maxCounter
        FROM   tCompany
        WHERE  ID = @ID_Company

      WHILE @currentCounter <= @maxCounter
        BEGIN
            DECLARE @ID_SoloParent INT = 0

            SELECT @ID_SoloParent = ID_SoloParent
            FROM   @IDs_SoloParent
            WHERE  RowID = @currentCounter

            exec pModel_GenerateCode_By_ModelName
              'SoloParent',
              @ID_SoloParent,
              1

            SET @currentCounter = @currentCounter + 1
        END
  END

GO

/* GENERATE CODE for SoloParent IN ALL COMPANY */
GO

DECLARE @IDs_Company TABLE
  (
     RowID      INT,
     ID_Company INT
  )
DECLARE @currentCounter INT = 1
DECLARE @maxCounter INT = 1

INSERT @IDs_Company
SELECT ROW_NUMBER()
         OVER(
           ORDER BY ID ASC) AS RowID,
       ID
FROM   tCompany

SELECT @maxCounter = COUNT(*)
FROM   @IDs_Company

WHILE @currentCounter <= @maxCounter
  BEGIN
      DECLARE @ID_Company INT = 0

      SELECT @ID_Company = ID_Company
      FROM   @IDs_Company
      WHERE  RowID = @currentCounter

      exec _tempGenerateSoloParent
        @ID_Company

      SET @currentCounter = @currentCounter + 1
  END

GO

DROP PROC _tempGenerateSoloParent

GO

GO

SELECT *
FROM   tSoloParent

GO

CReate OR
ALTER PROC _tempGenerateCertificateOfResidency(@ID_Company INT)
as
  BEGIN
      DECLARE @IDs_CertificateOfResidency TABLE
        (
           RowID                     INT,
           ID_CertificateOfResidency INT
        )
      DECLARE @currentCounter INT = 1
      DECLARE @maxCounter INT = 1

      INSERT @IDs_CertificateOfResidency
      SELECT ROW_NUMBER()
               OVER(
                 ORDER BY ID ASC) AS RowID,
             ID
      FROM   tCertificateOfResidency
      WHERE  ID_Company = @ID_Company
             AND ISNULL(Code, '') = ''

      SELECT @maxCounter = COUNT(*)
      FROM   @IDs_CertificateOfResidency

      IF( @maxCounter > 0 )
        SELECT Name,
               @maxCounter
        FROM   tCompany
        WHERE  ID = @ID_Company

      WHILE @currentCounter <= @maxCounter
        BEGIN
            DECLARE @ID_CertificateOfResidency INT = 0

            SELECT @ID_CertificateOfResidency = ID_CertificateOfResidency
            FROM   @IDs_CertificateOfResidency
            WHERE  RowID = @currentCounter

            exec pModel_GenerateCode_By_ModelName
              'CertificateOfResidency',
              @ID_CertificateOfResidency,
              1

            SET @currentCounter = @currentCounter + 1
        END
  END

GO

/* GENERATE CODE for CertificateOfResidency IN ALL COMPANY */
GO

DECLARE @IDs_Company TABLE
  (
     RowID      INT,
     ID_Company INT
  )
DECLARE @currentCounter INT = 1
DECLARE @maxCounter INT = 1

INSERT @IDs_Company
SELECT ROW_NUMBER()
         OVER(
           ORDER BY ID ASC) AS RowID,
       ID
FROM   tCompany

SELECT @maxCounter = COUNT(*)
FROM   @IDs_Company

WHILE @currentCounter <= @maxCounter
  BEGIN
      DECLARE @ID_Company INT = 0

      SELECT @ID_Company = ID_Company
      FROM   @IDs_Company
      WHERE  RowID = @currentCounter

      exec _tempGenerateCertificateOfResidency
        @ID_Company

      SET @currentCounter = @currentCounter + 1
  END

GO

DROP PROC _tempGenerateCertificateOfResidency

GO

GO

SELECT *
FROM   tCertificateOfResidency

GO

CReate OR
ALTER PROC _tempGenerateCertificationOfNoIncome(@ID_Company INT)
as
  BEGIN
      DECLARE @IDs_CertificationOfNoIncome TABLE
        (
           RowID                      INT,
           ID_CertificationOfNoIncome INT
        )
      DECLARE @currentCounter INT = 1
      DECLARE @maxCounter INT = 1

      INSERT @IDs_CertificationOfNoIncome
      SELECT ROW_NUMBER()
               OVER(
                 ORDER BY ID ASC) AS RowID,
             ID
      FROM   tCertificationOfNoIncome
      WHERE  ID_Company = @ID_Company
             AND ISNULL(Code, '') = ''

      SELECT @maxCounter = COUNT(*)
      FROM   @IDs_CertificationOfNoIncome

      IF( @maxCounter > 0 )
        SELECT Name,
               @maxCounter
        FROM   tCompany
        WHERE  ID = @ID_Company

      WHILE @currentCounter <= @maxCounter
        BEGIN
            DECLARE @ID_CertificationOfNoIncome INT = 0

            SELECT @ID_CertificationOfNoIncome = ID_CertificationOfNoIncome
            FROM   @IDs_CertificationOfNoIncome
            WHERE  RowID = @currentCounter

            exec pModel_GenerateCode_By_ModelName
              'CertificationOfNoIncome',
              @ID_CertificationOfNoIncome,
              1

            SET @currentCounter = @currentCounter + 1
        END
  END

GO

/* GENERATE CODE for CertificationOfNoIncome IN ALL COMPANY */
GO

DECLARE @IDs_Company TABLE
  (
     RowID      INT,
     ID_Company INT
  )
DECLARE @currentCounter INT = 1
DECLARE @maxCounter INT = 1

INSERT @IDs_Company
SELECT ROW_NUMBER()
         OVER(
           ORDER BY ID ASC) AS RowID,
       ID
FROM   tCompany

SELECT @maxCounter = COUNT(*)
FROM   @IDs_Company

WHILE @currentCounter <= @maxCounter
  BEGIN
      DECLARE @ID_Company INT = 0

      SELECT @ID_Company = ID_Company
      FROM   @IDs_Company
      WHERE  RowID = @currentCounter

      exec _tempGenerateCertificationOfNoIncome
        @ID_Company

      SET @currentCounter = @currentCounter + 1
  END

GO

DROP PROC _tempGenerateCertificationOfNoIncome

GO

exec pAddDocumentSeries
  'tResident_Business',
  'BUS' 
