GO

if OBJECT_ID('dbo.tDisaster') is NULL
  BEGIN
      exec _pCreateAppModuleWithTable
        'tDisaster',
        1,
        NULL,
        NULL
  END

GO

if OBJECT_ID('dbo.tHouseholdNumber_Disaster') is NULL
  BEGIN
      exec _pCreateAppModuleWithTable
        'tHouseholdNumber_Disaster',
        1,
        NULL,
        NULL
  END

GO

IF COL_LENGTH('tHouseholdNumber_Disaster', 'ID_Disaster') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tHouseholdNumber_Disaster',
        'ID_Disaster',
        2
  END

GO

IF COL_LENGTH('tHouseholdNumber_Disaster', 'ID_HouseholdNumber') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tHouseholdNumber_Disaster',
        'ID_HouseholdNumber',
        2

      exec _pAddModelDetail
        'tHouseholdNumber_Disaster',
        'ID_HouseholdNumber',
        'tHouseholdNumber',
        'tHouseholdNumber'
  END

GO

IF COL_LENGTH('tHouseholdNumber', 'Representative_ID_Resident') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tHouseholdNumber',
        'Representative_ID_Resident',
        2
  END

GO

IF COL_LENGTH('tResident', 'Source_ID_Resident_Family') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResident',
        'Source_ID_Resident_Family',
        2
  END

GO

exec _pRefreshAllViews

Go

GO

CREATE   OR
ALTER VIEW vResident
AS
  SELECT H.*,
         UC.Name                  AS CreatedBy,
         UM.Name                  AS LastModifiedBy,
         gender.Name              Name_Gender,
         company.Name             Name_Company,
         civilStatus.Name         Name_CivilStatus,
         educationalLevel.Name    Name_EducationalLevel,
         homeOwnershipStatus.Name Name_HomeOwnershipStatus,
         occupationalStatus.Name  Name_OccupationalStatus,
         householdNumber.Name     Name_HouseholdNumber,
         AssignedBHW.Name         AssignedBHW_Name_Employee,
         religion.Name            Name_Religion,
         AssignedBNS.Name         AssignedBNS_Name_Employee,
         AssignedBNS.Name         AssignedBBNS_Name_Employee
  FROM   tResident H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tGender gender
                ON gender.ID = h.ID_Gender
         LEFT JOIN tCivilStatus civilStatus
                ON civilStatus.ID = h.ID_CivilStatus
         LEFT JOIN tEducationalLevel educationalLevel
                ON educationalLevel.ID = h.ID_EducationalLevel
         LEFT JOIN tHomeOwnershipStatus homeOwnershipStatus
                ON homeOwnershipStatus.ID = h.ID_HomeOwnershipStatus
         LEFT JOIN tOccupationalStatus occupationalStatus
                ON occupationalStatus.ID = h.ID_OccupationalStatus
         LEFT JOIN tReligion religion
                ON religion.ID = h.ID_Religion
         LEFT JOIN tCompany company
                ON company.ID = h.ID_Company
         LEFT JOIN tEmployee AssignedBHW
                ON AssignedBHW.ID = h.AssignedBHW_ID_Employee
         LEFT JOIN tEmployee AssignedBNS
                ON AssignedBNS.ID = h.AssignedBNS_ID_Employee
         LEFT JOIN vHouseholdNumber householdNumber
                on H.ID_HouseholdNumber = householdNumber.ID

GO

CREATE OR
ALTER VIEW vResidentNonDeceased
as
  SELECT *
  FROM   vResident
  WHERE  IsActive = 1
         ANd ISNULL(IsDeceased, 0) = 0

GO

CREATE OR
ALTER VIEW vHouseholdNumber
AS
  SELECT H.*,
         UC.Name                 AS CreatedBy,
         UM.Name                 AS LastModifiedBy,
         AssignedBHW.Name        AssignedBHW_Name_Employee,
         residentRespondent.Name Representative_Name_Resident
  FROM   tHouseholdNumber H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tEmployee AssignedBHW
                ON AssignedBHW.ID = h.AssignedBHW_ID_Employee
         LEFT JOIN tResident residentRespondent
                ON h.Representative_ID_Resident = residentRespondent.ID

GO

CREATE OR
ALTER VIEW vHouseholdNumber_Disaster
AS
  SELECT H.*,
         UC.Name       AS CreatedBy,
         UM.Name       AS LastModifiedBy,
         disaster.Name Name_Disaster
  FROM   tHouseholdNumber_Disaster H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tDisaster disaster
                ON H.ID_Disaster = disaster.ID

GO

CREATE OR
ALTER VIEW dbo.vEmployeeActive
AS
  SELECT H.*
  FROM   vEmployee H
  WHERE  ISNULL(H.IsActive, 0) = 1

GO

GO

CREATE   OR
ALTER VIEW vEmployeeBarangayHealthWorker
AS
  SELECT *
  FROM   vEmployeeActive
  WHERE  Name_Position IN ( 'Barangay Health Worker' )
         and ISNULL(IsActive, 0) = 1

GO

CREATE   OR
ALTER VIEW vEmployeeBNS
AS
  SELECT *
  FROM   vEmployeeActive
  WHERE  Name_Position IN ( 'BNS' )
         and ISNULL(IsActive, 0) = 1

GO

CREATE OR
ALTER VIEW vHouseholdNumberDisasterRisk
as
  SELECT ID_HouseholdNumber,
         DisasterRisk = LTRIM(RTRIM(STUFF((SELECT ', ' + Name_Disaster
                                           FROM   vHouseholdNumber_Disaster
                                           where  ID_HouseholdNumber = hed.ID_HouseholdNumber
                                           FOR XML PATH ('')), 1, 1, '')))
  FROM   vHouseholdNumber_Disaster hed
  GROUP  BY ID_HouseholdNumber

GO

ALTER VIEW vHouseholdNumberResidentHeadOfFamilyCount
as
  Select household.ID,
         household.Name,
         resident.ID_Company,
         COUNT(*) Count
  FROM   tHouseholdNumber household
         inner join tResident resident
                 on household.ID = resident.ID_HouseholdNumber
  WHERE  resident.IsActive = 1
         and ISNULL(IsDeceased, 0) = 0
         And IsHeadofFamily = 1
  GROUP  BY household.ID,
            household.Name,
            resident.ID_Company

GO

CREATE    OR
ALTER VIEW vResident_ProgramListview_PhiiHealth
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid         Guid_Company,
         program.ID_Program   ID_Program,
         program.Name_Program Name_Program
  FROM   vResident hed
         INNER JOIN vResident_Program program
                 on hed.ID = program.ID_Resident
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  program.Name_Program IN ( 'PhiiHealth', 'PHILHEALTH' )
         AND hed.IsActive = 1
         AND ISNULL(hed.IsDeceased, 0) = 0

GO

ALTER VIEW vResident_ProgramListView_EducationalAssistance
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid         Guid_Company,
         program.ID_Program   ID_Program,
         program.Name_Program Name_Program
  FROM   vResident hed
         INNER JOIN vResident_Program program
                 on hed.ID = program.ID_Resident
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  program.Name_Program IN ( 'Educational Financial Assistance (Senior High)', 'Educational Financial Assistance (College)', 'Senior High Educational Assistance', 'College Educational Assistance' )
         AND hed.IsActive = 1
         AND ISNULL(hed.IsDeceased, 0) = 0

GO

ALTER VIEW vResident_ProgramListView_Listahanan
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid         Guid_Company,
         program.ID_Program   ID_Program,
         program.Name_Program Name_Program
  FROM   vResident hed
         INNER JOIN vResident_Program program
                 on hed.ID = program.ID_Resident
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  program.Name_Program IN ( 'Listahanan' )
         and hed.IsActive = 1
         AND ISNULL(hed.IsDeceased, 0) = 0

GO

CREATE   OR
alter VIEW vzResidentDisasterRiskReport
AS
  SELECT hed.ID,
         hed.Name,
         hed.Name_Gender,
         hed.ID_Gender,
         hed.ContactNumber,
         hed.VoteInfoPrecinctNumber,
         dbo.fGetIntAge(hed.DateBirth)                           Age,
         householdNumber.Name                                    Name_HouseholdNumber,
         householdNumber.AssignedBHW_Name_Employee,
         disasterRisk.Name_Disaster,
         disasterRisk.ID_Disaster,
         ISNULL(disasterRiskList.DisasterRisk, '<i>None</i>')    DisasterRisk,
         company.ID                                              ID_Company,
         company.ImageLogoLocationFilenamePath                   ImageLogoLocationFilenamePath,
         company.Name                                            Name_Company,
         company.Address                                         Address_Company,
         company.ContactNumber                                   ContactNumber_Company,
         company.BarangayListSideReportHTMLString                BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                             BarangayCaptainName_Company,
         company.BarangayCaptain_Name_Employee                   BarangayCaptain_Name_Employee,
         company.BarangayCaptain_LastName_Employee               BarangayCaptain_LastName_Employee,
         company.BarangayCaptain_FirstName_Employee              BarangayCaptain_FirstName_Employee,
         company.BarangayCaptain_MiddleName_Employee             BarangayCaptain_MiddleName_Employee,
         company.BarangayCaptain_ImageSignitureSpecimen_Employee BarangayCaptain_ImageSignitureSpecimen_Employee,
         municipality.Name                                       Name_MunicipalityCompany,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' <br/>Email: ' + company.Email
             ELSE ''
           END                                                   HeaderInfo_Company,
         municipality.ImageLogoLocationFilenamePath              ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                                    Address_MunicipalityCompany
  FROM   vResident hed
         iNNER JOIN vHouseholdNumber householdNumber
                 on hed.ID_HouseholdNumber = householdNumber.ID
         INNER JOIN vHouseholdNumberDisasterRisk disasterRiskList
                 on disasterRiskList.ID_HouseholdNumber = householdNumber.ID
         INNER JOIN vHouseholdNumber_Disaster disasterRisk
                 on disasterRisk.ID_HouseholdNumber = householdNumber.ID
         INNER JOIN vCompanyActive company
                 ON company.ID = hed.ID_Company
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = hed.ID_Company
  WHERE  hed.IsActive = 1
         and hed.IsVoter = 1
         and ISNULL(IsDeceased, 0) = 0
         And IsHeadofFamily = 1

GO

CREATE   OR
ALTER PROC pInsertDisaster(@Disaster   VARCHAR(MAX),
                           @ID_Company INT = 1)
as
  BEGIN
      IF(SELECT COUNT(*)
         FROM   tDisaster
         WHERE  Name = @Disaster) = 0
        BEGIN
            INSERT INTO [dbo].tDisaster
                        ([Name],
                         [IsActive],
                         ID_Company)
            VALUES      (@Disaster,
                         1,
                         @ID_Company)
        END
  END

GO

GO

CREATE OR
ALTER PROCEDURE pGetHouseholdNumber @ID         INT = -1,
                                    @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' as HouseholdNumber_Disaster

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL AS [_],
                           -1   AS [ID],
                           NULL AS [Code],
                           NULL AS [Name],
                           1    AS [IsActive],
                           NULL AS [ID_Company],
                           NULL AS [Comment],
                           NULL AS [DateCreated],
                           NULL AS [DateModified],
                           NULL AS [ID_CreatedBy],
                           NULL AS [ID_LastModifiedBy]) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vHouseholdNumber H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   vHouseholdNumber_Disaster
      where  ID_HouseholdNumber = @ID
  END

GO

CREATE OR
ALTER PROC [dbo].[pModel_AfterSaved_Resident] (@ID_CurrentObject VARCHAR(10),
                                               @IsNew            BIT = 0)
AS
  BEGIN
      /* Generate Document Series */
      DECLARE @Oid_Model UNIQUEIDENTIFIER;
      DECLARE @Code VARCHAR(MAX) = '';
      DECLARE @ID_Company INT;

      SELECT @ID_Company = ID_Company
      FROM   dbo.tResident
      WHERE  ID = @ID_CurrentObject;

      SELECT @Oid_Model = m.Oid
      FROM   dbo._tModel m
      WHERE  m.TableName = 'tResident';

      IF @IsNew = 1
        BEGIN
            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0, NULL);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tResident
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;

      INSERT tResident
             (Name,
              Source_ID_Resident_Family,
              ID_Company,
              IsActive,
              DateCreated,
              DateModified,
              ID_CreatedBy,
              ID_LastModifiedBy)
      SELECT Name,
             ID,
             @ID_Company,
             1,
             GETDATE(),
             GETDATE(),
             1,
             1
      FROM   tResident_Family family
      WHERE  family.ID_Resident = @ID_CurrentObject
             and ISNULL(family.ID_FamilyMember, 0) = 0

      Update tResident_Family
      SET    ID_FamilyMember = resident.ID
      FRoM   tResident resident
             inner join tResident_Family family
                     on resident.Source_ID_Resident_Family = family.ID
      WHERE  ID_Resident = @ID_CurrentObject
             AND resident.ID_Company = @ID_Company
  END;

GO

ALTER PROC [dbo].[pModel_AfterSaved] @ID_Model         UNIQUEIDENTIFIER,
                                     @ID_CurrentObject VARCHAR(10),
                                     @IsNew            BIT = 0
AS
  BEGIN
      DECLARE @ModelName VARCHAR(MAX) = '';

      SELECT @ModelName = Name
      FROM   dbo._tModel
      WHERE  Oid = @ID_Model;

      IF @ModelName = 'AppointmentSchedule'
        BEGIN
            EXEC dbo.pModel_AfterSaved_AppointmentSchedule
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Schedule'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Schedule
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'BillingInvoice'
        BEGIN
            EXEC dbo.pModel_AfterSaved_BillingInvoice
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Client'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Client
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PurchaseOrder'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PurchaseOrder
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ReceivingReport'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ReceivingReport
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PaymentTransaction'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PaymentTransaction
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_SOAP'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_SOAP
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Confinement'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Confinement
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Item'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_Item]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ClientDeposit'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ClientDeposit
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'ClientWithdraw'
        BEGIN
            EXEC dbo.pModel_AfterSaved_ClientWithdraw
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'TextBlast'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_TextBlast]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'SalesReturn'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_SalesReturn]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'PatientWaitingList'
        BEGIN
            EXEC dbo.pModel_AfterSaved_PatientWaitingList
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Lodging'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Lodging
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Vaccination'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Vaccination
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Patient_Wellness'
        BEGIN
            EXEC dbo.pModel_AfterSaved_Patient_Wellness
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'VeterinaryHealthCertificate'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_VeterinaryHealthCertificate]
              @ID_CurrentObject,
              @IsNew;
        END;
      ELSE IF @ModelName = 'Resident'
        BEGIN
            EXEC dbo.[pModel_AfterSaved_Resident]
              @ID_CurrentObject,
              @IsNew;
        END;

      PRINT 1;
  END;

GO

CREATE OR
ALTER PROC [dbo].pModel_GenerateCode @ID_Model         UNIQUEIDENTIFIER,
                                     @ID_CurrentObject VARCHAR(10),
                                     @IsNew            BIT = 0
AS
  BEGIN
      DECLARE @ModelName VARCHAR(MAX) = '';

      SELECT @ModelName = Name
      FROM   dbo._tModel
      WHERE  Oid = @ID_Model;

      IF @IsNew = 1
        BEGIN
            DECLARE @sql VARCHAR(MAX) = '
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @ID_CurrentObject VARCHAR(10);
            DECLARE @Code VARCHAR(MAX);
            DECLARE @ID_Company INT;

            SELECT @ID_Company = ID_Company
            FROM   dbo.t/*ModelName*/
            WHERE  ID = /*ID_CurrentObject*/

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  Name = ''/*ModelName*/'';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0, NULL);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.t/*ModelName*/
            SET    Code = @Code
            WHERE  ID = /*ID_CurrentObject*/'

            SET @sql = REPLACE(@sql, '/*ModelName*/', @ModelName)
            SET @sql = REPLACE(@sql, '/*ID_CurrentObject*/', @ID_CurrentObject)

            --print @sql
            exec (@sql)
        END;;
  END;

GO

CREATE        OR
ALTER PROC pGetDashboardBarangaySummary @ID_UserSession INT = NULL
as
  BEGIN
      DECLARE @ID_User    INT,
              @ID_Company INT
      DECLARE @ResidentCount INT = 0
      DECLARE @TotalResidentMaleCount INT = 0
      DECLARE @TotalResidentFemaleCount INT = 0
      DECLARE @HouseholdCount INT = 0
      DECLARE @HeadOfFamilyCount INT = 0
      DECLARE @ResidentCountAge60Above INT = 0
      DECLARE @ResidentCountAge18to59 INT = 0
      DECLARE @ResidentCountAge01to17 INT = 0
      DECLARE @ResidentCountAge5to9 INT = 0
      DECLARE @ResidentCountAge24to84months INT = 0
      DECLARE @ResidentCountAge12to23months INT = 0
      DECLARE @ResidentCountAge0to11months INT = 0
      DECLARE @ResidentCountMale INT = 0
      DECLARE @ResidentCountFemale INT = 0
      DECLARE @ResidentVaccinatedCount INT = 0
      DECLARE @ResidentNonVaccinatedCount INT = 0
      DECLARE @ResidentNoOccupationCount INT = 0
      DECLARE @VoterCount INT = 0
      DECLARE @Male_ID_Gender INT = 1
      DECLARE @Female_ID_Gender INT = 2
      DECLARE @Summary TABLE
        (
           RowIndex int IDENTITY(1, 1) PRIMARY KEY,
           Name     VARCHAR(MAX),
           Count    Int,
		   Label   VARCHAR(MAX)
        )

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      INSERT @Summary
      SELECT 'ResidentCount',
             COUNT(*),
             'Bilang ng Residente'
      FROM   vResidentNonDeceased
      where  ID_Company = @ID_Company
             and IsActive = 1

      INSERT @Summary
      SELECT 'HouseholdCount',
             COUNT(*),
             'Bilang ng Household'
      FROM   tHouseholdNumber
      WHERE  IsActive = 1
             and ID_Company = @ID_Company

      INSERT @Summary
      SELECT 'HeadOfFamilyCount',
             COUNT(*),
             'Bilang ng Pamilya'
      FROM   vResidentNonDeceased
      where  ID_Company = @ID_Company
             and ISNULL(IsHeadofFamily, 0) = 1

      INSERT @Summary
      SELECT 'ResidentCountMale',
             COUNT(*),
             'Bilang ng Lalake'
      FROM   vResidentNonDeceased
      where  ID_Company = @ID_Company
             and IsActive = 1
             ANd ID_Gender = @Male_ID_Gender

      INSERT @Summary
      SELECT 'ResidentCountFemale',
             COUNT(*),
             'Bilang ng Babae'
      FROM   vResidentNonDeceased
      where  ID_Company = @ID_Company
             and IsActive = 1
             ANd ID_Gender = @Female_ID_Gender

      INSERT @Summary
      SELECT 'ResidentCountAge0to11months',
             COUNT(*),
             'Bilang ng Sanggol (0-11 Buwan)'
      FROM   vResidentNonDeceased
      where  ID_Company = @ID_Company
             and IsActive = 1
             and DateBirth IS NOT NULL
             AND DATEDIFF(MONTH, DateBirth, GETDATE()) BETWEEN 0 AND 11

      INSERT @Summary
      SELECT 'ResidentCountAge01to17',
             COUNT(*),
             'Bilang ng Bata / Kabataan (1-17 taon)'
      FROM   vResidentNonDeceased
      where  ID_Company = @ID_Company
             and IsActive = 1
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) BETWEEN 1 AND 17

      INSERT @Summary
      SELECT 'ResidentCountAge18to59',
             COUNT(*),
             'Bilang ng Hustong Gulang (18-59 taon)'
      FROM   vResidentNonDeceased
      where  ID_Company = @ID_Company
             and IsActive = 1
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) BETWEEN 18 AND 59

      INSERT @Summary
      SELECT 'ResidentCountAge60Above',
             COUNT(*),
             'Bilang ng Senior Citizen (60 pataas)'
      FROm   vResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) >= 60

      INSERT @Summary
      SELECT 'ResidentVaccinatedCount',
             COUNT(*),
             'Bilang ng Bakunado'
      FROM   vResidentNonDeceased
      where  ID_Company = @ID_Company
             and ISNULL(IsVaccinated, 0) = 1

      INSERT @Summary
      SELECT 'ResidentNonVaccinatedCount',
             COUNT(*),
             'Bilang ng Walang Bakuna'
      FROM   vResidentNonDeceased
      where  ID_Company = @ID_Company
             and ISNULL(IsVaccinated, 0) = 0

      INSERT @Summary
      SELECT 'ResidentNoOccupationCount',
             COUNT(*),
             'Bilang ng Walang Trabaho'
      FROM   vResidentNonDeceased
      where  ID_Company = @ID_Company
             and ISNULL(ID_OccupationalStatus, 0) IN ( 1, 3 )

      SELECT '_',
             '' AS Summary

      SELECT GETDATE() Date

      SELECT *
      FROM   @Summary
  END

GO

CREATE OR
ALTER PROC pAddDocumentSeries(@TableName_Model VARCHAR(MAX),
                              @Prefix          VARCHAR(MAX),
                              @Counter         INT = 1,
                              @DigitCount      INT = 5)
AS
  BEGIN
      DECLARE @ID_Model UNIQUEIDENTIFIER
      DECLARE @Name_Model VARCHAR(MAX) = ''
      DECLARE @ID_DocumentSeries INT = 0
      DECLARE @IDs_Company typIntList

      SELECT @Name_Model = Name,
             @ID_Model = Oid
      FROM   dbo._tModel
      WHERE  TableName = @TableName_Model

      if(SELECt COUNT(*)
         FROM   tDocumentSeries
         where  ID_Company = 1
                AND ID_Model = @ID_Model) = 0
        begin
            INSERT dbo.tDocumentSeries
                   (Name,
                    ID_Company,
                    ID_Model,
                    Counter,
                    Prefix,
                    DigitCount,
                    IsActive,
                    DateCreated,
                    DateModified,
                    ID_CreatedBy,
                    ID_LastModifiedBy)
            VALUES(@Name_Model,
                   1,
                   @ID_Model,
                   @Counter,
                   @Prefix,
                   @DigitCount,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1)
        END

      SELECT @ID_DocumentSeries = ID
      FROM   tDocumentSeries
      where  ID_Company = 1
             and Name = @Name_Model

      Insert @IDs_Company
      SELECT ID
      FROm   tCompany
      except
      SELECT docSeries.ID_Company
      FROM   dbo.tDocumentSeries docSeries
      where  Name = @Name_Model

      INSERT dbo.tDocumentSeries
             (Code,
              Name,
              IsActive,
              Comment,
              DateCreated,
              DateModified,
              ID_CreatedBy,
              ID_LastModifiedBy,
              ID_Model,
              Counter,
              Prefix,
              IsAppendCurrentDate,
              DigitCount,
              ID_Company)
      SELECT docSeries.Code,
             docSeries.Name,
             docSeries.IsActive,
             docSeries.Comment,
             docSeries.DateCreated,
             docSeries.DateModified,
             docSeries.ID_CreatedBy,
             docSeries.ID_LastModifiedBy,
             docSeries.ID_Model,
             docSeries.Counter,
             docSeries.Prefix,
             docSeries.IsAppendCurrentDate,
             docSeries.DigitCount,
             company.ID
      FROM   dbo.tDocumentSeries docSeries,
             @IDs_Company company
      WHERE  docSeries.ID_Company = 1
             AND docSeries.ID = @ID_DocumentSeries
  END

GO

CREATE OR
ALTER PROC [dbo].pModel_GenerateCode_By_ModelName (@ModelName        VARCHAR(MAX),
                                                   @ID_CurrentObject VARCHAR(10),
                                                   @IsNew            BIT = 0)
AS
  BEGIN
      DECLARE @ID_Model UNIQUEIDENTIFIER

      SELECT @ID_Model = Oid
      FROM   dbo._tModel
      WHERE  Name = @ModelName

      exec pModel_GenerateCode
        @ID_Model,
        @ID_CurrentObject,
        @IsNew
  END;

GO

exec pAddDocumentSeries
  'tResident',
  'R'

exec pAddDocumentSeries
  'tCertificateOfIndigency',
  'CIND'

exec pAddDocumentSeries
  'tCertificateOfResidency',
  'CRES'

exec pAddDocumentSeries
  'tBarangayClearance',
  'BCLR'

exec pAddDocumentSeries
  'tSoloParent',
  'SP'

exec pAddDocumentSeries
  'tCertificationOfNoIncome',
  'CNIN'

IF(SELECT COUNT(*)
   FROM   _tReport
   where  Name = 'ResidentDisasterRiskReport') = 0
  begin
      exec _pCreateReportView
        'ResidentDisasterRiskReport',
        1
  end

exec pInsertDisaster
  'Baha'

exec pInsertDisaster
  'Lindol'

exec pInsertDisaster
  'Bagyo'

exec pInsertDisaster
  'Pagguho ng Lupa'

exec pInsertDisaster
  'Apoy'

exec pInsertDisaster
  'Pagsabog ng Bulkan' 
