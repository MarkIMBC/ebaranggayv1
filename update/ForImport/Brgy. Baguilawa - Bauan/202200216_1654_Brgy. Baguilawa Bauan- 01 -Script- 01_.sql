GO

GO

CREATE OR
ALTER function __temp_ArrangeDateFormat(@DateString VARCHAR(MAX),
                                        @separator  VARCHAR(MAX))
RETURNS VARCHAR(MAX)
AS
  BEGIN
      DEclare @result VARCHAR(MAX) = ''
      Declare @dataPart TABLE
        (
           ID   INT,
           Part VARCHAR(MAX)
        )

      INSERT @dataPart
      SELECT *
      FROM   dbo.fGetSplitString(@DateString, @separator)

      SELECT @result = @result + '' + Part
      FROM   @dataPart
      where  ID = 2

      SELECT @result = @result + '/' + Part
      FROM   @dataPart
      where  ID = 1

      SELECT @result = @result + '/' + Part
      FROM   @dataPart
      where  ID > 2

      Return @result
  END

GO

GO

DEclare @GUID_Company VARCHAR(MAX) = 'A04ED075-98E4-4BFE-9FF3-C36E3350E6B6'

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @ForImport TABLE
  (
     Purok                        VARCHAR(MAX),
     LastName                     VARCHAR(MAX),
     FirstName                    VARCHAR(MAX),
     MiddleName                   VARCHAR(MAX),
     Ext                          VARCHAR(MAX),
     Birthday_String              VARCHAR(MAX),
     BirthPlace                   VARCHAR(MAX),
     Sex_String                   VARCHAR(MAX),
     CivilStatus_String           VARCHAR(MAX),
     EducationalAttainment_String VARCHAR(MAX),
     ContactNumber                VARCHAR(MAX),
     Religion                     VARCHAR(MAX),
     Citizenship                  VARCHAR(MAX),
     Remarks                      VARCHAR(MAX),
     Guid                         VARCHAR(MAX),
     Name_Resident                VARCHAR(MAX),
     ID_Resident                  INT,
     ID_CivilStatus               INT,
     ID_EducationalLevel          INT,
     ID_Gender                    INT,
     DateBirth                    datetime
  )

INSERT @Forimport
       (Purok,
        LastName,
        FirstName,
        MiddleName,
        Ext,
        Birthday_String,
        BirthPlace,
        Sex_String,
        CivilStatus_String,
        EducationalAttainment_String,
        ContactNumber,
        Religion,
        Citizenship,
        Remarks,
        Guid)
SELECT 'Purok 1' [PUROK],
       [Last Name *],
       [First Name *],
       [Middle Name],
       [Ext],
       [BIRTHDAY (mm/dd/yyyy) *],
       [BIRTHPLACE],
       [SEX],
       [CIVIL STATUS *],
       [EDUCATIONAL ATTAINMENT],
       [CELLPHONE NO.],
       [RELIGION],
       [CITIZENSHIP],
       [REMARKS],
       [GIUD]
FROm   ForImport.dbo.[INHABITANTS-Purok-1]
UNION ALL
SELECT 'Purok 2' [PUROK],
       [Last Name *],
       [First Name *],
       [Middle Name],
       [Ext],
       [BIRTHDAY (mm/dd/yyyy) *],
       [BIRTHPLACE],
       [SEX],
       [SINGLE],
       '',
       '',
       '',
       '',
       '',
       [GIUD]
FROm   ForImport.dbo.[INHABITANTS-Purok-2]
UNION ALL
SELECT 'Purok 3' [PUROK],
       [Last Name *],
       [First Name *],
       [Middle Name],
       [Ext],
       [BIRTHDAY (mm/dd/yyyy) *],
       [BIRTHPLACE],
       [SEX],
       [CIVIL STATUS *],
       [EDUCATIONAL ATTAINMENT],
       [CELLPHONE NO.],
       [RELIGION],
       [CITIZENSHIP],
       [REMARKS],
       [GIUD]
FROm   ForImport.dbo.[INHABITANTS-Purok-3]
UNION ALL
SELECT 'Purok 4' [PUROK],
       [Last Name *],
       [First Name *],
       [Middle Name],
       [Ext],
       [BIRTHDAY (mm/dd/yyyy) *],
       [BIRTHPLACE],
       [SEX],
       [CIVIL STATUS *],
       [EDUCATIONAL ATTAINMENT],
       [CELLPHONE NO.],
       [RELIGION],
       [CITIZENSHIP],
       [REMARKS],
       [GIUD]
FROm   ForImport.dbo.[INHABITANTS-Purok-4]
UNION ALL
SELECT 'Purok 5' [PUROK],
       [Last Name *],
       [First Name *],
       [Middle Name],
       [Ext],
       [BIRTHDAY (mm/dd/yyyy) *],
       [BIRTHPLACE],
       [SEX],
       [CIVIL STATUS *],
       [EDUCATIONAL ATTAINMENT],
       [CELLPHONE NO.],
       [RELIGION],
       [CITIZENSHIP],
       [REMARKS],
       [GIUD]
FROm   ForImport.dbo.[INHABITANTS-Purok-5]
UNION ALL
SELECT 'Purok 6' [PUROK],
       [Last Name *],
       [First Name *],
       [Middle Name],
       [Ext],
       [BIRTHDAY (mm/dd/yyyy) *],
       [BIRTHPLACE],
       [SEX],
       [CIVIL STATUS *],
       [EDUCATIONAL ATTAINMENT],
       [CELLPHONE NO.],
       [RELIGION],
       [CITIZENSHIP],
       [REMARKS],
       [GIUD]
FROm   ForImport.dbo.[INHABITANTS-Purok-6]
UNION ALL
SELECT 'Purok 7' [PUROK],
       [Last Name *],
       [First Name *],
       [Middle Name],
       [Ext],
       [BIRTHDAY (mm/dd/yyyy) *],
       [BIRTHPLACE],
       [SEX],
       [CIVIL STATUS *],
       [EDUCATIONAL ATTAINMENT],
       [CELLPHONE NO.],
       [RELIGION],
       [CITIZENSHIP],
       [REMARKS],
       [GIUD]
FROm   ForImport.dbo.[INHABITANTS-Purok-7]

DELETE FROM @Forimport
WHERE  LEN(ISNULL(LastName, '')) = 0
       AND LEN(ISNULL(FirstName, '')) = 0

Update @Forimport
SET    ID_Gender = 1
where  Sex_String = 'M'

Update @Forimport
SET    ID_Gender = 2
where  Sex_String = 'F'

Update @Forimport
SET    ID_CivilStatus = 1
where  CivilStatus_String IN ( 'CHILDREN', 'SINGLE', 'SINGLKE' )

Update @Forimport
SET    ID_CivilStatus = 2
where  CivilStatus_String IN ( 'MARRIED' )

Update @Forimport
SET    ID_CivilStatus = 3
where  CivilStatus_String IN ( 'LIVE IN', 'LIVE-IN', 'LIVE -IN' )

Update @Forimport
SET    ID_CivilStatus = 4
where  CivilStatus_String IN ( 'SEPARATE', 'SEPARATED' )

Update @Forimport
SET    ID_CivilStatus = 5
where  CivilStatus_String IN ( 'WIDOW', 'WIDOWED' )

Update @Forimport
SET    DateBirth = TRY_CONVERT(datetime, Birthday_String)

Update @Forimport
SET    DateBirth = TRY_CONVERT(datetime, dbo.__temp_ArrangeDateFormat(Birthday_String, '/')) 
WHERE DateBirth IS NULL

Update @Forimport
SET    Name_Resident = LastName + ', ' + FirstName + ' ' + MiddleName

Update @ForImport
SET    ID_Resident = resident.ID
FROM   @ForImport import
       INNER JOIN tResident resident
               on import.Guid = resident.ImportGuid
WHERE  ID_Company = @ID_Company

SELECT Purok,
       LastName,
       FirstName,
       MiddleName,
       Birthday_String,
       DateBirth
FROM   @ForImport
WHere  DateBirth IS NULL
       and LEN(Birthday_String) > 0

SELECT *
FROM   @ForImport

INSERT INTO [dbo].[tResident]
            ([Name],
             ImportGuid,
             [IsActive],
             [ID_Company],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             comment)
SELECT Name_Resident,
       Guid,
       1,
       @ID_Company,
       GETDATE(),
       GETDATE(),
       1,
       1,
       'Imported '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
FROM   @ForImport
WHERE  ID_Resident IS NULL
order  by Name_Resident

Update @ForImport
SET    DateBirth = NULL
where  DateBirth = '1900-01-01 00:00:00.000'

Update @ForImport
SET    ID_Resident = resident.ID
FROM   @ForImport import
       INNER JOIN tResident resident
               on import.Guid = resident.ImportGuid
WHERE  ID_Company = @ID_Company

SELECT *
FROM   @Forimport

INSERT INTO [dbo].[tResident]
            ([Name],
             ImportGuid,
             [IsActive],
             [ID_Company],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             comment)
SELECT Name_Resident,
       Guid,
       1,
       @ID_Company,
       GETDATE(),
       GETDATE(),
       1,
       1,
       'Imported '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
FROM   @ForImport
WHERE  ID_Resident IS NULL
order  by Name_Resident

Update @ForImport
SET    ID_Resident = resident.ID
FROM   @ForImport import
       INNER JOIN tResident resident
               on import.Guid = resident.ImportGuid
WHERE  ID_Company = @ID_Company

Update tResident
SET    Name = import.Name_Resident,
       ID_CivilStatus = import.ID_CivilStatus,
       ID_EducationalLevel = import.ID_EducationalLevel,
       ID_Gender = import.ID_Gender,
       ContactNumber = import.ContactNumber,
       DateBirth = import.DateBirth
FROM   tResident resident
       INNER JOIN @ForImport import
               on import.Guid = resident.ImportGuid
WHERE  ID_Company = @ID_Company 


GO

DROP FUNCTION  dbo.__temp_ArrangeDateFormat
