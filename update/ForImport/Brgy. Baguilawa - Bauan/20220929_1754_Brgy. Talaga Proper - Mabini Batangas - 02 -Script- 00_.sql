GO

DEclare @GUID_Company VARCHAR(MAX) = 'FCC4E4DC-3CD8-4C41-81DC-10C87A06A521'

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @ForImport TABLE
  (
     RowIndex      INT,
     ID_Resident   INT,
     Name_Resident varchar(500),
     DateBirth     DateTime,
     ID_Gender     INT,
     GUID          varchar(500)
  )

INSERT @ForImport
       (RowIndex,
        Name_Resident,
        DateBirth,
        ID_Gender,
        GUID)
SELECT RowIndex,
       dbo.fGetCleanedString([Name of Family Head]),
       TRY_CONVERT(Date, [Birthday]),
       CASE
         WHEN dbo.fGetCleanedString([Sex]) = 'M' THEN 1
         else
           CASE
             WHEN dbo.fGetCleanedString([Sex]) = 'F' THEN 2
             else NULL
           END
       END,
       [GIUD]
FROM   ForImport.[dbo].[Brgy. Talaga Proper -  Mabini Batangas Resident] hed 


Update @ForImport
SET    ID_Resident = resident.ID
FROM   @ForImport import
       INNER JOIN tResident resident
               on import.Guid = resident.ImportGuid
WHERE  ID_Company = @ID_Company

Update @ForImport SET DateBirth = NULL where DateBirth = '1900-01-01 00:00:00.000'

INSERT INTO [dbo].[tResident]
            ([Name],
             ImportGuid,
             [IsActive],
             [ID_Company],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             comment)
SELECT Name_Resident,
       Guid,
       1,
       @ID_Company,
       GETDATE(),
       GETDATE(),
       1,
       1,
       'Imported '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
FROM   @ForImport
WHERE  ID_Resident IS NULL
order  by Name_Resident

Update @ForImport
SET    ID_Resident = resident.ID
FROM   @ForImport import
       INNER JOIN tResident resident
               on import.Guid = resident.ImportGuid
WHERE  ID_Company = @ID_Company


Update tResident
SET    Name = import.Name_Resident,
       ID_Gender = import.ID_Gender,
       DateBirth = import.DateBirth
FROM   tResident resident
       INNER JOIN @ForImport import
               on import.Guid = resident.ImportGuid
WHERE  ID_Company = @ID_Company 


