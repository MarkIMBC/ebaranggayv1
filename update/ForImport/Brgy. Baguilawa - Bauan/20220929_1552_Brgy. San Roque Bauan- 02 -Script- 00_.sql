GO

DEclare @GUID_Company VARCHAR(MAX) = '50575816-F22B-45CE-9C01-F7230181A920'

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @ForImport TABLE
  (
     RowIndex                INT,
     ID_Resident             INT,
     Name_Resident           varchar(500),
     IsHousehold             BIT,
     LastName                varchar(500),
     MiddleName              varchar(500),
     FirstName               varchar(500),
     DateBirth               DateTime,
     ID_Gender               INT,
     ID_RelationshipType     INT,
     IsHeadofFamily          BIT,
     Category                varchar(500),
     ID_OccupationalStatus   INT,
     Occupation              varchar(500),
     ContactNumber           varchar(500),
     AssignedBHW_ID_Employee INT,
     AssignedBHW             varchar(500),
     Address                 varchar(500),
     RelationshipTypeName    varchar(500),
     GUID                    varchar(500)
  )

INSERT @ForImport
       (RowIndex,
        LastName,
        MiddleName,
        FirstName,
        Occupation,
        IsHousehold,
        IsHeadofFamily,
        DateBirth,
        ID_Gender,
        RelationshipTypeName,
        ContactNumber,
        Address,
        AssignedBHW,
        GUID)
SELECT RowIndex,
       dbo.fGetCleanedString(SURNAME),
       dbo.fGetCleanedString(MIDDLENAME),
       dbo.fGetCleanedString(FIRSTNAME),
       dbo.fGetCleanedString(Occupation),
       CASE
         WHEN dbo.fGetCleanedString([HOUSEHOLD]) = 'YES' THEN 1
         else NULL
       END,
       CASE
         WHEN dbo.fGetCleanedString([HEAD OF THE FAMILY (YES/NO)]) = 'YES' THEN 1
         else NULL
       END,
       TRY_CONVERT(Date, [BIRTHDATE]),
       CASE
         WHEN dbo.fGetCleanedString([GENDER]) = 'M' THEN 1
         else
           CASE
             WHEN dbo.fGetCleanedString([GENDER]) = 'F' THEN 2
             else NULL
           END
       END,
       dbo.fGetCleanedString([RELATIONSHIP]),
       [CELLPHONE #],
       'Purok ' + [PUROK #],
       Bhw,
       [GIUD]
FROM   ForImport.[dbo].[San_Roque_Bauan_Masterlist_2022_2ND_SEM] hed

--SELECT DISTINCT ID_RelationshipType,
--                RelationshipTypeName
--FROM   @ForImport
--ORDER  BY RelationshipTypeName

--SELECT DISTINCT AssignedBHW_ID_Employee,
--                AssignedBHW
--FROM   @ForImport
--ORDER  BY AssignedBHW

Update @ForImport
SET    ID_Resident = resident.ID
FROM   @ForImport import
       INNER JOIN tResident resident
               on import.Guid = resident.ImportGuid
WHERE  ID_Company = @ID_Company

Update @Forimport
SET    Name_Resident = LastName + ', ' + FirstName + ' ' + MiddleName

Update @Forimport
SET    ID_OccupationalStatus = 2
WHERE  LEN(ISNULL(Occupation, '')) > 0

INSERT INTO [dbo].[tResident]
            ([Name],
             ImportGuid,
             [IsActive],
             [ID_Company],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             comment)
SELECT Name_Resident,
       Guid,
       1,
       @ID_Company,
       GETDATE(),
       GETDATE(),
       1,
       1,
       'Imported '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
FROM   @ForImport
WHERE  ID_Resident IS NULL
order  by Name_Resident

Update @ForImport
SET    ID_Resident = resident.ID
FROM   @ForImport import
       INNER JOIN tResident resident
               on import.Guid = resident.ImportGuid
WHERE  ID_Company = @ID_Company

SELECT *
FROM   @ForImport
Order  by RowIndex

Update tResident
SET    Name = import.Name_Resident,
       address = import.Address,
       ID_Gender = import.ID_Gender,
       ContactNumber = import.ContactNumber,
       DateBirth = import.DateBirth,
       IsHeadofFamily = import.IsHeadofFamily,
       ID_OccupationalStatus = import.ID_OccupationalStatus,
       Occupation = import.Occupation
FROM   tResident resident
       INNER JOIN @ForImport import
               on import.Guid = resident.ImportGuid
WHERE  ID_Company = @ID_Company 
