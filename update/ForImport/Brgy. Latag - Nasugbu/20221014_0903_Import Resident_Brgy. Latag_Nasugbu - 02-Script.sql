GO

DEclare @GUID_Company VARCHAR(MAX) = '7E491B8D-BB3C-4F0D-9CCA-7608FD256A3D'

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @ForImport TABLE
  (
     ID_Resident             INT,
     Name_Resident           VARCHAR(MAX),
     ID_CivilStatus_Resident INT,
     ID_Gender_Resident      INT,
     IsHeadofFamily_Resident BIT,
     DateBIrth               Datetime,
     LastName                VARCHAR(MAX),
     FirstName               VARCHAR(MAX),
     MiddleName              VARCHAR(MAX),
     DateBirth_String        VARCHAR(MAX),
     CivilStatus_String      VARCHAR(MAX),
     Gender_String           VARCHAR(MAX),
     Column1_String          VARCHAR(MAX),
     Guid                    VARCHAR(MAX)
  )

INSERT @ForImport
       (LastName,
        FirstName,
        MiddleName,
        DateBirth_String,
        CivilStatus_String,
        Gender_String,
        Column1_String,
        Guid)
SELECT dbo.fGetCleanedString([Last Name]),
       dbo.fGetCleanedString([First Name]),
       dbo.fGetCleanedString([Middle Name]),
       dbo.fGetCleanedString(Birthdate),
       dbo.fGetCleanedString([Civil Status]),
       dbo.fGetCleanedString(Gender),
       dbo.fGetCleanedString(Column1),
       GUID
FROM   ForImport.[dbo].[barangay_resident_record-Barangay Latag, Nasugbu Batangas]

Update @Forimport
SET    Name_Resident = LastName + ', ' + FirstName + ' ' + MiddleName

DELETE FROM @ForImport
WHERE  TRIM(Name_Resident) = ','

Update @Forimport
SET    IsHeadofFamily_Resident = 1
WHERE  Column1_String = 'Family Head'

Update @Forimport
SET    DateBirth_String = REPLACE(DateBirth_String, '//', '/')

Update @Forimport
SET    DateBirth = TRY_CONVERT(datetime, DateBirth_String)

Update @Forimport
SET    DateBirth = TRY_CONVERT(datetime, '01/08/1985')
WHERE  DateBirth_String = '01/081985'

Update @Forimport
SET    DateBirth = TRY_CONVERT(datetime, '01/19/1984')
WHERE  DateBirth_String = '01/191984'

Update @Forimport
SET    DateBirth = TRY_CONVERT(datetime, '04/03/1994')
WHERE  DateBirth_String = '04/031994'

Update @Forimport
SET    DateBirth = TRY_CONVERT(datetime, '05/06/2008')
WHERE  DateBirth_String = '05/06 2008'

Update @Forimport
SET    DateBirth = TRY_CONVERT(datetime, '05/14/1983')
WHERE  DateBirth_String = '05/14 1983'

Update @Forimport
SET    DateBirth = TRY_CONVERT(datetime, '06/10/1987')
WHERE  DateBirth_String = '06/10 1987'

Update @Forimport
SET    DateBirth = TRY_CONVERT(datetime, '07/06/2021')
WHERE  DateBirth_String = '07/.06/2021'

Update @Forimport
SET    DateBirth = TRY_CONVERT(datetime, '10/11/2010')
WHERE  DateBirth_String = '10/112010'

Update @Forimport
SET    DateBirth = TRY_CONVERT(datetime, '10/29/1990')
WHERE  DateBirth_String = '10/291990'

Update @Forimport
SET    DateBirth = TRY_CONVERT(datetime, '11/04/2020')
WHERE  DateBirth_String = '11/042020'

Update @Forimport
SET    DateBirth = TRY_CONVERT(datetime, '12/31/1986')
WHERE  DateBirth_String = '12/311986'

Update @Forimport
SET    ID_CivilStatus_Resident = 1
where  CivilStatus_String IN ( 'S', 'SINGLE', 'SINGLKE' )

Update @Forimport
SET    ID_Gender_Resident = 1
where  Gender_String = 'M'

Update @Forimport
SET    ID_Gender_Resident = 2
where  Gender_String = 'F'

Update @Forimport
SET    ID_CivilStatus_Resident = 2
where  CivilStatus_String IN ( 'M' )

Update @Forimport
SET    ID_CivilStatus_Resident = 5
where  CivilStatus_String IN ( 'W', 'WIDOWED' )

Update @ForImport
SET    ID_Resident = resident.ID
FROM   @ForImport import
       INNER JOIN tResident resident
               on import.Guid = resident.ImportGuid
WHERE  ID_Company = @ID_Company

INSERT INTO [dbo].[tResident]
            ([Name],
             ImportGuid,
             [IsActive],
             [ID_Company],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             comment)
SELECT Name_Resident,
       Guid,
       1,
       @ID_Company,
       GETDATE(),
       GETDATE(),
       1,
       1,
       'Imported '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
FROM   @ForImport
WHERE  ID_Resident IS NULL
order  by Name_Resident

--SELECT Distinct DateBirth_String
--FROM   @Forimport where DateBIrth is null	
SELECT Distinct CivilStatus_String
FROM   @Forimport
where  ID_CivilStatus_Resident is null

SELECT *
FROM   @Forimport

Update @ForImport
SET    ID_Resident = resident.ID
FROM   @ForImport import
       INNER JOIN tResident resident
               on import.Guid = resident.ImportGuid
WHERE  ID_Company = @ID_Company

Update tResident
SET    Name = import.Name_Resident,
       DateBirth = import.DateBIrth,
       ID_CivilStatus = import.ID_CivilStatus_Resident,
       IsHeadofFamily = import.IsHeadofFamily_Resident
FROM   tResident resident
       INNER JOIN @ForImport import
               on import.Guid = resident.ImportGuid
WHERE  ID_Company = @ID_Company 
