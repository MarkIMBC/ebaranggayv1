/*
ALTER TABLE ForImport.dbo.[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 0-11 mos] 
ADD GUID VARCHAR(MAX) NULL;
GO
ALTER TABLE ForImport.dbo.[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 10-17 yrs.] 
ADD GUID VARCHAR(MAX) NULL;
GO
ALTER TABLE ForImport.dbo.[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 12-23 mos] 
ADD GUID VARCHAR(MAX) NULL;
GO
ALTER TABLE ForImport.dbo.[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 18-59 yrs] 
ADD GUID VARCHAR(MAX) NULL;
GO
ALTER TABLE ForImport.dbo.[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 24-59 mos] 
ADD GUID VARCHAR(MAX) NULL;
GO
ALTER TABLE ForImport.dbo.[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 5-9 yrs.] 
ADD GUID VARCHAR(MAX) NULL;
GO
ALTER TABLE ForImport.dbo.[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 60 onwards] 
ADD GUID VARCHAR(MAX) NULL;
GO
Update ForImport.dbo.[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 0-11 mos]   SET GUID = NEWID()
Update ForImport.dbo.[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 10-17 yrs.]  SET GUID = NEWID()
Update ForImport.dbo.[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 12-23 mos] SET GUID = NEWID()
Update ForImport.dbo.[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 18-59 yrs]  SET GUID = NEWID()
Update ForImport.dbo.[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 24-59 mos]  SET GUID = NEWID()
Update ForImport.dbo.[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 5-9 yrs.]	   SET GUID = NEWID()
Update ForImport.dbo.[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 60 onwards] SET GUID = NEWID()
GO
exec _pAddModelProperty
  'tResident',
  'Lastname',
  1

GO

exec _pAddModelProperty
  'tResident',
  'Firstname',
  1

GO

exec _pAddModelProperty
  'tResident',
  'Middlename',
  1

GO
exec _pAddModelProperty
  'tResident',
  'Suffix',
  1

GO
exec _pAddModelProperty
  'tResident',
  'ImportGuid',
  1

exec _pAddModelProperty
  'tResident',
  'DateBirth',
  5

GO

exec _pRefreshAllViews
GO

go
*/
DEclare @GUID_Company VARCHAR(MAX) = '2BBA37C5-5531-4BBD-864D-6E3506A9217D'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @ForImport TABLE
  (
     ID_Resident                    INT,
     Lastname_Resident              VARCHAR(MAX),
     Firstname_Resident             VARCHAR(MAX),
     Middlename_Resident            VARCHAR(MAX),
     Suffix_Resident                VARCHAR(MAX),
     Address_Resident               VARCHAR(MAX),
     ID_Gender                      INT,
     ImportGuid_Resident            VARCHAR(MAX),
     ContactNumber                  VARCHAR(MAX),
     DateBirth                      DateTime,
     ID_CivilStatus                 INT,
     ID_OccupationalStatus_Resident INT,
     Occupation_Resident            VARCHAR(MAX),
     [Employment_Status*]           VARCHAR(MAX),
     [Profession*]                  VARCHAR(MAX),
     [Vaccination Status]           VARCHAR(MAX)
  )

INSERT @ForImport
       (Lastname_Resident,
        Firstname_Resident,
        Middlename_Resident,
        Suffix_Resident,
        Address_Resident,
        ID_Gender,
        ImportGuid_Resident,
        ContactNumber,
        DateBirth,
        ID_CivilStatus,
        [Employment_Status*],
        [Profession*],
        [Vaccination Status])
SELECT dbo.fGetCleanedString([Last_Name*]),
       dbo.fGetCleanedString([First_Name*]),
       dbo.fGetCleanedString([Middle_Name*]),
       dbo.fGetCleanedString([Suffix*]),
       dbo.fGetCleanedString([Current_Residence:_Unit/Building/House_Number,_Street_Name*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Barangay*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Municipality/City*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Province*] + ' ' + dbo.fGetCleanedString([Current_Residence:_Region])) Address,
       CASE
         WHEN [Sex*] LIKE '%Female%' THEN 2
         ELSE
           CASE
             WHEN [Sex*] LIKE '%_Male%' THEN 1
             ELSE NULL
           END
       END,
       Guid
       + '[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 0-11 mos]',
       dbo.fGetCleanedString([Contact No.]),
       TRY_CONVERT(datetime, dbo.fGetCleanedString(( [Birthdate_mm/dd/yyyy_*] ))),
       CASE
         WHEN [Civil_Status*] = '01_Single' THEN 1
         ELSE
           CASE
             WHEN [Civil_Status*] = '02_Married' THEN 2
             ELSE
               CASE
                 WHEN [Civil_Status*] = '03_Widow/Widower' THEN 5
                 ELSE
                   CASE
                     WHEN [Civil_Status*] = '04_Separated/Annulled' THEN 4
                     ELSE
                       CASE
                         WHEN [Civil_Status*] = '05_Living_with_Partner' THEN 3
                         ELSE NULL
                       END
                   END
               END
           END
       END,
       ''                                                                                                                [Employment_Status*],
       ''                                                                                                                [Profession*],
       [Vaccination Status]
FROM   ForImport.dbo.[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 0-11 mos]
WHERE  LEN(TRIM([Last_Name*])) > 0
UNION ALL /**********************************************************************************/
SELECT dbo.fGetCleanedString([Last_Name*]),
       dbo.fGetCleanedString([First_Name*]),
       dbo.fGetCleanedString([Middle_Name*]),
       dbo.fGetCleanedString([Suffix*]),
       dbo.fGetCleanedString([Current_Residence:_Unit/Building/House_Number,_Street_Name*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Barangay*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Municipality/City*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Province*] + ' ' + dbo.fGetCleanedString([Current_Residence:_Region])) Address,
       CASE
         WHEN [Sex*] LIKE '%Female%' THEN 2
         ELSE
           CASE
             WHEN [Sex*] LIKE '%_Male%' THEN 1
             ELSE NULL
           END
       END,
       Guid
       + '[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 10-17 yrs.]',
       dbo.fGetCleanedString([Contact No.]),
       TRY_CONVERT(datetime, dbo.fGetCleanedString(( [Birthdate_mm/dd/yyyy_*] ))),
       CASE
         WHEN [Civil_Status*] = '01_Single' THEN 1
         ELSE
           CASE
             WHEN [Civil_Status*] = '02_Married' THEN 2
             ELSE
               CASE
                 WHEN [Civil_Status*] = '03_Widow/Widower' THEN 5
                 ELSE
                   CASE
                     WHEN [Civil_Status*] = '04_Separated/Annulled' THEN 4
                     ELSE
                       CASE
                         WHEN [Civil_Status*] = '05_Living_with_Partner' THEN 3
                         ELSE NULL
                       END
                   END
               END
           END
       END,
       ''                                                                                                                [Employment_Status*],
       ''                                                                                                                [Profession*],
       [Vaccination Status]
FROM   ForImport.dbo.[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 10-17 yrs.]
WHERE  LEN(TRIM([Last_Name*])) > 0
UNION ALL /**********************************************************************************/
SELECT dbo.fGetCleanedString([Last_Name*]),
       dbo.fGetCleanedString([First_Name*]),
       dbo.fGetCleanedString([Middle_Name*]),
       dbo.fGetCleanedString([Suffix*]),
       dbo.fGetCleanedString([Current_Residence:_Unit/Building/House_Number,_Street_Name*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Barangay*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Municipality/City*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Province*] + ' ' + dbo.fGetCleanedString([Current_Residence:_Region])) Address,
       CASE
         WHEN [Sex*] LIKE '%Female%' THEN 2
         ELSE
           CASE
             WHEN [Sex*] LIKE '%_Male%' THEN 1
             ELSE NULL
           END
       END,
       Guid
       + '[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 12-23 mos]',
       dbo.fGetCleanedString([Contact No.]),
       TRY_CONVERT(datetime, dbo.fGetCleanedString(( [Birthdate_mm/dd/yyyy_*] ))),
       CASE
         WHEN [Civil_Status*] = '01_Single' THEN 1
         ELSE
           CASE
             WHEN [Civil_Status*] = '02_Married' THEN 2
             ELSE
               CASE
                 WHEN [Civil_Status*] = '03_Widow/Widower' THEN 5
                 ELSE
                   CASE
                     WHEN [Civil_Status*] = '04_Separated/Annulled' THEN 4
                     ELSE
                       CASE
                         WHEN [Civil_Status*] = '05_Living_with_Partner' THEN 3
                         ELSE NULL
                       END
                   END
               END
           END
       END,
       ''                                                                                                                [Employment_Status*],
       ''                                                                                                                [Profession*],
       [Vaccination Status]
FROM   ForImport.dbo.[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 12-23 mos]
WHERE  LEN(TRIM([Last_Name*])) > 0
UNION ALL /**********************************************************************************/
SELECT dbo.fGetCleanedString([Last_Name*]),
       dbo.fGetCleanedString([First_Name*]),
       dbo.fGetCleanedString([Middle_Name*]),
       dbo.fGetCleanedString([Suffix*]),
       dbo.fGetCleanedString([Current_Residence:_Unit/Building/House_Number,_Street_Name*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Barangay*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Municipality/City*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Province*] + ' ' + dbo.fGetCleanedString([Current_Residence:_Region])) Address,
       CASE
         WHEN [Sex*] LIKE '%Female%' THEN 2
         ELSE
           CASE
             WHEN [Sex*] LIKE '%_Male%' THEN 1
             ELSE NULL
           END
       END,
       Guid
       + '[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 18-59 yrs]',
       dbo.fGetCleanedString([Contact_No.*]),
       TRY_CONVERT(datetime, dbo.fGetCleanedString(( [Birthdate_mm/dd/yyyy_*] ))),
       CASE
         WHEN [Civil_Status*] = '01_Single' THEN 1
         ELSE
           CASE
             WHEN [Civil_Status*] = '02_Married' THEN 2
             ELSE
               CASE
                 WHEN [Civil_Status*] = '03_Widow/Widower' THEN 5
                 ELSE
                   CASE
                     WHEN [Civil_Status*] = '04_Separated/Annulled' THEN 4
                     ELSE
                       CASE
                         WHEN [Civil_Status*] = '05_Living_with_Partner' THEN 3
                         ELSE NULL
                       END
                   END
               END
           END
       END,
       [Employment_Status*],
       [Profession*],
	    [Vaccinated?]      [Vaccination Status]
FROM   ForImport.dbo.[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 18-59 yrs]
WHERE  LEN(TRIM([Last_Name*])) > 0
UNION ALL /**********************************************************************************/
SELECT dbo.fGetCleanedString([Last_Name*]),
       dbo.fGetCleanedString([First_Name*]),
       dbo.fGetCleanedString([Middle_Name*]),
       dbo.fGetCleanedString([Suffix*]),
       dbo.fGetCleanedString([Current_Residence:_Unit/Building/House_Number,_Street_Name*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Barangay*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Municipality/City*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Province*] + ' ' + dbo.fGetCleanedString([Current_Residence:_Region])) Address,
       CASE
         WHEN [Sex*] LIKE '%Female%' THEN 2
         ELSE
           CASE
             WHEN [Sex*] LIKE '%_Male%' THEN 1
             ELSE NULL
           END
       END,
       Guid
       + '[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 24-59 mos]',
       dbo.fGetCleanedString([Contact No.]),
       TRY_CONVERT(datetime, dbo.fGetCleanedString(( [Birthdate_mm/dd/yyyy_*] ))),
       CASE
         WHEN [Civil_Status*] = '01_Single' THEN 1
         ELSE
           CASE
             WHEN [Civil_Status*] = '02_Married' THEN 2
             ELSE
               CASE
                 WHEN [Civil_Status*] = '03_Widow/Widower' THEN 5
                 ELSE
                   CASE
                     WHEN [Civil_Status*] = '04_Separated/Annulled' THEN 4
                     ELSE
                       CASE
                         WHEN [Civil_Status*] = '05_Living_with_Partner' THEN 3
                         ELSE NULL
                       END
                   END
               END
           END
       END,
       ''                                                                                                                [Employment_Status*],
       ''                                                                                                                [Profession*],
       [Vaccination Status]
FROM   ForImport.dbo.[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 24-59 mos]
WHERE  LEN(TRIM([Last_Name*])) > 0
UNION ALL /**********************************************************************************/
SELECT dbo.fGetCleanedString([Last_Name*]),
       dbo.fGetCleanedString([First_Name*]),
       dbo.fGetCleanedString([Middle_Name*]),
       dbo.fGetCleanedString([Suffix*]),
       dbo.fGetCleanedString([Current_Residence:_Unit/Building/House_Number,_Street_Name*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Barangay*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Municipality/City*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Province*] + ' ' + dbo.fGetCleanedString([Current_Residence:_Region])) Address,
       CASE
         WHEN [Sex*] LIKE '%Female%' THEN 2
         ELSE
           CASE
             WHEN [Sex*] LIKE '%_Male%' THEN 1
             ELSE NULL
           END
       END,
       Guid
       + '[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 5-9 yrs.]',
       dbo.fGetCleanedString([Contact No.]),
       TRY_CONVERT(datetime, dbo.fGetCleanedString(( [Birthdate_mm/dd/yyyy_*] ))),
       CASE
         WHEN [Civil_Status*] = '01_Single' THEN 1
         ELSE
           CASE
             WHEN [Civil_Status*] = '02_Married' THEN 2
             ELSE
               CASE
                 WHEN [Civil_Status*] = '03_Widow/Widower' THEN 5
                 ELSE
                   CASE
                     WHEN [Civil_Status*] = '04_Separated/Annulled' THEN 4
                     ELSE
                       CASE
                         WHEN [Civil_Status*] = '05_Living_with_Partner' THEN 3
                         ELSE NULL
                       END
                   END
               END
           END
       END,
       ''                                                                                                                [Employment_Status*],
       ''                                                                                                                [Profession*],
       [Vaccination Status]
FROM   ForImport.dbo.[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 5-9 yrs.]
WHERE  LEN(TRIM([Last_Name*])) > 0
UNION ALL /**********************************************************************************/
SELECT dbo.fGetCleanedString([Last_Name*]),
       dbo.fGetCleanedString([First_Name*]),
       dbo.fGetCleanedString([Middle_Name*]),
       dbo.fGetCleanedString([Suffix*]),
       dbo.fGetCleanedString([Current_Residence:_Unit/Building/House_Number,_Street_Name*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Barangay*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Municipality/City*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Province*] + ' ' + dbo.fGetCleanedString([Current_Residence:_Region])) Address,
       CASE
         WHEN [Sex*] LIKE '%Female%' THEN 2
         ELSE
           CASE
             WHEN [Sex*] LIKE '%_Male%' THEN 1
             ELSE NULL
           END
       END,
       Guid
       + '[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 60 onwards]',
       dbo.fGetCleanedString([Contact_No.*]),
       TRY_CONVERT(datetime, dbo.fGetCleanedString(( [Birthdate_mm/dd/yyyy_*] ))),
       CASE
         WHEN [Civil_Status*] = '01_Single' THEN 1
         ELSE
           CASE
             WHEN [Civil_Status*] = '02_Married' THEN 2
             ELSE
               CASE
                 WHEN [Civil_Status*] = '03_Widow/Widower' THEN 5
                 ELSE
                   CASE
                     WHEN [Civil_Status*] = '04_Separated/Annulled' THEN 4
                     ELSE
                       CASE
                         WHEN [Civil_Status*] = '05_Living_with_Partner' THEN 3
                         ELSE NULL
                       END
                   END
               END
           END
       END,
       [Employment_Status*],
       [Profession*]
FROM   ForImport.dbo.[BARANGAY_SAN_JUAN_STO._TOMAS_BATANGAS_CENSUS(1) - 60 onwards]
WHERE  LEN(TRIM([Last_Name*])) > 0
UNION ALL
SELECT dbo.fGetCleanedString([Last_Name*]),
       dbo.fGetCleanedString([First_Name*]),
       dbo.fGetCleanedString([Middle_Name*]),
       dbo.fGetCleanedString([Suffix*]),
       dbo.fGetCleanedString([Current_Residence:_Unit/Building/House_Number,_Street_Name*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Barangay*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Municipality/City*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Province*] + ' ' + dbo.fGetCleanedString([Current_Residence:_Region])) Address,
       CASE
         WHEN [Sex*] LIKE '%Female%' THEN 2
         ELSE
           CASE
             WHEN [Sex*] LIKE '%_Male%' THEN 1
             ELSE NULL
           END
       END,
       Guid
       + '[BARANGAY-SAN-JUAN-STO.-TOMAS-BATANGAS-CENSUS_____2 - 0-11 mos]',
       dbo.fGetCleanedString([Contact No.]),
       TRY_CONVERT(datetime, dbo.fGetCleanedString(( [Birthdate_mm/dd/yyyy_*] ))),
       CASE
         WHEN [Civil_Status*] = '01_Single' THEN 1
         ELSE
           CASE
             WHEN [Civil_Status*] = '02_Married' THEN 2
             ELSE
               CASE
                 WHEN [Civil_Status*] = '03_Widow/Widower' THEN 5
                 ELSE
                   CASE
                     WHEN [Civil_Status*] = '04_Separated/Annulled' THEN 4
                     ELSE
                       CASE
                         WHEN [Civil_Status*] = '05_Living_with_Partner' THEN 3
                         ELSE NULL
                       END
                   END
               END
           END
       END,
       ''                                                                                                                [Employment_Status*],
       ''                                                                                                                [Profession*],
       [Vaccination Status]
FROM   ForImport.dbo.[BARANGAY-SAN-JUAN-STO.-TOMAS-BATANGAS-CENSUS_____2 - 0-11 mos]
WHERE  LEN(TRIM([Last_Name*])) > 0
UNION ALL /**********************************************************************************/
SELECT dbo.fGetCleanedString([Last_Name*]),
       dbo.fGetCleanedString([First_Name*]),
       dbo.fGetCleanedString([Middle_Name*]),
       dbo.fGetCleanedString([Suffix*]),
       dbo.fGetCleanedString([Current_Residence:_Unit/Building/House_Number,_Street_Name*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Barangay*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Municipality/City*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Province*] + ' ' + dbo.fGetCleanedString([Current_Residence:_Region])) Address,
       CASE
         WHEN [Sex*] LIKE '%Female%' THEN 2
         ELSE
           CASE
             WHEN [Sex*] LIKE '%_Male%' THEN 1
             ELSE NULL
           END
       END,
       Guid
       + '[BARANGAY-SAN-JUAN-STO.-TOMAS-BATANGAS-CENSUS_____2 - 10-17 yrs]',
       dbo.fGetCleanedString([Contact No.]),
       TRY_CONVERT(datetime, dbo.fGetCleanedString(( [Birthdate_mm/dd/yyyy_*] ))),
       CASE
         WHEN [Civil_Status*] = '01_Single' THEN 1
         ELSE
           CASE
             WHEN [Civil_Status*] = '02_Married' THEN 2
             ELSE
               CASE
                 WHEN [Civil_Status*] = '03_Widow/Widower' THEN 5
                 ELSE
                   CASE
                     WHEN [Civil_Status*] = '04_Separated/Annulled' THEN 4
                     ELSE
                       CASE
                         WHEN [Civil_Status*] = '05_Living_with_Partner' THEN 3
                         ELSE NULL
                       END
                   END
               END
           END
       END,
       ''                                                                                                                [Employment_Status*],
       ''                                                                                                                [Profession*],
       [Vaccination Status]
FROM   ForImport.dbo.[BARANGAY-SAN-JUAN-STO.-TOMAS-BATANGAS-CENSUS_____2 - 10-17 yrs]
WHERE  LEN(TRIM([Last_Name*])) > 0
UNION ALL /**********************************************************************************/
SELECT dbo.fGetCleanedString([Last_Name*]),
       dbo.fGetCleanedString([First_Name*]),
       dbo.fGetCleanedString([Middle_Name*]),
       dbo.fGetCleanedString([Suffix*]),
       dbo.fGetCleanedString([Current_Residence:_Unit/Building/House_Number,_Street_Name*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Barangay*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Municipality/City*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Province*] + ' ' + dbo.fGetCleanedString([Current_Residence:_Region])) Address,
       CASE
         WHEN [Sex*] LIKE '%Female%' THEN 2
         ELSE
           CASE
             WHEN [Sex*] LIKE '%_Male%' THEN 1
             ELSE NULL
           END
       END,
       Guid
       + '[BARANGAY-SAN-JUAN-STO.-TOMAS-BATANGAS-CENSUS_____2 - 12-23 mos.]',
       dbo.fGetCleanedString([Contact No.]),
       TRY_CONVERT(datetime, dbo.fGetCleanedString(( [Birthdate_mm/dd/yyyy_*] ))),
       CASE
         WHEN [Civil_Status*] = '01_Single' THEN 1
         ELSE
           CASE
             WHEN [Civil_Status*] = '02_Married' THEN 2
             ELSE
               CASE
                 WHEN [Civil_Status*] = '03_Widow/Widower' THEN 5
                 ELSE
                   CASE
                     WHEN [Civil_Status*] = '04_Separated/Annulled' THEN 4
                     ELSE
                       CASE
                         WHEN [Civil_Status*] = '05_Living_with_Partner' THEN 3
                         ELSE NULL
                       END
                   END
               END
           END
       END,
       ''                                                                                                                [Employment_Status*],
       ''                                                                                                                [Profession*],
       [Vaccination Status]
FROM   ForImport.dbo.[BARANGAY-SAN-JUAN-STO.-TOMAS-BATANGAS-CENSUS_____2 - 12-23 mos.]
WHERE  LEN(TRIM([Last_Name*])) > 0
UNION ALL /**********************************************************************************/
SELECT dbo.fGetCleanedString([Last_Name*]),
       dbo.fGetCleanedString([First_Name*]),
       dbo.fGetCleanedString([Middle_Name*]),
       dbo.fGetCleanedString([Suffix*]),
       dbo.fGetCleanedString([Current_Residence:_Unit/Building/House_Number,_Street_Name*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Barangay*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Municipality/City*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Province*] + ' ' + dbo.fGetCleanedString([Current_Residence:_Region])) Address,
       CASE
         WHEN [Sex*] LIKE '%Female%' THEN 2
         ELSE
           CASE
             WHEN [Sex*] LIKE '%_Male%' THEN 1
             ELSE NULL
           END
       END,
       Guid
       + '[BARANGAY-SAN-JUAN-STO.-TOMAS-BATANGAS-CENSUS_____2 - 18-59 yrs]',
       dbo.fGetCleanedString([Contact_No.*]),
       TRY_CONVERT(datetime, dbo.fGetCleanedString(( [Birthdate_mm/dd/yyyy_*] ))),
       CASE
         WHEN [Civil_Status*] = '01_Single' THEN 1
         ELSE
           CASE
             WHEN [Civil_Status*] = '02_Married' THEN 2
             ELSE
               CASE
                 WHEN [Civil_Status*] = '03_Widow/Widower' THEN 5
                 ELSE
                   CASE
                     WHEN [Civil_Status*] = '04_Separated/Annulled' THEN 4
                     ELSE
                       CASE
                         WHEN [Civil_Status*] = '05_Living_with_Partner' THEN 3
                         ELSE NULL
                       END
                   END
               END
           END
       END,
       [Employment_Status*],
       [Profession*],
      [Vaccinated?] [Vaccination Status]
FROM   ForImport.dbo.[BARANGAY-SAN-JUAN-STO.-TOMAS-BATANGAS-CENSUS_____2 - 18-59 yrs]
WHERE  LEN(TRIM([Last_Name*])) > 0
UNION ALL /**********************************************************************************/
SELECT dbo.fGetCleanedString([Last_Name*]),
       dbo.fGetCleanedString([First_Name*]),
       dbo.fGetCleanedString([Middle_Name*]),
       dbo.fGetCleanedString([Suffix*]),
       dbo.fGetCleanedString([Current_Residence:_Unit/Building/House_Number,_Street_Name*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Barangay*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Municipality/City*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Province*] + ' ' + dbo.fGetCleanedString([Current_Residence:_Region])) Address,
       CASE
         WHEN [Sex*] LIKE '%Female%' THEN 2
         ELSE
           CASE
             WHEN [Sex*] LIKE '%_Male%' THEN 1
             ELSE NULL
           END
       END,
       Guid
       + '[BARANGAY-SAN-JUAN-STO.-TOMAS-BATANGAS-CENSUS_____2 - 24-59 mos]',
       dbo.fGetCleanedString([Contact No.]),
       TRY_CONVERT(datetime, dbo.fGetCleanedString(( [Birthdate_mm/dd/yyyy_*] ))),
       CASE
         WHEN [Civil_Status*] = '01_Single' THEN 1
         ELSE
           CASE
             WHEN [Civil_Status*] = '02_Married' THEN 2
             ELSE
               CASE
                 WHEN [Civil_Status*] = '03_Widow/Widower' THEN 5
                 ELSE
                   CASE
                     WHEN [Civil_Status*] = '04_Separated/Annulled' THEN 4
                     ELSE
                       CASE
                         WHEN [Civil_Status*] = '05_Living_with_Partner' THEN 3
                         ELSE NULL
                       END
                   END
               END
           END
       END,
       ''                                                                                                                [Employment_Status*],
       ''                                                                                                                [Profession*],
       [Vaccination Status]
FROM   ForImport.dbo.[BARANGAY-SAN-JUAN-STO.-TOMAS-BATANGAS-CENSUS_____2 - 24-59 mos]
WHERE  LEN(TRIM([Last_Name*])) > 0
UNION ALL /**********************************************************************************/
SELECT dbo.fGetCleanedString([Last_Name*]),
       dbo.fGetCleanedString([First_Name*]),
       dbo.fGetCleanedString([Middle_Name*]),
       dbo.fGetCleanedString([Suffix*]),
       dbo.fGetCleanedString([Current_Residence:_Unit/Building/House_Number,_Street_Name*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Barangay*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Municipality/City*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Province*] + ' ' + dbo.fGetCleanedString([Current_Residence:_Region])) Address,
       CASE
         WHEN [Sex*] LIKE '%Female%' THEN 2
         ELSE
           CASE
             WHEN [Sex*] LIKE '%_Male%' THEN 1
             ELSE NULL
           END
       END,
       Guid
       + '[BARANGAY-SAN-JUAN-STO.-TOMAS-BATANGAS-CENSUS_____2 - 5-9 yrs]',
       dbo.fGetCleanedString([Contact No.]),
       TRY_CONVERT(datetime, dbo.fGetCleanedString(( [Birthdate_mm/dd/yyyy_*] ))),
       CASE
         WHEN [Civil_Status*] = '01_Single' THEN 1
         ELSE
           CASE
             WHEN [Civil_Status*] = '02_Married' THEN 2
             ELSE
               CASE
                 WHEN [Civil_Status*] = '03_Widow/Widower' THEN 5
                 ELSE
                   CASE
                     WHEN [Civil_Status*] = '04_Separated/Annulled' THEN 4
                     ELSE
                       CASE
                         WHEN [Civil_Status*] = '05_Living_with_Partner' THEN 3
                         ELSE NULL
                       END
                   END
               END
           END
       END,
       ''                                                                                                                [Employment_Status*],
       ''                                                                                                                [Profession*],
       [Vaccination Status]
FROM   ForImport.dbo.[BARANGAY-SAN-JUAN-STO.-TOMAS-BATANGAS-CENSUS_____2 - 5-9 yrs]
WHERE  LEN(TRIM([Last_Name*])) > 0
UNION ALL /**********************************************************************************/
SELECT dbo.fGetCleanedString([Last_Name*]),
       dbo.fGetCleanedString([First_Name*]),
       dbo.fGetCleanedString([Middle_Name*]),
       dbo.fGetCleanedString([Suffix*]),
       dbo.fGetCleanedString([Current_Residence:_Unit/Building/House_Number,_Street_Name*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Barangay*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Municipality/City*])
       + ' '
       + dbo.fGetCleanedString([Current_Residence:Province*] + ' ' + dbo.fGetCleanedString([Current_Residence:_Region])) Address,
       CASE
         WHEN [Sex*] LIKE '%Female%' THEN 2
         ELSE
           CASE
             WHEN [Sex*] LIKE '%_Male%' THEN 1
             ELSE NULL
           END
       END,
       Guid
       + '[BARANGAY-SAN-JUAN-STO.-TOMAS-BATANGAS-CENSUS_____2 - 60 onwards]',
       dbo.fGetCleanedString([Contact_No.*]),
       TRY_CONVERT(datetime, dbo.fGetCleanedString(( [Birthdate_mm/dd/yyyy_*] ))),
       CASE
         WHEN [Civil_Status*] = '01_Single' THEN 1
         ELSE
           CASE
             WHEN [Civil_Status*] = '02_Married' THEN 2
             ELSE
               CASE
                 WHEN [Civil_Status*] = '03_Widow/Widower' THEN 5
                 ELSE
                   CASE
                     WHEN [Civil_Status*] = '04_Separated/Annulled' THEN 4
                     ELSE
                       CASE
                         WHEN [Civil_Status*] = '05_Living_with_Partner' THEN 3
                         ELSE NULL
                       END
                   END
               END
           END
       END,
       [Employment_Status*],
       [Profession*]
FROM   ForImport.dbo.[BARANGAY-SAN-JUAN-STO.-TOMAS-BATANGAS-CENSUS_____2 - 60 onwards]
WHERE  LEN(TRIM([Last_Name*])) > 0

Update @ForImport
SET    [Profession*] = REPLACE([Profession*], '16_Barangay_Health Worker', 'Barangay Health Worker')

Update @ForImport
SET    [Profession*] = REPLACE([Profession*], '17_Maintenance_Staff', 'Maintenance Staff')

Update @ForImport
SET    [Profession*] = REPLACE([Profession*], '19_Other Deped Sto. Tomas South', 'Other Deped Sto. Tomas South')

Update @ForImport
SET    [Profession*] = REPLACE([Profession*], '19_Other', 'Other')

Update @ForImport
SET    [Profession*] = REPLACE([Profession*], 'n/a', '')

Update @ForImport
SET    [Profession*] = REPLACE([Profession*], 'None', '')

Update @ForImport
SET    [Profession*] = REPLACE([Profession*], '06_Nurse', 'Nurse')

Update @ForImport
SET    [Profession*] = REPLACE([Profession*], '13_Radiologic_Technologist', 'Radiologic Technologist')

Update @ForImport
SET    [Profession*] = REPLACE([Profession*], '11_Physical_Therapist', 'Physical Therapist')

Update @ForImport
SET    [Profession*] = REPLACE([Profession*], '01_Dental_Hygienist', 'Dental Hygienist')

Update @ForImport
SET    [Profession*] = REPLACE([Profession*], '05_Midwife', 'Midwife')

Update @ForImport
SET    Occupation_Resident = [Profession*]

Update @ForImport
SET    ID_OccupationalStatus_Resident = 2
WHERE  LEN(TRIM(Occupation_Resident)) > 0

Update @ForImport
SET    Address_Resident = REPLACE(Address_Resident, '_41028015_SAN_JUAN', 'Brgy. San Juan')

Update @ForImport
SET    Address_Resident = REPLACE(Address_Resident, '_41028_SANTO_TOMAS', 'Santo Tomas')

Update @ForImport
SET    Address_Resident = REPLACE(Address_Resident, '_0410_BATANGAS', 'Batangas')

Update @ForImport
SET    Address_Resident = REPLACE(Address_Resident, 'CALABARZON', 'Calabarzon')

Update @ForImport
SET    ID_Resident = resident.ID
FROM   @ForImport import
       INNER JOIN tResident resident
               on import.ImportGuid_Resident = resident.ImportGuid
WHERE  ID_Company = @ID_Company

INSERT INTO [dbo].[tResident]
            ([Name],
             [Lastname],
             [Firstname],
             [Middlename],
             Suffix,
             Address,
             ID_Gender,
             ImportGuid,
             [IsActive],
             [ID_Company],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             comment)
SELECT Lastname_Resident + ', ' + Firstname_Resident
       + ' ' + Middlename_Resident,
       Lastname_Resident,
       Firstname_Resident,
       Middlename_Resident,
       Suffix_Resident,
       Address_Resident,
       ID_Gender,
       ImportGuid_Resident,
       1,
       246,
       GETDATE(),
       GETDATE(),
       1,
       1,
       'Imported '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
FROM   @ForImport
WHERE  ID_Resident IS NULL
order  by Lastname_Resident + ' ' + Lastname_Resident + ' '
          + Firstname_Resident

Update @ForImport
SET    ID_Resident = resident.ID
FROM   @ForImport import
       INNER JOIN tResident resident
               on import.ImportGuid_Resident = resident.ImportGuid
WHERE  ID_Company = @ID_Company

Update tResident
SET    ContactNumber = import.ContactNumber,
       DateBirth = import.DateBirth,
       ID_CivilStatus = import.ID_CivilStatus,
       Address = import.Address_Resident,
       ID_OccupationalStatus = import.ID_OccupationalStatus_Resident,
       Occupation = import.Occupation_Resident
FROM   tResident resident
       INNER JOIN @ForImport import
               on import.ImportGuid_Resident = resident.ImportGuid
WHERE  ID_Company = @ID_Company

SELECT *
FROM   @ForImport

SELECT Lastname_Resident,
       Firstname_Resident,
       Middlename_Resident,
       ContactNumber,
       Address_Resident,
       Count(*)
FROM   @ForImport
GROUP  BY Lastname_Resident,
          Firstname_Resident,
          Middlename_Resident,
          ContactNumber,
          Address_Resident
HAVING COUNT(*) > 1 
