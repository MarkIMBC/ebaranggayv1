GO

iF COL_LENGTH('tResident', 'ReferenceID') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResident',
        'ReferenceID',
        1
  END

GO

exec _pRefreshAllViews

GO

DEclare @GUID_Company VARCHAR(MAX) = 'F4018D24-157C-4BA9-9B1E-6BBCDB039A63'

IF(SELECT Count(*)
   FROM   tCompany
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   tCompany
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @ForImport TABLE
  (
     RowIndex                     INT IDENTITY(1, 1),
     ID_Resident                  INT,
     Name_Resident                VARCHAR(MAX),
     Lastname_Resident            VARCHAR(MAX),
     Firstname_Resident           VARCHAR(MAX),
     Middlename_Resident          VARCHAR(MAX),
     Suffix_Resident              VARCHAR(MAX),
     Address                      VARCHAR(MAX),
     HouseHoldNumber              VARCHAR(MAX),
     HouseNumber                  VARCHAR(MAX),
     ContactNumber                VARCHAR(MAX),
     FamilyNumber                 VARCHAR(MAX),
     IsHeadofFamily               Bit,
     ReferenceID                  VARCHAR(MAX),
     DateBirth                    DateTime,
     Occupation                   VARCHAR(MAX),
     ID_EducationalLevel          INT,
     IsVoter                      BIT,
     ID_Gender                    INT,
     ID_CivilStatus               INT,
     ImportGuid_Resident          VARCHAR(MAX),
     Import_IsVoter               VARCHAR(MAX),
     Import_EducationalAttainment VARCHAR(MAX),
     Import_Gender                VARCHAR(MAX),
     Import_CivilStatus           VARCHAR(MAX),
     Import_Purok                 VARCHAR(MAX)
  )

INSERT @ForImport
       (Lastname_Resident,
        Firstname_Resident,
        Middlename_Resident,
        Suffix_Resident,
        HouseHoldNumber,
        HouseNumber,
      --  FamilyNumber,
        ContactNumber,
     --   ReferenceID,
        Occupation,
        Address,
        ImportGuid_Resident,
        Import_IsVoter,
        Import_EducationalAttainment,
        Import_Gender,
        Import_CivilStatus)
SELECT dbo.fGetCleanedString([SURNAME]),
       dbo.fGetCleanedString([FIRST NAME]),
       dbo.fGetCleanedString([MIDDLE NAME]),
       dbo.fGetCleanedString([EXT]),
       dbo.fGetCleanedString([HOUSEHOLD NO.]),
     -- dbo.fGetCleanedString([NO. OF FAMILIES]),
       dbo.fGetCleanedString([HOUSE NO]),
       dbo.fGetCleanedString([CONTACT NO]),
    --   dbo.fGetCleanedString([ID NO]),
       dbo.fGetCleanedString([JOB DESC]),
       'Purok ' + dbo.fGetCleanedString(PUROK),
       dbo.fGetCleanedString([GIUD]),
       dbo.fGetCleanedString([REGISTERED VOTERS]),
       dbo.fGetCleanedString([EDUCATIONAL ATTAINMENT]),
       dbo.fGetCleanedString(SEX),
       dbo.fGetCleanedString(STATUS)
FROM   ForImport.dbo.[San_Guillermo_Brgy_Profile_May_2022_012_Sheet_1]

DELETE FROM @ForImport
WHERE  LEN(Lastname_Resident) = 0
       AND LEN(Firstname_Resident) = 0
       AND LEN(Middlename_Resident) = 0

-- HouseHoldNo---
DECLARE @_tempHOUSEHOLDNO VARCHAR(MAX) = ''
DECLARE @_tempNumberOfFamily VARCHAR(MAX) = ''
DECLARE @Counter int =1
DECLARE @LastRowIndex int =0
DECLARE @RowCount int =0

SELECT @LastRowIndex = MAX(RowIndex)
FROM   @ForImport

SELECT @RowCount = Count(*)
FROM   @ForImport

WHILE ( @Counter <= @LastRowIndex )
  BEGIN
      DECLARE @HouseHoldNumber VARCHAR(MAX) = ''
      DECLARE @NumberOfFamily VARCHAR(MAX) = ''

      SELECT @HouseHoldNumber = HouseHoldNumber,
             @NumberOfFamily = FamilyNumber
      FROM   @ForImport
      WHERE  RowIndex = @Counter

      if( @HouseHoldNumber <> @_tempHOUSEHOLDNO
          AND LEN(@HouseHoldNumber) > 0 )
        BEGIN
            SET @_tempHOUSEHOLDNO = @HouseHoldNumber
        END

      if( @NumberOfFamily <> @_tempNumberOfFamily
          AND LEN(@NumberOfFamily) > 0 )
        BEGIN
            SET @_tempNumberOfFamily = @NumberOfFamily
        END

      Update @ForImport
      SET    HouseHoldNumber = @_tempHOUSEHOLDNO,
             FamilyNumber = @_tempNumberOfFamily
      WHERE  RowIndex = @Counter

      SET @Counter = @Counter + 1
  END

--Name---
Update @ForImport
SET    Name_Resident = CASE
                         WHEN LEN(Lastname_Resident) = 0 THEN Middlename_Resident
                         ELSE Lastname_Resident
                       END
                       + ', ' + Firstname_Resident + ' '
                       + CASE
                           WHEN LEN(Lastname_Resident) = 0 THEN ''
                           ELSE Middlename_Resident
                         END

UPdate @ForImport
SET    Name_Resident = Name_Resident
                       + CASE
                           WHEN LEN(Suffix_Resident) > 0 THEN ' ' + Suffix_Resident
                           ELSE ''
                         END

--Head of Family--
Update @ForImport
SET    IsHeadofFamily = Case
                          when CATEGORY = 'H' THEN 1
                          ELSE NULL
                        END
FROM   @ForImport import
       inner join (SELECT dbo.fGetCleanedString([CATEGORY]) CATEGORY,
                          GIUD
                   FROM   ForImport.dbo.[San_Guillermo_Brgy_Profile_May_2022_012_Sheet_1]) excel
               on import.ImportGuid_Resident = excel.GIUD

--Date Birth--
Update @ForImport
SET    DateBirth = excel.DateBirth
FROM   @ForImport import
       inner join (SELECT TRY_CONVERT(Datetime, dbo.fGetCleanedString([B-DAY])) DateBirth,
                          GIUD
                   FROM   ForImport.dbo.[San_Guillermo_Brgy_Profile_May_2022_012_Sheet_1]) excel
               on import.ImportGuid_Resident = excel.GIUD

Update @ForImport
SET    DateBirth = NULL
where  DateBirth = '1900-01-01 00:00:00.000'

--IsVoter--
Update @ForImport
SET    IsVoter = SystemManagement.dbo.fGet_Ebarangay_BitValueBySearchTag(Import_IsVoter)

-- Gender --
Update @ForImport
SET    ID_Gender = SystemManagement.dbo.fGet_Ebarangay_GenderBySearchTag(Import_Gender)

SELECT DISTINCT Import_Gender
FROM   @ForImport
WHERE  ID_Gender IS NULL

-- CivilStatus --
Update @ForImport
SET    ID_CivilStatus = SystemManagement.dbo.fGet_Ebarangay_CivilStatusBySearchTag(Import_CivilStatus)

SELECT DISTINCT Import_CivilStatus
FROM   @ForImport
WHERE  ID_CivilStatus IS NULL

-- Educational Level--
Update @ForImport
SET    ID_EducationalLevel = SystemManagement.dbo.fGet_Ebarangay_EducationalLevelBySearchTag(Import_EducationalAttainment)

SELECT DISTINCT Import_EducationalAttainment
FROM   @ForImport
WHERE  ID_EducationalLevel IS NULL

Update @ForImport
SET    ID_Resident = resident.ID
FROM   @ForImport import
       INNER JOIN tResident resident
               on import.ImportGuid_Resident = resident.ImportGuid
WHERE  ID_Company = @ID_Company

INSERT INTO [dbo].[tResident]
            (ImportGuid,
             [IsActive],
             [ID_Company],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             comment)
SELECT ImportGuid_Resident,
       1,
       @ID_Company,
       GETDATE(),
       GETDATE(),
       1,
       1,
       'Imported '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
FROM   @ForImport
WHERE  ID_Resident IS NULL

Update @ForImport
SET    ID_Resident = resident.ID
FROM   @ForImport import
       INNER JOIN tResident resident
               on import.ImportGuid_Resident = resident.ImportGuid
WHERE  ID_Company = @ID_Company


SELECT *
FROM   @ForImport

Update tResident
SET    Name = import.Name_Resident,
       ID_Gender = import.ID_Gender,
       ID_CivilStatus = import.ID_CivilStatus,
       IsHeadofFamily = import.IsHeadofFamily,
       DateBirth = import.DateBirth,
       ContactNumber = import.ContactNumber,
       ReferenceID = import.ReferenceID,
       Occupation = import.Occupation,
       ID_OccupationalStatus = CASE
                                 WHEN LEN(import.Occupation) > 0 THEN 2
                                 ELSE NULL
                               END,
       ID_EducationalLevel = import.ID_EducationalLevel,
       IsVoter = import.IsVoter,  Address = import.Address
FROM   tResident resident
       INNER JOIN @ForImport import
               on resident.ID = import.ID_Resident

			   GO
SELECT *
FROM   ForImport.[dbo].[San_Guillermo_Brgy_Profile_May_2022_012_Sheet_1] 
