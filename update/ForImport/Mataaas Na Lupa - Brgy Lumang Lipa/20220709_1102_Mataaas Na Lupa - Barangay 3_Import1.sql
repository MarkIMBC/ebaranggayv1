IF OBJECT_ID('tempdb..#ImportInvalidValues') IS NOT NULL
  BEGIN
      DROP PROC _________pAddImportInvalidValuesColumns;

      DROP TABLE #ImportInvalidValues;
  END

GO

CREATE TABLE #ImportInvalidValues
  (
     RowIndex INT
  )

GO

CREATE OR
ALTER PROC _________pAddImportInvalidValuesColumns (@ColumnName VARCHAR(MAX))
as
  BEGIN
      DECLARE @sql VARCHAR(MAX) = '
		ALTER TABLE #ImportInvalidValues
		  ADD /*ColumnName*/ VARCHAR(MAX);

		ALTER TABLE #ImportInvalidValues
		  ADD CONSTRAINT DF_ImportInvalidValues_/*ColumnName*/
		  DEFAULT '''' FOR /*ColumnName*/;

		ALTER TABLE #ImportInvalidValues
		  ADD /*ColumnName*/_Count VARCHAR(MAX);

		ALTER TABLE #ImportInvalidValues
		  ADD CONSTRAINT DF_ImportInvalidValues_/*ColumnName*/_Count
		  DEFAULT '''' FOR /*ColumnName*/_Count;
	  '
      DECLARE @sqlUpdate VARCHAR(MAX) = '
		
		Update #ImportInvalidValues SET /*ColumnName*/ = '''' WHERE /*ColumnName*/ IS NULL;
		Update #ImportInvalidValues SET /*ColumnName*/_Count = '''' WHERE /*ColumnName*/_Count IS NULL;
	  '

      SET @sql = REPLACE(@sql, '/*ColumnName*/', @ColumnName)
      SET @sqlUpdate = REPLACE(@sqlUpdate, '/*ColumnName*/', @ColumnName)

      exec (@sql)

      exec (@sqlUpdate)

      IF(SELECT COUNT(*)
         FROM   #ImportInvalidValues) = 0
        BEGIN
            DECLARE @Counter INT

            SET @Counter=1

            WHILE ( @Counter <= 200 )
              BEGIN
                  INSERT #ImportInvalidValues
                         (RowIndex)
                  VALUES (@Counter)

                  SET @Counter = @Counter + 1
              END
        END
  END

GO

GO

exec _________pAddImportInvalidValuesColumns
  'Education_Import';

exec _________pAddImportInvalidValuesColumns
  'Occupation_Import';

exec _________pAddImportInvalidValuesColumns
  'Birthday_Import';

GO

DEclare @GUID_Company VARCHAR(MAX) = 'A805BD00-701C-4CDC-BF60-745B9DA208E8'

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @ForImport TABLE
  (
     ID_Resident         INT,
     Name_Resident       VARCHAR(MAX),
     ImportGuid_Resident VARCHAR(MAX),
     Address             VARCHAR(MAX),
     DateBirth           DATETIME,
     ID_Gender           INT,
     Occupation          VARCHAR(MAX),
     ContactNumber       VARCHAR(MAX),
     ID_CivilStatus      INT,
     ID_EducationalLevel INT,
     ID_Program          INT,
     RowIndex            INT,
     Gender_Import       VARCHAR(MAX),
     Birthday_Import     VARCHAR(MAX),
     CivilStatus_Import  VARCHAR(MAX),
     Address_Import      VARCHAR(MAX),
     Occupation_Import   VARCHAR(MAX),
     Education_Import    VARCHAR(MAX),
     Program_Import      VARCHAR(MAX)
  )

INSERT @ForImport
       (Name_Resident,
        ImportGuid_Resident,
        RowIndex,
        Gender_Import,
        Birthday_Import,
        CivilStatus_Import,
        Address_Import,
        Occupation_Import,
        Education_Import,
        Program_Import)
SELECT CASE
         WHEN LEN(tbl.[Last name]) = 0 THEN tbl.[Middle name]
         ELSE tbl.[Last name]
       END
       + ', ' + tbl.[First Name] + ' '
       + CASE
           WHEN LEN(tbl.[Last name]) = 0 THEN ''
           ELSE tbl.[Middle name]
         END Name,
       tbl. [GIUD],
       RowINdex,
       import.Gender,
       import.Birthday,
       import.[Civil Status],
       import.Purok,
       import.Occupation,
       dbo.fGetCleanedString(import.Education),
       dbo.fGetCleanedString(import. [SSS/Senior/GSIS])
FROM   (SELECT dbo.fGetCleanedString([Last name])   [Last name],
               dbo.fGetCleanedString([First Name])  [First Name],
               dbo.fGetCleanedString([Middle name]) [Middle name],
               [GIUD],
               CONVERT(INT, [#])                    RowINdex
        FROM   ForImport.[dbo].[for barangay III - Mataas na kahoy (Importing)]) tbl
       INNER JOIN ForImport.[dbo].[for barangay III - Mataas na kahoy (Importing)] import
               on tbl.GIUD = import.GIUD
ORDER  BY RowINdex DESC

/*              Update Program               */
UPDATE @ForImport
SET    ID_Program = 14
WHERE  Program_Import = 'SSS'

UPDATE @ForImport
SET    ID_Program = 15
WHERE  Program_Import = 'GSIS'

UPDATE @ForImport
SET    ID_Program = 3
WHERE  Program_Import = 'Senior Citizen'

/******************************************************************/
/*              Insert Invalid Values For Education               */
/*Update Educational Level as Day Care*/
Update @ForImport
SET    ID_EducationalLevel = 2
WHERE  Education_Import IN ( 'DAY CARE', 'KINDER', 'KENDER' )

/*Update Educational Level as Elementary*/
Update @ForImport
SET    ID_EducationalLevel = 3
WHERE  Education_Import IN ( 'elem', 'elem under grad', 'Elem.', 'ELEMENTARY', 'Elementary Grad' )

Update @ForImport
SET    ID_EducationalLevel = 3
WHERE  Education_Import LIKE 'GR%'
       AND Education_Import NOT LIKE 'GRaduate%'

Update @ForImport
SET    ID_EducationalLevel = 3
WHERE  Education_Import LIKE 'GE%'

/*Update Educational Level as High School*/
Update @ForImport
SET    ID_EducationalLevel = 7
WHERE  Education_Import IN ( 'H.s', 'H.s students', 'H.s undergrad', 'High School Grad',
                             'High School', 'hs', 'h' )

Update @ForImport
SET    ID_EducationalLevel = 7
WHERE  Education_Import LIKE '%High School%'

Update @ForImport
SET    ID_EducationalLevel = 7
WHERE  Education_Import LIKE '%H.S%'

/*Update Educational Level as K to 12*/
Update @ForImport
SET    ID_EducationalLevel = 4
WHERE  Education_Import IN ( 'Senior h', 'Senior High Student', 'SENIOR HIGH' )

/*Update Educational Level as College*/
Update @ForImport
SET    ID_EducationalLevel = 5
WHERE  Education_Import IN ( 'col,.', 'col.', 'col', 'col. grad',
                             'college', 'Col. grad.', 'col. under grad', 'col.student',
                             'COLLEGE', 'COLLEGE GRAD', 'College Grad.', 'COLLEGE LEVEL',
                             'COLLEGE UNDERGRAD', 'COLLEGE GRAND', 'Under Grad College', 'BACHLOR' )

Update @ForImport
SET    ID_EducationalLevel = 5
WHERE  Education_Import IN ( 'ist yr. col.', 'ist yr. col.o', '1st yr College', '1st yr College Student',
                             '2nd yr College', '2ND YR. COLL.', '3rd Year College Student' )

Update @ForImport
SET    ID_EducationalLevel = 5
WHERE  Education_Import IN ( 'I.T. grad.', 'col. Enginner', 'EDUCATION' )

/*Update Educational Level as Bokasyonal*/
Update @ForImport
SET    ID_EducationalLevel = 8
WHERE  Education_Import IN ( 'vol', 'voc.', 'Vocational', 'TESDA GRAD', 'Auto Mechanics Tesda' )

/*Update Educational Level as Bokasyonal*/
Update @ForImport
SET    ID_EducationalLevel = 9
WHERE  Education_Import IN ( 'ALS graduate', 'ALS', 'SECONDARY ALS' )

UPdate #ImportInvalidValues
SET    Education_Import = Col,
       Education_Import_Count = Count
FROM   #ImportInvalidValues _temp
       INNER JOIN (--
                  SELECT DISTINCT Education_Import                   Col,
                                  COUNT(*)                           Count,
                                  ROW_NUMBER()
                                    OVER(
                                      ORDER BY Education_Import ASC) AS RowIndex
                   FROM   @ForImport
                   WHERE  ID_EducationalLevel IS NULL
                   GROUP  BY Education_Import
                  --
                  ) tbl
               on _temp.RowIndex = tbl.RowIndex

/****************************************************/
/*                     Occupation_Import                                  */
Update @ForImport
SET    Occupation = Occupation_Import
WHERE  Occupation_Import <> 'none'
       AND Occupation_Import <> 'STUDENT'

UPdate #ImportInvalidValues
SET    Occupation_Import = Col,
       Occupation_Import_Count = Count
FROM   #ImportInvalidValues _temp
       INNER JOIN (----
                  SELECT Occupation_Import                   Col,
                         Count(*)                            Count,
                         ROW_NUMBER()
                           OVER(
                             ORDER BY Occupation_Import ASC) AS RowIndex
                   FROM   @ForImport
                   WHERE  Occupation_Import IN ( 'none', 'STUDENT' )
                   GROUP  BY Occupation_Import
                  ---
                  ) tbl
               on _temp.RowIndex = tbl.RowIndex

/****************************************************/
Update @ForImport
SET    ID_CivilStatus = 1
WHERE  CivilStatus_Import = 'SINGLE'

Update @ForImport
SET    ID_CivilStatus = 2
WHERE  CivilStatus_Import = 'Married'

Update @ForImport
SET    ID_CivilStatus = 4
WHERE  CivilStatus_Import = 'Separated'

Update @ForImport
SET    ID_CivilStatus = 5
WHERE  CivilStatus_Import = 'Widowed'

Update @ForImport
SET    DateBirth = TRY_CONVERT(datetime, Birthday_Import)
WHERE  ISDATE(Birthday_Import) = 1

Update @ForImport
SET    Birthday_Import = RIGHT(Birthday_Import, LEN(Birthday_Import) - 1)
where  Birthday_Import LIKE '/%'
        OR Birthday_Import LIKE '`%'
        OR Birthday_Import LIKE 'O%'

Update @ForImport
SET    Birthday_Import = LEFT(Birthday_Import, LEN(Birthday_Import) - 1)
where  Birthday_Import LIKE '%/'

Update @ForImport
SET    Birthday_Import = REPLACE(Birthday_Import, '''', '')

Update @ForImport
SET    Birthday_Import = REPLACE(Birthday_Import, '`', '')

Update @ForImport
SET    Birthday_Import = REPLACE(Birthday_Import, '.', '/')

Update @ForImport
SET    Birthday_Import = REPLACE(Birthday_Import, '//', '/')

Update @ForImport
SET    Birthday_Import = '02/26/1993'
WHERE  Birthday_Import = '02/261993'

Update @ForImport
SET    Birthday_Import = '02/20/1979'
WHERE  Birthday_Import = '02/20/979'

Update @ForImport
SET    Birthday_Import = '04/08/2015'
WHERE  Birthday_Import = '04-0815'

Update @ForImport
SET    Birthday_Import = '04/05/1978'
WHERE  Birthday_Import = '04/0578'

Update @ForImport
SET    Birthday_Import = '08/21/1970'
WHERE  Birthday_Import = '08/211970'

Update @ForImport
SET    Birthday_Import = '07/24/2013'
WHERE  Birthday_Import = '07/24/1013'

Update @ForImport
SET    Birthday_Import = '01/16/1967'
WHERE  Birthday_Import = '01/161967'

Update @ForImport
SET    Birthday_Import = '01/16/1967'
WHERE  Birthday_Import = '01/161967'


Update @ForImport
SET    Birthday_Import = '11/27/1965'
WHERE  Birthday_Import = '11/271965'

Update @ForImport
SET    Birthday_Import = '11/19/1974'
WHERE  Birthday_Import = '11/19/1074'

Update @ForImport
SET    Birthday_Import = '12/08/12'
WHERE  Birthday_Import = '12/0812'
Update @ForImport
SET    Birthday_Import = '09/24/1987'
WHERE  Birthday_Import = '09/241987'


Update @ForImport
SET    Birthday_Import = '05/13/1996'
WHERE  Birthday_Import = '05/133/1996'

/*              Insert Invalid Values For DateBirth                      */
UPdate #ImportInvalidValues
SET    Birthday_Import = Col,
       Birthday_Import_Count = Count
FROM   #ImportInvalidValues _temp
       INNER JOIN (----
                  SELECT Birthday_Import                   Col,
                         Count(*)                          Count,
                         ROW_NUMBER()
                           OVER(
                             ORDER BY Birthday_Import ASC) AS RowIndex
                   FROM   @ForImport
                   WHERE  ISDATE(Birthday_Import) = 0
                   GROUP  BY Birthday_Import
                  ---
                  ) tbl
               on _temp.RowIndex = tbl.RowIndex

/****************************************************/
Update @ForImport
SET    Address = 'Purok ' + Address_Import
WHERE  LEN(Address_Import) > 0

--SELECT DISTINCT Birthday_Import,
--                TRY_CONVERT(datetime, Birthday_Import)
--FROM   @ForImport
--WHERE  ISDATE(Birthday_Import) = 1
--       AND DateBirth IS NULL
Update @ForImport
SET    DateBirth = TRY_CONVERT(datetime, Birthday_Import)
WHERE  ISDATE(Birthday_Import) = 1

DELETE FROM @ForImport
WHERE  Name_Resident = ','

Update @ForImport
SET    ID_Gender = 1
WHERE  Gender_Import = 'MALE'

Update @ForImport
SET    ID_Gender = 2
WHERE  Gender_Import = 'FEMALE'

DELETE FROM @ForImport
WHERE  LEN(REPLACE(Name_Resident, ' ', '')) = 0

Update @ForImport
SET    Name_Resident = LTRIM(RTRIM(Name_Resident))

Update @ForImport
SET    Name_Resident = LTRIM(Name_Resident)

Update @ForImport
SET    Name_Resident = LTRIM(RIGHT(Name_Resident, LEN(Name_Resident) - 1))
FROM   @ForImport
WHERE  Name_Resident LIKE ',%'

Update @ForImport
SET    ID_Resident = resident.ID
FROM   @ForImport import
       INNER JOIN tResident resident
               on import.ImportGuid_Resident = resident.ImportGuid
WHERE  ID_Company = @ID_Company

INSERT INTO [dbo].[tResident]
            ([Name],
             ImportGuid,
             [IsActive],
             [ID_Company],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             comment)
SELECT Name_Resident,
       ImportGuid_Resident,
       1,
       @ID_Company,
       GETDATE(),
       GETDATE(),
       1,
       1,
       'Imported '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
FROM   @ForImport
WHERE  ID_Resident IS NULL
ORDER  BY RowIndex DESC

Update @ForImport
SET    ID_Resident = resident.ID
FROM   @ForImport import
       INNER JOIN tResident resident
               on import.ImportGuid_Resident = resident.ImportGuid
WHERE  ID_Company = @ID_Company

SELECT *
FROM   #ImportInvalidValues

SELECT *
FROM   @ForImport
ORDER  BY RowIndex DESC


SELECT *
FROM   @ForImport WHERE ID_Program IS NOT NULL


Update tResident
SET    ID_Gender = import.ID_Gender,
       DateBirth = import.DateBirth,
       ID_CivilStatus = import.ID_CivilStatus,
       Address = import.Address,
       Occupation = import.Occupation,
       ID_OccupationalStatus = CASE
                                 WHEN LEN(import.Occupation) > 0 THEN 2
                                 ELSE NULL
                               END,
       ID_EducationalLevel = import.ID_EducationalLevel
FROM   tResident resident
       INNER JOIN @ForImport import
               on resident.ID = import.ID_Resident

INSERT INTO [dbo].[tResident_Program]
            ([IsActive],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [ID_Program],
             [ID_Resident])
SELECT 1,
       GETDATE(),
       GETDATE(),
       1,
       1,
       tbl.ID_Program,
       tbl.ID_Resident
FROM   (SELECT DISTINCT ID_Resident,
                        ID_Program
        FROM   @ForImport
        WHERE  ID_Program IS NOT NULL
        EXCEPT
        SELECT ID_Resident,
               ID_Program
        FROM   tResident_Program rProgram
               INNER JOIN tResident resident
                       on rProgram.ID_Resident = resident.ID
        WHERE  resident.ID_Company = @ID_Company) tbl

IF OBJECT_ID('tempdb..#ImportInvalidValues') IS NOT NULL
  BEGIN
      DROP PROC _________pAddImportInvalidValuesColumns;

      DROP TABLE #ImportInvalidValues;
  END 
