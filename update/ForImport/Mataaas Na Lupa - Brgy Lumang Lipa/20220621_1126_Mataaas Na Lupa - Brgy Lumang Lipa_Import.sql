DEclare @GUID_Company VARCHAR(MAX) = '2BBA37C5-5531-4BBD-864D-6E3506A9217D'

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

DECLARE @ForImport TABLE
  (
     ID_Resident            INT,
     Name_Resident          VARCHAR(MAX),
     ImportGuid_Resident    VARCHAR(MAX),
     DateBirth              DATETIME,
     ID_Gender              INT,
     ContactNumber_Resident VARCHAR(MAX),
     ID_CivilStatus         INT,
     ID_EducationalLevel    INT
  )

INSERT @ForImport
       (Name_Resident,
        ImportGuid_Resident,
        DateBirth,
        ID_Gender,
        ContactNumber_Resident,
        ID_CivilStatus,
        ID_EducationalLevel)
SELECT dbo.fGetCleanedString(excel.Name),
       Guid + '-'
       + '[BARANGAY-LUMANGLIPA-MataasNaLupa-BATANGAS]',
       TRY_Convert(Datetime, dbo.fGetCleanedString(Birthday)),
       CASE
         WHEN Male = '/' THEN 1
         ELSE
           CASE
             WHEN Female = '/' THEN 2
             ELSE NULL
           END
       END,
       dbo.fGetCleanedString([Contact No.]),
       civilStaus.ID,
       educLevel.ID
FROM   ForImport.[dbo].[BARANGAY-LUMANGLIPA-MataasNaLupa-BATANGAS] excel
       left join tCivilStatus civilStaus
              on excel.[Marital Status] = civilStaus.Name
       left join tEducationalLevel educLevel
              on excel.[Educational Attainment] = educLevel.Name
WHERE  LEN(TRIM(excel.Name)) > 0


Update @ForImport 
SET ContactNumber_Resident = REPLACE(ContactNumber_Resident, '-', '')


Update @ForImport
SET    ID_Resident = resident.ID
FROM   @ForImport import
       INNER JOIN tResident resident
               on import.ImportGuid_Resident = resident.ImportGuid
WHERE  ID_Company = @ID_Company


SELECT *
FROM   @ForImport

INSERT INTO [dbo].[tResident]
            ([Name],
             ImportGuid,
             [IsActive],
             [ID_Company],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             comment)
SELECT Name_Resident,
       ImportGuid_Resident,
       1,
       @ID_Company,
       GETDATE(),
       GETDATE(),
       1,
       1,
       'Imported '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
FROM   @ForImport
WHERE  ID_Resident IS NULL
order  by Name_Resident

Update @ForImport
SET    ID_Resident = resident.ID
FROM   @ForImport import
       INNER JOIN tResident resident
               on import.ImportGuid_Resident = resident.ImportGuid
WHERE  ID_Company = @ID_Company