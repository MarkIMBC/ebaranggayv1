GO

IF COL_LENGTH('tResident', 'ID_Religion') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResident',
        'ID_Religion',
        2
  END

GO

IF COL_LENGTH('tResident', 'BirthPlace') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResident',
        'BirthPlace',
        1
  END

GO

IF COL_LENGTH('tResident', 'SourceOfIncome') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResident',
        'SourceOfIncome',
        1
  END

GO

IF COL_LENGTH('tResident', 'BirthPlace') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResident',
        'BirthPlace',
        1
  END

GO

IF COL_LENGTH('tResident', 'SourceOfIncomeAmount') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResident',
        'SourceOfIncomeAmount',
        1
  END

GO

IF COL_LENGTH('tResident', 'StayInDuration') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResident',
        'StayInDuration',
        1
  END

GO

IF COL_LENGTH('tResident', 'ID_HouseholdNumber') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResident',
        'ID_HouseholdNumber',
        2
  END

GO

IF COL_LENGTH('tResident', 'AssignedBHW_ID_Employee') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResident',
        'AssignedBHW_ID_Employee',
        2
  END

GO

IF COL_LENGTH('tHouseholdNumber', 'AssignedBHW_ID_Employee') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tHouseholdNumber',
        'AssignedBHW_ID_Employee',
        2
  END

GO

if OBJECT_ID('dbo.tReligion') is NULL
  BEGIN
      exec _pCreateAppModuleWithTable
        'tReligion',
        1,
        NULL,
        NULL
  END

GO

if OBJECT_ID('dbo.tHouseholdNumber') is NULL
  BEGIN
      exec _pCreateAppModuleWithTable
        'tHouseholdNumber',
        1,
        NULL,
        NULL
  END

GO

if OBJECT_ID('dbo.tResident_HouseholdNumberLog') is NULL
  BEGIN
      exec _pCreateAppModuleWithTable
        'tResident_HouseholdNumberLog',
        1,
        NULL,
        NULL
  END

GO

IF COL_LENGTH('tResident_HouseholdNumberLog', 'ID_Resident') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResident_HouseholdNumberLog',
        'ID_Resident',
        2
  END

GO

IF COL_LENGTH('tResident_HouseholdNumberLog', 'ID_HouseholdNumber') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResident_HouseholdNumberLog',
        'ID_HouseholdNumber',
        2
  END

GO

IF COL_LENGTH('tResident_HouseholdNumberLog', 'HouseholdNumberName') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResident_HouseholdNumberLog',
        'HouseholdNumberName',
        1
  END

GO

IF COL_LENGTH('tResident', 'VoteInfoPrecinctNumber') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResident',
        'VoteInfoPrecinctNumber',
        1
  END

GO

IF COL_LENGTH('tResident', 'AssignedBNS_ID_Employee') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tResident',
        'AssignedBNS_ID_Employee',
        2
  END

GO

IF COL_LENGTH('tLawsuit', 'Complainant_ID_Resident') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tLawsuit',
        'Complainant_ID_Resident',
        2
  END

GO

IF COL_LENGTH('tLawsuit', 'Respondent_ID_Resident') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tLawsuit',
        'Respondent_ID_Resident',
        2
  END

GO

IF COL_LENGTH('tLawsuit', 'ID_LawSuitStatus') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tLawsuit',
        'ID_LawSuitStatus',
        2
  END

GO

IF COL_LENGTH('tLawsuit', 'ID_FilingStatus') IS NULL
  BEGIN
      exec _pAddModelProperty
        'tLawsuit',
        'ID_FilingStatus',
        2
  END

GO

if OBJECT_ID('dbo.tLawsuitStatus') is NULL
  BEGIN
      exec _pCreateAppModuleWithTable
        'tLawsuitStatus',
        1,
        NULL,
        NULL
  END

GO

exec _pRefreshAllViews

GO

CREATE OR
ALTER VIEW vResident
AS
  SELECT H.*,
         UC.Name                  AS CreatedBy,
         UM.Name                  AS LastModifiedBy,
         gender.Name              Name_Gender,
         company.Name             Name_Company,
         civilStatus.Name         Name_CivilStatus,
         educationalLevel.Name    Name_EducationalLevel,
         homeOwnershipStatus.Name Name_HomeOwnershipStatus,
         occupationalStatus.Name  Name_OccupationalStatus,
         AssignedBHW.Name         AssignedBHW_Name_Employee,
         religion.Name            Name_Religion,
         AssignedBNS.Name         AssignedBNS_Name_Employee,
         AssignedBNS.Name         AssignedBBNS_Name_Employee
  FROM   tResident H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tGender gender
                ON gender.ID = h.ID_Gender
         LEFT JOIN tCivilStatus civilStatus
                ON civilStatus.ID = h.ID_CivilStatus
         LEFT JOIN tEducationalLevel educationalLevel
                ON educationalLevel.ID = h.ID_EducationalLevel
         LEFT JOIN tHomeOwnershipStatus homeOwnershipStatus
                ON homeOwnershipStatus.ID = h.ID_HomeOwnershipStatus
         LEFT JOIN tOccupationalStatus occupationalStatus
                ON occupationalStatus.ID = h.ID_OccupationalStatus
         LEFT JOIN tReligion religion
                ON religion.ID = h.ID_Religion
         LEFT JOIN tCompany company
                ON company.ID = h.ID_Company
         LEFT JOIN tEmployee AssignedBHW
                ON AssignedBHW.ID = h.AssignedBHW_ID_Employee
         LEFT JOIN tEmployee AssignedBNS
                ON AssignedBNS.ID = h.AssignedBNS_ID_Employee

GO

ALTER VIEW vHouseholdNumber
AS
  SELECT H.*,
         UC.Name          AS CreatedBy,
         UM.Name          AS LastModifiedBy,
         AssignedBHW.Name AssignedBHW_Name_Employee
  FROM   tHouseholdNumber H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tEmployee AssignedBHW
                ON AssignedBHW.ID = h.AssignedBHW_ID_Employee

GO

CREATE    OR
ALTER VIEW vResident_ListvIew
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         hed.IsDeceased,
         company.Guid Guid_Company,
         ID_HouseholdNumber,
         IsHeadofFamily,
         hed.Name_OccupationalStatus,
         hed.Name_Religion,
         hed.AssignedBHW_ID_Employee,
         hed.AssignedBHW_Name_Employee
  FROM   vResident hed
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  hed.IsActive = 1
         AND ISNULL(IsDeceased, 0) = 0

GO

CREATE    OR
ALTER VIEW vBarangayHealthWorker_Resident_ListvIew
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         hed.IsDeceased,
         company.Guid Guid_Company,
         ID_HouseholdNumber,
         IsHeadofFamily,
         hed.Name_OccupationalStatus,
         hed.Name_Religion,
         hed.AssignedBHW_ID_Employee,
         hed.AssignedBHW_Name_Employee,
         company.Name Name_Company,
         company.MainRouteLink
  FROM   vResident hed
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  hed.IsActive = 1
         AND company.IsActive = 1
         AND ISNULL(IsDeceased, 0) = 0
         AND company.MainRouteLink IN ( 'Barangay' )

GO

CREATE OR
ALTER VIEW vHouseholdNumberrResidentList_Listview
as
  Select household.ID,
         household.Name,
         resident.ID_Company,
         COUNT(*) Count
  FROM   tHouseholdNumber household
         inner join vResident_Listview_HeadOfFamily resident
                 on household.ID = resident.ID_HouseholdNumber
  WHERE  resident.IsActive = 1
  GROUP  BY household.ID,
            household.Name,
            resident.ID_Company

GO

CREATE OR
ALTER VIEW vHouseholdNumberResidentCount
as
  Select household.ID,
         household.Name,
         resident.ID_Company,
         COUNT(*) Count
  FROM   tHouseholdNumber household
         inner join tResident resident
                 on household.ID = resident.ID_HouseholdNumber
  WHERE  resident.IsActive = 1
  GROUP  BY household.ID,
            household.Name,
            resident.ID_Company

GO

CREATE OR
ALTER VIEW vHouseholdNumberResidentHeadOfFamilyCount
as
  Select household.ID,
         household.Name,
         resident.ID_Company,
         COUNT(*) Count
  FROM   tHouseholdNumber household
         inner join tResident resident
                 on household.ID = resident.ID_HouseholdNumber
  WHERE  resident.IsActive = 1
         And IsHeadofFamily = 1
  GROUP  BY household.ID,
            household.Name,
            resident.ID_Company

GO

CREATE OR
ALTER VIEW vHouseholdNumberResidentCount_Listview
as
  Select household.ID,
         household.Name,
         summary.ID_Company,
         summary.Count
  FROM   tHouseholdNumber household
         lEFT join vHouseholdNumberResidentCount summary
                on household.ID = summary.ID
                   and household.ID_Company = summary.ID_Company

GO

CREATE   OR
ALTER VIEW vHouseholdNumber_Listview
as
  Select household.ID,
         household.Name,
         household.ID_Company,
         ISNULL(summary.Count, 0) Count,
         DateCreated,
         AssignedBHW_Name_Employee
  FROM   vHouseholdNumber household
         lEFT join vHouseholdNumberResidentHeadOfFamilyCount summary
                on household.ID = summary.ID
  WHERE  household.IsActive = 1

GO

CREATE    OR
ALTER VIEW vResident_Listview_HeadOfFamily
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid Guid_Company,
         ID_HouseholdNumber
  FROM   vResident hed
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  hed.IsActive = 1
         AND ISNULL(hed.IsHeadofFamily, 0) = 1
         ANd ISNULL(IsDeceased, 0) = 0

GO

CREATE OR
ALTER VIEW vEmployeeBarangayHealthWorker
AS
  SELECT *
  FROM   vEmployee
  WHERE  Name_Position IN ( 'Barangay Health Worker' )
         and ISNULL(IsActive, 0) = 1

GO

CREATE OR
ALTER VIEW vEmployeeBNS
AS
  SELECT *
  FROM   vEmployee
  WHERE  Name_Position IN ( 'BNS' )
         and ISNULL(IsActive, 0) = 1

GO

GO

CREATE OR
ALTER VIEW vLawSuit
AS
  SELECT H.*,
         UC.Name                                              AS CreatedBy,
         UM.Name                                              AS LastModifiedBy,
         ISNULL(residentComplaintnat.Name, H.ComplainantName) Complainant_Name_Resident,
         ISNULL(residentRespondent.Name, H.RespondentName)    Respondent_Name_Resident,
         fs.Name                                              Name_FilingStatus,
         lawsuitStatus.Name                                   Name_LawSuitStatus
  FROM   tLawSuit H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tResident residentComplaintnat
                ON H.Complainant_ID_Resident = residentComplaintnat.ID
         LEFT JOIN tResident residentRespondent
                ON H.Respondent_ID_Resident = residentRespondent.ID
         LEFT JOIN tFilingStatus fs
                ON H.ID_FilingStatus = fs.ID
         LEFT JOIN tLawSuitStatus lawsuitStatus
                ON H.ID_LawSuitStatus = lawsuitStatus.ID

GO

CREATE OR
ALTER VIEW vLawSuit_ListView
as
  SELECT h.ID,
         Date,
         Complainant_ID_Resident,
         Complainant_Name_Resident,
         Respondent_ID_Resident,
         Respondent_Name_Resident,
         Name_FilingStatus,
         Name_LawSuitStatus,
         h.ID_Company,
         h.Comment,
         ID_LawSuitStatus,
         ComplainantName,
         RespondentName
  FROM   vLawSuit h
  where  ISNULL(h.IsActive, 0) = 1

GO

ALTER VIEW dbo.vzResidentDetailReport
AS
  SELECT hed.Name,
         hed.IsActive,
         hed.ID_Company,
         hed.ID_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.Name_Gender,
         hed.Name_Company,
         hed.IsTransient,
         hed.IsPermanent,
         hed.IsMigrante,
         company.ImageLogoLocationFilenamePath,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                    HeaderInfo_Company,
         [dbo].[fGetOrdinalNumber] (GETDATE())    AS OrdinalNum,
         ISNULL(dbo.fGetIntAge(hed.DateBirth), 0) AS Age,
         hed.AssignedBHW_ID_Employee,
         hed.AssignedBHW_Name_Employee
  FROM   vResident hed
         LEFT JOIN dbo.vCompany company
                ON company.ID = hed.ID_Company
  WHERE  hed.IsActive = 1

GO

GO

CREATE  OR
ALTER VIEW vzResidentVotersListReport
AS
  SELECT hed.ID,
         hed.Name,
         hed.Name_Gender,
         hed.ID_Gender,
         hed.ContactNumber,
         hed.VoteInfoPrecinctNumber,
         dbo.fGetIntAge(hed.DateBirth)                           Age,
         company.ID                                              ID_Company,
         company.ImageLogoLocationFilenamePath                   ImageLogoLocationFilenamePath,
         company.Name                                            Name_Company,
         company.Address                                         Address_Company,
         company.ContactNumber                                   ContactNumber_Company,
         company.BarangayListSideReportHTMLString                BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                             BarangayCaptainName_Company,
         company.BarangayCaptain_Name_Employee                   BarangayCaptain_Name_Employee,
         company.BarangayCaptain_LastName_Employee               BarangayCaptain_LastName_Employee,
         company.BarangayCaptain_FirstName_Employee              BarangayCaptain_FirstName_Employee,
         company.BarangayCaptain_MiddleName_Employee             BarangayCaptain_MiddleName_Employee,
         company.BarangayCaptain_ImageSignitureSpecimen_Employee BarangayCaptain_ImageSignitureSpecimen_Employee,
         municipality.Name                                       Name_MunicipalityCompany,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' <br/>Email: ' + company.Email
             ELSE ''
           END                                                   HeaderInfo_Company,
         municipality.ImageLogoLocationFilenamePath              ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                                    Address_MunicipalityCompany
  FROM   vResident hed
         INNER JOIN vCompanyActive company
                 ON company.ID = hed.ID_Company
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = hed.ID_Company
  WHERE  hed.IsActive = 1
         and hed.IsVoter = 1

GO

GO

CREATE    OR
ALTER VIEW [dbo].vzBarangayComplaintReport
AS
  SELECT lawSuit.ID,
         lawSuit.Complainant_Name_Resident,
         lawSuit.Respondent_Name_Resident,
         lawSuit.Date,
         lawSuit.RespondentSMSNumber,
         lawSuit.ComplainantSMSNumber,
         lawSuit.Comment,
         company.ID            ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.Name          Name_Company,
         company.Address       Address_Company,
         company.ContactNumber ContactNumber_Company
  FROM   vLawSuit lawSuit
         INNER JOIN vCompany company
                 ON company.ID = lawSuit.ID_Company

GO

CREATE OR
ALTER PROCEDURE pGetLawsuit (@ID                      INT = -1,
                             @Complainant_ID_Resident INT = NULL,
                             @Respondent_ID_Resident  INT = NULL,
                             @ID_Session              INT = NULL)
AS
  BEGIN
      SELECT '_'

      DECLARE @ID_User      INT,
              @ID_Warehouse INT
      DECLARE @ID_FilingStatus INT = 1

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   residentComplaintnat.Name Complainant_Name_Resident,
                   residentRespondent.Name   Respondent_Name_Resident,
                   fs.Name                   Name_FilingStatus,
                   lawsuitStatus.Name        Name_LawSuitStatus
            FROM   (SELECT NULL                     AS [_],
                           -1                       AS [ID],
                           NULL                     AS [Code],
                           NULL                     AS [Name],
                           GetDate()                AS [Date],
                           1                        AS [IsActive],
                           NULL                     AS [ID_Company],
                           NULL                     AS [Comment],
                           NULL                     AS [DateCreated],
                           NULL                     AS [DateModified],
                           NULL                     AS [ID_CreatedBy],
                           NULL                     AS [ID_LastModifiedBy],
                           ''                       AS ComplainantName,
                           ''                       AS RespondentName,
                           @Complainant_ID_Resident AS [Complainant_ID_Resident],
                           @Respondent_ID_Resident  AS [Respondent_ID_Resident],
                           @ID_FilingStatus         AS [ID_FilingStatus],
                           1                        ID_LawSuitStatus) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tResident residentComplaintnat
                          ON H.Complainant_ID_Resident = residentComplaintnat.ID
                   LEFT JOIN tResident residentRespondent
                          ON H.Respondent_ID_Resident = residentRespondent.ID
                   LEFT JOIN tFilingStatus fs
                          ON H.ID_FilingStatus = fs.ID
                   LEFT JOIN tLawSuitStatus lawsuitStatus
                          ON H.ID_LawSuitStatus = lawsuitStatus.ID
        END
      ELSE
        BEGIN
            SELECT H.*,
                   H.ComplainantName _ComplainantName,
                   H.RespondentName  _RespondentName
            FROM   vLawSuit H
            WHERE  H.ID = @ID
        END
  END

GO

Create OR
ALTER Proc pChangeResidentHouseholdNumber(@ID_HouseholdNumber INT,
                                          @IDs_Resident       typIntList READONLY,
                                          @ID_UserSession     INT)
as
    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';
    DECLARE @ID_User INT = 0;

    SELECT @ID_User = ID_User
    FROM   dbo.tUserSession
    WHERE  ID = @ID_UserSession;

    Update tResident
    SET    ID_HouseholdNumber = @ID_HouseholdNumber
    WHERE  ID IN (SELECT ID
                  FROM   @IDs_Resident)

    INSERT INTO [dbo].[tResident_HouseholdNumberLog]
                ([IsActive],
                 [ID_Company],
                 [DateCreated],
                 [DateModified],
                 [ID_CreatedBy],
                 [ID_LastModifiedBy],
                 [ID_Resident],
                 [ID_HouseholdNumber],
                 [HouseholdNumberName])
    Select 1,
           resident.ID_Company,
           GETDATE(),
           GETDATE(),
           1,
           1,
           resident.ID,
           ID_HouseholdNumber,
           household.Name
    FROM   tResident resident
           inner join @IDs_Resident idsResident
                   on resident.ID = idsResident.ID
           LEFT join tHouseholdNumber household
                  on resident.ID_HouseholdNumber = household.ID

    SELECT '_';

    SELECT @Success Success,
           @message message;

GO

CREATE   OR
ALTER PROC pInsertPosition(@Position   VARCHAR(MAX),
                           @ID_Company INT = 0)
as
  BEGIN
      IF(SELECT COUNT(*)
         FROM   tPosition
         WHERE  Name = @Position) = 0
        BEGIN
            INSERT INTO [dbo].tPosition
                        ([Name],
                         [IsActive],
                         ID_Company)
            VALUES      (@Position,
                         1,
                         @ID_Company)
        END
  END

GO

GO

CREATE   OR
ALTER PROC pInsertLawSuitStatus(@LawSuitStatus VARCHAR(MAX),
                                @ID_Company    INT = 0)
as
  BEGIN
      IF(SELECT COUNT(*)
         FROM   tLawSuitStatus
         WHERE  Name = @LawSuitStatus) = 0
        BEGIN
            INSERT INTO [dbo].tLawSuitStatus
                        ([Name],
                         [IsActive],
                         ID_Company)
            VALUES      (@LawSuitStatus,
                         1,
                         @ID_Company)
        END
  END

GO

ALTER PROCEDURE pGetHouseholdNumber @ID         INT = -1,
                                    @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_'

      DECLARE @ID_User      INT,
              @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL AS [_],
                           -1   AS [ID],
                           NULL AS [Code],
                           NULL AS [Name],
                           1    AS [IsActive],
                           NULL AS [ID_Company],
                           NULL AS [Comment],
                           NULL AS [DateCreated],
                           NULL AS [DateModified],
                           NULL AS [ID_CreatedBy],
                           NULL AS [ID_LastModifiedBy]) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vHouseholdNumber H
            WHERE  H.ID = @ID
        END
  END

GO

CREATE   OR
ALTER PROC dbo.pGetDashboardResidentOccupationStatusPieChart (@ID_UserSession INT,
                                                              @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      DECLARE @dataSource TABLE
        (
           Name  VARCHAR(MAX),
           Count INT
        )

      INSERT @dataSource
      SELECT ISNULL(Name_OccupationalStatus, 'Walang nakatalaga'),
             Count(*)
      FROM   vResident resident
      Where  resident.IsActive = 1
             and ISNULL(resident.IsDeceased, 0) = 0
             AND resident.ID_Company = @ID_Company
      GROUP  BY Name_OccupationalStatus

      IF (SELECT COUNT(*)
          FROM   @dataSource) = 0
        BEGIN
            INSERT @dataSource
            SELECT 'No Record',
                   0
        END

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear DateYear

      SELECT *
      FROM   @dataSource
  END

GO

Create OR
ALTER PROC dbo.pGetDashboardResidentReligionDataSource (@ID_UserSession INT,
                                                        @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear                DateYear,
             'Relihiyon ' + @DateYear Label

      SELECT Name_Religion,
             Count(*) Count
      FROM   vResident
      WHERE  IsActive = 1
             AND YEAR(DateCreated) = @DateYear
             and ISNULL(IsDeceased, 0) = 0
             AND ID_Company = @ID_Company
             and ISNULL(ID_Religion, 0) > 0
      GROUP  BY Name_Religion
  END

GO

go

CREATE OR
ALTER PROC pInsertReligion (@Religion   VARCHAR(MAX),
                            @ID_Company INT = 1)
as
  BEGIN
      IF(SELECT COUNT(*)
         FROM   tReligion
         WHERE  Name = @Religion) = 0
        BEGIN
            INSERT INTO [dbo].tReligion
                        ([Name],
                         [IsActive],
                         ID_Company)
            VALUES      (@Religion,
                         1,
                         @ID_Company)
        END
  END

GO

CREATE     OR
ALTER PROC [dbo].[pChangeUserPassword](@ID             INT,
                                       @NewPassword    VARCHAR(MAX),
                                       @ID_UserSession INT)
AS
  BEGIN
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          Update tUser
          SET    Password = @NewPassword
          WHERE  ID = @ID
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END

GO

GO

CREATE        OR
ALTER PROC pGetDashboardBarangayHealthWorkerSummary @ID_UserSession INT = NULL
as
  BEGIN
      DECLARE @ID_User    INT,
              @ID_Company INT
      DECLARE @ResidentCount INT = 0
      DECLARE @PermanentResidentCount INT = 0
      DECLARE @MigrateResidentCount INT = 0
      DECLARE @TrancientResidentCount INT = 0
      DECLARE @HomeOwnershipStatusOwnerCount INT = 0
      DECLARE @EducationalLevelGraduatedCount INT = 0
      DECLARE @TotalResidentMaleCount INT = 0
      DECLARE @TotalResidentFemaleCount INT = 0
      DECLARE @ResidentCountAge60Above INT = 0
      DECLARE @ResidentCountAge18to59 INT = 0
      DECLARE @ResidentCountAge10to17 INT = 0
      DECLARE @ResidentCountAge5to9 INT = 0
      DECLARE @ResidentCountAge24to84months INT = 0
      DECLARE @ResidentCountAge12to23months INT = 0
      DECLARE @ResidentCountAge0to11months INT = 0
      DECLARE @ResidentCountMale INT = 0
      DECLARE @ResidentCountFemale INT = 0
      DECLARE @ResidentCount4Ps INT = 0
      DECLARE @ResidentCountSeniorCitizen INT = 0
      DECLARE @ResidentCountScholar INT = 0
      DECLARE @VoterCount INT = 0
      DECLARE @ResidentNoneWorkerCount INT = 0
      DECLARE @MayAriNgBahayAtLupaID_HomeOwnershipStatus INT = 1
      DECLARE @GraduateSchool_ID_EducationalLevel INT = 6
      DECLARE @Wala_ID_OccupationalStatus INT = 1
      DECLARE @Male_ID_Gender INT = 1
      DECLARE @Female_ID_Gender INT = 2

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      SELECT @ResidentCount = COUNT(*)
      FROM   tResident resident
             INNER JOIN vCompanyActive c
                     on resident.ID_Company = c.ID
      where  c.ID NOT IN ( 1 )
             and resident.IsActive = 1
             and ISNULL(resident.IsDeceased, 0) = 0

      SELECT @PermanentResidentCount = COUNT(*)
      FROM   tResident resident
             INNER JOIN vCompanyActive c
                     on resident.ID_Company = c.ID
      where  c.ID NOT IN ( 1 )
             and resident.IsActive = 1
             and ISNULL(resident.IsDeceased, 0) = 0
             AND IsPermanent = 1

      SELECT @VoterCount = COUNT(*)
      FROM   tResident resident
             INNER JOIN vCompanyActive c
                     on resident.ID_Company = c.ID
      where  c.ID NOT IN ( 1 )
             and resident.IsActive = 1
             and ISNULL(resident.IsDeceased, 0) = 0
             AND IsVoter = 1

      SELECT @MigrateResidentCount = COUNT(*)
      FROM   tResident resident
             INNER JOIN vCompanyActive c
                     on resident.ID_Company = c.ID
      where  c.ID NOT IN ( 1 )
             and resident.IsActive = 1
             and ISNULL(resident.IsDeceased, 0) = 0
             AND IsMigrante = 1

      SELECT @TrancientResidentCount = COUNT(*)
      FROM   tResident resident
             INNER JOIN vCompanyActive c
                     on resident.ID_Company = c.ID
      where  c.ID NOT IN ( 1 )
             and resident.IsActive = 1
             and ISNULL(resident.IsDeceased, 0) = 0
             AND IsTransient = 1

      SELECT @HomeOwnershipStatusOwnerCount = COUNT(*)
      FROM   tResident resident
             INNER JOIN vCompanyActive c
                     on resident.ID_Company = c.ID
      where  c.ID NOT IN ( 1 )
             and resident.IsActive = 1
             and ISNULL(resident.IsDeceased, 0) = 0
             AND IsTransient = 1
             ANd ID_HomeOwnershipStatus = @MayAriNgBahayAtLupaID_HomeOwnershipStatus

      SELECT @EducationalLevelGraduatedCount = COUNT(*)
      FROM   tResident resident
             INNER JOIN vCompanyActive c
                     on resident.ID_Company = c.ID
      where  c.ID NOT IN ( 1 )
             and resident.IsActive = 1
             and ISNULL(resident.IsDeceased, 0) = 0
             ANd ID_EducationalLevel = @GraduateSchool_ID_EducationalLevel

      SELECT @ResidentNoneWorkerCount = COUNT(*)
      FROM   tResident resident
             INNER JOIN vCompanyActive c
                     on resident.ID_Company = c.ID
      where  c.ID NOT IN ( 1 )
             and resident.IsActive = 1
             and ISNULL(resident.IsDeceased, 0) = 0
             ANd ID_OccupationalStatus = @Wala_ID_OccupationalStatus

      SELECT @ResidentCountMale = COUNT(*)
      FROM   tResident resident
             INNER JOIN vCompanyActive c
                     on resident.ID_Company = c.ID
      where  c.ID NOT IN ( 1 )
             and resident.IsActive = 1
             and ISNULL(resident.IsDeceased, 0) = 0
             ANd ID_Gender = @Male_ID_Gender

      SELECT @ResidentCountFemale = COUNT(*)
      FROM   tResident resident
             INNER JOIN vCompanyActive c
                     on resident.ID_Company = c.ID
      where  c.ID NOT IN ( 1 )
             and resident.IsActive = 1
             and ISNULL(resident.IsDeceased, 0) = 0
             ANd ID_Gender = @Female_ID_Gender

      SELECT @ResidentCountAge60Above = COUNT(*)
      FROM   tResident resident
             INNER JOIN vCompanyActive c
                     on resident.ID_Company = c.ID
      where  c.ID NOT IN ( 1 )
             and resident.IsActive = 1
             and ISNULL(resident.IsDeceased, 0) = 0
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) >= 60

      SELECT @ResidentCountAge18to59 = COUNT(*)
      FROM   tResident resident
             INNER JOIN vCompanyActive c
                     on resident.ID_Company = c.ID
      where  c.ID NOT IN ( 1 )
             and resident.IsActive = 1
             and ISNULL(resident.IsDeceased, 0) = 0
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) BETWEEN 18 AND 59

      SELECT @ResidentCountAge10to17 = COUNT(*)
      FROM   tResident resident
             INNER JOIN vCompanyActive c
                     on resident.ID_Company = c.ID
      where  c.ID NOT IN ( 1 )
             and resident.IsActive = 1
             and ISNULL(resident.IsDeceased, 0) = 0
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) BETWEEN 10 AND 17

      SELECT @ResidentCountAge5to9 = COUNT(*)
      FROM   tResident resident
             INNER JOIN vCompanyActive c
                     on resident.ID_Company = c.ID
      where  c.ID NOT IN ( 1 )
             and resident.IsActive = 1
             and ISNULL(resident.IsDeceased, 0) = 0
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) BETWEEN 5 AND 9

      SELECT @ResidentCountAge24to84months = COUNT(*)
      FROM   tResident resident
             INNER JOIN vCompanyActive c
                     on resident.ID_Company = c.ID
      where  c.ID NOT IN ( 1 )
             and resident.IsActive = 1
             and ISNULL(resident.IsDeceased, 0) = 0
             and DateBirth IS NOT NULL
             AND DATEDIFF(MONTH, DateBirth, GETDATE()) BETWEEN 24 AND 84

      SELECT @ResidentCountAge12to23months = COUNT(*)
      FROM   tResident resident
             INNER JOIN vCompanyActive c
                     on resident.ID_Company = c.ID
      where  c.ID NOT IN ( 1 )
             and resident.IsActive = 1
             and ISNULL(resident.IsDeceased, 0) = 0
             and DateBirth IS NOT NULL
             AND DATEDIFF(MONTH, DateBirth, GETDATE()) BETWEEN 12 AND 23

      SELECT @ResidentCountAge0to11months = COUNT(*)
      FROM   tResident resident
             INNER JOIN vCompanyActive c
                     on resident.ID_Company = c.ID
      where  c.ID NOT IN ( 1 )
             and resident.IsActive = 1
             and ISNULL(resident.IsDeceased, 0) = 0
             and DateBirth IS NOT NULL
             AND DATEDIFF(MONTH, DateBirth, GETDATE()) BETWEEN 0 AND 11

      SELECT @ResidentCount4Ps = COUNT(*)
      FROM   (SELECT DISTINCT resident.ID
              FROM   tResident resident
                     INNER JOIN vCompanyActive c
                             on resident.ID_Company = c.ID
                     inner join tResident_Program residentprogram
                             on resident.ID = residentprogram.ID_Resident
                     Inner join tProgram program
                             on residentprogram.ID_Program = program.ID
              where  c.ID NOT IN ( 1 )
                     and resident.IsActive = 1
                     and ISNULL(resident.IsDeceased, 0) = 0
                     AND program.Name IN ( '4Ps' )
              GROUP  BY resident.ID) tbl

      SELECT @ResidentCountSeniorCitizen = COUNT(*)
      FROM   tResident resident
             INNER JOIN vCompanyActive c
                     on resident.ID_Company = c.ID
      where  c.ID NOT IN ( 1 )
             and resident.IsActive = 1
             and ISNULL(resident.IsDeceased, 0) = 0
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) >= 60

      SELECT @ResidentCountScholar = COUNT(*)
      FROM   (SELECT DISTINCT resident.ID
              FROM   tResident resident
                     INNER JOIN vCompanyActive c
                             on resident.ID_Company = c.ID
                     inner join tResident_Program residentprogram
                             on resident.ID = residentprogram.ID_Resident
                     Inner join tProgram program
                             on residentprogram.ID_Program = program.ID
              where  c.ID NOT IN ( 1 )
                     and resident.IsActive = 1
                     and ISNULL(resident.IsDeceased, 0) = 0
                     AND program.Name IN ( 'Scholarship' )
              GROUP  BY resident.ID) tbl

      SELECT '_'

      SELECT ISNULL(@ResidentCount, 0)                  ResidentCount,
             ISNULL(@VoterCount, 0)                     VoterCount,
             ISNULL(@ResidentNoneWorkerCount, 0)        ResidentNoneWorkerCount,
             ISNULL(@PermanentResidentCount, 0)         PermanentResidentCount,
             ISNULL(@MigrateResidentCount, 0)           MigrateResidentCount,
             ISNULL(@TrancientResidentCount, 0)         TrancientResidentCount,
             ISNULL(@HomeOwnershipStatusOwnerCount, 0)  HomeOwnershipStatusOwnerCount,
             ISNULL(@EducationalLevelGraduatedCount, 0) EducationalLevelGraduatedCount,
             ISNULL(@ResidentCountAge60Above, 0)        ResidentCountAge60Above,
             ISNULL(@ResidentCountAge18to59, 0)         ResidentCountAge18to59,
             ISNULL(@ResidentCountAge10to17, 0)         ResidentCountAge10to17,
             ISNULL(@ResidentCountAge5to9, 0)           ResidentCountAge5to9,
             ISNULL(@ResidentCountAge24to84months, 0)   ResidentCountAge24to84months,
             ISNULL(@ResidentCountAge12to23months, 0)   ResidentCountAge12to23months,
             ISNULL(@ResidentCountAge0to11months, 0)    ResidentCountAge0to11months,
             ISNULL(@ResidentCountMale, 0)              ResidentCountMale,
             ISNULL(@ResidentCountFemale, 0)            ResidentCountFemale,
             ISNULL(@ResidentCount4Ps, 0)               ResidentCount4Ps,
             ISNULL(@ResidentCountSeniorCitizen, 0)     ResidentCountSeniorCitizen,
             ISNULL(@ResidentCountScholar, 0)           ResidentCountScholar
  END

GO

GO

CREATE OR
ALTER PROC dbo.pGetDashboardBarangayHealthWorkerMonthlyClientsDataSource (@ID_UserSession INT,
                                                                          @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      DECLARE @ClientDailyCount INT = 0
      DECLARE @ClientMonthlyCount INT = 0
      DECLARE @SalesDailyAmount DECIMAL(18, 2) = 0
      DECLARE @SalesMonthlyAmount DECIMAL(18, 2) = 0

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear                          DateYear,
             'Mga Bagong Resident ' + @DateYear Label

      SELECT SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 1 THEN 1
                   ELSE 0
                 END) AS 'January',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 2 THEN 1
                   ELSE 0
                 END) AS 'February',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 3 THEN 1
                   ELSE 0
                 END) AS 'March',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 4 THEN 1
                   ELSE 0
                 END) AS 'April',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 5 THEN 1
                   ELSE 0
                 END) AS 'May',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 6 THEN 1
                   ELSE 0
                 END) AS 'June',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 7 THEN 1
                   ELSE 0
                 END) AS 'July',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 8 THEN 1
                   ELSE 0
                 END) AS 'August',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 9 THEN 1
                   ELSE 0
                 END) AS 'September',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 10 THEN 1
                   ELSE 0
                 END) AS 'October',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 11 THEN 1
                   ELSE 0
                 END) AS 'November',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 12 THEN 1
                   ELSE 0
                 END) AS 'December',
             SUM(CASE datepart(year, resident. DateCreated)
                   WHEN 2012 THEN 1
                   ELSE 0
                 END) AS 'TOTAL',
             ID_Company
      FROM   tResident resident
             inner join vCompanyActive c
                     on resident.ID_Company = c.ID
      WHERE  resident.IsActive = 1
             AND YEAR(resident.DateCreated) = @DateYear
             AND c.ID NOT IN ( 1 )
      GROUP  BY ID_Company
  END

GO

GO

Create   OR
ALTER PROC dbo.pGetDashboardBarangayHealthWorkerRegisteredResidentDataSource (@ID_UserSession INT,
                                                                              @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_UserSession;

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      DECLARE @ClientDailyCount INT = 0
      DECLARE @ClientMonthlyCount INT = 0
      DECLARE @SalesDailyAmount DECIMAL(18, 2) = 0
      DECLARE @SalesMonthlyAmount DECIMAL(18, 2) = 0
      DECLARE @dataSource TABLE
        (
           Name  VARCHAR(MAX),
           Count INT
        )

      INSERT @dataSource
      SELECT TOP 10 resident.Name_Company,
                    Count(resident.ID_Company) as Count
      FROM   vResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      WHERE  company.ID NOT IN ( 1 )
             AND YEAR(resident.DateCreated) = @DateYear
      GROUP  BY resident.ID_Company,
                resident.Name_Company
      ORDER  BY Count DESC

      IF (SELECT COUNT(*)
          FROM   @dataSource) = 0
        BEGIN
            INSERT @dataSource
            SELECT 'No Record',
                   0
        END

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear DateYear

      SELECT *
      FROM   @dataSource
  END

GO

CREATE     OR
ALTER PROC dbo.pGetDashboardBarangayHealthWorkerResidentOccupationStatusPieChart (@ID_UserSession INT,
                                                                                  @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      DECLARE @dataSource TABLE
        (
           Name  VARCHAR(MAX),
           Count INT
        )

      INSERT @dataSource
      SELECT ISNULL(Name_OccupationalStatus, 'Walang nakatalaga'),
             Count(*)
      FROM   vResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      WHERE  company.ID NOT IN ( 1 )
             AND resident.IsActive = 1
             and ISNULL(resident.IsDeceased, 0) = 0
      GROUP  BY Name_OccupationalStatus

      IF (SELECT COUNT(*)
          FROM   @dataSource) = 0
        BEGIN
            INSERT @dataSource
            SELECT 'No Record',
                   0
        END

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear DateYear

      SELECT *
      FROM   @dataSource
  END

GO

GO

CREATE   OR
ALTER PROC dbo.pGetDashboardBarangayHealthWorkerResidentProgramDataSource (@ID_UserSession INT,
                                                                           @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      DECLARE @ClientDailyCount INT = 0
      DECLARE @ClientMonthlyCount INT = 0
      DECLARE @SalesDailyAmount DECIMAL(18, 2) = 0
      DECLARE @SalesMonthlyAmount DECIMAL(18, 2) = 0
      DECLARE @dataSource TABLE
        (
           Name  VARCHAR(MAX),
           Count INT
        )

      INSERT @dataSource
      SELECT Name_Program,
             Count(*)
      FROM   vResident_Program _program
             inner join tResident resident
                     on _program.ID_Resident = resident.ID
             INNER JOIN vCompanyActive c
                     on resident.ID_Company = c.ID
      WHERE  c.ID NOT IN ( 1 )
             AND resident.IsActive = 1
             and ISNULL(resident.IsDeceased, 0) = 0
      GROUP  BY Name_Program

      IF (SELECT COUNT(*)
          FROM   @dataSource) = 0
        BEGIN
            INSERT @dataSource
            SELECT 'No Record',
                   0
        END

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear DateYear

      SELECT *
      FROM   @dataSource
  END

GO

GO

Create   OR
ALTER PROC dbo.pGetDashboardBarangayHealthWorkerResidentReligionDataSource (@ID_UserSession INT,
                                                                            @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear                DateYear,
             'Relihiyon ' + @DateYear Label

      SELECT Name_Religion,
             Count(*) Count
      FROM   vResident resident
             inner join vCompanyActive c
                     on resident.ID_Company = c.ID
      WHERE  resident.IsActive = 1
             AND c.ID NOT IN ( 1 )
             AND YEAR(resident.DateCreated) = @DateYear
             and ISNULL(resident.IsDeceased, 0) = 0
             and ISNULL(resident.ID_Religion, 0) > 0
      GROUP  BY Name_Religion
  END

GO

GO

CREATE   OR
ALTER PROC dbo.pGetDashboardBarangayHealthWorkerMonthlyVaccinatedResidentDataSource (@ID_UserSession INT,
                                                                                     @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      DECLARE @ClientDailyCount INT = 0
      DECLARE @ClientMonthlyCount INT = 0
      DECLARE @SalesDailyAmount DECIMAL(18, 2) = 0
      DECLARE @SalesMonthlyAmount DECIMAL(18, 2) = 0

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear                   DateYear,
             'Mga Bakunado ' + @DateYear Label

      SELECT SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 1 THEN 1
                   ELSE 0
                 END) AS 'January',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 2 THEN 1
                   ELSE 0
                 END) AS 'February',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 3 THEN 1
                   ELSE 0
                 END) AS 'March',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 4 THEN 1
                   ELSE 0
                 END) AS 'April',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 5 THEN 1
                   ELSE 0
                 END) AS 'May',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 6 THEN 1
                   ELSE 0
                 END) AS 'June',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 7 THEN 1
                   ELSE 0
                 END) AS 'July',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 8 THEN 1
                   ELSE 0
                 END) AS 'August',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 9 THEN 1
                   ELSE 0
                 END) AS 'September',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 10 THEN 1
                   ELSE 0
                 END) AS 'October',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 11 THEN 1
                   ELSE 0
                 END) AS 'November',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 12 THEN 1
                   ELSE 0
                 END) AS 'December',
             SUM(CASE datepart(year, resident.DateCreated)
                   WHEN 2012 THEN 1
                   ELSE 0
                 END) AS 'TOTAL',
             ID_Company
      FROM   tResident resident
             inner join vCompanyActive c
                     on resident.ID_Company = c.ID
      WHERE  resident.IsActive = 1
             AND c.ID NOT IN ( 1 )
             AND YEAR(resident.DateCreated) = @DateYear
             and resident.IsVaccinated = 1
      GROUP  BY ID_Company
  END

GO

CREATE    OR
ALTER PROC pCreateBarangayLogins(@CompanyName       VARCHAR(MAX),
                                 @CompanyCode       VARCHAR(MAX),
                                 @MaxSMSCountPerDay INT = 25)
AS
  BEGIN
      exec pCreateNewCompanyAccess_validation
        @CompanyName,
        @CompanyCode

      exec pCreateCompanyLogins
        @CompanyName,
        @CompanyCode,
        @MaxSMSCountPerDay

      Update tCompany
      SET    MainRouteLink = 'Barangay'
      WHERE  Code = @CompanyCode
             AND Name = @CompanyName
  END

GO

CREATE OR
ALTER PROC pCreateBarangayHealthWorkerLogins(@CompanyName       VARCHAR(MAX),
                                             @CompanyCode       VARCHAR(MAX),
                                             @MaxSMSCountPerDay INT = 25)
AS
  BEGIN
      exec pCreateNewCompanyAccess_validation
        @CompanyName,
        @CompanyCode

      exec pCreateCompanyLogins
        @CompanyName,
        @CompanyCode,
        @MaxSMSCountPerDay

      Update tCompany
      SET    MainRouteLink = 'BarangayHealthWorker'
      WHERE  Code = @CompanyCode
             AND Name = @CompanyName
  END

GO

GO

CREATE   OR
ALTER PROC dbo.pHouseholdNumber_Delete_Validation (@ID_HouseholdNumber INT,
                                                   @ID_UserSession     INT)
AS
  BEGIN
      DECLARE @message VARCHAR(MAX) = ''
      DECLARE @isValid BIT = 1
      DECLARE @ID_User Int
      DECLARE @ID_Company Int

      SELECT @ID_User = ID_User
      FROM   tUserSession
      WHERE  ID = @ID_UserSession

      SELECT @ID_Company = ID_Company
      FROm   vUser
      WHERE  ID = @ID_User

      BEGIN TRY
          DECLARE @Count_Resident INT = 0

          SELECT @Count_Resident = COUNT(*)
          FROM   tResident
          WHERE  ID_Company = @ID_Company
                 and ID_HouseholdNumber = @ID_HouseholdNumber
                 and IsActive = 1
                 and IsHeadofFamily = 1

          IF ( @Count_Resident ) > 0
            BEGIN
                SET @message = 'Household has '
                               + CONVERT(VARCHAR(MAX), @Count_Resident)
                               + ' resident'
                               + CASE
                                   WHEN @Count_Resident > 1 THEN 's'
                                   ELSE ''
                                 END
                               + ' need'
                               + CASE
                                   WHEN @Count_Resident = 1 THEN 's'
                                   ELSE ''
                                 END
                               + ' to be remove.';

                THROW 50001, @message, 1;
            END;
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @isValid = 0;
      END CATCH

      SELECT '_';

      SELECT @isValid isValid,
             @message message;
  END

GO

exec pInsertPosition
  'Barangay Health Worker'

GO

exec pInsertPosition
  'BNS'

exec pInsertReligion
  'Roman Catholic'

exec pInsertReligion
  'Born Again Christian'

exec pInsertReligion
  'Iglesia Ni Cristo'

exec pInsertReligion
  'Baptist'

exec pInsertReligion
  'Islam'

GO

exec pInsertLawSuitStatus
  'Pending',
  1

exec pInsertLawSuitStatus
  'Nagkasundo',
  1

exec pInsertLawSuitStatus
  'Hindi Nagkasundo',
  1

GO

Update tLawSuit
set    ComplainantName = ISNULL(ComplainantName, '')

Update tLawSuit
set    RespondentName = ISNULL(RespondentName, '')

Update tLawSuit
set    ID_LawSuitStatus = ISNULL(ID_LawSuitStatus, 1)

exec pGetDashboardResidentReligionDataSource
  @ID_UserSession=N'61308',
  @DateYear=N'2022'

exec pGetDashboardBarangaySummary
  @ID_UserSession=N'61331'

Go

BEGIN TRY
    exec pCreateBarangayHealthWorkerLogins
      'City Health Worker',
      'chw'
END TRY
BEGIN CATCH
    SELECT ERROR_MESSAGE() Message
END CATCH 
