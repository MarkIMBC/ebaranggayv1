

GO  

CREATE OR ALTER FUNCTION [dbo].[fGetIntAge] (@dateBirth DATETIME)
RETURNS INT
AS
  BEGIN
      DECLARE @Now DATETIME
	  DECLARE @Age INT
	  SET @Now=GETDATE()

     SET @Age = ( CONVERT(int, CONVERT(char(8), @Now, 112)) - CONVERT(char(8), @dateBirth, 112) ) / 10000 

	  RETURN @Age
  END 

GO



exec _pRefreshAllViews

GO

ALTER VIEW vResident  
AS  
  SELECT H.*,  
         UC.Name                  AS CreatedBy,  
         UM.Name                  AS LastModifiedBy,
         gender.Name              Name_Gender,  
         company.Name             Name_Company,  
         civilStatus.Name         Name_CivilStatus,  
         educationalLevel.Name    Name_EducationalLevel,  
         homeOwnershipStatus.Name Name_HomeOwnershipStatus,  
         occupationalStatus.Name  Name_OccupationalStatus  
  FROM   tResident H  
         LEFT JOIN tUser UC  
                ON H.ID_CreatedBy = UC.ID  
         LEFT JOIN tUser UM  
                ON H.ID_LastModifiedBy = UM.ID  
         LEFT JOIN tGender gender  
                ON gender.ID = h.ID_Gender  
         LEFT JOIN tCivilStatus civilStatus  
                ON civilStatus.ID = h.ID_CivilStatus  
         LEFT JOIN tEducationalLevel educationalLevel  
                ON educationalLevel.ID = h.ID_EducationalLevel  
         LEFT JOIN tHomeOwnershipStatus homeOwnershipStatus  
                ON homeOwnershipStatus.ID = h.ID_HomeOwnershipStatus  
         LEFT JOIN tOccupationalStatus occupationalStatus  
                ON occupationalStatus.ID = h.ID_OccupationalStatus  
         LEFT JOIN tCompany company  
                ON company.ID = h.ID_Company  
 GO

 ALTER FUNCTION [dbo].[fGetOrdinalNumber] (@Date DATETIME)
RETURNS VARCHAR(15)
AS
  BEGIN
      DECLARE @day INT;
      DECLARE @strday VARCHAR (15);

      SELECT @day = DAY(@Date);

      SET @strday = CASE
                      WHEN @day IN ( 11, 12, 13 ) THEN CAST(@day AS VARCHAR(10)) + 'th'
                      WHEN @day % 10 = 1 THEN CAST(@day AS VARCHAR(10)) + 'st'
                      WHEN @day % 10 = 2 THEN CAST(@day AS VARCHAR(10)) + 'nd'
                      WHEN @day % 10 = 3 THEN CAST(@day AS VARCHAR(10)) + 'rd'
                      ELSE CAST(@day AS VARCHAR(10)) + 'th'
                    END

      RETURN @strday
  END 

GO
  
ALTER   
 VIEW [dbo].[vzResidentDetailReport]  
AS  
  SELECT hed.Name,
		 hed.IsActive,
		 hed.ID_Company,
		 hed.ID_Gender,
		 hed.ContactNumber,
		 hed.Address,
		 hed.Name_Gender,
		 hed.Name_Company,
         company.ImageLogoLocationFilenamePath,  
         CASE  
           WHEN LEN(company.Address) > 0 THEN '' + company.Address  
           ELSE ''  
         END  
         + CASE  
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber  
             ELSE ''  
           END  
         + CASE  
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email  
             ELSE ''  
           END HeaderInfo_Company,
		   [dbo].[fGetOrdinalNumber] (GETDATE()) AS  OrdinalNum,
		   dbo.fGetIntAge(hed.DateBirth)  AS Age
  FROM   vResident hed  
         LEFT JOIN dbo.vCompany company  
                ON company.ID = hed.ID_Company  
  WHERE  hed.IsActive = 1  


  GO

  ALTER VIEW vzResidentVotersListReport
  AS
    SELECT hed.ID,
           hed.Name,
           hed.Name_Gender,
           hed.ContactNumber,
           dbo.fGetIntAge(hed.DateBirth)                           Age,
           company.ID                                              ID_Company,
           company.ImageLogoLocationFilenamePath                   ImageLogoLocationFilenamePath,
           company.Name                                            Name_Company,
           company.Address                                         Address_Company,
           company.ContactNumber                                   ContactNumber_Company,
           company.BarangayListSideReportHTMLString                BarangayListSideReportHTMLString_Company,
           company.BarangayCaptainName                             BarangayCaptainName_Company,
           company.BarangayCaptain_Name_Employee                   BarangayCaptain_Name_Employee,
           company.BarangayCaptain_LastName_Employee               BarangayCaptain_LastName_Employee,
           company.BarangayCaptain_FirstName_Employee              BarangayCaptain_FirstName_Employee,
           company.BarangayCaptain_MiddleName_Employee             BarangayCaptain_MiddleName_Employee,
           company.BarangayCaptain_ImageSignitureSpecimen_Employee BarangayCaptain_ImageSignitureSpecimen_Employee,
           municipality.Name                                       Name_MunicipalityCompany,
           CASE
             WHEN LEN(company.Address) > 0 THEN '' + company.Address
             ELSE ''
           END
           + CASE
               WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
               ELSE ''
             END
           + CASE
               WHEN LEN(company.Email) > 0 THEN ' <br/>Email: ' + company.Email
               ELSE ''
             END                                                   HeaderInfo_Company,
           municipality.ImageLogoLocationFilenamePath              ImageLogoLocationFilenamePath_MunicipalityCompany,
           municipality.Address                                    Address_MunicipalityCompany
    FROM   vResident hed
           INNER JOIN vCompanyActive company
                   ON company.ID = hed.ID_Company
           INNER JOIN vMunicipalityCompany municipality
                   ON company.ID = hed.ID_Company
    WHERE  hed.IsActive = 1
           and hed.IsVoter = 1 
  GO

    
ALTER  
 VIEW [dbo].[vzResidentProgramReport]  
AS  
  SELECT hed.ID,  
         hed.Name,  
         hed.ID_Gender,  
         hed.Name_Gender,  
         hed.ContactNumber,
        dbo.fGetIntAge(hed.DateBirth)                           Age,
  REPLACE(rPrgString.ProgramString, ', ', '<br/>') ProgramString,  
         hed.ID_Company,  
         company.Name                                                                Name_Company,  
         company.ImageLogoLocationFilenamePath,  
         CASE  
           WHEN LEN(company.Address) > 0 THEN '' + company.Address  
           ELSE ''  
         END  
         + CASE  
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber  
             ELSE ''  
           END  
         + CASE  
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email  
             ELSE ''  
           END                                                                       HeaderInfo_Company  
  FROM   vResident hed  
      INNER JOIN vResidentProgramString rPrgString ON hed.ID = rPrgString.ID_Resident  
         LEFT JOIN dbo.vCompany company  
                ON company.ID = hed.ID_Company  
  WHERE  hed.IsActive = 1  
  
GO