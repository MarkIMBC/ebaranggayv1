GO
IF col_Length('dbo.tResident', 'IsDeceased') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tResident',
        'IsDeceased',
        4

      EXEC _pRefreshAllViews
  END

GO
IF col_Length('dbo.tResident', 'ReferenceID') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tResident',
        'ReferenceID',
        1

      EXEC _pRefreshAllViews
  END

GO

IF col_Length('dbo.tCertificateOfIndigency', 'Requestor') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tCertificateOfIndigency',
        'Requestor',
        1

      EXEC _pRefreshAllViews
  END

GO

ALTER VIEW [dbo].vzCertificationIndigencyFormReport
AS
  SELECT hed.ID                                                  ID,
         hed.Name                                                Name,
         hed.Code                                                Code,
         hed.Date                                                Date,
         hed.Address                                             Address,
         hed.Age                                                 Age,
         gender.TranslatedEngName                                TranslatedEngName_Gender,
         civilStatus.TranslatedEngName                           TranslatedEngName_CivilStatus,
         hed.Citizenship,
         hed.RequestOf,
         hed.Requestor,
         company.ID                                              ID_Company,
         company.ImageLogoLocationFilenamePath                   ImageLogoLocationFilenamePath,
         company.Name                                            Name_Company,
         REPLACE(company.Name, 'Brgy.', 'Barangay')              BarangayFullName_Company,
         company.Address                                         Address_Company,
         company.ContactNumber                                   ContactNumber_Company,
         company.BarangayListSideReportHTMLString                BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                             BarangayCaptainName_Company,
         company.BarangayCaptain_Name_Employee                   BarangayCaptain_Name_Employee,
         company.BarangayCaptain_ImageSignitureSpecimen_Employee BarangayCaptain_ImageSignitureSpecimen_Employee,
         company.BarangayCaptain_LastName_Employee               BarangayCaptain_LastName_Employee,
         company.BarangayCaptain_FirstName_Employee              BarangayCaptain_FirstName_Employee,
         company.BarangayCaptain_MiddleName_Employee             BarangayCaptain_MiddleName_Employee,
         municipality.Name                                       Name_MunicipalityCompany,
         municipality.ImageLogoLocationFilenamePath              ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                                    Address_MunicipalityCompany,
         [dbo].[fGetOrdinalNumber] (GETDATE())                   AS OrdinalNum
  FROM   vCertificateOfIndigency hed
         LEFT Join tCivilStatus civilStatus
                ON civilStatus.ID = hed.ID_CivilStatus
         LEFT Join tGender gender
                ON gender.ID = hed.ID_Gender
         INNER JOIN vCompanyActive company
                 ON company.ID = hed.ID_Company
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = hed.ID_Company

--LEFT Join tCivilStatus civilStatus    
--       ON civilStatus.ID = hed.ID_CivilStatus    
--LEFT Join tGender gender    
--       ON gender.ID = hed.ID_  
--db_ebarangayv1_dev
GO

ALTER VIEW vResident_ListvIew
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         hed.IsDeceased,
         company.Guid Guid_Company
  FROM   vResident hed
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  ISNULL(hed.IsDeceased, 0) IS NULL

GO

CREATE OR
ALTER VIEW vResident_Listview_Deceased
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         hed.IsDeceased,
         company.Guid Guid_Company
  FROM   vResident hed
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  Where  hed.IsActive = 1
         and hed.IsDeceased = 1

GO 
