GO

IF col_Length('dbo.tResident', 'IsBoarder') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tResident',
        'IsBoarder',
        4

      EXEC _pRefreshAllViews
  END

GO

GO

IF col_Length('dbo.tBarangayClearance', 'Citizenship') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tBarangayClearance',
        'Citizenship',
        1

      EXEC _pRefreshAllViews
  END

GO

GO

IF col_Length('dbo.tCertificateOfResidency', 'ID_Gender') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tCertificateOfResidency',
        'ID_Gender',
        2

      EXEC _pRefreshAllViews
  END

GO

IF col_Length('dbo.tCertificateOfResidency', 'ID_CivilStatus') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tCertificateOfResidency',
        'ID_CivilStatus',
        2

      EXEC _pRefreshAllViews
  END

GO

GO

IF col_Length('dbo.tCertificateOfIndigency', 'ID_Gender') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tCertificateOfIndigency',
        'ID_Gender',
        2

      EXEC _pRefreshAllViews
  END

GO

IF col_Length('dbo.tCertificateOfIndigency', 'ID_CivilStatus') IS NULL
  BEGIN
      EXEC _PaddModelProperty
        'tCertificateOfIndigency',
        'ID_CivilStatus',
        2

      EXEC _pRefreshAllViews
  END

GO

IF NOT EXISTS (SELECT 1
               FROM   tFamilyRelationshipType
               WHERE  Name = 'Apo')
  BEGIN
      INSERT INTO [dbo].[tFamilyRelationshipType]
                  ([Name],
                   [IsActive],
                   [ID_Company],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy])
      VALUES      ('Apo',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1)
  END

GO

IF NOT EXISTS (SELECT 1
               FROM   tFamilyRelationshipType
               WHERE  Name = 'Pamangkin')
  BEGIN
      INSERT INTO [dbo].[tFamilyRelationshipType]
                  ([Name],
                   [IsActive],
                   [ID_Company],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy])
      VALUES      ('Pamangkin',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1)
  END

GO

IF NOT EXISTS (SELECT 1
               FROM   tProgram
               WHERE  Name = 'Listahanan')
  BEGIN
      INSERT INTO [dbo].[tProgram]
                  ([Name],
                   [IsActive],
                   [ID_Company],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy])
      VALUES      ('Listahanan',
                   1,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1)
  END

GO

exec _pRefreshAllViews

GO

CREATE  OR
ALTER VIEW [dbo].[vProgram_Listview]
as
  SELECT hed.ID,
         hed.Code,
         hed.Name,
         hed.DateCreated,
         hed.DateModified,
         hed.ID_Company,
         hed.IsActive
  FROM   vProgram hed

GO

CREATE OR
ALTER FUNCTION [dbo].[fGetOrdinalNumber] (@Date DATETIME)
RETURNS VARCHAR(15)
AS
  BEGIN
      DECLARE @day INT;
      DECLARE @strday VARCHAR (15);

      SELECT @day = DAY(@Date);

      SET @strday = CASE
                      WHEN @day IN ( 11, 12, 13 ) THEN CAST(@day AS VARCHAR(10)) + 'th'
                      WHEN @day % 10 = 1 THEN CAST(@day AS VARCHAR(10)) + 'st'
                      WHEN @day % 10 = 2 THEN CAST(@day AS VARCHAR(10)) + 'nd'
                      WHEN @day % 10 = 3 THEN CAST(@day AS VARCHAR(10)) + 'rd'
                      ELSE CAST(@day AS VARCHAR(10)) + 'th'
                    END

      RETURN @strday
  END

GO

GO

CREATE   OR
ALTER VIEW vBarangayClearance
AS
  SELECT H.*,
         UC.Name          AS CreatedBy,
         UM.Name          AS LastModifiedBy,
         civilStatus.Name Name_CivilStatus,
         gender.Name      Name_Gender
  FROM   tBarangayClearance H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tCivilStatus civilStatus
                ON H.ID_CivilStatus = civilStatus.ID
         LEFT JOIN tGender gender
                ON H.ID_Gender = gender.ID

GO

CREATE    OR
ALTER VIEW vCertificateOfIndigency
AS
  SELECT H.*,
         UC.Name          AS CreatedBy,
         UM.Name          AS LastModifiedBy,
         gender.Name      Name_Gender,
         civilStatus.Name Name_CivilStatus
  FROM   tCertificateOfIndigency H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tGender gender
                ON H.ID_Gender = gender.ID
         LEFT JOIN tCivilStatus civilStatus
                ON H.ID_CivilStatus = civilStatus.ID

GO

ALTER VIEW vCertificateOfResidency
AS
  SELECT H.*,
         UC.Name          AS CreatedBy,
         UM.Name          AS LastModifiedBy,
         gender.Name      Name_Gender,
         civilStatus.Name Name_CivilStatus
  FROM   tCertificateOfResidency H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tGender gender
                ON H.ID_Gender = gender.ID
         LEFT JOIN tCivilStatus civilStatus
                ON H.ID_CivilStatus = civilStatus.ID

GO

ALTER VIEW [dbo].vzBarangayClearanceFormReport
AS
  SELECT hed.ID                                                  ID,
         hed.Name                                                Name,
         hed.Code                                                Code,
         hed.Date                                                Date,
         hed.Address                                             Address,
         hed.Age                                                 Age,
         hed.Name_CivilStatus                                    Name_CivilStatus,
         hed.Requestor,
         hed.Name_Gender,
         gender.TranslatedEngName                                TranslatedEngName_Gender,
         hed.Purpose,
         hed.Citizenship,
         civilStatus.TranslatedEngName                           TranslatedEngName_CivilStatus,
         dbo.fGetAPILink() + '/Content/Image/'
         + hed.ImageProfilePicFilename                           ImageProfilePicFilenamePath_BarangayClearance,
         company.ID                                              ID_Company,
         company.ImageLogoLocationFilenamePath                   ImageLogoLocationFilenamePath,
         company.Name                                            Name_Company,
         REPLACE(company.Name, 'Brgy.', 'Barangay')              BarangayFullName_Company,
         company.Address                                         Address_Company,
         company.ContactNumber                                   ContactNumber_Company,
         company.BarangayListSideReportHTMLString                BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                             BarangayCaptainName_Company,
         company.BarangayCaptain_Name_Employee                   BarangayCaptain_Name_Employee,
         company.BarangayCaptain_ImageSignitureSpecimen_Employee BarangayCaptain_ImageSignitureSpecimen_Employee,
         company.BarangayCaptain_LastName_Employee               BarangayCaptain_LastName_Employee,
         company.BarangayCaptain_FirstName_Employee              BarangayCaptain_FirstName_Employee,
         company.BarangayCaptain_MiddleName_Employee             BarangayCaptain_MiddleName_Employee,
         municipality.Name                                       Name_MunicipalityCompany,
         municipality.ImageLogoLocationFilenamePath              ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                                    Address_MunicipalityCompany,
         [dbo].[fGetOrdinalNumber] (GETDATE())                   AS OrdinalNum
  FROM   vBarangayClearance hed
         INNER JOIN vCompanyActive company
                 ON company.ID = hed.ID_Company
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = hed.ID_Company
         LEFT Join tCivilStatus civilStatus
                ON civilStatus.ID = hed.ID_CivilStatus
         LEFT Join tGender gender
                ON gender.ID = hed.ID_Gender

GO

ALTER VIEW [dbo].vzCertificationResidencyFormReport
AS
  SELECT hed.ID                                                  ID,
         hed.Name                                                Name,
         hed. Purpose,
         dbo.fGetAPILink() + '/Content/Image/'
         + hed.ImageProfilePicFilename                           ImageProfilePicFilenamePath_CertificateOfResidency,
         hed.Code                                                Code,
         hed.Date                                                Date,
         hed.Address                                             Address,
         hed.Age                                                 Age,
         hed.Citizenship,
         hed.Requestor,
         hed.Name_Gender,
         hed.Name_CivilStatus,
         gender.TranslatedEngName                                TranslatedEngName_Gender,
         civilStatus.TranslatedEngName                           TranslatedEngName_CivilStatus,
         hed.StayInDuration,
         company.ID                                              ID_Company,
         company.ImageLogoLocationFilenamePath                   ImageLogoLocationFilenamePath,
         REPLACE(company.Name, 'Brgy.', 'Barangay')              BarangayFullName_Company,
         company.Name                                            Name_Company,
         company.Address                                         Address_Company,
         company.ContactNumber                                   ContactNumber_Company,
         company.BarangayListSideReportHTMLString                BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                             BarangayCaptainName_Company,
         company.BarangayCaptain_Name_Employee                   BarangayCaptain_Name_Employee,
         company.BarangayCaptain_LastName_Employee               BarangayCaptain_LastName_Employee,
         company.BarangayCaptain_FirstName_Employee              BarangayCaptain_FirstName_Employee,
         company.BarangayCaptain_MiddleName_Employee             BarangayCaptain_MiddleName_Employee,
         company.BarangayCaptain_ImageSignitureSpecimen_Employee BarangayCaptain_ImageSignitureSpecimen_Employee,
         municipality.Name                                       Name_MunicipalityCompany,
         municipality.ImageLogoLocationFilenamePath              ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                                    Address_MunicipalityCompany,
         [dbo].[fGetOrdinalNumber] (GETDATE())                   AS OrdinalNum
  FROM   vCertificateOfResidency hed
         INNER JOIN vCompanyActive company
                 ON company.ID = hed.ID_Company
         LEFT Join tCivilStatus civilStatus
                ON civilStatus.ID = hed.ID_CivilStatus
         LEFT Join tGender gender
                ON gender.ID = hed.ID_Gender
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = hed.ID_Company

GO

ALTER VIEW [dbo].vzCertificationIndigencyFormReport
AS
  SELECT hed.ID                                                  ID,
         hed.Name                                                Name,
         hed.Code                                                Code,
         hed.Date                                                Date,
         hed.Address                                             Address,
         hed.Age                                                 Age,
         gender.TranslatedEngName                                TranslatedEngName_Gender,
         civilStatus.TranslatedEngName                           TranslatedEngName_CivilStatus,
         hed.Citizenship,
         hed.RequestOf,
         company.ID                                              ID_Company,
         company.ImageLogoLocationFilenamePath                   ImageLogoLocationFilenamePath,
         company.Name                                            Name_Company,
         REPLACE(company.Name, 'Brgy.', 'Barangay')              BarangayFullName_Company,
         company.Address                                         Address_Company,
         company.ContactNumber                                   ContactNumber_Company,
         company.BarangayListSideReportHTMLString                BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                             BarangayCaptainName_Company,
         company.BarangayCaptain_Name_Employee                   BarangayCaptain_Name_Employee,
         company.BarangayCaptain_ImageSignitureSpecimen_Employee BarangayCaptain_ImageSignitureSpecimen_Employee,
         company.BarangayCaptain_LastName_Employee               BarangayCaptain_LastName_Employee,
         company.BarangayCaptain_FirstName_Employee              BarangayCaptain_FirstName_Employee,
         company.BarangayCaptain_MiddleName_Employee             BarangayCaptain_MiddleName_Employee,
         municipality.Name                                       Name_MunicipalityCompany,
         municipality.ImageLogoLocationFilenamePath              ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                                    Address_MunicipalityCompany,
         [dbo].[fGetOrdinalNumber] (GETDATE())                   AS OrdinalNum
  FROM   vCertificateOfIndigency hed
         LEFT Join tCivilStatus civilStatus
                ON civilStatus.ID = hed.ID_CivilStatus
         LEFT Join tGender gender
                ON gender.ID = hed.ID_Gender
         INNER JOIN vCompanyActive company
                 ON company.ID = hed.ID_Company
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = hed.ID_Company

--LEFT Join tCivilStatus civilStatus  
--       ON civilStatus.ID = hed.ID_CivilStatus  
--LEFT Join tGender gender  
--       ON gender.ID = hed.ID_
GO

CREATE OR
ALTER VIEW vResident_ProgramListView_EducationalAssistance
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid         Guid_Company,
         program.ID_Program   ID_Program,
         program.Name_Program Name_Program
  FROM   vResident hed
         INNER JOIN vResident_Program program
                 on hed.ID = program.ID_Resident
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  program.ID_Program IN ( 6, 7 )

GO

CREATE OR
ALTER VIEW vResident_ProgramListView_4Ps
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid         Guid_Company,
         program.ID_Program   ID_Program,
         program.Name_Program Name_Program
  FROM   vResident hed
         INNER JOIN vResident_Program program
                 on hed.ID = program.ID_Resident
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  program.ID_Program IN ( 4 )

GO

CREATE OR
ALTER VIEW vResident_ProgramListView_PWD
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid         Guid_Company,
         program.ID_Program   ID_Program,
         program.Name_Program Name_Program
  FROM   vResident hed
         INNER JOIN vResident_Program program
                 on hed.ID = program.ID_Resident
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  program.ID_Program IN ( 8 )

GO

CREATE OR
ALTER VIEW vResident_ProgramListView_SeniorCitizen
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid         Guid_Company,
         program.ID_Program   ID_Program,
         program.Name_Program Name_Program
  FROM   vResident hed
         INNER JOIN vResident_Program program
                 on hed.ID = program.ID_Resident
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  program.ID_Program IN ( 3, 9 )

GO

CREATE OR
ALTER VIEW vResident_ProgramListView_SoloParent
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid         Guid_Company,
         program.ID_Program   ID_Program,
         program.Name_Program Name_Program
  FROM   vResident hed
         INNER JOIN vResident_Program program
                 on hed.ID = program.ID_Resident
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  program.ID_Program IN ( 10 )

GO

CREATE OR
ALTER VIEW vResident_ProgramListView_Listahanan
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid         Guid_Company,
         program.ID_Program   ID_Program,
         program.Name_Program Name_Program
  FROM   vResident hed
         INNER JOIN vResident_Program program
                 on hed.ID = program.ID_Resident
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  program.ID_Program IN ( 18 )

GO

CREATE OR
ALTER VIEW vResident_Listview_Boarder
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid Guid_Company
  FROM   vResident hed
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  Where  hed.IsActive = 1
         and hed.IsBoarder = 1

GO

Update tCompany
SET    Name = 'City Of Lipa, Batangas'
WHERE  Name = 'City Of Lipa batangas'

Update tCompany
SET    Name = 'Brgy. Tambo',
       BarangayListSideReportHTMLString =
'<center><b style="color: red">SANGGUNIANG PAMBARANGAY</b><br/><b>HON. JOEL I. KATIGBAK</b><br/> Barangay Chairman</br></center><br/> <b>HON. DIGNA A. BAUTISTA</b><br/> - Committee on Cooperatives and Agriculture<br/> - Committee on Women and Family,LGBT and Persons with Disability<br/> - Committee on Livelihood<br/><br/><b>HON. ARTURO I. ZARA</b><br/>- Committee on Finance, Budget and Appropriation<br/> - Committee on Rules and Privileges<br/> - Committee on Good Governance<br/> <br/><b>HON. GINA G. LUNA</b><br/>- Committee on Environmental Protection<br/> - Committee on Senior Citizen/ Urban Poor<br/> <br/><b>HON. SANDY S. ILUSTRE</b><br/> - Committee on Public Works<br/> - Committee on Trade and Industry<br/> <br/><b>HON. APOLLO VICENTE G. ORENSE</b><br/> - Committee on Information Technology<br/> - Committee on Religious & Liturgical Affairs<br/> - Committee on Cultural Affairs and Tourism<br/> - Committee on Risk Reduction<br/> <br/><b>HON. ALLAN J. SADSAD</b><br/> - Committee on Education<br/>- Committee on Health and Sanitation<br/> - Committee on Peace and Order, Public Safety and Drug Abuse<br/> <br/><b>HON.CAYETANO A. GANDEZA JR.</b><br/> - Committee on Labor and Employment<br/> - Committee on Transportation<br/> <br/><b>HON. MARY LOIS L. GANDEZA</b><br/> - SK CHAIRPERSON<br/>- Committee on Youth and Sports'
WHERE  Guid = '66A2A296-7EBD-4A81-960E-D435C9D22DF7'

SELECT *
FROm   vCompanyActive

select *
FROM   vCompanyActiveUser
Order  by Name_Company 
