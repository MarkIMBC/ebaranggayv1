GO

CREATE OR
ALTER VIEW vResident_ProgramListView_SeniorCitizen
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid         Guid_Company,
         program.ID_Program   ID_Program,
         program.Name_Program Name_Program
  FROM   vResident hed
         INNER JOIN vResident_Program program
                 on hed.ID = program.ID_Resident
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  program.Name_Program IN ( 'Senior Citizen Social Pension' )
         AND hed.IsActive = 1
         AND ISNULL(hed.IsDeceased, 0) = 0

GO

CREATE  OR
ALTER VIEW vResident_ProgramListView_EducationalAssistance
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid         Guid_Company,
         program.ID_Program   ID_Program,
         program.Name_Program Name_Program
  FROM   vResident hed
         INNER JOIN vResident_Program program
                 on hed.ID = program.ID_Resident
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  program.Name_Program IN ( 'Educational Financial Assistance (Senior High)', 'Educational Financial Assistance (College)' )
         AND hed.IsActive = 1
         AND ISNULL(hed.IsDeceased, 0) = 0

GO

CREATE OR
ALTER VIEW vResident_ProgramListview_GSIS
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid         Guid_Company,
         program.ID_Program   ID_Program,
         program.Name_Program Name_Program
  FROM   vResident hed
         INNER JOIN vResident_Program program
                 on hed.ID = program.ID_Resident
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  program.Name_Program = 'GSIS'
         AND hed.IsActive = 1
         AND ISNULL(hed.IsDeceased, 0) = 0

GO

CREATE OR
ALTER VIEW vResident_ProgramListView_Listahanan
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid         Guid_Company,
         program.ID_Program   ID_Program,
         program.Name_Program Name_Program
  FROM   vResident hed
         INNER JOIN vResident_Program program
                 on hed.ID = program.ID_Resident
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  program.Name_Program IN ( 'Listahanan' )
         and hed.IsActive = 1
         AND ISNULL(hed.IsDeceased, 0) = 0

GO

CREATE OR
ALTER VIEW vResident_ProgramListView_4Ps
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid         Guid_Company,
         program.ID_Program   ID_Program,
         program.Name_Program Name_Program
  FROM   vResident hed
         INNER JOIN vResident_Program program
                 on hed.ID = program.ID_Resident
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  program.Name_Program IN ( '4Ps' )
         AND hed.IsActive = 1
         AND ISNULL(hed.IsDeceased, 0) = 0

GO

CREATE  OR
ALTER VIEW vResident_ProgramListview_PhiiHealth
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid         Guid_Company,
         program.ID_Program   ID_Program,
         program.Name_Program Name_Program
  FROM   vResident hed
         INNER JOIN vResident_Program program
                 on hed.ID = program.ID_Resident
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  program.Name_Program = 'PhiiHealth'
         AND hed.IsActive = 1
         AND ISNULL(hed.IsDeceased, 0) = 0

GO

CREATE  OR
ALTER VIEW vResident_ProgramListView_PWD
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid         Guid_Company,
         program.ID_Program   ID_Program,
         program.Name_Program Name_Program
  FROM   vResident hed
         INNER JOIN vResident_Program program
                 on hed.ID = program.ID_Resident
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  program.Name_Program IN ( 'PWD Financial Assistance' )
         AND hed.IsActive = 1
         AND ISNULL(hed.IsDeceased, 0) = 0

GO

CREATE OR
ALTER VIEW vResident_ProgramListview_SoloParentsCashIncentives
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid         Guid_Company,
         program.ID_Program   ID_Program,
         program.Name_Program Name_Program
  FROM   vResident hed
         INNER JOIN vResident_Program program
                 on hed.ID = program.ID_Resident
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  program.Name_Program = 'Solo Parents Cash Incentives'
         AND hed.IsActive = 1
         AND ISNULL(hed.IsDeceased, 0) = 0

GO

CREATE OR
ALTER VIEW vResident_ProgramListview_SSS
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid         Guid_Company,
         program.ID_Program   ID_Program,
         program.Name_Program Name_Program
  FROM   vResident hed
         INNER JOIN vResident_Program program
                 on hed.ID = program.ID_Resident
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  program.Name_Program = 'SSS'
         AND hed.IsActive = 1
         AND ISNULL(hed.IsDeceased, 0) = 0

GO

CREATE  OR
ALTER VIEW vResident_Listview_SeniorCitizen
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid Guid_Company,
         redAge.Age
  FROM   vResident hed
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
         LEFT JOIN vResidentAge redAge
                on redAge.ID_Resident = hed.ID
  WHERE  redAge.Age >= 60
         AND hed.IsActive = 1
         AND ISNULL(hed.IsDeceased, 0) = 0

GO

CREATE  OR
ALTER VIEW vResident_Listview_Boarder
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid Guid_Company
  FROM   vResident hed
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  Where  hed.IsActive = 1
         and hed.IsBoarder = 1
         AND ISNULL(hed.IsDeceased, 0) = 0
GO

CREATE  OR
ALTER VIEW vResident_Listview_OFW
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid Guid_Company
  FROM   vResident hed
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  Where  hed.IsActive = 1
         and hed.isOFW = 1
         AND ISNULL(hed.IsDeceased, 0) = 0

GO

CREATE OR
ALTER VIEW vResident_Listview_PWD
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid Guid_Company
  FROM   vResident hed
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  hed.IsActive = 1
         and hed.IsPwd = 1
         AND ISNULL(hed.IsDeceased, 0) = 0

GO

CREATE  OR
ALTER VIEW vResident_Listview_SoloParent
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid Guid_Company
  FROM   vResident hed
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  hed.IsActive = 1
         and hed.isSoloParent = 1
         AND ISNULL(hed.IsDeceased, 0) = 0

GO

CREATE OR
ALTER VIEW vResident_Listview_Voter
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid Guid_Company
  FROM   vResident hed
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  where  hed.IsActive = 1
         and hed.IsVoter = 1
         AND ISNULL(hed.IsDeceased, 0) = 0

GO

CREATE OR
ALTER VIEW vzResidentVotersListReport
AS
  SELECT hed.ID,
         hed.Name,
         hed.Name_Gender,
         hed.ID_Gender,
         hed.ContactNumber,
         dbo.fGetIntAge(hed.DateBirth)                           Age,
         company.ID                                              ID_Company,
         company.ImageLogoLocationFilenamePath                   ImageLogoLocationFilenamePath,
         company.Name                                            Name_Company,
         company.Address                                         Address_Company,
         company.ContactNumber                                   ContactNumber_Company,
         company.BarangayListSideReportHTMLString                BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                             BarangayCaptainName_Company,
         company.BarangayCaptain_Name_Employee                   BarangayCaptain_Name_Employee,
         company.BarangayCaptain_LastName_Employee               BarangayCaptain_LastName_Employee,
         company.BarangayCaptain_FirstName_Employee              BarangayCaptain_FirstName_Employee,
         company.BarangayCaptain_MiddleName_Employee             BarangayCaptain_MiddleName_Employee,
         company.BarangayCaptain_ImageSignitureSpecimen_Employee BarangayCaptain_ImageSignitureSpecimen_Employee,
         municipality.Name                                       Name_MunicipalityCompany,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' <br/>Email: ' + company.Email
             ELSE ''
           END                                                   HeaderInfo_Company,
         municipality.ImageLogoLocationFilenamePath              ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                                    Address_MunicipalityCompany
  FROM   vResident hed
         INNER JOIN vCompanyActive company
                 ON company.ID = hed.ID_Company
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = hed.ID_Company
  WHERE  hed.IsActive = 1
         and hed.IsVoter = 1

GO

CREATE   OR
ALTER PROC pChangeUserPassword(@ID_User        Int,
                               @Password       VARCHAR(MAX),
                               @ID_UserSession INT)
AS
  BEGIN
      DECLARE @IDs_Patient typIntList
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          Update tUser
          SET    Password = @Password
          WHERE  ID = @ID_User
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END 
