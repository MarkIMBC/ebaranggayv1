﻿using JSLibrary;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;

namespace SMSPatientBirthDateGreetings
{
    class Program
    {
        static void Main(string[] args)
        {
            dynamic appSettings = getAppSettings();

            DateTime dateStart = DateTime.Now;
            DateTime dateEnd;

            Console.WriteLine("Vet Cloud SMS Patient Birth Day Greeting Sender Service run as of {0}", DateTime.Now.ToString("MM/dd/yyyy ddd hh:mm tt"));

            try
            {

                sendSMS(appSettings);
            }
            catch (Exception ex)
            {


                Console.WriteLine(ex);
                System.Console.ReadKey();
            }

            dateEnd = DateTime.Now;

            System.TimeSpan diff2 = dateEnd - dateStart;

            Console.WriteLine("Done.. Duration - {0}", diff2.ToString("hh\\:mm\\:ss"));
            System.Console.ReadKey();
        }

        public static void sendSMS(dynamic appSettings)
        {

            string connectonString = appSettings["ConnectionString"];
            string apiCode = appSettings["ITexMoAPICode"];
            string apiPassword = appSettings["ITexMoAPIPassword"];

            List<int> soapPlan = new List<int>();

            var dbAccess = new DBCollection(connectonString);

            var DetailView = dbAccess.GetDynamicObject("EXEC dbo.pGetForSendPatientBirthDate", true);

            var records = DetailView.records as List<dynamic>;

            if (records != null)
            {

                foreach (var d in records)
                {
                
                    try
                    {
                        var iTextMo_Status = "";

                        iTextMo_Status = Utility.sendTextMessage(d.ContactNumber_Client, d.Message, apiCode, apiPassword);

                        var _result = new DBCollection(connectonString).GetDynamicObject($"EXEC dbo.pLog_BirthDateSMSGreeting @ID_Client = {d.ID_Client}, @ContactNumber = '{d.ContactNumber_Client}',  @ID_Patient = {d.ID_Patient},  @iTextMo_Status = {iTextMo_Status}, @Message = '{d.Message}'", true);

                        Console.WriteLine("Status {0} - {1} - {2} - {3} - {4}",
                            iTextMo_Status,
                            d.Name_Company,
                            d.Name_Client,
                            d.Name_Patient,
                            d.ContactNumber_Client
                         );

                        Thread.Sleep(5000);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        System.Console.ReadKey();
                    }

                }


            }

        }

        public static Dictionary<String, Object> Dyn2Dict(dynamic dynObj)
        {
            var dictionary = new Dictionary<string, object>();
            foreach (PropertyDescriptor propertyDescriptor in TypeDescriptor.GetProperties(dynObj))
            {
                object obj = propertyDescriptor.GetValue(dynObj);
                dictionary.Add(propertyDescriptor.Name, obj);
            }
            return dictionary;
        }


        public static dynamic getAppSettings()
        {

            string filepath = "C:\\inetpub\\wwwroot\\ebaranggayv1\\api\\appsettings.json";
            string readResult = string.Empty;
            string writeResult = string.Empty;
            dynamic data;

            using (StreamReader r = new StreamReader(filepath))
            {
                var json = r.ReadToEnd();
                var jobj = JObject.Parse(json);
                readResult = jobj.ToString();
                foreach (var item in jobj.Properties())
                {
                    item.Value = item.Value.ToString().Replace("v1", "v2");
                }
                writeResult = jobj.ToString();

                data = JsonConvert.DeserializeObject(writeResult);
            }

            return data;
        }
    }
}

