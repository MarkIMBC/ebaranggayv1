﻿using JSLibrary;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;

namespace SMSSender
{
    class Program
    {
        static void Main(string[] args)
        {
            dynamic appSettings = getAppSettings();

            DateTime dateStart = DateTime.Now;
            DateTime dateEnd;

            Console.WriteLine("Vet Cloud SMS Sender Service run as of {0}", DateTime.Now.ToString("MM/dd/yyyy ddd hh:mm tt"));

            try
            {

                sendSMS(appSettings);
            }
            catch (Exception ex) {


                Console.WriteLine(ex);
                System.Console.ReadKey();
            }

            dateEnd = DateTime.Now;

            System.TimeSpan diff2 = dateEnd - dateStart;

            Console.WriteLine("Done.. Duration - {0}", diff2.ToString("hh\\:mm\\:ss"));
            System.Console.ReadKey();
        }

        public static void sendSMS(dynamic appSettings) {

            string connectonString = appSettings["ConnectionString"];
            string apiCode = appSettings["ITexMoAPICode"];
            string apiPassword = appSettings["ITexMoAPIPassword"];

            List<int> soapPlan = new List<int>();

            var dbAccess = new DBCollection(connectonString);

            var DetailView = dbAccess.GetDynamicObject("EXEC dbo.pGetForSendSOAPPlan", true);

            var records = DetailView.records as List<dynamic>;

            if (records != null)
            {

                foreach (var d in records)
                {

                    var isAllowedObj = dbAccess.GetDynamicObject("EXEC dbo.pCheckCompanySMSSending " + d.ID_Company + ", '" + d.DateSending + "'", true);

                    var result = "";

                    if (isAllowedObj.IsAllowedToSendSMS)
                    {
                        result = Utility.sendTextMessage(d.ContactNumber_Client, d.Message, apiCode, apiPassword);
                    }
                    else 
                    {
                        result = "-1";
                    }

                    Console.WriteLine("Status {0} - {1} - {2} - {3} TotaSMSCount: {4}; MaxSMSCountPerDay: {5}; Date Sent: {6}", result, d.Name_Client, d.ContactNumber_Client, d.Name_Company, isAllowedObj.TotalSMSCount, isAllowedObj.MaxSMSCountPerDay, d.DateSending);

                    var _result = new DBCollection(connectonString).GetDynamicObject("EXEC dbo.pNoteSOAPPlanAsSend @ID_Reference = " + d.ID_Reference + ", @Oid_Model = '" + d.Oid_Model + "', @iTextMo_Status = " + result + "", true);
                }

            }
      
        }

        public static Dictionary<String, Object> Dyn2Dict(dynamic dynObj)
        {
            var dictionary = new Dictionary<string, object>();
            foreach (PropertyDescriptor propertyDescriptor in TypeDescriptor.GetProperties(dynObj))
            {
                object obj = propertyDescriptor.GetValue(dynObj);
                dictionary.Add(propertyDescriptor.Name, obj);
            }
            return dictionary;
        }


        public static dynamic getAppSettings()
        {
                       
            string filepath = "C:\\inetpub\\wwwroot\\ebaranggayv1\\api\\appsettings.json";
            string readResult = string.Empty;
            string writeResult = string.Empty;
            dynamic data;

            using (StreamReader r = new StreamReader(filepath))
            {
                var json = r.ReadToEnd();
                var jobj = JObject.Parse(json);
                readResult = jobj.ToString();
                foreach (var item in jobj.Properties())
                {
                    item.Value = item.Value.ToString().Replace("v1", "v2");
                }
                writeResult = jobj.ToString();

                data = JsonConvert.DeserializeObject(writeResult);
            }

            return data;
        }
    }
}

