requirejs.config({
    shim : {
        angular: {
            exports : 'angular'
           },
        app:{
            deps:['angular','jquery']
        }

    },
    paths: {
		'jquery': '../libraries/adminlte/plugins/jquery/jquery.min',
		'bootstrap': '../libraries/adminlte/plugins/bootstrap/js/bootstrap.bundle.min',
		'adminlte': './libraries/adminlte/dist/js/adminlte.min',
		'angular': '../libraries/angularjs/angular.min',
		'angular-route': '../libraries/angularjs/angular-route.min',
		'app': 'app'
    }
});

requirejs(['app']);




// define(['angular', 'content/pages/home/home'], function (angular, controller) {
//     angular.module('app',[])
//         .controller('myController', controller);
// });



/*


<script src="./libraries/angularjs/angular.min.js"></script>
<script src="./libraries/angularjs/angular-sanitize.min.js"></script>
<script src="./libraries/angularjs/angular-route.min.js"></script>


<script src="./content/app.js"></script>
<script src="./content/components/navbar/navbar.js"></script>
<script src="./content/components/sidebar/sidebar.js"></script>
<script src="./content/components/controlsidebar/controlsidebar.js"></script>
<script src="./content/components/footer/footer.js"></script>

<script src="./content/pages/home/home.js"></script>



    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    <script src="./libraries/adminlte/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="./libraries/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="./libraries/adminlte/dist/js/adminlte.min.js"></script>


*/