define(['angular'],function (angular) {

    angular.module('app', []);
    angular.bootstrap(document, ['app']);
  
  });