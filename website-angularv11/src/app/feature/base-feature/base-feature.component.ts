import { CustomNavigationLink } from './../../../shared/APP_MODELS';
import { Component, ElementRef, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { ValidationService } from 'src/app/core/validation.service';
import { ToastService } from 'src/app/shared/toast.service';
import { BaseViewComponent } from '../base-view/base-view.component';

@Component({
  selector: 'app-base-feature',
  templateUrl: './base-feature.component.html',
  styleUrls: ['./base-feature.component.less'],
})
export class BaseFeatureComponent extends BaseViewComponent {
  async BaseView_OnInit(): Promise<void> {
    var _ = this;
    var componentName = _.constructor.name;

    componentName = componentName.replace('Component', '');

    this.currentUser = this.userAuth.getDecodedToken();

    if (componentName != _.currentUser.MainRouteLink) {
      _.router.navigate([_.currentUser.MainRouteLink]);
    }
  }
}
