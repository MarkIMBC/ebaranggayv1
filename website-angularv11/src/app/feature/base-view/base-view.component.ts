import { Component, ElementRef, isDevMode, OnInit } from '@angular/core';
import {
  Router,
  ActivatedRoute,
  UrlSegment,
  NavigationEnd,
} from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { ValidationService } from 'src/app/core/validation.service';
import { ToastService } from 'src/app/shared/toast.service';

@Component({
  selector: 'app-base-view',
  templateUrl: './base-view.component.html',
  styleUrls: ['./base-view.component.less'],
})
export class BaseViewComponent implements OnInit {
  currentUser: TokenSessionFields = new TokenSessionFields();

  protected _isDevMode: boolean = false;

  constructor(
    protected router: Router,
    protected route: ActivatedRoute,
    protected elRef: ElementRef,
    protected ds: DataService,
    protected toastService: ToastService,
    protected userAuth: UserAuthenticationService,
    protected cs: CrypterService,
    protected validationService: ValidationService,
    protected activeRoute: ActivatedRoute
  ) {
    this._isDevMode = isDevMode();

    this.onContructorInitialize();
  }

  async ngOnInit(): Promise<void> {
    this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        var url = event.url;

        window.localStorage.setItem('LastUrlSegment', url);
      }
    });
    this.currentUser = this.userAuth.getDecodedToken();
    if (
      this.currentUser.ID_User == undefined ||
      this.currentUser.ID_User == null
    ) {
      window.location.href = './Login';
      return;
    }

    await this.BaseView_OnInit();
  }

  async ngAfterViewInit() {
    await this.BaseView_OnAfterViewInit();
  }

  async BaseView_OnInit(): Promise<void> {}
  onContructorInitialize() {}

  BaseView_OnAfterViewInit() {}

  getRoutes() {
    const segments: UrlSegment[] = this.route.snapshot.url;
  }

  customNavigate(routelink: any, config?: any) {
    GeneralfxService.customNavigate(
      this.router,
      this.cs,
      this.currentUser,
      routelink,
      config
    );
  }

  getCustomNavigate(routelink: any, config?: any) {
    return GeneralfxService.getCustomNavigate(
      this.cs,
      this.currentUser,
      routelink,
      config
    );
  }
}
