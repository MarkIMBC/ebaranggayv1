import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { ReportViewerComponent } from 'src/app/shared/report-viewer/report-viewer.component';
import {
  FilterCriteriaType,
  IControlModelArg,
  IFilterFormValue,
  PropertyTypeEnum,
} from 'src/shared/APP_HELPER';
import { APP_REPORTVIEW } from 'src/shared/APP_MODELS';
import { CurrentObjectOnValueChangeArg } from '../base-detail-view/base-detail-view.component';

@Component({
  selector: 'base-app-report',
  templateUrl: './base-report.component.html',
  styleUrls: ['./base-report.component.less'],
})
export class BaseReportComponent implements OnInit {
  @ViewChild('reportviewer') reportviewer: ReportViewerComponent | undefined;
  filterValues: IFilterFormValue[] = [];

  currentUser: TokenSessionFields = new TokenSessionFields();
  ID_Report: string = '';
  configOptions: any = {};
  CurrentObject: any = {};

  private _subscribeActiveRoute: any;

  constructor(
    protected router: Router,
    protected activeRoute: ActivatedRoute,
    protected ds: DataService,
    private userAuth: UserAuthenticationService,
    protected cs: CrypterService
  ) {}

  async ngOnInit(): Promise<void> {
    this.currentUser = this.userAuth.getDecodedToken();

    this.BaseReport_onLoad();
  }

  BaseReport_onLoad() {}

  ngAfterViewInit() {
    this._subscribeActiveRoute = this.activeRoute.params.subscribe(
      async (routeParams) => {
        await this.loadConfigOption();
        await this.loadReportInfo();
        this.load();
      }
    );
  }

  ngOnDestroy(): void {
    this._subscribeActiveRoute.unsubscribe();
  }

  async loadReportInfo() {
    return new Promise<any>(async (resolve, reject) => {
      this.ds
        .execSP(
          'pGetReportByName',
          {
            ReportName: this.configOptions['ReportName'],
            ID_Session: this.currentUser.ID_UserSession,
          },
          { isReturnObject: true }
        )
        .then((obj: any) => {
          this.ID_Report = obj.Oid;

          resolve(obj);
        })
        .catch((obj: any) => {
          reject();
        });
    });
  }

  protected loadConfigOption() {
    var configOptionsString = this.activeRoute.snapshot.params['configOptions'];

    if (configOptionsString == undefined || configOptionsString == null) return;
    configOptionsString = this.cs.decrypt(configOptionsString);

    this.configOptions = JSON.parse(configOptionsString);
  }

  protected getDefaultFilterValues(
    filterValues: IFilterFormValue[]
  ): IFilterFormValue[] {
    filterValues.push({
      dataField: 'ID_Company',
      filterCriteriaType: FilterCriteriaType.Equal,
      propertyType: PropertyTypeEnum.Int,
      value: this.currentUser.ID_Company,
    });

    return filterValues;
  }

  protected getFilterValues(
    filterValues: IFilterFormValue[]
  ): IFilterFormValue[] {
    return filterValues;
  }

  load() {
    if (this.reportviewer == undefined) return;

    var filterValues: IFilterFormValue[] = [];

    this.onLoad();

    if (this.configOptions['filterValues']) {
      filterValues = this.configOptions['filterValues'];
    }

    filterValues = this.getDefaultFilterValues(filterValues);
    filterValues = this.getFilterValues(filterValues);

    this.reportviewer.load(this.ID_Report, filterValues);
  }

  exportPdf() {
    if (this.reportviewer == undefined) return;

    var filterValues: IFilterFormValue[] = [];

    this.onLoad();

    if (this.configOptions['filterValues']) {
      filterValues = this.configOptions['filterValues'];
    }

    filterValues = this.getDefaultFilterValues(filterValues);
    filterValues = this.getFilterValues(filterValues);

    this.reportviewer.exportPDF(this.ID_Report, filterValues);
  }

  exportExcel() {
    if (this.reportviewer == undefined) return;

    var filterValues: IFilterFormValue[] = [];

    this.onLoad();

    if (this.configOptions['filterValues']) {
      filterValues = this.configOptions['filterValues'];
    }

    filterValues = this.getDefaultFilterValues(filterValues);
    filterValues = this.getFilterValues(filterValues);

    this.reportviewer.exportExcel(this.ID_Report, filterValues);
  }

  protected onLoad() {}

  control_onModelChanged(e: IControlModelArg) {
    this.CurrentObject[e.name] = e.value;

    if (e.displayName != undefined) {
      this.CurrentObject[e.displayName] = e.displayValue;
    }

    this.load();
  }

  onbtnSaveClick() {
    this.load();
  }

  onbtnClearClick() {

  }

  onbtnRightClick(event: any) {
    if (event.item.name == 'exporttoexcel') {
      this.exportExcel();
    } else if (event.item.name == 'exporttopdf') {
      this.exportPdf();
    }
  }

  customNavigate(routelink: any, config?: any) {
    GeneralfxService.customNavigate(
      this.router,
      this.cs,
      this.currentUser,
      routelink,
      config
    );
  }

  getCustomNavigate(routelink: any, config?: any) {
    return GeneralfxService.getCustomNavigate(
      this.cs,
      this.currentUser,
      routelink,
      config
    );
  }
}
