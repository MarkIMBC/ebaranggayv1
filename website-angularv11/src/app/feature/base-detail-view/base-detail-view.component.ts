import { BaseViewComponent } from './../base-view/base-view.component';
import { AdminLTEMenuItem } from './../../shared/AdminLTEMenuItem';
import { UserAuthenticationService } from './../../core/UserAuthentication.service';
import { GeneralfxService } from './../../core/generalfx.service';
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/core/data.service';
import { TokenSessionFields } from 'src/app/core/UserAuthentication.service';
import {
  FilingStatusEnum,
  FilterCriteriaType,
  IControlModelArg,
  IDetailValidatingArg,
  IDetailViewAfterSavedArg,
  IFile,
  IFormValidation,
  OccupationalStatusEnum,
  PagingOption,
  PropertyTypeEnum,
} from 'src/shared/APP_HELPER';
import { Model } from 'src/shared/APP_MODELS';
import { ToastService } from 'src/app/shared/toast.service';
import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import { CrypterService } from 'src/app/core/crypter.service';
import { AdminLTEBaseControlComponent } from 'src/app/shared/control/admin-lte-base-control/admin-lte-base-control.component';
import { ValidationService } from 'src/app/core/validation.service';

@Component({
  selector: 'app-base-detail-view',
  templateUrl: './base-detail-view.component.html',
  styleUrls: ['./base-detail-view.component.less'],
})
export class BaseDetailViewComponent extends BaseViewComponent {
  FILINGSTATUS_FILED: number = FilingStatusEnum.Filed;
  FILINGSTATUS_APPROVED: number = FilingStatusEnum.Approved;
  FILINGSTATUS_CANCELLED: number = FilingStatusEnum.Cancelled;
  FILINGSTATUS_CONFINED: number = FilingStatusEnum.Confined;
  FILINGSTATUS_DISCHARGE: number = FilingStatusEnum.Discharged;
  OCCUPATIONALSTATUS_MAYROON: number = OccupationalStatusEnum.Mayroon;

  @ViewChild('reportsqllistdialog') reportsqllistdialog:
    | SQLListDialogComponent
    | undefined;

  @ViewChildren(AdminLTEBaseControlComponent)
  adminLTEControls!: QueryList<AdminLTEBaseControlComponent>;

  @Output() onAfterSaved: EventEmitter<IDetailViewAfterSavedArg> =
    new EventEmitter<IDetailViewAfterSavedArg>();
  @Output() onModelChanged: EventEmitter<IControlModelArg> =
    new EventEmitter<IControlModelArg>();

  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;

  protected _tempID: number = 0;

  ModelName: string = '';
  currentUser: TokenSessionFields = new TokenSessionFields();
  headerTitle: string = '';
  @Input() loading: boolean = true;

  displayMember: string = 'Code';
  routerFeatureName: string = '';

  menuItems: AdminLTEMenuItem[] = [];
  protected __ID_CurrentObject: number = 0;
  model: Model = new Model();

  isDirty: boolean = false;

  CurrentObject: any = {};
  PreviousObject: any = {};
  CurrentFiles: IFile[] = [];

  private _subscribeActiveRoute: any;

  async BaseView_OnInit() {
    this.currentUser = this.userAuth.getDecodedToken();

    this._subscribeActiveRoute = this.activeRoute.params.subscribe(
      async (routeParams) => {
        if (
          this.currentUser.ID_User == undefined ||
          this.currentUser.ID_User == null
        ) {
          window.location.href = './Login';
          return;
        }

        this.loadConfigOption();

        await this.loadCurrentModel();

        this.DetailView_Init();

        this.__ID_CurrentObject = await this._getDefault__ID_CurrentObject();
        await this.loadRecord();
      }
    );
  }

  _menuItem_New: AdminLTEMenuItem = {
    label: 'New',
    icon: 'fa fa-file',
    class: 'text-default',
    visible: true,
  };

  configOptions: any = {};

  ngOnDestroy(): void {
    this._subscribeActiveRoute.unsubscribe();
  }

  private loadConfigOption() {
    var configOptionsString = this.route.snapshot.params['configOptions'];

    if (configOptionsString == undefined || configOptionsString == null) return;

    configOptionsString = this.cs.decrypt(configOptionsString);

    this.configOptions = JSON.parse(configOptionsString);
  }

  async _menuItem_New_onClick() {
    this.__ID_CurrentObject = -1;
    await this.loadRecord();

    var routerFeatureName = this.model.Name;
    if (this.routerFeatureName.length > 0)
      routerFeatureName = this.routerFeatureName;

    var routeLink = [routerFeatureName, this.__ID_CurrentObject];
    this.customNavigate(routeLink, this.configOptions);
  }

  _menuItem_Save: AdminLTEMenuItem = {
    label: 'Save',
    icon: 'fa fa-save',
    class: 'text-success',
    visible: true,
  };

  async _menuItem_Save_onClick() {
    this.CurrentObject_onBeforeSaving();

    var isvalid = true;

    isvalid = await this.validateRecord();
    if (!isvalid) return;

    if (this.loading == true) return;
    this.loading = true;

    this.save()
      .then(async (r) => {
        await this.loadRecord();

        await this.redirectAfterSaved();

        await this._CurrentObject_onAfterSaving();

        this.toastService.success(
          `${this.CurrentObject[this.displayMember]} is saving successful.`
        );
        this.loading = false;
      })
      .catch((reason) => {
        this.toastService.danger(
          `${this.CurrentObject[this.displayMember]} is saving failed.`
        );
        this.loading = false;
      });
  }

  public CurrentObject_onBeforeSaving() {}

  private async _CurrentObject_onAfterSaving() {
    await this.CurrentObject_onAfterSaving();

    this.onAfterSaved.emit({
      CurrentObject: this.CurrentObject,
      Model: this.model,
    });
  }

  public CurrentObject_onAfterSaving() {}

  public async validateRecord() {
    this.loading = true;

    var isValid = true;
    var validationsWarningAppForm: IFormValidation[] = [];
    var validationsAppForm: IFormValidation[] = [];
    var CustomValidations: IFormValidation[] = await this.validation();

    CustomValidations.forEach((customValidation) => {
      if (!customValidation.isWarning) {
        validationsAppForm.push(customValidation);
      } else if (customValidation.isWarning == true) {
        validationsWarningAppForm.push(customValidation);
      }
    });

    if (validationsAppForm.length > 0) {
      this.toastService.danger(validationsAppForm[0].message);
      isValid = false;
    }

    if (validationsWarningAppForm.length > 0) {
      this.toastService.warning(validationsWarningAppForm[0].message);
    }

    this.loading = false;

    return isValid;
  }

  protected async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    return Promise.resolve(validations);
  }

  protected redirectAfterSaved() {
    var routerFeatureName = this.model.Name;
    if (this.routerFeatureName.length > 0)
      routerFeatureName = this.routerFeatureName;

    var routeLink = [routerFeatureName, this.__ID_CurrentObject];
    this.customNavigate(routeLink, this.configOptions);
  }

  _menuItem_Refresh: AdminLTEMenuItem = {
    label: 'Refresh',
    icon: 'fa fa-sync',
    class: 'text-info',
    visible: true,
  };

  _menuItem_Delete: AdminLTEMenuItem = {
    label: 'Delete',
    icon: 'fas fa-trash-alt',
    class: 'text-danger',
    visible: true,
  };

  async _menuItem_Refresh_onClick() {
    await this.loadRecord();
  }

  loadInitMenuItem() {
    this.menuItems.push(this._menuItem_New);
    this.menuItems.push(this._menuItem_Save);

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  addMenuItem(item: AdminLTEMenuItem) {
    this.menuItems.push(item);
  }

  protected async _getDefault__ID_CurrentObject() {
    return parseInt(this.route.snapshot.params['ID_CurrentObject']);
  }

  async loadCurrentModel() {
    this.loading = true;

    var params: any = {};
    params['name'] = this.ModelName;

    var obj = await this.ds.execSP(
      `pGetModelByName`,
      {
        name: this.ModelName,
      },
      {
        isReturnObject: true,
      }
    );

    this.model = obj;

    this.loading = false;
  }

  async loadRecord() {
    this.loading = true;

    var params: any = this.pGetRecordOptions();

    params['ID'] = this.__ID_CurrentObject;
    params['ID_Session'] = this.currentUser.ID_UserSession;

    var obj = await this.ds.execSP(`pGet${this.model.Name}`, params, {
      isReturnObject: true,
    });

    this.authenticateCurrentObjectOwnerShip(obj);

    this.PreviousObject = JSON.parse(JSON.stringify(obj));
    this.CurrentObject = JSON.parse(JSON.stringify(obj));

    this.menuItems = [];

    this.loadInitMenuItem();
    this.loadReportMenuItems();
    this.loadMenuItems();
    this.loadRightDrowDownMenu();

    this.loading = false;
    this.isDirty = false;

    this.DetailView_onLoad();
  }

  public authenticateCurrentObjectOwnerShip(obj: any) {
    if (obj['ID_Company'] != undefined) {
      if (obj.ID_Company != this.currentUser.ID_Company) {
        this.router.navigate(['PageNotFound']);
        return;
      }
    }
  }

  public async LoadCurrentObjectRecord(ID_CurrentObject: number) {
    this.currentUser = this.userAuth.getDecodedToken();
    if (
      this.currentUser.ID_User == undefined ||
      this.currentUser.ID_User == null
    ) {
      window.location.href = './Login';
      return;
    }

    this.__ID_CurrentObject = ID_CurrentObject;

    await this.loadCurrentModel();
    await this.loadRecord();
  }

  loadReportMenuItems() {
    if (this.CurrentObject.IsActive != true) return;
    if (this.CurrentObject.ID <= 0) return;

    if (!this.CurrentObject.Reports) this.CurrentObject.Reports = [];

    var menuReport: AdminLTEMenuItem = {
      label: 'Report',
      name: 'mainreportmenu',
      icon: 'far fa-file',
      class: 'text-primary',
      visible: true,
    };

    if (this.CurrentObject.Reports.length <= 10) {
      menuReport['items'] = [];
      this.CurrentObject.Reports.forEach((report: any) => {
        if (menuReport['items']) {
          menuReport['items'].push(report);
        }
      });
    } else {
      menuReport['name'] = 'mainreportmenudialog';
      menuReport['items'] = undefined;
    }

    if (this.CurrentObject.Reports.length > 0) {
      this.addMenuItem(menuReport);
    }
  }

  protected pGetRecordOptions(): any {
    return {};
  }

  loadMenuItems() {}

  DetailView_Init() {}

  DetailView_AfterInit() {}

  DetailView_onLoad() {}

  async initmenubar_OnClick(e: any) {
    if (e.item.label == 'New') {
      this._menuItem_New_onClick();
    } else if (e.item.label == 'Save') {
      this._menuItem_Save_onClick();
    } else if (e.item.label == 'Refresh') {
      this._menuItem_Refresh_onClick();
    } else if (e.item.label == 'Delete') {
      this.doDelete();
    } else if (e.item.name.includes('ModelReport')) {
      this.customNavigate(['Report'], {
        ReportName: e.item.Name_Report,
        filterValues: [
          {
            dataField: 'ID',
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID,
          },
        ],
      });
    } else if (e.item.name == 'mainreportmenudialog') {
      if (this.reportsqllistdialog == undefined) return;

      var sql = '/*encryptsqlstart*/SELECT * FROM (';

      for (let i = 0; i < this.CurrentObject.Reports.length; i++) {
        let report = this.CurrentObject.Reports[i];

        sql += `SELECT ${i} ID, '${report.label}' label, '${report.Name_Report}' Name_Report \n`;

        if (i !== this.CurrentObject.Reports.length - 1) {
          sql += 'UNION ALL \n';
        }
      }

      sql += ') tbl ORDER BY label /*encryptsqlend*/';

      var obj: any;
      obj = await this.reportsqllistdialog.open({
        sql: sql,
        columns: [
          {
            name: 'label',
            caption: 'Report',
            propertyType: PropertyTypeEnum.String,
          },
        ],
        orderByString: 'label',
      });

      obj.rows.forEach((record: any) => {
        var reportName = record.reportname;

        this.customNavigate(['Report'], {
          ReportName: reportName,
          filterValues: [
            {
              dataField: 'ID',
              filterCriteriaType: FilterCriteriaType.Equal,
              propertyType: PropertyTypeEnum.Int,
              value: this.CurrentObject.ID,
            },
          ],
        });
      });
    }

    this.menubar_OnClick(e);
  }

  menubar_OnClick(e: any) {}

  compute() {}

  deleteDetail(detailName: string, obj: any) {
    var index = GeneralfxService.findIndexByKeyValue(
      this.CurrentObject[detailName],
      'ID',
      obj.ID + ''
    );

    this.CurrentObject.BillingInvoice_Detail.splice(index, 1);

    this.compute();
  }

  async doDelete() {
    if (this.isDirty) {
      this.toastService.warning(
        `Please save first before delete ${
          this.CurrentObject[this.displayMember]
        }.`
      );
      return;
    }

    var isvalid = true;

    isvalid = await this.validateDoDelete();
    if (!isvalid) return;

    //if(confirm(`Would you like to delete ${this.CurrentObject[this.displayMember]}?`) != true) return;

    await this.loadRecord();

    this.CurrentObject.IsActive = 0;
    this.CurrentObject.DateDeleted = new Date();

    this.loading = true;
    await this.save()
      .then(async (r) => {
        await this.loadRecord();

        this.redirectAfterSaved();

        this.toastService.success(
          `${this.CurrentObject[this.displayMember]} is deleted successfully.`
        );
        this.loading = false;
      })
      .catch((reason) => {
        this.toastService.danger(
          `${this.CurrentObject[this.displayMember]} is deleting failed.`
        );
        this.loading = false;
      });
  }

  async validateDoDelete() {
    return true;
  }

  control_onModelChanged(e: IControlModelArg) {
    this.CurrentObject[e.name] = e.value;

    if (e.displayName != undefined) {
      this.CurrentObject[e.displayName] = e.displayValue;
    }

    var arg: CurrentObjectOnValueChangeArg = {
      name: e.name,
      value: e.value,
    };

    this.CurrentObject_onValueChange(arg);

    this.isDirty = true;

    this._control_onModelChanged(e);
  }

  _control_onModelChanged(e: IControlModelArg) {
    this.onModelChanged.emit(e);
  }

  getTempID(): number {
    this._tempID -= 1;
    return this._tempID;
  }

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {}

  getFiles(): IFile[] {
    var currentFiles: IFile[] = [];

    if (this.adminLTEControls) {
      this.adminLTEControls.forEach((i: any) => {
        if (i['file']) {

          var fileImageObject: IFile = {
            file: i['file'],
            dataField: i.name,
            isImage: true,
          };
          currentFiles.push(fileImageObject);
        }
      });
    }

    return currentFiles;
  }

  public async save(): Promise<any> {
    var CurrentObject = this.CurrentObject;
    var PreviousObject = this.PreviousObject;
    var model = this.model;
    var currentUser = this.currentUser;
    var currentFiles: IFile[] = [];

    currentFiles = this.getFiles();

    if (
      CurrentObject['ID_Company'] == null &&
      CurrentObject['ID_Company'] == undefined &&
      CurrentObject.ID < 1
    ) {
      CurrentObject.ID_Company = currentUser.ID_Company;
    }

    var promise = new Promise<any>(async (resolve, reject) => {
      this.ds
        .saveObject(
          model.Oid,
          CurrentObject,
          PreviousObject,
          currentFiles,
          currentUser
        )
        .then((r) => {
          if (r.key != undefined) {
            var id = (r.key + '').replace("'", '');
            var ID_CurrentObject = parseInt(id);
            this.__ID_CurrentObject = ID_CurrentObject;

            resolve(r);
          } else {
            reject('Saving Failed....');
          }
        })
        .catch((reason) => {
          reject(reason);
        });
    });

    return promise;
  }

  rightDropDownItems: AdminLTEMenuItem[] = [];

  loadRightDrowDownMenu() {}

  rightDropDown_onMainButtonClick() {}

  rightDropDown_onMenuItemButtonClick(event: any) {}
}

export class CurrentObjectOnValueChangeArg {
  name: string = '';
  value: any;
}
