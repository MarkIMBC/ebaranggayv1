import { Component, ViewChild } from '@angular/core';
import { Item } from 'src/shared/APP_MODELS';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { BaseListViewComponent } from '../../base-list-view/base-list-view.component';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import { NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-resident-list',
  templateUrl: './resident-list.component.html',
  styleUrls: [
    './../../base-list-view/base-list-view.component.less',
    './resident-list.component.less',
  ],
})
export class ResidentListComponent extends BaseListViewComponent {
  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;

  headerTitle: string = 'Mga Residente';
  customFilter: string = '';
  referenceSourceModel = '';

  CurrentObject: any = {
    Name: '',
    Name_Company: '',
    Name_ItemCategory: '',
    ID_VaccinatedStatus: null,
    Name_VaccinatedStatus: '',
  };

  ID_Vaccinated_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: `
          SELECT 1 ID, 'Nabakunahan' Name
          UNION ALL
          SELECT 2 ID, 'Hindi pa nabakunahan' Name
      `,
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  AssignedBHW_ID_Employee_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM vEmployeeBarangayHealthWorker',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  dataSource: any[] = [];

  isLoadByNavigationEnd: boolean = false;

  async ListView_Onload() {
    var listViewOptionID_AssignedBHW_ID_Employee =
      this.AssignedBHW_ID_Employee_LookupboxOption.listviewOption;

    if (listViewOptionID_AssignedBHW_ID_Employee != undefined) {
      listViewOptionID_AssignedBHW_ID_Employee.sql = `/*encryptsqlstart*/
                                      SELECT
                                            ID,
                                            Name
                                      FROM dbo.vEmployeeBarangayHealthWorker
                                      WHERE
                                            ID_Company = ${this.currentUser.ID_Company}
                                      /*encryptsqlend*/`;
    }

    if (this.configOptions['ModelName']) {
      var modelName = this.configOptions['ModelName'];
      this.referenceSourceModel = modelName;

      if (modelName == 'HouseholdNumber') {
        this.items = [
          {
            label: 'Refresh',
            icon: 'fas fa-sync',
            visible: true,
            command: () => {
              this.loadRecords();
              return true;
            },
          },
          {
            label: 'Add Family',
            icon: 'fas fa-plus  ',
            visible: true,
            command: () => {
              return true;
            },
          },
        ];
      }
    }

    if (this.configOptions['headerTitle']) {
      this.headerTitle = this.configOptions['headerTitle'];
    }

    if (this.configOptions['customFilter']) {
      this.customFilter = this.configOptions['customFilter'];
    }

    this.router.events.subscribe(async (val) => {
      if (val instanceof NavigationEnd) {
        this.isLoadByNavigationEnd = true;
        await this.loadRecords();
      }
    });

    if (!this.isLoadByNavigationEnd) await this.loadRecords();
  }

  async menubar_OnClick(e: any) {
    var menuItem: AdminLTEMenuItem = e.item;

    if (menuItem.label == 'New') {
      this.menuItem_New_onClick();
    } else if (menuItem.label == 'Refresh') {
      this.loadRecords();
    } else if (menuItem.label == 'Add Family') {
      if (this.sqllistdialog == undefined) return;

      var ID_HouseholdNumber = 0;

      if (this.configOptions['ID_HouseholdNumber']) {
        ID_HouseholdNumber = this.configOptions['ID_HouseholdNumber'];
      }

      this.sqllistdialog.caption = 'Select Head of the Family';

      var obj: any;
      obj = await this.sqllistdialog.open({
        sql: `
            /*encryptsqlstart*/
            SELECT
              *
            FROM vResident_Listview_HeadOfFamily
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company} AND
              ISNULL(ID_HouseholdNumber,0) IN (0) AND
              IsActive = 1
              /*encryptsqlend*/
            `,
        columns: [
          {
            name: 'Name',
            caption: 'Name',
            propertyType: PropertyTypeEnum.String,
          },
        ],
        orderByString: 'Name',
      });

      var IDs_Resident: any[] = [];
      obj.rows.forEach((row: any) => {
        IDs_Resident.push(row.ID);
      });

      var ID_HouseholdNumber = 0;

      if (this.configOptions['ID_HouseholdNumber']) {
        ID_HouseholdNumber = this.configOptions['ID_HouseholdNumber'];
      }

      this.changeResidentHouseholdNumber(ID_HouseholdNumber, IDs_Resident);
    }
  }

  async changeResidentHouseholdNumber(
    ID_HouseholdNumber: any,
    IDs_Resident: any[]
  ): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        'pChangeResidentHouseholdNumber',
        {
          ID_HouseholdNumber: ID_HouseholdNumber,
          IDs_Resident: IDs_Resident,
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      this.loadRecords();

      res(obj);
    });
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT
              *
            FROM vBarangayHealthWorker_Resident_ListvIew
            /*encryptsqlend*/
            WHERE
              ${this.customFilter.length > 0 ? this.customFilter + ' AND ' : ''}
              IsActive = 1
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.Name.length > 0) {
      filterString += `Name LIKE '%${this.CurrentObject.Name}%'`;
    }

    if (this.CurrentObject.Name_Company.length > 0) {
      filterString += `Name_Company LIKE '%${this.CurrentObject.Name_Company}%'`;
    }

    return filterString;
  }

  async goToRemoveOnHouseholdNumber(rowData: any) {
    var ID_HouseholdNumber = null;

    await this.changeResidentHouseholdNumber(ID_HouseholdNumber, [rowData.ID]);
    this.loadRecords();
  }

  goToLink(rowData: any) {
    if (this.configOptions['ID_HouseholdNumber']) {
      this.customNavigate(['Resident', rowData.ID], {
        IsHeadOfFamilyPage: true,
      });

      return;
    }

    var config: any[] = this.getCustomNavigate(['Resident', rowData.ID], {});

    const url = this.router.serializeUrl(this.router.createUrlTree(config));

    window.open(url, '_blank');
  }

  Row_OnClick(rowData: Item) {
    if (this.configOptions['ID_HouseholdNumber']) {
      this.customNavigate(['Resident', rowData.ID], {
        IsHeadOfFamilyPage: true,
      });

      return;
    }

    this.customNavigate(['Resident', rowData.ID], {});
  }

  menuItem_New_onClick() {
    this.customNavigate(['Resident', -1], {});
  }
}
