import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BarangayHealthWorkerComponent } from './barangay-health-worker.component';

const routes: Routes = [
  {
    path: '',
    component: BarangayHealthWorkerComponent,
    children: [
      {
        path: 'Dashboard',
        loadChildren: () =>
          import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
      },

      {
        path: 'ResidentList',
        loadChildren: () =>
          import('./resident-list/resident-list.module').then(
            (m) => m.ResidentListModule
          ),
      },

      {
        path: 'ResidentList/:configOptions',
        loadChildren: () =>
          import('./resident-list/resident-list.module').then(
            (m) => m.ResidentListModule
          ),
      },

      {
        path: 'Resident/:ID_CurrentObject/:configOptions',
        loadChildren: () =>
          import('./resident-detail/resident-detail.module').then(
            (m) => m.ResidentDetailModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BarangayHealthWorkerRoutingModule {}
