import { BaseFeatureComponent } from './../base-feature/base-feature.component';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-barangay-health-worker',
  templateUrl: './barangay-health-worker.component.html',
  styleUrls: ['./barangay-health-worker.component.less'],
})
export class BarangayHealthWorkerComponent extends BaseFeatureComponent {}
