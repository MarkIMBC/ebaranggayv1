import { Component, OnInit } from '@angular/core';
import { BaseDetailViewComponent } from '../../base-detail-view/base-detail-view.component';

@Component({
  selector: 'app-base-barangay-health-worker-detail-view',
  templateUrl: './base-barangay-health-worker-detail-view.component.html',
  styleUrls: ['./base-barangay-health-worker-detail-view.component.less'],
})
export class BaseBarangayHealthWorkerDetailViewComponent extends BaseDetailViewComponent {
  public authenticateCurrentObjectOwnerShip(obj: any) {}

  protected redirectAfterSaved() {
    var routerFeatureName = this.model.Name;
    var _Code_Company = this.route.snapshot.params['Code_Company'];

    if (this.routerFeatureName.length > 0)
      routerFeatureName = this.routerFeatureName;

    var routeLink = [_Code_Company, routerFeatureName, this.__ID_CurrentObject];
    this.customNavigate(routeLink, this.configOptions);
  }
}
