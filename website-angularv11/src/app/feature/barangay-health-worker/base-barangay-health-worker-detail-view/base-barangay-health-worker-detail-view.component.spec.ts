import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseBarangayHealthWorkerDetailViewComponent } from './base-barangay-health-worker-detail-view.component';

describe('BaseBarangayHealthWorkerDetailViewComponent', () => {
  let component: BaseBarangayHealthWorkerDetailViewComponent;
  let fixture: ComponentFixture<BaseBarangayHealthWorkerDetailViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BaseBarangayHealthWorkerDetailViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseBarangayHealthWorkerDetailViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
