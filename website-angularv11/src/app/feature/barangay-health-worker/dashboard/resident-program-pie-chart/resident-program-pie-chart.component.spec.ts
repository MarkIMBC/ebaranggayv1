import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidentProgramPieChartComponent } from './resident-program-pie-chart.component';

describe('ResidentProgramPieChartComponent', () => {
  let component: ResidentProgramPieChartComponent;
  let fixture: ComponentFixture<ResidentProgramPieChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResidentProgramPieChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidentProgramPieChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
