import { Component, OnInit } from '@angular/core';
import { DashboardBarangayHealthWorkerSummary } from 'src/shared/APP_HELPER';
import { BaseViewComponent } from '../../base-view/base-view.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less'],
})
export class DashboardComponent extends BaseViewComponent {
  residentSummary: DashboardBarangayHealthWorkerSummary = new DashboardBarangayHealthWorkerSummary();
  async BaseView_OnInit() {
    await this.loadRecords();
  }

  async loadRecords() {
    var obj = await this.ds.execSP(
      'pGetDashboardBarangayHealthWorkerSummary',
      {
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
      }
    );

    this.residentSummary = obj;
  }
}
