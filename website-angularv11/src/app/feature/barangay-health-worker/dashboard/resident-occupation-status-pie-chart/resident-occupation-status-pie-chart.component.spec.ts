import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidentOccupationStatusPieChartComponent } from './resident-occupation-status-pie-chart.component';

describe('ResidentOccupationStatusPieChartComponent', () => {
  let component: ResidentOccupationStatusPieChartComponent;
  let fixture: ComponentFixture<ResidentOccupationStatusPieChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResidentOccupationStatusPieChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidentOccupationStatusPieChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
