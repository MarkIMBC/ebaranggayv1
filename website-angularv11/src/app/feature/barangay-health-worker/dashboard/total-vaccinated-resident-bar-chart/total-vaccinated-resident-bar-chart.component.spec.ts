import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalVaccinatedResidentBarChartComponent } from './total-vaccinated-resident-bar-chart.component';

describe('TotalVaccinatedResidentBarChartComponent', () => {
  let component: TotalVaccinatedResidentBarChartComponent;
  let fixture: ComponentFixture<TotalVaccinatedResidentBarChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TotalVaccinatedResidentBarChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalVaccinatedResidentBarChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
