import { LayoutModule } from './../../layout/layout.module';
import {
  APP_INITIALIZER,
  CUSTOM_ELEMENTS_SCHEMA,
  NgModule,
  NO_ERRORS_SCHEMA,
} from '@angular/core';
import { CommonModule } from '@angular/common';

import { BarangayHealthWorkerRoutingModule } from './barangay-health-worker-routing.module';
import { BarangayHealthWorkerComponent } from './barangay-health-worker.component';

import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { appInit } from 'src/app/app.module';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { LoaderInterceptor } from 'src/app/core/LoaderInterceptor';
import { BaseBarangayHealthWorkerDetailViewComponent } from './base-barangay-health-worker-detail-view/base-barangay-health-worker-detail-view.component';

@NgModule({
  declarations: [BarangayHealthWorkerComponent, BaseBarangayHealthWorkerDetailViewComponent],
  imports: [
    CommonModule,
    LayoutModule,
    ModalModule,
    SharedModule,
    BarangayHealthWorkerRoutingModule,
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: appInit,
      multi: true,
      deps: [DataService],
    },
    CrypterService,
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class BarangayHealthWorkerModule {}

