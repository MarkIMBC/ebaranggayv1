
import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/core/UserAuthentication.service';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { Resident_Program_DTO, PropertyTypeEnum, IControlModelArg, Resident_Family_DTO } from 'src/shared/APP_HELPER';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';

@Component({
  selector: 'resident-family-table',
  templateUrl: './resident-family-table.component.html',
  styleUrls: ['./resident-family-table.component.less']
})
export class ResidentFamilyTableComponent implements OnInit {

  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;

  @Input() FamilyMembers: Resident_Family_DTO[] = []

  tempID: number = 0;


  currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(private userAuth: UserAuthenticationService) { }

  ngOnInit(): void {

    this.currentUser = this.userAuth.getDecodedToken();
  }

  menuItems: AdminLTEMenuItem[] = [
    {
      label: 'Mag Dagdag ng Pamilya',
      icon: 'fa fa-plus',
      visible: true
    }
  ];

  async doAddProgram() {

    if (this.sqllistdialog == undefined) return;

    var strIDs_currentIDPrograms = '';
    var IDs_Program: any[] = [];

    this.FamilyMembers.forEach(FamilyMember => IDs_Program.push(FamilyMember.ID_FamilyMember));
    strIDs_currentIDPrograms = IDs_Program.length > 0 ?  IDs_Program.join(",") : '0';

    

    var obj: any;
    obj = await this.sqllistdialog.open({
      sql: `/*encryptsqlstart*/
            SELECT
              ID, Name
            FROM dbo.vResident
            /*encryptsqlend*/
            WHERE
                
              IsActive = 1 AND ID_Company =${this.currentUser.ID_Company}`,
      columns: [
        {
          name: 'Name',
          caption: 'Resident',
          propertyType: PropertyTypeEnum.String
        },
      ]
    });

    this.tempID--;

    obj.rows.forEach((record: any) => {
      var item = {
        ID: this.tempID,
        ID_FamilyMember: record.ID,
        Name_FamilyMember: record.Name,
      };

      if (this.FamilyMembers == null) this.FamilyMembers = [];
      this.FamilyMembers.push(item);
    });

  }

  async menuItems_OnClick(arg: any) {

    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.label == 'Mag Dagdag ng Pamilya') {

      this.doAddProgram();
   
    }
  }

  async BtnDeleteSchedule_OnClick(ScheduleList: any) {

    var index = GeneralfxService.findIndexByKeyValue(
      this.FamilyMembers,
      "ID",
      ScheduleList.ID + ""
    );

    this.FamilyMembers.splice(index, 1);
  }


  FamilyMember_onModelChanged(ScheduleList: any, e: IControlModelArg) {

    ScheduleList[e.name] = e.value;
  }



  ID_Relationship_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM tFamilyRelationshipType',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };


}




