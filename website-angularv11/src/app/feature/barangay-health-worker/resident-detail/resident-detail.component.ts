import { Component, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import {
  FilterCriteriaType,
  IFormValidation,
  PropertyTypeEnum,
} from 'src/shared/APP_HELPER';
import { BaseBarangayHealthWorkerDetailViewComponent } from '../base-barangay-health-worker-detail-view/base-barangay-health-worker-detail-view.component';

@Component({
  selector: 'app-resident-detail',
  templateUrl: './resident-detail.component.html',
  styleUrls: [
    './../../base-detail-view/base-detail-view.component.less',
    './resident-detail.component.less',
  ],
})
export class ResidentDetailComponent extends BaseBarangayHealthWorkerDetailViewComponent {
  ModelName: string = 'Resident';
  headerTitle: string = 'Residente';

  displayMember: string = 'Name';

  ID_Gender_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM tGender',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  ID_Religion_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM tReligion',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  ID_HouseholdNumber_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM tHouseholdNumber',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  ID_CivilStatus_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM tCivilStatus',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  ID_EducationalLevel_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM tEducationalLevel',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  OccupationalStatus_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM tOccupationalStatus',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  AssignedBHW_ID_Employee_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM vEmployeeBarangayHealthWorker',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  ID_HomeOwnershipStatus_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM tHomeOwnershipStatus',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  loadInitMenuItem() {
    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  loadReportMenuItems() {
    if (this.CurrentObject.IsActive != true) return;
    if (this.CurrentObject.ID <= 0) return;

    if (!this.CurrentObject.Reports) this.CurrentObject.Reports = [];

    var menuReport: AdminLTEMenuItem = {
      label: 'Report',
      name: 'mainreportmenu',
      icon: 'far fa-file',
      class: 'text-primary',
      visible: true,
      items: [],
    };

    this.CurrentObject.Reports.forEach((report: any) => {
      if (menuReport['items']) {
        menuReport['items'].push(report);
      }
    });

    if (this.CurrentObject.Reports.length > 0) {
      this.addMenuItem(menuReport);
    }
  }

  async menubar_OnClick(e: any) {
    if (e.item.name == 'mainreportmenu') {
      if (this.reportsqllistdialog == undefined) return;

      var obj: any;
      obj = await this.reportsqllistdialog.open({
        sql: `/*encryptsqlstart*/
              SELECT
                *
              FROM dbo.vResidentDetailReportList
              /*encryptsqlend*/`,
        columns: [
          {
            name: 'label',
            caption: 'Report',
            propertyType: PropertyTypeEnum.String,
          },
        ],
        orderByString: 'label',
      });

      obj.rows.forEach((record: any) => {
        var reportName = record.reportname;

        this.customNavigate(['Report'], {
          ReportName: reportName,
          filterValues: [
            {
              dataField: 'ID',
              filterCriteriaType: FilterCriteriaType.Equal,
              propertyType: PropertyTypeEnum.Int,
              value: this.CurrentObject.ID,
            },
          ],
        });
      });
    }
  }

  IsHeadOfFamilyPage: boolean = false;

  DetailView_onLoad() {
    var listViewOptionID_AssignedBHW_ID_Employee =
      this.AssignedBHW_ID_Employee_LookupboxOption.listviewOption;

    if (listViewOptionID_AssignedBHW_ID_Employee != undefined) {
      listViewOptionID_AssignedBHW_ID_Employee.sql = `/*encryptsqlstart*/
                                      SELECT
                                            ID,
                                            Name
                                      FROM dbo.vEmployeeBarangayHealthWorker
                                      WHERE
                                            ID_Company = ${this.currentUser.ID_Company}
                                      /*encryptsqlend*/`;
    }

    if (this.configOptions['IsHeadOfFamilyPage']) {
      this.IsHeadOfFamilyPage = this.configOptions['IsHeadOfFamilyPage'];
    }

    if (this.CurrentObject.Resident_Program == undefined)
      this.CurrentObject.Resident_Program = [];

    if (this.CurrentObject.Resident_Family == undefined)
      this.CurrentObject.Resident_Family = [];

    if (this.CurrentObject.Resident_MedicalRecord == undefined)
      this.CurrentObject.Resident_MedicalRecord = [];
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    if (this.CurrentObject.Name == null) {
      this.CurrentObject.Name = '';
    }

    if (this.CurrentObject.Name.length == 0) {
      validations.push({
        message: 'Name is required.',
      });
    }

    if (validations.length == 0) {
      validations = await this.validateBackend();
    }

    return Promise.resolve(validations);
  }

  async validateBackend(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    var validateObj = await this.ds.execSP(
      'pResident_Validation',
      {
        ID_Resident: this.CurrentObject.ID,
        Name: this.CurrentObject.Name,
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
      }
    );

    if (validateObj.isValid != true) {
      validations.push({
        message: validateObj.message,
        isWarning: validateObj.isWarning,
      });
    }

    return validations;
  }

  public CurrentObject_onBeforeSaving() {
    if (this.CurrentObject.IsMigrante !== true) {
      this.CurrentObject.MigrantAddress = '';
    }

    if (
      this.CurrentObject.ID_OccupationalStatus !==
      this.OCCUPATIONALSTATUS_MAYROON
    ) {
      this.CurrentObject.Occupation = '';
    }

    if (this.CurrentObject.IsVaccinated !== true) {
      this.CurrentObject.DateLastVaccination = null;
    }
  }

  CommunityTaxCertificate_onClick() {
    if (this.CurrentObject.ID < 1) return;
    this.customNavigate(['ResidentRequest_CommunityTaxCertificate', -1], {
      ID_Resident: this.CurrentObject.ID,
    });
  }

  IndigencyCertificate_onClick() {
    if (this.CurrentObject.ID < 1) return;
    this.customNavigate(['ResidentRequest_IndigencyCertificate', -1], {
      ID_Resident: this.CurrentObject.ID,
    });
  }

  BarangayBusinessClearance_onClick() {
    if (this.CurrentObject.ID < 1) return;
    this.customNavigate(['ResidentRequest_BarangayBusinessClearance', -1], {
      ID_Resident: this.CurrentObject.ID,
    });
  }

  Certificate_onClick() {
    if (this.CurrentObject.ID < 1) return;
    this.customNavigate(['ResidentRequest_Certificate', -1], {
      ID_Resident: this.CurrentObject.ID,
    });
  }
}
