import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidentMedicalRecordTableComponent } from './resident-medical-record-table.component';

describe('ResidentMedicalRecordTableComponent', () => {
  let component: ResidentMedicalRecordTableComponent;
  let fixture: ComponentFixture<ResidentMedicalRecordTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResidentMedicalRecordTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidentMedicalRecordTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
