import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidentMedicalRecordPregnancyDiagnosisTableComponent } from './resident-medical-record-pregnancy-diagnosis-table.component';

describe('ResidentMedicalRecordPregnancyDiagnosisTableComponent', () => {
  let component: ResidentMedicalRecordPregnancyDiagnosisTableComponent;
  let fixture: ComponentFixture<ResidentMedicalRecordPregnancyDiagnosisTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResidentMedicalRecordPregnancyDiagnosisTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidentMedicalRecordPregnancyDiagnosisTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
