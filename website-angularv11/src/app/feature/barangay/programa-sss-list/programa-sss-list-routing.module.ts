import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProgramaSSSListComponent } from './programa-sss-list.component';

const routes: Routes = [{ path: '', component: ProgramaSSSListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgramaSSSListRoutingModule { }
