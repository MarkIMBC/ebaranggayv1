import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramaSSSListComponent } from './programa-sss-list.component';

describe('ProgramaSSSListComponent', () => {
  let component: ProgramaSSSListComponent;
  let fixture: ComponentFixture<ProgramaSSSListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProgramaSSSListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramaSSSListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
