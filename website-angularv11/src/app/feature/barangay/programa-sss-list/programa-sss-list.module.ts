import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProgramaSSSListRoutingModule } from './programa-sss-list-routing.module';
import { ProgramaSSSListComponent } from './programa-sss-list.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [ProgramaSSSListComponent],
  imports: [CommonModule, SharedModule, ProgramaSSSListRoutingModule],
})
export class ProgramaSSSListModule {}
