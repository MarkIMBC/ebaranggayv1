import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportResidentDisasterRiskComponent } from './report-resident-disaster-risk.component';

describe('ReportResidentDisasterRiskComponent', () => {
  let component: ReportResidentDisasterRiskComponent;
  let fixture: ComponentFixture<ReportResidentDisasterRiskComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportResidentDisasterRiskComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportResidentDisasterRiskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
