import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportResidentDisasterRiskComponent } from './report-resident-disaster-risk.component';

const routes: Routes = [{ path: '', component: ReportResidentDisasterRiskComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportResidentDisasterRiskRoutingModule { }
