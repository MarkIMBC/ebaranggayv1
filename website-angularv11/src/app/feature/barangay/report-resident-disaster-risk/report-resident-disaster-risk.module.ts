import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from './../../../shared/shared.module';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { ReportResidentDisasterRiskRoutingModule } from './report-resident-disaster-risk-routing.module';
import { ReportResidentDisasterRiskComponent } from './report-resident-disaster-risk.component';
@NgModule({
  declarations: [ReportResidentDisasterRiskComponent],
  imports: [
    CommonModule,
    SharedModule,
    NgxSliderModule,
    ReportResidentDisasterRiskRoutingModule,
  ],
})
export class ReportResidentDisasterRiskModule {}
