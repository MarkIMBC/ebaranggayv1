import { Component } from '@angular/core';
import * as moment from 'moment';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import {
  FilterCriteriaType,
  IFilterFormValue,
  PropertyTypeEnum,
} from 'src/shared/APP_HELPER';
import { ReportComponent } from '../report/report.component';
import { Options } from '@angular-slider/ngx-slider';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
@Component({
  selector: 'app-report-resident-disaster-risk',
  templateUrl: './report-resident-disaster-risk.component.html',
  styleUrls: ['./report-resident-disaster-risk.component.less'],
})
export class ReportResidentDisasterRiskComponent extends ReportComponent {
  AssignedBHW_ID_Employee_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM vEmployeeBarangayHealthWorker',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  options: Options = {
    floor: 0,
    ceil: 120,
    selectionBarGradient: {
      from: 'white',
      to: 'teal',
    },
    getPointerColor: (value: number): string => {
      return 'teal';
    },
  };

  ID_Disaster_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM tDisaster',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  CurrentObject: any = {
    AgeStart: 0,
    AgeEnd: 120,
    ID_Disaster: null,
    Name_Disaster: '',
  };

  configOptions: any = {
    ReportName: 'ResidentDisasterRiskReport',
  };

  protected getFilterValues(
    filterValues: IFilterFormValue[]
  ): IFilterFormValue[] {
    var caption = '';

    if (this.CurrentObject['Name']) {
      filterValues.push({
        dataField: 'Name',
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: this.CurrentObject['Name'],
      });
    }

    if (this.CurrentObject['Name_HouseholdNumber']) {
      filterValues.push({
        dataField: 'Name_HouseholdNumber',
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: this.CurrentObject['Name_HouseholdNumber'],
      });
    }

    if (this.CurrentObject['IsPermanent']) {
      filterValues.push({
        dataField: 'IsPermanent',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Int,
        value: 1,
      });
    }

    if (this.CurrentObject['IsMigrante']) {
      filterValues.push({
        dataField: 'IsMigrante',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Int,
        value: 1,
      });
    }

    if (this.CurrentObject['IsTransient']) {
      filterValues.push({
        dataField: 'IsTransient',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Int,
        value: 1,
      });
    }

    if (this.CurrentObject['ID_Disaster']) {
      filterValues.push({
        dataField: 'ID_Disaster',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Int,
        value: this.CurrentObject['ID_Disaster'],
      });
    }

    if (this.CurrentObject.AssignedBHW_ID_Employee != null) {
      filterValues.push({
        dataField: 'AssignedBHW_ID_Employee',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Int,
        value: [this.CurrentObject['AssignedBHW_ID_Employee']],
      });
    }

    /*Caption*/
    var index = -1;
    index = GeneralfxService.findIndexByKeyValue(
      filterValues,
      'dataField',
      'ID_Disaster'
    );
    if (index > -1) {
      var formFilter = filterValues[index];
      var dateStart = new Date(formFilter.value[0]);
      var dateEnd = new Date(formFilter.value[1]);

      caption += `Disaster Risk "${this.CurrentObject.Name_Disaster}"<br/>`;
    }

    if (caption.length > 0) {
      filterValues.push({
        dataField: 'Header_CustomCaption',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.String,
        value: caption,
      });
    }

    return filterValues;
  }

  onbtnClearClick() {
    this.CurrentObject = {
      AgeStart: 0,
      AgeEnd: 120,
    };
  }

  BaseReport_onLoad() {
    var listViewOptionID_AssignedBHW_ID_Employee =
      this.AssignedBHW_ID_Employee_LookupboxOption.listviewOption;

    if (listViewOptionID_AssignedBHW_ID_Employee != undefined) {
      listViewOptionID_AssignedBHW_ID_Employee.sql = `/*encryptsqlstart*/
                                    SELECT
                                          ID,
                                          Name
                                    FROM dbo.vEmployeeBarangayHealthWorker
                                    WHERE
                                          ID_Company = ${this.currentUser.ID_Company}
                                    /*encryptsqlend*/`;
    }
  }
}
