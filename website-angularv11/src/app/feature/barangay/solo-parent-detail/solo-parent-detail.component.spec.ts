import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SoloParentDetailComponent } from './solo-parent-detail.component';

describe('SoloParentDetailComponent', () => {
  let component: SoloParentDetailComponent;
  let fixture: ComponentFixture<SoloParentDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SoloParentDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SoloParentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
