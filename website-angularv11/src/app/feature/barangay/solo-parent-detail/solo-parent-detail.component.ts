import { IFormValidation, PropertyTypeEnum } from 'src/shared/APP_HELPER';
import {
  BaseDetailViewComponent,
  CurrentObjectOnValueChangeArg,
} from '../../base-detail-view/base-detail-view.component';
import { Component } from '@angular/core';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';

@Component({
  selector: 'app-solo-parent-detail',
  templateUrl: './solo-parent-detail.component.html',
  styleUrls: [
    './../../base-detail-view/base-detail-view.component.less',
    './solo-parent-detail.component.less',
  ],
})
export class SoloParentDetailComponent extends BaseDetailViewComponent {
  ModelName: string = 'SoloParent';
  headerTitle: string = 'Solo Parent';

  ID_Relationship_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM tFamilyRelationshipType',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  DetailView_onLoad() {}

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {}

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    var Name = this.CurrentObject.Name;
    var Resident_BusinessOwner = this.CurrentObject.Resident_BusinessOwner;
    var RequestedBy = this.CurrentObject.RequestedBy;

    if (Name == undefined || Name == null) Name = '';

    return Promise.resolve(validations);
  }
}
