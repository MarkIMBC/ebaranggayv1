import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SoloParentDetailRoutingModule } from './solo-parent-detail-routing.module';
import { SoloParentDetailComponent } from './solo-parent-detail.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [SoloParentDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    SoloParentDetailRoutingModule,
  ],
})
export class SoloParentDetailModule {}
