import { Component, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import {
  FilterCriteriaType,
  LawSuitStatusEnum,
  PropertyTypeEnum,
} from 'src/shared/APP_HELPER';
import { APP_REPORTVIEW } from 'src/shared/APP_MODELS';
import { BaseDetailViewComponent } from '../../base-detail-view/base-detail-view.component';

@Component({
  selector: 'app-law-suit-detail',
  templateUrl: './law-suit-detail.component.html',
  styleUrls: [
    './../../base-detail-view/base-detail-view.component.less',
    './law-suit-detail.component.less',
  ],
})
export class LawSuitDetailComponent extends BaseDetailViewComponent {
  ModelName: string = 'LawSuit';
  headerTitle: string = 'Habla';

  displayMember: string = 'Code';

  ID_LawSuitStatus_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM tLawSuitStatus',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  Complainant_ID_Resident_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: '',
      columns: [
        {
          name: 'Name',
          caption: 'Pangalan',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'Name_Gender',
          caption: 'Kasarlan',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  Respondent_ID_Resident_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: '',
      columns: [
        {
          name: 'Name',
          caption: 'Pangalan',
          propertyType: PropertyTypeEnum.String,
        },
        {
          name: 'Name_Gender',
          caption: 'Kasarlan',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  loadInitMenuItem() {
    this.menuItems.push(this._menuItem_New);

    if (this.CurrentObject.IsActive != true) return;

    this.menuItems.push(this._menuItem_Save);

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
      if (this.CurrentObject.ID_LawSuitStatus == LawSuitStatusEnum.Pending) {
        this.menuItems.push(this._menuItem_Delete);
      }
    }
  }

  loadMenuItems() {
    if (this.CurrentObject.IsActive != true) return;

    if (this.CurrentObject.ID < 1) return;

    this.loadMenuItems_smsreminder();

    var menuReport: AdminLTEMenuItem = {
      label: 'Report',
      icon: 'far fa-file',
      class: 'text-primary',
      visible: true,
      items: [],
    };

    var barangayComplaintMenuItem = {
      label: 'Brgy. Complaint Report',
      name: APP_REPORTVIEW.BARANGAYCOMPLAINTREPORT,
      icon: 'far fa-file',
      class: 'text-primary',
      visible: true,
    };

    if (menuReport.items) {
      menuReport.items.push(barangayComplaintMenuItem);
    }

    this.menuItems.push(menuReport);
  }

  loadMenuItems_smsreminder() {
    var sendSMSMenuItem: AdminLTEMenuItem = {
      label: 'SMS Reminder',
      icon: 'fas fa-sms',
      class: 'text-primary',
      visible: true,
    };

    sendSMSMenuItem.items = [];

    if (
      this.CurrentObject.ComplainantSMSNumber != null &&
      this.CurrentObject.ComplainantSMSNumber.length > 0
    ) {
      sendSMSMenuItem.items.push({
        label: 'Magpadala sa Complainant',
        name: 'ComplainantSendSMS',
        icon: 'far fa-file',
        class: 'text-primary',
        visible: true,
      });
    }

    if (
      this.CurrentObject.RespondentSMSNumber != null &&
      this.CurrentObject.RespondentSMSNumber.length > 0
    ) {
      sendSMSMenuItem.items.push({
        label: 'Magpadala sa Respondent',
        name: 'RespondentSendSMS',
        icon: 'far fa-file',
        class: 'text-primary',
        visible: true,
      });
    }

    if (sendSMSMenuItem.items.length > 0) {
      this.menuItems.push(sendSMSMenuItem);
    }
  }

  async menubar_OnClick(e: any) {
    if (e.item.name == 'ComplainantSendSMS') {
      if (this.isDirty) {
        this.toastService.warning(
          `Please save first before approving ${this.CurrentObject.Code}.`
        );
        return;
      }
    } else if (e.item.name == 'RespondentSendSMS') {
      if (this.isDirty) {
        this.toastService.warning(
          `Please save first before approving ${this.CurrentObject.Code}.`
        );
        return;
      }
    } else if (e.item.name == APP_REPORTVIEW.BARANGAYCOMPLAINTREPORT) {
      this.customNavigate(['Report'], {
        ReportName: 'BARANGAYCOMPLAINTREPORT',
        filterValues: [
          {
            dataField: 'ID',
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID,
          },
        ],
      });
    }
  }

  DetailView_onLoad() {
    var _Complainant_ID_Resident_LookupboxOption =
      this.Complainant_ID_Resident_LookupboxOption.listviewOption;

    if (_Complainant_ID_Resident_LookupboxOption != undefined) {
      _Complainant_ID_Resident_LookupboxOption.sql = `/*encryptsqlstart*/
                                    SELECT
                                          ID,
                                          Name
                                    FROM dbo.vResident_Listview
                                    WHERE
                                          ID_Company = ${this.currentUser.ID_Company}
                                    /*encryptsqlend*/`;
    }

    var _Respondent_ID_Resident_LookupboxOption =
      this.Respondent_ID_Resident_LookupboxOption.listviewOption;

    if (_Respondent_ID_Resident_LookupboxOption != undefined) {
      _Respondent_ID_Resident_LookupboxOption.sql = `/*encryptsqlstart*/
                                    SELECT
                                          ID,
                                          Name
                                    FROM dbo.vResident_Listview
                                    WHERE
                                          ID_Company = ${this.currentUser.ID_Company}`;
    }
  }

  async CurrentObject_onValueChange(e: any) {
    if (e.name == 'Complainant_ID_Resident') {
      if (e.value == null) {
        this.CurrentObject.ComplainantName =
          this.CurrentObject._ComplainantName;

        this.CurrentObject.Complainant_Name_Resident =
          this.CurrentObject._ComplainantName;
      } else {
        this.CurrentObject.ComplainantName =
          this.CurrentObject.Complainant_Name_Resident;
      }
    } else if (e.name == 'Respondent_ID_Resident') {
      if (e.value == null) {
        this.CurrentObject.RespondentName = this.CurrentObject._RespondentName;

        this.CurrentObject.Respondent_Name_Resident =
          this.CurrentObject._RespondentName;
      } else {
        this.CurrentObject.RespondentName =
          this.CurrentObject.Respondent_Name_Resident;
      }
    }
  }
  protected pGetRecordOptions(): any {
    var options: any = {};
    var configKeys = ['Complainant_ID_Resident', 'Respondent_ID_Resident'];

    configKeys.forEach((key) => {
      if (
        this.configOptions[key] != undefined &&
        this.configOptions[key] != null
      ) {
        options[key] = this.configOptions[key];
      }
    });

    return options;
  }
}
