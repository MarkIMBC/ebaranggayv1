import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LawSuitDetailComponent } from './law-suit-detail.component';

const routes: Routes = [{ path: '', component: LawSuitDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LawSuitDetailRoutingModule { }
