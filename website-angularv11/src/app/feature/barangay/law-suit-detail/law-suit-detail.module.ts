import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LawSuitDetailRoutingModule } from './law-suit-detail-routing.module';
import { LawSuitDetailComponent } from './law-suit-detail.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [LawSuitDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    LawSuitDetailRoutingModule
  ]
})
export class LawSuitDetailModule { }
