import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LawSuitDetailComponent } from './law-suit-detail.component';

describe('LawSuitDetailComponent', () => {
  let component: LawSuitDetailComponent;
  let fixture: ComponentFixture<LawSuitDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LawSuitDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LawSuitDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
