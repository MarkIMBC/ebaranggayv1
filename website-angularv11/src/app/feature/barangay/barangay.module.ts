import { LayoutModule } from './../../layout/layout.module';
import {
  APP_INITIALIZER,
  CUSTOM_ELEMENTS_SCHEMA,
  NgModule,
  NO_ERRORS_SCHEMA,
} from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { BarangayRoutingModule } from './barangay-routing.module';
import { BarangayComponent } from './barangay.component';
import { InitialPageComponent } from './initial-page/initial-page.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { appInit } from 'src/app/app.module';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { LoaderInterceptor } from 'src/app/core/LoaderInterceptor';

@NgModule({
  declarations: [BarangayComponent, InitialPageComponent],
  imports: [
    CommonModule,
    LayoutModule,
    ModalModule,
    SharedModule,
    BarangayRoutingModule,
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: appInit,
      multi: true,
      deps: [DataService],
    },
    CrypterService,
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class BarangayModule {}
