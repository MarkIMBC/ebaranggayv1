import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DisputeDetailRoutingModule } from './dispute-detail-routing.module';
import { DisputeDetailComponent } from './dispute-detail.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [DisputeDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    DisputeDetailRoutingModule
  ]
})
export class DisputeDetailModule { }
