import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DisputeDetailComponent } from './dispute-detail.component';

const routes: Routes = [{ path: '', component: DisputeDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DisputeDetailRoutingModule { }
