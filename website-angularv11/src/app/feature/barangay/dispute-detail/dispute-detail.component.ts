import { ItemTypeEnum } from '../../../../shared/APP_HELPER';
import { Component, ViewChild } from '@angular/core';
import { BaseDetailViewComponent } from '../../base-detail-view/base-detail-view.component';

@Component({
  selector: 'app-dispute-detail',
  templateUrl: './dispute-detail.component.html',
  styleUrls: [
    './../../base-detail-view/base-detail-view.component.less',
    './dispute-detail.component.less'
  ]
})
export class DisputeDetailComponent extends BaseDetailViewComponent {

  ModelName: string = 'Dispute'
  headerTitle: string = 'Hindi Pagkakaunawaan'

  displayMember: string = "Name";

  loadInitMenuItem() {

    this.menuItems.push(this._menuItem_New);

    if(this.CurrentObject.IsActive != true) return;

    this.menuItems.push(this._menuItem_Save);

    if (this.CurrentObject.ID > 0) {

      this.menuItems.push(this._menuItem_Refresh);
      this.menuItems.push(this._menuItem_Delete);
    }
  }

  DetailView_onLoad() {

    this.CurrentObject.ID_ItemType = ItemTypeEnum.Service;
  }

}
