import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProgramaPantawidPamilyaListComponent } from './programa-pantawid-pamilya-list.component';

const routes: Routes = [{ path: '', component: ProgramaPantawidPamilyaListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgramaPantawidPamilyaListRoutingModule { }
