import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProgramaPantawidPamilyaListRoutingModule } from './programa-pantawid-pamilya-list-routing.module';
import { ProgramaPantawidPamilyaListComponent } from './programa-pantawid-pamilya-list.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ProgramaPantawidPamilyaListComponent],
  imports: [
    CommonModule,
    ProgramaPantawidPamilyaListRoutingModule,
    SharedModule
  ]
})
export class ProgramaPantawidPamilyaListModule { }



