import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramaPantawidPamilyaListComponent } from './programa-pantawid-pamilya-list.component';

describe('ProgramaPantawidPamilyaListComponent', () => {
  let component: ProgramaPantawidPamilyaListComponent;
  let fixture: ComponentFixture<ProgramaPantawidPamilyaListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProgramaPantawidPamilyaListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramaPantawidPamilyaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
