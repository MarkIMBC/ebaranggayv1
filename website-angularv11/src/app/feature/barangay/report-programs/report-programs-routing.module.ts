import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportProgramsComponent } from './report-programs.component';

const routes: Routes = [{ path: '', component: ReportProgramsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportProgramsRoutingModule { }
