import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportProgramsRoutingModule } from './report-programs-routing.module';
import { ReportProgramsComponent } from './report-programs.component';
import { SharedModule } from './../../../shared/shared.module';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
@NgModule({
  declarations: [ReportProgramsComponent],
  imports: [
    CommonModule,
    ReportProgramsRoutingModule,
    NgxSliderModule,
    SharedModule
  ]
})
export class ReportProgramsModule { }
