import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResidentListBoarderRoutingModule } from './resident-list-boarder-routing.module';
import { ResidentListBoarderComponent } from './resident-list-boarder.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [ResidentListBoarderComponent],
  imports: [
    CommonModule,
    ResidentListBoarderRoutingModule,
    SharedModule

  ]
})
export class ResidentListBoarderModule { }
