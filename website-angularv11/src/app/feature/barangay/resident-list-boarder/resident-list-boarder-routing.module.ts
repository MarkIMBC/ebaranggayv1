import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResidentListBoarderComponent } from './resident-list-boarder.component';

const routes: Routes = [{ path: '', component: ResidentListBoarderComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResidentListBoarderRoutingModule { }
