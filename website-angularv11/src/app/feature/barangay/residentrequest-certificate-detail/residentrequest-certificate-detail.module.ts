import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResidentRequestCertificateDetailRoutingModule } from './residentrequest-certificate-detail-routing.module';
import { ResidentRequestCertificateDetailComponent } from './residentrequest-certificate-detail.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ResidentRequestCertificateDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    ResidentRequestCertificateDetailRoutingModule
  ]
})
export class ResidentRequestCertificateDetailModule { }
