import { ResidentRequest_Certificate } from './../../../../shared/APP_MODELS';
import { Component } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import {
  PropertyTypeEnum,
  FilterCriteriaType,
  ItemTypeEnum,
  IControlModelArg,
} from 'src/shared/APP_HELPER';
import { APP_REPORTVIEW } from 'src/shared/APP_MODELS';
import { BaseDetailViewComponent } from '../../base-detail-view/base-detail-view.component';

@Component({
  selector: 'residentrequest-certificate-detail',
  templateUrl: './residentrequest-certificate-detail.component.html',
  styleUrls: [
    './../../base-detail-view/base-detail-view.component.less',
    './residentrequest-certificate-detail.component.less',
  ],
})
export class ResidentRequestCertificateDetailComponent extends BaseDetailViewComponent {
  ModelName: string = 'ResidentRequest';
  headerTitle: string = 'Certificate';
  displayMember: string = 'Name';
  routerFeatureName: string = 'ResidentRequest_Certificate';

  currentResidentRequest_Certificate: any = new ResidentRequest_Certificate();

  ID_Gender_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM tGender',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  loadInitMenuItem() {
    if (this.CurrentObject.IsActive == true) {
      this.menuItems.push(this._menuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  loadMenuItems() {
    var menuReport: AdminLTEMenuItem = {
      label: 'Report',
      icon: 'far fa-file',
      class: 'text-primary',
      visible: true,
      items: [],
    };

    var sendSMSReminderMenuItem = {
      label: 'Mag Send ng SMS Reminder',
      name: 'sendSMSReminder',
      icon: 'fas fa-sms',
      class: 'text-primary',
      visible: true,
    };

    var certificateOfResidencyMenuItem = {
      label: 'Certificate Of Residency',
      name: APP_REPORTVIEW.RESIDENTREQUESTCERTIFICATE,
      icon: 'far fa-file',
      class: 'text-primary',
      visible: true,
    };

    if (menuReport.items) {
      menuReport.items.push(certificateOfResidencyMenuItem);
    }

    if (this.CurrentObject.IsActive != true) return;

    this.addMenuItem(this._menuItem_Delete);

    if (this.CurrentObject.ID > 0) {
      this.addMenuItem(sendSMSReminderMenuItem);
      this.addMenuItem(menuReport);
    }
  }

  async menubar_OnClick(e: any) {
    if (e.item.name == APP_REPORTVIEW.RESIDENTREQUESTCERTIFICATE) {
      this.customNavigate(['Report'], {
        ReportName: 'RESIDENTREQUESTCERTIFICATE',
        filterValues: [
          {
            dataField: 'ID',
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.currentResidentRequest_Certificate.ID,
          },
        ],
      });
    } else if (e.item.name == 'sendSMSReminder') {
      this.loading = true;

      if (this.CurrentObject.ContactNumber_ResidentRequest == null)
        this.CurrentObject.ContactNumber_ResidentRequest = '';

      var contactNumbers =
        this.CurrentObject.ContactNumber_ResidentRequest.split(',');

      var obj = {
        CellPhoneNumbers: contactNumbers,
        Message:
          "Magandang araw, ang iyong CERTIFICATE OF RESIDENCY na iyong nirequest ay natapos nang i-proseso at nakahanda na ito'y iyong kunin.",
      };

      var r = await this.ds.sendtext(obj);

      if (
        r == null ||
        r.PhoneNumberStatus == undefined ||
        r.PhoneNumberStatus == null
      ) {
        this.toastService.warning(
          'Ang SMS Reminder ay hindi nag tagumpay sa pagpapadala.'
        );
      } else {
        this.toastService.success(
          'Ang SMS reminder ay matagumpay na naipadala.'
        );
      }

      this.loading = false;
    }
  }

  DetailView_onLoad() {
    if (
      this.CurrentObject['ResidentRequest_Certificate'] == null ||
      this.CurrentObject['ResidentRequest_Certificate'] == undefined
    ) {
      this.CurrentObject['ResidentRequest_Certificate'] = [];
    }

    if (this.CurrentObject['ResidentRequest_Certificate'].length == 0) {
      var obj = new ResidentRequest_Certificate();
      obj.ID = this.getTempID();
      obj.ID_Company = this.currentUser.ID_Company;
      this.CurrentObject['ResidentRequest_Certificate'].push(obj);
    }

    this.currentResidentRequest_Certificate =
      this.CurrentObject['ResidentRequest_Certificate'][0];

    if (this.currentResidentRequest_Certificate.ID_Company == null) {
      this.currentResidentRequest_Certificate.ID_Company =
        this.currentUser.ID_Company;
    }
  }

  currentResidentRequest_Certificate_onModelChanged(e: IControlModelArg) {
    this.currentResidentRequest_Certificate[e.name] = e.value;

    if (e.displayName != undefined) {
      this.currentResidentRequest_Certificate[e.displayName] = e.displayValue;
    }

    this.isDirty = true;
  }

  protected pGetRecordOptions(): any {
    var options: any = {};
    var configKeys = ['ID_Resident', 'ID_ResidentRequest'];

    configKeys.forEach((key) => {
      if (
        this.configOptions[key] != undefined &&
        this.configOptions[key] != null
      ) {
        options[key] = this.configOptions[key];
      }
    });

    return options;
  }
}
