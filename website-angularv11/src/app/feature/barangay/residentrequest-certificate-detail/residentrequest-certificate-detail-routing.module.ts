import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResidentRequestCertificateDetailComponent } from './residentrequest-certificate-detail.component';

const routes: Routes = [{ path: '', component: ResidentRequestCertificateDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResidentRequestCertificateDetailRoutingModule { }
