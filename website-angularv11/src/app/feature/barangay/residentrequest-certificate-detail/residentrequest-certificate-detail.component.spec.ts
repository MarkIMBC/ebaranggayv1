import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidentRequestCertificateDetailComponent } from './residentrequest-certificate-detail.component';

describe('ResidentRequestCertificateDetailComponent', () => {
  let component: ResidentRequestCertificateDetailComponent;
  let fixture: ComponentFixture<ResidentRequestCertificateDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResidentRequestCertificateDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidentRequestCertificateDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
