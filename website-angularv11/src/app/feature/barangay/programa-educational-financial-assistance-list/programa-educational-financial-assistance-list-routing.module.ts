import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProgramaEducationalFinancialAssistanceListComponent } from './programa-educational-financial-assistance-list.component';

const routes: Routes = [{ path: '', component: ProgramaEducationalFinancialAssistanceListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgramaEducationalFinancialAssistanceListRoutingModule { }



