import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProgramaEducationalFinancialAssistanceListRoutingModule } from './programa-educational-financial-assistance-list-routing.module';
import { ProgramaEducationalFinancialAssistanceListComponent } from './programa-educational-financial-assistance-list.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ProgramaEducationalFinancialAssistanceListComponent],
  imports: [
    CommonModule,
    ProgramaEducationalFinancialAssistanceListRoutingModule,
    SharedModule,
  ]
})
export class ProgramaEducationalFinancialAssistanceListModule { }





