import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramaEducationalFinancialAssistanceListComponent } from './programa-educational-financial-assistance-list.component';

describe('ProgramaEducationalFinancialAssistanceListComponent', () => {
  let component: ProgramaEducationalFinancialAssistanceListComponent;
  let fixture: ComponentFixture<ProgramaEducationalFinancialAssistanceListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProgramaEducationalFinancialAssistanceListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramaEducationalFinancialAssistanceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
