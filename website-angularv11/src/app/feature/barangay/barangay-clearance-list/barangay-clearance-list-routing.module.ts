import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BarangayClearanceListComponent } from './barangay-clearance-list.component';

const routes: Routes = [{ path: '', component: BarangayClearanceListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BarangayClearanceListRoutingModule { }
