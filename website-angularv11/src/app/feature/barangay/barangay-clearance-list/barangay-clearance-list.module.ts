import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BarangayClearanceListRoutingModule } from './barangay-clearance-list-routing.module';
import { BarangayClearanceListComponent } from './barangay-clearance-list.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [BarangayClearanceListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    BarangayClearanceListRoutingModule
  ]
})
export class BarangayClearanceListModule { }
