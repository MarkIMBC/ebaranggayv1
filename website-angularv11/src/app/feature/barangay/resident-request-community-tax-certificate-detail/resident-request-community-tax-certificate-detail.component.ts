import { ResidentRequest_CommunityTaxCertificate } from './../../../../shared/APP_MODELS';
import { Component } from '@angular/core';
import { BaseDetailViewComponent } from '../../base-detail-view/base-detail-view.component';
import { IControlModelArg, IFormValidation } from 'src/shared/APP_HELPER';

@Component({
  selector: 'app-resident-request-community-tax-certificate-detail',
  templateUrl:
    './resident-request-community-tax-certificate-detail.component.html',
  styleUrls: [
    './../../base-detail-view/base-detail-view.component.less',
    './resident-request-community-tax-certificate-detail.component.less',
  ],
})
export class ResidentRequestCommunityTaxCertificateDetailComponent extends BaseDetailViewComponent {
  ModelName: string = 'ResidentRequest';
  headerTitle: string = 'Community Tax Certificate';
  routerFeatureName: string = 'ResidentRequest_CommunityTaxCertificate';

  currentResidentRequest_CommunityTaxCertificate: any =
    new ResidentRequest_CommunityTaxCertificate();

  displayMember: string = 'Name';

  loadInitMenuItem() {
    if (this.CurrentObject.IsActive == true) {
      this.menuItems.push(this._menuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  loadMenuItems() {
    if (this.CurrentObject.IsActive != true) return;

    //this.addMenuItem(this._menuItem_Delete);
  }

  async menubar_OnClick(e: any) {}

  async DetailView_onLoad() {
    this.loadRightDrowDownMenu();
    if (
      this.CurrentObject['ResidentRequest_CommunityTaxCertificate'] == null ||
      this.CurrentObject['ResidentRequest_CommunityTaxCertificate'] == undefined
    ) {
      this.CurrentObject['ResidentRequest_CommunityTaxCertificate'] = [];
    }

    if (
      this.CurrentObject['ResidentRequest_CommunityTaxCertificate'].length == 0
    ) {
      var obj = new ResidentRequest_CommunityTaxCertificate();
      obj.ID = this.getTempID();
      obj.ID_Company = this.currentUser.ID_Company;

      this.CurrentObject['ResidentRequest_CommunityTaxCertificate'].push(obj);
    }

    this.currentResidentRequest_CommunityTaxCertificate =
      this.CurrentObject['ResidentRequest_CommunityTaxCertificate'][0];

    if (
      this.currentResidentRequest_CommunityTaxCertificate.ID_Company == null
    ) {
      this.currentResidentRequest_CommunityTaxCertificate.ID_Company =
        this.currentUser.ID_Company;
    }
  }

  currentResidentRequest_CommunityTaxCertificate_onModelChanged(
    e: IControlModelArg
  ) {
    this.currentResidentRequest_CommunityTaxCertificate[e.name] = e.value;

    if (e.displayName != undefined) {
      this.currentResidentRequest_CommunityTaxCertificate[e.displayName] =
        e.displayValue;
    }

    this.isDirty = true;
  }

  public CurrentObject_onBeforeSaving() {}

  protected pGetRecordOptions(): any {
    var options: any = {};
    var configKeys = ['ID_Resident', 'ID_ResidentRequest'];

    configKeys.forEach((key) => {
      if (
        this.configOptions[key] != undefined &&
        this.configOptions[key] != null
      ) {
        options[key] = this.configOptions[key];
      }
    });

    return options;
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    var Name = this.CurrentObject.Name;

    if (Name == undefined || Name == null) Name = '';

    if (Name.length == 0) {
      validations.push({
        message: 'Name is required.',
      });
    }

    return Promise.resolve(validations);
  }
}
