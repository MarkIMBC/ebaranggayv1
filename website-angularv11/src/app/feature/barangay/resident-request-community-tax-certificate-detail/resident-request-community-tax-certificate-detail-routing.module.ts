import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResidentRequestCommunityTaxCertificateDetailComponent } from './resident-request-community-tax-certificate-detail.component';

const routes: Routes = [{ path: '', component: ResidentRequestCommunityTaxCertificateDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResidentRequestCommunityTaxCertificateDetailRoutingModule { }
