import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidentRequestCommunityTaxCertificateDetailComponent } from './resident-request-community-tax-certificate-detail.component';

describe('ResidentRequestCommunityTaxCertificateDetailComponent', () => {
  let component: ResidentRequestCommunityTaxCertificateDetailComponent;
  let fixture: ComponentFixture<ResidentRequestCommunityTaxCertificateDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResidentRequestCommunityTaxCertificateDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidentRequestCommunityTaxCertificateDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
