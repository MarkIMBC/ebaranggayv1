import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResidentRequestCommunityTaxCertificateDetailRoutingModule } from './resident-request-community-tax-certificate-detail-routing.module';
import { ResidentRequestCommunityTaxCertificateDetailComponent } from './resident-request-community-tax-certificate-detail.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ResidentRequestCommunityTaxCertificateDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    ResidentRequestCommunityTaxCertificateDetailRoutingModule
  ]
})
export class ResidentRequestCommunityTaxCertificateDetailModule { }

