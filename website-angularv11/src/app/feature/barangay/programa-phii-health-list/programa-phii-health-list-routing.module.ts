import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProgramaPhiiHealthListComponent } from './programa-phii-health-list.component';

const routes: Routes = [{ path: '', component: ProgramaPhiiHealthListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgramaPhiiHealthListRoutingModule { }
