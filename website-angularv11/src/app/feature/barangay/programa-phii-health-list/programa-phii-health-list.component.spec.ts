import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramaPhiiHealthListComponent } from './programa-phii-health-list.component';

describe('ProgramaPhiiHealthListComponent', () => {
  let component: ProgramaPhiiHealthListComponent;
  let fixture: ComponentFixture<ProgramaPhiiHealthListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProgramaPhiiHealthListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramaPhiiHealthListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
