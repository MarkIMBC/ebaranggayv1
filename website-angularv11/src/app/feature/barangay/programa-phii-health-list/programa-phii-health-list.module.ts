import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProgramaPhiiHealthListRoutingModule } from './programa-phii-health-list-routing.module';
import { ProgramaPhiiHealthListComponent } from './programa-phii-health-list.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [ProgramaPhiiHealthListComponent],
  imports: [CommonModule, SharedModule, ProgramaPhiiHealthListRoutingModule],
})
export class ProgramaPhiiHealthListModule {}
