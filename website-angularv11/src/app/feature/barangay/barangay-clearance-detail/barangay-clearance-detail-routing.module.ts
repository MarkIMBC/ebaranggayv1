import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BarangayClearanceDetailComponent } from './barangay-clearance-detail.component';

const routes: Routes = [{ path: '', component: BarangayClearanceDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BarangayClearanceDetailRoutingModule { }
