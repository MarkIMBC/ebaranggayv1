import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BarangayClearanceDetailRoutingModule } from './barangay-clearance-detail-routing.module';
import { BarangayClearanceDetailComponent } from './barangay-clearance-detail.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [BarangayClearanceDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    BarangayClearanceDetailRoutingModule
  ]
})
export class BarangayClearanceDetailModule { }



