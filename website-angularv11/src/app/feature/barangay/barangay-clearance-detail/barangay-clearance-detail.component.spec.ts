import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BarangayClearanceDetailComponent } from './barangay-clearance-detail.component';

describe('BarangayClearanceDetailComponent', () => {
  let component: BarangayClearanceDetailComponent;
  let fixture: ComponentFixture<BarangayClearanceDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BarangayClearanceDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BarangayClearanceDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
