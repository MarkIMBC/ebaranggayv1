import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CertificateOfIndigencyDetailComponent } from './certificate-of-indigency-detail.component';

const routes: Routes = [{ path: '', component: CertificateOfIndigencyDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CertificateOfIndigencyDetailRoutingModule { }
