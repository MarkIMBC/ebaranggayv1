import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CertificateOfIndigencyDetailRoutingModule } from './certificate-of-indigency-detail-routing.module';
import { CertificateOfIndigencyDetailComponent } from './certificate-of-indigency-detail.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [CertificateOfIndigencyDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    CertificateOfIndigencyDetailRoutingModule,
  ],
})
export class CertificateOfIndigencyDetailModule {}
