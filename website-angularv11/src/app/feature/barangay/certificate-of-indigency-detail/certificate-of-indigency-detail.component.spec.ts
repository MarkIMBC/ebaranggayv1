import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CertificateOfIndigencyDetailComponent } from './certificate-of-indigency-detail.component';

describe('CertificateOfIndigencyDetailComponent', () => {
  let component: CertificateOfIndigencyDetailComponent;
  let fixture: ComponentFixture<CertificateOfIndigencyDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CertificateOfIndigencyDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificateOfIndigencyDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
