import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProgramDetailRoutingModule } from './program-detail-routing.module';
import { ProgramDetailComponent } from './program-detail.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ProgramDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    ProgramDetailRoutingModule
  ]
})
export class ProgramDetailModule { }
