import { IFormValidation } from 'src/shared/APP_HELPER';
import {
  BaseDetailViewComponent,
  CurrentObjectOnValueChangeArg,
} from '../../base-detail-view/base-detail-view.component';
import { Component } from '@angular/core';

@Component({
  selector: 'app-program-detail',
  templateUrl: './program-detail.component.html',
  styleUrls: [
    './../../base-detail-view/base-detail-view.component.less',
    './program-detail.component.less',
  ],
})
export class ProgramDetailComponent extends BaseDetailViewComponent {
  ModelName: string = 'Program';
  headerTitle: string = 'Listahanan';

  public authenticateCurrentObjectOwnerShip(obj: any) {
    // if (obj['ID_Company'] != undefined) {
    //   if (obj.ID_Company != this.currentUser.ID_Company) {
    //     this.router.navigate(['PageNotFound']);
    //     return;
    //   }
    // }
  }

  loadInitMenuItem() {
    this.menuItems.push(this._menuItem_New);

    if (this.CurrentObject.ID_Company != 1) {
      this.menuItems.push(this._menuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  DetailView_onLoad() {}

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {}

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    var Name = this.CurrentObject.Name;

    if (Name == undefined || Name == null) Name = '';

    return Promise.resolve(validations);
  }
}
