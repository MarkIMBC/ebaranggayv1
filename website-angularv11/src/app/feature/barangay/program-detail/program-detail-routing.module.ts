import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProgramDetailComponent } from './program-detail.component';

const routes: Routes = [{ path: '', component: ProgramDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgramDetailRoutingModule { }
