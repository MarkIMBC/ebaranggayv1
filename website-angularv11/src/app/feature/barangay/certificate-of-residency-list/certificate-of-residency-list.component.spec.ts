import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CertificateOfResidencyListComponent } from './certificate-of-residency-list.component';

describe('CertificateOfResidencyListComponent', () => {
  let component: CertificateOfResidencyListComponent;
  let fixture: ComponentFixture<CertificateOfResidencyListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CertificateOfResidencyListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificateOfResidencyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
