import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CertificateOfResidencyListComponent } from './certificate-of-residency-list.component';

const routes: Routes = [{ path: '', component: CertificateOfResidencyListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CertificateOfResidencyListRoutingModule { }
