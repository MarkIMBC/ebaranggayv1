import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CertificateOfResidencyListRoutingModule } from './certificate-of-residency-list-routing.module';
import { CertificateOfResidencyListComponent } from './certificate-of-residency-list.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [CertificateOfResidencyListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    CertificateOfResidencyListRoutingModule,
  ],
})
export class CertificateOfResidencyListModule {}
