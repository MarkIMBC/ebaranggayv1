import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CertificationOfNoIncomeListComponent } from './certification-of-no-income-list.component';

const routes: Routes = [{ path: '', component: CertificationOfNoIncomeListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CertificationOfNoIncomeListRoutingModule { }
