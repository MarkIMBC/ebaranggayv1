import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CertificationOfNoIncomeListRoutingModule } from './certification-of-no-income-list-routing.module';
import { CertificationOfNoIncomeListComponent } from './certification-of-no-income-list.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [CertificationOfNoIncomeListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    CertificationOfNoIncomeListRoutingModule,
  ],
})
export class CertificationOfNoIncomeListModule {}
