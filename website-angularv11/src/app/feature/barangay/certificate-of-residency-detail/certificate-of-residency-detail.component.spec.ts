import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CertificateOfResidencyDetailComponent } from './certificate-of-residency-detail.component';

describe('CertificateOfResidencyDetailComponent', () => {
  let component: CertificateOfResidencyDetailComponent;
  let fixture: ComponentFixture<CertificateOfResidencyDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CertificateOfResidencyDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificateOfResidencyDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
