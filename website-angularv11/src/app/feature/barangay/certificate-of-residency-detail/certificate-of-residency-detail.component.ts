import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import {
  FilterCriteriaType,
  IFormValidation,
  PropertyTypeEnum,
} from 'src/shared/APP_HELPER';
import {
  BaseDetailViewComponent,
  CurrentObjectOnValueChangeArg,
} from '../../base-detail-view/base-detail-view.component';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { Component } from '@angular/core';

@Component({
  selector: 'app-certificate-of-residency-detail',
  templateUrl: './certificate-of-residency-detail.component.html',
  styleUrls: [
    './../../base-detail-view/base-detail-view.component.less',
    './certificate-of-residency-detail.component.less',
  ],
})
export class CertificateOfResidencyDetailComponent extends BaseDetailViewComponent {
  ModelName: string = 'CertificateOfResidency';
  headerTitle: string = 'Certificate Of Residency';

  ID_Gender_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM tGender',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  ID_CivilStatus_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM tCivilStatus',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };


  DetailView_onLoad() {}

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {}

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    var Name = this.CurrentObject.Name;
    var Resident_BusinessOwner = this.CurrentObject.Resident_BusinessOwner;
    var RequestedBy = this.CurrentObject.RequestedBy;

    if (Name == undefined || Name == null) Name = '';

    return Promise.resolve(validations);
  }
}
