import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CertificateOfResidencyDetailComponent } from './certificate-of-residency-detail.component';

const routes: Routes = [{ path: '', component: CertificateOfResidencyDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CertificateOfResidencyDetailRoutingModule { }
