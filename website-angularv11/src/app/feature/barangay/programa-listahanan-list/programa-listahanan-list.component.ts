
import { Component } from '@angular/core';
import { Item } from 'src/shared/APP_MODELS';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { BaseListViewComponent } from '../../base-list-view/base-list-view.component';

@Component({
  selector: 'app-programa-listahanan-list',
  templateUrl:'./programa-listahanan-list.component.html',
  styleUrls: [
    './../../base-list-view/base-list-view.component.less',
    './programa-listahanan-list.component.less',
  ],
})
export class ProgramaListahananListComponent extends BaseListViewComponent {
  headerTitle: string = 'Listahanan';

  CurrentObject: any = {
    Name: '',
    Name_ItemCategory: '',
    ID_VaccinatedStatus: null,
    Name_VaccinatedStatus: '',
  };

  ID_Vaccinated_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: `
          SELECT 1 ID, 'Nabakunahan' Name
          UNION ALL
          SELECT 2 ID, 'Hindi pa nabakunahan' Name
      `,
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  dataSource: any[] = [];

  async ListView_Onload() {
    await this.loadRecords();
  }

  OrderByString: string = 'Name ASC, Name_Program ASC';

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT
              *
            FROM vResident_ProgramListView_Listahanan
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  dataSource_InitLoad(obj: any) {
    var _records: any[] = [];
    var _currentRecord: any;
    var _ID_Resident = 0;

    obj.Records.forEach((record: any) => {
      if (_ID_Resident != record.ID) {
        _currentRecord = Object.assign({}, record);

        _currentRecord.Programs = [];
        _ID_Resident = _currentRecord.ID;

        delete _currentRecord.ID_Program;
        delete _currentRecord.Name_Program;

        _records.push(_currentRecord);
      }

      _currentRecord.Programs.push({
        ID: record.ID_Program,
        Name: record.Name_Program,
      });
    });

    obj.Records = _records;

    this.dataSource = obj.Records;
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.Name.length > 0) {
      filterString += `Name LIKE '%${this.CurrentObject.Name}%'`;
    }

    if (this.CurrentObject.ID_VaccinatedStatus != null) {
      filterString += `ISNULL(IsVaccinated, 0) = ${
        this.CurrentObject.ID_VaccinatedStatus != 1 ? 0 : 1
      }`;
    }

    return filterString;
  }

  Row_OnClick(rowData: Item) {
    this.customNavigate(['Resident', rowData.ID], {});
  }

  menuItem_New_onClick() {
    this.customNavigate(['Resident', -1], {});
  }
}
