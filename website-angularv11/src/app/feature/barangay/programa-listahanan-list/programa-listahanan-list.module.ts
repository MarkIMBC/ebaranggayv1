import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProgramaListahananListRoutingModule } from './programa-listahanan-list-routing.module';
import { ProgramaListahananListComponent } from './programa-listahanan-list.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ProgramaListahananListComponent],
  imports: [
    CommonModule,
    ProgramaListahananListRoutingModule,
    SharedModule

  ]
})
export class ProgramaListahananListModule { }
