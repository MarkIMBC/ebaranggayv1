import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProgramaListahananListComponent } from './programa-listahanan-list.component';

const routes: Routes = [{ path: '', component: ProgramaListahananListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgramaListahananListRoutingModule { }
