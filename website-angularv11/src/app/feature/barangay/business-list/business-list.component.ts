

import { Component } from '@angular/core';
import { Item } from 'src/shared/APP_MODELS';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { BaseListViewComponent } from '../../base-list-view/base-list-view.component';

@Component({
  selector: 'app-business-list',
  templateUrl: './business-list.component.html',
  styleUrls: [
    './../../base-list-view/base-list-view.component.less',
    './business-list.component.less',
  ],
})
export class BusinessListComponent extends BaseListViewComponent {
  headerTitle: string = 'Mga Negosyo';

  CurrentObject: any = {
    Name: '',
    Name_ItemCategory: '',
    ID_VaccinatedStatus: null,
    Name_VaccinatedStatus: '',
  };


  dataSource: any[] = [];

  async ListView_Onload() {
    await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT
              *
            FROM vResident_Business_Listview
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company} AND
              IsActive = 1
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.Name.length > 0) {
      filterString += `Name LIKE '%${this.CurrentObject.Name}%'`;
    }

    if (this.CurrentObject.ID_VaccinatedStatus != null) {
      filterString += `ISNULL(IsVaccinated, 0) = ${
        this.CurrentObject.ID_VaccinatedStatus != 1 ? 0 : 1
      }`;
    }

    return filterString;
  }

  Row_OnClick(rowData: Item) {
    this.customNavigate(['Resident_Business', rowData.ID], {});
  }

  menuItem_New_onClick() {
    this.customNavigate(['Resident_Business', -1], {});
  }
}

