
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';


import { BusinessListRoutingModule } from './business-list-routing.module';
import { BusinessListComponent } from './business-list.component';

import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';

@NgModule({
  declarations: [BusinessListComponent],
  imports: [
    CommonModule,
    SharedModule,
    BusinessListRoutingModule
  ],
  providers: [
    DataService,
    CrypterService,
  ],
})
export class BusinessListModule { }
