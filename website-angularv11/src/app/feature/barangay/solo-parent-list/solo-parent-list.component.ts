import { Component } from '@angular/core';
import { Item } from 'src/shared/APP_MODELS';
import { BaseListViewComponent } from '../../base-list-view/base-list-view.component';

@Component({
  selector: 'app-solo-parent-list',
  templateUrl: './solo-parent-list.component.html',
  styleUrls: [
    './../../base-list-view/base-list-view.component.less',
    './solo-parent-list.component.less',
  ],
})
export class SoloParentListComponent extends BaseListViewComponent {
  headerTitle: string = 'Solo Parent';

  CurrentObject: any = {
    Name: '',
    Name_ItemCategory: '',
    ID_VaccinatedStatus: null,
    Name_VaccinatedStatus: '',
  };

  dataSource: any[] = [];

  async ListView_Onload() {
    await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT
              *
            FROM vSoloParent_Listview
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company} AND
              IsActive = 1
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.Name.length > 0) {
      filterString += `Name LIKE '%${this.CurrentObject.Name}%'`;
    }

    return filterString;
  }

  Row_OnClick(rowData: Item) {
    this.customNavigate(['SoloParent', rowData.ID], {});
  }

  menuItem_New_onClick() {
    this.customNavigate(['SoloParent', -1], {});
  }
}
