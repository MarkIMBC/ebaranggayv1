import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { ModalModule } from "src/app/shared/modal/modal.module";
import { SharedModule } from "src/app/shared/shared.module";
import { SoloParentListRoutingModule } from "./solo-parent-list-routing.module";
import { SoloParentListComponent } from "./solo-parent-list.component";

@NgModule({
  declarations: [SoloParentListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    SoloParentListRoutingModule
  ]
})
export class SoloParentListModule { }

