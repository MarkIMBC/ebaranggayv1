import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SoloParentListComponent } from './solo-parent-list.component';

const routes: Routes = [{ path: '', component: SoloParentListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SoloParentListRoutingModule { }
