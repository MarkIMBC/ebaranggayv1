import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AnnoucementDetailRoutingModule } from './annoucement-detail-routing.module';
import { AnnoucementDetailComponent } from './annoucement-detail.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [AnnoucementDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    AnnoucementDetailRoutingModule
  ]
})
export class AnnoucementDetailModule { }
