import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnnoucementDetailComponent } from './annoucement-detail.component';

const routes: Routes = [{ path: '', component: AnnoucementDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnnoucementDetailRoutingModule { }
