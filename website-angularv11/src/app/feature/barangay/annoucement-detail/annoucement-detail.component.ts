import { IFormValidation, ItemTypeEnum } from '../../../../shared/APP_HELPER';
import { Component, ViewChild } from '@angular/core';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { BaseDetailViewComponent } from '../../base-detail-view/base-detail-view.component';

@Component({
  selector: 'app-annoucement-detail',
  templateUrl: './annoucement-detail.component.html',
  styleUrls: [
    './../../base-detail-view/base-detail-view.component.less',
    './annoucement-detail.component.less'
  ]
})
export class AnnoucementDetailComponent extends BaseDetailViewComponent {

  ModelName: string = 'Announcement'
  headerTitle: string = 'Anunsyo'

  displayMember: string = "Name";

  loadInitMenuItem() {

    this.menuItems.push(this._menuItem_New);

    if(this.CurrentObject.IsActive != true) return;

    this.menuItems.push(this._menuItem_Save);

    if (this.CurrentObject.ID > 0) {

      this.menuItems.push(this._menuItem_Refresh);
      this.menuItems.push(this._menuItem_Delete);
    }
  }

  DetailView_onLoad() {

  }

  async validation(): Promise<IFormValidation[]> {

    var validations: IFormValidation[] = [];
    var ContactNumber = this.CurrentObject.ContactNumber;

    if(ContactNumber == null || ContactNumber == undefined ) ContactNumber = '';

    if (this.CurrentObject.Name == null) this.CurrentObject.Name = '';
    this.CurrentObject.Name = this.CurrentObject.Name.trimStart();
    this.CurrentObject.Name = this.CurrentObject.Name.trimEnd();

    if (this.CurrentObject.Name.length == 0) {

      validations.push({

        message: `Ang Anunsyo ay kailangan.`,
      });
    }


    return Promise.resolve(validations);
  }

}
