import { Component } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum, InventoryStatusEnum } from 'src/shared/APP_HELPER';
import { APP_MODEL } from 'src/shared/APP_MODELS';
import { BaseListViewComponent } from '../../base-list-view/base-list-view.component';

@Component({
  selector: 'app-resident-request-transaction-list',
  templateUrl: './resident-request-transaction-list.component.html',
  styleUrls: [
    './../../base-list-view/base-list-view.component.less',
    './resident-request-transaction-list.component.less',
  ],
})
export class ResidentRequestTransactionListComponent extends BaseListViewComponent {
  headerTitle: string = 'Mga Request';

  CurrentObject: any = {
    Name: '',
    Name_ItemCategory: '',
    ID_InventoryStatus: 0,
    Name_InventoryStatus: '',
  };

  dataSource: any[] = [];

  items: AdminLTEMenuItem[] = [
    {
      label: 'Refresh',
      icon: 'fas fa-sync',
      visible: true,
      command: () => {
        this.loadRecords();
        return true;
      },
    },
  ];

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {
        return true;
      },
    },
    {
      label: 'ResidentRequest',
      visible: true,
      isActive: true,
      command: () => {
        return true;
      },
    },
  ];

  ID_InventoryStatus_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: `
        SELECT '-1' ID, 'Available Stock' Name
        UNION ALL
        Select
          ID, Name
        FROM tInventoryStatus
        WHERE
          ID IN (1, 2)
      `,
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  async ListView_Onload() {
    await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT
                *
            FROM vResidentRequestTransaction_ListView
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.Name.length > 0) {
      filterString += `Name LIKE '%${this.CurrentObject.Name}%'`;
    }

    if (this.CurrentObject['ID_InventoryStatus']) {
      if (this.CurrentObject['ID_InventoryStatus'] == -1) {
        if (filterString.length > 0) filterString += ' AND ';
        filterString += `ISNULL(ID_InventoryStatus, 0) IN (
          ${InventoryStatusEnum.High},
          ${InventoryStatusEnum.Medium}
        )`;
      } else {
        if (filterString.length > 0) filterString += ' AND ';
        filterString += `ISNULL(ID_InventoryStatus, 0) = ${this.CurrentObject['ID_InventoryStatus']} `;
      }
    }

    return filterString;
  }

  Row_OnClick(rowData: any) {
    var routerLink = '';

    if (
      rowData.Oid_Model == APP_MODEL.RESIDENTREQUEST_CERTIFICATE.toLowerCase()
    ) {
      routerLink = 'ResidentRequest_Certificate';
    } else if (
      rowData.Oid_Model ==
      APP_MODEL.RESIDENTREQUEST_COMMUNITYTAXCERTIFICATE.toLowerCase()
    ) {
      routerLink = 'ResidentRequest_CommunityTaxCertificate';
    } else if (
      rowData.Oid_Model ==
      APP_MODEL.RESIDENTREQUEST_BARANGAYBUSINESSCLEARANCE.toLowerCase()
    ) {
      routerLink = 'ResidentRequest_BarangayBusinessClearance';
    } else if (
      rowData.Oid_Model ==
      APP_MODEL.RESIDENTREQUEST_INDIGENCYCERTIFICATE.toLowerCase()
    ) {
      routerLink = 'ResidentRequest_IndigencyCertificate';
    }

    this.customNavigate([routerLink, rowData.ID_ResidentRequest], {});
  }

  menuItem_New_onClick() {
    this.customNavigate(['ResidentRequest', -1], {});
  }
}
