import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResidentRequestTransactionListRoutingModule } from './resident-request-transaction-list-routing.module';
import { ResidentRequestTransactionListComponent } from './resident-request-transaction-list.component';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ResidentRequestTransactionListComponent],
  imports: [
    CommonModule,
    SharedModule,
    ResidentRequestTransactionListRoutingModule
  ],
  providers: [
    DataService,
    CrypterService,
  ],
})
export class ResidentRequestTransactionListModule { }
