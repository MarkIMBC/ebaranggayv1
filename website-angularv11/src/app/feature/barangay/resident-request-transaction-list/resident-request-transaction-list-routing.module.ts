import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResidentRequestTransactionListComponent } from './resident-request-transaction-list.component';

const routes: Routes = [{ path: '', component: ResidentRequestTransactionListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResidentRequestTransactionListRoutingModule { }
