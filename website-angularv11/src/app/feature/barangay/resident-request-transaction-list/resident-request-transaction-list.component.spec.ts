import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidentRequestTransactionListComponent } from './resident-request-transaction-list.component';

describe('ResidentRequestTransactionListComponent', () => {
  let component: ResidentRequestTransactionListComponent;
  let fixture: ComponentFixture<ResidentRequestTransactionListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResidentRequestTransactionListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidentRequestTransactionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
