import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { Item, LawSuit } from 'src/shared/APP_MODELS';
import { BaseListViewComponent } from '../../base-list-view/base-list-view.component';

@Component({
  selector: 'app-law-suit-list',
  templateUrl: './law-suit-list.component.html',
  styleUrls: [
    './../../base-list-view/base-list-view.component.less',
    './law-suit-list.component.less',
  ],
})
export class LawSuitListComponent extends BaseListViewComponent {
  headerTitle: string = 'Mga Habla';

  CurrentObject: any = {
    Complainant_Name_Resident: '',
    Respondent_Name_Resident: '',
  };

  dataSource: any[] = [];

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {
        return true;
      },
    },
    {
      label: 'ItemService',
      isActive: true,
      visible: true,
      command: () => {
        return true;
      },
    },
  ];

  async ListView_Onload() {
    await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT
              *
            FROM vLawSuit_Listview
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.Complainant_Name_Resident.length > 0) {
      filterString += `Complainant_Name_Resident LIKE '%${this.CurrentObject.Complainant_Name_Resident}%'`;
    }

    if (this.CurrentObject.Respondent_Name_Resident.length > 0) {
      filterString += `Respondent_Name_Resident LIKE '%${this.CurrentObject.Respondent_Name_Resident}%'`;
    }

    return filterString;
  }

  Row_OnClick(rowData: Item) {
    this.customNavigate(['LawSuit', rowData.ID], {});
  }

  menuItem_New_onClick() {
    this.customNavigate(['LawSuit', -1], {});
  }
}
