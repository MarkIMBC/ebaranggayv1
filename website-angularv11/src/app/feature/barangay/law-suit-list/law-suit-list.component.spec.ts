import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LawSuitListComponent } from './law-suit-list.component';

describe('LawSuitListComponent', () => {
  let component: LawSuitListComponent;
  let fixture: ComponentFixture<LawSuitListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LawSuitListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LawSuitListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
