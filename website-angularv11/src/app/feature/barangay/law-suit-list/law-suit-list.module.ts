import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LawSuitListRoutingModule } from './law-suit-list-routing.module';
import { LawSuitListComponent } from './law-suit-list.component';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [LawSuitListComponent],
  imports: [
    CommonModule,
    SharedModule,
    LawSuitListRoutingModule
  ],
  providers: [
    DataService,
    CrypterService,
  ],
})
export class LawSuitListModule { }

