import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LawSuitListComponent } from './law-suit-list.component';

const routes: Routes = [{ path: '', component: LawSuitListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LawSuitListRoutingModule { }
