import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidentRequestDetailComponent } from './residentrequest-detail.component';

describe('ResidentRequestDetailComponent', () => {
  let component: ResidentRequestDetailComponent;
  let fixture: ComponentFixture<ResidentRequestDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResidentRequestDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidentRequestDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
