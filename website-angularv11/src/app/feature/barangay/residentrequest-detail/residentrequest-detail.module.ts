import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ResidentRequestDetailRoutingModule } from './residentrequest-detail-routing.module';
import { ResidentRequestDetailComponent } from './residentrequest-detail.component';
import { ResidenceCertificateListComponent } from './residence-certificate-list/residence-certificate-list.component';


@NgModule({
  declarations: [ResidentRequestDetailComponent, ResidenceCertificateListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    ResidentRequestDetailRoutingModule
  ]
})
export class ResidentRequestDetailModule { }
