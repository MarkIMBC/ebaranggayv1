import { Component, ViewChild } from "@angular/core";
import { ItemTypeEnum } from "src/shared/APP_HELPER";
import { BaseDetailViewComponent } from "../../base-detail-view/base-detail-view.component";
import { ResidenceCertificateListComponent } from "./residence-certificate-list/residence-certificate-list.component";

@Component({
  selector: 'app-residentrequest-detail',
  templateUrl: './residentrequest-detail.component.html',
  styleUrls: [
    './../../base-detail-view/base-detail-view.component.less',
    './residentrequest-detail.component.less'
  ]
})
export class ResidentRequestDetailComponent extends BaseDetailViewComponent {

  @ViewChild('residencecertificatelist') residencecertificatelist: ResidenceCertificateListComponent | undefined;

  ModelName: string = 'ResidentRequest'
  headerTitle: string = 'Resident Request'

  displayMember: string = "Name";

  loadInitMenuItem() {

    this.menuItems.push(this._menuItem_New);

    if(this.CurrentObject.IsActive != true) return;

    this.menuItems.push(this._menuItem_Save);

    if (this.CurrentObject.ID > 0) {

      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  loadMenuItems() {

    if(this.CurrentObject.IsActive != true) return;

    if(this.residencecertificatelist){

      this.residencecertificatelist.load(this.CurrentObject.ID)
    }

  }

  async menubar_OnClick(e: any) {


  }

  DetailView_onLoad() {

    this.CurrentObject.ID_ItemType = ItemTypeEnum.Inventoriable;
  }

  public CurrentObject_onBeforeSaving() {

    var item: any = this.CurrentObject;
    var fieldNames: string[] = ['UnitCost','UnitPrice','MinInventoryCount','MaxInventoryCount'];

    fieldNames.forEach(fieldName => {

      if(!item[fieldName]) item[fieldName] = 0;
      item[fieldName] = Math.abs(item[fieldName]);
    });
  }
}
