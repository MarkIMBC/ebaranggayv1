import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidenceCertificateListComponent } from './residence-certificate-list.component';

describe('ResidenceCertificateListComponent', () => {
  let component: ResidenceCertificateListComponent;
  let fixture: ComponentFixture<ResidenceCertificateListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResidenceCertificateListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidenceCertificateListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
