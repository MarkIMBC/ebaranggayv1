import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { ToastService } from 'src/app/shared/toast.service';

@Component({
  selector: 'residence-certificate-list',
  templateUrl: './residence-certificate-list.component.html',
  styleUrls: [
    './../../../base-list-view/base-list-view.component.less',
    './residence-certificate-list.component.less',
  ],
})
export class ResidenceCertificateListComponent implements OnInit {
  dataSource: any[] = [];
  loading: boolean = false;
  currentUser: TokenSessionFields = new TokenSessionFields();

  @Output() onCancel: EventEmitter<any> = new EventEmitter<any>();

  menuItems: AdminLTEMenuItem[] = [
    {
      label: 'Add Residence Certificate',
      icon: 'fa fa-plus',
      class: 'text-default',
      visible: true,
    },
  ];

  private ID_ResidentRequest: number = 0;

  constructor(
    private ds: DataService,
    private cs: CrypterService,
    private userAuth: UserAuthenticationService,
    private toastService: ToastService,
    private router: Router
  ) {}

  async ngOnInit(): Promise<void> {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  load(ID_ResidentRequest: number) {
    this.ID_ResidentRequest = ID_ResidentRequest;

    var sql = `SELECT
                    *
                FROM dbo.vResidentRequest_Certificate_ListvIew
                WHERE
                  ID_ResidentRequest = ${this.ID_ResidentRequest} AND ID_Company = ${this.currentUser.ID_Company}
                  AND ID_ResidentRequest > 0
    `;

    sql = this.cs.encrypt(sql);
    this.ds.query<any>(sql).then((obj) => {
      this.dataSource = obj;
    });
  }

  onRowClick(rowData: any) {
    var routeLink = ['ResidentRequest_Certificate', rowData.ID];

    GeneralfxService.customNavigate(
      this.router,
      this.cs,
      this.currentUser,
      routeLink,
      {}
    );
  }

  initmenubar_OnClick(e: any) {
    if (e.item.label == 'Add Residence Certificate') {
      var routeLink = ['ResidentRequest_Certificate', -1];

      GeneralfxService.customNavigate(
        this.router,
        this.cs,
        this.currentUser,
        routeLink,
        {
          ID_ResidentRequest: this.ID_ResidentRequest,
        }
      );
    }
  }
}
