import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResidentListSeniorCitizenComponent } from './resident-list-senior-citizen.component';

const routes: Routes = [{ path: '', component: ResidentListSeniorCitizenComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResidentListSeniorCitizenRoutingModule { }
