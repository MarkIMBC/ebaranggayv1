import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResidentListSeniorCitizenRoutingModule } from './resident-list-senior-citizen-routing.module';
import { ResidentListSeniorCitizenComponent } from './resident-list-senior-citizen.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [ResidentListSeniorCitizenComponent],
  imports: [CommonModule, SharedModule, ResidentListSeniorCitizenRoutingModule],
})
export class ResidentListSeniorCitizenModule {}
