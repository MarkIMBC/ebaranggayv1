import { InitialPageComponent } from './initial-page/initial-page.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BarangayComponent } from './barangay.component';

const routes: Routes = [
  {
    path: '',
    component: BarangayComponent,
    children: [
      {
        path: '',
        component: InitialPageComponent,
      },

      {
        path: 'AnnoucementList',
        loadChildren: () =>
          import('./annoucement-list/annoucement-list.module').then(
            (m) => m.AnnoucementListModule
          ),
      },

      {
        path: 'Announcement/:ID_CurrentObject/:configOptions',
        loadChildren: () =>
          import('./annoucement-detail/annoucement-detail.module').then(
            (m) => m.AnnoucementDetailModule
          ),
      },

      {
        path: 'Employee/:ID_CurrentObject/:configOptions',
        loadChildren: () =>
          import('./employee-detail/employee-detail.module').then(
            (m) => m.EmployeeDetailModule
          ),
      },

      {
        path: 'CurrentEmployee',
        loadChildren: () =>
          import('./employee-detail/employee-detail.module').then(
            (m) => m.EmployeeDetailModule
          ),
      },

      {
        path: 'Employee',
        loadChildren: () =>
          import('./employee-list/employee-list.module').then(
            (m) => m.EmployeeListModule
          ),
      },

      {
        path: 'CompanyDetail',
        loadChildren: () =>
          import('./company-detail/company-detail.module').then(
            (m) => m.CompanyDetailModule
          ),
      },

      {
        path: 'EmployeeDetail',
        loadChildren: () =>
          import('./employee-detail/employee-detail.module').then(
            (m) => m.EmployeeDetailModule
          ),
      },

      {
        path: 'EmployeeList',
        loadChildren: () =>
          import('./employee-list/employee-list.module').then(
            (m) => m.EmployeeListModule
          ),
      },

      {
        path: 'ResidentRequestList',
        loadChildren: () =>
          import('./residentrequest-list/residentrequest-list.module').then(
            (m) => m.ResidentRequestListModule
          ),
      },

      {
        path: 'ResidentRequest/:ID_CurrentObject/:configOptions',
        loadChildren: () =>
          import('./residentrequest-detail/residentrequest-detail.module').then(
            (m) => m.ResidentRequestDetailModule
          ),
      },

      {
        path: 'ResidentRequestTransactionList',
        loadChildren: () =>
          import(
            './resident-request-transaction-list/resident-request-transaction-list.module'
          ).then((m) => m.ResidentRequestTransactionListModule),
      },

      {
        path: 'ResidentRequest_Certificate/:ID_CurrentObject/:configOptions',
        loadChildren: () =>
          import(
            './residentrequest-certificate-detail/residentrequest-certificate-detail.module'
          ).then((m) => m.ResidentRequestCertificateDetailModule),
      },

      {
        path: 'ResidentRequest_BarangayBusinessClearance/:ID_CurrentObject/:configOptions',
        loadChildren: () =>
          import(
            './resident-request-barangay-business-clearance-detail/resident-request-barangay-business-clearance-detail.module'
          ).then((m) => m.ResidentRequestBarangayBusinessClearanceDetailModule),
      },

      {
        path: 'ResidentRequest_CommunityTaxCertificate/:ID_CurrentObject/:configOptions',
        loadChildren: () =>
          import(
            './resident-request-community-tax-certificate-detail/resident-request-community-tax-certificate-detail.module'
          ).then((m) => m.ResidentRequestCommunityTaxCertificateDetailModule),
      },

      {
        path: 'ResidentRequest_IndigencyCertificate/:ID_CurrentObject/:configOptions',
        loadChildren: () =>
          import(
            './resident-request-indigency-certificate-detail/resident-request-indigency-certificate-detail.module'
          ).then((m) => m.ResidentRequestIndigencyCertificateDetailModule),
      },

      {
        path: 'LawSuitList',
        loadChildren: () =>
          import('./law-suit-list/law-suit-list.module').then(
            (m) => m.LawSuitListModule
          ),
      },

      {
        path: 'LawSuit/:ID_CurrentObject/:configOptions',
        loadChildren: () =>
          import('./law-suit-detail/law-suit-detail.module').then(
            (m) => m.LawSuitDetailModule
          ),
      },

      {
        path: 'DisputeList',
        loadChildren: () =>
          import('./dispute-list/dispute-list.module').then(
            (m) => m.DisputeListModule
          ),
      },

      {
        path: 'Dispute/:ID_CurrentObject/:configOptions',
        loadChildren: () =>
          import('./dispute-detail/dispute-detail.module').then(
            (m) => m.DisputeDetailModule
          ),
      },

      {
        path: 'ResidentList',
        loadChildren: () =>
          import('./resident-list/resident-list.module').then(
            (m) => m.ResidentListModule
          ),
      },

      {
        path: 'ResidentList/:configOptions',
        loadChildren: () =>
          import('./resident-list/resident-list.module').then(
            (m) => m.ResidentListModule
          ),
      },

      {
        path: 'Resident/:ID_CurrentObject/:configOptions',
        loadChildren: () =>
          import('./resident-detail/resident-detail.module').then(
            (m) => m.ResidentDetailModule
          ),
      },

      {
        path: 'Report/:configOptions',
        loadChildren: () =>
          import('./report/report.module').then((m) => m.ReportModule),
      },

      {
        path: 'Report/:reportname/:configOptions',
        loadChildren: () =>
          import('./report/report.module').then((m) => m.ReportModule),
      },

      {
        path: 'Dashboard',
        loadChildren: () =>
          import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
      },

      {
        path: 'ProgramaPantawidPamilyaList',
        loadChildren: () =>
          import(
            './programa-pantawid-pamilya-list/programa-pantawid-pamilya-list.module'
          ).then((m) => m.ProgramaPantawidPamilyaListModule),
      },

      {
        path: 'ProgramaSeniorCitizenList',
        loadChildren: () =>
          import(
            './programa-senior-citizen-list/programa-senior-citizen-list.module'
          ).then((m) => m.ProgramaSeniorCitizenListModule),
      },

      {
        path: 'ProgramaSoloParentList',
        loadChildren: () =>
          import(
            './programa-solo-parent-list/programa-solo-parent-list.module'
          ).then((m) => m.ProgramaSoloParentListModule),
      },

      {
        path: 'ProgramaEducationalFinancialAssistanceList',
        loadChildren: () =>
          import(
            './programa-educational-financial-assistance-list/programa-educational-financial-assistance-list.module'
          ).then((m) => m.ProgramaEducationalFinancialAssistanceListModule),
      },

      {
        path: 'ProgramaPwdList',
        loadChildren: () =>
          import('./programa-pwd-list/programa-pwd-list.module').then(
            (m) => m.ProgramaPwdListModule
          ),
      },

      {
        path: 'ResidentReport',
        loadChildren: () =>
          import('./report-resident/report-resident.module').then(
            (m) => m.ReportResidentModule
          ),
      },

      {
        path: 'ResidentProgramReport',
        loadChildren: () =>
          import('./report-programs/report-programs.module').then(
            (m) => m.ReportProgramsModule
          ),
      },

      {
        path: 'Resident_BusinessList',
        loadChildren: () =>
          import('./business-list/business-list.module').then(
            (m) => m.BusinessListModule
          ),
      },

      {
        path: 'Resident_Business/:ID_CurrentObject/:configOptions',
        loadChildren: () =>
          import('./business-detail/business-detail.module').then(
            (m) => m.BusinessDetailModule
          ),
      },

      {
        path: 'BarangayClearanceList',
        loadChildren: () =>
          import(
            './barangay-clearance-list/barangay-clearance-list.module'
          ).then((m) => m.BarangayClearanceListModule),
      },

      {
        path: 'BarangayClearance/:ID_CurrentObject/:configOptions',
        loadChildren: () =>
          import(
            './barangay-clearance-detail/barangay-clearance-detail.module'
          ).then((m) => m.BarangayClearanceDetailModule),
      },

      {
        path: 'CertificateOfIndigencyList',
        loadChildren: () =>
          import(
            './certificate-of-indigency-list/certificate-of-indigency-list.module'
          ).then((m) => m.CertificateOfIndigencyListModule),
      },

      {
        path: 'CertificateOfIndigency/:ID_CurrentObject/:configOptions',
        loadChildren: () =>
          import(
            './certificate-of-indigency-detail/certificate-of-indigency-detail.module'
          ).then((m) => m.CertificateOfIndigencyDetailModule),
      },

      {
        path: 'CertificateOfResidencyList',
        loadChildren: () =>
          import(
            './certificate-of-residency-list/certificate-of-residency-list.module'
          ).then((m) => m.CertificateOfResidencyListModule),
      },

      {
        path: 'CertificateOfResidency/:ID_CurrentObject/:configOptions',
        loadChildren: () =>
          import(
            './certificate-of-residency-detail/certificate-of-residency-detail.module'
          ).then((m) => m.CertificateOfResidencyDetailModule),
      },

      {
        path: 'VotersReport',
        loadChildren: () =>
          import('./report-voters/report-voters.module').then(
            (m) => m.ReportVotersModule
          ),
      },
      {
        path: 'SoloParentList',
        loadChildren: () =>
          import('./solo-parent-list/solo-parent-list.module').then(
            (m) => m.SoloParentListModule
          ),
      },
      {
        path: 'SoloParent/:ID_CurrentObject/:configOptions',
        loadChildren: () =>
          import('./solo-parent-detail/solo-parent-detail.module').then(
            (m) => m.SoloParentDetailModule
          ),
      },

      {
        path: 'CertificationOfNoIncomeList',
        loadChildren: () =>
          import(
            './certification-of-no-income-list/certification-of-no-income-list.module'
          ).then((m) => m.CertificationOfNoIncomeListModule),
      },

      {
        path: 'CertificationOfNoIncome/:ID_CurrentObject/:configOptions',
        loadChildren: () =>
          import(
            './certification-of-no-income-detail/certification-of-no-income-detail.module'
          ).then((m) => m.CertificationOfNoIncomeDetailModule),
      },

      {
        path: 'ProgramaListahanan',
        loadChildren: () =>
          import(
            './programa-listahanan-list/programa-listahanan-list.module'
          ).then((m) => m.ProgramaListahananListModule),
      },

      {
        path: 'ProgramaListahananList',
        loadChildren: () =>
          import(
            './programa-listahanan-list/programa-listahanan-list.module'
          ).then((m) => m.ProgramaListahananListModule),
      },

      {
        path: 'Program/:ID_CurrentObject/:configOptions',
        loadChildren: () =>
          import('./program-detail/program-detail.module').then(
            (m) => m.ProgramDetailModule
          ),
      },

      {
        path: 'ResidentListVoter',
        loadChildren: () =>
          import('.//resident-list-voter/resident-list-voter.module').then(
            (m) => m.ResidentListVoterModule
          ),
      },

      {
        path: 'ResidentListSeniorCitizen',
        loadChildren: () =>
          import(
            './resident-list-senior-citizen/resident-list-senior-citizen.module'
          ).then((m) => m.ResidentListSeniorCitizenModule),
      },

      {
        path: 'ResidentListSoloParent',
        loadChildren: () =>
          import(
            './resident-list-solo-parent/resident-list-solo-parent.module'
          ).then((m) => m.ResidentListSoloParentModule),
      },
      {
        path: 'ResidentListOFW',
        loadChildren: () =>
          import('./resident-list-ofw/resident-list-ofw.module').then(
            (m) => m.ResidentListOFWModule
          ),
      },
      {
        path: 'ResidentListBoarder',
        loadChildren: () =>
          import('./resident-list-boarder/resident-list-boarder.module').then(
            (m) => m.ResidentListBoarderModule
          ),
      },

      {
        path: 'ResidentListPWD',
        loadChildren: () =>
          import('./resident-list-pwd/resident-list-pwd.module').then(
            (m) => m.ResidentListPWDModule
          ),
      },
      {
        path: 'ResidentListDeceased',
        loadChildren: () =>
          import('./resident-list-deceased/resident-list-deceased.module').then(
            (m) => m.ResidentListDeceasedModule
          ),
      },

      {
        path: 'ProgramaGSISList',
        loadChildren: () =>
          import('./programa-gsis-list/programa-gsis-list.module').then(
            (m) => m.ProgramaGSISListModule
          ),
      },

      {
        path: 'ProgramaSSSList',
        loadChildren: () =>
          import('./programa-sss-list/programa-sss-list.module').then(
            (m) => m.ProgramaSSSListModule
          ),
      },

      {
        path: 'ProgramaPhiiHealthList',
        loadChildren: () =>
          import(
            './programa-phii-health-list/programa-phii-health-list.module'
          ).then((m) => m.ProgramaPhiiHealthListModule),
      },
      {
        path: 'ResidentListHeadOfFamily',
        loadChildren: () =>
          import(
            './resident-list-head-of-family/resident-list-head-of-family.module'
          ).then((m) => m.ResidentListHeadOfFamilyModule),
      },
      {
        path: 'HouseholdList',
        loadChildren: () =>
          import('./household-list/household-list.module').then(
            (m) => m.HouseholdListModule
          ),
      },
      {
        path: 'HouseholdNumber/:ID_CurrentObject/:configOptions',
        loadChildren: () =>
          import(
            './household-number-detail/household-number-detail.module'
          ).then((m) => m.HouseholdNumberDetailModule),
      },
      {
        path: 'ResidentDisasterRiskReport',
        loadChildren: () =>
          import(
            './report-resident-disaster-risk/report-resident-disaster-risk.module'
          ).then((m) => m.ReportResidentDisasterRiskModule),
      },
      {
        path: 'ResidentHeadOfTheFamilyDetail',
        loadChildren: () =>
          import(
            './resident-head-of-the-family-detail/resident-head-of-the-family-detail.module'
          ).then((m) => m.ResidentHeadOfTheFamilyDetailModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BarangayRoutingModule {}
