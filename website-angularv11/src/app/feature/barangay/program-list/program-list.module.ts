import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProgramListRoutingModule } from './program-list-routing.module';
import { ProgramListComponent } from './program-list.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [ProgramListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    ProgramListRoutingModule,
  ],
})
export class ProgramListModule {}
