import { Component } from '@angular/core';
import { Item } from 'src/shared/APP_MODELS';
import { BaseListViewComponent } from '../../base-list-view/base-list-view.component';

@Component({
  selector: 'app-program-list',
  templateUrl: './program-list.component.html',
  styleUrls: [
    './../../base-list-view/base-list-view.component.less',
    './program-list.component.less',
  ],
})
export class ProgramListComponent extends BaseListViewComponent {
  headerTitle: string = 'Listahanan';
  OrderByString: string = 'Name';

  CurrentObject: any = {
    Name: '',
  };

  dataSource: any[] = [];

  async ListView_Onload() {
    await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT
              *
            FROM vProgram_Listview
            /*encryptsqlend*/
            WHERE
              ID_Company IN (${this.currentUser.ID_Company}, 1) AND
              IsActive = 1
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.Name.length > 0) {
      filterString += `Name LIKE '%${this.CurrentObject.Name}%'`;
    }

    return filterString;
  }

  Row_OnClick(rowData: Item) {
    this.customNavigate(['Program', rowData.ID], {});
  }

  menuItem_New_onClick() {
    this.customNavigate(['Program', -1], {});
  }
}
