import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HouseholdListComponent } from './household-list.component';

const routes: Routes = [{ path: '', component: HouseholdListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HouseholdListRoutingModule { }
