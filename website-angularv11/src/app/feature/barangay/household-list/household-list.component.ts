import { Component, ViewChild } from '@angular/core';
import { Item } from 'src/shared/APP_MODELS';
import { BaseListViewComponent } from '../../base-list-view/base-list-view.component';

@Component({
  selector: 'app-household-list',
  templateUrl: './household-list.component.html',
  styleUrls: [
    './../../base-list-view/base-list-view.component.less',
    './household-list.component.less',
  ],
})
export class HouseholdListComponent extends BaseListViewComponent {
  headerTitle: string = 'Household';
  OrderByString: string = 'Name';

  CurrentObject: any = {
    Name: '',
    AssignedBHW_Name_Employee: '',
  };

  dataSource: any[] = [];

  async ListView_Onload() {
    await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT
              *
            FROM vHouseholdNumber_Listview
            /*encryptsqlend*/
            WHERE
              ID_Company IN (${this.currentUser.ID_Company})
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.Name.length > 0) {
      filterString += `Name LIKE '%${this.CurrentObject.Name}%'`;
    }

    if (this.CurrentObject.AssignedBHW_Name_Employee.length > 0) {
      filterString += `AssignedBHW_Name_Employee LIKE '%${this.CurrentObject.AssignedBHW_Name_Employee}%'`;
    }

    return filterString;
  }

  HouseholdInfo_OnClick(rowData: Item) {
    this.customNavigate(['HouseholdNumber', rowData.ID], {});
  }

  ResidentCount_OnClick(rowData: Item) {
    this.customNavigate(['ResidentList'], {
      customFilter:
        'ID_HouseholdNumber = ' + rowData.ID + ' AND IsHeadOfFamily = 1 ',
      headerTitle: 'Household No. ' + rowData.Name + ' - Residents',
      ModelName: 'HouseholdNumber',
      ID_HouseholdNumber: rowData.ID,
      Name_HouseholdNumber: rowData.Name,
    });
  }

  menuItem_New_onClick() {
    this.customNavigate(['HouseholdNumber', -1], {});
  }
}
