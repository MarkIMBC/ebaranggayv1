import { SharedModule } from './../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HouseholdListRoutingModule } from './household-list-routing.module';
import { HouseholdListComponent } from './household-list.component';

@NgModule({
  declarations: [HouseholdListComponent],
  imports: [CommonModule, SharedModule, HouseholdListRoutingModule],
})
export class HouseholdListModule {}
