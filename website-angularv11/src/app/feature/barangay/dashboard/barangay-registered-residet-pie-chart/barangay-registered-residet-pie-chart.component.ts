import {
  Component,
  ChangeDetectionStrategy,
  OnInit,
  ElementRef,
  Input,
  TemplateRef,
} from '@angular/core';
import { Router } from '@angular/router';
import * as Chart from 'chart.js';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { ToastService } from 'src/app/shared/toast.service';

@Component({
  selector: 'barangay-registered-residet-pie-chart',
  templateUrl: './barangay-registered-residet-pie-chart.component.html',
  styleUrls: ['./barangay-registered-residet-pie-chart.component.less'],
})
export class BarangayRegisteredResidetPieChartComponent implements OnInit {
  currentUser: TokenSessionFields = new TokenSessionFields();
  dashboardinfochart: any;
  myChart: any;
  dataSource: any;
  ServiceCategories: Array<string> = [];
  ServiceCount: Array<number> = [];
  chartLabel: string = '';

  constructor(
    protected router: Router,
    protected elRef: ElementRef,
    protected ds: DataService,
    protected userAuth: UserAuthenticationService,
    protected toastService: ToastService,
    protected cs: CrypterService
  ) {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  @Input() customBodyTemplate!: TemplateRef<any>;

  async ngOnInit() {
    await this.loadRecords();
    this.myChart = new Chart('ServicesChart', {
      type: 'pie',

      data: {
        labels: this.ServiceCategories,
        datasets: [
          {
            label: this.chartLabel,
            data: this.ServiceCount,
            backgroundColor: [
              '#82C272',
              '#00A88F',
              '#0087AC',
              '#005FAA',
              '#323B81',
              '#62BEB6',
              '#034D44',
              '#62BEB6',
              '#034D44',
            ],
            borderColor: [
              '#82C272',
              '#00A88F',
              '#0087AC',
              '#005FAA',
              '#323B81',
              '#62BEB6',
              '#034D44',
              '#62BEB6',
              '#034D44',
            ],
            borderWidth: 1,
          },
        ],
      },

      options: {
        scales: {
          // y: {
          //     beginAtZero: true
          // }
        },
      },
    });

    this.myChart.update();
  }

  async loadRecords() {
    var obj = await this.ds.execSP(
      'pGetDashboardBarangayRegisteredResidentDataSource',
      {
        ID_UserSession: this.currentUser.ID_UserSession,
        DateYear: '2022',
      },
      {
        isReturnObject: true,
      }
    );

    this.dataSource = obj.DataSource;
    this.chartLabel = obj.Label;

    this.dataSource.forEach((obj: { Count: number }) => {
      this.ServiceCount.push(obj.Count);
    });
    this.dataSource.forEach((obj: { Name: string }) => {
      this.ServiceCategories.push(obj.Name);
    });
  }
}
