import {
  Component,
  OnInit,
  Input,
  TemplateRef,
  ElementRef,
} from '@angular/core';
import { Chart } from 'chart.js';
import { Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { ToastService } from 'src/app/shared/toast.service';

@Component({
  selector: 'total-vaccinated-resident-bar-chart',
  templateUrl: './total-vaccinated-resident-bar-chart.component.html',
  styleUrls: ['./total-vaccinated-resident-bar-chart.component.less']
})
export class TotalVaccinatedResidentBarChartComponent implements OnInit {
  currentUser: TokenSessionFields = new TokenSessionFields();
  dashboardinfochart: any;
  myChart: any;
  dataSource: any;
  chartLabel: string = '';
  MonthlyClient: Array<number> = [];
  constructor(
    protected router: Router,
    protected elRef: ElementRef,
    protected ds: DataService,
    protected userAuth: UserAuthenticationService,
    protected toastService: ToastService,
    protected cs: CrypterService
  ) {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  @Input() customBodyTemplate!: TemplateRef<any>;

  async ngOnInit() {
    await this.loadRecords();

    const data = {
      labels: [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
      ],
      datasets: [
        {
          label: 'Total Vaccinated Individuals',
          data: this.MonthlyClient,
          fill: false,
          borderColor: '#62BEB6',
          backgroundColor: '#62BEB6',
        },
      ],
    };
    this.myChart = new Chart('MonthlyClientChart1', {
      type: 'line',
      data: data,
      options: {
        responsive: true,
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true,
                callback: function (value, index, values) {
                  return value.toLocaleString();
                },
              },
            },
          ],
        },
        plugins: {
          legend: {
            position: 'top',
          },

          title: {
            display: true,
          },
        },
      },
    });

    this.myChart.update();
  }

  async loadRecords() {
    var obj = await this.ds.execSP(
      'pGetDashboardBarangayMonthlyVaccinatedResidentDataSource',
      {
        ID_UserSession: this.currentUser.ID_UserSession,
        DateYear: '2022',
      },
      {
        isReturnObject: true,
      }
    );

    this.dataSource = obj.DataSource;
    this.chartLabel = obj.Label;

    this.MonthlyClient.push(
      this.dataSource[0].January,
      this.dataSource[0].February,
      this.dataSource[0].March,
      this.dataSource[0].April,
      this.dataSource[0].May,
      this.dataSource[0].June,
      this.dataSource[0].July,
      this.dataSource[0].August,
      this.dataSource[0].September,
      this.dataSource[0].October,
      this.dataSource[0].November,
      this.dataSource[0].December
    );
  }
}
