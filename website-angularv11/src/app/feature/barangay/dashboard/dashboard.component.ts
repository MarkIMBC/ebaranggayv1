import { BaseViewComponent } from './../../base-view/base-view.component';
import { Component, OnInit } from '@angular/core';
import { DashboardBarangaySummary } from 'src/shared/APP_HELPER';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less'],
})
export class DashboardComponent extends BaseViewComponent {
  residentSummary: DashboardBarangaySummary = new DashboardBarangaySummary();
  async BaseView_OnInit() {
    await this.loadRecords();
  }

  async loadRecords() {
    var obj = await this.ds.execSP(
      'pGetDashboardBarangaySummary',
      {
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
      }
    );

    this.residentSummary = obj;
  }
}
