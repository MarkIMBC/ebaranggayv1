import {
  Component,
  ChangeDetectionStrategy,
  OnInit,
  ElementRef,
  Input,
  TemplateRef,
} from '@angular/core';
import { Router } from '@angular/router';
import * as Chart from 'chart.js';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { ToastService } from 'src/app/shared/toast.service';

@Component({
  selector: 'resident-occupation-status-pie-chart',
  templateUrl: './resident-occupation-status-pie-chart.component.html',
  styleUrls: ['./resident-occupation-status-pie-chart.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResidentOccupationStatusPieChartComponent implements OnInit {
  currentUser: TokenSessionFields = new TokenSessionFields();
  dashboardinfochart: any;
  myChart: any;
  dataSource: any;
  ServiceCategories: Array<string> = [];
  ServiceCount: Array<number> = [];
  chartLabel: string = '';

  constructor(
    protected router: Router,
    protected elRef: ElementRef,
    protected ds: DataService,
    protected userAuth: UserAuthenticationService,
    protected toastService: ToastService,
    protected cs: CrypterService
  ) {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  @Input() customBodyTemplate!: TemplateRef<any>;

  async ngOnInit() {
    var _ = this;

    await this.loadRecords();
    this.myChart = new Chart('ResidentOccupationStatusPieChartComponent', {
      type: 'pie',
      data: {
        labels: this.ServiceCategories,
        datasets: [
          {
            label: 'May Mga Trabaho',
            data: this.ServiceCount,
            backgroundColor: [
              '#82C272',
              '#00A88F',
              '#0087AC',
              '#005FAA',
              '#323B81',
              '#62BEB6',
              '#034D44',
              '#62BEB6',
              '#034D44',
            ],
            borderColor: [
              '#82C272',
              '#00A88F',
              '#0087AC',
              '#005FAA',
              '#323B81',
              '#62BEB6',
              '#034D44',
              '#62BEB6',
              '#034D44',
            ],
            borderWidth: 1,
          },
        ],
      },

      options: {
        scales: {
          // y: {
          //     beginAtZero: true
          // }
        },
      },
    });

    var canvas = document.getElementById(
      'ResidentOccupationStatusPieChartComponent'
    );

    var chart = this.myChart;

    if (canvas) {
      canvas.onclick = function (evt: any) {
        var activePoints = chart.getElementsAtEvent(evt);
        console.log(evt);
        if (activePoints[0]) {
          var chartData = activePoints[0]['_chart'].config.data;
          var idx = activePoints[0]['_index'];

          var label = chartData.labels[idx];
          var fllterName_OccupationalStatus = chartData.labels[idx];

          if (fllterName_OccupationalStatus == 'Walang nakatalaga') fllterName_OccupationalStatus = '';

          GeneralfxService.customNavigate(
            _.router,
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              customFilter:
                "ISNULL(Name_OccupationalStatus, '') = '" + fllterName_OccupationalStatus + "'",
              headerTitle: 'May Mga Trabaho (' + label + ')',
              ModelName: 'OccupationalStatus',
            }
          );
        }
      };
    }

    this.myChart.update();
  }

  async loadRecords() {
    var obj = await this.ds.execSP(
      'pGetDashboardResidentOccupationStatusPieChart',
      {
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
      }
    );

    this.dataSource = obj.DataSource;
    this.chartLabel = obj.Label;

    this.dataSource.forEach((obj: { Count: number }) => {
      this.ServiceCount.push(obj.Count);
    });
    this.dataSource.forEach((obj: { Name: string }) => {
      this.ServiceCategories.push(obj.Name);
    });
  }
}
