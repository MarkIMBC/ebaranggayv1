import {
  Component,
  OnInit,
  Input,
  TemplateRef,
  ElementRef,
  ChangeDetectionStrategy,
} from '@angular/core';
import { Chart } from 'chart.js';
import { Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { ToastService } from 'src/app/shared/toast.service';
import { ThrowStmt } from '@angular/compiler';
import { GeneralfxService } from 'src/app/core/generalfx.service';

@Component({
  selector: 'resident-religion-bar-chart',
  templateUrl: './resident-religion-bar-chart.component.html',
  styleUrls: ['./resident-religion-bar-chart.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResidentReligionBarChartComponent implements OnInit {
  currentUser: TokenSessionFields = new TokenSessionFields();
  dashboardinfochart: any;
  myChart: any;
  dataSource: any;
  chartLabel: string = '';
  Labels: Array<string> = [];
  data: Array<number> = [];
  constructor(
    protected router: Router,
    protected elRef: ElementRef,
    protected ds: DataService,
    protected userAuth: UserAuthenticationService,
    protected toastService: ToastService,
    protected cs: CrypterService
  ) {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  @Input() customBodyTemplate!: TemplateRef<any>;

  async ngOnInit() {
    var _ = this;

    await this.loadRecords();

    const data = {
      labels: this.Labels,
      datasets: [
        {
          label: this.chartLabel,
          data: this.data,
          fill: false,
          borderColor: '#62BEB6',
          backgroundColor: '#62BEB6',
        },
      ],
    };
    this.myChart = new Chart('ResidentReligionBarChart', {
      type: 'bar',
      data: data,
      options: {
        responsive: true,
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true,
                callback: function (value, index, values) {
                  return value.toLocaleString();
                },
              },
            },
          ],
        },
        plugins: {
          legend: {
            position: 'top',
          },

          title: {
            display: true,
          },
        },
      },
    });

    var canvas = document.getElementById('ResidentReligionBarChart');

    var chart = this.myChart;

    if (canvas) {
      canvas.onclick = function (evt: any) {
        var activePoints = chart.getElementsAtEvent(evt);
       
        if (activePoints[0]) {
          var chartData = activePoints[0]['_chart'].config.data;
          var idx = activePoints[0]['_index'];

          var label = chartData.labels[idx];
          var fllterName = chartData.labels[idx];

          GeneralfxService.customNavigate(
            _.router,
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              customFilter: "ISNULL(Name_Religion, '') = '" + fllterName + "'",
              headerTitle: 'Relihiyon (' + label + ')',
              ModelName: 'Religion',
            }
          );
        }
      };
    }

    this.myChart.update();
  }

  async loadRecords() {
    var obj = await this.ds.execSP(
      'pGetDashboardResidentReligionDataSource',
      {
        ID_UserSession: this.currentUser.ID_UserSession,
        DateYear: '2022',
      },
      {
        isReturnObject: true,
      }
    );

    this.dataSource = obj.DataSource;
    this.chartLabel = obj.Label;

    this.Labels = [];
    this.data = [];

    this.dataSource.forEach((record: any) => {
      this.Labels.push(record.Name_Religion);
      this.data.push(record.Count);
    });
  }
}
