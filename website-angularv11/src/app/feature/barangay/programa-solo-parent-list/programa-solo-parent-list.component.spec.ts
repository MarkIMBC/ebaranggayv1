import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramaSoloParentListComponent } from './programa-solo-parent-list.component';

describe('ProgramaSoloParentListComponent', () => {
  let component: ProgramaSoloParentListComponent;
  let fixture: ComponentFixture<ProgramaSoloParentListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProgramaSoloParentListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramaSoloParentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
