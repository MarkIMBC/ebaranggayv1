import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProgramaSoloParentListRoutingModule } from './programa-solo-parent-list-routing.module';
import { ProgramaSoloParentListComponent } from './programa-solo-parent-list.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [ProgramaSoloParentListComponent],
  imports: [
    CommonModule,
    ProgramaSoloParentListRoutingModule,
    SharedModule
  ]
})
export class ProgramaSoloParentListModule { }
