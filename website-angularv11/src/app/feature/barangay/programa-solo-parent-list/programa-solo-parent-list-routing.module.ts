import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProgramaSoloParentListComponent } from './programa-solo-parent-list.component';

const routes: Routes = [{ path: '', component: ProgramaSoloParentListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgramaSoloParentListRoutingModule { }
