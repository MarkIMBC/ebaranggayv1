import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CertificateOfIndigencyListRoutingModule } from './certificate-of-indigency-list-routing.module';
import { CertificateOfIndigencyListComponent } from './certificate-of-indigency-list.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [CertificateOfIndigencyListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    CertificateOfIndigencyListRoutingModule
  ]
})
export class CertificateOfIndigencyListModule { }
