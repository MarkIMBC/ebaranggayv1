import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CertificateOfIndigencyListComponent } from './certificate-of-indigency-list.component';

describe('CertificateOfIndigencyListComponent', () => {
  let component: CertificateOfIndigencyListComponent;
  let fixture: ComponentFixture<CertificateOfIndigencyListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CertificateOfIndigencyListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificateOfIndigencyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
