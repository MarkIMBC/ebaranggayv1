import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CertificateOfIndigencyListComponent } from './certificate-of-indigency-list.component';

const routes: Routes = [{ path: '', component: CertificateOfIndigencyListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CertificateOfIndigencyListRoutingModule { }
