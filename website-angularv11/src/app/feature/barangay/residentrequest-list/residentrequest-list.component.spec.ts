import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidentRequestListComponent } from './residentrequest-list.component';

describe('ResidentRequestListComponent', () => {
  let component: ResidentRequestListComponent;
  let fixture: ComponentFixture<ResidentRequestListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResidentRequestListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidentRequestListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
