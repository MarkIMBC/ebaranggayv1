import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';

import { ResidentRequestListRoutingModule } from './residentrequest-list-routing.module';
import { ResidentRequestListComponent } from './residentrequest-list.component';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';

@NgModule({
  declarations: [ResidentRequestListComponent],
  imports: [
    CommonModule,
    SharedModule,
    ResidentRequestListRoutingModule
  ],
  providers: [
    DataService,
    CrypterService,
  ],
})
export class ResidentRequestListModule { }
