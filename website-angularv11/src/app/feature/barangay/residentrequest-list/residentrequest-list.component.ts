import { Component } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum, InventoryStatusEnum } from 'src/shared/APP_HELPER';
import { ResidentRequest, Item } from 'src/shared/APP_MODELS';
import { BaseListViewComponent } from '../../base-list-view/base-list-view.component';

@Component({
  selector: 'app-residentrequest-list',
  templateUrl: './residentrequest-list.component.html',
  styleUrls: [
    './../../base-list-view/base-list-view.component.less',
    './residentrequest-list.component.less',
  ],
})
export class ResidentRequestListComponent extends BaseListViewComponent {
  headerTitle: string = 'Mga Request';

  CurrentObject: any = {
    Name: '',
    Name_ItemCategory: '',
    ID_InventoryStatus: 0,
    Name_InventoryStatus: '',
  };

  dataSource: ResidentRequest[] = [];

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {
        return true;
      },
    },
    {
      label: 'ResidentRequest',
      visible: true,
      isActive: true,
      command: () => {
        return true;
      },
    },
  ];

  ID_InventoryStatus_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: `
        SELECT '-1' ID, 'Available Stock' Name
        UNION ALL
        Select
          ID, Name
        FROM tInventoryStatus
        WHERE
          ID IN (1, 2)
      `,
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  async ListView_Onload() {
    await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT
                *
            FROM vResidentRequest_ListvIew
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.Name.length > 0) {
      filterString += `Name LIKE '%${this.CurrentObject.Name}%'`;
    }

    if (this.CurrentObject['ID_InventoryStatus']) {
      if (this.CurrentObject['ID_InventoryStatus'] == -1) {
        if (filterString.length > 0) filterString += ' AND ';
        filterString += `ISNULL(ID_InventoryStatus, 0) IN (
          ${InventoryStatusEnum.High},
          ${InventoryStatusEnum.Medium}
        )`;
      } else {
        if (filterString.length > 0) filterString += ' AND ';
        filterString += `ISNULL(ID_InventoryStatus, 0) = ${this.CurrentObject['ID_InventoryStatus']} `;
      }
    }

    return filterString;
  }

  Row_OnClick(rowData: Item) {
    this.customNavigate(['ResidentRequest', rowData.ID], {});
  }

  menuItem_New_onClick() {
    this.customNavigate(['ResidentRequest', -1], {});
  }
}
