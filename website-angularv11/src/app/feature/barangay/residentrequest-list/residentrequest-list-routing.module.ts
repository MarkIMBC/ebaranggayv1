import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResidentRequestListComponent } from './residentrequest-list.component';

const routes: Routes = [{ path: '', component: ResidentRequestListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResidentRequestListRoutingModule { }
