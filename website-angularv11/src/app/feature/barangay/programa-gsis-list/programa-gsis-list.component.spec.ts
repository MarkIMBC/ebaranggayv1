import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramaGSISListComponent } from './programa-gsis-list.component';

describe('ProgramaGSISListComponent', () => {
  let component: ProgramaGSISListComponent;
  let fixture: ComponentFixture<ProgramaGSISListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProgramaGSISListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramaGSISListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
