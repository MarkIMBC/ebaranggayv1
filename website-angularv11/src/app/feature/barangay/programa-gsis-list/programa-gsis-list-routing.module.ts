import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProgramaGSISListComponent } from './programa-gsis-list.component';

const routes: Routes = [{ path: '', component: ProgramaGSISListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgramaGSISListRoutingModule { }
