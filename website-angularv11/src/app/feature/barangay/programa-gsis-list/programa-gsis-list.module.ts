import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProgramaGSISListRoutingModule } from './programa-gsis-list-routing.module';
import { ProgramaGSISListComponent } from './programa-gsis-list.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ProgramaGSISListComponent],
  imports: [
    CommonModule,SharedModule,
    ProgramaGSISListRoutingModule
  ]
})
export class ProgramaGSISListModule { }
