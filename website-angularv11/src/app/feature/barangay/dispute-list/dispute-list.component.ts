
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { Item_DTO } from 'src/shared/APP_HELPER';
import { Item } from 'src/shared/APP_MODELS';
import { BaseListViewComponent } from '../../base-list-view/base-list-view.component';

@Component({
  selector: 'app-dispute-list',
  templateUrl: './dispute-list.component.html',
  styleUrls: [
    './../../base-list-view/base-list-view.component.less',
    './dispute-list.component.less'
  ],
})
export class DisputeListComponent extends BaseListViewComponent {

  headerTitle: string = 'Mga Hindi Pagkakaunawaan';

  CurrentObject: any = {
    Name: '',
    Name_ItemCategory: ''
  }

  dataSource: Item_DTO[] = []

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {


        return true;
      }
    },
    {
      label: 'ItemService',
      isActive: true,
      visible: true,
      command: () => {


        return true;
      }
    }
  ]

  async ListView_Onload() {

    await this.loadRecords()
  }

  async loadRecords() {

    var sql = "";
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `/*encryptsqlstart*/
            SELECT
              *
            FROM vDispute_Listview
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company} AND
              IsActive = 1
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {

    var filterString = "";

    if (this.CurrentObject.Name.length > 0) {

      filterString += `Name LIKE '%${this.CurrentObject.Name}%'`;
    }

    return filterString;
  }

  Row_OnClick(rowData: Item) {

    this.customNavigate(['Dispute', rowData.ID], {});
  }

  menuItem_New_onClick() {

    this.customNavigate(['Dispute', -1], {});
  }
}
