import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DisputeListRoutingModule } from './dispute-list-routing.module';
import { DisputeListComponent } from './dispute-list.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [DisputeListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    DisputeListRoutingModule
  ]
})
export class DisputeListModule { }
