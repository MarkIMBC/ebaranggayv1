import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DisputeListComponent } from './dispute-list.component';

const routes: Routes = [{ path: '', component: DisputeListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DisputeListRoutingModule { }
