import { BaseReportComponent } from '../../base-report/base-report.component';
import { Component, OnInit } from '@angular/core';
import { FilterCriteriaType, PropertyTypeEnum } from 'src/shared/APP_HELPER';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./../../base-report/base-report.component.less'],
})
export class ReportComponent extends BaseReportComponent {
  ReportName: string = '';
  dataSource: any[] = [];

  async onLoad() {
    // var sql = this.cs.encrypt(
    //   `/*encryptsqlstart*/
    //     SELECT *
    //     FROM dbo.vResidentDetailReportList
    //     /*encryptsqlend*/`
    // );

    // var _dataSource: string[] = [];

    // var objs = await this.ds.query<any>(sql);

    // this.dataSource = [];
    // objs.forEach(function (obj) {
    //   _dataSource.push(obj);
    // });

    // this.dataSource = _dataSource;
  }

  // async Row_OnClick(rowData: any) {
  //   var reportName = rowData.reportname;

  //   this.ReportName = reportName;

  //   this.customNavigate(['Report'], {
  //     ReportName: reportName,
  //     filterValues: [
  //       {
  //         dataField: 'ID',
  //         filterCriteriaType: FilterCriteriaType.Equal,
  //         propertyType: PropertyTypeEnum.Int,
  //         value: 15,
  //       },
  //     ],
  //   });

  //   await this.loadConfigOption();
  //   await this.loadReportInfo();
  //   await this.load();

  //   await this.loadConfigOption();
  //   await this.loadReportInfo();
  //   await this.load();
  // }

  // protected loadConfigOption() {
  //   var configOptionsString = this.route.snapshot.params['configOptions'];

  //   if (configOptionsString == undefined || configOptionsString == null) return;
  //   configOptionsString = this.cs.decrypt(configOptionsString);

  //   this.configOptions = JSON.parse(configOptionsString);

  //   console.log(this.configOptions);
  // }
}
