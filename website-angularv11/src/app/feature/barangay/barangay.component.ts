import { Component, OnInit } from '@angular/core';
import { BaseFeatureComponent } from '../base-feature/base-feature.component';

@Component({
  selector: 'app-barangay',
  templateUrl: './barangay.component.html',
  styleUrls: ['./barangay.component.less'],
})
export class BarangayComponent extends BaseFeatureComponent {}
