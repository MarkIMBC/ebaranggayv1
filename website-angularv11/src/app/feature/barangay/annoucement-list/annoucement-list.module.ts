import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AnnoucementListRoutingModule } from './annoucement-list-routing.module';
import { AnnoucementListComponent } from './annoucement-list.component';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [AnnoucementListComponent],
  imports: [
    CommonModule,
    SharedModule,
    AnnoucementListRoutingModule
  ],
  providers: [
    DataService,
    CrypterService,
  ],
})
export class AnnoucementListModule { }
