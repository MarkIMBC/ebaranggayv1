import { BaseListViewComponent } from '../../base-list-view/base-list-view.component';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';

@Component({
  selector: 'app-annoucement-list',
  templateUrl: './annoucement-list.component.html',
  styleUrls: [
    './../../base-list-view/base-list-view.component.less',
    './annoucement-list.component.less',
  ],
})
export class AnnoucementListComponent extends BaseListViewComponent {
  headerTitle: string = 'Mga Anunsyo';

  CurrentObject: any = {
    Name: '',
  };

  breadCrumbItems: AdminLTEMenuItem[] = [
    {
      label: 'Home',
      routerLink: ['../Home'],
      visible: true,
      command: () => {
        return true;
      },
    },
    {
      label: 'Annoucement',
      visible: true,
      isActive: true,
      command: () => {
        return true;
      },
    },
  ];

  async ListView_Onload() {
    await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT
                *
            FROM vAnnouncement_ListvIew
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company}
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = '';

    return filterString;
  }

  Row_OnClick(rowData: any) {

    var routerLink = 'Announcement';

    this.customNavigate([routerLink, rowData.ID], {});
  }

  menuItem_New_onClick() {
    this.customNavigate(['Announcement', -1], {});
  }
}
