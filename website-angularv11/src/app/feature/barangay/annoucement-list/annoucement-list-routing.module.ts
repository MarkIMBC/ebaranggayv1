import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnnoucementListComponent } from './annoucement-list.component';

const routes: Routes = [{ path: '', component: AnnoucementListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnnoucementListRoutingModule { }
