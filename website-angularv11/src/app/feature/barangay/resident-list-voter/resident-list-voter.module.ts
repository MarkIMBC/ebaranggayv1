import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResidentListVoterRoutingModule } from './resident-list-voter-routing.module';
import { ResidentListVoterComponent } from './resident-list-voter.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [ResidentListVoterComponent],
  imports: [CommonModule, SharedModule, ResidentListVoterRoutingModule],
})
export class ResidentListVoterModule {}
