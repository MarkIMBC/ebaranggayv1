import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResidentListVoterComponent } from './resident-list-voter.component';

const routes: Routes = [{ path: '', component: ResidentListVoterComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResidentListVoterRoutingModule { }
