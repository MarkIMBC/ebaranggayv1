import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportVotersComponent } from './report-voters.component';

const routes: Routes = [{ path: '', component: ReportVotersComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportVotersRoutingModule { }
