
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportVotersRoutingModule } from './report-voters-routing.module';
import { ReportVotersComponent } from './report-voters.component';

import { SharedModule } from './../../../shared/shared.module';
import { NgxSliderModule } from '@angular-slider/ngx-slider';

@NgModule({
  declarations: [ReportVotersComponent],
  imports: [
    CommonModule,
    ReportVotersRoutingModule,
    NgxSliderModule,
    SharedModule
  ]
})
export class ReportVotersModule { }


