import { Component } from '@angular/core';
import * as moment from 'moment';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import {
  FilterCriteriaType,
  IFilterFormValue,
  PropertyTypeEnum,
} from 'src/shared/APP_HELPER';
import { ReportComponent } from '../report/report.component';
import { Options } from '@angular-slider/ngx-slider';

@Component({
  selector: 'app-report-voters',
  templateUrl: './report-voters.component.html',
  styleUrls: ['./report-voters.component.less'],
})
export class ReportVotersComponent extends ReportComponent {
  configOptions: any = {
    ReportName: 'ResidentVoterListReport',
  };


  options: Options = {
    floor: 0,
    ceil: 120,
    selectionBarGradient: {
      from: 'white',
      to: 'teal'
    },
    getPointerColor: (value: number): string => {

      return 'teal';
  }
  };
  CurrentObject: any = {
    AgeStart: 0,
    AgeEnd: 120
   }

   ID_Gender_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM tGender',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  ID_Program_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: `/*encryptsqlstart*/
            SELECT
                  ID,
                  Name
            FROM dbo.vProgram
            /*encryptsqlend*/`,
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  protected getFilterValues(
    filterValues: IFilterFormValue[]
  ): IFilterFormValue[] {
    if (this.CurrentObject['Name']) {
      filterValues.push({
        dataField: 'Name',
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: this.CurrentObject['Name'],
      });
    }


    if (this.CurrentObject['ID_Gender']) {
      filterValues.push({
        dataField: 'ID_Gender',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Int,
        value: this.CurrentObject['ID_Gender'],
      });
    }

    if (this.CurrentObject['VoteInfoPrecinctNumber']) {
      filterValues.push({
        dataField: 'VoteInfoPrecinctNumber',
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: this.CurrentObject['VoteInfoPrecinctNumber'],
      });
    }

    if (this.CurrentObject['ContactNumber']) {
      filterValues.push({
        dataField: 'ContactNumber',
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: this.CurrentObject['ContactNumber'],
      });
    }

    filterValues.push({
      dataField: "Age",
      filterCriteriaType: FilterCriteriaType.GreaterThan,
      propertyType: PropertyTypeEnum.Int,
      value: [this.CurrentObject['AgeStart']]
    });

    filterValues.push({
      dataField: "Age",
      filterCriteriaType: FilterCriteriaType.LessThan,
      propertyType: PropertyTypeEnum.Int,
      value: [this.CurrentObject['AgeEnd']+1]
    });


    return filterValues;
  }


}
