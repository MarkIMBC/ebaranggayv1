import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResidentListPWDRoutingModule } from './resident-list-pwd-routing.module';
import { ResidentListPWDComponent } from './resident-list-pwd.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ResidentListPWDComponent],
  imports: [
    CommonModule,  SharedModule,
    ResidentListPWDRoutingModule
  ]
})
export class ResidentListPWDModule { }

