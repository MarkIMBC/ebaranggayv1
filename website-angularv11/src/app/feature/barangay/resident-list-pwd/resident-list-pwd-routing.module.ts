import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResidentListPWDComponent } from './resident-list-pwd.component';

const routes: Routes = [{ path: '', component: ResidentListPWDComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResidentListPWDRoutingModule { }
