import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidentListPWDComponent } from './resident-list-pwd.component';

describe('ResidentListPWDComponent', () => {
  let component: ResidentListPWDComponent;
  let fixture: ComponentFixture<ResidentListPWDComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResidentListPWDComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidentListPWDComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
