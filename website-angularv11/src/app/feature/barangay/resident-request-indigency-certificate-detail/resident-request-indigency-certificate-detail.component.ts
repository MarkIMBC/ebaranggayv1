import { ResidentRequest_IndigencyCertificate } from './../../../../shared/APP_MODELS';
import { Component } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import {
  FilterCriteriaType,
  IControlModelArg,
  PropertyTypeEnum,
} from 'src/shared/APP_HELPER';
import { APP_REPORTVIEW } from 'src/shared/APP_MODELS';
import { BaseDetailViewComponent } from '../../base-detail-view/base-detail-view.component';

@Component({
  selector: 'app-resident-request-indigency-certificate-detail',
  templateUrl: './resident-request-indigency-certificate-detail.component.html',
  styleUrls: [
    './../../base-detail-view/base-detail-view.component.less',
    './resident-request-indigency-certificate-detail.component.less',
  ],
})
export class ResidentRequestIndigencyCertificateDetailComponent extends BaseDetailViewComponent {
  ModelName: string = 'ResidentRequest';
  headerTitle: string = 'Indigency Certificate';
  displayMember: string = 'Name';
  routerFeatureName: string = 'ResidentRequest_IndigencyCertificate';

  currentResidentRequest_IndigencyCertificate: any =
    new ResidentRequest_IndigencyCertificate();

  loadInitMenuItem() {
    if (this.CurrentObject.IsActive == true) {
      this.menuItems.push(this._menuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  loadMenuItems() {
    var menuReport: AdminLTEMenuItem = {
      label: 'Report',
      icon: 'far fa-file',
      class: 'text-primary',
      visible: true,
      items: [],
    };

    var sendSMSReminderMenuItem = {
      label: 'Mag Send ng SMS Reminder',
      name: 'sendSMSReminder',
      icon: 'fas fa-sms',
      class: 'text-primary',
      visible: true,
    };

    var barangayBusinessClearanceMenuItem = {
      label: 'Certificate Of Indegency',
      name: APP_REPORTVIEW.RESIDENTREQUESTCERTIFICATEINDIGENCY,
      icon: 'far fa-file',
      class: 'text-primary',
      visible: true,
    };

    if (menuReport.items) {
      menuReport.items.push(barangayBusinessClearanceMenuItem);
    }

    if (this.CurrentObject.IsActive != true) return;

    this.addMenuItem(this._menuItem_Delete);

    if (this.CurrentObject.ID > 0) {
      this.addMenuItem(sendSMSReminderMenuItem);
      this.addMenuItem(menuReport);
    }
  }

  async menubar_OnClick(e: any) {
    if (e.item.name == APP_REPORTVIEW.RESIDENTREQUESTCERTIFICATEINDIGENCY) {
      this.customNavigate(['Report'], {
        ReportName: 'RESIDENTREQUESTCERTIFICATEINDIGENCY',
        filterValues: [
          {
            dataField: 'ID',
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.currentResidentRequest_IndigencyCertificate.ID,
          },
        ],
      });
    } else if (e.item.name == 'sendSMSReminder') {
      this.loading = true;

      if (this.CurrentObject.ContactNumber_ResidentRequest == null)
        this.CurrentObject.ContactNumber_ResidentRequest = '';

      var contactNumbers =
        this.CurrentObject.ContactNumber_ResidentRequest.split(',');

      var obj = {
        CellPhoneNumbers: contactNumbers,
        Message:
          "Magandang araw, ang iyong CERTIFICATE OF INDIGENCY na iyong nirequest ay natapos nang i-proseso at nakahanda na ito'y iyong kunin.",
      };

      var r = await this.ds.sendtext(obj);

      if (
        r == null ||
        r.PhoneNumberStatus == undefined ||
        r.PhoneNumberStatus == null
      ) {
        this.toastService.warning(
          'Ang SMS Reminder ay hindi nag tagumpay sa pagpapadala.'
        );
      } else {
        this.toastService.success(
          'Ang SMS reminder ay matagumpay na naipadala.'
        );
      }

      this.loading = false;
    }
  }

  DetailView_onLoad() {
    if (
      this.CurrentObject['ResidentRequest_IndigencyCertificate'] == null ||
      this.CurrentObject['ResidentRequest_IndigencyCertificate'] == undefined
    ) {
      this.CurrentObject['ResidentRequest_IndigencyCertificate'] = [];
    }

    if (
      this.CurrentObject['ResidentRequest_IndigencyCertificate'].length == 0
    ) {
      var obj = new ResidentRequest_IndigencyCertificate();
      obj.ID = this.getTempID();
      obj.ID_Company = this.currentUser.ID_Company;

      this.CurrentObject['ResidentRequest_IndigencyCertificate'].push(obj);
    }

    this.currentResidentRequest_IndigencyCertificate =
      this.CurrentObject['ResidentRequest_IndigencyCertificate'][0];

    if (this.currentResidentRequest_IndigencyCertificate.ID_Company == null) {
      this.currentResidentRequest_IndigencyCertificate.ID_Company =
        this.currentUser.ID_Company;
    }
  }

  currentResidentRequest_CommunityTaxCertificate_onModelChanged(
    e: IControlModelArg
  ) {
    this.currentResidentRequest_IndigencyCertificate[e.name] = e.value;

    if (e.displayName != undefined) {
      this.currentResidentRequest_IndigencyCertificate[e.displayName] =
        e.displayValue;
    }

    this.isDirty = true;
  }

  public CurrentObject_onBeforeSaving() {}

  protected pGetRecordOptions(): any {
    var options: any = {};
    var configKeys = ['ID_Resident', 'ID_ResidentRequest'];

    configKeys.forEach((key) => {
      if (
        this.configOptions[key] != undefined &&
        this.configOptions[key] != null
      ) {
        options[key] = this.configOptions[key];
      }
    });

    return options;
  }
}
