import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidentRequestIndigencyCertificateDetailComponent } from './resident-request-indigency-certificate-detail.component';

describe('ResidentRequestIndigencyCertificateDetailComponent', () => {
  let component: ResidentRequestIndigencyCertificateDetailComponent;
  let fixture: ComponentFixture<ResidentRequestIndigencyCertificateDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResidentRequestIndigencyCertificateDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidentRequestIndigencyCertificateDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
