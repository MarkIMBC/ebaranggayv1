import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResidentRequestIndigencyCertificateDetailRoutingModule } from './resident-request-indigency-certificate-detail-routing.module';
import { ResidentRequestIndigencyCertificateDetailComponent } from './resident-request-indigency-certificate-detail.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ResidentRequestIndigencyCertificateDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    ResidentRequestIndigencyCertificateDetailRoutingModule
  ]
})
export class ResidentRequestIndigencyCertificateDetailModule { }
