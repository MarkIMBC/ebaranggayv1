import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResidentRequestIndigencyCertificateDetailComponent } from './resident-request-indigency-certificate-detail.component';

const routes: Routes = [{ path: '', component: ResidentRequestIndigencyCertificateDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResidentRequestIndigencyCertificateDetailRoutingModule { }
