import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramaSeniorCitizenListComponent } from './programa-senior-citizen-list.component';

describe('ProgramaSeniorCitizenListComponent', () => {
  let component: ProgramaSeniorCitizenListComponent;
  let fixture: ComponentFixture<ProgramaSeniorCitizenListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProgramaSeniorCitizenListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramaSeniorCitizenListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
