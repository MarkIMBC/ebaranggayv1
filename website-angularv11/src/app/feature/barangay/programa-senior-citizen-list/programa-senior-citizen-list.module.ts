import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProgramaSeniorCitizenListRoutingModule } from './programa-senior-citizen-list-routing.module';
import { ProgramaSeniorCitizenListComponent } from './programa-senior-citizen-list.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ProgramaSeniorCitizenListComponent],
  imports: [
    CommonModule,
    ProgramaSeniorCitizenListRoutingModule,
    SharedModule,
  ]
})
export class ProgramaSeniorCitizenListModule { }
