import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProgramaSeniorCitizenListComponent } from './programa-senior-citizen-list.component';

const routes: Routes = [{ path: '', component: ProgramaSeniorCitizenListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgramaSeniorCitizenListRoutingModule { }
