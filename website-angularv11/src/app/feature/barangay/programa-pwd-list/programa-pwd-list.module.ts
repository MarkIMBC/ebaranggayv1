import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';

import { ProgramaPwdListRoutingModule } from './programa-pwd-list-routing.module';
import { ProgramaPwdListComponent } from './programa-pwd-list.component';


@NgModule({
  declarations: [ProgramaPwdListComponent],
  imports: [
    CommonModule,
    ProgramaPwdListRoutingModule,
    SharedModule
  ]
})
export class ProgramaPwdListModule { }
