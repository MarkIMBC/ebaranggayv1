import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProgramaPwdListComponent } from './programa-pwd-list.component';

const routes: Routes = [{ path: '', component: ProgramaPwdListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgramaPwdListRoutingModule { }
