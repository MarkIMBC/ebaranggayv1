import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramaPwdListComponent } from './programa-pwd-list.component';

describe('ProgramaPwdListComponent', () => {
  let component: ProgramaPwdListComponent;
  let fixture: ComponentFixture<ProgramaPwdListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProgramaPwdListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramaPwdListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
