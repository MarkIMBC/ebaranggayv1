import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HouseholdNumberResidentListComponent } from './household-number-resident-list.component';

describe('HouseholdNumberResidentListComponent', () => {
  let component: HouseholdNumberResidentListComponent;
  let fixture: ComponentFixture<HouseholdNumberResidentListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HouseholdNumberResidentListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HouseholdNumberResidentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
