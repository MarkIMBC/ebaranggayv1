import { SharedModule } from './../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HouseholdNumberResidentListRoutingModule } from './household-number-resident-list-routing.module';
import { HouseholdNumberResidentListComponent } from './household-number-resident-list.component';

@NgModule({
  declarations: [HouseholdNumberResidentListComponent],
  imports: [
    CommonModule,
    SharedModule,
    HouseholdNumberResidentListRoutingModule,
  ],
})
export class HouseholdNumberResidentListModule {}
