import { Component } from '@angular/core';
import { Item } from 'src/shared/APP_MODELS';
import { BaseListViewComponent } from '../../base-list-view/base-list-view.component';

@Component({
  selector: 'app-household-number-resident-list',
  templateUrl: './household-number-resident-list.component.html',
  styleUrls: [
    './../../base-list-view/base-list-view.component.less',
    './household-number-resident-list.component.less',
  ],
})
export class HouseholdNumberResidentListComponent extends BaseListViewComponent {
  headerTitle: string = 'Listahanan';
  OrderByString: string = 'Name';

  CurrentObject: any = {
    Name: '',
  };

  dataSource: any[] = [];

  async ListView_Onload() {
    await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT
              *
            FROM vHouseholdNumberResident_Listview
            /*encryptsqlend*/
            WHERE
              ID_Company IN (${this.currentUser.ID_Company}, 1)
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = '';

    if (this.CurrentObject.Name.length > 0) {
      filterString += `Name LIKE '%${this.CurrentObject.Name}%'`;
    }

    return filterString;
  }

  Row_OnClick(rowData: Item) {
    this.customNavigate(['Household', rowData.ID], {
      customFilter: 'ID_HouseholdNumber = ' + rowData.ID
    });
  }

  menuItem_New_onClick() {
    this.customNavigate(['Household', -1], {});
  }
}
