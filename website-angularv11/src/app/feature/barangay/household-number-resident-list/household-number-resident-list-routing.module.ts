import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HouseholdNumberResidentListComponent } from './household-number-resident-list.component';

const routes: Routes = [{ path: '', component: HouseholdNumberResidentListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HouseholdNumberResidentListRoutingModule { }
