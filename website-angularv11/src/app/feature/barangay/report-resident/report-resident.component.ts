import { Component } from '@angular/core';
import * as moment from 'moment';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import {
  FilterCriteriaType,
  IFilterFormValue,
  PropertyTypeEnum,
} from 'src/shared/APP_HELPER';
import { ReportComponent } from '../report/report.component';
import { Options } from '@angular-slider/ngx-slider';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
@Component({
  selector: 'app-report-resident',
  templateUrl: './report-resident.component.html',
  styleUrls: ['./report-resident.component.less'],
})
export class ReportResidentComponent extends ReportComponent {
  //Ngx Slider Configurations
  AssignedBHW_ID_Employee_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: '',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  options: Options = {
    floor: 0,
    ceil: 120,
    selectionBarGradient: {
      from: 'white',
      to: 'teal',
    },
    getPointerColor: (value: number): string => {
      return 'teal';
    },
  };

  ID_Gender_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM tGender',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  ID_IsVaccinated_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: "SELECT -1 ID, 'All' Name UNION SELECT 1 ID, 'Vaccinated' Name UNION SELECT 2 ID, 'Non Vaccinated' Name",
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  CurrentObject: any = {
    AgeStart: 0,
    AgeEnd: 120,
    IsPermanent: false,
    IsMigrante: false,
    IsTransient: false,
  };

  configOptions: any = {
    ReportName: 'RESIDENTDETAILREPORT',
  };

  protected getFilterValues(
    filterValues: IFilterFormValue[]
  ): IFilterFormValue[] {
    /*  if (this.CurrentObject['Name']) {

      filterValues.push({
        dataField: "Name",
        filterCriteriaType: FilterCriteriaType.Like,
        propertyType: PropertyTypeEnum.String,
        value: this.CurrentObject['Name']
      });
    } */

    if (this.CurrentObject['IsPermanent']) {
      filterValues.push({
        dataField: 'IsPermanent',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Int,
        value: 1,
      });
    }

    if (this.CurrentObject['IsMigrante']) {
      filterValues.push({
        dataField: 'IsMigrante',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Int,
        value: 1,
      });
    }

    if (this.CurrentObject['IsTransient']) {
      filterValues.push({
        dataField: 'IsTransient',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Int,
        value: 1,
      });
    }

    if (this.CurrentObject['IsPregnant']) {
      filterValues.push({
        dataField: 'IsPregnant',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Int,
        value: 1,
      });
    }

    if (this.CurrentObject['ID_IsVaccinated']) {
      var value_ID_IsVaccinated = null;
      switch (this.CurrentObject['ID_IsVaccinated']) {
        case VaccinationOptionEnum.Vaccinated:
          value_ID_IsVaccinated = 1;
          break;
        case VaccinationOptionEnum.NonVaccinated:
          value_ID_IsVaccinated = 0;
          break;
      }

      if (this.CurrentObject['ID_IsVaccinated'] != VaccinationOptionEnum.All) {
        filterValues.push({
          dataField: 'IsVaccinated',
          filterCriteriaType: FilterCriteriaType.Equal,
          propertyType: PropertyTypeEnum.Int,
          value: value_ID_IsVaccinated,
        });
      }
    }

    if (this.CurrentObject['ID_Gender']) {
      filterValues.push({
        dataField: 'ID_Gender',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Int,
        value: this.CurrentObject['ID_Gender'],
      });
    }

    filterValues.push({
      dataField: 'Age',
      filterCriteriaType: FilterCriteriaType.GreaterThan,
      propertyType: PropertyTypeEnum.Int,
      value: [this.CurrentObject['AgeStart'] - 1],
    });

    filterValues.push({
      dataField: 'Age',
      filterCriteriaType: FilterCriteriaType.LessThan,
      propertyType: PropertyTypeEnum.Int,
      value: [this.CurrentObject['AgeEnd'] + 1],
    });

    if (this.CurrentObject.AssignedBHW_ID_Employee != null) {
      filterValues.push({
        dataField: 'AssignedBHW_ID_Employee',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.Int,
        value: [this.CurrentObject['AssignedBHW_ID_Employee']],
      });
    }

    /************************* Caption ***************************/
    var caption = '';

    //IsTransient
    if (this.CurrentObject['IsTransient']) {
      if (caption.length > 0) caption += ', ';
      caption += `Transient`;
    }

    //IsPermanent
    if (this.CurrentObject['IsPermanent']) {
      if (caption.length > 0) caption += ', ';
      caption += `Permanent`;
    }

    //IsMigrante
    if (this.CurrentObject['IsMigrante']) {
      if (caption.length > 0) caption += ', ';
      caption += `Migrante`;
    }

    //Gender
    if (this.CurrentObject['ID_Gender']) {
      if (this.CurrentObject['ID_Gender'] == 1) {
        if (caption.length > 0) caption += ', ';
        caption += `Mga Lalake`;
      } else if (this.CurrentObject['ID_Gender'] == 2) {
        if (caption.length > 0) caption += ', ';
        caption += `Mga Lalake`;
      }
    }

    //IsPregnant
    if (this.CurrentObject['IsPregnant']) {
      if (caption.length > 0) caption += ', ';
      caption += `Pregnant`;
    }

    //ID_IsVaccinated
    if (this.CurrentObject['ID_IsVaccinated']) {
      if (this.CurrentObject['ID_IsVaccinated'] != VaccinationOptionEnum.All) {
        if (caption.length > 0) caption += ', ';
        caption += `All ${this.CurrentObject['Name_IsVaccinated']}`;
      }
    }

    //AssignedBHW_ID_Employee
    if (this.CurrentObject['AssignedBHW_ID_Employee']) {
      if (caption.length > 0) caption += ', ';
      caption += `BHW ${this.CurrentObject['AssignedBHW_Name_Employee']}`;
    }

    if (caption.length > 0) caption = '<b>Filter By:</b> ' + caption;

    if (caption.length > 0) {
      filterValues.push({
        dataField: 'Header_CustomCaption',
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.String,
        value: caption,
      });
    }
    /************************************************************/
    return filterValues;
  }

  onbtnClearClick() {
    this.CurrentObject = {
      AgeStart: 0,
      AgeEnd: 120,
    };

    this.load();
  }

  BaseReport_onLoad() {
    var listViewOptionID_AssignedBHW_ID_Employee =
      this.AssignedBHW_ID_Employee_LookupboxOption.listviewOption;

    if (listViewOptionID_AssignedBHW_ID_Employee != undefined) {
      listViewOptionID_AssignedBHW_ID_Employee.sql = `/*encryptsqlstart*/
                                    SELECT
                                          ID,
                                          Name
                                    FROM dbo.vEmployeeBarangayHealthWorker
                                    WHERE
                                          ID_Company = ${this.currentUser.ID_Company}
                                    /*encryptsqlend*/`;
    }
  }
}

enum VaccinationOptionEnum {
  All = -1,
  Vaccinated = 1,
  NonVaccinated = 2,
}
