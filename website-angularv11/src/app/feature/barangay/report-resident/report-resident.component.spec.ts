import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportResidentComponent } from './report-resident.component';

describe('ReportResidentComponent', () => {
  let component: ReportResidentComponent;
  let fixture: ComponentFixture<ReportResidentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportResidentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportResidentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
