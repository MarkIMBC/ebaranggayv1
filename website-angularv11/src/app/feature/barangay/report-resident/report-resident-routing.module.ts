import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportResidentComponent } from './report-resident.component';

const routes: Routes = [{ path: '', component: ReportResidentComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportResidentRoutingModule { }
