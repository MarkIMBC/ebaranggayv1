import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportResidentRoutingModule } from './report-resident-routing.module';
import { ReportResidentComponent } from './report-resident.component';
import { SharedModule } from './../../../shared/shared.module';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
@NgModule({
  declarations: [ReportResidentComponent],
  imports: [
    CommonModule,
     SharedModule,
     NgxSliderModule,
    ReportResidentRoutingModule,

  ]
})
export class ReportResidentModule { }


