import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResidentListOFWRoutingModule } from './resident-list-deceased-routing.module';
import { ResidentListDeceasedComponent } from './resident-list-deceased.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [ResidentListDeceasedComponent],
  imports: [CommonModule, SharedModule, ResidentListOFWRoutingModule],
})
export class ResidentListDeceasedModule {}
