import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResidentListDeceasedComponent } from './resident-list-deceased.component';

const routes: Routes = [{ path: '', component: ResidentListDeceasedComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResidentListOFWRoutingModule { }
