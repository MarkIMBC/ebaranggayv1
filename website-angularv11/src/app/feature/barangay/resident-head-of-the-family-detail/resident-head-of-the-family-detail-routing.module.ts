import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResidentHeadOfTheFamilyDetailComponent } from './resident-head-of-the-family-detail.component';

const routes: Routes = [{ path: '', component: ResidentHeadOfTheFamilyDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResidentHeadOfTheFamilyDetailRoutingModule { }
