import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidentHeadOfTheFamilyDetailComponent } from './resident-head-of-the-family-detail.component';

describe('ResidentHeadOfTheFamilyDetailComponent', () => {
  let component: ResidentHeadOfTheFamilyDetailComponent;
  let fixture: ComponentFixture<ResidentHeadOfTheFamilyDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResidentHeadOfTheFamilyDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidentHeadOfTheFamilyDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
