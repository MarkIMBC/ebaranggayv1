import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResidentHeadOfTheFamilyDetailRoutingModule } from './resident-head-of-the-family-detail-routing.module';
import { ResidentHeadOfTheFamilyDetailComponent } from './resident-head-of-the-family-detail.component';


@NgModule({
  declarations: [ResidentHeadOfTheFamilyDetailComponent],
  imports: [
    CommonModule,
    ResidentHeadOfTheFamilyDetailRoutingModule
  ]
})
export class ResidentHeadOfTheFamilyDetailModule { }
