import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidentProgramTableComponent } from './resident-program-table.component';

describe('ResidentProgramTableComponent', () => {
  let component: ResidentProgramTableComponent;
  let fixture: ComponentFixture<ResidentProgramTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResidentProgramTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidentProgramTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
