import { BaseDetailViewComponent } from '../../../base-detail-view/base-detail-view.component';
import { Component, Input, OnInit } from '@angular/core';
import { IControlModelArg, IFormValidation } from 'src/shared/APP_HELPER';

@Component({
  selector: 'vaccination-info',
  templateUrl: './vaccination-info.component.html',
  styleUrls: ['./vaccination-info.component.less'],
})
export class VaccinationInfoComponent implements OnInit {
  @Input() CurrentObject: any;
  @Input() BaseDetailView: BaseDetailViewComponent | undefined;

  constructor() {}

  ngOnInit(): void {}

  control_onModelChanged(e: IControlModelArg) {
    if (this.BaseDetailView) {
      this.BaseDetailView.control_onModelChanged(e);
    }
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    if (
      this.CurrentObject.IsVaccinated == true &&
      this.CurrentObject.DateLastVaccination == null
    ) {
      validations.push({
        message: `Date Vaccination is required.`,
      });
    }

    return Promise.resolve(validations);
  }
}
