import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidentMedicalRecordVaccinationTableComponent } from './resident-medical-record-vaccination-table.component';

describe('ResidentMedicalRecordVaccinationTableComponent', () => {
  let component: ResidentMedicalRecordVaccinationTableComponent;
  let fixture: ComponentFixture<ResidentMedicalRecordVaccinationTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResidentMedicalRecordVaccinationTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidentMedicalRecordVaccinationTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
