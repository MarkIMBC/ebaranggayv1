import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResidentDetailRoutingModule } from './resident-detail-routing.module';
import { ResidentDetailComponent } from './resident-detail.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ResidentProgramTableComponent } from './resident-program-table/resident-program-table.component';
import { VaccinationInfoComponent } from './vaccination-info/vaccination-info.component';
import { ResidentFamilyTableComponent } from './resident-family-table/resident-family-table.component';
import { ResidentMedicalRecordTableComponent } from './resident-medical-record-table/resident-medical-record-table.component';
import { ResidentMedicalRecordVaccinationTableComponent } from './resident-medical-record-vaccination-table/resident-medical-record-vaccination-table.component';
import { ResidentMedicalRecordPregnancyDiagnosisTableComponent } from './resident-medical-record-pregnancy-diagnosis-table/resident-medical-record-pregnancy-diagnosis-table.component';
import { ResidentMedicalRecordOthersTableComponent } from './resident-medical-record-others-table/resident-medical-record-others-table.component';
import { LawSuitListComponent } from './law-suit-list/law-suit-list.component';
import { ComplaintLawSuitListComponent } from './complaint-law-suit-list/complaint-law-suit-list.component';

@NgModule({
  declarations: [
    ResidentDetailComponent,
    ResidentProgramTableComponent,
    VaccinationInfoComponent,
    ResidentFamilyTableComponent,
    ResidentMedicalRecordTableComponent,
    ResidentMedicalRecordVaccinationTableComponent,
    ResidentMedicalRecordPregnancyDiagnosisTableComponent,
    ResidentMedicalRecordOthersTableComponent,
    LawSuitListComponent,
    ComplaintLawSuitListComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    ResidentDetailRoutingModule,
  ],
})
export class ResidentDetailModule {}
