import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComplaintLawSuitListComponent } from './complaint-law-suit-list.component';

describe('ComplaintLawSuitListComponent', () => {
  let component: ComplaintLawSuitListComponent;
  let fixture: ComponentFixture<ComplaintLawSuitListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComplaintLawSuitListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplaintLawSuitListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
