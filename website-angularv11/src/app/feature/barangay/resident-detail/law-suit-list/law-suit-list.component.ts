import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { ToastService } from 'src/app/shared/toast.service';
import { FilingStatusEnum } from 'src/shared/APP_HELPER';

@Component({
  selector: 'law-suit-list',
  templateUrl: './law-suit-list.component.html',
  styleUrls: ['./law-suit-list.component.less'],
})
export class LawSuitListComponent implements OnInit {
  dataSource: any[] = [];
  loading: boolean = false;
  currentUser: TokenSessionFields = new TokenSessionFields();

  @Input() CurrentObject: any = {};
  @Input() IsDirty: boolean = false;
  @Input() IsDisabled: boolean = false;
  @Output() onCancel: EventEmitter<any> = new EventEmitter<any>();
  @Output() onLoad: EventEmitter<any> = new EventEmitter<any>();

  private ID_Patient: number = 0;

  constructor(
    private ds: DataService,
    private cs: CrypterService,
    private userAuth: UserAuthenticationService,
    private toastService: ToastService,
    private router: Router
  ) {}

  menuItems: AdminLTEMenuItem[] = [];

  loadMenuItems() {
    var menuItem_New = {
      label: 'Create',
      icon: 'fa fa-plus',
      visible: true,
    };

    var menuItem_View = {
      label: 'View',
      icon: 'fa fa-stethoscope',
      visible: true,
    };

    var menuItem_Refresh = {
      label: 'Refresh',
      icon: 'fa fa-sync',
      visible: true,
    };

    this.menuItems = [];

    if (
      this.CurrentObject.ID == 0 ||
      this.CurrentObject.ID == -1 ||
      this.CurrentObject.ID == null
    )
      return;
    if (this.IsDisabled) return;

    this.menuItems.push(menuItem_New);
    this.menuItems.push(menuItem_Refresh);
  }

  async menuItems_OnClick(arg: any) {
    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.label == 'Create') {
      // this.doCreate();
    } else if (menuItem.label == 'Refresh') {
      this.doRefresh();
    }
  }

  doCreate() {
    GeneralfxService.customNavigate(
      this.router,
      this.cs,
      this.currentUser,
      ['LawSuit', -1],
      {
        Complainant_ID_Resident: this.CurrentObject.ID,
      }
    );
  }

  doRefresh() {
    this.Load(this._ID_Resident);
  }

  async ngOnInit(): Promise<void> {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  private _ID_Resident: number = 0;
  async Load(ID_Resident: number) {
    this._ID_Resident = ID_Resident;
    var sql = `SELECT *
               FROM dbo.vLawSuit_ListView
               WHERE
                Complainant_ID_Resident = ${this._ID_Resident} AND
                ID_Company = (${this.currentUser.ID_Company})
               ORDER BY
                Date DESC
    `;

    sql = this.cs.encrypt(sql);
    this.ds.query<any>(sql).then((obj) => {
      this.dataSource = obj;

      this.loadMenuItems();
      this.onLoad.emit({
        dataSource: this.dataSource,
      });
    });
  }

  onRowClick(rowData: any) {
    GeneralfxService.customNavigate(
      this.router,
      this.cs,
      this.currentUser,
      ['LawSuit', rowData.ID],
      {}
    );
  }
}
