import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import {
  Resident_Program_DTO,
  PropertyTypeEnum,
  IControlModelArg,
  Resident_MedicalRecord_DTO,
} from 'src/shared/APP_HELPER';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';

@Component({
  selector: 'resident-medical-record-others-table',
  templateUrl: './resident-medical-record-others-table.component.html',
  styleUrls: ['./resident-medical-record-others-table.component.less'],
})
export class ResidentMedicalRecordOthersTableComponent implements OnInit {
  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;

  @Input() MedicalRecords: Resident_MedicalRecord_DTO[] = [];

  tempID: number = 0;

  Vaccination_ID_MedicalRecordType: number = 1;
  PregnacyDiagnosis_ID_MedicalRecordType: number = 2;

  currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(private userAuth: UserAuthenticationService) {}

  ngOnInit(): void {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  menuItems: AdminLTEMenuItem[] = [
    {
      label: 'Mag Dagdag',
      name: 'AddMedicalRecord',
      icon: 'fa fa-plus',
      visible: true,
    },
  ];

  async doAdd() {
    if (this.MedicalRecords == null) this.MedicalRecords = [];

    this.tempID--;

    var item: any = {
      ID: this.tempID,
    };

    this.MedicalRecords.unshift(item);
  }

  async menuItems_OnClick(arg: any) {
    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.name == 'AddMedicalRecord') {
      this.doAdd();
    }
  }

  async BtnDeleteSchedule_OnClick(ScheduleList: any) {
    var index = GeneralfxService.findIndexByKeyValue(
      this.MedicalRecords,
      'ID',
      ScheduleList.ID + ''
    );

    this.MedicalRecords.splice(index, 1);
  }

  MedicalRecord_onModelChanged(record: any, e: IControlModelArg) {
    record[e.name] = e.value;
  }

  ID_MedicalRecordType_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: `SELECT
                  ID,
                  Name
            FROM tMedicalRecordType WHERE ID NOT IN (${this.Vaccination_ID_MedicalRecordType}, ${this.PregnacyDiagnosis_ID_MedicalRecordType})`,
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };
}
