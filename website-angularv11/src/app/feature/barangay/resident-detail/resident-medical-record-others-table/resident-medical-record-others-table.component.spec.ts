import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidentMedicalRecordOthersTableComponent } from './resident-medical-record-others-table.component';

describe('ResidentMedicalRecordOthersTableComponent', () => {
  let component: ResidentMedicalRecordOthersTableComponent;
  let fixture: ComponentFixture<ResidentMedicalRecordOthersTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResidentMedicalRecordOthersTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidentMedicalRecordOthersTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
