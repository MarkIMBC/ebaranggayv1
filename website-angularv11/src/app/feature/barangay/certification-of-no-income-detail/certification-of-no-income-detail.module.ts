import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CertificationOfNoIncomeDetailRoutingModule } from './certification-of-no-income-detail-routing.module';
import { CertificationOfNoIncomeDetailComponent } from './certification-of-no-income-detail.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [CertificationOfNoIncomeDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    CertificationOfNoIncomeDetailRoutingModule,
  ],
})
export class CertificationOfNoIncomeDetailModule {}
