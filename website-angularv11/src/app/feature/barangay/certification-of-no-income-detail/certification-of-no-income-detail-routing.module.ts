import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CertificationOfNoIncomeDetailComponent } from './certification-of-no-income-detail.component';

const routes: Routes = [{ path: '', component: CertificationOfNoIncomeDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CertificationOfNoIncomeDetailRoutingModule { }
