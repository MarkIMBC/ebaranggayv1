import { IFormValidation } from 'src/shared/APP_HELPER';
import {
  BaseDetailViewComponent,
  CurrentObjectOnValueChangeArg,
} from '../../base-detail-view/base-detail-view.component';
import { Component } from '@angular/core';

@Component({
  selector: 'app-certification-of-no-income-detail',
  templateUrl: './certification-of-no-income-detail.component.html',
  styleUrls: [
    './../../base-detail-view/base-detail-view.component.less',
    './certification-of-no-income-detail.component.less',
  ],
})
export class CertificationOfNoIncomeDetailComponent extends BaseDetailViewComponent {
  ModelName: string = 'CertificationOfNoIncome';
  headerTitle: string = 'Certification Of No Income';

  DetailView_onLoad() {}

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {}

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    var Name = this.CurrentObject.Name;

    if (Name == undefined || Name == null) Name = '';

    return Promise.resolve(validations);
  }
}
