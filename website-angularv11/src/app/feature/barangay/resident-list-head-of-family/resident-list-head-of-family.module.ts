import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResidentListHeadOfFamilyRoutingModule } from './resident-list-head-of-family-routing.module';
import { ResidentListHeadOfFamilyComponent } from './resident-list-head-of-family.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [ResidentListHeadOfFamilyComponent],
  imports: [CommonModule, SharedModule, ResidentListHeadOfFamilyRoutingModule],
})
export class ResidentListHeadOfFamilyModule {}
