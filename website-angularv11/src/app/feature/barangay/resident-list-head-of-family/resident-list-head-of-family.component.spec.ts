import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidentListHeadOfFamilyComponent } from './resident-list-head-of-family.component';

describe('ResidentListHeadOfFamilyComponent', () => {
  let component: ResidentListHeadOfFamilyComponent;
  let fixture: ComponentFixture<ResidentListHeadOfFamilyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResidentListHeadOfFamilyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidentListHeadOfFamilyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
