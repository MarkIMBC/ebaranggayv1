import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResidentListHeadOfFamilyComponent } from './resident-list-head-of-family.component';

const routes: Routes = [{ path: '', component: ResidentListHeadOfFamilyComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResidentListHeadOfFamilyRoutingModule { }
