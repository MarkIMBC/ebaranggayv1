import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeDetailRoutingModule } from './employee-detail-routing.module';
import { EmployeeDetailComponent } from './employee-detail.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [EmployeeDetailComponent],
  imports: [
    CommonModule,
    SharedModule,
    EmployeeDetailRoutingModule
  ]
})
export class EmployeeDetailModule { }
