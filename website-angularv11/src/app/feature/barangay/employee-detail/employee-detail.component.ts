import { ChangeUserPasswordDialogComponent } from './../../../shared/change-user-password-dialog/change-user-password-dialog.component';
import { Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { IFormValidation, PropertyTypeEnum } from 'src/shared/APP_HELPER';
import {
  BaseDetailViewComponent,
  CurrentObjectOnValueChangeArg,
} from '../../base-detail-view/base-detail-view.component';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: [
    './../../base-detail-view/base-detail-view.component.less',
    './employee-detail.component.less',
  ],
})
export class EmployeeDetailComponent extends BaseDetailViewComponent {
  @ViewChild('changeuserpassworddialog')
  changeuserpassworddialog: ChangeUserPasswordDialogComponent | undefined;

  ModelName: string = 'Employee';
  displayMember: string = 'Name';

  ID_Position_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM tPosition',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  protected async _getDefault__ID_CurrentObject() {
    var id = null;

    if (this.route.snapshot.params['ID_CurrentObject']) {
      id = this.route.snapshot.params['ID_CurrentObject'];
    } else {
      if (this.currentUser != undefined) {
        if (this.currentUser.ID_Employee != undefined) {
          id = this.currentUser.ID_Employee;
          this.routerFeatureName = 'CurrentEmployee';
        }
      }
    }

    return id;
  }

  loadInitMenuItem() {
    if (this.route.snapshot.params['ID_CurrentObject']) {
      this.menuItems.push(this._menuItem_New);
    }

    this.menuItems.push(this._menuItem_Save);

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  loadMenuItems() {
    var btnChangeUserPassword: AdminLTEMenuItem = {
      label: 'Change Password',
      name: 'btnchangepassword',
      icon: 'fa fa-file',
      class: 'text-default',
      visible: true,
    };

    if (this.CurrentObject.UserAccount_ID_User) {
      this.menuItems.push(btnChangeUserPassword);
    }
  }

  menubar_OnClick(e: any) {
    var menuItem: AdminLTEMenuItem = e.item;

    if (menuItem.name == 'btnchangepassword') {
      if (this.changeuserpassworddialog) {
        this.changeuserpassworddialog.show(
          this.CurrentObject.UserAccount_ID_User
        );
      }
    }
  }

  DetailView_onLoad() {
    this.headerTitle = this.CurrentObject.Name;
  }

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {
    if (
      this.CurrentObject['LastName'] == undefined ||
      this.CurrentObject['LastName'] == null
    )
      this.CurrentObject['LastName'] = '';
    if (
      this.CurrentObject['FirstName'] == undefined ||
      this.CurrentObject['FirstName'] == null
    )
      this.CurrentObject['FirstName'] = '';
    if (
      this.CurrentObject['MiddleName'] == undefined ||
      this.CurrentObject['MiddleName'] == null
    )
      this.CurrentObject['MiddleName'] = '';

    this.CurrentObject.Name =
      this.CurrentObject['LastName'] +
      ', ' +
      this.CurrentObject['FirstName'] +
      ' ' +
      this.CurrentObject['MiddleName'];

    this.headerTitle = this.CurrentObject.Name;
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    var LastName = this.CurrentObject.LastName;
    var FirstName = this.CurrentObject.FirstName;
    var MiddleName = this.CurrentObject.MiddleName;
    var ID_Position = this.CurrentObject.ID_Position;

    if (LastName == undefined || LastName == null) LastName = '';
    if (FirstName == undefined || FirstName == null) FirstName = '';
    if (MiddleName == undefined || MiddleName == null) MiddleName = '';
    if (ID_Position == undefined || ID_Position == null) ID_Position = 0;

    if (LastName.length == 0) {
      validations.push({
        message: 'Last Name is required.',
      });
    }

    if (FirstName.length == 0) {
      validations.push({
        message: 'First Name is required.',
      });
    }

    if (ID_Position == 0) {
      validations.push({
        message: 'Position is required.',
      });
    }

    return Promise.resolve(validations);
  }

  protected redirectAfterSaved() {
    var routerFeatureName = this.model.Name;
    if (this.routerFeatureName.length > 0)
      routerFeatureName = this.routerFeatureName;

    if (this.route.snapshot.params['ID_CurrentObject']) {

      var routeLink = [routerFeatureName, this.__ID_CurrentObject];
      this.customNavigate(routeLink, this.configOptions);
    }else{

      this.loadRecord();
    }

  }

}
