import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidentListSoloParentComponent } from './resident-list-solo-parent.component';

describe('ResidentListSoloParentComponent', () => {
  let component: ResidentListSoloParentComponent;
  let fixture: ComponentFixture<ResidentListSoloParentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResidentListSoloParentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidentListSoloParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
