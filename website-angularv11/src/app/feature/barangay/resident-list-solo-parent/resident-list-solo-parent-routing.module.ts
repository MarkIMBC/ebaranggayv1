import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResidentListSoloParentComponent } from './resident-list-solo-parent.component';

const routes: Routes = [{ path: '', component: ResidentListSoloParentComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResidentListSoloParentRoutingModule { }
