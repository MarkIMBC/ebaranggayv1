import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResidentListSoloParentRoutingModule } from './resident-list-solo-parent-routing.module';
import { ResidentListSoloParentComponent } from './resident-list-solo-parent.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ResidentListSoloParentComponent],
  imports: [
    CommonModule,SharedModule,
    ResidentListSoloParentRoutingModule
  ]
})
export class ResidentListSoloParentModule { }
