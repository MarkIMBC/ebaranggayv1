import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResidentListOFWComponent } from './resident-list-ofw.component';

const routes: Routes = [{ path: '', component: ResidentListOFWComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResidentListOFWRoutingModule { }
