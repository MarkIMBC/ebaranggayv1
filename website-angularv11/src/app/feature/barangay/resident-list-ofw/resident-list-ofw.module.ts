import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResidentListOFWRoutingModule } from './resident-list-ofw-routing.module';
import { ResidentListOFWComponent } from './resident-list-ofw.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [ResidentListOFWComponent],
  imports: [CommonModule, SharedModule, ResidentListOFWRoutingModule],
})
export class ResidentListOFWModule {}
