import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidentListOFWComponent } from './resident-list-ofw.component';

describe('ResidentListOFWComponent', () => {
  let component: ResidentListOFWComponent;
  let fixture: ComponentFixture<ResidentListOFWComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResidentListOFWComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidentListOFWComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
