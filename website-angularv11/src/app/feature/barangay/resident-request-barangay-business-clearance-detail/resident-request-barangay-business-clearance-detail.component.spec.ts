import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidentRequestBarangayBusinessClearanceDetailComponent } from './resident-request-barangay-business-clearance-detail.component';

describe('ResidentRequestBarangayBusinessClearanceDetailComponent', () => {
  let component: ResidentRequestBarangayBusinessClearanceDetailComponent;
  let fixture: ComponentFixture<ResidentRequestBarangayBusinessClearanceDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResidentRequestBarangayBusinessClearanceDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidentRequestBarangayBusinessClearanceDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
