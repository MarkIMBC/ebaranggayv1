import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResidentRequestBarangayBusinessClearanceDetailComponent } from './resident-request-barangay-business-clearance-detail.component';

const routes: Routes = [{ path: '', component: ResidentRequestBarangayBusinessClearanceDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResidentRequestBarangayBusinessClearanceDetailRoutingModule { }
