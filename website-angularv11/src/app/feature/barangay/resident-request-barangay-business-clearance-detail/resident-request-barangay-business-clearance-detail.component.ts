import { ResidentRequest_BarangayBusinessClearance } from './../../../../shared/APP_MODELS';
import { Component } from '@angular/core';
import {
  FilterCriteriaType,
  IControlModelArg,
  PropertyTypeEnum,
} from 'src/shared/APP_HELPER';
import { APP_REPORTVIEW } from 'src/shared/APP_MODELS';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { BaseDetailViewComponent } from '../../base-detail-view/base-detail-view.component';

@Component({
  selector: 'resident-request-barangay-business-clearance-detail',
  templateUrl:
    './resident-request-barangay-business-clearance-detail.component.html',
  styleUrls: [
    './../../base-detail-view/base-detail-view.component.less',
    './resident-request-barangay-business-clearance-detail.component.less',
  ],
})
export class ResidentRequestBarangayBusinessClearanceDetailComponent extends BaseDetailViewComponent {
  ModelName: string = 'ResidentRequest';
  headerTitle: string = 'Barangay Business Clearance';
  routerFeatureName: string = 'ResidentRequest_BarangayBusinessClearance';

  currentResidentRequest_BarangayBusinessClearance: any =
    new ResidentRequest_BarangayBusinessClearance();

  displayMember: string = 'Name';

  loadInitMenuItem() {
    if (this.CurrentObject.IsActive == true) {
      this.menuItems.push(this._menuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  loadMenuItems() {
    var menuReport: AdminLTEMenuItem = {
      label: 'Report',
      icon: 'far fa-file',
      class: 'text-primary',
      visible: true,
      items: [],
    };

    var barangayBusinessClearanceMenuItem = {
      label: 'Barangay Business Clearance',
      name: APP_REPORTVIEW.RESIDENTREQUESTBARANGAYBUSINESSCLEARANCE,
      icon: 'far fa-file',
      class: 'text-primary',
      visible: true,
    };

    var sendSMSReminderMenuItem = {
      label: 'Mag Send ng SMS Reminder',
      name: 'sendSMSReminder',
      icon: 'fas fa-sms',
      class: 'text-primary',
      visible: true,
    };

    if (menuReport.items) {
      menuReport.items.push(barangayBusinessClearanceMenuItem);
    }

    if (this.CurrentObject.IsActive != true) return;

    //this.addMenuItem(this._menuItem_Delete);

    if (this.CurrentObject.ID > 0) {
      this.addMenuItem(sendSMSReminderMenuItem);
      this.addMenuItem(menuReport);
    }
  }

  async menubar_OnClick(e: any) {
    if (
      e.item.name == APP_REPORTVIEW.RESIDENTREQUESTBARANGAYBUSINESSCLEARANCE
    ) {
      this.customNavigate(['Report'], {
        ReportName: 'RESIDENTREQUESTBARANGAYBUSINESSCLEARANCE',
        filterValues: [
          {
            dataField: 'ID',
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID,
          },
        ],
      });
    } else if (e.item.name == 'sendSMSReminder') {
      this.loading = true;

      if (this.CurrentObject.ContactNumber_ResidentRequest == null)
        this.CurrentObject.ContactNumber_ResidentRequest = '';

      var contactNumbers =
        this.CurrentObject.ContactNumber_ResidentRequest.split(',');

      var obj = {
        CellPhoneNumbers: contactNumbers,
        Message:
          "Magandang araw, ang iyong BARANGAY BUSINESS CLEARANCE na iyong nirequest ay natapos nang i-proseso at nakahanda na ito'y iyong kunin.",
      };

      var r = await this.ds.sendtext(obj);

      if (
        r == null ||
        r.PhoneNumberStatus == undefined ||
        r.PhoneNumberStatus == null
      ) {
        this.toastService.warning(
          'Ang SMS Reminder ay hindi nag tagumpay sa pagpapadala.'
        );
      } else {
        this.toastService.success(
          'Ang SMS reminder ay matagumpay na naipadala.'
        );
      }

      this.loading = false;
    }
  }

  DetailView_onLoad() {
    if (
      this.CurrentObject['ResidentRequest_BarangayBusinessClearance'] == null ||
      this.CurrentObject['ResidentRequest_BarangayBusinessClearance'] ==
        undefined
    ) {
      this.CurrentObject['ResidentRequest_BarangayBusinessClearance'] = [];
    }

    if (
      this.CurrentObject['ResidentRequest_BarangayBusinessClearance'].length ==
      0
    ) {
      var obj = new ResidentRequest_BarangayBusinessClearance();
      obj.ID = this.getTempID();
      obj.ID_Company = this.currentUser.ID_Company;

      this.CurrentObject['ResidentRequest_BarangayBusinessClearance'].push(obj);
    }

    this.currentResidentRequest_BarangayBusinessClearance =
      this.CurrentObject['ResidentRequest_BarangayBusinessClearance'][0];

    if (
      this.currentResidentRequest_BarangayBusinessClearance.ID_Company == null
    ) {
      this.currentResidentRequest_BarangayBusinessClearance.ID_Company =
        this.currentUser.ID_Company;
    }
  }

  currentResidentRequest_CommunityTaxCertificate_onModelChanged(
    e: IControlModelArg
  ) {
    this.currentResidentRequest_BarangayBusinessClearance[e.name] = e.value;

    if (e.displayName != undefined) {
      this.currentResidentRequest_BarangayBusinessClearance[e.displayName] =
        e.displayValue;
    }

    this.isDirty = true;
  }

  public CurrentObject_onBeforeSaving() {}

  protected pGetRecordOptions(): any {
    var options: any = {};
    var configKeys = ['ID_Resident', 'ID_ResidentRequest'];

    configKeys.forEach((key) => {
      if (
        this.configOptions[key] != undefined &&
        this.configOptions[key] != null
      ) {
        options[key] = this.configOptions[key];
      }
    });

    return options;
  }
}
