import { Component, OnInit } from '@angular/core';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { Router } from '@angular/router';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/core/UserAuthentication.service';

@Component({
  selector: 'app-initial-page',
  templateUrl: './initial-page.component.html',
  styleUrls: ['./initial-page.component.less']
})
export class InitialPageComponent implements OnInit {
  cardBodyHeight: string = '100px';
  isVisible: boolean = false;

  company: any = {
    ImageHeaderLocationFilenamePath: '',
    Name: '',
  };

  currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(
    private ds: DataService,
    private userAuth: UserAuthenticationService,
    protected cs: CrypterService,
    protected router: Router
  ) {}

  ngOnInit(): void {
    this.currentUser = this.userAuth.getDecodedToken();

    this.loadCompany();
  }

  async ngAfterViewInit() {
    setTimeout(() => {
      this.resize();
      this.isVisible = true;
    }, 2000);
  }

  async loadCompany() {
    var queryString = this.cs.encrypt(`
                      SELECT
                        Name,
                        IsShowPaymentWarningLabel,
                        ImageHeaderLocationFilenamePath,
                        MainRouteLink
                      FROM vCompany
                      WHERE
                        ID IN (${this.currentUser.ID_Company})
    `);

    var records = await this.ds.query<any>(queryString);
    this.company = records[0];

    this.router.navigate([this.company.MainRouteLink]);
  }

  resize() {
    var height = window.innerHeight;
    var cardBodyHeight = 0;

    cardBodyHeight = height - 100;

    if (cardBodyHeight < 350) {
      cardBodyHeight = 400;
    }

    this.cardBodyHeight = cardBodyHeight.toString() + 'px';
  }
}
