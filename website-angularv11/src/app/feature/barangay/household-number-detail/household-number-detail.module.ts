import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HouseholdNumberDetailRoutingModule } from './household-number-detail-routing.module';
import { HouseholdNumberDetailComponent } from './household-number-detail.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { DisasterRiskCheckListComponent } from './disaster-risk-check-list/disaster-risk-check-list.component';
import { HeadOfFamilyTableComponent } from './head-of-family-table/head-of-family-table.component';

@NgModule({
  declarations: [HouseholdNumberDetailComponent, DisasterRiskCheckListComponent, HeadOfFamilyTableComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    HouseholdNumberDetailRoutingModule,
  ],
})
export class HouseholdNumberDetailModule {}
