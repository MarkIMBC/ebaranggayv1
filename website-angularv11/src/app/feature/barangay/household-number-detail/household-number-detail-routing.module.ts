import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HouseholdNumberDetailComponent } from './household-number-detail.component';

const routes: Routes = [{ path: '', component: HouseholdNumberDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HouseholdNumberDetailRoutingModule { }
