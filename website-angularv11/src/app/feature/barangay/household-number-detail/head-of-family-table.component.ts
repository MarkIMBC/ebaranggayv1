import { Component, isDevMode, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { ToastService } from 'src/app/shared/toast.service';
import { Item } from 'src/shared/APP_MODELS';

@Component({
  selector: 'head-of-family-table',
  templateUrl: './head-of-family-table.component.html',
  styleUrls: [
    './../../../base-list-view/base-list-view.component.less',
    './head-of-family-table.component.less',
  ],
})
export class HeadOfFamilyTableComponent implements OnInit {
  currentUser: TokenSessionFields = new TokenSessionFields();

  dataSource: any[] = [];

  _ID_HouseholdNumber: number = 0;

  constructor(
    protected router: Router,
    protected ds: DataService,
    protected toastService: ToastService,
    protected userAuth: UserAuthenticationService,
    protected cs: CrypterService
  ) {}
  ngOnInit(): void {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  async goToRemoveOnHouseholdNumber(rowData: any) {
    var ID_HouseholdNumber = null;
  }

  goToLink(rowData: any) {
    var config: any[] = GeneralfxService.getCustomNavigate(
      this.cs,
      this.currentUser,
      ['Resident', rowData.ID],
      {
        IsHeadOfFamilyPage: true,
      }
    );

    const url = this.router.serializeUrl(this.router.createUrlTree(config));

    window.open(url, '_blank');
  }

  Row_OnClick(rowData: Item) {
    GeneralfxService.customNavigate(
      this.router,
      this.cs,
      this.currentUser,
      ['Resident', rowData.ID],
      {
        IsHeadOfFamilyPage: true,
      }
    );
  }

  getFilterString() {
    return '';
  }

  refresh() {
    this.load(this._ID_HouseholdNumber);
  }

  async load(ID_HouseholdNumber: number) {
    var sql = '';
    var filterString = this.getFilterString();

    this._ID_HouseholdNumber = ID_HouseholdNumber;

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    var sql = this.cs.encrypt(
      `
      /*encryptsqlstart*/
        SELECT
          *
        FROM vResident_Listview
      /*encryptsqlend*/
      WHERE
        ID_Company = ${this.currentUser.ID_Company} AND
        ID_HouseholdNumber = ${this._ID_HouseholdNumber} AND
        IsActive = 1
        ${filterString}
    `
    );

    this.dataSource = await this.ds.query<any>(sql);
  }
}
