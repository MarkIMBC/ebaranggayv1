import { Component, isDevMode, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import { ToastService } from 'src/app/shared/toast.service';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { Item } from 'src/shared/APP_MODELS';

@Component({
  selector: 'head-of-family-table',
  templateUrl: './head-of-family-table.component.html',
  styleUrls: [
    './../../../base-list-view/base-list-view.component.less',
    './head-of-family-table.component.less',
  ],
})
export class HeadOfFamilyTableComponent implements OnInit {
  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;

  currentUser: TokenSessionFields = new TokenSessionFields();

  dataSource: any[] = [];
  menuItems: AdminLTEMenuItem[] = [];

  _ID_HouseholdNumber: number = 0;

  constructor(
    protected router: Router,
    protected ds: DataService,
    protected toastService: ToastService,
    protected userAuth: UserAuthenticationService,
    protected cs: CrypterService
  ) {}
  ngOnInit(): void {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  async goToRemoveOnHouseholdNumber(rowData: any) {
    var ID_HouseholdNumber = null;

    await this.changeResidentHouseholdNumber(
      ID_HouseholdNumber,
      [rowData.ID],
      `Head Of Family has been removed.`
    );
    this.refresh();
  }
  goToLink(rowData: any) {
    var config: any[] = GeneralfxService.getCustomNavigate(
      this.cs,
      this.currentUser,
      ['Resident', rowData.ID],
      {
        IsHeadOfFamilyPage: true,
      }
    );

    const url = this.router.serializeUrl(this.router.createUrlTree(config));

    window.open(url, '_blank');
  }

  Row_OnClick(rowData: Item) {
    GeneralfxService.customNavigate(
      this.router,
      this.cs,
      this.currentUser,
      ['Resident', rowData.ID],
      {
        IsHeadOfFamilyPage: true,
      }
    );
  }

  getFilterString() {
    return '';
  }

  refresh() {
    this.load(this._ID_HouseholdNumber);
  }

  loadMenuItems() {
    this.menuItems = [];
    this.menuItems.push({
      label: 'Add Family',
      icon: 'fas fa-plus  ',
      visible: true,
      command: () => {
        return true;
      },
    });
  }

  async load(ID_HouseholdNumber: number) {
    var sql = '';
    var filterString = this.getFilterString();

    this.loadMenuItems();

    this._ID_HouseholdNumber = ID_HouseholdNumber;

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    var sql = this.cs.encrypt(
      `
      /*encryptsqlstart*/
        SELECT
          *
        FROM vResident_Listview_HeadOfFamily
      /*encryptsqlend*/
      WHERE
        ID_Company = ${this.currentUser.ID_Company} AND
        ID_HouseholdNumber = ${this._ID_HouseholdNumber} AND
        IsActive = 1
        ${filterString}
      ORDER BY
        Name ASC
    `
    );

    this.dataSource = await this.ds.query<any>(sql);
  }

  async menuItems_OnClick(arg: any) {
    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.label == 'Add Family') {
      if (this.sqllistdialog == undefined) return;

      var ID_HouseholdNumber = 0;

      this.sqllistdialog.caption = 'Select Head of the Family';

      var obj: any;
      obj = await this.sqllistdialog.open({
        sql: `
            /*encryptsqlstart*/
            SELECT
              *
            FROM vResident_Listview_HeadOfFamily
            /*encryptsqlend*/
            WHERE
              ID_Company = ${this.currentUser.ID_Company} AND
              ISNULL(ID_HouseholdNumber,0) IN (0) AND
              IsActive = 1
              /*encryptsqlend*/
            `,
        columns: [
          {
            name: 'Name',
            caption: 'Name',
            propertyType: PropertyTypeEnum.String,
          },
        ],
        orderByString: 'Name',
      });

      var IDs_Resident: any[] = [];
      obj.rows.forEach((row: any) => {
        IDs_Resident.push(row.ID);
      });

      this.changeResidentHouseholdNumber(
        this._ID_HouseholdNumber,
        IDs_Resident,
        `(${IDs_Resident.length}) Head Of Family has been added.`
      );
    }
  }

  async changeResidentHouseholdNumber(
    ID_HouseholdNumber: any,
    IDs_Resident: any[],
    successMsg: string
  ): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        'pChangeResidentHouseholdNumber',
        {
          ID_HouseholdNumber: ID_HouseholdNumber,
          IDs_Resident: IDs_Resident,
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      this.toastService.success(successMsg);

      this.refresh();

      res(obj);
    });
  }
}
