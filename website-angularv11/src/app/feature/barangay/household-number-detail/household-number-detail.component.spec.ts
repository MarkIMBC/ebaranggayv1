import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HouseholdNumberDetailComponent } from './household-number-detail.component';

describe('HouseholdNumberDetailComponent', () => {
  let component: HouseholdNumberDetailComponent;
  let fixture: ComponentFixture<HouseholdNumberDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HouseholdNumberDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HouseholdNumberDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
