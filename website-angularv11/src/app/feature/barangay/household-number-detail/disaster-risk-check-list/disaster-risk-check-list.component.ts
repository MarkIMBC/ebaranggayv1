import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';

import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import {
  HouseholdNumber_Disaster_DTO,
  IControlModelArg,
} from 'src/shared/APP_HELPER';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';

@Component({
  selector: 'disaster-risk-check-list',
  templateUrl: './disaster-risk-check-list.component.html',
  styleUrls: ['./disaster-risk-check-list.component.less'],
})
export class DisasterRiskCheckListComponent implements OnInit {
  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;

  @Output() onChanged = new EventEmitter<any>();

  tempID: number = 0;

  currentUser: TokenSessionFields = new TokenSessionFields();
  DisasterList: any[] = [];

  constructor(
    private userAuth: UserAuthenticationService,
    protected ds: DataService,
    protected cs: CrypterService
  ) {}

  ngOnInit(): void {
    this.currentUser = this.userAuth.getDecodedToken();

    this.loadDisasterList();
  }

  async loadDisasterList(): Promise<void> {
    this.DisasterList = [];

    var sql = this.cs.encrypt(
      `SELECT
              ID,
              Name,
              CONVERT(BIT, 0) IsChecked
       FROM dbo.tDisaster
    `
    );
    var __DisasterList: string[] = [];

    var objs = await this.ds.query<any>(sql);

    objs.forEach(function (obj) {
      __DisasterList.push(obj);
    });

    this.DisasterList = __DisasterList;
  }

  disaster_onModelChanged(disaster: any, e: IControlModelArg) {
    disaster.IsChecked = !disaster.IsChecked;

    this.onChanged.emit({
      Records: this.DisasterList,
    });
  }

  load(householdNumberDisasters: HouseholdNumber_Disaster_DTO[]) {
    this.DisasterList.forEach((disaster: any) => {
      householdNumberDisasters.forEach(
        (householdNumberDisaster: HouseholdNumber_Disaster_DTO) => {
          if (disaster.ID == householdNumberDisaster.ID_Disaster) {
            disaster.IsChecked = true;
          }
        }
      );
    });
  }
}
