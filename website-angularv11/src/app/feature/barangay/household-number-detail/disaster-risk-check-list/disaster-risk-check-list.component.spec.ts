import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisasterRiskCheckListComponent } from './disaster-risk-check-list.component';

describe('DisasterRiskCheckListComponent', () => {
  let component: DisasterRiskCheckListComponent;
  let fixture: ComponentFixture<DisasterRiskCheckListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisasterRiskCheckListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DisasterRiskCheckListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
