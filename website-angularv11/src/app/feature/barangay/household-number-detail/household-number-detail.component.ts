import { HeadOfFamilyTableComponent } from './head-of-family-table/head-of-family-table.component';
import { Disaster } from './../../../../shared/APP_MODELS';
import { IFormValidation, PropertyTypeEnum } from 'src/shared/APP_HELPER';
import {
  BaseDetailViewComponent,
  CurrentObjectOnValueChangeArg,
} from '../../base-detail-view/base-detail-view.component';
import { Component, ViewChild } from '@angular/core';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { DisasterRiskCheckListComponent } from './disaster-risk-check-list/disaster-risk-check-list.component';

@Component({
  selector: 'app-household-number-detail',
  templateUrl: './household-number-detail.component.html',
  styleUrls: [
    './../../base-detail-view/base-detail-view.component.less',
    './household-number-detail.component.less',
  ],
})
export class HouseholdNumberDetailComponent extends BaseDetailViewComponent {
  @ViewChild('disasterriskchecklist') disasterriskchecklist:
    | DisasterRiskCheckListComponent
    | undefined;

  @ViewChild('headoffamilytable') headoffamilytable:
    | HeadOfFamilyTableComponent
    | undefined;
  ModelName: string = 'HouseholdNumber';
  headerTitle: string = 'Household';
  displayMember: string = 'Name';

  AssignedBHW_ID_Employee_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM vEmployeeBarangayHealthWorker',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  Representative_ID_Resident_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: '',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  async validateDoDelete() {
    var validations: IFormValidation[] = [];

    var validateObj = await this.ds.execSP(
      'pHouseholdNumber_Delete_Validation',
      {
        ID_HouseholdNumber: this.CurrentObject.ID,
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
      }
    );

    if (validateObj.isValid != true) {
      validations.push({ message: validateObj.message });

      this.toastService.warning(validateObj.message);
    }

    return validateObj.isValid;
  }

  loadInitMenuItem() {
    this.menuItems.push(this._menuItem_New);

    if (this.CurrentObject.IsActive != true) {
      return;
    }

    if (this.CurrentObject.ID_Company != 1) {
      this.menuItems.push(this._menuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
    }

    if (this.CurrentObject.IsActive == true) {
      this.menuItems.push(this._menuItem_Delete);
    }
  }

  DetailView_onLoad() {
    var listViewOptionID_AssignedBHW_ID_Employee =
      this.AssignedBHW_ID_Employee_LookupboxOption.listviewOption;

    if (listViewOptionID_AssignedBHW_ID_Employee != undefined) {
      listViewOptionID_AssignedBHW_ID_Employee.sql = `/*encryptsqlstart*/
                                    SELECT
                                          ID,
                                          Name
                                    FROM dbo.vEmployeeBarangayHealthWorker
                                    WHERE
                                          ID_Company = ${this.currentUser.ID_Company}
                                    /*encryptsqlend*/`;
    }

    var _Representative_ID_Resident_LookupboxOption =
      this.Representative_ID_Resident_LookupboxOption.listviewOption;

    if (_Representative_ID_Resident_LookupboxOption != undefined) {
      _Representative_ID_Resident_LookupboxOption.sql = `
                                  /*encryptsqlstart*/
                                    SELECT *
                                    FROM dbo.vResident_Listview_HeadOfFamily
                                    WHERE
                                          ID_Company = ${this.currentUser.ID_Company} AND
                                          ID_HouseholdNumber = ${this.CurrentObject.ID}
                                  /*encryptsqlend*/`;
    }

    if (this.CurrentObject.HouseholdNumber_Disaster == null) {
      this.CurrentObject.HouseholdNumber_Disaster = [];
    }

    if (this.disasterriskchecklist) {
      this.disasterriskchecklist.load(
        this.CurrentObject.HouseholdNumber_Disaster
      );
    }

    if (this.headoffamilytable) {
      this.headoffamilytable.load(this.CurrentObject.ID);
    }
  }

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {
    this.CurrentObject = this.CurrentObject;
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    var Name = this.CurrentObject.Name;

    if (Name == undefined || Name == null) Name = '';

    return Promise.resolve(validations);
  }

  disasterriskchecklist_onChanged(e: any) {
    this.CurrentObject.HouseholdNumber_Disaster = [];

    var disasters: any[] = e.Records;

    disasters.forEach((disaster: any) => {
      if (disaster.IsChecked == true) {
        this.CurrentObject.HouseholdNumber_Disaster.push({
          ID_Disaster: disaster.ID,
          Name_Disaster: disaster.Name,
        });
      }
    });
  }
}
