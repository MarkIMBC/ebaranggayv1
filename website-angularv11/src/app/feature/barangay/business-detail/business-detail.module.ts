
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BusinessDetailRoutingModule } from './business-detail-routing.module';
import { BusinessDetailComponent } from './business-detail.component';

import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';

@NgModule({
  declarations: [BusinessDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    BusinessDetailRoutingModule,
  ]
})
export class BusinessDetailModule { }
