import { Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { FilterCriteriaType, IFormValidation, PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { BaseDetailViewComponent, CurrentObjectOnValueChangeArg } from '../../base-detail-view/base-detail-view.component';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';


@Component({
  selector: 'app-business-detail',
  templateUrl: './business-detail.component.html',
  styleUrls: [
    './../../base-detail-view/base-detail-view.component.less',
    './business-detail.component.html',
  ],
})
export class BusinessDetailComponent extends BaseDetailViewComponent {

  ModelName: string = 'Resident_Business'
  headerTitle: string = 'Negosyo';

  DetailView_onLoad() {


  }

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {


  }

  async menubar_OnClick(e: any) {
    if (e.item.name == 'ResidentBusinessReport') {
      this.customNavigate(['Report'], {
        ReportName: 'ResidentBusinessReport',
        filterValues: [
          {
            dataField: 'ID',
            filterCriteriaType: FilterCriteriaType.Equal,
            propertyType: PropertyTypeEnum.Int,
            value: this.CurrentObject.ID,
          },
        ],
      });
    }
  }
  async validation(): Promise<IFormValidation[]> {

    var validations: IFormValidation[] = [];

    var Name = this.CurrentObject.Name;
    var Resident_BusinessOwner = this.CurrentObject.Resident_BusinessOwner;
    var RequestedBy = this.CurrentObject.RequestedBy;


    if (Name == undefined || Name == null) Name = '';
    if (Resident_BusinessOwner == undefined || Resident_BusinessOwner == null) Resident_BusinessOwner = '';
    if (RequestedBy == undefined || RequestedBy == null) RequestedBy = '';

    if (Name.length == 0) {
      validations.push({
        message: "Pangalan ng Negosyo ay kailangan sagutan.",
      });
    }

    if (Resident_BusinessOwner.length == 0) {
      validations.push({
        message: "Pangalan ng May-ari Negosyo ay kailangan sagutan.",
      });
    }

    if (RequestedBy.length == 0) {
      validations.push({
        message: "Pangalan ng Nagrequest  ay kailangan sagutan.",
      });
    }


    return Promise.resolve(validations);
  }

}
