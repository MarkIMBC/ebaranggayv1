import { Component, OnInit } from '@angular/core';
import { BaseFeatureComponent } from '../base-feature/base-feature.component';

@Component({
  selector: 'app-municipality',
  templateUrl: './municipality.component.html',
  styleUrls: ['./municipality.component.less'],
})
export class MunicipalityComponent extends BaseFeatureComponent {}
