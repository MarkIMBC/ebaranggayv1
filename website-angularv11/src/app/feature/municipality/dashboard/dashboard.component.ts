import { Component, OnInit } from '@angular/core';
import { BaseViewComponent } from '../../base-view/base-view.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less'],
})
export class DashboardComponent extends BaseViewComponent {
  residentSummary: ResidentSummary = new ResidentSummary();

  async BaseView_OnInit() {
    await this.loadRecords();
  }

  async loadRecords() {
    var obj = await this.ds.execSP(
      'pGetDashboardMunicipalitySummary',
      {
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
      }
    );

    this.residentSummary = obj;
  }
}

class ResidentSummary {
  ResidentCount: number = 0;
  PermanentResidentCount: number = 0;
  MigrateResidentCount: number = 0;
  TrancientResidentCount: number = 0;
  ResidentCountAge60Above: number = 0;
  ResidentCountAge18to59: number = 0;
  ResidentCountAge10to17: number = 0;
  ResidentCountAge5to9: number = 0;
  ResidentCountAge24to84months: number = 0;
  ResidentCountAge12to23months: number = 0;
  ResidentCountAge0to11months: number = 0;
  ResidentCountMale: number = 0;
  ResidentCountFemale: number = 0;
  ResidentCount4Ps: number = 0;
  ResidentCountSeniorCitizen: number = 0;
  ResidentCountScholar: number = 0;
  VoterCount: number = 0;
}
