import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { ModalModule } from "src/app/shared/modal/modal.module";
import { SharedModule } from "src/app/shared/shared.module";
import { DashboardRoutingModule } from "./dashboard-routing.module";
import { DashboardComponent } from "./dashboard.component";
import { RegisteredResidentBarChartComponent } from "./registered-resident-bar-chart/registered-resident-bar-chart.component";
import { BarangayRegisteredResidetPieChartComponent } from './barangay-registered-residet-pie-chart/barangay-registered-residet-pie-chart.component';
import { TotalVaccinatedResidentBarChartComponent } from './total-vaccinated-resident-bar-chart/total-vaccinated-resident-bar-chart.component';
import { ProgramsPieChartComponent } from './programs-pie-chart/programs-pie-chart.component';
import { PregnancyDiagnosisBarChartComponent } from './pregnancy-diagnosis-bar-chart/pregnancy-diagnosis-bar-chart.component';

@NgModule({
  declarations: [DashboardComponent, RegisteredResidentBarChartComponent, BarangayRegisteredResidetPieChartComponent, TotalVaccinatedResidentBarChartComponent, ProgramsPieChartComponent, PregnancyDiagnosisBarChartComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    DashboardRoutingModule
  ]
})
export class DashboardModule { }
