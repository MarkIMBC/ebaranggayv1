import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  TemplateRef,
  ElementRef,
} from '@angular/core';
import { Chart } from 'chart.js';
import { Router } from '@angular/router';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { ToastService } from 'src/app/shared/toast.service';

@Component({
  selector: 'pregnancy-diagnosis-bar-chart',
  templateUrl: './pregnancy-diagnosis-bar-chart.component.html',
  styleUrls: ['./pregnancy-diagnosis-bar-chart.component.less'],
})
export class PregnancyDiagnosisBarChartComponent implements OnInit {
  currentUser: TokenSessionFields = new TokenSessionFields();
  dashboardinfochart: any;
  myChart: any;
  dataSource: any;
  MonthlySales: Array<number> = [];
  constructor(
    protected router: Router,
    protected elRef: ElementRef,
    protected ds: DataService,
    protected userAuth: UserAuthenticationService,
    protected toastService: ToastService,
    protected cs: CrypterService
  ) {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  @Input() customBodyTemplate!: TemplateRef<any>;

  async ngOnInit() {
    this.loadRecords();
    this.myChart = new Chart('DashboardMunicipalityPregnancyDiagnosis', {
      type: 'bar',

      data: {
        labels: [
          'January',
          'February',
          'March',
          'April',
          'May',
          'June',
          'July',
          'August',
          'September',
          'October',
          'November',
          'December',
        ],
        datasets: [
          {
            label: 'Taunang Diagnosis ng Pagbubuntis',
            data: this.MonthlySales,
            backgroundColor: [
              '#62BEB6',
              '#034D44',
              '#62BEB6',
              '#034D44',
              '#62BEB6',
              '#034D44',
              '#62BEB6',
              '#034D44',
              '#62BEB6',
              '#034D44',
              '#62BEB6',
              '#034D44',
            ],
            borderColor: [
              '#62BEB6',
              '#034D44',
              '#62BEB6',
              '#034D44',
              '#62BEB6',
              '#034D44',
              '#62BEB6',
              '#034D44',
              '#62BEB6',
              '#034D44',
              '#62BEB6',
              '#034D44',
            ],
            borderWidth: 1,
          },
        ],
      },

      options: {
        scales: {
          // y: {
          //     beginAtZero: true
          // }
        },
      },
    });
  }

  async loadRecords() {
    var obj = await this.ds.execSP(
      'pGetDashboardMunicipalityPregnancyDiagnosis',
      {
        ID_UserSession: this.currentUser.ID_UserSession,
        DateYear: '2022',
      },
      {
        isReturnObject: true,
      }
    );

    this.dataSource = obj.DataSource;
    var i;

    this.dataSource.forEach((obj: { TotalCount: number }) => {
      this.MonthlySales.push(obj.TotalCount);
    });

    console.log(this.MonthlySales);
    this.myChart.update();
  }
}
