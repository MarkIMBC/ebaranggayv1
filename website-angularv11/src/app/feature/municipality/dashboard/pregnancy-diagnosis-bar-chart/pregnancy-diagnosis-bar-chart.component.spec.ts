import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PregnancyDiagnosisBarChartComponent } from './pregnancy-diagnosis-bar-chart.component';

describe('PregnancyDiagnosisBarChartComponent', () => {
  let component: PregnancyDiagnosisBarChartComponent;
  let fixture: ComponentFixture<PregnancyDiagnosisBarChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PregnancyDiagnosisBarChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PregnancyDiagnosisBarChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
