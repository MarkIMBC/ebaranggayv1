import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramsPieChartComponent } from './programs-pie-chart.component';

describe('ProgramsPieChartComponent', () => {
  let component: ProgramsPieChartComponent;
  let fixture: ComponentFixture<ProgramsPieChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProgramsPieChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramsPieChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
