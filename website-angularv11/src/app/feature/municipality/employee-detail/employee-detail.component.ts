import { Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { IFormValidation, PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { BaseDetailViewComponent, CurrentObjectOnValueChangeArg } from '../../base-detail-view/base-detail-view.component';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: [
    './../../base-detail-view/base-detail-view.component.less',
    './employee-detail.component.less'
  ]
})
export class EmployeeDetailComponent extends BaseDetailViewComponent {

  ModelName: string = 'Employee'
  displayMember: string = "Name";

  ID_Position_LookupboxOption: AdminLTEDataLookupboxOption = {

    listviewOption: {
      sql: 'SELECT ID, Name FROM tPosition',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        }
      ]
    }
  }

  DetailView_onLoad() {

    this.headerTitle = this.CurrentObject.Name;
  }

  CurrentObject_onValueChange(e: CurrentObjectOnValueChangeArg) {

    if (this.CurrentObject['LastName'] == undefined || this.CurrentObject['LastName'] == null) this.CurrentObject['LastName'] = '';
    if (this.CurrentObject['FirstName'] == undefined || this.CurrentObject['FirstName'] == null) this.CurrentObject['FirstName'] = '';
    if (this.CurrentObject['MiddleName'] == undefined || this.CurrentObject['MiddleName'] == null) this.CurrentObject['MiddleName'] = '';

    this.CurrentObject.Name = this.CurrentObject['LastName']+ ', ' + this.CurrentObject['FirstName'] + ' ' + this.CurrentObject['MiddleName'];

    this.headerTitle = this.CurrentObject.Name;
  }

  async validation(): Promise<IFormValidation[]> {

    var validations: IFormValidation[] = [];

    var LastName = this.CurrentObject.LastName;
    var FirstName = this.CurrentObject.FirstName;
    var MiddleName = this.CurrentObject.MiddleName;
    var ID_Position = this.CurrentObject.ID_Position;

    if (LastName == undefined || LastName == null) LastName = '';
    if (FirstName == undefined || FirstName == null) FirstName = '';
    if (MiddleName == undefined || MiddleName == null) MiddleName = '';
    if (ID_Position == undefined || ID_Position == null) ID_Position = 0;

    if (LastName.length == 0) {
      validations.push({
        message: "Last Name is required.",
      });
    }

    if (FirstName.length == 0) {
      validations.push({
        message: "First Name is required.",
      });
    }

    if (ID_Position == 0) {
      validations.push({
        message: "Position is required.",
      });
    }

    return Promise.resolve(validations);
  }

}
