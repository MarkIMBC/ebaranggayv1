import { CommonModule } from '@angular/common';
import { APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { LayoutModule } from 'src/app/layout/layout.module';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { MunicipalityRoutingModule } from './municipality-routing.module';
import { MunicipalityComponent } from './municipality.component';
import { BaseMunicipalityDetailViewComponent } from './base-municipality-detail-view/base-municipality-detail-view.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { appInit } from 'src/app/app.module';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { LoaderInterceptor } from 'src/app/core/LoaderInterceptor';

@NgModule({
  declarations: [MunicipalityComponent, BaseMunicipalityDetailViewComponent],
  imports: [
    CommonModule,
    LayoutModule,
    ModalModule,
    SharedModule,
    MunicipalityRoutingModule,
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: appInit,
      multi: true,
      deps: [DataService],
    },
    CrypterService,
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class MunicipalityModule {}
