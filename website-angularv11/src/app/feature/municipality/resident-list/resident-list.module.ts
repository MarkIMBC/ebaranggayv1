import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResidentListRoutingModule } from './resident-list-routing.module';
import { ResidentListComponent } from './resident-list.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ResidentListComponent],
  imports: [
    CommonModule,
    SharedModule,
    ResidentListRoutingModule
  ]
})
export class ResidentListModule { }
