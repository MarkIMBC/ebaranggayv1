import { Component } from '@angular/core';
import { Item } from 'src/shared/APP_MODELS';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { BaseListViewComponent } from '../../base-list-view/base-list-view.component';
import { NavigationEnd } from '@angular/router';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';

@Component({
  selector: 'app-resident-list',
  templateUrl: './resident-list.component.html',
  styleUrls: [
    './../../base-list-view/base-list-view.component.less',
    './resident-list.component.less',
  ],
})
export class ResidentListComponent extends BaseListViewComponent {
  headerTitle: string = 'Mga Residente';

  CurrentObject: any = {
    Name: '',
    Name_ItemCategory: '',
    ID_VaccinatedStatus: null,
    Name_VaccinatedStatus: '',
  };

  ID_Vaccinated_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: `
          SELECT 1 ID, 'Nabakunahan' Name
          UNION ALL
          SELECT 2 ID, 'Hindi pa nabakunahan' Name
      `,
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };

  items: AdminLTEMenuItem[] = [
    {
      label: 'Refresh',
      icon: 'fas fa-sync',
      visible: true,
      command: () => {
        this.loadRecords();
        return true;
      },
    },
  ];

  dataSource: any[] = [];

  isLoadByNavigationEnd: boolean = false;

  async ListView_Onload() {
    this.router.events.subscribe(async (val) => {
      if (val instanceof NavigationEnd) {
        this.isLoadByNavigationEnd = true;
        await this.loadRecords();
      }
    });

    if (!this.isLoadByNavigationEnd) await this.loadRecords();
  }

  async loadRecords() {
    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = ' AND ' + filterString;

    sql = `/*encryptsqlstart*/
            SELECT
              *
            FROM vResident_Listview
            /*encryptsqlend*/
            WHERE
              IsActive = 1
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {
    var filterString = '';

    var _Guid_Company = this.route.snapshot.params['Guid_Company'];

    if (this.CurrentObject.Name.length > 0) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `Name LIKE '%${this.CurrentObject.Name}%'`;
    }

    if (this.CurrentObject.ID_VaccinatedStatus != null) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `ISNULL(IsVaccinated, 0) = ${
        this.CurrentObject.ID_VaccinatedStatus != 1 ? 0 : 1
      }`;
    }

    if (_Guid_Company != null) {
      if (filterString.length > 0) filterString += ' AND ';
      filterString += `ISNULL(Guid_Company, '') = '${_Guid_Company}'`;
    }

    return filterString;
  }

  Row_OnClick(rowData: Item) {
    var _Code_Company = this.route.snapshot.params['Code_Company'];

    this.customNavigate([_Code_Company, 'Resident', rowData.ID], {});
  }

  menuItem_New_onClick() {
    this.customNavigate(['Resident', -1], {});
  }
}
