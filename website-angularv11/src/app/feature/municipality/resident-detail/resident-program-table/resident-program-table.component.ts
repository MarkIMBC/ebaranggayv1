import { SQLListDialogComponent } from 'src/app/shared/control/sql-list-dialog/sql-list-dialog.component';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/core/UserAuthentication.service';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { GeneralfxService } from 'src/app/core/generalfx.service';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { Resident_Program_DTO, PropertyTypeEnum, IControlModelArg } from 'src/shared/APP_HELPER';

@Component({
  selector: 'resident-program-table',
  templateUrl: './resident-program-table.component.html',
  styleUrls: ['./resident-program-table.component.less']
})
export class ResidentProgramTableComponent implements OnInit {

  @ViewChild('sqllistdialog') sqllistdialog: SQLListDialogComponent | undefined;

  @Input() residentPrograms: Resident_Program_DTO[] = []

  tempID: number = 0;


  currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(private userAuth: UserAuthenticationService) { }

  ngOnInit(): void {

    this.currentUser = this.userAuth.getDecodedToken();
  }

  menuItems: AdminLTEMenuItem[] = [
    {
      label: 'Mga Programa',
      icon: 'fa fa-plus',
      visible: true
    }
  ];

  async doAddProgram() {

    if (this.sqllistdialog == undefined) return;

    var strIDs_currentIDPrograms = '';
    var IDs_Program: any[] = [];

    this.residentPrograms.forEach(residentProgram => IDs_Program.push(residentProgram.ID_Program));
    strIDs_currentIDPrograms = IDs_Program.length > 0 ?  IDs_Program.join(",") : '0';

    var obj: any;
    obj = await this.sqllistdialog.open({
      sql: `/*encryptsqlstart*/
            SELECT
              ID, Name
            FROM dbo.vProgram
            /*encryptsqlend*/
            WHERE
              ID NOT IN (${strIDs_currentIDPrograms}) AND
              IsActive = 1`,
      columns: [
        {
          name: 'Name',
          caption: 'Program',
          propertyType: PropertyTypeEnum.String
        },
      ]
    });

    this.tempID--;

    obj.rows.forEach((record: any) => {
      var item = {
        ID: this.tempID,
        ID_Program: record.ID,
        Name_Program: record.Name,
      };

      if (this.residentPrograms == null) this.residentPrograms = [];
      this.residentPrograms.push(item);
    });

  }

  async menuItems_OnClick(arg: any) {

    var menuItem: AdminLTEMenuItem = arg.item;

    if (menuItem.label == 'Mga Programa') {

      this.doAddProgram();
    }
  }

  async BtnDeleteSchedule_OnClick(ScheduleList: any) {

    var index = GeneralfxService.findIndexByKeyValue(
      this.residentPrograms,
      "ID",
      ScheduleList.ID + ""
    );

    this.residentPrograms.splice(index, 1);
  }


  residentProgram_onModelChanged(ScheduleList: any, e: IControlModelArg) {

    ScheduleList[e.name] = e.value;
  }
}
