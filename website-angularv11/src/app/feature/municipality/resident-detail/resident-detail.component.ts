import { Component, ViewChild } from '@angular/core';
import { AdminLTEDataLookupboxOption } from 'src/app/shared/control/admin-lte-data-lookupbox/admin-lte-data-lookupbox.component';
import { IFormValidation, ItemTypeEnum, PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { BaseMunicipalityDetailViewComponent } from '../base-municipality-detail-view/base-municipality-detail-view.component';

@Component({
  selector: 'app-resident-detail',
  templateUrl: './resident-detail.component.html',
  styleUrls: [
    './../../base-detail-view/base-detail-view.component.less',
    './resident-detail.component.less',
  ],
})
export class ResidentDetailComponent extends BaseMunicipalityDetailViewComponent {
  ModelName: string = 'Resident';
  headerTitle: string = 'Residente';

  displayMember: string = 'Name';

  ID_Gender_LookupboxOption: AdminLTEDataLookupboxOption = {
    listviewOption: {
      sql: 'SELECT ID, Name FROM tGender',
      columns: [
        {
          name: 'Name',
          caption: 'Name',
          propertyType: PropertyTypeEnum.String,
        },
      ],
    },
  };


  loadInitMenuItem() {
  //  this.menuItems.push(this._menuItem_New);

    if (this.CurrentObject.IsActive != true) return;

    this.menuItems.push(this._menuItem_Save);

    if (this.CurrentObject.ID > 0) {
      this.menuItems.push(this._menuItem_Refresh);
      this.menuItems.push(this._menuItem_Delete);
    }
  }

  DetailView_onLoad() {
    this.CurrentObject.ID_ItemType = ItemTypeEnum.Service;

    if (this.CurrentObject.Resident_Program == undefined)
      this.CurrentObject.Resident_Program = [];
  }

  async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    return Promise.resolve(validations);
  }

  public CurrentObject_onBeforeSaving() {
    if (this.CurrentObject.IsVaccinated !== true) {
      this.CurrentObject.DateLastVaccination = null;
    }
  }
}
