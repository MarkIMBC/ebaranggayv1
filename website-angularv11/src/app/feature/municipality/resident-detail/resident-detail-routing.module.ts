import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResidentDetailComponent } from './resident-detail.component';

const routes: Routes = [{ path: '', component: ResidentDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResidentDetailRoutingModule { }
