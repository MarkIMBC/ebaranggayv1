import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResidentDetailRoutingModule } from './resident-detail-routing.module';
import { ResidentDetailComponent } from './resident-detail.component';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/shared/modal/modal.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ResidentProgramTableComponent } from './resident-program-table/resident-program-table.component';
import { VaccinationInfoComponent } from './vaccination-info/vaccination-info.component';


@NgModule({
  declarations: [ResidentDetailComponent, ResidentProgramTableComponent, VaccinationInfoComponent],
  imports: [
    CommonModule,
    FormsModule,
    ModalModule,
    SharedModule,
    ResidentDetailRoutingModule
  ]
})
export class ResidentDetailModule { }
