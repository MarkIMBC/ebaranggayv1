import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MunicipalityComponent } from './municipality.component';

const routes: Routes = [
  {
    path: '',
    component: MunicipalityComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
      },

      {
        path: 'Dashboard',
        loadChildren: () =>
          import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
      },

      {
        path: 'CompanyDetail',
        loadChildren: () =>
          import('./company-detail/company-detail.module').then(
            (m) => m.CompanyDetailModule
          ),
      },

      {
        path: 'Employee/:ID_CurrentObject/:configOptions',
        loadChildren: () =>
          import('./employee-detail/employee-detail.module').then(
            (m) => m.EmployeeDetailModule
          ),
      },

      {
        path: 'Employee',
        loadChildren: () =>
          import('./employee-list/employee-list.module').then(
            (m) => m.EmployeeListModule
          ),
      },

      {
        path: 'ResidentList/:Code_Company/:Guid_Company',
        loadChildren: () =>
          import('./resident-list/resident-list.module').then(
            (m) => m.ResidentListModule
          ),
      },

      {
        path: ':Code_Company/Resident/:ID_CurrentObject/:configOptions',
        loadChildren: () =>
          import('./resident-detail/resident-detail.module').then(
            (m) => m.ResidentDetailModule
          ),
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MunicipalityRoutingModule {}
