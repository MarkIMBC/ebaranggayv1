import { Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { BaseDetailViewComponent } from '../../base-detail-view/base-detail-view.component';

@Component({
  selector: 'app-company-detail',
  templateUrl: './company-detail.component.html',
  styleUrls: [
    './../../base-detail-view/base-detail-view.component.less',
    './company-detail.component.less'
  ]
})
export class CompanyDetailComponent extends BaseDetailViewComponent {

  ModelName: string = 'Company'
  headerTitle: string = 'Company'

  displayMember: string = "Name";
  routerFeatureName: string = "CompanyDetail";

  protected async _getDefault__ID_CurrentObject() {

    var id: number = 0;
    if (this.currentUser != undefined) {

      if (this.currentUser.ID_Company != undefined) {

        id = this.currentUser.ID_Company;
      }
    }

    return id;
  }

  loadInitMenuItem() {

    if (this.CurrentObject.ID > 0) {

      this.menuItems.push(this._menuItem_Save);

      this.menuItems.push(this._menuItem_Refresh);
    }
  }

  protected redirectAfterSaved() {

    var routerFeatureName = this.model.Name;
    if (this.routerFeatureName.length > 0) routerFeatureName = this.routerFeatureName;

    var routeLink = [routerFeatureName];
    this.customNavigate(routeLink);
  }
}
