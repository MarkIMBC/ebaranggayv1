import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
} from '@angular/core';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from 'src/app/core/UserAuthentication.service';
import { CurrentObjectOnValueChangeArg } from 'src/app/feature/base-detail-view/base-detail-view.component';
import { IControlModelArg, IFormValidation } from 'src/shared/APP_HELPER';
import { ModalComponent } from '../modal/modal.component';
import { ToastService } from '../toast.service';

@Component({
  selector: 'change-user-password-dialog',
  templateUrl: './change-user-password-dialog.component.html',
  styleUrls: ['./change-user-password-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChangeUserPasswordDialogComponent implements OnInit {
  @ViewChild('modalDialog') modalDialog: ModalComponent | undefined;

  currentUser: TokenSessionFields = new TokenSessionFields();
  CurrentObject: any = {
    ID_User: 0,
    Password: '',
    ReTypePassword: '',
  };
  loading: boolean = false;

  constructor(
    private ds: DataService,
    private userAuth: UserAuthenticationService,
    public toastService: ToastService,
    private cs: CrypterService
  ) {}

  async ngOnInit(): Promise<void> {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  control_onModelChanged(e: IControlModelArg) {
    this.CurrentObject[e.name] = e.value;

    if (e.displayName != undefined) {
      this.CurrentObject[e.displayName] = e.displayValue;
    }

    var arg: CurrentObjectOnValueChangeArg = {
      name: e.name,
      value: e.value,
    };
  }

  async show(ID_User: number): Promise<any> {
    var modalDialog = this.modalDialog;
    this.CurrentObject.ID_User = ID_User;
    this.CurrentObject.Password = '';
    this.CurrentObject.ReTypePassword = '';

    this.loading = false;

    var promise = new Promise<any>(async (resolve, reject) => {
      if (modalDialog == undefined) {
        reject('no modal instantiate..');
      } else {
        this.loading = false;
        await modalDialog.open();
        resolve(null);
      }
    });

    return promise;
  }

  private async validateRecord() {
    var isValid = true;
    var validationsAppForm: IFormValidation[] = [];

    if (this.CurrentObject.Password != this.CurrentObject.ReTypePassword) {
      validationsAppForm.push({
        message: 'Re-typed password not matched..',
      });
    }

    if (validationsAppForm.length > 0) {
      this.toastService.warning(validationsAppForm[0].message);
      isValid = false;
    }

    return isValid;
  }

  async btnSave() {
    var isvalid = await this.validateRecord();

    if (!isvalid) return;

    this.loading = true;

    try {
      var obj: any = await this.changeUserPassword();

      if (obj.Success) {
        this.toastService.success(
          `User password has been changed successfully. The page is refreshing...`
        );
      } else {
        this.toastService.danger(obj.message);
      }
    } catch (error) {}

    this.loading = false;

    if (obj.Success) {
      if (this.modalDialog != undefined) {
        setTimeout(() => {

          window.location.reload();
        }, 2000);

        //this.modalDialog.close();
      }
    }
  }

  async changeUserPassword(): Promise<any> {
    var _ = this;

    return new Promise<any[]>(async (res, rej) => {
      this.loading = true;

      var obj = await this.ds.execSP(
        'pChangeUserPassword',
        {
          ID: _.CurrentObject.ID_User,
          NewPassword: _.CurrentObject.Password,
          ID_UserSession: _.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      this.loading = false;

      res(obj);
    });
  }

  btnClose_onClick() {
    if (this.modalDialog == undefined) return;
    this.modalDialog.close();
  }

  //Load User Information
}
