import { DatePipe, DecimalPipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import { AlignEnum, PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { AdminLTEListColumn } from '../AdminLTEListColumn';

@Pipe({name: 'adminltelistdataformat'})
export class AdminLTEListDataFormat implements PipeTransform {

  constructor(
    private decimalPipe: DecimalPipe,
    private datePipe: DatePipe
  ) { }
  transform(value: any, column: AdminLTEListColumn): any{

    var result = value;

    if(column.propertyType == PropertyTypeEnum.Date || column.propertyType == PropertyTypeEnum.DateTime){
      result = this.datePipe.transform(value, column.format);
    }else if(column.propertyType == PropertyTypeEnum.Int || column.propertyType == PropertyTypeEnum.Decimal){
      result = this.decimalPipe.transform(value, column.format);
    }

    return result;
  }
}
