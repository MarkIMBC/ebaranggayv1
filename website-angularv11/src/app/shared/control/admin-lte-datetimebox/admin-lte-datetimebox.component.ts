import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { format as formatDate } from 'date-fns';
import { AdminLTEBaseControlComponent } from '../admin-lte-base-control/admin-lte-base-control.component';

@Component({
  selector: 'admin-lte-datetimebox',
  templateUrl: './admin-lte-datetimebox.component.html',
  styleUrls: [
    './../admin-lte-base-control/admin-lte-base-control.component.less',
    './admin-lte-datetimebox.component.less'
  ],
  providers: [{ provide: AdminLTEBaseControlComponent, useExisting: forwardRef(() => AdminLTEDateTimeboxComponent) }]
})
export class AdminLTEDateTimeboxComponent extends AdminLTEBaseControlComponent{

  @Input() format: string = 'MM-dd-yyyy';
  @Input() valueformat: string = 'yyyy-MM-ddTHH:mm';

  _min: string = '';
  @Input() set min(val: string) {

    if(val == null || val == '') return;

    var result = formatDate(new Date(val), this.valueformat);
    this._min = result;
  }

  @Input() set initialValue(val: any) {

    if(val == null || val == '') return;

    this.setValue(val);
  }

  setValue(val: any){

    this.value = formatDate(new Date(val), this.valueformat);
  }
}
