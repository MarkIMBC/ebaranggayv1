import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { AdminLTEBaseControlComponent } from '../admin-lte-base-control/admin-lte-base-control.component';

@Component({
  selector: 'admin-lte-textarea',
  templateUrl: './admin-lte-textarea.component.html',
  styleUrls: ['./admin-lte-textarea.component.less'],
  providers: [{ provide: AdminLTEBaseControlComponent, useExisting: forwardRef(() => AdminLTETextareaComponent) }]
})
export class AdminLTETextareaComponent extends AdminLTEBaseControlComponent{

  @Input() rows: string = '10';
}
