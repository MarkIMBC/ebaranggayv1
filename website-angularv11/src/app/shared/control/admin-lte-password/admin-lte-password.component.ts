import {
  Component,
  forwardRef,
} from '@angular/core';
import { AdminLTEBaseControlComponent } from '../admin-lte-base-control/admin-lte-base-control.component';

@Component({
  selector: 'admin-lte-password',
  templateUrl: './admin-lte-password.component.html',
  styleUrls: ['./admin-lte-password.component.less'],
  providers: [
    {
      provide: AdminLTEBaseControlComponent,
      useExisting: forwardRef(() => AdminLTEPasswordComponent),
    },
  ],
})
export class AdminLTEPasswordComponent extends AdminLTEBaseControlComponent {}
