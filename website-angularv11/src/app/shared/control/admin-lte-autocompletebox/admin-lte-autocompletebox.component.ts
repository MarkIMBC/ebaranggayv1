import { AdminLTEBaseControlComponent } from './../admin-lte-base-control/admin-lte-base-control.component';
import { IControlModelArg } from './../../../../shared/APP_HELPER';
import { Component, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'admin-lte-autocompletebox',
  templateUrl: './admin-lte-autocompletebox.component.html',
  styleUrls: ['./admin-lte-autocompletebox.component.less'],
  providers: [{ provide: AdminLTEBaseControlComponent, useExisting: forwardRef(() => AdminLTEAutocompleteboxComponent) }]
})
export class AdminLTEAutocompleteboxComponent extends AdminLTEBaseControlComponent {

  @Input()
  dataList: string[] = [];

  afterLoad(): void {

    // if (this.input) {

    //   $(this.input.nativeElement).attr("list", this.idTag);
    // }
  }
}
