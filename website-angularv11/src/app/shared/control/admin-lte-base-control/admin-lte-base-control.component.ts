import { Component, ElementRef, EventEmitter, forwardRef, Input, OnInit, Output, ViewChild } from '@angular/core';
import { format as formatDate } from 'date-fns';
import { IControlModelArg } from 'src/shared/APP_HELPER';

@Component({
  selector: 'admin-lte-base-control',
  templateUrl: './admin-lte-base-control.component.html',
  styleUrls: ['./admin-lte-base-control.component.less'],
})
export class AdminLTEBaseControlComponent{

  @ViewChild('input')
  input: ElementRef | undefined;

  id: string = '';

  @Output() onEnterKeyUp: EventEmitter<IControlModelArg> = new EventEmitter();
  @Output() onModelChanged: EventEmitter<IControlModelArg> = new EventEmitter();

  @Input() name: string = '';
  @Input() displayName: string = '';
  @Input() caption: string = '';
  @Input() format: string = '';
  idTag: string = '';

  @Input() set initialValue(val: any) {

    this.value = val;
  }
  value: any = '';

  @Input() readonly: boolean = false;
  @Input() placeholder: string = '';

  constructor() {

    this.idTag = Math.floor(Math.random() * 99999) + formatDate(new Date(), 'yyyyMMddTHHmmss');
  }

  ngOnInit(): void {

  }

  _onModelChanged(event: any) {

    this.onModelChanged.emit({
      name: this.name,
      value: this.value
    });
  }

  onKey(event: any) {

    if(event.keyCode == 13){

      this.onEnterKeyUp.emit({
        name: this.name,
        value: this.value
      });
    }
  }

  ngAfterViewInit(){
    this.id = this.name;

    this.afterLoad();
  }

  afterLoad(): void { }

}
