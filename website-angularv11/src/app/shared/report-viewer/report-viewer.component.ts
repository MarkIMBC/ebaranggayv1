import { Component, ElementRef, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { DataService } from 'src/app/core/data.service';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/core/UserAuthentication.service';
import { IFilterFormValue, FilterCriteriaType, PropertyTypeEnum } from 'src/shared/APP_HELPER';
import { AdminLTEMenuItem } from '../AdminLTEMenuItem';
import * as $ from 'jquery';
@Component({
  selector: 'report-viewer',
  templateUrl: './report-viewer.component.html',
  styleUrls: ['./report-viewer.component.less']
})
export class ReportViewerComponent implements OnInit {

  @ViewChild("form") form: ElementRef | undefined;

  @Input() filterContentTemplate!: TemplateRef<any>;
  @Output() onbtnSaveClick: EventEmitter<any> = new EventEmitter();
  @Output() onbtnClearClick: EventEmitter<any> = new EventEmitter();
  @Output() onbtnRightClick: EventEmitter<any> = new EventEmitter();

  ID_Report: string = '';
  filterValues: IFilterFormValue[] = [];
  method: string = '';

  currentUser: TokenSessionFields = new TokenSessionFields();

  loading: boolean = false;
  cardBodyHeight: string = '700px';
  items: AdminLTEMenuItem[] = [
    {
      label: 'Refresh',
      visible: true,
      icon: 'fa fa-sync'
    }
  ]

  constructor(private userAuth: UserAuthenticationService ) { }

  ngOnInit(): void {

    this.currentUser = this.userAuth.getDecodedToken();
  }


  rightDropDownItems: AdminLTEMenuItem[] = [
    {
      label: `Export to PDF`,
      visible: true,
      name: 'exporttopdf'
    },
    {
      label: `Export to Excel`,
      visible: true,
      name: 'exporttoexcel'
    },
  ];

  loadRightDrowDownMenu() {

  }

  rightDropDown_onMainButtonClick() {

  }

  rightDropDown_onMenuItemButtonClick(event: any) {

    this.onbtnRightClick.emit(event);
  }

  onLoad(event: any): void { }

  menubar_OnClick(e: any){

    var menuItem: AdminLTEMenuItem =  e.item;

    if(menuItem.label == "Refresh"){

      this.onbtnSaveClick.emit();
    }
  }

  public load(ID_Report: string, filterValues: IFilterFormValue[]){

    this.ID_Report = ID_Report;
    this.filterValues = filterValues;
    this.method = "Viewer";

    this.refreshView();
  }

  public exportPDF(ID_Report: string, filterValues: IFilterFormValue[]){

    this.ID_Report = ID_Report;
    this.filterValues = filterValues;
    this.method = "Pdf";

    this.refreshView();
  }

  public exportExcel(ID_Report: string, filterValues: IFilterFormValue[]){

    this.ID_Report = ID_Report;
    this.filterValues = filterValues;
    this.method = "Excel";

    this.refreshView();
  }

  refreshView(): Promise<void> {

    if (this.form == undefined) return Promise.resolve();
    if(this.ID_Report == '') return Promise.resolve();

    var method: string = this.method;
    var form = $(this.form.nativeElement);
    var formFilters: IFilterFormValue[] = [];
    var formFilterInput = null;

    this.loading = true;

    this.filterValues.forEach((filterValue: IFilterFormValue) => {

      formFilters.push(filterValue);
    });

    formFilters = this.getDefaultFormFilter(formFilters);


    form.attr(
      "action",
      DataService.API_URL + `Report/${method}/${this.ID_Report}`
    );

    if (formFilters.length > 0) {
      formFilterInput = $(
        '<input type="hidden" name="filterValue" value="' +
        btoa(JSON.stringify(formFilters)) +
        '" />'
      );
      form.append(formFilterInput);
    } else {
      formFilterInput = $(
        '<input type="hidden" name="filterValue" value="" />'
      );
      form.append(formFilterInput);
    }

    form.submit();

    if (formFilterInput) formFilterInput.remove();

    this.loading = false;

    return Promise.resolve();
  }

  async ngAfterViewInit() {

    setTimeout(() => {

      this.resize();
    }, 1000);
  }

  protected getDefaultFormFilter(formFilters?: any): IFilterFormValue[] {

    formFilters.push({
      dataField: "ID_Company",
      filterCriteriaType: FilterCriteriaType.Equal,
      propertyType: PropertyTypeEnum.Int,
      value: this.currentUser.ID_Company
    });

    return formFilters;
  }

  private resize() {

    var height = window.innerHeight;
    var cardBodyHeight = 0;

    cardBodyHeight = height - 105;

    if (cardBodyHeight < 350) {

      cardBodyHeight = 400;
    }

    this.cardBodyHeight = cardBodyHeight.toString() + 'px';
  }

  btnSave_onClick(){

    this.onbtnSaveClick.emit();
  }

  btnClear_onClick(){

    this.onbtnClearClick.emit();
  }
}
