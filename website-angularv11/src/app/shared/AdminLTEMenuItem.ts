export class AdminLTEMenuItem {

  label: string = '';
  icon?: string = "";
  isShowIcon?: boolean = true;
  class?: string = '';
  visible: boolean = true;
  routerLink?: any ="";
  name?: string ="";
  badge?: AdminLTEMenuItemBadge
  isActive?: boolean = false;
  isOpen?: boolean = false;
  Oid_Model?: string = "";
  command?() {

  }
  items?: AdminLTEMenuItem[] = [];

  constructor(){
    this.class =  '';
  }
}

export class AdminLTEMenuItemBadge {

  caption: string = '';

  color?: string = "danger";
}


