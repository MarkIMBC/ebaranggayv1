import { AlignEnum, PropertyTypeEnum } from '../../shared/APP_HELPER';

export class AdminLTEListColumn {

  caption: string = '';
  name: string = '';
  propertyType: PropertyTypeEnum = PropertyTypeEnum.String;
  format?: string = '';
  align?: AlignEnum = AlignEnum.left;
  style?: any;
}


