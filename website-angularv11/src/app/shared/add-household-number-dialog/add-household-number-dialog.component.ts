import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewChild,
} from '@angular/core';
import { CrypterService } from 'src/app/core/crypter.service';
import { DataService } from 'src/app/core/data.service';
import { ModalComponent } from '../modal/modal.component';
import { ToastService } from '../toast.service';
import { from } from 'rxjs';
import { CurrentObjectOnValueChangeArg } from 'src/app/feature/base-detail-view/base-detail-view.component';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/core/UserAuthentication.service';
import { IControlModelArg, IFormValidation } from 'src/shared/APP_HELPER';

@Component({
  selector: 'add-household-number-dialog',
  templateUrl: './add-household-number-dialog.component.html',
  styleUrls: ['./add-household-number-dialog.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddHouseholdNumberDialogComponent implements OnInit {
  @ViewChild('modalDialog') modalDialog: ModalComponent | undefined;

  currentUser: TokenSessionFields = new TokenSessionFields();
  CurrentObject: any = {
    ID_HouseholdNumber: 0,
    Name: '',
  };
  loading: boolean = false;

  constructor(
    private ds: DataService,
    private userAuth: UserAuthenticationService,
    public toastService: ToastService,
    private cs: CrypterService
  ) {}

  async ngOnInit(): Promise<void> {
    this.currentUser = this.userAuth.getDecodedToken();
  }

  control_onModelChanged(e: IControlModelArg) {
    this.CurrentObject[e.name] = e.value;

    if (e.displayName != undefined) {
      this.CurrentObject[e.displayName] = e.displayValue;
    }

    var arg: CurrentObjectOnValueChangeArg = {
      name: e.name,
      value: e.value,
    };
  }

  async show(ID_HouseholdNumber: number): Promise<any> {
    var modalDialog = this.modalDialog;
    this.CurrentObject.ID_HouseholdNumber = ID_HouseholdNumber;

    var promise = new Promise<any>(async (resolve, reject) => {
      if (modalDialog == undefined) {
        reject('no modal instantiate..');
      } else {
        this.loading = false;
        await modalDialog.open();
        resolve(null);
      }
    });

    return promise;
  }

  private async validateRecord() {
    var isValid = true;
    var validationsAppForm: IFormValidation[] = [];

    if (this.CurrentObject.Password != this.CurrentObject.ReTypePassword) {
      validationsAppForm.push({
        message: 'Re-typed password not matched..',
      });
    }

    if (validationsAppForm.length > 0) {
      this.toastService.warning(validationsAppForm[0].message);
      isValid = false;
    }

    return isValid;
  }

  async btnSave() {
    var isvalid = await this.validateRecord();

    if (!isvalid) return;

    this.loading = true;

    try {
      var obj: any = this.changeHouseholdNumberPassword();

      if (obj.Success) {
        this.toastService.success(
          `HouseholdNumber password has been changed successfully.`
        );
      } else {
        this.toastService.danger(obj.message);
      }
    } catch (error) {}

    this.loading = false;

    if (obj.Success) {
      if (this.modalDialog != undefined) {
        this.modalDialog.close();
      }
    }
  }

  async changeHouseholdNumberPassword(): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        'pChangeHouseholdNumberPassword',
        {
          ID_HouseholdNumber: this.CurrentObject.ID_HouseholdNumber,
          Password: this.CurrentObject.Password,
          ID_UserSession:
            this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      res(obj);
    });
  }

  btnClose_onClick() {
    if (this.modalDialog == undefined) return;
    this.modalDialog.close();
  }

  //Load HouseholdNumber Information
}
