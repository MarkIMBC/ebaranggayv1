import { GeneralfxService } from 'src/app/core/generalfx.service';
import { CrypterService } from 'src/app/core/crypter.service';
import { Guid } from 'guid-typescript';
import { DataService } from './../../core/data.service';
import {
  TokenSessionFields,
  UserAuthenticationService,
} from './../../core/UserAuthentication.service';
import { SystemService } from './../../core/system.service';
import { Component, OnInit, isDevMode } from '@angular/core';
import { AdminLTEMenuItem } from 'src/app/shared/AdminLTEMenuItem';
import { Subscription } from 'rxjs';
import { PropertyTypeEnum, FilterCriteriaType } from 'src/shared/APP_HELPER';
import { Router } from '@angular/router';

@Component({
  selector: 'sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.less'],
})
export class SidemenuComponent implements OnInit {
  MenuItems: AdminLTEMenuItem[] = [];

  companyName: string = '';
  currentUser: TokenSessionFields = new TokenSessionFields();

  message: string = '';
  subscription: Subscription | undefined;

  constructor(
    private userAuth: UserAuthenticationService,
    private ds: DataService,
    protected router: Router,
    private systemService: SystemService,
    private cs: CrypterService
  ) {}

  async ngOnInit(): Promise<void> {
    this.currentUser = this.userAuth.getDecodedToken();
    if (
      this.currentUser.ID_User == undefined ||
      this.currentUser.ID_User == null
    ) {
      this.MenuItems = [];
      return;
    }

    this.ds
      .execSP(
        'pGetCompany',
        {
          ID: this.currentUser.ID_Company,
          ID_Session: this.currentUser.ID_UserSession,
        },
        { isReturnObject: true }
      )
      .then((obj) => {
        this.companyName = obj.Name;
      });

    switch (this.currentUser.MainRouteLink) {
      case 'Barangay':
        this.loadBarangayMenuItems();
        break;
      case 'BarangayHealthWorker':
        this.loadBarangayHealthWorkerMenuItems();
        break;
      case 'Municipality':
        this.loadMunicipalityMenuItems();
        break;
    }
  }

  loadBarangayMenuItems() {
    this.MenuItems.push({
      label: 'Dashboard',
      icon: 'fas fa-boxes',
      isOpen: false,
      visible: true,
      routerLink: 'Dashboard',
    });

    this.MenuItems.push({
      label: 'Mga Anunsyo',
      icon: 'fas fa-boxes',
      isOpen: false,
      visible: true,
      routerLink: 'AnnoucementList',
    });

    this.MenuItems.push({
      label: 'Transaksyon',
      icon: 'fas fa-boxes',
      isOpen: false,
      visible: true,
      items: [
        // {
        //   label: 'Mga Request',
        //   isOpen: false,
        //   routerLink: 'ResidentRequestTransactionList',
        //   visible: true,
        // },
        {
          label: 'Mga Residente',
          isOpen: false,
          routerLink: 'ResidentList',
          visible: true,
        },
        {
          label: 'Household',
          isOpen: false,
          routerLink: 'HouseholdList',
          visible: true,
        },
        {
          label: 'Head of the Family',
          isOpen: false,
          routerLink: 'ResidentListHeadOfFamily',
          visible: true,
        },
        {
          label: 'Mga Negosyo',
          isOpen: false,
          routerLink: 'Resident_BusinessList',
          visible: true,
        },
        {
          label: 'Mga Botante',
          isOpen: false,
          routerLink: 'ResidentListVoter',
          visible: true,
        },

        {
          label: 'Mga Senior Citizen',
          isOpen: false,
          routerLink: 'ResidentListSeniorCitizen',
          visible: true,
        },
        {
          label: 'Mga Solo Parent',
          isOpen: false,
          routerLink: 'ResidentListSoloParent',
          visible: true,
        },
        {
          label: 'Mga OFW',
          isOpen: false,
          routerLink: 'ResidentListOFW',
          visible: true,
        },
        {
          label: 'Mga PWD',
          isOpen: false,
          routerLink: 'ResidentListPWD',
          visible: true,
        },
        {
          label: 'Mga Boarder',
          isOpen: false,
          routerLink: 'ResidentListBoarder',
          visible: true,
        },

        {
          label: 'Mga Yumao',
          isOpen: false,
          routerLink: 'ResidentListDeceased',
          visible: true,
        },
      ],
    });

    this.MenuItems.push({
      label: 'Mga Programa',
      icon: 'fas fa-boxes',
      isOpen: false,
      visible: true,
      items: [
        // {
        //   label: 'Mga Request',
        //   isOpen: false,
        //   routerLink: 'ResidentRequestTransactionList',
        //   visible: true,
        // },

        {
          label: '4Ps',
          isOpen: false,
          routerLink: 'ProgramaPantawidPamilyaList',
          visible: true,
        },

        {
          label: 'Senior Citizen Pensionnaire',
          isOpen: false,
          routerLink: 'ProgramaSeniorCitizenList',
          visible: true,
        },
        {
          label: 'Scholar',
          isOpen: false,
          routerLink: 'ProgramaEducationalFinancialAssistanceList',
          visible: true,
        },

        {
          label: 'Solo Parent Incentives',
          isOpen: false,
          routerLink: 'ProgramaSoloParentList',
          visible: true,
        },
        {
          label: 'PWD Benificiaries',
          isOpen: false,
          routerLink: 'ProgramaPwdList',
          visible: true,
        },
        {
          label: 'GSIS',
          isOpen: false,
          routerLink: 'ProgramaGSISList',
          visible: true,
        },
        {
          label: 'SSS',
          isOpen: false,
          routerLink: 'ProgramaSSSList',
          visible: true,
        },
        {
          label: 'Philhealth',
          isOpen: false,
          routerLink: 'ProgramaPhiiHealthList',
          visible: true,
        },

        {
          label: 'Listahanan',
          isOpen: false,
          routerLink: 'ProgramaListahananList',
          visible: true,
        },
      ],
    });

    this.MenuItems.push({
      label: 'Certificate Module',
      icon: 'fas fa-boxes',
      isOpen: false,
      visible: true,
      items: [
        {
          label: 'Barangay Clearance',
          icon: 'fas fa-boxes',
          routerLink: 'BarangayClearanceList',
          isOpen: false,
          visible: true,
        },
        {
          label: 'Certificate Of Indigency',
          icon: 'fas fa-boxes',
          routerLink: 'CertificateOfIndigencyList',
          isOpen: false,
          visible: true,
        },
        {
          label: 'Certificate Of Residency',
          icon: 'fas fa-boxes',
          routerLink: 'CertificateOfResidencyList',
          isOpen: false,
          visible: true,
        },
        {
          label: 'Solo Parent',
          icon: 'fas fa-boxes',
          routerLink: 'SoloParentList',
          isOpen: false,
          visible: true,
        },
        {
          label: 'No Income',
          icon: 'fas fa-boxes',
          routerLink: 'CertificationOfNoIncomeList',
          isOpen: false,
          visible: true,
        },
      ],
    });

    this.MenuItems.push({
      label: 'Report',
      icon: 'fas fa-boxes',
      isOpen: false,
      visible: true,
      items: [
        // {
        //   label: 'Mga Request',
        //   isOpen: false,
        //   routerLink: 'ResidentRequestTransactionList',
        //   visible: true,
        // },
        {
          label: 'Residente',
          isOpen: false,
          routerLink: 'ResidentReport',
          visible: true,
        },

        {
          label: 'Mga Programa ',
          isOpen: false,
          routerLink: 'ResidentProgramReport',
          visible: true,
        },

        {
          label: 'Mga Botante ',
          isOpen: false,
          routerLink: 'VotersReport',
          visible: true,
        },
        {
          label: 'Resident Disaster Risk',
          isOpen: false,
          routerLink: 'ResidentDisasterRiskReport',
          visible: true,
        },
      ],
    });

    this.MenuItems.push({
      label: 'Katarungang Pambarangay',
      icon: 'fas fa-boxes',
      isOpen: false,
      visible: true,
      items: [
        {
          label: 'Mga Habla',
          isOpen: false,
          routerLink: 'LawSuitList',
          visible: true,
        },
        {
          label: 'Brgy. Complaint Form',
          icon: 'fas fa-download',
          isOpen: false,
          visible: true,
        },
        {
          label: 'Brgy. Summon Form',
          icon: 'fas fa-download',
          isOpen: false,
          visible: true,
        },
        {
          label: 'Brgy. Amicable Settlement',
          icon: 'fas fa-download',
          isOpen: false,
          visible: true,
        },
        {
          label: 'Certificate of Non Conciliation',
          icon: 'fas fa-download',
          isOpen: false,
          visible: true,
        },
      ],
    });

    this.MenuItems.push({
      label: 'Mga Batas',
      icon: 'fas fa-boxes',
      isOpen: false,
      visible: true,
      items: [
        {
          label: 'Katarungang Pambarangay',
          icon: 'fas fa-download',
          isOpen: false,
          visible: true,
        },
        {
          label: 'The Local Goverment Code RA-7160',
          icon: 'fas fa-download',
          isOpen: false,
          visible: true,
        },
      ],
    });

    this.MenuItems.push({
      label: 'Sangguniang Pambarangay',
      icon: 'fas fa-boxes',
      routerLink: 'Employee',
      isOpen: false,
      visible: true,
    });
  }

  async loadMunicipalityMenuItems() {
    var obj = await this.ds.execSP(
      'pGetBarangayList',
      {},
      { isReturnObject: true }
    );

    this.MenuItems.push({
      label: 'Dashboard',
      icon: 'fas fa-boxes',
      isOpen: false,
      visible: true,
      routerLink: 'Dashboard',
    });

    var barangayResidentList: AdminLTEMenuItem[] = [];

    obj.DataSource.forEach(
      (record: { Name: string; Code: string; Guid: string }) => {
        barangayResidentList.push({
          label: record.Name,
          isOpen: false,
          routerLink: `ResidentList/${record.Code}/${record.Guid}`,
          visible: true,
        });
      }
    );

    this.MenuItems.push({
      label: 'Resident',
      icon: 'fas fa-boxes',
      isOpen: false,
      visible: true,
      items: barangayResidentList,
    });
  }

  async loadBarangayHealthWorkerMenuItems() {
    var _ = this;

    this.MenuItems.push({
      label: 'Dashboard',
      icon: 'fas fa-boxes',
      isOpen: false,
      visible: true,
      routerLink: 'Dashboard',
    });

    this.MenuItems.push({
      label: 'Mga Programa',
      icon: 'fas fa-boxes',
      isOpen: false,
      visible: true,
      items: [
        {
          label: 'ECCD PROGRAM- SUPPLEMENTARY FEEDING PROGRAM 3-4.7YRS.OLD',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle:
                'ECCD PROGRAM- SUPPLEMENTARY FEEDING PROGRAM 3-4.7YRS.OLD',
            }
          ),
          visible: true,
          isShowIcon: true,
        },

        {
          label: 'BALIK-ESKWELA',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle: 'BALIK-ESKWELA',
            }
          ),
          visible: true,
          isShowIcon: true,
        },
        {
          label: 'EMERGENCY',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle: 'EMERGENCY-ESKWELA',
            }
          ),
          visible: true,
          isShowIcon: true,
        },

        {
          label: 'Medical-financial',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle: 'Medical-financial',
            }
          ),
          visible: true,
          isShowIcon: false,
        },
        {
          label: 'Financial-Burial (5000)',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle: 'Financial-Burial (5000)',
            }
          ),
          visible: true,
          isShowIcon: false,
        },
        {
          label: 'Disaster Victim (assistance) Emergency Relief',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle: 'Disaster Victim (assistance) Emergency Relief',
            }
          ),
          visible: true,
          isShowIcon: false,
        },
        {
          label: 'Burial',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle: 'Burial',
            }
          ),
          visible: true,
          isShowIcon: false,
        },
        {
          label: 'Pre-marriage Counselling (Magpapakasal)',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle: 'Pre-marriage Counselling (Magpapakasal)',
            }
          ),
          visible: true,
        },

        {
          label: 'Senior Citizen',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle: 'Senior Citizen',
            }
          ),
          visible: true,
        },

        {
          label: 'Pondoy Damayan(Mutual Fund ₱60./year)',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle: 'Pondoy Damayan(Mutual Fund ₱60./year)',
            }
          ),
          visible: true,
          isShowIcon: false,
        },

        {
          label: 'National Pensioner',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle: 'National Pensioner',
            }
          ),
          visible: true,
          isShowIcon: false,
        },
        {
          label: 'Local Pensioner',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle: 'Local Pensioner',
            }
          ),
          visible: true,
          isShowIcon: false,
        },

        {
          label: 'Solo Parent',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle: 'Solo Parent',
            }
          ),
          visible: true,
        },

        {
          label: 'Extended Benefit Allowance',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle: 'Extended Benefit Allowance',
            }
          ),
          visible: true,
          isShowIcon: false,
        },

        {
          label: 'Urban Poor',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle: 'Urban Poor',
            }
          ),
          visible: true,
        },
        {
          label: 'Basic Utilities Support',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle: 'Basic Utilities Support',
            }
          ),
          visible: true,
          isShowIcon: false,
        },

        {
          label: 'Local Housing Program',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle: 'Local Housing Program',
            }
          ),
          visible: true,
          isShowIcon: false,
        },

        {
          label: 'Water Meter',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle: 'Water Meter',
            }
          ),
          visible: true,
          isShowIcon: false,
        },

        {
          label: 'Electric Meter',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle: 'Electric Meter',
            }
          ),
          visible: true,
          isShowIcon: false,
        },

        {
          label: 'Extended Benefit Allowance',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle: 'Extended Benefit Allowance',
            }
          ),
          visible: true,
          isShowIcon: false,
        },
        {
          label: 'Educational Assistance',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle: 'Educational Assistance',
            }
          ),
          visible: true,
        },
        {
          label: 'TUGON Program(Elem-HighSchool)',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle: 'TUGON Program(Elem-HighSchool)',
            }
          ),
          visible: true,
          isShowIcon: false,
        },
        {
          label: 'Scholarship(SH-College)',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle: 'Scholarship(SH-College)',
            }
          ),
          visible: true,
          isShowIcon: false,
        },
        {
          label: 'Educational-Financial(SH-College)',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle: 'Educational-Financial(SH-College)',
            }
          ),
          visible: true,
          isShowIcon: false,
        },
        {
          label: 'Educational Assistance',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle: 'Educational Assistance',
            }
          ),
          visible: true,
        },
        {
          label: 'Empowering better aid for PWD',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle: 'Empowering better aid for PWD',
            }
          ),
          visible: true,
          isShowIcon: false,
        },
        {
          label: 'Enrolled at DTC-Lipa (Provincial Training Center - Lipa)',
          isOpen: false,
          routerLink: GeneralfxService.getNavigate(
            _.cs,
            _.currentUser,
            ['ResidentList'],
            {
              headerTitle:
                'Enrolled at DTC-Lipa (Provincial Training Center - Lipa)',
            }
          ),
          visible: true,
        },
      ],
    });
  }

  async addMenuItem(menuItem: AdminLTEMenuItem, routerLink: any) {
    var _ = this;

    if (routerLink != null) {
      menuItem.routerLink = routerLink;
    }

    this.MenuItems.push(menuItem);
  }
  async loadReportMenuItems() {
    var obj = await this.ds.execSP(
      'pGetRecordPaging',
      {
        sql: `SELECT * FROm vResidentDetailReportList`,
        PageNumber: 1,
        DisplayCount: 100,
        OrderByString: 'label ASC',
      },
      {
        isReturnObject: true,
      }
    );

    this.MenuItems.push({
      label: 'Resident Reports',
      icon: 'fas fa-boxes',
      routerLink: 'Employee',
      isOpen: false,
      visible: true,
      items: obj.Records,
    });
  }

  ngOnDestroy() {}

  async menu_click(menu: any) {
    if (menu.label == 'Brgy. Complaint Form') {
      window.location.href = './assets/forms/Brgy_Complaint_Form.docx';
    } else if (menu.label == 'Brgy. Summon Form') {
      window.location.href = './assets/forms/Brgy_Summon.docx';
    } else if (menu.label == 'Brgy. Amicable Settlement') {
      window.location.href = './assets/forms/Brgy_Amicable_Settlement.docx';
    } else if (menu.label == 'Certificate of Non Conciliation') {
      window.location.href =
        './assets/forms/Brgy_Certificate_of_Non_Conciliation.docx';
    } else if (menu.label == 'Certificate of Non Conciliation') {
      window.location.href =
        './assets/forms/Brgy_Certificate_of_Non_Conciliation.docx';
    } else if (menu.label == 'Katarungang Pambarangay') {
      window.location.href = './assets/forms/KatarungangPambarangay.pdf';
    } else if (menu.label == 'The Local Goverment Code RA-7160') {
      window.location.href = './assets/forms/TheLocalGovermentCodeRA-7160.pdf';
    } else if (menu.name.includes('ModelReport')) {
      GeneralfxService.customNavigate(
        this.router,
        this.cs,
        this.currentUser,
        ['Report', menu.reportname],
        {
          ReportName: menu.reportname,
          filterValues: [
            {
              dataField: 'ID',
              filterCriteriaType: FilterCriteriaType.Equal,
              propertyType: PropertyTypeEnum.Int,
              value: 15,
            },
          ],
        }
      );
    }
  }
}
