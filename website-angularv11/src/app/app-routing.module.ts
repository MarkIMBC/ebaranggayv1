import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './defaultpage/page-not-found/page-not-found.component';
import { StarterComponent } from './feature/starter/starter.component';

const routes: Routes = [

  { path: '', component: StarterComponent},

  { path: 'Login', component: LoginComponent},

  { path: 'Starter', loadChildren: () => import('./feature/starter/starter.module').then(m => m.StarterModule) },

  { path: 'PageNotFound', component: PageNotFoundComponent },

  //{ path: 'Report/:configOptions', loadChildren: () => import('./feature/report/basereport.module').then(m => m.ReportModule) },

  { path: 'Barangay', loadChildren: () => import('./feature/barangay/barangay.module').then(m => m.BarangayModule) },

  { path: 'Municipality', loadChildren: () => import('./feature/municipality/municipality.module').then(m => m.MunicipalityModule) },

  { path: 'BusinessDetail', loadChildren: () => import('./feature/barangay/business-detail/business-detail.module').then(m => m.BusinessDetailModule) },

  { path: 'BarangayHealthWorker', loadChildren: () => import('./feature/barangay-health-worker/barangay-health-worker.module').then(m => m.BarangayHealthWorkerModule) },

  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [
      RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
