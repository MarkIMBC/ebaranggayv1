import { Injectable } from '@angular/core';
import jwtDecode from 'jwt-decode';
import { CrypterService } from './crypter.service';
import { DataService } from './data.service';
import jwt_decode from 'jwt-decode';
@Injectable({
  providedIn: 'root',
})
export class UserAuthenticationService {
  constructor(private ds: DataService, private ck: CrypterService) {}

  isNullOrUndefined<T>(obj: T | null | undefined): obj is null | undefined {
    return typeof obj === 'undefined' || obj === null;
  }

  LogIn(username: string, password: string): Promise<IUserSession> {
    if (password) {
      if (password.length > 0) {
        password = this.ck.encrypt(password);
      }
    }

    return new Promise<IUserSession>((res, rej) => {
      this.ds
        .post('Login/Token', {
          Username: username,
          Password: password,
        })
        .then((r: any) => {
          window.localStorage.setItem('STORTKN', r.Token);
          window.localStorage.setItem('STORDNTL', r.EncryptToken);

          res(r);

          return Promise.resolve(r);
        })
        .catch((err) => {
          rej(err);
          return Promise.reject(err);
        });
    });
  }

  LogOut(): Promise<IUserSession> {
    var session = this.getDecodedToken();
    var ID_Session = session.ID_UserSession;

    return new Promise<IUserSession>((res, rej) => {
      this.ds
        .get('Login/LogOut/' + ID_Session)
        .then((r) => {
          window.localStorage.clear();
          return Promise.resolve(res);
        })
        .catch((err) => {
          return Promise.resolve(err);
        });
    });
  }

  async CheckSession(): Promise<ICheckSession> {
    var session = this.getDecodedToken();

    var res: ICheckSession = {
      ID: -1,
      ID_Warehouse: -1,
      IsValid: false,
    };
    if (session == null) {
      return Promise.resolve(res);
    }
    //res =  await this.ds.get(`Login/CheckSession`) as ICheckSession;
    return new Promise<ICheckSession>((resolve, reject) => {
      this.ds.get(`Login/CheckSession`).then(
        (d) => {
          return Promise.resolve(res);
        },
        (r) => {
          return Promise.reject(res);
        }
      );
    });
  }

  getCurrentSession(): IUserSession {
    var str: any = '';
    str = window.localStorage.getItem('STORDNTL');

    if (this.isNullOrUndefined(str) == true) return new IUserSession();
    var user = JSON.parse(str) as IUserSession;

    return user;
  }

  getDecodedToken(): TokenSessionFields {
    var str: string = '';

    var STORDNTL = window.localStorage.getItem('STORDNTL');

    if (STORDNTL != null) {
      str = STORDNTL;
    }
    str = this.ck.decrypt(str);

    try {
      var obj: TokenSessionFields = JSON.parse(str);

      obj.ID_UserSession = obj.ID_Session;

      return obj;
    } catch (Error) {
      return new TokenSessionFields();
    }
  }
}

export class IUserSession {
  ID?: number;
  ID_Employee?: number;
  FirstName?: string;
  LastName?: string;
  ID_UserSession?: number;
  Token?: string;
}

export class TokenSessionFields {
  ID_Company?: number = 0;
  ID_User?: number = 0;
  ID_UserGroup?: number = 0;
  ID_Employee?: number = 0;
  LastName?: string;
  FirstName?: string;
  Name_Employee?: string;
  ID_Position?: number = 0;
  ID_Session?: number = 0;
  ID_UserSession?: number = 0;
  Password?: string;
  MainRouteLink?: string = '';
}

export class ICheckSession {
  ID?: number;
  ID_Warehouse?: number;
  IsValid?: boolean;
}
