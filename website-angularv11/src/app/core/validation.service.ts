import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  constructor() { }

  validateEmail(email : string){

    var emailPattern =/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    return emailPattern.test(email);
  }

  validatePHMobileNumber(contactNumber : string){

    var pattern = /^(09|\+639|639)\d{9}$/gm;
    var result = pattern.test(contactNumber);

    return result;
  }
}
