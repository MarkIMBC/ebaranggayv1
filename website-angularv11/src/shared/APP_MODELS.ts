
    //THIS IS FILE GENERATED. PLEASE DON'T EDIT
    //Jefrey Sambile :)
    
            export class Announcement {
                DateModified?:Date;
Name?:string;
IsActive?:boolean;
DateCreated?:Date;
Code?:string;
ID?:number;
ID_LastModifiedBy?:number;
Comment?:string;
ID_Company?:number;
ID_CreatedBy?:number;
DateStart?:Date;
DateEnd?:Date;

            }
        
            export class Appointment {
                ID_CreatedBy?:number;
Comment?:string;
DateModified?:Date;
Code?:string;
Name?:string;
ID?:number;
DateCreated?:Date;
ID_LastModifiedBy?:number;
ID_Company?:number;
IsActive?:boolean;
DateStart?:Date;
DateEnd?:Date;
ID_Client?:number;
ID_Patient?:number;
ID_Doctor?:number;
Description?:string;

            }
        
            export class AppointmentRequest {
                ID_CreatedBy?:number;
DateModified?:Date;
Comment?:string;
ID_Company?:number;
Code?:string;
ID?:number;
DateCreated?:Date;
IsActive?:boolean;
ID_LastModifiedBy?:number;
Name?:string;
ID_Patient?:number;
Reason?:string;
DateStart?:Date;
DateEnd?:Date;

            }
        
            export class AppointmentSchedule {
                ID_LastModifiedBy?:number;
DateCreated?:Date;
Code?:string;
DateModified?:Date;
Comment?:string;
ID_Company?:number;
ID_CreatedBy?:number;
Name?:string;
IsActive?:boolean;
ID?:number;
ID_Schedule?:number;
ID_ScheduleType?:number;
DateStart?:Date;
DateEnd?:Date;
ID_Doctor?:number;
ID_Patient?:number;
ID_ServiceType?:number;
AppointmentStatus_ID_FilingStatus?:number;

            }
        
            export class AppointmentStatusLog {
                DateModified?:Date;
ID_Company?:number;
Comment?:string;
DateCreated?:Date;
ID_CreatedBy?:number;
Code?:string;
Name?:string;
IsActive?:boolean;
ID_LastModifiedBy?:number;
ID?:number;
Oid_Model?:string;
Appointment_ID_CurrentObject?:number;
Appointment_ID_FilingStatus?:number;

            }
        
            export class ApproverMatrix {
                ID?:number;
Code?:string;
Comment?:string;
ID_CreatedBy?:number;
IsActive?:boolean;
DateCreated?:Date;
Name?:string;
DateModified?:Date;
ID_LastModifiedBy?:number;
ApproverMatrix_Detail?:ApproverMatrix_Detail[];

            }
        
            export class ApproverMatrix_Detail {
                IsActive?:boolean;
Name?:string;
ID_LastModifiedBy?:number;
Code?:string;
ID_CreatedBy?:number;
Comment?:string;
ID?:number;
DateCreated?:Date;
DateModified?:Date;
ID_ApproverMatrix?:number;

            }
        
            export class AppSetting {
                DecimalValue?:number;
ID?:number;
DateCreated?:Date;
DateModified?:Date;
IsActive?:boolean;
BoolValue?:boolean;
StringValue?:string;
ID_CreatedBy?:number;
Code?:string;
DateTimeValue?:Date;
ImageValue?:string;
ID_PropertyType?:number;
IntValue?:number;
Name?:string;
ID_LastModifiedBy?:number;
Comment?:string;
ColorValue?:string;

            }
        
            export class AuditTrail {
                Date?:Date;
ID_DetailView?:number;
ID_AuditType?:number;
ID_User?:number;
ID_Session?:number;
Name?:string;
ID_CurrentObject?:string;
IsActive?:boolean;
Comment?:string;
ID?:number;
Code?:string;
ID_Model?:string;
AuditTrail_Detail?:AuditTrail_Detail[];
Description?:string;
DateCreated?:Date;
ID_CreatedBy?:number;

            }
        
            export class AuditTrail_Detail {
                Code?:string;
IsActive?:boolean;
Name?:string;
OldValue?:string;
NewValue?:string;
ID?:number;
ID_AuditTrail?:number;
ID_Model?:number;
ID_CurrentObject?:number;
Comment?:string;
ModelProperty?:string;
ID_AuditTrailType?:number;

            }
        
            export class AuditTrailType {
                Code?:string;
ID?:number;
Comment?:string;
Name?:string;
IsActive?:boolean;

            }
        
            export class BarangayClearance {
                Name?:string;
ID_Company?:number;
DateModified?:Date;
ID_LastModifiedBy?:number;
ID?:number;
ID_CreatedBy?:number;
Code?:string;
Comment?:string;
DateCreated?:Date;
IsActive?:boolean;
Date?:Date;
Age?:string;
ID_Gender?:number;
ID_CivilStatus?:number;
Address?:string;
Purpose?:string;
ImageProfilePicFilename?:string;
Requestor?:string;
Citizenship?:string;

            }
        
            export class BillingInvoice {
                ID_Company?:number;
Comment?:string;
IsActive?:boolean;
ID?:number;
ID_LastModifiedBy?:number;
DateModified?:Date;
Code?:string;
Name?:string;
ID_CreatedBy?:number;
DateCreated?:Date;
ID_Patient?:number;
BillingInvoice_Detail?:BillingInvoice_Detail[];
ID_TaxScheme?:number;
Date?:Date;
BillingAddress?:string;
VatAmount?:number;
DiscountAmount?:number;
GrossAmount?:number;
VatPercentage?:number;
NetAmount?:number;
ID_FilingStatus?:number;
ID_ApprovedBy?:number;
ID_CanceledBy?:number;
DateApproved?:Date;
DateCanceled?:Date;
Payment_ID_FilingStatus?:number;
RemainingAmount?:number;
ID_Client?:number;
Discount?:number;
DiscountRate?:number;
IsComputeDiscountRate?:boolean;
SubTotal?:number;
TotalAmount?:number;
AttendingPhysician_ID_Employee?:number;
ID_SOAPType?:number;
ID_Patient_SOAP?:number;
ID_Patient_Confinement?:number;
BillingInvoice_Patient?:BillingInvoice_Patient[];
PatientNames?:string;
ConfinementDepositAmount?:number;
RemainingDepositAmount?:number;
ConsumedDepositAmount?:number;
InitialSubtotalAmount?:number;
ID_Patient_Vaccination?:number;
IsWalkIn?:boolean;
WalkInCustomerName?:string;
ID_Patient_Wellness?:number;
TotalItemDiscountAmount?:number;
OtherReferenceNumber?:string;

            }
        
            export class BillingInvoice_Detail {
                Code?:string;
IsActive?:boolean;
Name?:string;
ID?:number;
Comment?:string;
ID_BillingInvoice?:number;
ID_Item?:number;
Quantity?:number;
Amount?:number;
UnitPrice?:number;
DateExpiration?:Date;
UnitCost?:number;
DiscountAmount?:number;
IsComputeDiscountRate?:boolean;
DiscountRate?:number;
ID_Patient_Confinement_ItemsServices?:number;
ID_Patient_Vaccination?:number;
ID_Patient_Wellness_Detail?:number;

            }
        
            export class BillingInvoice_Patient {
                ID_CreatedBy?:number;
ID_Company?:number;
DateModified?:Date;
ID_LastModifiedBy?:number;
DateCreated?:Date;
Comment?:string;
ID?:number;
Code?:string;
Name?:string;
IsActive?:boolean;
ID_BillingInvoice?:number;
ID_Patient?:number;
ID_Patient_Confinement_Patient?:number;

            }
        
            export class BillingInvoiceWalkIn {
                ID_Company?:number;
Name?:string;
Code?:string;
ID_LastModifiedBy?:number;
ID_CreatedBy?:number;
ID?:number;
DateCreated?:Date;
IsActive?:boolean;
DateModified?:Date;
Comment?:string;

            }
        
            export class BillingInvoiceWalkInList {
                DateCreated?:Date;
Code?:string;
ID_LastModifiedBy?:number;
ID?:number;
IsActive?:boolean;
ID_CreatedBy?:number;
ID_Company?:number;
Comment?:string;
Name?:string;
DateModified?:Date;

            }
        
            export class BreedSpecie {
                IsActive?:boolean;
ID?:number;
Comment?:string;
Name?:string;
ID_LastModifiedBy?:number;
Code?:string;
DateCreated?:Date;
ID_CreatedBy?:number;
DateModified?:Date;
ID_Company?:number;

            }
        
            export class CaseType {
                ID_Company?:number;
DateModified?:Date;
ID_CreatedBy?:number;
ID_LastModifiedBy?:number;
ID?:number;
Comment?:string;
Name?:string;
IsActive?:boolean;
DateCreated?:Date;
Code?:string;

            }
        
            export class CertificateOfIndigency {
                ID_CreatedBy?:number;
Name?:string;
DateModified?:Date;
ID?:number;
Code?:string;
ID_Company?:number;
ID_LastModifiedBy?:number;
DateCreated?:Date;
IsActive?:boolean;
Comment?:string;
Date?:Date;
Age?:string;
Citizenship?:string;
Address?:string;
RequestOf?:string;
ID_Gender?:number;
ID_CivilStatus?:number;
Requestor?:string;

            }
        
            export class CertificateOfResidency {
                Code?:string;
DateCreated?:Date;
ID_Company?:number;
DateModified?:Date;
ID_LastModifiedBy?:number;
ID_CreatedBy?:number;
Name?:string;
ID?:number;
IsActive?:boolean;
Comment?:string;
Age?:string;
Citizenship?:string;
Address?:string;
StayInDuration?:string;
Requestor?:string;
Date?:Date;
ImageProfilePicFilename?:string;
Purpose?:string;
ID_Gender?:number;
ID_CivilStatus?:number;

            }
        
            export class CertificationOfNoIncome {
                IsActive?:boolean;
DateCreated?:Date;
Comment?:string;
Name?:string;
DateModified?:Date;
ID?:number;
ID_Company?:number;
ID_LastModifiedBy?:number;
Code?:string;
ID_CreatedBy?:number;
Date?:Date;
Age?:string;
Citizenship?:string;

            }
        
            export class CivilStatus {
                Comment?:string;
DateModified?:Date;
ID_Company?:number;
ID_LastModifiedBy?:number;
Name?:string;
ID?:number;
IsActive?:boolean;
ID_CreatedBy?:number;
DateCreated?:Date;
Code?:string;
TranslatedEngName?:string;

            }
        
            export class Client {
                ID_LastModifiedBy?:number;
ID_Company?:number;
Name?:string;
IsActive?:boolean;
ID?:number;
DateCreated?:Date;
Code?:string;
ID_CreatedBy?:number;
DateModified?:Date;
Comment?:string;
ContactNumber?:string;
Email?:string;
Address?:string;
ContactNumber2?:string;
Old_client_id?:number;
tempID?:string;
DateLastVisited?:Date;
CurrentCreditAmount?:number;
CustomCode?:string;

            }
        
            export class Client_CreditLogs {
                ID_Company?:number;
Name?:string;
ID_CreatedBy?:number;
IsActive?:boolean;
Comment?:string;
ID_LastModifiedBy?:number;
Code?:string;
DateModified?:Date;
DateCreated?:Date;
ID?:number;
ID_Patient?:number;
Date?:Date;
CreditAmount?:number;
ID_Client?:number;
RemainingAmount?:number;

            }
        
            export class ClientDeposit {
                IsActive?:boolean;
ID?:number;
Code?:string;
ID_Company?:number;
ID_LastModifiedBy?:number;
Comment?:string;
ID_CreatedBy?:number;
DateModified?:Date;
DateCreated?:Date;
Name?:string;
ID_Client?:number;
ID_FilingStatus?:number;
ID_Patient_Confinement?:number;
Date?:Date;
ID_ApprovedBy?:number;
DateApproved?:Date;
ID_CanceledBy?:number;
DateCanceled?:Date;
DepositAmount?:number;

            }
        
            export class ClientWithdraw {
                ID_Company?:number;
Name?:string;
ID_CreatedBy?:number;
Code?:string;
DateModified?:Date;
ID?:number;
Comment?:string;
DateCreated?:Date;
IsActive?:boolean;
ID_LastModifiedBy?:number;
ID_Client?:number;
ID_FilingStatus?:number;
ID_Patient_Confinement?:number;
Date?:Date;
ID_ApprovedBy?:number;
DateApproved?:Date;
ID_CanceledBy?:number;
DateCanceled?:Date;
WithdrawAmount?:number;

            }
        
            export class ColumnAlignment {
                Comment?:string;
ID?:number;
Name?:string;
IsActive?:boolean;
Code?:string;

            }
        
            export class Company {
                ID?:number;
DateModified?:Date;
IsActive?:boolean;
Name?:string;
DateCreated?:Date;
ID_LastModifiedBy?:number;
Comment?:string;
ID_CreatedBy?:number;
Code?:string;
ID_Country?:number;
Address?:string;
ImageLogoFilename?:string;
ContactNumber?:string;
ImageHeaderFilename?:string;
Email?:string;
IsShowPOSReceiptLogo?:boolean;
SOAPPlanSMSMessage?:string;
IsRemoveBoldText?:boolean;
IsShowHeader?:boolean;
POSReceiptFontSize?:string;
SecurityPIN?:string;
IsShowPaymentLabel?:boolean;
IsShowPaymentWarningLabel?:boolean;
ID_PackagePlan?:number;
dbpassword?:string;
dbusername?:string;
Guid?:string;
ReceptionPortalGuid?:string;
MainRouteLink?:string;
BarangayListSideReportHTMLString?:string;
BarangayCaptainName?:string;
BarangayCaptain_ID_Employee?:number;

            }
        
            export class Company_Subscription {
                IsActive?:boolean;
ID_Company?:number;
ID?:number;
Comment?:string;
Name?:string;
ID_LastModifiedBy?:number;
Code?:string;
DateCreated?:Date;
ID_CreatedBy?:number;
DateModified?:Date;
DateStart?:Date;
DateEnd?:Date;

            }
        
            export class CompanyInfo {
                DateModified?:Date;
ID_LastModifiedBy?:number;
DateCreated?:Date;
Comment?:string;
IsActive?:boolean;
ID_Company?:number;
Name?:string;
ID?:number;
ID_CreatedBy?:number;
Code?:string;

            }
        
            export class CompanyTextBlastTemplate {
                DateCreated?:Date;
Name?:string;
ID_LastModifiedBy?:number;
DateModified?:Date;
ID_Company?:number;
Comment?:string;
ID?:number;
IsActive?:boolean;
Code?:string;
ID_CreatedBy?:number;
Template?:string;

            }
        
            export class ControlType {
                Name?:string;
Code?:string;
ID_CreatedBy?:number;
ID?:number;
Comment?:string;
IsActive?:boolean;
ID_LastModifiedBy?:number;
DateCreated?:Date;
DateModified?:Date;

            }
        
            export class CustomDetailViewRoute {
                DateModified?:Date;
DateCreated?:Date;
IsActive?:boolean;
ID?:number;
ID_LastModifiedBy?:number;
Name?:string;
Comment?:string;
ID_CreatedBy?:number;
Code?:string;
ID_Company?:number;
RouterLink?:string;
Oid_DetailView?:string;

            }
        
            export class CustomNavigationLink {
                DateCreated?:Date;
ID_CreatedBy?:number;
ID_LastModifiedBy?:number;
DateModified?:Date;
ID?:number;
Comment?:string;
Code?:string;
IsActive?:boolean;
Name?:string;
ID_Company?:number;
Oid_ListView?:string;
RouterLink?:string;
ID_ViewType?:number;
Oid_Report?:string;

            }
        
            export class DatabaseMemoryLog {
                Name?:string;
ID_LastModifiedBy?:number;
DateCreated?:Date;
ID?:number;
Comment?:string;
Code?:string;
ID_Company?:number;
ID_CreatedBy?:number;
IsActive?:boolean;
DateModified?:Date;
Size?:number;
SizeInMB ?:number;

            }
        
            export class DentalExamination {
                ID?:number;
Comment?:string;
DateModified?:Date;
ID_LastModifiedBy?:number;
IsActive?:boolean;
Code?:string;
Name?:string;
DateCreated?:Date;
ID_CreatedBy?:number;
ID_Company?:number;

            }
        
            export class Dentition {
                ID_Company?:number;
IsActive?:boolean;
Code?:string;
ID_CreatedBy?:number;
DateCreated?:Date;
Name?:string;
ID?:number;
DateModified?:Date;
Comment?:string;
ID_LastModifiedBy?:number;

            }
        
            export class DetailView {
                DateModified?:Date;
DateCreated?:Date;
Caption?:string;
Name?:string;
IsActive?:boolean;
Comment?:string;
Oid?:string;
ID_CreatedBy?:number;
ID_Model?:string;
ID_LastModifiedBy?:number;
Code?:string;
DetailView_Detail?:DetailView_Detail[];
JsController?:string;
Height?:number;
Width?:number;

            }
        
            export class DetailView_Detail {
                ID_DetailView?:string;
IsActive?:boolean;
DateCreated?:Date;
ID_Tab?:string;
DateModified?:Date;
ID_LastModifiedBy?:number;
Oid?:string;
Code?:string;
ID_CreatedBy?:number;
ID_Section?:string;
DataSource?:string;
ID_ModelProperty?:string;
DisplayProperty?:string;
ID_PropertyType?:number;
Caption?:string;
Name?:string;
Comment?:string;
Format?:string;
ID_ControlType?:number;
SeqNo?:number;
IsLoadData?:boolean;
ColCount?:number;
ColSpan?:number;
IsDisabled?:boolean;
Height?:number;
ID_ListView?:string;
IsReadOnly?:boolean;
ID_LabelLocation?:number;
IsShowLabel?:boolean;
IsRequired?:boolean;
ValueExpr?:string;
DisplayExpr?:string;
ID_LookUp_ListView?:string;
LookUp_ListView_Caption?:string;
LookUp_ListView_DataSource?:string;
GroupIndex?:number;
SearchExpr?:string;
Precision?:number;
ID_Parent_Grid?:string;
DetailView_Detail_Link?:DetailView_Detail_Link[];
IsShowClearButton?:boolean;
IsSearchEnabled?:boolean;
InputMask?:string;

            }
        
            export class DetailView_Detail_Link {
                Oid?:string;
Code?:string;
IsActive?:boolean;
ID_LastModifiedBy?:number;
ID_CreatedBy?:number;
Comment?:string;
DateModified?:Date;
Name?:string;
ID_DetailView_Detail?:string;
DateCreated?:Date;
ID_DetailView_Detail_Link?:string;
IsRequired?:boolean;

            }
        
            export class Disaster {
                Comment?:string;
ID_LastModifiedBy?:number;
ID_CreatedBy?:number;
ID?:number;
IsActive?:boolean;
DateModified?:Date;
Name?:string;
ID_Company?:number;
DateCreated?:Date;
Code?:string;

            }
        
            export class Dispute {
                Name?:string;
Code?:string;
DateModified?:Date;
DateCreated?:Date;
ID_LastModifiedBy?:number;
ID_Company?:number;
ID?:number;
IsActive?:boolean;
Comment?:string;
ID_CreatedBy?:number;

            }
        
            export class DocumentSeries {
                Code?:string;
Name?:string;
Comment?:string;
ID_CreatedBy?:number;
DateCreated?:Date;
ID?:number;
IsActive?:boolean;
DateModified?:Date;
ID_LastModifiedBy?:number;
ID_Model?:string;
Counter?:number;
Prefix?:string;
IsAppendCurrentDate?:boolean;
DigitCount?:number;
ID_Company?:number;

            }
        
            export class DocumentStatus {
                Code?:string;
ID?:number;
Comment?:string;
ID_CreatedBy?:number;
DateModified?:Date;
Name?:string;
ID_LastModifiedBy?:number;
IsActive?:boolean;
ID_Company?:number;
DateCreated?:Date;

            }
        
            export class EducationalLevel {
                ID_Company?:number;
ID_CreatedBy?:number;
DateModified?:Date;
DateCreated?:Date;
Comment?:string;
Name?:string;
ID?:number;
ID_LastModifiedBy?:number;
Code?:string;
IsActive?:boolean;

            }
        
            export class Employee {
                IsActive?:boolean;
ID?:number;
ID_CreatedBy?:number;
Comment?:string;
DateCreated?:Date;
ID_LastModifiedBy?:number;
Code?:string;
DateModified?:Date;
ID_Position?:number;
LastName?:string;
FirstName?:string;
MiddleName?:string;
ID_Gender?:string;
ID_EmployeeStatus?:string;
FullAddress?:string;
Email?:string;
ContactNumber?:string;
Name?:string;
ID_Company?:number;
IsSystemUsed?:boolean;
PRCLicenseNumber?:string;
PTR?:string;
S2?:string;
TINNumber?:string;
DatePRCExpiration?:Date;
ImageSignitureSpecimen?:string;

            }
        
            export class EmployeeInfo {
                DateModified?:Date;
ID_Company?:number;
Code?:string;
ID_CreatedBy?:number;
Comment?:string;
Name?:string;
ID?:number;
ID_LastModifiedBy?:number;
DateCreated?:Date;
IsActive?:boolean;

            }
        
            export class ExpenseCategory {
                ID_CreatedBy?:number;
Code?:string;
Comment?:string;
IsActive?:boolean;
DateModified?:Date;
DateCreated?:Date;
ID_Company?:number;
Name?:string;
ID_LastModifiedBy?:number;
ID?:number;

            }
        
            export class FamilyRelationshipType {
                DateCreated?:Date;
ID?:number;
Comment?:string;
ID_CreatedBy?:number;
ID_LastModifiedBy?:number;
DateModified?:Date;
Code?:string;
IsActive?:boolean;
Name?:string;
ID_Company?:number;

            }
        
            export class FilingStatus {
                ID_Company?:number;
ID?:number;
Code?:string;
DateCreated?:Date;
Name?:string;
DateModified?:Date;
IsActive?:boolean;
ID_LastModifiedBy?:number;
Comment?:string;
ID_CreatedBy?:number;

            }
        
            export class ForBilling {
                IsActive?:boolean;
Comment?:string;
Code?:string;
ID_Company?:number;
DateModified?:Date;
ID?:number;
ID_LastModifiedBy?:number;
DateCreated?:Date;
ID_CreatedBy?:number;
Name?:string;

            }
        
            export class HelpDeskVideoTutorial {
                ID?:number;
ID_LastModifiedBy?:number;
DateCreated?:Date;
DateModified?:Date;
IsActive?:boolean;
Comment?:string;
ID_Company?:number;
ID_CreatedBy?:number;
Code?:string;
Name?:string;
VideoLink?:string;
SearcgTag?:string;
ThumbnailImage?:string;
SearchTag?:string;

            }
        
            export class HomeOwnershipStatus {
                Code?:string;
ID?:number;
IsActive?:boolean;
Comment?:string;
Name?:string;
ID_Company?:number;
DateCreated?:Date;
ID_LastModifiedBy?:number;
DateModified?:Date;
ID_CreatedBy?:number;

            }
        
            export class HouseholdNumber {
                DateCreated?:Date;
ID_CreatedBy?:number;
Code?:string;
Name?:string;
ID_Company?:number;
Comment?:string;
DateModified?:Date;
ID_LastModifiedBy?:number;
ID?:number;
IsActive?:boolean;
AssignedBHW_ID_Employee?:number;
HouseholdNumber_Disaster?:HouseholdNumber_Disaster[];
Representative_ID_Resident?:number;

            }
        
            export class HouseholdNumber_Disaster {
                Comment?:string;
ID?:number;
ID_Company?:number;
Code?:string;
IsActive?:boolean;
ID_LastModifiedBy?:number;
DateModified?:Date;
Name?:string;
DateCreated?:Date;
ID_CreatedBy?:number;
ID_Disaster?:number;
ID_HouseholdNumber?:number;

            }
        
            export class illingInvoiceWalkInList {
                DateModified?:Date;
Comment?:string;
ID_Company?:number;
ID_LastModifiedBy?:number;
DateCreated?:Date;
ID?:number;
Name?:string;
Code?:string;
IsActive?:boolean;
ID_CreatedBy?:number;

            }
        
            export class InventoryStatus {
                ID?:number;
IsActive?:boolean;
ID_LastModifiedBy?:number;
ID_Company?:number;
DateModified?:Date;
DateCreated?:Date;
Comment?:string;
Code?:string;
Name?:string;
ID_CreatedBy?:number;

            }
        
            export class InventorySummary {
                Comment?:string;
DateModified?:Date;
ID_Company?:number;
IsActive?:boolean;
DateCreated?:Date;
Name?:string;
ID_LastModifiedBy?:number;
Code?:string;
ID?:number;
ID_CreatedBy?:number;

            }
        
            export class InventoryTrail {
                ID_Company?:number;
DateCreated?:Date;
ID?:number;
Name?:string;
DateModified?:Date;
Code?:string;
ID_LastModifiedBy?:number;
Comment?:string;
IsActive?:boolean;
ID_CreatedBy?:number;
ID_Item?:number;
Quantity?:number;
UnitPrice?:number;
ID_FilingStatus?:number;
Date?:Date;
DateExpired?:Date;
BatchNo?:number;

            }
        
            export class IssueTracker {
                Code?:string;
Comment?:string;
IsActive?:boolean;
DateModified?:Date;
Name?:string;
ID_LastModifiedBy?:number;
DateCreated?:Date;
ID?:number;
ID_CreatedBy?:number;
ID_Company?:number;
Issue?:string;
DeveloperSide?:string;
Solution?:string;
ID_FilingStatus?:number;
ID_ApprovedBy?:number;
DateApproved?:Date;

            }
        
            export class Item {
                ID_Company?:number;
DateModified?:Date;
DateCreated?:Date;
IsActive?:boolean;
Comment?:string;
ID_LastModifiedBy?:number;
ID_CreatedBy?:number;
Code?:string;
ID?:number;
Name?:string;
Item_Supplier?:Item_Supplier[];
ID_ItemType?:number;
ID_ItemCategory?:number;
MinInventoryCount?:number;
MaxInventoryCount?:number;
UnitCost?:number;
UnitPrice?:number;
CurrentInventoryCount?:number;
Old_item_id?:number;
Old_procedure_id?:number;
OtherInfo_DateExpiration?:Date;
ID_InventoryStatus?:number;
BarCode?:string;
CustomCode?:string;
_tempSupplier?:string;

            }
        
            export class Item_Supplier {
                Code?:string;
Comment?:string;
Name?:string;
ID?:number;
IsActive?:boolean;
ID_Item?:number;
ID_Supplier?:number;

            }
        
            export class Item_UnitCostLog {
                ID?:number;
ID_CreatedBy?:number;
ID_LastModifiedBy?:number;
Code?:string;
Comment?:string;
DateCreated?:Date;
IsActive?:boolean;
ID_Company?:number;
DateModified?:Date;
Name?:string;
ID_Item?:number;
Price?:number;

            }
        
            export class Item_UnitPriceLog {
                Code?:string;
IsActive?:boolean;
ID_CreatedBy?:number;
ID_LastModifiedBy?:number;
ID_Company?:number;
DateModified?:Date;
DateCreated?:Date;
ID?:number;
Comment?:string;
Name?:string;
ID_Item?:number;
Price?:number;

            }
        
            export class ItemCategory {
                ID_CreatedBy?:number;
ID_Company?:number;
DateModified?:Date;
Name?:string;
ID_LastModifiedBy?:number;
Code?:string;
Comment?:string;
ID?:number;
IsActive?:boolean;
DateCreated?:Date;
ID_ItemType?:number;

            }
        
            export class ItemInventoriable {
                DateModified?:Date;
ID_LastModifiedBy?:number;
Code?:string;
DateCreated?:Date;
ID?:number;
Comment?:string;
ID_Company?:number;
ID_CreatedBy?:number;
Name?:string;
IsActive?:boolean;

            }
        
            export class ItemService {
                Code?:string;
ID_CreatedBy?:number;
Name?:string;
ID?:number;
IsActive?:boolean;
ID_Company?:number;
DateModified?:Date;
ID_LastModifiedBy?:number;
Comment?:string;
DateCreated?:Date;

            }
        
            export class ItemType {
                IsActive?:boolean;
ID_CreatedBy?:number;
ID?:number;
ID_LastModifiedBy?:number;
Name?:string;
DateModified?:Date;
Comment?:string;
DateCreated?:Date;
ID_Company?:number;
Code?:string;

            }
        
            export class LawSuit {
                DateModified?:Date;
Comment?:string;
Name?:string;
ID?:number;
ID_LastModifiedBy?:number;
Code?:string;
ID_CreatedBy?:number;
ID_Company?:number;
IsActive?:boolean;
DateCreated?:Date;
ComplainantName?:string;
RespondentName?:string;
Date?:Date;
RespondentSMSNumber?:string;
ComplainantSMSNumber?:string;
Complainant_ID_Resident?:number;
Respondent_ID_Resident?:number;
ID_LawSuitStatus?:number;
ID_FilingStatus?:number;

            }
        
            export class LawsuitStatus {
                Name?:string;
ID_Company?:number;
ID?:number;
ID_CreatedBy?:number;
IsActive?:boolean;
DateCreated?:Date;
Code?:string;
DateModified?:Date;
Comment?:string;
ID_LastModifiedBy?:number;

            }
        
            export class ListView {
                ID_LastModifiedBy?:number;
DateModified?:Date;
ID_Model?:string;
ID_CreatedBy?:number;
DateCreated?:Date;
Oid?:string;
DataSource?:string;
Code?:string;
Comment?:string;
Caption?:string;
IsActive?:boolean;
Name?:string;
ListView_Detail?:ListView_Detail[];
PageSize?:number;
IsAllowAdd?:boolean;
IsAllowDelete?:boolean;
IsAllowEdit?:boolean;
JsController?:string;

            }
        
            export class ListView_Detail {
                ID_ModelProperty?:string;
DateCreated?:Date;
DataSource?:string;
Comment?:string;
IsActive?:boolean;
DisplayProperty?:string;
Caption?:string;
ID_ListView?:string;
Oid?:string;
ID_LastModifiedBy?:number;
Name?:string;
Code?:string;
DateModified?:Date;
ID_CreatedBy?:number;
Format?:string;
Width?:number;
Fixed?:boolean;
VisibleIndex?:number;
IsAllowEdit?:boolean;
ID_ControlType?:number;
ID_ColumnAlignment?:number;
IsVisible?:boolean;
FixedPosition?:string;
IsRequired?:boolean;
ID_SummaryType?:number;
Precision?:number;
ID_PropertyType?:number;
IsAddModelClass?:boolean;
GroupIndex?:number;
IsFilter?:boolean;
ID_FilterControlType?:number;

            }
        
            export class LocalShippingPermitIssuance {
                DateModified?:Date;
Name?:string;
IsActive?:boolean;
Code?:string;
ID_LastModifiedBy?:number;
ID_Company?:number;
ID?:number;
DateCreated?:Date;
Comment?:string;
ID_CreatedBy?:number;
ShippingShipper?:string;
ShippingAddress?:string;
ShippingCategory?:string;
ShippingSource?:string;
ShippingPurpose?:string;
ShippingProductSource?:string;
ShippingLicenceNumber?:string;
ShippingAccredationNumber?:string;
ShippingCountryOrigin?:string;
ProposedShippingDate?:Date;
ProposedArrivalDate?:Date;
OriginProvince?:string;
OriginMunicipality?:string;
OriginBarangay?:string;
OriginEstablishment?:string;
OriginFullAddress?:string;
OriginSender?:string;
OriginContactNumber?:string;
DestinationProvince?:string;
DestinationMunicipality?:string;
DestinationBarangay?:string;
DestinationEstablishment?:string;
DestinationFullAddress?:string;
DestinationSender?:string;
DestinationContactNumber?:string;
ModeOfTransfortationIsLand?:boolean;
ModeOfTransfortationLandCarrierType?:string;
ModeOfTransfortationLandPlateNumber?:string;
ModeOfTransfortationIsAir?:boolean;
ModeOfTransfortationAirCarrierType?:string;
ModeOfTransfortationAirPlateNumber?:string;
ModeOfTransfortationIsWater?:boolean;
ModeOfTransfortationWaterCarrierType?:string;
ModeOfTransfortationWaterPlateNumber?:string;
ID_FilingStatus?:number;

            }
        
            export class LocalShippingPermitIssuance_Item {
                ID_CreatedBy?:number;
DateModified?:Date;
ID_LastModifiedBy?:number;
DateCreated?:Date;
ID?:number;
IsActive?:boolean;
ID_Company?:number;
Comment?:string;
Name?:string;
Code?:string;
ID_LocalShippingPermitIssuance?:number;

            }
        
            export class LocalShippingPermitIssuance_Requirement {
                ID_LastModifiedBy?:number;
DateModified?:Date;
Name?:string;
DateCreated?:Date;
Comment?:string;
ID?:number;
ID_CreatedBy?:number;
IsActive?:boolean;
ID_Company?:number;
Code?:string;
ID_LocalShippingPermitIssuance?:number;
ID_DocumentType?:number;
IssuedBy?:string;
ImageLocation?:string;
PRCLicenseNo?:string;

            }
        
            export class MedicalHistoryQuestionnaire {
                DateCreated?:Date;
Comment?:string;
ID_LastModifiedBy?:number;
Code?:string;
ID?:number;
DateModified?:Date;
Name?:string;
ID_CreatedBy?:number;
ID_Company?:number;
IsActive?:boolean;
ID_QuestionType?:number;
IsParent?:boolean;
SeqNo?:number;
ID_Parent?:number;

            }
        
            export class MedicalRecordType {
                Comment?:string;
DateCreated?:Date;
DateModified?:Date;
ID_LastModifiedBy?:number;
ID?:number;
ID_CreatedBy?:number;
Code?:string;
Name?:string;
IsActive?:boolean;
ID_Company?:number;

            }
        
            export class MedicationRoute {
                Name?:string;
DateModified?:Date;
ID_Company?:number;
Comment?:string;
ID_CreatedBy?:number;
ID?:number;
IsActive?:boolean;
Code?:string;
DateCreated?:Date;
ID_LastModifiedBy?:number;

            }
        
            export class Message {
                IsActive?:boolean;
ID_Company?:number;
Name?:string;
ID_CreatedBy?:number;
Comment?:string;
DateModified?:Date;
DateCreated?:Date;
Code?:string;
ID_LastModifiedBy?:number;
ID?:number;
ID_User?:number;
Sender_ID_User?:number;
IsOpened?:boolean;
Recipient_ID_User?:number;

            }
        
            export class Model {
                Color3?:string;
Comment?:string;
Color2?:string;
Color1?:string;
IsReadOnly?:boolean;
ID_CreatedBy?:number;
Name?:string;
TableName?:string;
IsActive?:boolean;
DateModified?:Date;
DateCreated?:Date;
ID_LastModifiedBy?:number;
DisplayName?:string;
Oid?:string;
ID_DetailView?:string;
Caption?:string;
ViewSource?:string;
ControllerPath?:string;
Icon?:string;
IsLoadData?:boolean;
IsEnableAuditTrail?:boolean;
IsEnableComment?:boolean;
IsEnableFileAttachment?:boolean;
IsSearchEnabled?:boolean;

            }
        
            export class Model_Property {
                ID_CreatedBy?:number;
ID_PropertyType?:number;
ID_LastModifiedBy?:number;
DateModified?:Date;
Caption?:string;
Oid?:string;
Name?:string;
IsActive?:boolean;
ID_Model?:string;
DateCreated?:Date;
ID_PropertyModel?:string;
ID_ModelProperty_Key?:string;
DefaultValue?:string;
IsAggregated?:boolean;
DisplayProperty?:string;

            }
        
            export class ModelReport {
                ID_Company?:number;
ID?:number;
DateCreated?:Date;
DateModified?:Date;
Oid_Model?:string;
Oid_Report?:string;

            }
        
            export class Navigation {
                Oid?:string;
Icon?:string;
ID_LastModifiedBy?:number;
ID_CreatedBy?:number;
Caption?:string;
SeqNo?:number;
IsActive?:boolean;
Name?:string;
Code?:string;
DateModified?:Date;
DateCreated?:Date;
Comment?:string;
ID_View?:string;
ID_Parent?:string;
Route?:string;

            }
        
            export class OccupationalStatus {
                DateModified?:Date;
Comment?:string;
ID_CreatedBy?:number;
ID?:number;
Name?:string;
DateCreated?:Date;
ID_Company?:number;
IsActive?:boolean;
Code?:string;
ID_LastModifiedBy?:number;

            }
        
            export class Patient {
                Code?:string;
Comment?:string;
ID_LastModifiedBy?:number;
ID_CreatedBy?:number;
DateModified?:Date;
Name?:string;
IsActive?:boolean;
ID?:number;
DateCreated?:Date;
FirstName?:string;
LastName?:string;
MiddleName?:string;
ID_Gender?:number;
Email?:string;
DateBirth?:Date;
FullAddress?:string;
ID_Country?:number;
ContactNumber?:string;
Patient_History?:Patient_History[];
Patient_SOAP_RegularConsoltation?:Patient_SOAP_RegularConsoltation[];
Patient_DentalExamination?:Patient_DentalExamination[];
ID_Company?:number;
Species?:string;
ID_Client?:number;
IsNeutered?:boolean;
IsDeceased?:boolean;
Old_patient_id?:number;
AnimalWellness?:string;
DateDeceased?:Date;
DateLastVisited?:Date;
CurrentCreditAmount?:number;
ProfileImageFile?:string;
CustomCode?:string;
WaitingStatus_ID_FilingStatus?:number;
Microchip?:string;

            }
        
            export class Patient_BirthDateSMSGreetingLog {
                DateCreated?:Date;
ID?:number;
ID_LastModifiedBy?:number;
ID_Company?:number;
Code?:string;
ID_CreatedBy?:number;
IsActive?:boolean;
DateModified?:Date;
Comment?:string;
Name?:string;
ID_Client?:number;
ContactNumber?:string;
ID_Patient?:number;
iTextMo_Status?:number;
DateSent?:Date;
Message?:string;

            }
        
            export class Patient_Confinement {
                ID_Company?:number;
ID_CreatedBy?:number;
Code?:string;
ID?:number;
Comment?:string;
IsActive?:boolean;
Name?:string;
DateCreated?:Date;
ID_LastModifiedBy?:number;
DateModified?:Date;
Date?:Date;
DateDischarge?:Date;
ID_Client?:number;
ID_Patient?:number;
ID_FilingStatus?:number;
ID_DischargeBy?:number;
DateCanceled?:Date;
ID_CanceledBy?:number;
Patient_Confinement_ItemsServices?:Patient_Confinement_ItemsServices[];
BillingInvoice_ID_FilingStatus?:number;
SubTotal?:number;
TotalAmount?:number;
ID_Patient_SOAP?:number;
PatientNames?:string;
Patient_Confinement_Patient?:Patient_Confinement_Patient[];

            }
        
            export class Patient_Confinement_ItemsServices {
                ID_LastModifiedBy?:number;
Comment?:string;
Code?:string;
Name?:string;
IsActive?:boolean;
ID_CreatedBy?:number;
ID?:number;
ID_Company?:number;
DateCreated?:Date;
DateModified?:Date;
ID_Patient_Confinement?:number;
ID_Item?:number;
Quantity?:number;
Date?:Date;
DateExpiration?:Date;
UnitPrice?:number;
UnitCost?:number;
Amount?:number;
ID_Patient_SOAP?:number;
ID_Patient_SOAP_Treatment?:number;
ID_Patient_SOAP_Prescription?:number;
Route?:string;
ID_Patient?:number;

            }
        
            export class Patient_Confinement_Patient {
                DateCreated?:Date;
ID_LastModifiedBy?:number;
ID?:number;
Code?:string;
DateModified?:Date;
IsActive?:boolean;
Name?:string;
ID_CreatedBy?:number;
ID_Company?:number;
Comment?:string;
ID_Patient_Confinement?:number;
ID_Patient?:number;

            }
        
            export class Patient_CreditLogs {
                ID?:number;
ID_CreatedBy?:number;
ID_Company?:number;
Name?:string;
ID_LastModifiedBy?:number;
Comment?:string;
DateCreated?:Date;
DateModified?:Date;
IsActive?:boolean;
Code?:string;
ID_Patient?:number;
Date?:Date;
CreditAmount?:number;

            }
        
            export class Patient_DentalExamination {
                Code?:string;
Name?:string;
IsActive?:boolean;
ID?:number;
Comment?:string;
ID_Patient?:number;
ID_Doctor?:number;
Date?:Date;
GUID?:string;
ID_Dentition?:number;
ID_FilingStatus?:number;
ID_ApprovedBy?:number;
ID_CanceledBy?:number;
DateApproved?:Date;
DateCanceled?:Date;

            }
        
            export class Patient_DentalExamination_Image {
                ID?:number;
IsActive?:boolean;
DateModified?:Date;
ID_LastModifiedBy?:number;
ID_Company?:number;
Name?:string;
ID_CreatedBy?:number;
Comment?:string;
DateCreated?:Date;
Code?:string;
ID_Patient_DentalExamination?:number;
ImageValue?:string;

            }
        
            export class Patient_DentalExamination_MedicalHistory {
                IsActive?:boolean;
Code?:string;
Name?:string;
ID_Company?:number;
ID_CreatedBy?:number;
ID_LastModifiedBy?:number;
Comment?:string;
DateCreated?:Date;
DateModified?:Date;
ID?:number;
ID_Patient_DentalExamination?:number;
ID_MedicalHistoryQuestionnaire?:number;
Answer?:string;

            }
        
            export class Patient_DentalExamination_ToothInfo {
                Name?:string;
DateCreated?:Date;
DateModified?:Date;
ID?:number;
Comment?:string;
ID_Company?:number;
ID_LastModifiedBy?:number;
IsActive?:boolean;
Code?:string;
ID_CreatedBy?:number;
ID_Patient_DentalExamination?:number;
ID_Tooth?:number;
IDs_ToothSurface?:string;
ID_ToothStatus?:number;
GUID?:string;

            }
        
            export class Patient_History {
                Comment?:string;
Name?:string;
Code?:string;
ID?:number;
IsActive?:boolean;
ID_Patient?:number;
Date?:Date;
ID_Doctor?:number;
ID_FilingStatus?:number;

            }
        
            export class Patient_Lodging {
                Name?:string;
IsActive?:boolean;
ID_CreatedBy?:number;
ID_Company?:number;
Comment?:string;
ID_LastModifiedBy?:number;
DateModified?:Date;
ID?:number;
Code?:string;
DateCreated?:Date;
ID_Client?:number;
ID_Patient?:number;
DateStart?:Date;
DateEnd?:Date;
ID_FilingStatus?:number;
DateCheckIn?:Date;
DateCheckOut?:Date;
RateAmount?:number;
AdvancedPaymentAmount?:number;
TotalAmount?:number;
PayableAmount?:number;
HourCount?:number;
PaymentAmount?:number;
ChangeAmount?:number;
RemainingAmount?:number;

            }
        
            export class Patient_SOAP {
                Name?:string;
Code?:string;
ID_CreatedBy?:number;
ID_LastModifiedBy?:number;
Comment?:string;
DateModified?:Date;
ID?:number;
ID_Company?:number;
DateCreated?:Date;
IsActive?:boolean;
Subjective?:string;
Objective?:string;
Assessment?:string;
Prescription?:string;
ID_Patient?:number;
Date?:Date;
ID_SOAPType?:number;
Planning?:string;
ID_ApprovedBy?:number;
ID_CanceledBy?:number;
DateApproved?:Date;
DateCanceled?:Date;
ID_FilingStatus?:number;
Patient_SOAP_Plan?:Patient_SOAP_Plan[];
Old_soap_id?:number;
LabImageFilePath01?:string;
LabImageFilePath02?:string;
LabImageFilePath03?:string;
LabImageFilePath04?:string;
LabImageFilePath05?:string;
LabImageFilePath06?:string;
LabImageFilePath07?:string;
LabImageFilePath08?:string;
LabImageFilePath09?:string;
LabImageFilePath10?:string;
LabImageFilePath11?:string;
LabImageFilePath12?:string;
LabImageFilePath13?:string;
LabImageFilePath14?:string;
LabImageFilePath15?:string;
LabImageRemark01?:string;
LabImageRemark02?:string;
LabImageRemark03?:string;
LabImageRemark04?:string;
LabImageRemark05?:string;
LabImageRemark06?:string;
LabImageRemark07?:string;
LabImageRemark08?:string;
LabImageRemark09?:string;
LabImageRemark10?:string;
LabImageRemark11?:string;
LabImageRemark12?:string;
LabImageRemark13?:string;
LabImageRemark14?:string;
LabImageRemark15?:string;
LabImageRowIndex01?:string;
LabImageRowIndex02?:string;
LabImageRowIndex03?:string;
LabImageRowIndex04?:string;
LabImageRowIndex05?:string;
LabImageRowIndex06?:string;
LabImageRowIndex07?:string;
LabImageRowIndex08?:string;
LabImageRowIndex09?:string;
LabImageRowIndex10?:string;
LabImageRowIndex11?:string;
LabImageRowIndex12?:string;
LabImageRowIndex13?:string;
LabImageRowIndex14?:string;
LabImageRowIndex15?:string;
History?:string;
AttendingPhysician_ID_Employee?:number;
Diagnosis?:string;
Treatment?:string;
ClientCommunication?:string;
ClinicalExamination?:string;
Interpretation?:string;
Patient_SOAP_Prescription?:Patient_SOAP_Prescription[];
DateDone?:Date;
ID_DoneBy?:number;
ID_Patient_Confinement?:number;
CaseType?:string;
BillingInvoice_ID_FilingStatus?:number;
Patient_SOAP_Treatment?:Patient_SOAP_Treatment[];

            }
        
            export class Patient_SOAP_Plan {
                ID?:number;
Code?:string;
ID_Patient_SOAP?:number;
DateReturn?:Date;
ID_Item?:number;
Comment?:string;
DateCreated?:Date;
DateModified?:Date;
IsSentSMS?:boolean;
CustomItem?:string;
old_return_patient_id?:number;
Appointment_ID_FilingStatus?:number;
Appointment_CancellationRemarks?:string;

            }
        
            export class Patient_SOAP_Prescription {
                Code?:string;
DateCreated?:Date;
DateModified?:Date;
ID_LastModifiedBy?:number;
ID_CreatedBy?:number;
ID_Company?:number;
Name?:string;
IsActive?:boolean;
ID?:number;
Comment?:string;
ID_Item?:number;
ID_Patient_SOAP?:number;
Quantity?:number;
UnitCost?:number;
DateExpiration?:Date;
UnitPrice?:number;
IsCharged?:boolean;

            }
        
            export class Patient_SOAP_RegularConsoltation {
                IsActive?:boolean;
ID?:number;
Name?:string;
Comment?:string;
Code?:string;
ID_Patient?:number;

            }
        
            export class Patient_SOAP_SMSStatus {
                IsActive?:boolean;
DateCreated?:Date;
ID?:number;
Comment?:string;
ID_CreatedBy?:number;
Code?:string;
Name?:string;
DateModified?:Date;
ID_Company?:number;
ID_LastModifiedBy?:number;
iTextMo_Status?:number;
ID_Patient_SOAP?:number;

            }
        
            export class Patient_SOAP_Treatment {
                ID_Company?:number;
ID_CreatedBy?:number;
ID?:number;
DateModified?:Date;
DateCreated?:Date;
ID_LastModifiedBy?:number;
IsActive?:boolean;
Comment?:string;
Code?:string;
Name?:string;
ID_Patient_SOAP?:number;
ID_Item?:number;
Quantity?:number;
UnitPrice?:number;
UnitCost?:number;
DateExpiration?:Date;

            }
        
            export class Patient_Vaccination {
                ID?:number;
ID_Company?:number;
Name?:string;
Code?:string;
IsActive?:boolean;
ID_CreatedBy?:number;
DateModified?:Date;
DateCreated?:Date;
Comment?:string;
ID_LastModifiedBy?:number;
ID_Client?:number;
ID_Patient?:number;
Date?:Date;
Temparature?:string;
HeartRate?:string;
Weight?:string;
ID_Item?:number;
ID_FilingStatus?:number;
AttendingPhysician?:string;
DateExpiration?:Date;
DateCanceled?:Date;
ID_CanceledBy?:number;
UnitCost?:number;
UnitPrice?:number;
AttendingPhysician_ID_Employee?:number;
Patient_Vaccination_Schedule?:Patient_Vaccination_Schedule[];
ID_Patient_SOAP?:number;
Appointment_ID_FilingStatus?:number;
Appointment_CancellationRemarks?:string;

            }
        
            export class Patient_Vaccination_Schedule {
                Comment?:string;
ID?:number;
DateModified?:Date;
DateCreated?:Date;
IsActive?:boolean;
ID_LastModifiedBy?:number;
ID_Company?:number;
Code?:string;
Name?:string;
ID_CreatedBy?:number;
ID_Patient_Vaccination?:number;
Date?:Date;
DateSent?:Date;
IsSentSMS?:boolean;
CustomItem?:string;

            }
        
            export class Patient_Wellness {
                ID_LastModifiedBy?:number;
Code?:string;
ID_Company?:number;
IsActive?:boolean;
Comment?:string;
Name?:string;
DateCreated?:Date;
ID?:number;
ID_CreatedBy?:number;
DateModified?:Date;
Patient_Wellness_Detail?:Patient_Wellness_Detail[];
Patient_Wellness_Schedule?:Patient_Wellness_Schedule[];
Date?:Date;
ID_Client?:number;
ID_Patient?:number;
AttendingPhysician_ID_Employee?:number;
AttendingPhysician?:string;
ID_FilingStatus?:number;
DateCanceled?:Date;
ID_CanceledBy?:number;
ID_Patient_SOAP?:number;
ID_Patient_Vaccination?:number;
old_schedule_id?:number;
old_appointment_id?:number;
old_return_patient_id?:number;

            }
        
            export class Patient_Wellness_Detail {
                Comment?:string;
Name?:string;
ID_LastModifiedBy?:number;
IsActive?:boolean;
DateCreated?:Date;
ID_Company?:number;
ID_CreatedBy?:number;
ID?:number;
DateModified?:Date;
Code?:string;
ID_Patient_Wellness?:number;
ID_Item?:number;
UnitCost?:number;
UnitPrice?:number;
DateExpiration?:Date;
tPatient_Vaccination_Detail?:number;
ID_Patient_Vaccination_Detail?:number;
Quantity?:number;
CustomItem?:string;
Appointment_ID_FilingStatus?:number;
Appointment_CancellationRemarks?:string;

            }
        
            export class Patient_Wellness_Schedule {
                DateModified?:Date;
Code?:string;
ID_LastModifiedBy?:number;
ID_Company?:number;
Comment?:string;
Name?:string;
ID?:number;
IsActive?:boolean;
DateCreated?:Date;
ID_CreatedBy?:number;
ID_Patient_Wellness?:number;
Date?:Date;
DateSent?:Date;
IsSentSMS?:boolean;
tPatient_Vaccination_Schedule?:number;
ID_Patient_Vaccination_Schedule?:number;
Appointment_ID_FilingStatus?:number;
Appointment_CancellationRemarks?:string;

            }
        
            export class PatientAppointment {
                Name?:string;
DateCreated?:Date;
IsActive?:boolean;
ID_LastModifiedBy?:number;
Code?:string;
DateModified?:Date;
ID?:number;
ID_Company?:number;
ID_CreatedBy?:number;
Comment?:string;
ID_Patient?:number;
DateStart?:Date;
DateEnd?:Date;
ID_Doctor?:number;
ID_ScheduleType?:number;
AppointmentStatus_ID_FilingStatus?:number;
ID_SOAPType?:number;
ID_Client?:number;
ID_FilingStatus?:number;
DateDone?:Date;
ID_DoneBy?:number;
DateCanceled?:Date;
ID_CanceledBy?:number;
Appointment_ID_FilingStatus?:number;
Appointment_CancellationRemarks?:string;

            }
        
            export class PatientGrooming {
                ID_CreatedBy?:number;
IsActive?:boolean;
ID_LastModifiedBy?:number;
ID_Company?:number;
DateCreated?:Date;
DateModified?:Date;
Name?:string;
ID?:number;
Comment?:string;
Code?:string;

            }
        
            export class PatientSOAPList {
                Comment?:string;
Code?:string;
DateModified?:Date;
ID?:number;
ID_LastModifiedBy?:number;
DateCreated?:Date;
ID_Company?:number;
IsActive?:boolean;
Name?:string;
ID_CreatedBy?:number;

            }
        
            export class PatientWaitingList {
                DateCreated?:Date;
ID_Company?:number;
Name?:string;
DateModified?:Date;
ID_CreatedBy?:number;
Comment?:string;
IsActive?:boolean;
ID?:number;
ID_LastModifiedBy?:number;
ID_Patient?:number;
WaitingStatus_ID_FilingStatus?:number;
BillingInvoice_ID_FilingStatus?:number;
ID_Client?:number;
Code?:string;

            }
        
            export class PatientWaitingList_Logs {
                ID_LastModifiedBy?:number;
Code?:string;
IsActive?:boolean;
DateCreated?:Date;
DateModified?:Date;
Name?:string;
ID_CreatedBy?:number;
Comment?:string;
ID_Company?:number;
ID?:number;
ID_PatientWaitingList?:number;
WaitingStatus_ID_FilingStatus?:number;

            }
        
            export class Payable {
                Code?:string;
ID_CreatedBy?:number;
Comment?:string;
DateCreated?:Date;
ID_Company?:number;
IsActive?:boolean;
ID?:number;
DateModified?:Date;
ID_LastModifiedBy?:number;
Name?:string;
Payable_Detail?:Payable_Detail[];
Date?:Date;
Payment_ID_FilingStatus?:number;
TotalAmount?:number;
RemaningAmount?:number;
PaidAmount?:number;

            }
        
            export class Payable_Detail {
                DateCreated?:Date;
Comment?:string;
IsActive?:boolean;
ID?:number;
ID_CreatedBy?:number;
Name?:string;
DateModified?:Date;
ID_Company?:number;
ID_LastModifiedBy?:number;
Code?:string;
ID_Payable?:number;
ID_ExpenseCategory?:number;
Amount?:number;

            }
        
            export class PayablePayment {
                ID_CreatedBy?:number;
ID_LastModifiedBy?:number;
Comment?:string;
DateModified?:Date;
ID_Company?:number;
DateCreated?:Date;
IsActive?:boolean;
ID?:number;
Name?:string;
Code?:string;
ID_Payable?:number;
Date?:Date;
ID_FilingStatus?:number;
CashAmount?:number;
TotalAmount?:number;

            }
        
            export class PaymentMethod {
                ID_CreatedBy?:number;
Comment?:string;
Code?:string;
DateCreated?:Date;
ID_LastModifiedBy?:number;
Name?:string;
ID?:number;
IsActive?:boolean;
DateModified?:Date;
ID_Company?:number;

            }
        
            export class PaymentTransaction {
                DateModified?:Date;
IsActive?:boolean;
ID_Company?:number;
Name?:string;
ID?:number;
Code?:string;
Comment?:string;
DateCreated?:Date;
ID_CreatedBy?:number;
ID_LastModifiedBy?:number;
ID_BillingInvoice?:number;
ID_TaxScheme?:number;
GrossAmount?:number;
VatAmount?:number;
NetAmount?:number;
Date?:Date;
ID_FilingStatus?:number;
ID_PaymentMethod?:number;
CashAmount?:number;
CheckAmount?:number;
CheckNumber?:string;
PayableAmount?:number;
PaymentAmount?:number;
ChangeAmount?:number;
DateApproved?:Date;
ID_ApprovedBy?:number;
DateCanceled?:Date;
ID_CanceledBy?:number;
CardAmount?:number;
GCashAmount?:number;
ID_CardType?:number;
CardHolderName?:string;
ReferenceTransactionNumber?:string;
CardNumber?:string;
RemainingAmount?:number;
CreditAmount?:number;

            }
        
            export class Position {
                DateCreated?:Date;
DateModified?:Date;
ID_CreatedBy?:number;
Code?:string;
Name?:string;
Comment?:string;
ID_LastModifiedBy?:number;
ID?:number;
IsActive?:boolean;
ID_Company?:number;

            }
        
            export class Program {
                ID?:number;
Name?:string;
DateCreated?:Date;
ID_CreatedBy?:number;
Code?:string;
IsActive?:boolean;
DateModified?:Date;
Comment?:string;
ID_LastModifiedBy?:number;
ID_Company?:number;

            }
        
            export class PropertyType {
                Name?:string;
Comment?:string;
IsActive?:boolean;
ID?:number;

            }
        
            export class PurchaseOrder {
                ID_LastModifiedBy?:number;
DateCreated?:Date;
DateModified?:Date;
ID_CreatedBy?:number;
ID_Company?:number;
Name?:string;
Code?:string;
IsActive?:boolean;
ID?:number;
Comment?:string;
PurchaseOrder_Detail?:PurchaseOrder_Detail[];
ID_Supplier?:number;
TotalQuantity?:number;
TotalGrossAmount?:number;
TotalNetAmount?:number;
TotalVatAmount?:number;
DocumentDate?:Date;
ID_FilingStatus?:number;
ID_SubmittedBy?:number;
DateSubmitted?:Date;
ID_TaxScheme?:number;
DateApproved?:Date;
ID_ApprovedBy?:number;
DateCancelled?:Date;
ID_CancelledBy?:number;
GrossAmount?:number;
VatAmount?:number;
NetAmount?:number;
Date?:Date;
ID_CanceledBy?:number;
DateCanceled?:Date;
ServingStatus_ID_FilingStatus?:number;
DiscountRate?:number;
DiscountAmount?:number;
IsComputeDiscountRate?:boolean;
SubTotal?:number;
TotalAmount?:number;

            }
        
            export class PurchaseOrder_Detail {
                IsActive?:boolean;
Name?:string;
Code?:string;
Comment?:string;
ID?:number;
ID_PurchaseOrder?:number;
ID_Item?:number;
Quantity?:number;
VATAmount?:number;
GrossAmount?:number;
NetAmount?:number;
ID_UOM?:number;
DiscountAmount?:number;
UnitCost?:number;
Balance?:number;
UnitPrice?:number;
Amount?:number;
RemainingQuantity?:number;
ID_PurchaseOrder_Detail?:number;

            }
        
            export class ReceivingReport {
                ID_CreatedBy?:number;
IsActive?:boolean;
ID?:number;
Name?:string;
Comment?:string;
Code?:string;
DateModified?:Date;
ID_LastModifiedBy?:number;
ID_Company?:number;
DateCreated?:Date;
ReceivingReport_Detail?:ReceivingReport_Detail[];
ID_FilingStatus?:number;
ID_PurchaseOrder?:number;
ID_Supplier?:number;
ID_TaxScheme?:number;
GrossAmount?:number;
VatAmount?:number;
NetAmount?:number;
ID_ApprovedBy?:number;
DateApproved?:Date;
Date?:Date;
ID_CanceledBy?:number;
DateCanceled?:Date;
ServingStatus_ID_FilingStatus?:number;
DiscountRate?:number;
DiscountAmount?:number;
IsComputeDiscountRate?:boolean;
SubTotal?:number;
TotalAmount?:number;

            }
        
            export class ReceivingReport_Detail {
                DateCreated?:Date;
Name?:string;
ID_LastModifiedBy?:number;
Comment?:string;
ID?:number;
ID_Company?:number;
ID_CreatedBy?:number;
Code?:string;
DateModified?:Date;
IsActive?:boolean;
ID_ReceivingReport?:number;
ID_PurchaseOrder_Detail?:number;
ID_Item?:number;
Quantity?:number;
Amount?:number;
UnitPrice?:number;

            }
        
            export class RecurScheduleType {
                DateCreated?:Date;
IsActive?:boolean;
DateModified?:Date;
ID_CreatedBy?:number;
Code?:string;
ID_Company?:number;
Name?:string;
Comment?:string;
ID?:number;
ID_LastModifiedBy?:number;
DayCount?:number;

            }
        
            export class Religion {
                Comment?:string;
ID?:number;
IsActive?:boolean;
DateCreated?:Date;
DateModified?:Date;
ID_CreatedBy?:number;
Name?:string;
Code?:string;
ID_LastModifiedBy?:number;
ID_Company?:number;

            }
        
            export class Report {
                Code?:string;
IsActive?:boolean;
Name?:string;
ID_LastModifiedBy?:number;
DateModified?:Date;
ReportPath?:string;
Oid?:string;
Comment?:string;
DateCreated?:Date;
ID_CreatedBy?:number;
Report_Filters?:Report_Filters[];
Caption?:string;

            }
        
            export class Report_Filters {
                Name?:string;
Comment?:string;
ID_Report?:string;
DateModified?:Date;
Code?:string;
ID_LastModifiedBy?:number;
Oid?:string;
IsActive?:boolean;
DateCreated?:Date;
ID_CreatedBy?:number;
ID_ControlType?:number;
ID_PropertyType?:number;
DataSource?:string;
Caption?:string;

            }
        
            export class Resident {
                DateCreated?:Date;
Comment?:string;
DateModified?:Date;
IsActive?:boolean;
ID?:number;
ID_LastModifiedBy?:number;
ID_CreatedBy?:number;
Code?:string;
Name?:string;
ID_Company?:number;
ID_Gender?:number;
ContactNumber?:string;
Address?:string;
Resident_Program?:Resident_Program[];
IsVaccinated?:boolean;
DateLastVaccination?:Date;
IsPermanent?:boolean;
IsMigrante?:boolean;
IsTransient?:boolean;
ID_CivilStatus?:number;
ID_EducationalLevel?:number;
ID_HomeOwnershipStatus?:number;
MigrantAddress?:string;
ID_OccupationalStatus?:number;
Occupation?:string;
Lastname?:string;
Firstname?:string;
Middlename?:string;
Suffix?:string;
ImportGuid?:string;
DateBirth?:Date;
ImageProfilePicFilename?:string;
Resident_MedicalRecord?:Resident_MedicalRecord[];
Resident_Family?:Resident_Family[];
IsPregnant?:boolean;
IsHeadofFamily?:boolean;
IsVoter?:boolean;
isSoloParent?:boolean;
IsOFW?:boolean;
IsPWD?:boolean;
IsBoarder?:boolean;
IsDeceased?:boolean;
ReferenceID?:string;
ID_HouseholdNumber?:number;
AssignedBHW_ID_Employee?:number;
StayInDuration?:string;
ID_Religion?:number;
BirthPlace?:string;
SourceOfIncome?:string;
SourceOfIncomeAmount?:string;
VoteInfoPrecinctNumber?:string;
AssignedBNS_ID_Employee?:number;

            }
        
            export class Resident_Business {
                ID_Company?:number;
IsActive?:boolean;
ID_CreatedBy?:number;
Comment?:string;
Code?:string;
DateModified?:Date;
ID_LastModifiedBy?:number;
Name?:string;
DateCreated?:Date;
ID?:number;
Resident_BusinessOwner?:string;
Address?:string;
DateValid?:Date;
RequestedBy?:string;
BusinessName?:string;
ContactNumber?:string;

            }
        
            export class Resident_Family {
                Name?:string;
ID_CreatedBy?:number;
ID_LastModifiedBy?:number;
IsActive?:boolean;
Comment?:string;
Code?:string;
DateCreated?:Date;
DateModified?:Date;
ID_Company?:number;
ID?:number;
ID_Resident?:number;
ID_FamilyMember?:number;
ID_RelationshipType?:number;

            }
        
            export class Resident_HouseholdNumberLog {
                ID_Company?:number;
ID_CreatedBy?:number;
Code?:string;
Comment?:string;
IsActive?:boolean;
DateModified?:Date;
Name?:string;
DateCreated?:Date;
ID?:number;
ID_LastModifiedBy?:number;
ID_Resident?:number;
ID_HouseholdNumber?:number;
HouseholdNumberName?:string;

            }
        
            export class Resident_MedicalRecord {
                ID_CreatedBy?:number;
Comment?:string;
DateCreated?:Date;
IsActive?:boolean;
ID?:number;
Code?:string;
ID_LastModifiedBy?:number;
ID_Company?:number;
Name?:string;
DateModified?:Date;
ID_Resident?:number;
Date?:Date;
ID_MedicalRecordType?:number;

            }
        
            export class Resident_Program {
                Name?:string;
IsActive?:boolean;
ID?:number;
Comment?:string;
DateModified?:Date;
Code?:string;
ID_Company?:number;
DateCreated?:Date;
ID_CreatedBy?:number;
ID_LastModifiedBy?:number;
ID_Program?:number;
ID_Resident?:number;

            }
        
            export class ResidentRequest {
                ID_Company?:number;
DateModified?:Date;
DateCreated?:Date;
Name?:string;
Comment?:string;
ID_LastModifiedBy?:number;
ID_CreatedBy?:number;
ID?:number;
IsActive?:boolean;
Code?:string;
ResidentRequest_Certificate?:ResidentRequest_Certificate[];
ResidentRequest_BarangayBusinessClearance?:ResidentRequest_BarangayBusinessClearance[];
ResidentRequest_CommunityTaxCertificate?:ResidentRequest_CommunityTaxCertificate[];
ContactNumber?:string;
Address?:string;
Purpose?:string;
ResidentRequest_IndigencyCertificate?:ResidentRequest_IndigencyCertificate[];
ID_Resident?:number;
Date?:Date;
ID_FilingStatus?:number;
ID_ApprovedBy?:number;
DateApproved?:Date;
DateCanceled?:Date;

            }
        
            export class ResidentRequest_BarangayBusinessClearance {
                Comment?:string;
Code?:string;
DateCreated?:Date;
ID?:number;
IsActive?:boolean;
Name?:string;
ID_CreatedBy?:number;
ID_LastModifiedBy?:number;
DateModified?:Date;
ID_Company?:number;
ID_ResidentRequest?:number;
BusinessName?:string;
RegistrationNumber?:string;
ImageCertificateofRegistration?:string;
Address?:string;
DateValidity?:Date;
ID_Resident?:number;

            }
        
            export class ResidentRequest_Certificate {
                ID_Company?:number;
Name?:string;
ID_LastModifiedBy?:number;
DateCreated?:Date;
Comment?:string;
IsActive?:boolean;
ID_CreatedBy?:number;
DateModified?:Date;
ID?:number;
Code?:string;
ID_ResidentRequest?:number;
DateBirth?:Date;
ID_Gender?:number;
ID_Resident?:number;

            }
        
            export class ResidentRequest_CommunityTaxCertificate {
                ID_Company?:number;
Name?:string;
DateCreated?:Date;
IsActive?:boolean;
ID_CreatedBy?:number;
DateModified?:Date;
ID_LastModifiedBy?:number;
ID?:number;
Comment?:string;
Code?:string;
ID_ResidentRequest?:number;
CompanyName?:string;
PositionName?:string;
AnnualIncomeAmount?:number;
ID_Resident?:number;

            }
        
            export class ResidentRequest_IndigencyCertificate {
                ID?:number;
DateModified?:Date;
ID_CreatedBy?:number;
ID_LastModifiedBy?:number;
ID_Company?:number;
DateCreated?:Date;
Comment?:string;
Code?:string;
Name?:string;
IsActive?:boolean;
ID_ResidentRequest?:number;
ID_Gender?:number;
ID_Resident?:number;

            }
        
            export class SalesReturn {
                ID_LastModifiedBy?:number;
ID?:number;
Code?:string;
IsActive?:boolean;
ID_Company?:number;
Name?:string;
ID_CreatedBy?:number;
DateModified?:Date;
Comment?:string;
DateCreated?:Date;
ID_BillingInvoice?:number;
Date?:Date;
TotalAmount?:boolean;
ID_FilingStatus?:number;
SalesReturn_Detail?:SalesReturn_Detail[];
DateApproved?:Date;
ID_ApprovedBy?:number;
DateCanceled?:Date;
ID_CanceledBy?:number;

            }
        
            export class SalesReturn_Detail {
                Comment?:string;
ID_CreatedBy?:number;
ID_LastModifiedBy?:number;
Name?:string;
DateCreated?:Date;
IsActive?:boolean;
Code?:string;
ID?:number;
DateModified?:Date;
ID_Company?:number;
ID_SalesReturn?:number;
ID_BillingInvoice_Detail?:number;
ID_Item?:number;
Quantity?:number;
UnitPrice?:number;
IsComputeDiscountRate?:boolean;
DiscountRate?:number;
DiscountAmount?:number;
Amount?:number;

            }
        
            export class Schedule {
                DateCreated?:Date;
IsActive?:boolean;
ID_CreatedBy?:number;
DateModified?:Date;
Comment?:string;
ID_LastModifiedBy?:number;
ID?:number;
Name?:string;
ID_Company?:number;
Code?:string;
ID_ScheduleType?:number;
DateStart?:Date;
DateEnd?:Date;
AccommodateCount?:number;
Count?:number;
ID_Doctor?:number;
VacantCount?:number;
Schedule_PatientAppointment?:Schedule_PatientAppointment[];
ID_ServiceType?:number;

            }
        
            export class Schedule_PatientAppointment {
                Comment?:string;
IsActive?:boolean;
Code?:string;
ID?:number;
Name?:string;
ID_Schedule?:number;
ID_ScheduleType?:number;
DateStart?:Date;
DateEnd?:Date;
ID_Doctor?:number;
ID_Patient?:number;
DateCreated?:Date;

            }
        
            export class ServiceType {
                Code?:string;
DateModified?:Date;
ID?:number;
IsActive?:boolean;
Comment?:string;
DateCreated?:Date;
ID_LastModifiedBy?:number;
ID_Company?:number;
ID_CreatedBy?:number;
Name?:string;

            }
        
            export class SMSLogs {
                Name?:string;
DateCreated?:Date;
ID_LastModifiedBy?:number;
ID_CreatedBy?:number;
Comment?:string;
DateModified?:Date;
ID_Company?:number;
IsActive?:boolean;
Code?:string;
ID?:number;
ID_CurrentObject?:string;
SMSMessage?:string;

            }
        
            export class SMSPatientSOAP_Company {
                DateModified?:Date;
ID?:number;
Code?:string;
Name?:string;
DateCreated?:Date;
ID_Company?:number;
ID_LastModifiedBy?:number;
ID_CreatedBy?:number;
Comment?:string;
IsActive?:boolean;

            }
        
            export class SOAPType {
                ID?:number;
Comment?:string;
IsActive?:boolean;
ID_LastModifiedBy?:number;
ID_CreatedBy?:number;
Name?:string;
ID_Company?:number;
DateModified?:Date;
DateCreated?:Date;
Code?:string;

            }
        
            export class SoloParent {
                DateModified?:Date;
IsActive?:boolean;
ID_CreatedBy?:number;
ID?:number;
Name?:string;
ID_Company?:number;
DateCreated?:Date;
Code?:string;
Comment?:string;
ID_LastModifiedBy?:number;
Date?:Date;
ID_RelationshipType?:number;
Age?:string;
ParentName?:string;
ParentAge?:string;
Purpose?:string;
Reason?:string;

            }
        
            export class Student {
                IsActive?:boolean;
DateCreated?:Date;
Comment?:string;
Code?:string;
ID_LastModifiedBy?:number;
ID_Company?:number;
DateModified?:Date;
Name?:string;
ID?:number;
ID_CreatedBy?:number;
LastName?:string;
FirstName?:string;
MiddleName?:string;
Address?:string;
PhoneNumber?:string;
Email?:string;

            }
        
            export class Supplier {
                ID_Company?:number;
Name?:string;
DateCreated?:Date;
ID_LastModifiedBy?:number;
Code?:string;
ID_CreatedBy?:number;
ID?:number;
DateModified?:Date;
IsActive?:boolean;
Comment?:string;
Address?:string;
ContactDetail?:string;
TINNumber?:string;

            }
        
            export class SystemVersion {
                DateModified?:Date;
ID_LastModifiedBy?:number;
ID_Company?:number;
Code?:string;
IsActive?:boolean;
DateCreated?:Date;
ID_CreatedBy?:number;
ID?:number;
Name?:string;
Comment?:string;

            }
        
            export class TaxScheme {
                Name?:string;
Code?:string;
Comment?:string;
IsActive?:boolean;
DateCreated?:Date;
ID_LastModifiedBy?:number;
ID_Company?:number;
ID?:number;
DateModified?:Date;
ID_CreatedBy?:number;

            }
        
            export class Teacher {
                Name?:string;
ID_Company?:number;
ID_CreatedBy?:number;
Comment?:string;
Code?:string;
DateCreated?:Date;
ID?:number;
DateModified?:Date;
ID_LastModifiedBy?:number;
IsActive?:boolean;
FirstName?:string;
LastName?:string;
MiddleName?:string;
Address?:string;
Email?:string;
PhoneNumber?:string;

            }
        
            export class TeethQuandrant {
                ID_CreatedBy?:number;
IsActive?:boolean;
Comment?:string;
ID_LastModifiedBy?:number;
DateModified?:Date;
Name?:string;
Code?:string;
ID_Company?:number;
ID?:number;
DateCreated?:Date;

            }
        
            export class TextBlast {
                Code?:string;
DateModified?:Date;
Name?:string;
DateCreated?:Date;
ID_Company?:number;
IsActive?:boolean;
Comment?:string;
ID?:number;
ID_CreatedBy?:number;
ID_LastModifiedBy?:number;
Date?:Date;
ReccurrenceCount?:number;
ID_TimePeriodType?:number;
Message?:string;
TextBlast_Client?:TextBlast_Client[];
ID_FilingStatus?:number;
DateApproved?:Date;
ID_ApprovedBy?:number;
DateCanceled?:Date;
ID_CanceledBy?:number;
ID_CompanyTextBlastTemplate?:number;

            }
        
            export class TextBlast_Client {
                Comment?:string;
ID_CreatedBy?:number;
Name?:string;
DateCreated?:Date;
ID?:number;
ID_Company?:number;
ID_LastModifiedBy?:number;
IsActive?:boolean;
Code?:string;
DateModified?:Date;
ID_Client?:number;
DateSent?:Date;
IsSent?:number;
ID_TextBlast?:number;

            }
        
            export class TextBlast_Client_SMSStatus {
                Code?:string;
DateModified?:Date;
ID_Company?:number;
ID_CreatedBy?:number;
Name?:string;
DateCreated?:Date;
Comment?:string;
ID_LastModifiedBy?:number;
IsActive?:boolean;
ID?:number;
ID_TextBlast_Client?:number;
iTextMo_Status?:number;

            }
        
            export class Tooth {
                ID_CreatedBy?:number;
Left_ID_ToothSurface?:number;
Top_ID_ToothSurface?:number;
Bottom_ID_ToothSurface?:number;
Right_ID_ToothSurface?:number;
DateModified?:Date;
ToothNumber?:number;
IsActive?:boolean;
Comment?:string;
Name?:string;
Location?:string;
ID_LastModifiedBy?:number;
Middle_ID_ToothSurface?:number;
DateCreated?:Date;
Code?:string;
ID?:number;
ID_Dentition?:number;
ID_TeethQuandrant?:number;

            }
        
            export class ToothInfo {
                ID_CreatedBy?:number;
ID_Company?:number;
Comment?:string;
Code?:string;
ID?:number;
Name?:string;
IsActive?:boolean;
ID_LastModifiedBy?:number;
DateModified?:Date;
DateCreated?:Date;
ID_Tooth?:number;
IDs_ToothSurface?:string;
ID_ToothStatus?:number;

            }
        
            export class ToothStatus {
                IsActive?:boolean;
DateModified?:Date;
Name?:string;
ID?:number;
Comment?:string;
ID_Company?:number;
DateCreated?:Date;
ID_CreatedBy?:number;
ID_LastModifiedBy?:number;
Code?:string;
ID_ToothStatusType?:number;

            }
        
            export class ToothStatusType {
                ID_Company?:number;
Comment?:string;
DateCreated?:Date;
ID?:number;
ID_LastModifiedBy?:number;
Name?:string;
DateModified?:Date;
IsActive?:boolean;
ID_CreatedBy?:number;
Code?:string;

            }
        
            export class ToothSurface {
                ID?:number;
DateModified?:Date;
Comment?:string;
DateCreated?:Date;
ID_Company?:number;
Code?:string;
IsActive?:boolean;
ID_CreatedBy?:number;
ID_LastModifiedBy?:number;
Name?:string;

            }
        
            export class UnitOfMeasure {
                DateModified?:Date;
DateCreated?:Date;
ID?:number;
ID_LastModifiedBy?:number;
ID_Company?:number;
Comment?:string;
IsActive?:boolean;
Name?:string;
Code?:string;
ID_CreatedBy?:number;

            }
        
            export class User {
                DateModified?:Date;
Name?:string;
ID_Employee?:number;
IsActive?:boolean;
Code?:string;
Comment?:string;
ID_CreatedBy?:number;
DateCreated?:Date;
ID_LastModifiedBy?:number;
ID?:number;
Username?:string;
ID_UserGroup?:number;
Password?:string;
IsRequiredPasswordChangedOnLogin?:boolean;
User_Roles?:User_Roles[];
ID_Patient?:number;

            }
        
            export class User_Roles {
                IsActive?:boolean;
Comment?:string;
Name?:string;
Code?:string;
ID?:number;
ID_User?:number;
ID_UserRole?:number;
SeqNo?:number;

            }
        
            export class UserComment {
                Comment?:string;
DateCreated?:Date;
ID?:number;
DateModified?:Date;
ID_LastModifiedBy?:number;
IsActive?:boolean;
ID_CurrentObject?:number;
Code?:string;
ID_Model?:string;
ID_CreatedBy?:number;
Name?:string;

            }
        
            export class UserGroup {
                Name?:string;
ID?:number;
DateCreated?:Date;
DateModified?:Date;
Comment?:string;
Code?:string;
IsActive?:boolean;
ID_LastModifiedBy?:number;
ID_CreatedBy?:number;

            }
        
            export class UserRole {
                Code?:string;
DateCreated?:Date;
ID?:number;
ID_CreatedBy?:number;
DateModified?:Date;
Comment?:string;
IsActive?:boolean;
ID_LastModifiedBy?:number;
Name?:string;
UserRole_Detail?:UserRole_Detail[];
Description?:string;
IsFullAccess?:boolean;
UserRole_Reports?:UserRole_Reports[];

            }
        
            export class UserRole_Detail {
                IsActive?:boolean;
Name?:string;
Code?:string;
ID?:number;
Comment?:string;
ID_UserRole?:number;
ID_Model?:string;
IsView?:boolean;
IsCreate?:boolean;
IsEdit?:boolean;
IsDelete?:boolean;
IsDeny?:boolean;
SeqNo?:number;

            }
        
            export class UserRole_Reports {
                Code?:string;
Comment?:string;
IsActive?:boolean;
ID?:number;
Name?:string;
ID_UserRole?:number;
ID_Report?:string;

            }
        
            export class UserSession {
                ID_Warehouse?:number;
Name?:string;
ID_User?:number;
Code?:string;
Comment?:string;
IsActive?:boolean;
ID?:number;
DateCreated?:Date;

            }
        
            export class VaccinationOption {
                Name?:string;
ID?:number;
DateModified?:Date;
ID_CreatedBy?:number;
Comment?:string;
DateCreated?:Date;
ID_Company?:number;
IsActive?:boolean;
Code?:string;
ID_LastModifiedBy?:number;

            }
        
            export class VeterinaryCertificate {
                Code?:string;
ID_Company?:number;
ID?:number;
Name?:string;
Comment?:string;
DateModified?:Date;
DateCreated?:Date;
IsActive?:boolean;
ID_CreatedBy?:number;
ID_LastModifiedBy?:number;
Date?:Date;
ID_Client?:number;
ID_Patient?:number;
PatientDateBirth?:Date;
DestinationAddress?:string;
DateVaccinated?:Date;
ID_Item?:number;
SerialNumber?:string;
LotNumber?:string;
AttendingPhysician_ID_Employee?:number;

            }
        
            export class VeterinaryHealthCertificate {
                ID_Company?:number;
ID?:number;
IsActive?:boolean;
DateCreated?:Date;
Code?:string;
ID_LastModifiedBy?:number;
Name?:string;
Comment?:string;
ID_CreatedBy?:number;
DateModified?:Date;
Date?:Date;
ID_Client?:number;
ID_Patient?:number;
PatientDateBirth?:Date;
DestinationAddress?:string;
DateVaccinated?:Date;
ID_Item?:number;
SerialNumber?:string;
LotNumber?:string;
AttendingPhysician_ID_Employee?:number;
ID_FilingStatus?:number;
Color?:string;
Weight?:string;
DateCanceled?:Date;
ID_CanceledBy?:number;
DateVaccination?:Date;
ID_VaccinationOption?:number;

            }
        
            export class View {
                Name?:string;
ID_CreatedBy?:number;
ID_Dashboard?:string;
Code?:string;
ControllerPath?:string;
DateCreated?:Date;
DateModified?:Date;
Oid?:string;
ID_ListView?:string;
ID_ViewType?:number;
Comment?:string;
ID_Report?:string;
ID_Model?:string;
ID_LastModifiedBy?:number;
CustomViewPath?:string;
IsActive?:boolean;
DataSource?:string;

            }
        
            export class ViewType {
                Code?:string;
ID_LastModifiedBy?:number;
ID_Company?:number;
ID?:number;
IsActive?:boolean;
DateCreated?:Date;
Name?:string;
ID_CreatedBy?:number;
Comment?:string;
DateModified?:Date;

            }
        
    //THIS IS FILE GENERATED. PLEASE DON'T EDIT
    export class APP_MODEL {

        static readonly MODEL:string = '669EE4AC-077D-438A-A410-B323B74173C4';
static readonly MODEL_PROPERTY:string = '00F4342C-BB85-4E56-AF82-4D5342627A50';
static readonly VIEW:string = 'B4686D2F-2AAF-497A-B304-1113F9A4D34A';
static readonly LISTVIEW:string = 'E4861291-C2DB-4316-8415-38E1B1C9B5C7';
static readonly DETAILVIEW:string = '7FFF8508-94EC-4276-BB32-E4B353146F79';
static readonly LISTVIEW_DETAIL:string = '62A90881-9DC1-4420-AF8A-ABC6A26B1C82';
static readonly DETAILVIEW_DETAIL:string = '71286C19-EA5D-4CD8-8B83-33670D418C0E';
static readonly NAVIGATION:string = '3F43B3EF-B221-444B-89F1-214EA25869B0';
static readonly PROPERTYTYPE:string = 'B8FC3C30-6F07-419A-B236-15D592070102';
static readonly COLUMNALIGNMENT:string = 'F3741551-4051-4975-90CE-CDB55880C441';
static readonly USER:string = 'EBD8FB35-ACFA-45CD-83FE-5C7A8B4590BC';
static readonly AUDITTRAIL:string = 'AEA2A2FD-EDF0-4D5D-BC38-A91910DE1278';
static readonly AUDITTRAIL_DETAIL:string = '9130B122-A74E-47AF-9F4B-6459D704AF46';
static readonly USERGROUP:string = 'B7F415FC-9196-45DE-A999-C53529823FFC';
static readonly APPSETTING:string = '59F8DED7-C597-48E1-9AD1-D2FC18337F12';
static readonly USERROLE:string = '3F376489-98F0-4046-A726-E5031634B63F';
static readonly DETAILVIEW_DETAIL_LINK:string = '78E87ADE-8C67-4787-B172-B747019BF50A';
static readonly CONTROLTYPE:string = '7A64767B-0C26-44E5-9286-496279BD0C1C';
static readonly APPROVERMATRIX:string = 'E6410902-A28E-4840-8730-75D5A326AEEF';
static readonly APPROVERMATRIX_DETAIL:string = 'E7E942D0-7BDC-48F1-8A1E-4F2D4C92239E';
static readonly USERSESSION:string = '44C704DA-CF45-4AFA-B83D-63DDC0C1228E';
static readonly USERCOMMENT:string = '5EF92B53-B486-4AFE-801C-D0EFB3C54CA9';
static readonly AUDITTRAILTYPE:string = '6AC0FC27-EB39-4F30-965E-27D579FB625F';
static readonly DOCUMENTSERIES:string = '69B6A1DD-1233-4267-BD64-0C4D5D8673B3';
static readonly PATIENT:string = '38C42E70-228A-4441-98FE-96C805EF153B';
static readonly COMPANY:string = '6411240C-5631-49E9-A4A7-FE788B329E27';
static readonly SCHEDULE:string = 'E2671C16-17B6-47DE-A4F4-E2A14C417DB3';
static readonly USERROLE_DETAIL:string = 'EB992658-1469-4626-AD4F-669AC09F8A78';
static readonly APPOINTMENT:string = '5F6596D4-9EB5-4C70-8CE0-ECCD76D7ABD2';
static readonly APPOINTMENTREQUEST:string = 'B68B8809-7440-4A08-84AF-B715B726BF0A';
static readonly EMPLOYEE:string = 'F29FC12E-FB26-4CF6-8D7E-F926C7340D40';
static readonly POSITION:string = '552E2A55-E5CE-4324-A381-247C1E31BE31';
static readonly ITEM:string = '8319DB68-CB32-49BD-B675-00560BB52A0D';
static readonly PATIENT_HISTORY:string = '9742B616-27EC-47F4-A0B4-3ACEBB45E5F6';
static readonly ITEM_SUPPLIER:string = '4DB9B636-7CEF-4308-BEFE-4CFCC3985E45';
static readonly SUPPLIER:string = '43FB1756-2203-4187-8D8D-2673C37BA4F3';
static readonly PATIENT_SOAP_REGULARCONSOLTATION:string = 'B26EFF00-C496-463F-BA6C-2F7E9408E080';
static readonly STUDENT:string = '6271CE27-C562-44BD-9731-CB73BAB3562F';
static readonly TEACHER:string = '2F2A80C8-E6A7-4AE0-B49F-FEBFAEABB9BC';
static readonly PURCHASEORDER:string = '1ADEBF4B-5D09-4E7A-B00A-46305304B326';
static readonly PURCHASEORDER_DETAIL:string = '4B6F7ABA-C3C3-47F0-9014-F3570127F1E6';
static readonly UNITOFMEASURE:string = 'BEF55752-E4EF-4816-A8AE-9A126B583AE3';
static readonly FILINGSTATUS:string = 'E0EAADFA-E358-483C-BA58-340AB9ACAF9B';
static readonly DOCUMENTSTATUS:string = 'F50003E4-C34D-476D-86A2-02A9E938003D';
static readonly TAXSCHEME:string = 'F1C02D67-DEE2-4F6B-8EC6-D48DF4D50D59';
static readonly APPOINTMENTSCHEDULE:string = '089D6478-AB6B-448A-8A0F-5F6A579F1D1B';
static readonly USER_ROLES:string = '2C7820DD-69BD-42C9-85AA-CD56028546A1';
static readonly SCHEDULE_PATIENTAPPOINTMENT:string = '54B68117-E627-476A-814F-8C4B9DE122A5';
static readonly MESSAGE:string = 'EF9AA343-13B2-473F-B024-E23E95E80CB2';
static readonly REPORT:string = '6834FE43-6072-4C31-B29A-2371F397B4BC';
static readonly USERROLE_REPORTS:string = 'C38A3A8C-B82B-4A58-92C3-6BC1D2D8AA38';
static readonly RECEIVINGREPORT:string = 'F382C30E-BA84-448E-B35E-0FF338DF4CD8';
static readonly ITEMTYPE:string = '9F6DEFF6-8832-4BB9-968E-02085EA25EC6';
static readonly DENTALEXAMINATION:string = '9B8FBF54-7515-469E-8979-1818470CB01F';
static readonly ITEMCATEGORY:string = '224D905D-F9CE-4812-8341-A7DA6C7DCE89';
static readonly TOOTHSURFACE:string = 'B836628E-E1D0-4897-B30F-AEF292B32997';
static readonly TOOTH:string = '5E78413A-25FE-4E88-9045-4CA20010C5A0';
static readonly TOOTHSTATUS:string = 'ED9DD4D5-18EC-4CCA-81DC-3C5A1CE002E8';
static readonly TOOTHINFO:string = 'D60E9EFB-3F1D-4BE3-AE7A-E29CA7814B38';
static readonly PATIENT_DENTALEXAMINATION:string = '29F62CF2-B170-41E2-B62D-55AB32C8D6FD';
static readonly REPORT_FILTERS:string = 'F7B840D7-65EE-4A21-9E66-2DA4A9826FCC';
static readonly PATIENT_DENTALEXAMINATION_TOOTHINFO:string = 'DE5E23E1-D2B6-4492-BD63-EB5191881514';
static readonly DENTITION:string = '27016D36-1029-4F5E-A321-8045386B865C';
static readonly TOOTHSTATUSTYPE:string = 'BBAB1036-9594-4E81-85CE-97019A1B3AE7';
static readonly TEETHQUANDRANT:string = 'E7F2AF47-11EB-41F6-BF5B-EE034C60F37E';
static readonly BILLINGINVOICE:string = 'D2AE318A-4BFE-4E61-ABD0-7E19DE2D869C';
static readonly BILLINGINVOICE_DETAIL:string = '150637F0-344D-4555-A7E4-4544D30106C9';
static readonly RECEIVINGREPORT_DETAIL:string = 'BB42F1FC-0694-4D6F-9C9A-744759ACBA23';
static readonly INVENTORYTRAIL:string = '4E81D31B-9B8D-4B8A-9ECC-27143D0F6A64';
static readonly INVENTORYSTATUS:string = 'CE90DE3D-BE45-4639-98E6-D1E844AE081F';
static readonly CUSTOMDETAILVIEWROUTE:string = 'AAB934CF-B6BA-4F8C-90B4-4CAB16B3918F';
static readonly CUSTOMNAVIGATIONLINK:string = 'A8172ABF-527D-482D-BB97-B58796B93F81';
static readonly PATIENTAPPOINTMENT:string = '27F5F610-75A3-438B-925F-4C3F9E481D44';
static readonly SERVICETYPE:string = '6966EE96-AAB4-4FBC-B174-49BC0B5F1B2F';
static readonly PATIENT_DENTALEXAMINATION_IMAGE:string = 'EF50E4F5-03A6-4089-87E1-6A3ED54BE499';
static readonly INVENTORYSUMMARY:string = '85C86E84-1FB0-42D3-B5D0-17AA577867FD';
static readonly ITEMINVENTORIABLE:string = '1B89C7FA-ECFB-4B6C-9C09-C6DD2AD96E97';
static readonly ITEMSERVICE:string = '254E3C70-5F23-4DA6-89F9-40D52732B5BD';
static readonly MEDICALHISTORYQUESTIONNAIRE:string = 'EB6C564E-D3EF-440C-97D4-1E8192F3803C';
static readonly PATIENT_DENTALEXAMINATION_MEDICALHISTORY:string = '057CB80D-A9E9-46A6-B8EF-4ABA5C6BE368';
static readonly LOCALSHIPPINGPERMITISSUANCE:string = 'A30937F4-5FCC-4C7B-A193-7BFB77EB88EC';
static readonly LOCALSHIPPINGPERMITISSUANCE_REQUIREMENT:string = '12E72379-D9E6-4590-B5D5-B3FE1C450A50';
static readonly LOCALSHIPPINGPERMITISSUANCE_ITEM:string = 'F08213BD-5BFD-44A3-97C3-BFAF859F2D70';
static readonly PAYMENTTRANSACTION:string = '8F4B8EE4-ADE5-4DFC-BA71-0C55DDAF4DAB';
static readonly PAYMENTMETHOD:string = 'BE3FFC78-53DC-4A24-AF80-DCA1FFCAB62B';
static readonly VIEWTYPE:string = '99C3110E-BAC8-4334-9876-FFFDE6FE97E2';
static readonly PATIENT_SOAP:string = 'D2CB2235-594C-4195-95F2-37BAA864933C';
static readonly SOAPTYPE:string = '4C8E24DA-F734-4D17-972D-6CC1ED82E09A';
static readonly CLIENT:string = '97D88ACC-5412-467B-B5E0-8A2AC297611F';
static readonly PATIENT_SOAP_PLAN:string = '8231AA9D-A1FD-475F-BBB6-232D9B265AF3';
static readonly COMPANYINFO:string = '2ACB7A8C-166D-4736-B449-A8C5888AC7C5';
static readonly DATABASEMEMORYLOG:string = 'E193A853-2973-479B-9A0F-C4B9FE68FDEC';
static readonly ISSUETRACKER:string = '6BC93B07-87E0-4E20-AAEE-F9142817A964';
static readonly BREEDSPECIE:string = '2558D1A3-1DE0-4BCA-B467-4ACFAA4659F6';
static readonly PATIENTSOAPLIST:string = '5940CA12-2CB8-4B17-9C35-FBA23E798C86';
static readonly SMSPATIENTSOAP_COMPANY:string = '1F7C33BF-256B-470E-B40D-1CA5419A828F';
static readonly PATIENT_SOAP_SMSSTATUS:string = '58BA5AAE-609E-4177-BB6E-7028F428CD02';
static readonly PATIENT_SOAP_PRESCRIPTION:string = 'F8531618-3F1B-4A85-810F-650E304F84BF';
static readonly ITEM_UNITPRICELOG:string = 'FCBE9935-C451-4FC7-97DA-D344F13E1915';
static readonly ITEM_UNITCOSTLOG:string = '073479BB-4AF9-499C-901E-0CC91C83E248';
static readonly MODELREPORT:string = '8EE0D07D-6E53-43F5-8047-4C81A058ED8E';
static readonly EXPENSECATEGORY:string = '9052C3AC-22EE-4020-B76B-6955AB6B43BE';
static readonly PAYABLE:string = '48FB7081-BDE7-48A9-8E7D-EC060F5436BC';
static readonly PAYABLE_DETAIL:string = '4735542D-C089-4CD0-9EE2-B7FFC81A8BC0';
static readonly PAYABLEPAYMENT:string = 'EFD5E1AC-D635-403D-9FDF-561135C50B49';
static readonly EMPLOYEEINFO:string = 'C2A4DFF9-C579-4A13-BD97-B2AE18E728F6';
static readonly PATIENT_CONFINEMENT:string = 'CA0490FB-C86A-4892-A99D-482CA8454D2A';
static readonly PATIENT_CONFINEMENT_ITEMSSERVICES:string = '93F24012-90CA-49DE-BEB9-8E020088CDAB';
static readonly CASETYPE:string = '8DA90D6B-1A5A-4A92-84EF-659B82C871F6';
static readonly PATIENT_CREDITLOGS:string = '4D035111-5063-4267-97DF-7635E93B0E9D';
static readonly CLIENT_CREDITLOGS:string = '86D4C3D2-6F89-44EC-BE4D-E2C1906D825C';
static readonly RECURSCHEDULETYPE:string = 'D5350136-942F-4F2B-933F-2F1770286A8A';
static readonly BILLINGINVOICE_PATIENT:string = '7ECC7CC7-2E80-4EBF-858B-09D1F711DDF8';
static readonly CLIENTDEPOSIT:string = '6491046E-3EA8-4EB5-901C-CABD6FE9A02F';
static readonly TEXTBLAST:string = '610E6235-FF67-47BD-8006-86C0F3269C09';
static readonly TEXTBLAST_CLIENT:string = '8778F21C-211E-49F9-9357-34C06097B1FF';
static readonly TEXTBLAST_CLIENT_SMSSTATUS:string = '1A5496AE-C2C1-4B1F-8F22-BFCE0296D31F';
static readonly COMPANYTEXTBLASTTEMPLATE:string = '16E946B5-28C5-4124-94C8-966A33101E9F';
static readonly PATIENTWAITINGLIST:string = '65B4D20E-AB2A-445F-8BBC-22ED554778BF';
static readonly FORBILLING:string = '41BFB6D3-B075-4F34-909C-91118C4D05B5';
static readonly PATIENTWAITINGLIST_LOGS:string = '820109C8-9B29-4820-91F3-B66E4EB819F4';
static readonly VETERINARYCERTIFICATE:string = '896BEA0A-CAB7-4B85-AF3A-CB87596B0FD0';
static readonly VETERINARYHEALTHCERTIFICATE:string = '4ADC2CE8-C571-4522-BA9D-07DCAF8B4FCC';
static readonly HELPDESKVIDEOTUTORIAL:string = '9B57ECA9-ECF3-4E36-8A2A-D3F425C21095';
static readonly SALESRETURN:string = '33EF7B68-A8FB-41F6-9BF5-096DB7CBF6DD';
static readonly SALESRETURN_DETAIL:string = 'EA899D9D-9F6D-4CE5-9453-700D14A7BDA9';
static readonly CLIENTWITHDRAW:string = '24C0F354-DC4F-46ED-A70B-9527E921AA88';
static readonly COMPANY_SUBSCRIPTION:string = 'E57C0DCE-44CF-46A0-BA5A-8BCC33025EE2';
static readonly PATIENT_SOAP_TREATMENT:string = '585B9296-8AD0-4957-9E76-6E63F945A0F2';
static readonly MEDICATIONROUTE:string = '6FB477EB-2CB1-4DB4-BE6E-CE66F266C196';
static readonly PATIENT_VACCINATION:string = 'D0FACE56-E303-47BF-BADE-A1029FCC5453';
static readonly PATIENT_VACCINATION_SCHEDULE:string = '8D3738EC-916D-48FD-BAC7-2FF57B8D09E8';
static readonly PATIENT_LODGING:string = 'AA981D31-15A4-4F17-B305-4C98AE3A2876';
static readonly BILLINGINVOICEWALKIN:string = '7126D7E5-80C1-41DB-9132-78F292FF71C1';
static readonly ILLINGINVOICEWALKINLIST:string = 'F287C125-5E3D-465A-9226-A2C1C8C05702';
static readonly BILLINGINVOICEWALKINLIST:string = 'F4FA00A9-7514-406D-8213-3B08AB986DF6';
static readonly PATIENT_WELLNESS:string = 'AA7BE806-3371-448F-94C8-7E42DA2F68BB';
static readonly PATIENT_WELLNESS_DETAIL:string = 'BF51C568-46C2-477E-B564-4E2768D5CC96';
static readonly PATIENT_WELLNESS_SCHEDULE:string = 'A689B0A4-1688-4266-9633-F16E3B6EFE83';
static readonly APPOINTMENTSTATUSLOG:string = '8960397F-86C9-4D1F-8228-1972F1A29C05';
static readonly PATIENT_BIRTHDATESMSGREETINGLOG:string = '2FE6FB85-D60D-4250-B657-6AD37C138740';
static readonly PATIENT_CONFINEMENT_PATIENT:string = '1DA11E8E-82BC-437D-8C85-460E052BA814';
static readonly SYSTEMVERSION:string = 'C5AAF73A-2A4A-4997-9F1C-923DE12BE5E4';
static readonly PATIENTGROOMING:string = '73CA682E-5669-4277-8CA7-51F5882C2BDC';
static readonly RESIDENTREQUEST:string = '10F32E45-68C5-476C-B9D3-33D8BE1B16DC';
static readonly RESIDENTREQUEST_CERTIFICATE:string = '9CA582D7-4948-4DFD-ABCB-62B37B75B9BE';
static readonly RESIDENTREQUEST_BARANGAYBUSINESSCLEARANCE:string = '1788E7AC-88E6-45F6-827E-D88F1E86A83A';
static readonly RESIDENTREQUEST_COMMUNITYTAXCERTIFICATE:string = '2719D1A1-A51C-48E8-8984-7F49D8FBA9F1';
static readonly RESIDENTREQUEST_INDIGENCYCERTIFICATE:string = '89E1F5BB-FD6B-41FB-AA24-6D0594A388A8';
static readonly LAWSUIT:string = '8F1C0242-F3F5-4E24-952B-B6C1ED494C35';
static readonly DISPUTE:string = '534AE384-AE9F-418C-9C92-43D2A9A70BA5';
static readonly RESIDENT:string = '1607785F-764B-42DE-BAEE-CA459D3C5F75';
static readonly RESIDENT_PROGRAM:string = '29EAF168-C002-4231-97FB-283589A1B510';
static readonly PROGRAM:string = '597D5261-C04D-4142-81C9-A5AB2ABF9721';
static readonly SMSLOGS:string = 'E6B3A4F8-C384-48FB-AFD9-6C615DE6EC95';
static readonly ANNOUNCEMENT:string = '69797739-A450-45CE-B9BC-CDF706681D3D';
static readonly VACCINATIONOPTION:string = '5F1C533B-2FA7-4175-90AB-77558ABE0F1A';
static readonly HOMEOWNERSHIPSTATUS:string = 'C7E9FBA1-FCCE-46DE-9183-C9AE56DC8664';
static readonly CIVILSTATUS:string = '686E6D16-F862-4C7D-ADCD-E05E6B6EBACB';
static readonly EDUCATIONALLEVEL:string = '893D169B-9425-43A4-A5D8-4F46C50BDF19';
static readonly OCCUPATIONALSTATUS:string = '045AA19D-AF8E-4DEE-9492-EF86F8B53387';
static readonly MEDICALRECORDTYPE:string = '83D4079C-4941-4E19-95EE-DF1C00C3FC61';
static readonly RESIDENT_MEDICALRECORD:string = '6B1B7E90-8DA7-401C-B39F-7DC1BA23B398';
static readonly RESIDENT_FAMILY:string = 'ED77C129-2105-4F4D-BA69-3043D3EFD204';
static readonly FAMILYRELATIONSHIPTYPE:string = '7F699464-4965-4F9B-8F0A-51A6F0550CF3';
static readonly RESIDENT_BUSINESS:string = 'F78EEC92-2492-48BA-BC26-17FEF4CE0FB8';
static readonly BARANGAYCLEARANCE:string = 'C8CD891B-07EE-4B5C-85ED-4B11D4AD13DE';
static readonly CERTIFICATEOFRESIDENCY:string = '8BEFCAB4-2495-4C25-85AF-2E2B76CF2036';
static readonly CERTIFICATEOFINDIGENCY:string = 'E618CDB0-9205-470D-94DC-BBFDA88C65EA';
static readonly SOLOPARENT:string = '1D7859CC-931B-44DA-9309-A777EA9D6221';
static readonly CERTIFICATIONOFNOINCOME:string = 'AF837ADF-94AE-4F5E-BD9D-CC7DBAF71EA4';
static readonly HOUSEHOLDNUMBER:string = '803693B3-0F21-45E8-BD67-F6D3FF04353F';
static readonly RESIDENT_HOUSEHOLDNUMBERLOG:string = '9146B9B3-6E12-4014-A8C7-95B35A57CBDD';
static readonly RELIGION:string = 'B26BE0CA-B7E9-4CAA-A6F6-0B91C56EA17A';
static readonly LAWSUITSTATUS:string = '2B69FFA8-97CE-421D-937B-415EBE7A4FEB';
static readonly DISASTER:string = 'EE4604B9-00B0-45E5-89BB-077C4F0D3734';
static readonly HOUSEHOLDNUMBER_DISASTER:string = '93188DB7-19DE-42DF-8152-FE10751614F9';


    }
    export class APP_DETAILVIEW {

        static readonly ANNOUNCEMENT_DETAILVIEW:string = 'FF7E03AC-9A1D-46E1-89C2-70F628FF4074';
static readonly APPOINTMENT_DETAILVIEW:string = 'BB575646-DBB3-48A5-9AD9-8F4E663FA6B4';
static readonly APPOINTMENTREQUEST_DETAILVIEW:string = '931705DD-E732-4011-B096-AB47B0F7C44E';
static readonly APPOINTMENTSCHEDULE_DETAILVIEW:string = 'BBEB8D63-DF33-4519-AB8C-D69EE94F8912';
static readonly APPOINTMENTSTATUSLOG_DETAILVIEW:string = '19A078CF-B7F1-4EE3-8930-3D8C97EBB5C8';
static readonly APPROVERMATRIX_DETAILVIEW:string = 'C0CC208F-A9D4-4573-BA73-68AFBC3B8FB0';
static readonly APPROVERMATRIX_DETAIL_DETAILVIEW:string = 'C3CA1B86-1AAD-48CD-AE7B-43584E7DFA41';
static readonly APPSETTING_DETAILVIEW:string = '581E635C-448A-49D8-9718-B450572AA547';
static readonly AUDITTRAIL_DETAILVIEW:string = 'E5C31125-D147-461C-A47C-8FC27A889D2A';
static readonly AUDITTRAIL_DETAIL_DETAILVIEW:string = 'C6A9C377-3570-4053-858D-1A3651A1BBA5';
static readonly AUDITTRAILTYPE_DETAILVIEW:string = 'A3ECBA3F-8104-4779-8F43-ED7D23ABD144';
static readonly BARANGAYCLEARANCE_DETAILVIEW:string = '87FA5398-8EA4-4AEE-8A84-C5EB5A6CF11F';
static readonly BILLINGINVOICE_DETAILVIEW:string = '8F926A27-770F-47ED-952C-4979F2705393';
static readonly BILLINGINVOICE_DETAIL_DETAILVIEW:string = '0D999ADE-25FB-47A1-A909-9C6170219FC6';
static readonly BILLINGINVOICE_PATIENT_DETAILVIEW:string = '6E4450A1-2AE9-433F-AFF3-7FFBFEB127F6';
static readonly BILLINGINVOICEWALKIN_DETAILVIEW:string = 'EC27DAFE-5DC6-44E3-B90A-83B01452704B';
static readonly BILLINGINVOICEWALKINLIST_DETAILVIEW:string = 'F1C01C62-E9AE-4F2F-B8D5-78020D1FB5FF';
static readonly BREEDSPECIE_DETAILVIEW:string = '38D7AD98-F5A8-4A2C-8B92-D51768CB0660';
static readonly CASETYPE_DETAILVIEW:string = 'D6A661C7-9AA3-4E15-B9A4-E0469671A2B9';
static readonly CERTIFICATEOFINDIGENCY_DETAILVIEW:string = '99BDED9F-3A17-4A7D-BE8D-C7E41BA84C9E';
static readonly CERTIFICATEOFRESIDENCY_DETAILVIEW:string = '6A8815F5-FED7-4497-ABB9-B3889DE848C4';
static readonly CERTIFICATIONOFNOINCOME_DETAILVIEW:string = 'F098CAA5-3D49-493C-9847-42A608C015C1';
static readonly CIVILSTATUS_DETAILVIEW:string = '50DC35FB-5BC1-47EA-9CB0-E55A523CC953';
static readonly CLIENT_DETAILVIEW:string = '335842AF-092B-40E5-90E0-E9F990DDB291';
static readonly CLIENT_CREDITLOGS_DETAILVIEW:string = '78828950-F8D1-4523-88DB-1D8C66ACB0B8';
static readonly CLIENTDEPOSIT_DETAILVIEW:string = 'D4E22690-1492-48F6-A842-F70F93E28794';
static readonly CLIENTWITHDRAW_DETAILVIEW:string = '682C5240-72CC-43EA-836A-CB9E132DF343';
static readonly COLUMNALIGNMENT_DETAILVIEW:string = 'A0A0DAB2-FE0E-4BF5-8B1B-DF330A206D55';
static readonly COMPANY_DETAILVIEW:string = 'FB746700-8F09-47B2-90A4-2E6558A34A17';
static readonly COMPANY_SUBSCRIPTION_DETAILVIEW:string = '1C149C3F-4A21-4537-8A7B-6D22E2BB160F';
static readonly COMPANYINFO_DETAILVIEW:string = '612E5E18-EDA5-4695-AA89-558E12E563E4';
static readonly COMPANYTEXTBLASTTEMPLATE_DETAILVIEW:string = 'C6608421-A536-48BB-B970-46258594562A';
static readonly CONTROLTYPE_DETAILVIEW:string = '7AFF0F77-F7F2-4436-927D-7D66B4884FC7';
static readonly CUSTOMDETAILVIEWROUTE_DETAILVIEW:string = '661D5BE6-5B30-465B-BB9D-2D09C74CE141';
static readonly CUSTOMNAVIGATIONLINK_DETAILVIEW:string = 'C1A01BCE-7282-4802-B552-19BE6560529B';
static readonly DATABASEMEMORYLOG_DETAILVIEW:string = 'E4812D35-DEE8-4DF7-B91D-835E7EBB951E';
static readonly DENTALEXAMINATION_DETAILVIEW:string = '31C9AC5A-3680-4FE8-A5B9-9AE2753C2299';
static readonly DENTITION_DETAILVIEW:string = '3186ED07-064E-46D2-A14C-ABECF5B2A2FA';
static readonly DETAILVIEW_DETAILVIEW:string = 'F654D3BD-E6F8-4F15-A501-C14002F53E2F';
static readonly DETAILVIEW_DETAIL_DETAILVIEW:string = '50E576BF-94BF-4294-8FE8-56909BC09271';
static readonly DETAILVIEW_DETAIL_LINK_DETAILVIEW:string = '4DBD0D34-0FA8-4492-AEE2-4BCE01C5B549';
static readonly DISASTER_DETAILVIEW:string = '7E5D2D59-17AB-4AE3-9C66-372AE8060A5E';
static readonly DISPUTE_DETAILVIEW:string = '2A4C9360-8E6C-4481-A5B1-ADBC4B4984E9';
static readonly DOCUMENTSERIES_DETAILVIEW:string = '650B45DA-07D3-4A4E-B5E0-0453462364E4';
static readonly DOCUMENTSTATUS_DETAILVIEW:string = 'B1FB09C5-CA7B-4DB5-8415-DB7AF89CF914';
static readonly EDUCATIONALLEVEL_DETAILVIEW:string = 'B95CD5D3-F8ED-48E8-9E43-7BD1610C6312';
static readonly EMPLOYEE_DETAILVIEW:string = 'C8E9E902-407B-418D-A2AE-39472FB452A7';
static readonly EMPLOYEEINFO_DETAILVIEW:string = '136A48CF-FCC3-413E-BC6A-0C8CC726E7E3';
static readonly EXPENSECATEGORY_DETAILVIEW:string = '5BDFA249-BBA9-4794-AE1F-308017EF4DD1';
static readonly FAMILYRELATIONSHIPTYPE_DETAILVIEW:string = '895B7E9A-A335-4513-9485-FA7DF9BEB182';
static readonly FILINGSTATUS_DETAILVIEW:string = 'AEED135C-D1FA-44FA-BF54-1588FCC9165E';
static readonly FORBILLING_DETAILVIEW:string = '20A7212F-757D-433E-9D6F-34311F778E71';
static readonly HELPDESKVIDEOTUTORIAL_DETAILVIEW:string = 'A39F9C6B-F3A6-41CA-8292-6F241875080F';
static readonly HOMEOWNERSHIPSTATUS_DETAILVIEW:string = 'CF8585A1-75A7-4654-B292-0355E8DF7CD5';
static readonly HOUSEHOLDNUMBER_DETAILVIEW:string = '8B320A24-A3AA-4DAB-8301-632F3144490A';
static readonly HOUSEHOLDNUMBER_DISASTER_DETAILVIEW:string = '7594A9E1-EA3D-45A7-8363-734765EB93E0';
static readonly ILLINGINVOICEWALKINLIST_DETAILVIEW:string = 'AC0C5E1A-88C3-4E03-8885-9A997951AF1F';
static readonly INVENTORYSTATUS_DETAILVIEW:string = '2C25F46E-DE25-4D56-9336-2E239906EEA0';
static readonly INVENTORYSUMMARY_DETAILVIEW:string = '7750CD56-423C-4995-9F79-3776B5E85308';
static readonly INVENTORYTRAIL_DETAILVIEW:string = '46674633-6E5D-4347-8B14-2A622E901777';
static readonly ISSUETRACKER_DETAILVIEW:string = 'DB004252-91F3-4642-9D4D-0C68427AB699';
static readonly ITEM_DETAILVIEW:string = '3B83CAAA-930C-41D8-929B-09420BF73468';
static readonly ITEM_SUPPLIER_DETAILVIEW:string = 'FF4E69FC-3882-40A3-9FB3-042BBBF2416C';
static readonly ITEM_UNITCOSTLOG_DETAILVIEW:string = '16E3DF91-55C1-465E-A3C4-2AE08183A057';
static readonly ITEM_UNITPRICELOG_DETAILVIEW:string = '8787AC0C-837D-4D5C-B7B6-74A6A026FC02';
static readonly ITEMCATEGORY_DETAILVIEW:string = 'C794AA5D-E76E-463B-9272-965B6DD87A0B';
static readonly ITEMINVENTORIABLE_DETAILVIEW:string = '0AECD097-71DE-4FD7-AA5E-5CD7440BF273';
static readonly ITEMSERVICE_DETAILVIEW:string = '132F953D-026A-456B-BE82-E8A83E71CC8D';
static readonly ITEMTYPE_DETAILVIEW:string = '36E7A244-EC44-490D-8CF4-0F41A53EED94';
static readonly LAWSUIT_DETAILVIEW:string = '3F1CC451-80F6-4E0D-81CC-D66F0E61FF93';
static readonly LAWSUITSTATUS_DETAILVIEW:string = '5434D2DF-1521-4192-87FA-C62CED8408AA';
static readonly LISTVIEW_DETAILVIEW:string = 'DBE478CA-0D36-4035-80AE-F0240DF8953F';
static readonly LISTVIEW_DETAIL_DETAILVIEW:string = '8EACE893-AAD3-43C8-8177-8745151F4A8C';
static readonly LOCALSHIPPINGPERMITISSUANCE_DETAILVIEW:string = 'EB934658-DAFA-4F99-80B1-447118B0C4B7';
static readonly LOCALSHIPPINGPERMITISSUANCE_ITEM_DETAILVIEW:string = '00FBEECC-86AD-464A-9F09-CE45EE34A1E2';
static readonly LOCALSHIPPINGPERMITISSUANCE_REQUIREMENT_DETAILVIEW:string = '9988C19D-0935-4E0E-8206-756595C28D3A';
static readonly MEDICALHISTORYQUESTIONNAIRE_DETAILVIEW:string = '413DEDAD-A8DF-49A9-BD93-3B82483EB3B8';
static readonly MEDICALRECORDTYPE_DETAILVIEW:string = '61790802-2839-4B47-ABAD-1F4AF870B1E1';
static readonly MEDICATIONROUTE_DETAILVIEW:string = '5352F0F8-E4B4-47F1-816C-08F9C3A1EC8B';
static readonly MESSAGE_DETAILVIEW:string = '4ADBB9CB-90D1-4CCA-9143-8AC714B1C6FD';
static readonly MODEL_DETAILVIEW:string = '2E2DC8B6-98D2-4734-969D-82A1D94653B8';
static readonly MODEL_PROPERTY_DETAILVIEW:string = '20E7A54D-EBA0-485F-A65A-948CACCA7418';
static readonly MODELREPORT_DETAILVIEW:string = '651EEAAC-8049-427E-9DBC-F2E52B1971AD';
static readonly NAVIGATION_DETAILVIEW:string = 'D506944F-10E9-4D02-B30D-A3E4AC5D0ED6';
static readonly OCCUPATIONALSTATUS_DETAILVIEW:string = '667F9EC7-8F0C-4BEA-84E8-E3C5B499A1D4';
static readonly PATIENT_DETAILVIEW:string = '95A38913-F389-4FC4-8605-D4CE313B500A';
static readonly PATIENT_BIRTHDATESMSGREETINGLOG_DETAILVIEW:string = '108B04B0-2E45-4E59-8522-101D39413865';
static readonly PATIENT_CONFINEMENT_DETAILVIEW:string = '1C4EED5E-59C5-4D31-B62D-BBDF618B0ADA';
static readonly PATIENT_CONFINEMENT_ITEMSSERVICES_DETAILVIEW:string = 'D8CFC3B5-2C54-4830-97F4-273F12599134';
static readonly PATIENT_CONFINEMENT_PATIENT_DETAILVIEW:string = 'A579962D-0714-4D45-B389-3170C5734678';
static readonly PATIENT_CREDITLOGS_DETAILVIEW:string = '4C5DC449-E83A-4AA7-8659-3133DF04B331';
static readonly PATIENT_DENTALEXAMINATION_DETAILVIEW:string = '313D3569-0D18-4688-A0A0-5F5AD5CBF676';
static readonly PATIENT_DENTALEXAMINATION_IMAGE_DETAILVIEW:string = 'A0262370-EA97-4CED-9E2B-FFE78D7503E0';
static readonly PATIENT_DENTALEXAMINATION_MEDICALHISTORY_DETAILVIEW:string = '39272826-3A9D-426E-8E68-8FC539147D57';
static readonly PATIENT_DENTALEXAMINATION_TOOTHINFO_DETAILVIEW:string = '6B5BC64C-12F2-40BE-870A-DB0838035955';
static readonly PATIENT_HISTORY_DETAILVIEW:string = 'A09ED991-56EE-40D0-A1D1-ABC1AB6601E4';
static readonly PATIENT_LODGING_DETAILVIEW:string = '9BC77C88-1911-41F6-AC56-258E67E3DCE0';
static readonly PATIENT_SOAP_DETAILVIEW:string = '30C737FA-4B57-44A1-8038-86D801A18997';
static readonly PATIENT_SOAP_PLAN_DETAILVIEW:string = '8351C8AC-7FF2-4AEB-A3D3-503C47C4EA76';
static readonly PATIENT_SOAP_PRESCRIPTION_DETAILVIEW:string = 'A71C6601-24D3-4554-9221-D7CB2EF2A02B';
static readonly PATIENT_SOAP_REGULARCONSOLTATION_DETAILVIEW:string = '1E142AF3-F38B-4157-8C9E-5A7A88DD5F07';
static readonly PATIENT_SOAP_SMSSTATUS_DETAILVIEW:string = '1DA1F2C5-FB7A-4FEB-A32D-3B370E7753AA';
static readonly PATIENT_SOAP_TREATMENT_DETAILVIEW:string = '58957520-3D17-471D-A641-8D90468873ED';
static readonly PATIENT_VACCINATION_DETAILVIEW:string = '5397A64A-0298-4C7F-8693-A5279839B080';
static readonly PATIENT_VACCINATION_SCHEDULE_DETAILVIEW:string = 'E768D48F-4479-45BF-AC48-BA26AA0CE869';
static readonly PATIENT_WELLNESS_DETAILVIEW:string = 'DC9A351A-F1BF-4938-BB1B-A50A1D71B523';
static readonly PATIENT_WELLNESS_DETAIL_DETAILVIEW:string = '39D0124C-C374-48B0-BE42-D6890AA5B142';
static readonly PATIENT_WELLNESS_SCHEDULE_DETAILVIEW:string = 'C33504F4-3170-410F-AB2F-186A64274043';
static readonly PATIENTAPPOINTMENT_DETAILVIEW:string = '63F9518D-9C4C-432C-9694-0EFF1AEE13A4';
static readonly PATIENTGROOMING_DETAILVIEW:string = '76339E9D-1C16-47CD-8E2A-EC86E09B48F7';
static readonly PATIENTSOAPLIST_DETAILVIEW:string = '5BED4CF1-6F82-464D-9A70-2BEDD9225D07';
static readonly PATIENTWAITINGLIST_DETAILVIEW:string = '833D3654-A848-47A8-9979-B5182AF4FA1F';
static readonly PATIENTWAITINGLIST_LOGS_DETAILVIEW:string = 'F7141099-8F63-482C-ADA8-5ABEA08DD38E';
static readonly PAYABLE_DETAILVIEW:string = '1B178747-C278-4247-8763-BBDC31479E59';
static readonly PAYABLE_DETAIL_DETAILVIEW:string = '34E1E292-4982-41FB-9D91-0CC77FC03288';
static readonly PAYABLEPAYMENT_DETAILVIEW:string = 'F1B42C0D-32E3-4123-AB66-6278915FBCF5';
static readonly PAYMENTMETHOD_DETAILVIEW:string = '1CAE9EF9-672F-4596-90FA-AC5AEBFE8EAE';
static readonly PAYMENTTRANSACTION_DETAILVIEW:string = 'DE46FD9D-B2FC-4AF9-BD60-9B058A717CCF';
static readonly POSITION_DETAILVIEW:string = '828017A5-46FC-48B9-BBC5-EEEE7C895951';
static readonly PROGRAM_DETAILVIEW:string = '83A05859-94D1-4E56-9DBE-E63426669F0A';
static readonly PROPERTYTYPE_DETAILVIEW:string = 'ACC0AEF5-0A90-4D41-873E-C8AAC2549A6B';
static readonly PURCHASEORDER_DETAILVIEW:string = '1D823500-3B9F-4985-A89E-CAF97370CF93';
static readonly PURCHASEORDER_DETAIL_DETAILVIEW:string = 'CEBEDD9E-5B03-4C3C-856A-315B919044C0';
static readonly RECEIVINGREPORT_DETAILVIEW:string = '03DFB3C6-6B53-4395-BFB4-AF5EB9BFF408';
static readonly RECEIVINGREPORT_DETAIL_DETAILVIEW:string = '322664AE-33EF-4096-B748-821A3BF39931';
static readonly RECURSCHEDULETYPE_DETAILVIEW:string = 'F65BD026-0953-46A2-95A8-F551F524CEEB';
static readonly RELIGION_DETAILVIEW:string = 'A7129EBD-E048-4624-B2CA-7FB13D204FFF';
static readonly REPORT_DETAILVIEW:string = '1EA6964E-CA19-4B35-AED0-03EF31403757';
static readonly REPORT_FILTERS_DETAILVIEW:string = '0E4D2B8A-C03D-4E01-A146-6F62F6BDD31C';
static readonly RESIDENT_DETAILVIEW:string = 'E6A7FAF6-2F3A-4860-96F4-0553E34E3300';
static readonly RESIDENT_BUSINESS_DETAILVIEW:string = 'A25F5F75-ADF5-4E02-826A-EB9E91CCDA17';
static readonly RESIDENT_FAMILY_DETAILVIEW:string = '258636D8-9784-4E53-8227-DD3B6E5422A4';
static readonly RESIDENT_HOUSEHOLDNUMBERLOG_DETAILVIEW:string = '23D77501-901B-4A23-8FF8-817863013E49';
static readonly RESIDENT_MEDICALRECORD_DETAILVIEW:string = 'EB9D2474-1701-4A24-B063-875411DD9ABA';
static readonly RESIDENT_PROGRAM_DETAILVIEW:string = 'BB3163C2-48CE-4A1C-80C2-FB400D5FADD1';
static readonly RESIDENTREQUEST_DETAILVIEW:string = '9FF54382-EC1E-4D78-AA86-870EB1963165';
static readonly RESIDENTREQUEST_BARANGAYBUSINESSCLEARANCE_DETAILVIEW:string = '0AF7C78D-E3CA-4594-A665-667FB0E90F61';
static readonly RESIDENTREQUEST_CERTIFICATE_DETAILVIEW:string = '4A6DC849-70CE-4F64-A038-AA8391823600';
static readonly RESIDENTREQUEST_COMMUNITYTAXCERTIFICATE_DETAILVIEW:string = 'A2201C3B-FC80-4854-A0B8-BAEE548B4B50';
static readonly RESIDENTREQUEST_INDIGENCYCERTIFICATE_DETAILVIEW:string = '38342209-E645-4981-968A-E540A263D73B';
static readonly SALESRETURN_DETAILVIEW:string = '9E1A9D74-53F4-43A5-A14B-A68DC82B6BB2';
static readonly SALESRETURN_DETAIL_DETAILVIEW:string = 'FD9EE54F-4EBF-4A66-B14D-C8056D149C9D';
static readonly SCHEDULE_DETAILVIEW:string = '66EFD120-0CC1-4B4E-A0BE-A51055B49197';
static readonly SCHEDULE_PATIENTAPPOINTMENT_DETAILVIEW:string = '7F0E255E-77F5-426A-AB42-6FA560DEF3E7';
static readonly SERVICETYPE_DETAILVIEW:string = '0B24BD1F-92D3-4A10-8408-EFB51EF03703';
static readonly SMSLOGS_DETAILVIEW:string = '787D44EF-7DD0-469F-8B5A-3F2D813DB2B2';
static readonly SMSPATIENTSOAP_COMPANY_DETAILVIEW:string = '621D2246-8B72-4755-A77F-E58E2BC024FC';
static readonly SOAPTYPE_DETAILVIEW:string = 'E5B2D89F-F84D-47F7-9C7E-3D596AADAD81';
static readonly SOLOPARENT_DETAILVIEW:string = '99BE95C7-DEF2-482B-9397-6A1D4B520E30';
static readonly STUDENT_DETAILVIEW:string = 'B75391C5-DB03-4064-B259-895590EDB0CD';
static readonly SUPPLIER_DETAILVIEW:string = 'D4EB10F9-2E01-449D-9FA5-0AA48E655B2F';
static readonly SYSTEMVERSION_DETAILVIEW:string = 'B6D1859A-2879-40AE-A5CA-F7357F9919DB';
static readonly TAXSCHEME_DETAILVIEW:string = 'F0D2EE8E-A25F-41EA-A8AE-CB97AFF96865';
static readonly TEACHER_DETAILVIEW:string = 'B915BD14-7706-467E-9DB7-AB5A3072D6B4';
static readonly TEETHQUANDRANT_DETAILVIEW:string = 'C1CA9843-CD9B-40DC-B424-E269ECCCAFC8';
static readonly TEXTBLAST_DETAILVIEW:string = '56B04B74-F7E3-427A-94FC-51737B365DF7';
static readonly TEXTBLAST_CLIENT_DETAILVIEW:string = 'BE2442D0-1F47-4FA1-BB3D-57C48B08B5B9';
static readonly TEXTBLAST_CLIENT_SMSSTATUS_DETAILVIEW:string = 'AAC01C0C-297A-4C2F-9632-8CCEE912A62E';
static readonly TOOTH_DETAILVIEW:string = '515C8DC7-1FE7-4BCE-87CF-F19EF66CD772';
static readonly TOOTHINFO_DETAILVIEW:string = '741E8A11-0A8A-4B6A-B899-B427F4368503';
static readonly TOOTHSTATUS_DETAILVIEW:string = '3AFB8350-9B16-48E0-8815-D3E8F927952B';
static readonly TOOTHSTATUSTYPE_DETAILVIEW:string = '2B982B3D-9514-48CF-B5D7-878514877F02';
static readonly TOOTHSURFACE_DETAILVIEW:string = 'B2B45796-DA7F-4718-B4B8-6F13A6B0FCA3';
static readonly UNITOFMEASURE_DETAILVIEW:string = '192195C3-7201-4E1A-AC7B-CCD1F3BE914D';
static readonly USER_DETAILVIEW:string = 'F9D1615E-6908-4D41-BA85-141DB9C2005C';
static readonly USER_ROLES_DETAILVIEW:string = '8C5A2CE1-DFBE-4C6D-8F3C-AB0283C69D82';
static readonly USERCOMMENT_DETAILVIEW:string = 'A452E594-0AEB-422C-B669-1C3F41BC6C99';
static readonly USERGROUP_DETAILVIEW:string = '0B6E0ACA-FCA0-4905-8B1B-AC29E92039BF';
static readonly USERROLE_DETAILVIEW:string = '1E496EF9-C72F-4E44-86D3-631F3CACF10A';
static readonly USERROLE_DETAIL_DETAILVIEW:string = '4E658FB8-BC3E-4122-B481-5C15B06BFE1A';
static readonly USERROLE_REPORTS_DETAILVIEW:string = '93A5E70E-F04C-45FA-913F-481024B2D9AE';
static readonly USERSESSION_DETAILVIEW:string = '7E250377-8E03-468F-AAA4-B4F01DBA5EC3';
static readonly VACCINATIONOPTION_DETAILVIEW:string = 'D43CA615-9314-45FF-87C6-AE6D9F4C1382';
static readonly VETERINARYCERTIFICATE_DETAILVIEW:string = 'F9082F72-F223-4DEF-9E23-2934733D7040';
static readonly VETERINARYHEALTHCERTIFICATE_DETAILVIEW:string = 'E231A0C0-9E83-4297-B68F-E046EC620CCC';
static readonly VIEW_DETAILVIEW:string = 'C8991901-C61D-40CF-BB17-72A899C8FA8D';
static readonly VIEWTYPE_DETAILVIEW:string = '04458838-162E-4AF4-9373-41E0EAF60D03';


    }
    //THIS IS FILE GENERATED. PLEASE DON'T EDIT
    export class APP_LISTVIEW {

        static readonly ANNOUNCEMENT_LISTVIEW:string = 'C5B119BF-473E-4755-B057-CBF973E7769E';
static readonly APPOINTMENT_LISTVIEW:string = 'B92E9D68-EBB2-4412-8F79-2ED988AE96CE';
static readonly APPOINTMENTREQUEST_LISTVIEW:string = 'B2317062-580A-46B2-8851-7D50F4EFDDBC';
static readonly APPOINTMENTSCHEDULE_LISTVIEW:string = 'A9433644-84B9-4F6C-8032-0939B235DD08';
static readonly APPOINTMENTSTATUSLOG_LISTVIEW:string = 'A5BD2429-624B-4DD5-AADF-178069BB8F2D';
static readonly APPROVERMATRIX_LISTVIEW:string = '2E2C6832-42FA-48EA-BCF4-41F153BD92AE';
static readonly APPROVERMATRIX_DETAIL_LISTVIEW:string = 'CCD7AE7C-DEF6-4272-A5C5-B2A0E453BA1F';
static readonly APPROVERMATRIX_DETAIL_LISTVIEW_DETAIL:string = 'DEECA804-4DF0-4105-89ED-2F76C130622F';
static readonly APPSETTING_LISTVIEW:string = 'EE7223E0-3D44-433D-8CB2-96C731EE88DA';
static readonly AUDITTRAIL_LISTVIEW:string = '31032B8E-6655-4EE6-A675-9364FD9E8CEC';
static readonly AUDITTRAIL_DETAIL_LISTVIEW:string = 'E72D87FF-03D4-4F09-9EF9-D44D289C5A4F';
static readonly AUDITTRAIL_DETAIL_LISTVIEW_DETAIL:string = '3178205E-1163-4CEE-AB67-EB380848EB72';
static readonly AUDITTRAILTYPE_LISTVIEW:string = 'E6F1A323-D5D5-4117-94C7-A546B92F0219';
static readonly BARANGAYCLEARANCE_LISTVIEW:string = '72FC420F-E8B5-4EAE-BD00-8F806ADE14EE';
static readonly BILLINGINVOICE_LISTVIEW:string = '430AFF04-F8FA-4F28-AAFB-57946C697E16';
static readonly BILLINGINVOICE_DETAIL_LISTVIEW:string = 'B085304C-0819-4546-B997-CA22E29A3CBA';
static readonly BILLINGINVOICE_DETAIL_LISTVIEW_DETAIL:string = '4D56651C-1093-4231-B6B3-AD58A5C9E18C';
static readonly BILLINGINVOICE_PATIENT_LISTVIEW:string = '4A4C0D76-3A12-48F5-B758-97E4B34BC36A';
static readonly BILLINGINVOICE_PATIENT_LISTVIEW_DETAIL:string = '00DFE812-6751-4080-B61F-446A890C33B1';
static readonly BILLINGINVOICEWALKIN_LISTVIEW:string = 'A3965929-55ED-4DDC-98A2-C07E97F3CD00';
static readonly BILLINGINVOICEWALKINLIST_LISTVIEW:string = '3B9478B9-9F8D-4045-B03E-6790DBC7DC2B';
static readonly BREEDSPECIE_LISTVIEW:string = '3E9A2C93-6303-498E-B2D7-C6CB859F6841';
static readonly CASETYPE_LISTVIEW:string = 'F039341B-21AF-4BE3-8709-0D60C0A38ABF';
static readonly CERTIFICATEOFINDIGENCY_LISTVIEW:string = '0EA13EE4-C23E-44AD-8635-E2E9232982C6';
static readonly CERTIFICATEOFRESIDENCY_LISTVIEW:string = 'DDD83A09-E2F0-4C01-87DA-79BB1EEC07B7';
static readonly CERTIFICATIONOFNOINCOME_LISTVIEW:string = 'AC2F9891-30FD-4983-AC2B-81F8768603D5';
static readonly CIVILSTATUS_LISTVIEW:string = 'FAF029E3-55C0-49AA-9D21-08807206BE90';
static readonly CLIENT_LISTVIEW:string = '27B8B575-43AA-4AC2-8456-E80BAB8F7586';
static readonly CLIENT_CREDITLOGS_LISTVIEW:string = '9C9C0806-BD29-4815-A108-1611373D234F';
static readonly CLIENTDEPOSIT_LISTVIEW:string = '56052FA2-7567-4734-AB8F-A099F07770E1';
static readonly CLIENTWITHDRAW_LISTVIEW:string = '31CD53BD-E76A-4FDA-8CD0-24F3CF5F071E';
static readonly COLUMNALIGNMENT_LISTVIEW:string = 'ED844957-652C-4A7E-BAFA-C6EAA59A95F4';
static readonly COMPANY_LISTVIEW:string = '215C8E14-773C-4743-A5D5-D80EFDC642F9';
static readonly COMPANY_SUBSCRIPTION_LISTVIEW:string = 'A9962F22-603B-4F0E-A92E-B3FB1435AD78';
static readonly COMPANYINFO_LISTVIEW:string = '6D0C08A4-9074-431B-8BF4-4ACA9937219E';
static readonly COMPANYTEXTBLASTTEMPLATE_LISTVIEW:string = 'D9299239-09C1-4A16-A83B-D949F06FC233';
static readonly CONTROLTYPE_LISTVIEW:string = '5A2B1CCA-D1A9-4848-ADC8-A4C53F37EF5D';
static readonly CUSTOMDETAILVIEWROUTE_LISTVIEW:string = '173E43BB-E868-4B94-B052-7CB36E2D8CEB';
static readonly CUSTOMNAVIGATIONLINK_LISTVIEW:string = '8160EEC9-5B47-4D21-BD80-22A466BFD5AF';
static readonly DATABASEMEMORYLOG_LISTVIEW:string = 'F57E99ED-90DF-4296-AB9E-7B9740EEF1D5';
static readonly DENTALEXAMINATION_LISTVIEW:string = 'D91A6C4C-DA49-44D7-A9BE-B091D2B445F3';
static readonly DENTITION_LISTVIEW:string = '1DDA3C0D-0543-44A3-80DB-054831A0EF9B';
static readonly DETAILVIEW_LISTVIEW:string = '649BBF30-7076-4AA5-9DB5-2FA59FE532D1';
static readonly DETAILVIEW_DETAIL_LISTVIEW:string = '9282537A-0E8F-4165-A46F-3F65D51D979A';
static readonly DETAILVIEW_DETAIL_LISTVIEW_DETAIL:string = 'B678FFB6-AB5B-4A15-8E5F-0AF1880A671C';
static readonly DETAILVIEW_DETAIL_LINK_LISTVIEW:string = 'EF4954DB-93EB-4192-8199-90D36C9ADCA2';
static readonly DETAILVIEW_DETAIL_LINK_LISTVIEW_DETAIL:string = 'E3CAD1C3-E7B0-4AB2-88CE-1E23B182323C';
static readonly DISASTER_LISTVIEW:string = '9BA33CAE-7F36-40CB-A3ED-84A9DB75A96C';
static readonly DISPUTE_LISTVIEW:string = '30FF9CDD-14E3-4655-8A8D-DE0864A87DCC';
static readonly DOCUMENTSERIES_LISTVIEW:string = '978EF56E-BCF1-4F5C-A50F-D7D4567D5671';
static readonly DOCUMENTSTATUS_LISTVIEW:string = 'AC94E37C-AD1B-45D8-A300-1609A2EC5322';
static readonly EDUCATIONALLEVEL_LISTVIEW:string = '9BBB42C2-2165-465B-94BE-2645E842F6B4';
static readonly EMPLOYEE_LISTVIEW:string = 'D7B16278-B16A-4076-A4F1-69C245B1F139';
static readonly EMPLOYEEINFO_LISTVIEW:string = 'AA97DE59-455D-48EC-8F83-114FEBE5736C';
static readonly EXPENSECATEGORY_LISTVIEW:string = '5B58314F-F886-4622-8363-DF2448500FB5';
static readonly FAMILYRELATIONSHIPTYPE_LISTVIEW:string = 'FFFE1276-C42E-4CD2-8B1F-8CF8B136A77D';
static readonly FILINGSTATUS_LISTVIEW:string = '89F24CCD-C4C6-47E7-9FEC-27D03F1005A4';
static readonly FORBILLING_LISTVIEW:string = '70B77467-0F10-490F-9B9B-F9FA1399CAA6';
static readonly HELPDESKVIDEOTUTORIAL_LISTVIEW:string = '4B1F25A2-7CA7-4DE3-8720-02475A24546D';
static readonly HOMEOWNERSHIPSTATUS_LISTVIEW:string = 'FE4CC20C-0578-45C4-BC18-91AC638B203D';
static readonly HOUSEHOLDNUMBER_LISTVIEW:string = 'E6011A1A-C51F-4E72-9A35-A7B6170609EC';
static readonly HOUSEHOLDNUMBER_DISASTER_LISTVIEW:string = 'D0ED1E36-7FF6-43DB-8D83-B0DDCD919554';
static readonly HOUSEHOLDNUMBER_DISASTER_LISTVIEW_DETAIL:string = 'E5FAD986-2201-4CBD-9739-7E1C85B5FD5D';
static readonly ILLINGINVOICEWALKINLIST_LISTVIEW:string = 'B7944248-CC79-490B-BCE0-C34E433D6218';
static readonly INVENTORYSTATUS_LISTVIEW:string = '73E6F2C7-B058-47FC-878F-30F58C8A7298';
static readonly INVENTORYSUMMARY_LISTVIEW:string = '3B7F739F-1CC4-4FEF-B990-D1108663C304';
static readonly INVENTORYTRAIL_LISTVIEW:string = '51024FF0-8DE5-4601-A4AE-D09F7B700DA9';
static readonly ISSUETRACKER_LISTVIEW:string = '1A56B83A-6D97-45A6-BCB0-A362D53B6C45';
static readonly ITEM_LISTVIEW:string = '349D55CA-4250-4F10-BF05-FB2912D87BA1';
static readonly ITEM_SUPPLIER_LISTVIEW:string = '1D0DC541-C3C4-43F2-832E-5E001EC66518';
static readonly ITEM_SUPPLIER_LISTVIEW_DETAIL:string = '6DBD78A8-E07E-44DB-9175-734D345D57DA';
static readonly ITEM_UNITCOSTLOG_LISTVIEW:string = '5FA71A20-2053-49FC-B677-68D7B06B08F1';
static readonly ITEM_UNITPRICELOG_LISTVIEW:string = '54D94EF5-546D-46E4-B5CA-CDFD310BB5BC';
static readonly ITEMCATEGORY_LISTVIEW:string = '5FE4A938-404D-4289-9197-63506D5CB053';
static readonly ITEMINVENTORIABLE_LISTVIEW:string = '9FCCDF71-7FA5-4CEB-BEFE-A7FCC68C1E73';
static readonly ITEMSERVICE_LISTVIEW:string = '6B9F89A5-18A2-45FC-984E-4819FD195195';
static readonly ITEMTYPE_LISTVIEW:string = 'DE73A265-FE78-464A-8C37-94DDAE4310C2';
static readonly LAWSUIT_LISTVIEW:string = '3446495E-DA90-4155-AE1E-CBD390DB9DB2';
static readonly LAWSUITSTATUS_LISTVIEW:string = '6B0571F2-C131-4721-8C18-AFC4523E56C6';
static readonly LISTVIEW_LISTVIEW:string = '1C29DB62-83FD-4ED0-9896-40B4E7730A64';
static readonly LISTVIEW_DETAIL_LISTVIEW:string = '3CFC43F5-BDE0-4C13-871B-E6038E0AD2B1';
static readonly LISTVIEW_DETAIL_LISTVIEW_DETAIL:string = 'F0D91198-F864-406D-B6A2-0FECBB3EDDA6';
static readonly LOCALSHIPPINGPERMITISSUANCE_LISTVIEW:string = '453C3B1A-E962-4B0E-9ED5-8D523F8A5178';
static readonly LOCALSHIPPINGPERMITISSUANCE_ITEM_LISTVIEW:string = '53729446-3095-4EF7-A33F-9A87CC098574';
static readonly LOCALSHIPPINGPERMITISSUANCE_REQUIREMENT_LISTVIEW:string = '4AD2A9D1-89D5-43BD-8B59-34CBF3036F79';
static readonly MEDICALHISTORYQUESTIONNAIRE_LISTVIEW:string = 'DDF4DC18-AC10-4715-AF4F-C5FC4BFDA493';
static readonly MEDICALRECORDTYPE_LISTVIEW:string = 'D47926BB-D5FC-4BBC-961F-F641CE2819B6';
static readonly MEDICATIONROUTE_LISTVIEW:string = 'AC3D389C-0465-4856-84DF-5BCBBE7067CC';
static readonly MESSAGE_LISTVIEW:string = '7ED15429-7E69-42DA-BBF0-84EB7DCB4841';
static readonly MODEL_LISTVIEW:string = 'CD0CED64-E799-42E6-835F-B0796A8A985F';
static readonly MODEL_PROPERTY_LISTVIEW:string = '39690037-ABE6-43D1-8AAB-CD4DA24A216B';
static readonly MODEL_MODELPROPERTY_LISTVIEW:string = '86F07DBE-4117-46F7-A039-412EF1EA0FFC';
static readonly MODELREPORT_LISTVIEW:string = '894CF8E6-182D-42B2-BC37-8F3C1F0775B2';
static readonly NAVIGATION_LISTVIEW:string = 'F2E290B7-BE5C-45F7-9304-F5B137537A74';
static readonly OCCUPATIONALSTATUS_LISTVIEW:string = 'A6CA8711-71E9-495A-9158-4C9BC12C3FCF';
static readonly PATIENT_LISTVIEW:string = '0DDCCAB3-603A-40FD-8E80-8A5237DB8CDE';
static readonly PATIENT_BIRTHDATESMSGREETINGLOG_LISTVIEW:string = '9A025111-D41A-43FD-A11B-36291ED79802';
static readonly PATIENT_CONFINEMENT_LISTVIEW:string = '15BD0092-E556-4A46-8FC1-30B1867F9A07';
static readonly PATIENT_CONFINEMENT_ITEMSSERVICES_LISTVIEW:string = '017C83D0-9660-4D87-BBF2-DF5C98B26BE6';
static readonly PATIENT_CONFINEMENT_ITEMSSERVICES_LISTVIEW_DETAIL:string = 'C3593B49-F03D-4243-A6F5-4BC69A1D463C';
static readonly PATIENT_CONFINEMENT_PATIENT_LISTVIEW:string = '3949AC4D-6FFF-435F-BEDB-252E2C8914D1';
static readonly PATIENT_CONFINEMENT_PATIENT_LISTVIEW_DETAIL:string = 'FE55F862-DACD-4A3D-865B-079AC798FB58';
static readonly PATIENT_CREDITLOGS_LISTVIEW:string = '292C54CB-BB41-4A3B-AA3B-42313D81188D';
static readonly PATIENT_DENTALEXAMINATION_LISTVIEW:string = '321C2F78-BE18-4602-B9EE-C77685555168';
static readonly PATIENT_DENTALEXAMINATION_LISTVIEW_DETAIL:string = '78AA9905-33F6-41A6-BACA-6AC08CE9EFE5';
static readonly PATIENT_DENTALEXAMINATION_IMAGE_LISTVIEW:string = '8E9B0980-A483-4560-A249-E9F4166B7DDC';
static readonly PATIENT_DENTALEXAMINATION_MEDICALHISTORY_LISTVIEW:string = '7E56F209-7EA6-479E-9C3C-E99E8AB8B944';
static readonly PATIENT_DENTALEXAMINATION_TOOTHINFO_LISTVIEW:string = 'E70EACE9-34A1-499A-B0B4-60F62664AEDF';
static readonly PATIENT_HISTORY_LISTVIEW:string = 'BAC2BA9C-CE9E-4960-9428-5AC936651F31';
static readonly PATIENT_HISTORY_LISTVIEW_DETAIL:string = '31929B33-21DB-42C2-AB81-FE6FB2F76032';
static readonly PATIENT_LODGING_LISTVIEW:string = '1646B132-E320-4FFB-A529-1FFB85CDA84B';
static readonly PATIENT_SOAP_LISTVIEW:string = '6E15CFDC-B6EE-48DB-85E5-70CF1A4B456D';
static readonly PATIENT_SOAP_PLAN_LISTVIEW:string = 'DE96F910-B333-4B16-8CED-182BE3B368C3';
static readonly PATIENT_SOAP_PLAN_LISTVIEW_DETAIL:string = '895C5E05-A729-4550-8C99-7BF7AC161392';
static readonly PATIENT_SOAP_PRESCRIPTION_LISTVIEW:string = '3C71520E-4325-4260-850B-B65916E2497A';
static readonly PATIENT_SOAP_PRESCRIPTION_LISTVIEW_DETAIL:string = '71883300-632D-4C7C-90C0-06A009C7446D';
static readonly PATIENT_SOAP_REGULARCONSOLTATION_LISTVIEW:string = '7E4DAD89-DC71-463C-984F-80F223D284CD';
static readonly PATIENT_SOAP_REGULARCONSOLTATION_LISTVIEW_DETAIL:string = 'D4E16C27-3762-4AE8-830F-B1CF53DED97D';
static readonly PATIENT_SOAP_SMSSTATUS_LISTVIEW:string = '93637C33-E5F4-4679-AA3E-1993B126AE39';
static readonly PATIENT_SOAP_TREATMENT_LISTVIEW:string = '3677463D-9DC9-44C7-BAF4-4A8F69686F38';
static readonly PATIENT_SOAP_TREATMENT_LISTVIEW_DETAIL:string = '1E56F259-C8A4-44D1-85E9-765248C3A5C5';
static readonly PATIENT_VACCINATION_LISTVIEW:string = '386AC3AF-0A99-44F6-8E40-6E239C1A087A';
static readonly PATIENT_VACCINATION_SCHEDULE_LISTVIEW:string = '8C6A03B6-272E-4CA5-95E4-2EB7E49E3587';
static readonly PATIENT_VACCINATION_SCHEDULE_LISTVIEW_DETAIL:string = 'DAEF6599-9EAE-4EA8-9E67-DAA84FF468E0';
static readonly PATIENT_WELLNESS_LISTVIEW:string = '8B500F9D-9BA6-4224-89F5-B7EB70DF7415';
static readonly PATIENT_WELLNESS_DETAIL_LISTVIEW:string = '0ADE8206-BC44-4EED-A2A4-EEB24B765E61';
static readonly PATIENT_WELLNESS_DETAIL_LISTVIEW_DETAIL:string = '9D4CE605-AD83-4D78-8577-82E7710AF0FF';
static readonly PATIENT_WELLNESS_SCHEDULE_LISTVIEW:string = 'DFA7122A-45BA-4632-9EED-5A8CD69A6778';
static readonly PATIENT_WELLNESS_SCHEDULE_LISTVIEW_DETAIL:string = '8CC6482C-61C1-49BA-8A66-BAF65F735929';
static readonly PATIENTAPPOINTMENT_LISTVIEW:string = '455FE610-F56D-42F2-ABBD-9CDB3B93CBFA';
static readonly PATIENTGROOMING_LISTVIEW:string = '8AE1730D-E1F5-418E-8CE8-A6C364A35E5D';
static readonly PATIENTSOAPLIST_LISTVIEW:string = 'AA57E2A5-8424-4BF8-B2E5-BEFE847489D8';
static readonly PATIENTWAITINGLIST_LISTVIEW:string = 'A42F8F43-75C1-4D3E-8E53-6A76024255F5';
static readonly PATIENTWAITINGLIST_LOGS_LISTVIEW:string = '933DFB7C-9B03-4B09-837B-9C27FFA81328';
static readonly PAYABLE_LISTVIEW:string = '6E080EB9-D014-4061-9543-3318844DA9BD';
static readonly PAYABLE_DETAIL_LISTVIEW:string = '51C72D84-5C27-45D2-BD7B-F8EC1F1CFE1E';
static readonly PAYABLE_DETAIL_LISTVIEW_DETAIL:string = '458285E9-D1D5-4329-BF05-0E860D205DA9';
static readonly PAYABLEPAYMENT_LISTVIEW:string = '3D550C9D-C972-4328-BDDA-F805EDF4CA22';
static readonly PAYMENTMETHOD_LISTVIEW:string = '9B8C92D7-3B17-4A6E-9569-F398F6AC2738';
static readonly PAYMENTTRANSACTION_LISTVIEW:string = '9DDD8950-5B2A-42C0-A412-9387586A7680';
static readonly POSITION_LISTVIEW:string = '38E908FC-7EFB-4BD7-A7C2-6FBEC7D700CF';
static readonly PROGRAM_LISTVIEW:string = 'D92D92FC-B823-4487-B681-C00D27BEB60B';
static readonly PROPERTYTYPE_LISTVIEW:string = '2B21F843-E71F-440B-B085-82DB34FB7B46';
static readonly PURCHASEORDER_LISTVIEW:string = 'EC5E4676-16EC-4D75-B61B-79BC4C0CECFD';
static readonly PURCHASEORDER_DETAIL_LISTVIEW:string = 'FDB2F695-4266-43EF-80E9-29B7758B0032';
static readonly PURCHASEORDER_DETAIL_LISTVIEW_DETAIL:string = 'DD6AC4AB-A161-473B-B6D0-6FEC2518BA83';
static readonly RECEIVINGREPORT_LISTVIEW:string = 'AFADE9C7-C210-482D-8B52-7FA9DEC8A45F';
static readonly RECEIVINGREPORT_DETAIL_LISTVIEW:string = '9D30FE67-98D9-481C-B411-B7484861F920';
static readonly RECEIVINGREPORT_DETAIL_LISTVIEW_DETAIL:string = '2BC0DAF9-E506-46F7-B16A-659E9478C4C9';
static readonly RECURSCHEDULETYPE_LISTVIEW:string = 'F70D34C5-0686-4C48-8937-E358B46A9193';
static readonly RELIGION_LISTVIEW:string = '9EFC859E-65BC-4964-92BA-637028C37B7A';
static readonly REPORT_LISTVIEW:string = '80415A11-9B90-4081-BF19-4B332C592C76';
static readonly REPORT_FILTERS_LISTVIEW:string = '89FAA200-FE28-495F-9001-CE21D712AAA5';
static readonly REPORT_FILTERS_LISTVIEW_DETAIL:string = 'D6E61BD3-8ACF-4CD6-9887-60F8014E1369';
static readonly RESIDENT_LISTVIEW:string = '33C5B6A9-C2E3-450C-8D7A-288DD99B0F55';
static readonly RESIDENT_BUSINESS_LISTVIEW:string = 'DAC80C80-1F91-4176-8835-F9DBE77A8608';
static readonly RESIDENT_FAMILY_LISTVIEW:string = '1AFEE394-E0FE-4F1D-9E3A-6F710DBA361D';
static readonly RESIDENT_FAMILY_LISTVIEW_DETAIL:string = 'D5CDC759-7AD5-4202-88CF-00106550D2F3';
static readonly RESIDENT_HOUSEHOLDNUMBERLOG_LISTVIEW:string = '2B28EEFF-1C50-445C-AC20-ACCF94288D0E';
static readonly RESIDENT_MEDICALRECORD_LISTVIEW:string = 'B1787C88-524A-4B05-8190-9C103A88001F';
static readonly RESIDENT_MEDICALRECORD_LISTVIEW_DETAIL:string = 'ADC8BBFA-5D92-4A2B-8801-82AB957320B8';
static readonly RESIDENT_PROGRAM_LISTVIEW:string = '15F98D1C-9C88-41D6-B237-C352489F5DCE';
static readonly RESIDENT_PROGRAM_LISTVIEW_DETAIL:string = 'F9525D55-69DC-4B1A-BC86-5D0B0F9B1024';
static readonly RESIDENTREQUEST_LISTVIEW:string = '2B6DADBD-1161-436F-99D8-4A0E7FD0E2E1';
static readonly RESIDENTREQUEST_BARANGAYBUSINESSCLEARANCE_LISTVIEW:string = 'C3A6CE98-E215-40FF-B4E6-54694EB58C89';
static readonly RESIDENTREQUEST_BARANGAYBUSINESSCLEARANCE_LISTVIEW_DETAIL:string = '7A3BEA03-A5AB-4026-A0D7-FA121B616F96';
static readonly RESIDENTREQUEST_CERTIFICATE_LISTVIEW:string = '188A91BD-187A-4221-805B-946CCEC0464E';
static readonly RESIDENTREQUEST_CERTIFICATE_LISTVIEW_DETAIL:string = '3944F210-99F0-4147-A399-505B3DE5F4F1';
static readonly RESIDENTREQUEST_COMMUNITYTAXCERTIFICATE_LISTVIEW:string = '28FAA3B7-19CE-457C-ACA4-AC5543BAA647';
static readonly RESIDENTREQUEST_COMMUNITYTAXCERTIFICATE_LISTVIEW_DETAIL:string = 'E4151EF8-6032-44CF-83CD-E20FE7ABE53C';
static readonly RESIDENTREQUEST_INDIGENCYCERTIFICATE_LISTVIEW:string = '1F218EFC-B9A5-48FB-9A7B-3B20E2346C6B';
static readonly RESIDENTREQUEST_INDIGENCYCERTIFICATE_LISTVIEW_DETAIL:string = 'B2DFE416-0E34-4804-86EE-B88F6B338E58';
static readonly SALESRETURN_LISTVIEW:string = 'A7D20F6E-425E-448C-BACF-8C3A1BD4126F';
static readonly SALESRETURN_DETAIL_LISTVIEW:string = 'B6484983-64A9-4637-B7E1-E26D0176752E';
static readonly SALESRETURN_DETAIL_LISTVIEW_DETAIL:string = '0998A3D0-E51D-4027-848B-9451F421E2F6';
static readonly SCHEDULE_LISTVIEW:string = '71D60680-C93B-4C6B-B258-1DE2C891438D';
static readonly SCHEDULE_PATIENTAPPOINTMENT_LISTVIEW:string = 'F9EE6B16-3967-4C3B-99CD-6496D446C3E3';
static readonly SCHEDULE_PATIENTAPPOINTMENT_LISTVIEW_DETAIL:string = '4373E413-9EB3-41F8-A949-31BEA02FA994';
static readonly SERVICETYPE_LISTVIEW:string = 'CEB2CFA6-4979-4034-8FBC-A5F02529A8DF';
static readonly SMSLOGS_LISTVIEW:string = 'AB7A835E-DD33-4350-BA69-7D541E398A4C';
static readonly SMSPATIENTSOAP_COMPANY_LISTVIEW:string = '0AF28C53-74CD-4ED4-8C8C-03B2D121E47D';
static readonly SOAPTYPE_LISTVIEW:string = '9BB1FAC3-18B1-4FDE-923A-053EC0019B52';
static readonly SOLOPARENT_LISTVIEW:string = 'E91310DD-7823-465D-A154-BE01BC44EA87';
static readonly STUDENT_LISTVIEW:string = 'B8BE70E0-3DD0-421C-BD42-7FD406C8C7F5';
static readonly SUPPLIER_LISTVIEW:string = '6248B630-98BF-48BA-8938-DB7BD5C98694';
static readonly SYSTEMVERSION_LISTVIEW:string = '71AA07E7-26DD-4370-BB3D-C94C7DD787F6';
static readonly TAXSCHEME_LISTVIEW:string = '661C2317-4088-482C-9EB8-06099CB23F1B';
static readonly TEACHER_LISTVIEW:string = '0180053D-017A-40FA-AAC1-5F2415D0DDA6';
static readonly TEETHQUANDRANT_LISTVIEW:string = '87C68ACC-4C67-475E-BDDF-1A6587F836F0';
static readonly TEXTBLAST_LISTVIEW:string = 'E99E52EB-8C56-4F13-8A6A-6A3292BC3634';
static readonly TEXTBLAST_CLIENT_LISTVIEW:string = 'E46E38FE-AA70-4279-AAAA-3D0B5DC21A07';
static readonly TEXTBLAST_CLIENT_LISTVIEW_DETAIL:string = '2593A49F-2B6C-45BB-B243-CA8101C419FE';
static readonly TEXTBLAST_CLIENT_SMSSTATUS_LISTVIEW:string = 'D1FE45BF-2490-4A42-A6D9-5BEFF2BD1E88';
static readonly TOOTH_LISTVIEW:string = '0230CC88-77EC-4FA4-960E-68756E94433D';
static readonly TOOTHINFO_LISTVIEW:string = '75327E97-8DBC-4055-BFD6-84E9637D4AF1';
static readonly TOOTHSTATUS_LISTVIEW:string = 'CDB8DFE4-062A-492C-AF18-5FD03549FA6B';
static readonly TOOTHSTATUSTYPE_LISTVIEW:string = '5BEEE52F-FC95-4356-AFEB-9ECC727BC7D2';
static readonly TOOTHSURFACE_LISTVIEW:string = '5165F460-07B7-4A85-AA6C-109F37BFE7A7';
static readonly UNITOFMEASURE_LISTVIEW:string = 'AE5D6834-1290-415A-85A7-5075CA161588';
static readonly USER_LISTVIEW:string = '354B51A1-688E-4380-8072-3318EA9A22BC';
static readonly USER_ROLES_LISTVIEW:string = 'BD682CEA-26C5-46EC-97F7-643EF08EA9E8';
static readonly USER_ROLES_LISTVIEW_DETAIL:string = '4DFF1AF2-35CE-4FFD-BA16-7B7AC68053FE';
static readonly USERCOMMENT_LISTVIEW:string = '77D098BE-D0D8-430D-9332-A1B619F28080';
static readonly USERGROUP_LISTVIEW:string = '7B1E90B1-9063-44E2-80A8-16320A095339';
static readonly USERROLE_LISTVIEW:string = '7ACDB2A1-33D3-41BA-B6FD-E1DAB8FDB019';
static readonly USERROLE_DETAIL_LISTVIEW:string = '01AA9ADB-06B9-4BC7-AEA2-F9D34B233D93';
static readonly USERROLE_DETAIL_LISTVIEW_DETAIL:string = '366A431E-2688-4E53-B011-29AC7A76D5FB';
static readonly USERROLE_REPORTS_LISTVIEW:string = '414FC315-C7F6-45B5-8285-A0F73828B51D';
static readonly USERROLE_REPORTS_LISTVIEW_DETAIL:string = 'F5DE4475-5690-4A0B-B98D-6C1CCD81CF88';
static readonly USERSESSION_LISTVIEW:string = '888CB45E-8C96-41ED-82FF-673F3CDA592A';
static readonly VACCINATIONOPTION_LISTVIEW:string = 'FDA944DB-6486-47EF-9DC6-B32C9F6FECF1';
static readonly VETERINARYCERTIFICATE_LISTVIEW:string = '643A99C8-AA8F-490E-9EC9-27E3B80D0569';
static readonly VETERINARYHEALTHCERTIFICATE_LISTVIEW:string = 'B111DFE5-A3A3-487D-8AD0-35E213D72E77';
static readonly VIEW_LISTVIEW:string = 'B450F145-3FD9-48FC-B3F1-A01A30DBAC7D';
static readonly VIEWTYPE_LISTVIEW:string = 'F275FB68-94E1-4697-8C5E-1B83935916F4';


    }
    //THIS IS FILE GENERATED. PLEASE DON'T EDIT
    export class APP_REPORTVIEW {

        static readonly ACKNOWLEDGEMENTREPORT:string = '364E3A4C-8218-4577-8E08-A0E2914F0097';
static readonly ADMISSIONREPORT:string = '401337DF-B43B-48DE-A109-52C4380F2471';
static readonly AGREEMENTFORCONFINEMENT:string = '0A830A2D-C118-4CF3-BCAA-CD575A562920';
static readonly ANIMACARE__ACKNOWLEDGEMENTREPORT:string = '8224E644-ED3B-4076-A905-363EF3FD4B86';
static readonly ANIMACARE__ADMISSIONREPORT:string = '88C1E8C5-62EA-4703-848C-21E31A3A2480';
static readonly ANIMACARE__AGREEMENTFORCONFINEMENT:string = '033E1E91-753C-42F8-BA0B-5E62FBC97D68';
static readonly ANIMACARE__CONCENTTOOPERATION:string = 'AC6C2C55-DFF3-419E-8B2C-17555A38690A';
static readonly ANIMACARE__EUTHANASIAAUTHORIZATION:string = '615EE552-05AB-4122-A94A-1631B90FE716';
static readonly ANIMACARE__VETERINARYHEALTHCLINIC:string = '7ECA2157-C5E4-401F-8272-F9E50ABF2BFD';
static readonly BARANGAYCLEARANCE:string = 'F4209948-D960-4A33-AAA1-A91142BBA066';
static readonly BARANGAYCLEARANCEFORMREPORT:string = 'D03BBDF7-298C-4680-95B1-075BAB0D6997';
static readonly BARANGAYCLEARANCEFORMREPORT__CITYOFLIPABATANGAS__BRGYTAMBO:string = 'AE3F00A2-1A9C-4C10-968A-17D631B29037';
static readonly BARANGAYCLEARANCEFORMREPORT_CITYOFLIPABATANGAS_BRGYTAMBO:string = '74640E4D-E7E5-4B8B-B7E0-E4EAB26E6833';
static readonly BARANGAYCOMPLAINTREPORT:string = '3A0C99D8-4634-4C38-B7BD-E081509BF7B1';
static readonly BARCCERTIFICATE:string = 'FDC74CDC-9404-4EC8-A3E7-03F44E202F8B';
static readonly BILLINGINVOICEAGINGREPORT:string = 'A95F5C94-8DFD-4402-80F6-AB76C76DF3DC';
static readonly BILLINGINVOICEPAIDLISTREPORT:string = '2CE71017-E906-4C8C-B9F2-AB02B6125312';
static readonly CELESTIALANIMALCLINIC__ACKNOWLEDGEMENTREPORT:string = '5FCB692A-C0B7-44CD-A17F-659FF4F642BB';
static readonly CELESTIALANIMALCLINIC__ADMISSIONREPORT:string = '92E44EED-4269-47DD-96C5-D0F4451ED95C';
static readonly CELESTIALANIMALCLINIC__AGREEMENTFORCONFINEMENT:string = '95E27D74-1330-4341-9119-FD8C27F55DD4';
static readonly CELESTIALANIMALCLINIC__CONCENTTOOPERATION:string = '878561E2-C075-44B2-A31F-82AB9F6FAFB9';
static readonly CELESTIALANIMALCLINIC__EUTHANASIAAUTHORIZATION:string = '034163C2-305A-4D75-AB56-E21D776B6294';
static readonly CELESTIALANIMALCLINIC__VETERINARYHEALTHCLINIC:string = '5A2269C5-C14F-40B4-8DC6-A6F9B7492CBD';
static readonly CERIFICATIONAGRICULTURALLAND:string = 'E13AF155-B42B-4784-9141-6E912F810F27';
static readonly CERTIFICATEOF14DAYQUARANTINECOMPLETION:string = '614F7C1D-AB60-4E0A-BEC0-07432DDE89E9';
static readonly CERTIFICATEOF7DAYQUARANTINE:string = 'AC4F83C6-F2B5-43E6-99DB-DF4057AD121F';
static readonly CERTIFICATEOFACCEPTANCE:string = 'BD6CAACD-C50F-487C-814D-2E6ED6A82B05';
static readonly CERTIFICATEOFEXCLUSION_COVIDINVESTIGATION:string = 'BF05A2DE-95EA-4775-AB75-FF181E1176DE';
static readonly CERTIFICATEOFGOODMORAL:string = '8DFE49B3-72F7-44C6-A758-72169731D6C4';
static readonly CERTIFICATEOFINCLUSION_BHERTS:string = '90B6EC04-F97C-4D77-AAC6-13F859E7097D';
static readonly CERTIFICATEOFRESIDENCYFORMREPORT_CITYOFLIPABATANGAS_BRGYTAMBO:string = '4288BDC0-F80B-4848-B7EF-015E8EA4953C';
static readonly CERTIFICATEOFSUSTAINABLELIVELIHOODPROGRAM:string = 'FD7E8AB9-B333-43A8-9609-A3FDE799A72F';
static readonly CERTIFICATEOFTRAVELDUETOEXAM:string = 'ADE580BA-8563-471B-8316-FBA8B0FC60A1';
static readonly CERTIFICATETOGOBACKTOPREVIOUSRESIDENCE:string = '2D31805E-4118-418A-8B2E-5B488342C7A2';
static readonly CERTIFICATETOTRAVEL:string = '6E0CCF13-E877-42FF-AEB1-6D982F47C53C';
static readonly CERTIFICATIONFORNONINCOME:string = '1640D6BA-C6EF-4EF7-9002-1B83BBC7A677';
static readonly CERTIFICATIONFORWORKANDFAMILY:string = '466BC54F-94FD-4394-B568-A0B911B68B5C';
static readonly CERTIFICATIONINDEGENCY:string = '29D064A0-9639-4E64-BBDF-79ADC7933958';
static readonly CERTIFICATIONINDIGENCYFORMREPORT:string = '89D91511-9EE2-46A6-B3EA-AF611E7F1668';
static readonly CERTIFICATIONINDIGENCYFORMREPORT_CITYOFLIPABATANGAS_BRGYTAMBO:string = 'A7FDA0F5-6AA5-4E4B-AC14-A130F8A81D5A';
static readonly CERTIFICATIONOF14QUARANTINE:string = 'AE5A9F66-F71A-4A05-ACCB-9239EFB2B6F1';
static readonly CERTIFICATIONOFCOVIDFREECASE:string = '5E856C77-869D-4155-B423-78E77DE7894B';
static readonly CERTIFICATIONOFDEATH:string = '823ACF02-E4F2-4803-BA7E-696FB0150566';
static readonly CERTIFICATIONOFEARNINGS:string = '474140E2-EBA9-4898-ACF9-A87500AB8DE3';
static readonly CERTIFICATIONOFEFFECTOFLOCKDOWN:string = 'E32FEBB0-A00A-42BA-BD35-7E76562BBF3D';
static readonly CERTIFICATIONOFISOLATION:string = '251FE96B-50E1-4ECD-907B-3F2DF36AAED9';
static readonly CERTIFICATIONOFNOINCOMEFORM:string = '3F9E46F8-DC99-41C4-A851-479722EAB41F';
static readonly CERTIFICATIONOFNONEMPLOYED:string = 'F502BC2A-DE6E-4D6B-989D-B4E9125B0B5D';
static readonly CERTIFICATIONOFNONRESIDENT:string = '0997E546-B724-45B2-8EC6-98A97158D5BF';
static readonly CERTIFICATIONOFNOOFFENSEINKATARUNGANGPAMBARANGAY:string = '83E515C8-5C7A-45F3-BA4F-6EE8928CFAD8';
static readonly CERTIFICATIONOFNORECORDFORBHERTS:string = '5D9C29D2-08DF-4E34-9D30-905F4CE4572B';
static readonly CERTIFICATIONOFPAFEXAMINATION:string = '0D82E1BE-1A3C-40A1-BD36-BBB5EDDB56AB';
static readonly CERTIFICATIONOFPATRICIPATIONOFBUSINESS:string = '15291235-B8E8-4B66-8BF1-7FDC9E329919';
static readonly CERTIFICATIONOFRESIDENCY:string = '12F0746D-FFE5-4EAD-AF6A-D06DB63BDBA7';
static readonly CERTIFICATIONOFRESIDENCY_HEADOFFAMILY:string = 'CE71BC56-136D-4099-BCAE-28DBB86F9C64';
static readonly CERTIFICATIONOFRESIDENCY_PERMANENT:string = '29E2103E-3402-48BC-A8B0-ABB895D7EEA9';
static readonly CERTIFICATIONOFRTPCTESTREQUEST:string = 'D40D8CF8-5D4C-4745-8DD2-0942644BF956';
static readonly CERTIFICATIONOFSELLER:string = 'B9217935-3B02-496F-9294-8866CB4AD10C';
static readonly CERTIFICATIONRESIDENCYFORMREPORT:string = 'C14362D9-E1CC-44E6-86D6-09BC3F37A51D';
static readonly CLEARANCETOCONSTRUCT:string = 'D51A6C43-A36E-41FD-8573-3BF36870A6E1';
static readonly CLEARANCETOOPERATE:string = '6921F158-3C40-4534-8ED8-4C2EB1961F99';
static readonly CLEARANCETOOPERATEFORMREPORT_CITYOFLIPABATANGAS_BRGYTAMBO:string = 'F45D9500-C8A8-44DF-BFB8-4BA9CEC2E6AA';
static readonly CLIENTDEPOSITREPORT:string = '93926488-574F-4F64-A01A-CA68EF5F3EA2';
static readonly CLINICAVETERINARIA__AGAINSTMEDICALADVICE:string = 'FC2C4709-3102-4A4B-81F5-9E0DF5A4F367';
static readonly CLINICAVETERINARIA__CONFINEMENTSURGERYAGREEMENT:string = 'B655E69F-B2D1-4798-871C-A3BD5B893175';
static readonly CLINICAVETERINARIA__CONSENTFORSURGERY:string = '36AFB6CD-C35F-4F53-B112-4944C5A24FB7';
static readonly CLINICAVETERINARIA__DISCHARGEINSTRUCTION:string = 'ECEE03F5-E822-40F2-91C1-6CA7B2D12E76';
static readonly CLINICAVETERINARIA__EUTHANASIAAUTHORIZATION:string = '6BD29CED-9B38-43F2-A087-BEE41DE622A1';
static readonly CONCENTTOOPERATION:string = '0C4DF27F-214B-4343-85C5-7257290732E3';
static readonly DENTALEXAMINATION:string = '015D04C6-CD29-4511-9665-B6CD862FC076';
static readonly DENTALEXAMINATIONPRESCRIPTION:string = 'E988843A-0CC0-4237-B210-2656213748C7';
static readonly EUTHANASIAAUTHORIZATION:string = '121502C0-0659-4EA4-8C51-DF9F50CB400D';
static readonly HEALTHCERTIFICATE:string = 'E53F5C56-9B9B-456C-92EE-322AB51BE5BE';
static readonly INVENTORYDETAILREPORT:string = 'B8F1D7AF-9BE0-4F33-9C2A-2236B6E31A28';
static readonly INVENTORYSUMMARYREPORT:string = 'C65DCB24-C60D-47BA-B994-5EA4CAFBE3BE';
static readonly ITEMMASTERFILE:string = '450CABE0-1AFB-4B96-8FC2-1B6DDC2C1B96';
static readonly LISTOFEXCLUSIONNONCOVIDRESIDENTS:string = '2CB7BC12-D5C4-44D1-B846-6848E9610487';
static readonly PAGPAPATUNAY:string = '1A3908E9-D9B4-480F-8795-BBAC0D3E1ABF';
static readonly PAGPAPATUNAYNANAKAUWINASAKANILANGBAYAN:string = '97613388-0AA9-4676-8927-3B62C3DA4870';
static readonly PAGPAPATUNAYNGEVACUATE:string = '0A102254-FAA3-4CE8-B494-F0F2C916EAD4';
static readonly PAGPAPATUNAYNGHINDIMAKAKAKUHANGSAP:string = 'BD485A8E-3320-46A3-9EBE-DF3F4D658083';
static readonly PAGPAPATUNAYSOLOPARENT:string = '11757652-4DCC-448D-A3FD-FFC325D51094';
static readonly PATIENTBILLINGINVOICEREPORT:string = 'F0965A00-9237-44C7-BE71-8F50F0A944B4';
static readonly PATIENTBILLINGINVOICEREPORTNODISCOUNTPERITEM:string = '31149445-5F30-419E-9BF2-6580D1CF85F6';
static readonly PATIENTSOAP:string = 'C85BF9E5-24AD-4DB7-8485-CA44DC525762';
static readonly PATIENTSOAPPRESCRIPTION:string = 'CF559745-8872-4442-A463-2C146E1DA503';
static readonly PAYMENTTRANSACTIONDETAILREPORT:string = '9069283A-2496-4320-8303-04241B80C658';
static readonly PAYMENTTRANSACTIONREPORT:string = 'CB195754-192E-4E7B-AF1F-909F3A726B4A';
static readonly POSSUMMARY:string = 'B203B5CB-AA13-4EF6-8F8F-8874F017347C';
static readonly PURCHASEORDERREPORT:string = '937874F1-DCC4-41F8-95CE-8D17A77EF2F3';
static readonly QUARANTINEPASSCERTIFICATION:string = '3AC6E0C6-56AF-4EC0-BCB1-9C822C295F76';
static readonly RECEIVEDDEPOSITCREDITS:string = 'DD2E14B6-CF4D-4532-83A5-7F8B65A315AE';
static readonly RECEIVINGREPORT:string = '457C015B-944D-4C67-BA8A-430FCE9663E4';
static readonly RESIDENTBUSINESSREPORT:string = 'FE7C1E33-B2AC-4ACE-8F12-0C605E56E582';
static readonly RESIDENTDETAILREPORT:string = 'DC5CB091-83E7-4187-9B45-B1EBDCC3BA82';
static readonly RESIDENTPROGRAMREPORT:string = '1860FD0F-43FC-42AE-A38A-06484346BE3C';
static readonly RESIDENTREQUESTBARANGAYBUSINESSCLEARANCE:string = 'E9CD9FB2-32FF-43CE-AFA6-9D309D7F1264';
static readonly RESIDENTREQUESTCERTIFICATE:string = '22C87B66-F7C5-485F-AF39-261AFA651D27';
static readonly RESIDENTREQUESTCERTIFICATEINDIGENCY:string = '92781692-782E-43C5-BE51-5D72DFE3453D';
static readonly RESIDENTVOTERLISTREPORT:string = 'D30612BB-E3E5-4A8B-8DB0-AD81E5507D60';
static readonly SALESINCOMEREPORT:string = '648A3F5F-381F-40EB-8775-78BA465C69DD';
static readonly SOLOPARENTFORM:string = 'EC32EAFC-0046-4D3D-AF4D-1BE968A627F0';
static readonly TANGGAPANNGBARCCHAIRMAN:string = '482635BD-1E42-41AF-A471-8FF1BC912361';
static readonly VETERINARYHEALTHCLINIC:string = '911F3BCE-F65D-4126-A6B2-5246E79C8580';

    }