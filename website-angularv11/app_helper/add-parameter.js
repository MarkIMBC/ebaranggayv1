const {
    columns
} = require("mssql");

var CurrentObject = {
    "ID": 36943,
    "Code": "SOAP-01077",
    "Name": null,
    "IsActive": true,
    "ID_Company": 1,
    "Comment": null,
    "DateCreated": "2021-04-06T14:09:04.85",
    "DateModified": "2021-04-10T09:05:40.01",
    "ID_CreatedBy": 10,
    "ID_LastModifiedBy": 1,
    "Subjective": null,
    "Objective": null,
    "Assessment": null,
    "Prescription": null,
    "Planning": null,
    "ID_Patient": 38084,
    "Date": "2021-04-06T14:04:04",
    "ID_SOAPType": 1,
    "ID_ApprovedBy": 10,
    "ID_CanceledBy": null,
    "DateApproved": "2021-04-06T14:09:39.63",
    "DateCanceled": null,
    "ID_FilingStatus": 3,
    "Old_soap_id": null,
    "LabImageFilePath01": "169617776_509378580080606_5982359800116532744_n_20210406140904.jpg",
    "LabImageFilePath02": "169088430_1327445050989061_7595465837276634910_n_20210406140904.jpg",
    "LabImageFilePath03": null,
    "LabImageFilePath04": null,
    "LabImageFilePath05": null,
    "LabImageFilePath06": null,
    "LabImageFilePath07": null,
    "LabImageFilePath08": null,
    "LabImageFilePath09": null,
    "LabImageFilePath10": null,
    "LabImageFilePath11": null,
    "LabImageFilePath12": null,
    "LabImageFilePath13": null,
    "LabImageFilePath14": null,
    "LabImageFilePath15": null,
    "LabImageRemark01": "CBC: \t\r  Wbc= \t\r  Lym= \t\r  Mon= \t\r  Neu= \t\r  Eos= \t\r  Bas= \t\r  Rbc= \t\r  Hgb= \t\r  Hct= \t\r  Mcv= \t\r  Mch= \t\r  Mchc \t\r  Plt= \t\r  Mpv= \t\r\t\rBlood Chem: \t\r  Alt= \t\r  Alp= \t\r  Alb= \t\r  Amy= \t\r  Tbil= \t\r  Bun= \t\r  Crea= \t\r  Ca= \t\r  Phos= \t\r  Glu= \t\r  Na= \t\r  K= \t\r  TP= \t\r  Glob= \t\r\t\rMicroscopic Exam: \t\r",
    "LabImageRemark02": "CBC: \t\r  Wbc= \t\r  Lym= \t\r  Mon= \t\r  Neu= \t\r  Eos= \t\r  Bas= \t\r  Rbc= \t\r  Hgb= \t\r  Hct= \t\r  Mcv= \t\r  Mch= \t\r  Mchc \t\r  Plt= \t\r  Mpv= \t\r\t\rBlood Chem: \t\r  Alt= \t\r  Alp= \t\r  Alb= \t\r  Amy= \t\r  Tbil= \t\r  Bun= \t\r  Crea= \t\r  Ca= \t\r  Phos= \t\r  Glu= \t\r  Na= \t\r  K= \t\r  TP= \t\r  Glob= \t\r\t\rMicroscopic Exam: \t\r",
    "LabImageRemark03": null,
    "LabImageRemark04": null,
    "LabImageRemark05": null,
    "LabImageRemark06": null,
    "LabImageRemark07": null,
    "LabImageRemark08": null,
    "LabImageRemark09": null,
    "LabImageRemark10": null,
    "LabImageRemark11": null,
    "LabImageRemark12": null,
    "LabImageRemark13": null,
    "LabImageRemark14": null,
    "LabImageRemark15": null,
    "LabImageRowIndex01": "1",
    "LabImageRowIndex02": "2",
    "LabImageRowIndex03": null,
    "LabImageRowIndex04": null,
    "LabImageRowIndex05": null,
    "LabImageRowIndex06": null,
    "LabImageRowIndex07": null,
    "LabImageRowIndex08": null,
    "LabImageRowIndex09": null,
    "LabImageRowIndex10": null,
    "LabImageRowIndex11": null,
    "LabImageRowIndex12": null,
    "LabImageRowIndex13": null,
    "LabImageRowIndex14": null,
    "LabImageRowIndex15": null,
    "History": "quajsdiasfuaedjifvnijf",
    "AttendingPhysician_ID_Employee": 218,
    "Diagnosis": "Differential Diagnosis: \tqwe\nNotes: \t\nTest Results: \t\nFinal Diagnosis: \tasd\nPrognosis: \t\nCategory: \t\n",
    "Treatment": "qwdasnsidgnssdf",
    "ClientCommunication": null,
    "ClinicalExamination": "Weight : \nBody Temp: ",
    "Interpretation": "qwjdhaidhasdfkfnjdfv",
    "CreatedBy": null,
    "LastModifiedBy": "Depaur, Mark Joseph Origenes",
    "Name_CreatedBy": null,
    "Name_LastModifiedBy": "Depaur, Mark Joseph Origenes",
    "DateString": "04/06/2021",
    "Name_SOAPType": "Consultation",
    "Name_Patient": "567567567",
    "Name_Client": "Maureen Jane Villasanta",
    "Name_ApprovedBy": null,
    "Name_CanceledBy": null,
    "Name_FilingStatus": "Approved",
    "ID_Client": 33381,
    "AttendingPhysician_Name_Employee": "Balbacal, Lester  Gonzalvo",
    "IsDeceased": false,
    "ObjectiveTemplate": "Heart Rate (bpm): \t\rRespiratory Rate (brpm): \t\rWeight (kg): \t\rLength (cm): \t\rCRT: \t\rBCS: \t\rLymph Nodes: \t\rPalpebral Reflex: \t\rTemperature: \t\r",
    "AssessmentTemplate": "Differential Diagnosis: \t\rNotes: \t\rTest Results: \t\rFinal Diagnosis: \t\rPrognosis: \t\rCategory: \t\r",
    "LaboratoryTemplate": "CBC: \t\r  Wbc= \t\r  Lym= \t\r  Mon= \t\r  Neu= \t\r  Eos= \t\r  Bas= \t\r  Rbc= \t\r  Hgb= \t\r  Hct= \t\r  Mcv= \t\r  Mch= \t\r  Mchc \t\r  Plt= \t\r  Mpv= \t\r\t\rBlood Chem: \t\r  Alt= \t\r  Alp= \t\r  Alb= \t\r  Amy= \t\r  Tbil= \t\r  Bun= \t\r  Crea= \t\r  Ca= \t\r  Phos= \t\r  Glu= \t\r  Na= \t\r  K= \t\r  TP= \t\r  Glob= \t\r\t\rMicroscopic Exam: \t\r",
    "ClinicalExaminationTemplate": "Heart Rate (bpm): \t\rRespiratory Rate (brpm): \t\rWeight (kg): \t\rLength (cm): \t\rCRT: \t\rBCS: \t\rLymph Nodes: \t\rPalpebral Reflex: \t\rTemperature: \t\r",
    "DiagnosisTemplate": "Differential Diagnosis: \t\rNotes: \t\rTest Results: \t\rFinal Diagnosis: \t\rPrognosis: \t\rCategory: \t\r",
    "_IsNew": false,
    "LabImages": [{
            "ImageRowIndex": 2,
            "RowIndex": 2,
            "ImageNo": "02",
            "FilePath": "169088430_1327445050989061_7595465837276634910_n_20210406140904.jpg",
            "Remark": "CBC: \t\r  Wbc= \t\r  Lym= \t\r  Mon= \t\r  Neu= \t\r  Eos= \t\r  Bas= \t\r  Rbc= \t\r  Hgb= \t\r  Hct= \t\r  Mcv= \t\r  Mch= \t\r  Mchc \t\r  Plt= \t\r  Mpv= \t\r\t\rBlood Chem: \t\r  Alt= \t\r  Alp= \t\r  Alb= \t\r  Amy= \t\r  Tbil= \t\r  Bun= \t\r  Crea= \t\r  Ca= \t\r  Phos= \t\r  Glu= \t\r  Na= \t\r  K= \t\r  TP= \t\r  Glob= \t\r\t\rMicroscopic Exam: \t\r",
            "_IsNew": false
        },
        {
            "ImageRowIndex": 1,
            "RowIndex": 1,
            "ImageNo": "01",
            "FilePath": "169617776_509378580080606_5982359800116532744_n_20210406140904.jpg",
            "Remark": "CBC: \t\r  Wbc= \t\r  Lym= \t\r  Mon= \t\r  Neu= \t\r  Eos= \t\r  Bas= \t\r  Rbc= \t\r  Hgb= \t\r  Hct= \t\r  Mcv= \t\r  Mch= \t\r  Mchc \t\r  Plt= \t\r  Mpv= \t\r\t\rBlood Chem: \t\r  Alt= \t\r  Alp= \t\r  Alb= \t\r  Amy= \t\r  Tbil= \t\r  Bun= \t\r  Crea= \t\r  Ca= \t\r  Phos= \t\r  Glu= \t\r  Na= \t\r  K= \t\r  TP= \t\r  Glob= \t\r\t\rMicroscopic Exam: \t\r",
            "_IsNew": false
        }
    ],
    "Patient_SOAP_Plan": [{
            "ID": 11982,
            "Code": null,
            "Name": null,
            "ID_Patient_SOAP": 36943,
            "DateReturn": null,
            "ID_Item": 15406,
            "Comment": "56756",
            "DateCreated": "2021-04-10T09:05:21.403",
            "DateModified": "2021-04-10T09:05:21.403",
            "DateSent": null,
            "IsSentSMS": null,
            "Name_Item": "Deworming 20kg",
            "_IsNew": false
        },
        {
            "ID": 11107,
            "Code": null,
            "Name": null,
            "ID_Patient_SOAP": 36943,
            "DateReturn": "2021-04-22T14:06:32",
            "ID_Item": 7498,
            "Comment": "qsadnafijfdsjidjigbjidfjnigb",
            "DateCreated": "2021-04-06T14:09:04.867",
            "DateModified": "2021-04-06T14:09:04.867",
            "DateSent": null,
            "IsSentSMS": null,
            "Name_Item": "Vaccination 5in1 Dog",
            "_IsNew": false
        }
    ],
    "Patient_SOAP_Prescription": [{
            "ID": 1044,
            "Code": null,
            "Name": null,
            "IsActive": true,
            "ID_Company": null,
            "Comment": null,
            "DateCreated": "2021-04-06T14:09:04.857",
            "DateModified": "2021-04-10T09:05:40.017",
            "ID_CreatedBy": 10,
            "ID_LastModifiedBy": 1,
            "ID_Item": 13190,
            "ID_Patient_SOAP": 36943,
            "Quantity": 2,
            "CreatedBy": null,
            "LastModifiedBy": "Depaur, Mark Joseph Origenes",
            "Name_Item": "Brand A 100 ml ",
            "_IsNew": false
        },
        {
            "ID": 1045,
            "Code": null,
            "Name": null,
            "IsActive": true,
            "ID_Company": null,
            "Comment": null,
            "DateCreated": "2021-04-06T14:09:04.857",
            "DateModified": "2021-04-10T09:05:40.017",
            "ID_CreatedBy": 10,
            "ID_LastModifiedBy": 1,
            "ID_Item": 13213,
            "ID_Patient_SOAP": 36943,
            "Quantity": 3,
            "CreatedBy": null,
            "LastModifiedBy": "Depaur, Mark Joseph Origenes",
            "Name_Item": "Medicine AAA 150 ml ",
            "_IsNew": false
        },
        {
            "ID": 1046,
            "Code": null,
            "Name": null,
            "IsActive": true,
            "ID_Company": null,
            "Comment": null,
            "DateCreated": "2021-04-06T14:09:04.857",
            "DateModified": "2021-04-10T09:05:40.017",
            "ID_CreatedBy": 10,
            "ID_LastModifiedBy": 1,
            "ID_Item": 13338,
            "ID_Patient_SOAP": 36943,
            "Quantity": 4,
            "CreatedBy": null,
            "LastModifiedBy": "Depaur, Mark Joseph Origenes",
            "Name_Item": "Herbal Vit (Bottle)",
            "_IsNew": false
        }
    ]
}



for (const property in CurrentObject) {

    // console.log(`${property}: ${CurrentObject[property]}`);
}

var propertyList = ['Patient_SOAP_Plan', 'Patient_SOAP_Prescription']

var propertyListDateColumnsData = [{
    "DateColumnName": "Date"
}, {
    "DateColumnName": "DateApproved"
}, {
    "DateColumnName": "DateCanceled"
}, {
    "DateColumnName": "DateCreated"
}, {
    "DateColumnName": "DateModified"
}, {
    "DateColumnName": "DateReturn"
}]



var propertyList = [];
var dateColumns = [];

propertyListDateColumnsData.forEach((Obj) => {

    dateColumns.push(Obj.DateColumnName);
});


var convertDateToString = (Obj) => {

    for (const property in Obj) {

        var hasOnPRopertyList = propertyList.includes(property);

        if (hasOnPRopertyList == true) {

            if(Obj[property] == null) Obj[property]= [];

            Obj[property].forEach((detail) => {

                convertDateToString(detail);
            })

        } else {

            if (dateColumns.includes(property)) Obj[property] = 'Convert Ka na dapata';
        }

    }
}

convertDateToString(CurrentObject)

console.log(CurrentObject);

