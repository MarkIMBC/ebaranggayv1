﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SqlQueryNotificationStoredProcedure-590d43e9-0bb5-4c44-a071-a27802beb6a9] AS BEGIN BEGIN TRANSACTION; RECEIVE TOP(0) conversation_handle FROM [SqlQueryNotificationService-590d43e9-0bb5-4c44-a071-a27802beb6a9]; IF (SELECT COUNT(*) FROM [SqlQueryNotificationService-590d43e9-0bb5-4c44-a071-a27802beb6a9] WHERE message_type_name = 'http://schemas.microsoft.com/SQL/ServiceBroker/DialogTimer') > 0 BEGIN if ((SELECT COUNT(*) FROM sys.services WHERE name = 'SqlQueryNotificationService-590d43e9-0bb5-4c44-a071-a27802beb6a9') > 0)   DROP SERVICE [SqlQueryNotificationService-590d43e9-0bb5-4c44-a071-a27802beb6a9]; if (OBJECT_ID('SqlQueryNotificationService-590d43e9-0bb5-4c44-a071-a27802beb6a9', 'SQ') IS NOT NULL)   DROP QUEUE [SqlQueryNotificationService-590d43e9-0bb5-4c44-a071-a27802beb6a9]; DROP PROCEDURE [SqlQueryNotificationStoredProcedure-590d43e9-0bb5-4c44-a071-a27802beb6a9]; END COMMIT TRANSACTION; END
GO
