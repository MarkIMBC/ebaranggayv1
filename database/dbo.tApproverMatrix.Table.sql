﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tApproverMatrix](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
 CONSTRAINT [PK_tApproverMatrix] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tApproverMatrix] ADD  CONSTRAINT [DF__tApprover__IsAct__3429BB53]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tApproverMatrix]  WITH NOCHECK ADD  CONSTRAINT [FK_tApproverMatrix_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tApproverMatrix] CHECK CONSTRAINT [FK_tApproverMatrix_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tApproverMatrix]  WITH NOCHECK ADD  CONSTRAINT [FK_tApproverMatrix_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tApproverMatrix] CHECK CONSTRAINT [FK_tApproverMatrix_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tApproverMatrix]
		ON [dbo].[tApproverMatrix]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tApproverMatrix
			SET    DateCreated = GETDATE()
			FROM   dbo.tApproverMatrix hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tApproverMatrix] ENABLE TRIGGER [rDateCreated_tApproverMatrix]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tApproverMatrix]
		ON [dbo].[tApproverMatrix]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tApproverMatrix
			SET    DateModified = GETDATE()
			FROM   dbo.tApproverMatrix hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tApproverMatrix] ENABLE TRIGGER [rDateModified_tApproverMatrix]
GO
