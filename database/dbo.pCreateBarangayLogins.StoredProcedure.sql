﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE    
 PROC [dbo].[pCreateBarangayLogins](@CompanyName       VARCHAR(MAX),
                                 @CompanyCode       VARCHAR(MAX),
                                 @MaxSMSCountPerDay INT = 25)
AS
  BEGIN
      exec pCreateNewCompanyAccess_validation
        @CompanyName,
        @CompanyCode

      exec pCreateCompanyLogins
        @CompanyName,
        @CompanyCode,
        @MaxSMSCountPerDay

      Update tCompany
      SET    MainRouteLink = 'Barangay'
      WHERE  Code = @CompanyCode
             AND Name = @CompanyName
  END

GO
