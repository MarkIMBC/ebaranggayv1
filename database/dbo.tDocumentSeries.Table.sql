﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tDocumentSeries](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_Model] [uniqueidentifier] NULL,
	[Counter] [int] NULL,
	[Prefix] [varchar](300) NULL,
	[IsAppendCurrentDate] [bit] NULL,
	[DigitCount] [int] NULL,
	[ID_Company] [int] NULL,
 CONSTRAINT [PK_tDocumentSeries] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tDocumentSeries] ADD  CONSTRAINT [DF__tDocument__IsAct__69478F08]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tDocumentSeries]  WITH NOCHECK ADD  CONSTRAINT [FK_tDocumentSeries_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tDocumentSeries] CHECK CONSTRAINT [FK_tDocumentSeries_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tDocumentSeries]  WITH NOCHECK ADD  CONSTRAINT [FK_tDocumentSeries_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tDocumentSeries] CHECK CONSTRAINT [FK_tDocumentSeries_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tDocumentSeries]
		ON [dbo].[tDocumentSeries]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tDocumentSeries
			SET    DateCreated = GETDATE()
			FROM   dbo.tDocumentSeries hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tDocumentSeries] ENABLE TRIGGER [rDateCreated_tDocumentSeries]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tDocumentSeries]
		ON [dbo].[tDocumentSeries]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tDocumentSeries
			SET    DateModified = GETDATE()
			FROM   dbo.tDocumentSeries hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tDocumentSeries] ENABLE TRIGGER [rDateModified_tDocumentSeries]
GO
