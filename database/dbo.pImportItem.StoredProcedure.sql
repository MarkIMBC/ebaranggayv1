﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[pImportItem] (@records typImportItem READONLY, @ID_UserSession INT)
AS
BEGIN

  DECLARE @ID_Company INT = 0;
  DECLARE @ID_User INT = 0;

  DECLARE @Success BIT = 1;
  DECLARE @message VARCHAR(300) = '';


  DECLARE @items TABLE (
    Old_item_id INT
   ,Name VARCHAR(MAX)
   ,category VARCHAR(MAX)
   ,UnitPrice DECIMAL(18, 2)
   ,UnitCost DECIMAL(18, 2)
  )

  DECLARE @Old_item_ids TABLE (
    Old_item_id INT
  )

  SELECT
    @ID_User = ID_User
  FROM dbo.tUserSession
  WHERE ID = @ID_UserSession;

  SELECT
    @ID_Company = u.ID_Company
  FROM vUser u
  WHERE u.ID = @ID_User


  INSERT @items (Old_item_id,
  Name,
  category,
  UnitCost,
  UnitPrice)
    SELECT
      record.product_id
     ,ISNULL(record.brand_name, '') + ' ' + ISNULL(record.generic_name, '') + ' ' +
      CASE
        WHEN record.packaging_dosage_form = 'n/a' THEN ''
        ELSE ISNULL(packaging_dosage_form, '')
      END
     ,record.category
     ,record.price
     ,record.selling_price
    FROM @records record


  INSERT @Old_item_ids (Old_item_id)
    SELECT
      CONVERT(INT, Old_item_id)
    FROM @items
    EXCEPT
    SELECT
      i.Old_item_id
    FROM tItem i
    WHERE i.ID_Company = @ID_Company


  INSERT tItem (Old_item_id,
  ID_ItemType,
  Name,
  UnitCost,
  UnitPrice,
  ID_ItemCategory,
  Code,
  IsActive,
  ID_Company,
  Comment,
  DateCreated,
  DateModified,
  ID_CreatedBy,
  ID_LastModifiedBy)

    SELECT
      rec.Old_item_id
     ,2
     ,rec.Name
     ,rec.UnitCost
     ,rec.UnitPrice
     ,NULL
     ,NULL
     ,1
     ,@ID_Company
     ,NULL
     ,GETDATE()
     ,GETDATE()
     ,@ID_User
     ,@ID_User
    FROM @items rec
    WHERE CONVERT(INT, Old_item_id) IN (SELECT
        CONVERT(INT, Old_item_id)
      FROM @Old_item_ids)

  UPDATE tItem
  SET Name = record.Name
     ,UnitCost = record.UnitCost
     ,UnitPrice = record.UnitPrice
     ,DateModified = GETDATE()
  FROM tItem i
  INNER JOIN @items record
    ON i.Old_item_id = record.Old_item_id
  WHERE i.ID_Company = @ID_Company

  SELECT
    '_';

  SELECT
    @Success Success
   ,@message message;


END
GO
