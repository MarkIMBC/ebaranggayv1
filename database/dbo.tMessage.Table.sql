﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tMessage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_User] [int] NULL,
	[Sender_ID_User] [int] NULL,
	[IsOpened] [bit] NULL,
	[Recipient_ID_User] [int] NULL,
 CONSTRAINT [PK_tMessage] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tMessage] ON [dbo].[tMessage]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tMessage] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tMessage]  WITH CHECK ADD  CONSTRAINT [FK_tMessage_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tMessage] CHECK CONSTRAINT [FK_tMessage_ID_Company]
GO
ALTER TABLE [dbo].[tMessage]  WITH CHECK ADD  CONSTRAINT [FK_tMessage_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tMessage] CHECK CONSTRAINT [FK_tMessage_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tMessage]  WITH CHECK ADD  CONSTRAINT [FK_tMessage_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tMessage] CHECK CONSTRAINT [FK_tMessage_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tMessage]
		ON [dbo].[tMessage]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tMessage
			SET    DateCreated = GETDATE()
			FROM   dbo.tMessage hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tMessage] ENABLE TRIGGER [rDateCreated_tMessage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tMessage]
		ON [dbo].[tMessage]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tMessage
			SET    DateModified = GETDATE()
			FROM   dbo.tMessage hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tMessage] ENABLE TRIGGER [rDateModified_tMessage]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[tr_dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde_Sender] ON [dbo].[tMessage] 
WITH EXECUTE AS SELF
AFTER insert, update, delete AS 
BEGIN
    SET NOCOUNT ON;

    DECLARE @rowsToProcess INT
    DECLARE @currentRow INT
    DECLARE @records XML
    DECLARE @theMessageContainer NVARCHAR(MAX)
    DECLARE @dmlType NVARCHAR(10)
    DECLARE @modifiedRecordsTable TABLE ([RowNumber] INT IDENTITY(1, 1), [ID] int, [Code] varchar(50), [Name] varchar(200), [IsActive] bit, [ID_Company] int, [Comment] varchar(MAX), [DateCreated] datetime, [DateModified] datetime, [ID_CreatedBy] int, [ID_LastModifiedBy] int, [ID_User] int, [Recipient_ID_User] int, [Sender_ID_User] int, [IsOpened] bit)
    DECLARE @exceptTable TABLE ([RowNumber] INT, [ID] int, [Code] varchar(50), [Name] varchar(200), [IsActive] bit, [ID_Company] int, [Comment] varchar(MAX), [DateCreated] datetime, [DateModified] datetime, [ID_CreatedBy] int, [ID_LastModifiedBy] int, [ID_User] int, [Recipient_ID_User] int, [Sender_ID_User] int, [IsOpened] bit)
	DECLARE @deletedTable TABLE ([RowNumber] INT IDENTITY(1, 1), [ID] int, [Code] varchar(50), [Name] varchar(200), [IsActive] bit, [ID_Company] int, [Comment] varchar(MAX), [DateCreated] datetime, [DateModified] datetime, [ID_CreatedBy] int, [ID_LastModifiedBy] int, [ID_User] int, [Recipient_ID_User] int, [Sender_ID_User] int, [IsOpened] bit)
    DECLARE @insertedTable TABLE ([RowNumber] INT IDENTITY(1, 1), [ID] int, [Code] varchar(50), [Name] varchar(200), [IsActive] bit, [ID_Company] int, [Comment] varchar(MAX), [DateCreated] datetime, [DateModified] datetime, [ID_CreatedBy] int, [ID_LastModifiedBy] int, [ID_User] int, [Recipient_ID_User] int, [Sender_ID_User] int, [IsOpened] bit)
    DECLARE @var1 int
    DECLARE @var2 varchar(50)
    DECLARE @var3 varchar(200)
    DECLARE @var4 bit
    DECLARE @var5 int
    DECLARE @var6 varchar(max)
    DECLARE @var7 datetime
    DECLARE @var8 datetime
    DECLARE @var9 int
    DECLARE @var10 int
    DECLARE @var11 int
    DECLARE @var12 int
    DECLARE @var13 int
    DECLARE @var14 bit

    DECLARE @conversationHandlerExists INT
    SELECT @conversationHandlerExists = COUNT(*) FROM sys.conversation_endpoints WHERE conversation_handle = 'ba118f68-81f6-ea11-885b-c67558a5f429';
    IF @conversationHandlerExists = 0
    BEGIN
        DECLARE @conversation_handle UNIQUEIDENTIFIER;
        DECLARE @schema_id INT;
        SELECT @schema_id = schema_id FROM sys.schemas WITH (NOLOCK) WHERE name = N'dbo';

        
        IF EXISTS (SELECT * FROM sys.triggers WITH (NOLOCK) WHERE object_id = OBJECT_ID(N'[dbo].[tr_dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde_Sender]')) DROP TRIGGER [dbo].[tr_dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde_Sender];

        
        IF EXISTS (SELECT * FROM sys.service_queues WITH (NOLOCK) WHERE schema_id = @schema_id AND name = N'dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde_Sender') EXEC (N'ALTER QUEUE [dbo].[dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde_Sender] WITH ACTIVATION (STATUS = OFF)');

        
        SELECT conversation_handle INTO #Conversations FROM sys.conversation_endpoints WITH (NOLOCK) WHERE far_service LIKE N'dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde_%' ORDER BY is_initiator ASC;
        DECLARE conversation_cursor CURSOR FAST_FORWARD FOR SELECT conversation_handle FROM #Conversations;
        OPEN conversation_cursor;
        FETCH NEXT FROM conversation_cursor INTO @conversation_handle;
        WHILE @@FETCH_STATUS = 0 
        BEGIN
            END CONVERSATION @conversation_handle WITH CLEANUP;
            FETCH NEXT FROM conversation_cursor INTO @conversation_handle;
        END
        CLOSE conversation_cursor;
        DEALLOCATE conversation_cursor;
        DROP TABLE #Conversations;

        
        IF EXISTS (SELECT * FROM sys.services WITH (NOLOCK) WHERE name = N'dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde_Receiver') DROP SERVICE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde_Receiver];
        
        IF EXISTS (SELECT * FROM sys.services WITH (NOLOCK) WHERE name = N'dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde_Sender') DROP SERVICE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde_Sender];

        
        IF EXISTS (SELECT * FROM sys.service_queues WITH (NOLOCK) WHERE schema_id = @schema_id AND name = N'dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde_Receiver') DROP QUEUE [dbo].[dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde_Receiver];
        
        IF EXISTS (SELECT * FROM sys.service_queues WITH (NOLOCK) WHERE schema_id = @schema_id AND name = N'dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde_Sender') DROP QUEUE [dbo].[dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde_Sender];

        
        IF EXISTS (SELECT * FROM sys.service_contracts WITH (NOLOCK) WHERE name = N'dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde') DROP CONTRACT [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde];
        
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/StartMessage/Insert') DROP MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/StartMessage/Insert];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/StartMessage/Update') DROP MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/StartMessage/Update];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/StartMessage/Delete') DROP MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/StartMessage/Delete];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID') DROP MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Code') DROP MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Code];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Name') DROP MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Name];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/IsActive') DROP MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/IsActive];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_Company') DROP MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_Company];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Comment') DROP MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Comment];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/DateCreated') DROP MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/DateCreated];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/DateModified') DROP MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/DateModified];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_CreatedBy') DROP MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_CreatedBy];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_LastModifiedBy') DROP MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_LastModifiedBy];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_User') DROP MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_User];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Recipient_ID_User') DROP MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Recipient_ID_User];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Sender_ID_User') DROP MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Sender_ID_User];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/IsOpened') DROP MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/IsOpened];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/EndMessage') DROP MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/EndMessage];

        
        IF EXISTS (SELECT * FROM sys.objects WITH (NOLOCK) WHERE schema_id = @schema_id AND name = N'dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde_QueueActivationSender') DROP PROCEDURE [dbo].[dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde_QueueActivationSender];
        RETURN
    END
    
    IF NOT EXISTS(SELECT 1 FROM INSERTED)
    BEGIN
        SET @dmlType = 'Delete'
        INSERT INTO @modifiedRecordsTable SELECT [ID], [Code], [Name], [IsActive], [ID_Company], [Comment], [DateCreated], [DateModified], [ID_CreatedBy], [ID_LastModifiedBy], [ID_User], [Recipient_ID_User], [Sender_ID_User], [IsOpened] FROM DELETED 
    END
    ELSE
    BEGIN
        IF NOT EXISTS(SELECT * FROM DELETED)
        BEGIN
            SET @dmlType = 'Insert'
            INSERT INTO @modifiedRecordsTable SELECT [ID], [Code], [Name], [IsActive], [ID_Company], [Comment], [DateCreated], [DateModified], [ID_CreatedBy], [ID_LastModifiedBy], [ID_User], [Recipient_ID_User], [Sender_ID_User], [IsOpened] FROM INSERTED 
        END
        ELSE
        BEGIN
            SET @dmlType = 'Update';
            INSERT INTO @deletedTable SELECT [ID],[Code],[Name],[IsActive],[ID_Company],[Comment],[DateCreated],[DateModified],[ID_CreatedBy],[ID_LastModifiedBy],[ID_User],[Recipient_ID_User],[Sender_ID_User],[IsOpened] FROM DELETED
            INSERT INTO @insertedTable SELECT [ID],[Code],[Name],[IsActive],[ID_Company],[Comment],[DateCreated],[DateModified],[ID_CreatedBy],[ID_LastModifiedBy],[ID_User],[Recipient_ID_User],[Sender_ID_User],[IsOpened] FROM INSERTED
            INSERT INTO @exceptTable SELECT [RowNumber],[ID],[Code],[Name],[IsActive],[ID_Company],[Comment],[DateCreated],[DateModified],[ID_CreatedBy],[ID_LastModifiedBy],[ID_User],[Recipient_ID_User],[Sender_ID_User],[IsOpened] FROM @insertedTable EXCEPT SELECT [RowNumber],[ID],[Code],[Name],[IsActive],[ID_Company],[Comment],[DateCreated],[DateModified],[ID_CreatedBy],[ID_LastModifiedBy],[ID_User],[Recipient_ID_User],[Sender_ID_User],[IsOpened] FROM @deletedTable

            INSERT INTO @modifiedRecordsTable SELECT [ID],[Code],[Name],[IsActive],[ID_Company],[Comment],[DateCreated],[DateModified],[ID_CreatedBy],[ID_LastModifiedBy],[ID_User],[Recipient_ID_User],[Sender_ID_User],[IsOpened] FROM @exceptTable e 
        END
    END

    SELECT @rowsToProcess = COUNT(1) FROM @modifiedRecordsTable    

    BEGIN TRY
        WHILE @rowsToProcess > 0
        BEGIN
            SELECT	@var1 = [ID], @var2 = [Code], @var3 = [Name], @var4 = [IsActive], @var5 = [ID_Company], @var6 = [Comment], @var7 = [DateCreated], @var8 = [DateModified], @var9 = [ID_CreatedBy], @var10 = [ID_LastModifiedBy], @var11 = [ID_User], @var12 = [Recipient_ID_User], @var13 = [Sender_ID_User], @var14 = [IsOpened]
            FROM	@modifiedRecordsTable
            WHERE	[RowNumber] = @rowsToProcess
                
            IF @dmlType = 'Insert' 
            BEGIN
                ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/StartMessage/Insert] (CONVERT(NVARCHAR, @dmlType))

                IF @var1 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID] (CONVERT(NVARCHAR(MAX), @var1))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID] (0x)
                END
                IF @var2 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Code] (CONVERT(NVARCHAR(MAX), @var2))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Code] (0x)
                END
                IF @var3 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Name] (CONVERT(NVARCHAR(MAX), @var3))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Name] (0x)
                END
                IF @var4 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/IsActive] (CONVERT(NVARCHAR(MAX), @var4))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/IsActive] (0x)
                END
                IF @var5 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_Company] (CONVERT(NVARCHAR(MAX), @var5))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_Company] (0x)
                END
                IF @var6 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Comment] (CONVERT(NVARCHAR(MAX), @var6))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Comment] (0x)
                END
                IF @var7 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/DateCreated] (CONVERT(NVARCHAR(MAX), @var7, 121))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/DateCreated] (0x)
                END
                IF @var8 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/DateModified] (CONVERT(NVARCHAR(MAX), @var8, 121))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/DateModified] (0x)
                END
                IF @var9 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_CreatedBy] (CONVERT(NVARCHAR(MAX), @var9))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_CreatedBy] (0x)
                END
                IF @var10 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_LastModifiedBy] (CONVERT(NVARCHAR(MAX), @var10))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_LastModifiedBy] (0x)
                END
                IF @var11 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_User] (CONVERT(NVARCHAR(MAX), @var11))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_User] (0x)
                END
                IF @var12 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Recipient_ID_User] (CONVERT(NVARCHAR(MAX), @var12))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Recipient_ID_User] (0x)
                END
                IF @var13 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Sender_ID_User] (CONVERT(NVARCHAR(MAX), @var13))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Sender_ID_User] (0x)
                END
                IF @var14 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/IsOpened] (CONVERT(NVARCHAR(MAX), @var14))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/IsOpened] (0x)
                END

                ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/EndMessage] (0x)
            END
        
            IF @dmlType = 'Update'
            BEGIN
                ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/StartMessage/Update] (CONVERT(NVARCHAR, @dmlType))

                IF @var1 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID] (CONVERT(NVARCHAR(MAX), @var1))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID] (0x)
                END
                IF @var2 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Code] (CONVERT(NVARCHAR(MAX), @var2))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Code] (0x)
                END
                IF @var3 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Name] (CONVERT(NVARCHAR(MAX), @var3))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Name] (0x)
                END
                IF @var4 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/IsActive] (CONVERT(NVARCHAR(MAX), @var4))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/IsActive] (0x)
                END
                IF @var5 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_Company] (CONVERT(NVARCHAR(MAX), @var5))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_Company] (0x)
                END
                IF @var6 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Comment] (CONVERT(NVARCHAR(MAX), @var6))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Comment] (0x)
                END
                IF @var7 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/DateCreated] (CONVERT(NVARCHAR(MAX), @var7, 121))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/DateCreated] (0x)
                END
                IF @var8 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/DateModified] (CONVERT(NVARCHAR(MAX), @var8, 121))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/DateModified] (0x)
                END
                IF @var9 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_CreatedBy] (CONVERT(NVARCHAR(MAX), @var9))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_CreatedBy] (0x)
                END
                IF @var10 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_LastModifiedBy] (CONVERT(NVARCHAR(MAX), @var10))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_LastModifiedBy] (0x)
                END
                IF @var11 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_User] (CONVERT(NVARCHAR(MAX), @var11))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_User] (0x)
                END
                IF @var12 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Recipient_ID_User] (CONVERT(NVARCHAR(MAX), @var12))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Recipient_ID_User] (0x)
                END
                IF @var13 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Sender_ID_User] (CONVERT(NVARCHAR(MAX), @var13))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Sender_ID_User] (0x)
                END
                IF @var14 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/IsOpened] (CONVERT(NVARCHAR(MAX), @var14))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/IsOpened] (0x)
                END

                ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/EndMessage] (0x)
            END

            IF @dmlType = 'Delete'
            BEGIN
                ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/StartMessage/Delete] (CONVERT(NVARCHAR, @dmlType))

                IF @var1 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID] (CONVERT(NVARCHAR(MAX), @var1))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID] (0x)
                END
                IF @var2 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Code] (CONVERT(NVARCHAR(MAX), @var2))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Code] (0x)
                END
                IF @var3 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Name] (CONVERT(NVARCHAR(MAX), @var3))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Name] (0x)
                END
                IF @var4 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/IsActive] (CONVERT(NVARCHAR(MAX), @var4))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/IsActive] (0x)
                END
                IF @var5 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_Company] (CONVERT(NVARCHAR(MAX), @var5))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_Company] (0x)
                END
                IF @var6 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Comment] (CONVERT(NVARCHAR(MAX), @var6))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Comment] (0x)
                END
                IF @var7 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/DateCreated] (CONVERT(NVARCHAR(MAX), @var7, 121))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/DateCreated] (0x)
                END
                IF @var8 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/DateModified] (CONVERT(NVARCHAR(MAX), @var8, 121))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/DateModified] (0x)
                END
                IF @var9 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_CreatedBy] (CONVERT(NVARCHAR(MAX), @var9))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_CreatedBy] (0x)
                END
                IF @var10 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_LastModifiedBy] (CONVERT(NVARCHAR(MAX), @var10))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_LastModifiedBy] (0x)
                END
                IF @var11 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_User] (CONVERT(NVARCHAR(MAX), @var11))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/ID_User] (0x)
                END
                IF @var12 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Recipient_ID_User] (CONVERT(NVARCHAR(MAX), @var12))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Recipient_ID_User] (0x)
                END
                IF @var13 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Sender_ID_User] (CONVERT(NVARCHAR(MAX), @var13))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/Sender_ID_User] (0x)
                END
                IF @var14 IS NOT NULL BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/IsOpened] (CONVERT(NVARCHAR(MAX), @var14))
                END
                ELSE BEGIN
                    ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/IsOpened] (0x)
                END

                ;SEND ON CONVERSATION 'ba118f68-81f6-ea11-885b-c67558a5f429' MESSAGE TYPE [dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde/EndMessage] (0x)
            END

            SET @rowsToProcess = @rowsToProcess - 1
        END
    END TRY
    BEGIN CATCH
        DECLARE @ErrorMessage NVARCHAR(4000)
        DECLARE @ErrorSeverity INT
        DECLARE @ErrorState INT

        SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()

        RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState) 
    END CATCH
END
GO
ALTER TABLE [dbo].[tMessage] ENABLE TRIGGER [tr_dbo_tMessage_8aa6b7f4-f2ce-44d8-9a71-2c222b01cbde_Sender]
GO
