﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fDocStatus_SalesOrder_Detail] ( @ID_SalesOrder INT )
RETURNS @tblDetail TABLE
    (
      ID_DocDetail INT ,
      DRQty DECIMAL(18, 2) ,
      Balance DECIMAL(18, 2) ,
      ID_DocStatus INT
    )
AS
    BEGIN
        INSERT  @tblDetail
                ( ID_DocDetail ,
                  DRQty ,
                  Balance ,
                  ID_DocStatus
                )
                SELECT  H.ID_DocDetail ,
                        H.DRQty ,
                        H.SOBalance ,
                        ( CASE WHEN H.SOQty = H.SOBalance
                                    AND H.DRQty = 0 THEN 1
                               WHEN H.SOBalance > 0
                                    AND H.DRQty > 0 THEN 2
                               WHEN H.SOBalance = 0 THEN 3
                          END ) AS ID_DocStatus
                FROM    ( SELECT    SD.ID AS ID_DocDetail ,
                                    SD.Quantity AS SOQty ,
                                    ISNULL(DD.Quantity, 0.00) AS DRQty ,
                                    SD.Quantity - ISNULL(DD.Quantity, 0.00) AS SOBalance
                          FROM      dbo.tSalesOrder_Detail SD --
                                    LEFT JOIN ( SELECT  SUM(DD.Quantity) Quantity ,
                                                        DD.ID_Doc ,
                                                        DD.ID_DocDetail ,
                                                        DD.ID_DeliveryReceipt
                                                FROM    tDeliveryReceipt_Detail DD
                                                        LEFT JOIN tDeliveryReceipt DR ON DD.ID_DeliveryReceipt = DR.ID
                                                WHERE   DR.ID_FilingStatus = 2 AND DD.ID_Doc = @ID_SalesOrder
                                                GROUP BY DD.ID_Doc ,
                                                        DD.ID_DocDetail ,
                                                        DD.ID_DeliveryReceipt
                                              ) DD ON DD.ID_DocDetail = SD.ID
                                                      AND DD.ID_Doc = SD.ID_SalesOrder
									--
                                    LEFT JOIN dbo.tSalesOrder SO ON SD.ID_SalesOrder = SO.ID
                          WHERE     SD.ID_SalesOrder = @ID_SalesOrder
                                    AND SO.ID_FilingStatus = 2
                        ) H
        RETURN
    END
GO
