﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


 CREATE VIEW [dbo].[_vGenerateTSClass]
 AS
SELECT Name + ' : ' +
       CASE
           WHEN system_type_id IN(36, 167)
           THEN 'string'
           WHEN system_type_id IN(104)
           THEN 'boolean'
           WHEN system_type_id IN(56,106)
           THEN 'number'
           WHEN system_type_id IN(61)
           THEN 'Date'
		   ELSE
		   'string'
       END + ';' AS TS,  
       Name, 
       system_type_id,
	   column_id,
	   OBJECT_NAME([Object_id]) AS TableName
FROM sys.all_columns

GROUP BY Name, system_type_id, [Object_id], column_id

GO
