﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vPatient_SOAP_Prescription_Count]
as
  select ID_Patient_SOAP,
         Count(*) TotalCount
  FROM   tPatient_SOAP_Prescription
  GROUP  BY ID_Patient_SOAP

GO
