﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TuktukanFinal](
	[Patient Number] [nvarchar](255) NULL,
	[Patient Name] [nvarchar](255) NULL,
	[Mobile Number] [nvarchar](255) NULL,
	[Contact Number] [nvarchar](255) NULL,
	[Email Address] [nvarchar](255) NULL,
	[Secondary Mobile] [nvarchar](255) NULL,
	[Gender] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[Locality] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[Pincode] [nvarchar](255) NULL,
	[National Id] [nvarchar](255) NULL,
	[Date of Birth] [nvarchar](255) NULL,
	[Age] [nvarchar](255) NULL,
	[Anniversary Date] [nvarchar](255) NULL,
	[Blood Group] [nvarchar](255) NULL,
	[Remarks] [nvarchar](255) NULL,
	[Medical History] [nvarchar](255) NULL,
	[Referred By] [nvarchar](255) NULL,
	[Groups] [nvarchar](255) NULL,
	[Patient Notes] [nvarchar](255) NULL,
	[ID] [nvarchar](255) NULL,
	[PatientNumber] [nvarchar](255) NULL
) ON [PRIMARY]
GO
