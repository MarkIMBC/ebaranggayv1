﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create 
 PROC [dbo].[pDischargePatient_Confinement] (@IDs_Patient_Confinement typIntList READONLY,
                                              @ID_UserSession          INT)
AS
  BEGIN
      DECLARE @Discharged_ID_FilingStatus INT = 15;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          exec [pDischargePatient_Confinement_validation]
            @IDs_Patient_Confinement,
            @ID_UserSession

          UPDATE dbo.tPatient_Confinement
          SET    ID_FilingStatus = @Discharged_ID_FilingStatus,
                 DateDischarge = GETDATE(),
                 ID_DischargeBy = @ID_User
          FROM   dbo.tPatient_Confinement bi
                 INNER JOIN @IDs_Patient_Confinement ids
                         ON bi.ID = ids.ID;

          exec pUpdatePatient_Confinemen_BillingStatus
            @IDs_Patient_Confinement
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO
