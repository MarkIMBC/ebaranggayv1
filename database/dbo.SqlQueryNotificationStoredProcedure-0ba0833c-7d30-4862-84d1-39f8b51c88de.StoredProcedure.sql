﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SqlQueryNotificationStoredProcedure-0ba0833c-7d30-4862-84d1-39f8b51c88de] AS BEGIN BEGIN TRANSACTION; RECEIVE TOP(0) conversation_handle FROM [SqlQueryNotificationService-0ba0833c-7d30-4862-84d1-39f8b51c88de]; IF (SELECT COUNT(*) FROM [SqlQueryNotificationService-0ba0833c-7d30-4862-84d1-39f8b51c88de] WHERE message_type_name = 'http://schemas.microsoft.com/SQL/ServiceBroker/DialogTimer') > 0 BEGIN if ((SELECT COUNT(*) FROM sys.services WHERE name = 'SqlQueryNotificationService-0ba0833c-7d30-4862-84d1-39f8b51c88de') > 0)   DROP SERVICE [SqlQueryNotificationService-0ba0833c-7d30-4862-84d1-39f8b51c88de]; if (OBJECT_ID('SqlQueryNotificationService-0ba0833c-7d30-4862-84d1-39f8b51c88de', 'SQ') IS NOT NULL)   DROP QUEUE [SqlQueryNotificationService-0ba0833c-7d30-4862-84d1-39f8b51c88de]; DROP PROCEDURE [SqlQueryNotificationStoredProcedure-0ba0833c-7d30-4862-84d1-39f8b51c88de]; END COMMIT TRANSACTION; END
GO
