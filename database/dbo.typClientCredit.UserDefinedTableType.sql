﻿CREATE TYPE [dbo].[typClientCredit] AS TABLE(
	[ID_Client] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[CreditAmount] [decimal](18, 4) NOT NULL,
	[Code] [varchar](250) NOT NULL,
	[Comment] [varchar](250) NOT NULL
)
GO
