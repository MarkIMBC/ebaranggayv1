﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create 
 Proc [dbo].[pChangeResidentHouseholdNumber](@ID_HouseholdNumber INT,
                                          @IDs_Resident       typIntList READONLY,
                                          @ID_UserSession     INT)
as
    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';
    DECLARE @ID_User INT = 0;

    SELECT @ID_User = ID_User
    FROM   dbo.tUserSession
    WHERE  ID = @ID_UserSession;

    Update tResident
    SET    ID_HouseholdNumber = @ID_HouseholdNumber
    WHERE  ID IN (SELECT ID
                  FROM   @IDs_Resident)

    INSERT INTO [dbo].[tResident_HouseholdNumberLog]
                ([IsActive],
                 [ID_Company],
                 [DateCreated],
                 [DateModified],
                 [ID_CreatedBy],
                 [ID_LastModifiedBy],
                 [ID_Resident],
                 [ID_HouseholdNumber],
                 [HouseholdNumberName])
    Select 1,
           resident.ID_Company,
           GETDATE(),
           GETDATE(),
           1,
           1,
           resident.ID,
           ID_HouseholdNumber,
           household.Name
    FROM   tResident resident
           inner join @IDs_Resident idsResident
                   on resident.ID = idsResident.ID
           LEFT join tHouseholdNumber household
                  on resident.ID_HouseholdNumber = household.ID

    SELECT '_';

    SELECT @Success Success,
           @message message;

GO
