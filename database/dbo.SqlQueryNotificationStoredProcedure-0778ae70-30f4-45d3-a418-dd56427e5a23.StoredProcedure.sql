﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SqlQueryNotificationStoredProcedure-0778ae70-30f4-45d3-a418-dd56427e5a23] AS BEGIN BEGIN TRANSACTION; RECEIVE TOP(0) conversation_handle FROM [SqlQueryNotificationService-0778ae70-30f4-45d3-a418-dd56427e5a23]; IF (SELECT COUNT(*) FROM [SqlQueryNotificationService-0778ae70-30f4-45d3-a418-dd56427e5a23] WHERE message_type_name = 'http://schemas.microsoft.com/SQL/ServiceBroker/DialogTimer') > 0 BEGIN if ((SELECT COUNT(*) FROM sys.services WHERE name = 'SqlQueryNotificationService-0778ae70-30f4-45d3-a418-dd56427e5a23') > 0)   DROP SERVICE [SqlQueryNotificationService-0778ae70-30f4-45d3-a418-dd56427e5a23]; if (OBJECT_ID('SqlQueryNotificationService-0778ae70-30f4-45d3-a418-dd56427e5a23', 'SQ') IS NOT NULL)   DROP QUEUE [SqlQueryNotificationService-0778ae70-30f4-45d3-a418-dd56427e5a23]; DROP PROCEDURE [SqlQueryNotificationStoredProcedure-0778ae70-30f4-45d3-a418-dd56427e5a23]; END COMMIT TRANSACTION; END
GO
