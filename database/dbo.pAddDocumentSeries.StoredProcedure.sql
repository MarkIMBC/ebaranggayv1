﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pAddDocumentSeries](@TableName_Model VARCHAR(MAX),
                              @Prefix          VARCHAR(MAX),
                              @Counter         INT = 1,
                              @DigitCount      INT = 5)
AS
  BEGIN
      DECLARE @ID_Model UNIQUEIDENTIFIER
      DECLARE @Name_Model VARCHAR(MAX) = ''
      DECLARE @ID_DocumentSeries INT = 0
      DECLARE @IDs_Company typIntList

      SELECT @Name_Model = Name,
             @ID_Model = Oid
      FROM   dbo._tModel
      WHERE  TableName = @TableName_Model

      if(SELECt COUNT(*)
         FROM   tDocumentSeries
         where  ID_Company = 1
                AND ID_Model = @ID_Model) = 0
        begin
            INSERT dbo.tDocumentSeries
                   (Name,
                    ID_Company,
                    ID_Model,
                    Counter,
                    Prefix,
                    DigitCount,
                    IsActive,
                    DateCreated,
                    DateModified,
                    ID_CreatedBy,
                    ID_LastModifiedBy)
            VALUES(@Name_Model,
                   1,
                   @ID_Model,
                   @Counter,
                   @Prefix,
                   @DigitCount,
                   1,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1)
        END

      SELECT @ID_DocumentSeries = ID
      FROM   tDocumentSeries
      where  ID_Company = 1
             and Name = @Name_Model

      Insert @IDs_Company
      SELECT ID
      FROm   tCompany
      except
      SELECT docSeries.ID_Company
      FROM   dbo.tDocumentSeries docSeries
      where  Name = @Name_Model

      INSERT dbo.tDocumentSeries
             (Code,
              Name,
              IsActive,
              Comment,
              DateCreated,
              DateModified,
              ID_CreatedBy,
              ID_LastModifiedBy,
              ID_Model,
              Counter,
              Prefix,
              IsAppendCurrentDate,
              DigitCount,
              ID_Company)
      SELECT docSeries.Code,
             docSeries.Name,
             docSeries.IsActive,
             docSeries.Comment,
             docSeries.DateCreated,
             docSeries.DateModified,
             docSeries.ID_CreatedBy,
             docSeries.ID_LastModifiedBy,
             docSeries.ID_Model,
             docSeries.Counter,
             docSeries.Prefix,
             docSeries.IsAppendCurrentDate,
             docSeries.DigitCount,
             company.ID
      FROM   dbo.tDocumentSeries docSeries,
             @IDs_Company company
      WHERE  docSeries.ID_Company = 1
             AND docSeries.ID = @ID_DocumentSeries
  END

GO
