﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE     
 PROC [dbo].[pInsertFamilyRelationshipType](@FamilyRelationshipType VARCHAR(MAX), @ID_Company INT = 1)  
as  
  BEGIN  
      IF(SELECT COUNT(*)  
         FROM   tFamilyRelationshipType  
         WHERE  Name = @FamilyRelationshipType) = 0  
        BEGIN  
            INSERT INTO [dbo].tFamilyRelationshipType  
                        ([Name],  
                         [IsActive], ID_Company)  
            VALUES      (@FamilyRelationshipType,  
                         1, @ID_Company)  
        END  
  END   
GO
