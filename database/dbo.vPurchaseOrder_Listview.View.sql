﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   VIEW [dbo].[vPurchaseOrder_Listview]
as

  SELECT ID,
         Date,
         Code,
		 ID_Company,
         GrossAmount,
         VatAmount,
         NetAmount,
         DateCreated,
         CreatedBy_Name_User,
         DateModified,
         LastModifiedBy_Name_User,
         DateApproved,
         ApprovedBy_Name_User,
         CanceledBy_Name_User,
         DateCanceled,
         ID_FilingStatus,
         Name_FilingStatus,
         ServingStatus_Name_FilingStatus,
         DateString,
         DateCreatedString,
         DateModifiedString,
         DateApprovedString,
         DateCanceledString,
		 Name_Supplier
  FROM   vPurchaseOrder
GO
