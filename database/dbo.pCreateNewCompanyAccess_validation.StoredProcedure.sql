﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pCreateNewCompanyAccess_validation]
(
    @CompanyName VARCHAR(MAX),
    @code VARCHAR(MAX)
)
AS
BEGIN

    DECLARE @Count_ValidateCompanyNameExist INT = 0;
	DECLARE @Count_ValidateCompanyCodeExist INT = 0;
    DECLARE @message VARCHAR(400) = '';

	/* Validate if Company Name has value.*/  
	IF(LEN(@CompanyName) = 0)
	BEGIN

		SET @message = 'Company Name is required.';
        THROW 50001, @message, 1;
	END

	/* Validate if code has value.*/  
	IF(LEN(@code) = 0)
	BEGIN

		SET @message = 'Company Code is required.';
        THROW 50001, @message, 1;
	END


	/* Validate if Company Name Exis*/  
    SELECT @Count_ValidateCompanyNameExist = COUNT(*)
    FROM dbo.tCompany
    WHERE Name = @CompanyName;

    IF @Count_ValidateCompanyNameExist > 0
    BEGIN

        SET @message = @CompanyName + ', company record is already exist.';
        THROW 50001, @message, 1;
    END;

	/* Validate if Company Code Exis*/  
    SELECT @Count_ValidateCompanyCodeExist = COUNT(*)
    FROM dbo.tCompany
    WHERE Code = @code;

    IF @Count_ValidateCompanyCodeExist > 0
    BEGIN

        SET @message = @CompanyName + ', code is already exist.';
        THROW 50001, @message, 1;
    END;


END;

GO
