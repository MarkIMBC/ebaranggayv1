﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vzResidentRequest_BarangayBusinessClearance]
AS
  SELECT bz.Name         NAME,
         bz.ID           ID,
         bz.BusinessName BusinessName,
         bz.RegistrationNumber,
         company.ID      CompanyID,
         company.Name    CompanyName
  FROM   vResidentRequest_BarangayBusinessClearance bz
         INNER JOIN tCompany company
                 ON company.ID = bz.ID_Company

GO
