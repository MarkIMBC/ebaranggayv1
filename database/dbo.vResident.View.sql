﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 VIEW [dbo].[vResident]
AS
  SELECT H.*,
         UC.Name                  AS CreatedBy,
         UM.Name                  AS LastModifiedBy,
         gender.Name              Name_Gender,
         company.Name             Name_Company,
         civilStatus.Name         Name_CivilStatus,
         educationalLevel.Name    Name_EducationalLevel,
         homeOwnershipStatus.Name Name_HomeOwnershipStatus,
         occupationalStatus.Name  Name_OccupationalStatus,
         householdNumber.Name     Name_HouseholdNumber,
         AssignedBHW.Name         AssignedBHW_Name_Employee,
         religion.Name            Name_Religion,
         AssignedBNS.Name         AssignedBNS_Name_Employee,
         AssignedBNS.Name         AssignedBBNS_Name_Employee
  FROM   tResident H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tGender gender
                ON gender.ID = h.ID_Gender
         LEFT JOIN tCivilStatus civilStatus
                ON civilStatus.ID = h.ID_CivilStatus
         LEFT JOIN tEducationalLevel educationalLevel
                ON educationalLevel.ID = h.ID_EducationalLevel
         LEFT JOIN tHomeOwnershipStatus homeOwnershipStatus
                ON homeOwnershipStatus.ID = h.ID_HomeOwnershipStatus
         LEFT JOIN tOccupationalStatus occupationalStatus
                ON occupationalStatus.ID = h.ID_OccupationalStatus
         LEFT JOIN tReligion religion
                ON religion.ID = h.ID_Religion
         LEFT JOIN tCompany company
                ON company.ID = h.ID_Company
         LEFT JOIN tEmployee AssignedBHW
                ON AssignedBHW.ID = h.AssignedBHW_ID_Employee
         LEFT JOIN tEmployee AssignedBNS
                ON AssignedBNS.ID = h.AssignedBNS_ID_Employee
         LEFT JOIN vHouseholdNumber householdNumber
                on H.ID_HouseholdNumber = householdNumber.ID

GO
