﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vResidentRequest_BarangayBusinessClearance]
AS
  SELECT H.*,
         ResidentRequest.Name          Name_ResidentRequest,
         ResidentRequest.ContactNumber ContactNumber_ResidentRequest,
         UC.NAME                       AS CreatedBy,
         UM.NAME                       AS LastModifiedBy
  FROM   tResidentRequest_BarangayBusinessClearance H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tResidentRequest ResidentRequest
                ON ResidentRequest.ID = h.ID_ResidentRequest

GO
