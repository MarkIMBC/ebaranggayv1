﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  
 VIEW [dbo].[vProgram_Listview]
as
  SELECT hed.ID,
         hed.Code,
         hed.Name,
         hed.DateCreated,
         hed.DateModified,
         hed.ID_Company,
         hed.IsActive
  FROM   vProgram hed

GO
