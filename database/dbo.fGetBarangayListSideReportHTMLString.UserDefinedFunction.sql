﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 FUNCTION [dbo].[fGetBarangayListSideReportHTMLString]()
RETURNS VARCHAR(MAX)
AS
  BEGIN
      DECLARE @result VARCHAR(MAX) = ''

      /***********************************************/
      SET @result = @result + '<b>_____________________</b>'
                    + '<br/>'
      SET @result = @result + '<b>Punong Barangay</b>' + '<br/>'
                    + '<br/>'
      /***********************************************/
      SET @result = @result + '<b>_____________________</b>'
                    + '<br/>'
      SET @result = @result + '<b>Position</b>' + '<br/>'
      SET @result = @result + 'Department' + '<br/>' + '<br/>'

      /***********************************************/
      RETURN @result
  END

GO
