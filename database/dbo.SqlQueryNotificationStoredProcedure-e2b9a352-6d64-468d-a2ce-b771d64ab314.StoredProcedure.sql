﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SqlQueryNotificationStoredProcedure-e2b9a352-6d64-468d-a2ce-b771d64ab314] AS BEGIN BEGIN TRANSACTION; RECEIVE TOP(0) conversation_handle FROM [SqlQueryNotificationService-e2b9a352-6d64-468d-a2ce-b771d64ab314]; IF (SELECT COUNT(*) FROM [SqlQueryNotificationService-e2b9a352-6d64-468d-a2ce-b771d64ab314] WHERE message_type_name = 'http://schemas.microsoft.com/SQL/ServiceBroker/DialogTimer') > 0 BEGIN if ((SELECT COUNT(*) FROM sys.services WHERE name = 'SqlQueryNotificationService-e2b9a352-6d64-468d-a2ce-b771d64ab314') > 0)   DROP SERVICE [SqlQueryNotificationService-e2b9a352-6d64-468d-a2ce-b771d64ab314]; if (OBJECT_ID('SqlQueryNotificationService-e2b9a352-6d64-468d-a2ce-b771d64ab314', 'SQ') IS NOT NULL)   DROP QUEUE [SqlQueryNotificationService-e2b9a352-6d64-468d-a2ce-b771d64ab314]; DROP PROCEDURE [SqlQueryNotificationStoredProcedure-e2b9a352-6d64-468d-a2ce-b771d64ab314]; END COMMIT TRANSACTION; END
GO
