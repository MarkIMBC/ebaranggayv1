﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pPurchaseOrder_Approve] @ID INT
AS
BEGIN
  
  UPDATE tPurchaseOrder SET ID_FilingStatus = 3, DateApproved = GETDATE(), ID_ApprovedBy = 1 WHERE ID = @ID

END

GO
