﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE 
 FUNCTION [dbo].[fGetTextBlastMessage] (@TextBlastMessageTemplate VARCHAR(MAX),
                                             @Client                   VARCHAR(MAX))
RETURNS VARCHAR(MAX)
  BEGIN
      Declare @message Varchar(MAX) = @TextBlastMessageTemplate

      SET @message = REPLACE(@message, '/*Client*/', LTRIM(RTRIM(@Client)))

      RETURN @message
  END


GO
