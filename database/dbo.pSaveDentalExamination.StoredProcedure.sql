﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pSaveDentalExamination] (@data             TYPPATIENDENTALEXAMTOOTH READONLY,
                                       @images           TYPPATIENDENTALEXAMIMAGE READONLY,
                                       @medicalhistories TYPPATIENDENTALEXAMMEDHISTORY READONLY)
AS
    BEGIN TRANSACTION;

    BEGIN TRY
        DECLARE @ID_Model_Patient_DentalExamination UNIQUEIDENTIFIER;
        DECLARE @parentRecord TABLE
          (
             ID_Patient_DentalExamination INT NOT NULL,
             GUID                         VARCHAR(MAX) NULL
          );
        DECLARE @details TABLE
          (
             ID_Patient_DentalExamination       INT,
             ID_Patient_DentalExamination_Tooth INT,
             GUID                               VARCHAR(50)
          );
        DECLARE @record TYPPATIENDENTALEXAMTOOTH;

        SELECT @ID_Model_Patient_DentalExamination = Oid
        FROM   dbo._tModel
        WHERE  TableName = 'tPatient_DentalExamination';

        INSERT @record
               (ID_Patient_DentalExamination,
                Date,
                ID_Patient,
                ID_Doctor,
                ID_Patient_DentalExamination_ToothInfo,
                ID_Tooth,
                IDs_ToothSurface,
                ID_ToothStatus,
                Comment,
                GUID_Patient_DentalExamination,
                GUID_Patient_DentalExamination_ToothInfo,
                IsDelete,
                ID_Dentition,
                Prescription)
        SELECT ID_Patient_DentalExamination,
               Date,
               ID_Patient,
               ID_Doctor,
               ID_Patient_DentalExamination_ToothInfo,
               ID_Tooth,
               IDs_ToothSurface,
               ID_ToothStatus,
               Comment,
               GUID_Patient_DentalExamination,
               GUID_Patient_DentalExamination_ToothInfo,
               IsDelete,
               ID_Dentition,
               Prescription
        FROM   @data;

        --------------- ID Linking do not change (Start)--------------------          
        --- Update ID_Patient_DentalExamination ----------          
        INSERT @parentRecord
               (ID_Patient_DentalExamination,
                GUID)
        SELECT ID_Patient_DentalExamination,
               NewId()
        FROM   @record
        WHERE  IsNull(ID_Patient_DentalExamination, 0) < 0
        GROUP  BY ID_Patient_DentalExamination;

        UPDATE @record
        SET    GUID_Patient_DentalExamination = pRecord.GUID
        FROM   @record record
               LEFT JOIN @parentRecord pRecord
                      ON pRecord.ID_Patient_DentalExamination = record.ID_Patient_DentalExamination;

        INSERT INTO dbo.tPatient_DentalExamination
                    (IsActive,
                     GUID,
                     Code)
        SELECT 1,
               GUID_Patient_DentalExamination,
               dbo.fGenerateDocumentSeries(@ID_Model_Patient_DentalExamination, patient.ID_Company, Row_Number()
                                                                                  OVER (
                                                                                    ORDER BY GUID_Patient_DentalExamination), NULL)
        FROM   @record record left Join tPatient patient on patient.ID = record.ID_Patient 
        WHERE  IsNull(ID_Patient_DentalExamination, 0) < 1
        GROUP  BY record.ID_Patient_DentalExamination,
                  record.Date,
                  record.ID_Patient,
                  record.ID_Doctor,
				  patient.ID_Company ,
                  GUID_Patient_DentalExamination;

        DECLARE @totalInsertRecord INT = 0;

        SELECT DISTINCT @totalInsertRecord = Count(*)
        FROM   @parentRecord;

        EXEC dbo.PupDateDocumentSeries
          @ID_Model = @ID_Model_Patient_DentalExamination,
          @plusCount = @totalInsertRecord;

        UPDATE @record
        SET    ID_Patient_DentalExamination = pDenExam.ID,
               Comment = record.Prescription
        FROM   @record record
               INNER JOIN dbo.tPatient_DentalExamination pDenExam
                       ON record.GUID_Patient_DentalExamination = pDenExam.GUID;

        --- Update ID_Patient_DentalExamination_ToothInfo ----------          
        INSERT @details
               (ID_Patient_DentalExamination,
                ID_Patient_DentalExamination_Tooth,
                GUID)
        SELECT DISTINCT ID_Patient_DentalExamination,
                        ID_Patient_DentalExamination_ToothInfo,
                        NewId()
        FROM   @record
        WHERE  ID_Patient_DentalExamination_ToothInfo < 1;

        INSERT dbo.tPatient_DentalExamination_ToothInfo
               (IsActive,
                GUID)
        SELECT 1,
               GUID
        FROM   @details;

        UPDATE @record
        SET    GUID_Patient_DentalExamination_ToothInfo = detail.GUID
        FROM   @record record
               LEFT JOIN @details detail
                      ON detail.ID_Patient_DentalExamination_Tooth = record.ID_Patient_DentalExamination_ToothInfo;

        UPDATE @record
        SET    ID_Patient_DentalExamination_ToothInfo = det.ID
        FROM   @record record
               INNER JOIN tPatient_DentalExamination_ToothInfo det
                       ON det.GUID = record.GUID_Patient_DentalExamination_ToothInfo;

        --------------- ID Linking do not change (END)--------------------        
        ---- Update Patient DentalExamination -----          
        UPDATE dbo.tPatient_DentalExamination
        SET    Date = record.Date,
               ID_Patient = record.ID_Patient,
               ID_Doctor = record.ID_Doctor,
               ID_Dentition = record.ID_Dentition,
               Comment = record.Prescription,
               ID_FilingStatus = 1
        FROM   dbo.tPatient_DentalExamination hed
               INNER JOIN @record record
                       ON hed.ID = record.ID_Patient_DentalExamination
        WHERE  IsNull(IsDelete, 0) = 0;

        ---- Update Patient DentalExamination Tooth Info -----          
        UPDATE dbo.tPatient_DentalExamination_ToothInfo
        SET    ID_Patient_DentalExamination = record.ID_Patient_DentalExamination,
               ID_Tooth = record.ID_Tooth,
               IDs_ToothSurface = record.IDs_ToothSurface,
               ID_ToothStatus = record.ID_ToothStatus,
               Comment = record.Comment
        FROM   dbo.tPatient_DentalExamination_ToothInfo det
               INNER JOIN @record record
                       ON det.ID = record.ID_Patient_DentalExamination_ToothInfo
        WHERE  IsNull(IsDelete, 0) = 0;

        DELETE tPatient_DentalExamination_ToothInfo
        WHERE  ID IN (SELECT ID_Patient_DentalExamination_ToothInfo
                      FROM   @record
                      WHERE  IsDelete = 1);

        ----------------------- Saving Image Info --------------------------------  
        UPDATE tPatient_DentalExamination_Image
        SET    Comment = images.Comment
        FROM   tPatient_DentalExamination_Image rec
               INNER JOIN @images images
                       ON rec.ID = images.ID;

        --------------------- Saving Medical History --------------------  
        INSERT dbo.tPatient_DentalExamination_MedicalHistory
               (Code,
                Name,
                IsActive,
                ID_Company,
                Comment,
                DateCreated,
                DateModified,
                ID_CreatedBy,
                ID_LastModifiedBy,
                ID_Patient_DentalExamination,
                ID_MedicalHistoryQuestionnaire,
                Answer)
        SELECT '',
               '',
               1,
               1,
               Comment,
               GetDate(),
               GetDate(),
               1,
               1,
               ID_Patient_DentalExamination,
               ID_MedicalHistoryQuestionnaire,
               Answer
        FROM   @medicalhistories
        WHERE  IsNull(ID, 0) < 1;

        UPDATE dbo.tPatient_DentalExamination_MedicalHistory
        SET    Answer = record.Answer
        FROM   dbo.tPatient_DentalExamination_MedicalHistory pMedHistory
               INNER JOIN @medicalhistories record
                       ON record.ID = pMedHistory.ID
        WHERE  IsNull(record.ID, 0) > 0;

        ------------------ Query Header -------------------------  
        SELECT '_',
               '' records;

        SELECT CONVERT(BIT, 1) Success;

        SELECT ID,
               Code,
               DateString,
               Doctor,
               Patient,
               ID_Doctor,
               ID_Patient,
               ID_Dentition,
               Date
        FROM   dbo.vPatient_DentalExamination hed
        WHERE  hed.ID IN (SELECT DISTINCT ID_Patient_DentalExamination
                          FROM   @record);

        COMMIT TRANSACTION;
    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION;
        SELECT '_',
               '' records;
        SELECT CONVERT(BIT, 0) Success;
    END CATCH; 
GO
