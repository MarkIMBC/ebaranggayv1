﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SqlQueryNotificationStoredProcedure-56af2d2e-fe8b-4fdd-841a-c012853d31e2] AS BEGIN BEGIN TRANSACTION; RECEIVE TOP(0) conversation_handle FROM [SqlQueryNotificationService-56af2d2e-fe8b-4fdd-841a-c012853d31e2]; IF (SELECT COUNT(*) FROM [SqlQueryNotificationService-56af2d2e-fe8b-4fdd-841a-c012853d31e2] WHERE message_type_name = 'http://schemas.microsoft.com/SQL/ServiceBroker/DialogTimer') > 0 BEGIN if ((SELECT COUNT(*) FROM sys.services WHERE name = 'SqlQueryNotificationService-56af2d2e-fe8b-4fdd-841a-c012853d31e2') > 0)   DROP SERVICE [SqlQueryNotificationService-56af2d2e-fe8b-4fdd-841a-c012853d31e2]; if (OBJECT_ID('SqlQueryNotificationService-56af2d2e-fe8b-4fdd-841a-c012853d31e2', 'SQ') IS NOT NULL)   DROP QUEUE [SqlQueryNotificationService-56af2d2e-fe8b-4fdd-841a-c012853d31e2]; DROP PROCEDURE [SqlQueryNotificationStoredProcedure-56af2d2e-fe8b-4fdd-841a-c012853d31e2]; END COMMIT TRANSACTION; END
GO
