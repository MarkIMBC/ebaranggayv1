﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE    
 VIEW [dbo].[vCertificateOfIndigency]
AS
  SELECT H.*,
         UC.Name          AS CreatedBy,
         UM.Name          AS LastModifiedBy,
         gender.Name      Name_Gender,
         civilStatus.Name Name_CivilStatus
  FROM   tCertificateOfIndigency H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tGender gender
                ON H.ID_Gender = gender.ID
         LEFT JOIN tCivilStatus civilStatus
                ON H.ID_CivilStatus = civilStatus.ID

GO
