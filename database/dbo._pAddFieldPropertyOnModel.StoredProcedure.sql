﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[_pAddFieldPropertyOnModel] @Oid_Model UNIQUEIDENTIFIER, @Name VARCHAR(100), @ID_PropertyType INT, @ID_ControlType INT, @ID_ReferenceModel UNIQUEIDENTIFIER
  AS
BEGIN
    DECLARE @TableName VARCHAR(100) = NULL
    SELECT @TableName = TableName FROM _tModel WHERE Oid = @Oid_Model
  
     IF ( @TableName IS NULL )
        RETURN

    IF EXISTS( SELECT * FROM _tModel_Property WHERE TRIM(UPPER(Name)) = TRIM(UPPER(@Name)) AND ID_Model = @Oid_Model )
        RETURN

    EXEC _pAddModelProperty @TableName, @Name, @ID_PropertyType

    IF @ID_ReferenceModel IS NOT NULL
      BEGIN
        DECLARE @RefTable VARCHAR(100)
        SELECT @RefTable = TableName FROM _tModel WHERE Oid = @ID_ReferenceModel
        IF @RefTable IS NOT NULL 
          BEGIN
            EXEC _pCreateForeignKey @TableName, @Name, @RefTable, 'ID'
          END
      END

    --IF (@ID_ControlType IS NOT NULL)
    --  BEGIN
    --    UPDATE DET SET ID_ControlType = @ID_ControlType FROM _tDetailView_Detail DET 
    --      INNER JOIN _tDetailView DV ON DET.ID_DetailView = DV.Oid
    --      INNER JOIN _tModel MO ON DV.ID_MoSdel = MO.Oid
    --      INNER JOIN _tModel_Property MP ON MO.Oid = MP.ID_Model
    --      WHERE MP.Name = @Name AND MO.Oid = @Oid_Model  
    --  END  
END
GO
