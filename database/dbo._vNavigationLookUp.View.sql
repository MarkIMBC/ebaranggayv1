﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_vNavigationLookUp]
AS
SELECT Oid ID,
       Oid,
       Name
FROM _tNavigation;
GO
