﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tBarangayClearance](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[Date] [datetime] NULL,
	[Age] [varchar](300) NULL,
	[ID_Gender] [int] NULL,
	[ID_CivilStatus] [int] NULL,
	[Address] [varchar](300) NULL,
	[Purpose] [varchar](300) NULL,
	[ImageProfilePicFilename] [varchar](5000) NULL,
	[Requestor] [varchar](300) NULL,
	[Citizenship] [varchar](300) NULL,
 CONSTRAINT [PK_tBarangayClearance] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tBarangayClearance] ON [dbo].[tBarangayClearance]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tBarangayClearance] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tBarangayClearance]  WITH CHECK ADD  CONSTRAINT [FK_tBarangayClearance_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tBarangayClearance] CHECK CONSTRAINT [FK_tBarangayClearance_ID_Company]
GO
ALTER TABLE [dbo].[tBarangayClearance]  WITH CHECK ADD  CONSTRAINT [FK_tBarangayClearance_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tBarangayClearance] CHECK CONSTRAINT [FK_tBarangayClearance_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tBarangayClearance]  WITH CHECK ADD  CONSTRAINT [FK_tBarangayClearance_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tBarangayClearance] CHECK CONSTRAINT [FK_tBarangayClearance_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE TRIGGER [dbo].[rDateCreated_tBarangayClearance] ON [dbo].[tBarangayClearance] FOR INSERT
			AS
				DECLARE @ID INT
				SELECT @ID = ID FROM Inserted
				UPDATE dbo.tBarangayClearance SET DateCreated = GETDATE() WHERE ID = @ID
		
GO
ALTER TABLE [dbo].[tBarangayClearance] ENABLE TRIGGER [rDateCreated_tBarangayClearance]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE TRIGGER [dbo].[rDateModified_tBarangayClearance] ON [dbo].[tBarangayClearance] FOR UPDATE, INSERT
			AS
				DECLARE @ID INT
				SELECT @ID = ID FROM Inserted
				UPDATE dbo.tBarangayClearance SET DateModified = GETDATE() WHERE ID = @ID
		
GO
ALTER TABLE [dbo].[tBarangayClearance] ENABLE TRIGGER [rDateModified_tBarangayClearance]
GO
