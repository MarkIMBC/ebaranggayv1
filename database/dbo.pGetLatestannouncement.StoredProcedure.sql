﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pGetLatestannouncement](@ID_UserSession INT,
                                  @PageNumber     INT = 1,
                                  @PageSize       INT = 100)
AS
  BEGIN
      DECLARE @ID_User      INT,
              @ID_Warehouse INT
      DECLARE @ID_Company INT
      DEClare @Success BIT = 1
      DECLARE @RecordCount INT = 0
      DECLARE @DisplayCount INT = 0
      DECLARE @TotalPageCount INT = 0

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_UserSession

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      DECLARE @table table
        (
           ID               INT,
           Name             VARCHAR(MAX),
           DateStart        DateTime,
           DateEnd          DateTime,
           LastUpdateString VARCHAR(MAX)
        )

      SELECT @RecordCount = Count(*)
      FROm   vAnnouncement
      WHERE  ID_Company = @ID_Company
             AND CONVERT(Date, GETDATE()) BETWEEN CONVERT(Date, DateStart) AND Convert(Date, DateEnd)
             AND IsActive = 1

      SET @TotalPageCount = ( @RecordCount / @PageSize ) + 1
      SET @TotalPageCount = CASE
                              WHEN @RecordCount = 0 THEN 0
                              ELSE @TotalPageCount
                            END

      INSERT INTO @table
                  (ID,
                   Name,
                   DateStart,
                   DateEnd,
                   LastUpdateString)
      SELECT ID,
             Name,
             DateStart,
             DateEnd,
             dbo.GetDateFormatSince(DateModified) LastUpdateString
      FROm   vAnnouncement
      WHERE  ID_Company = @ID_Company
             AND CONVERT(Date, GETDATE()) BETWEEN CONVERT(Date, DateStart) AND Convert(Date, DateEnd)
             AND IsActive = 1
      ORDER  BY DATEDIFF(ss, DateStart, Getdate()) ASC
      OFFSET @PageSize * (@PageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY
      OPTION (RECOMPILE);

      SELECT @DisplayCount = COUNT(*)
      FROM   @table

      SELECT '_',
             '' Records

      SELECT @Success        Success,
             @RecordCount    TotalRecordCount,
             @DisplayCount   DisplayRecordCount,
             @PageNumber     CurrentPageNumber,
             @TotalPageCount TotalPageCount,
             2000            MilliSecondsDelay

      SELECT *
      FROM   @table
  END 
GO
