﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pObjectAuditTrail]
    @Oid_Model VARCHAR(200),
    @ID_CurrentObject VARCHAR(100) = 1 ,
    @ModelPropertLink VARCHAR(100) = NULL
AS
    DECLARE @TableName VARCHAR(MAX) = '' ,
        @ID_PrimaryKey VARCHAR(100)

    SELECT  @TableName = TableName ,
            @ID_PrimaryKey = PrimaryKey
    FROM    dbo.[_vModel]
    WHERE   Oid = @Oid_Model

    IF ( @ModelPropertLink IS NULL )
        SET @ModelPropertLink = @ID_PrimaryKey

    DECLARE @Columns VARCHAR(MAX)
    DECLARE @Values VARCHAR(MAX)

    SELECT  @Columns = COALESCE(@Columns + ' , ', '') + '[' + Name + ']' ,
            @Values = ( CASE ID_PropertyType
                          WHEN 1
                          THEN COALESCE(@Values + ' , ', '') + 'CAST(ISNULL('
                               + Name
                               + ',NULL) COLLATE DATABASE_DEFAULT AS VARCHAR(MAX)) AS '
                               + Name
                          ELSE COALESCE(@Values + ' , ', '') + 'CAST(ISNULL('
                               + Name + ',NULL) AS VARCHAR(MAX)) AS ' + Name
                        END )
    FROM    dbo.[_tModel_Property]
    WHERE   --ISNULL(IsAllowInsert, 1) = 1
            Name <> 'ID' --AND IsReadOnly <> 1
            AND ID_PropertyType <> 10
            AND ID_Model = @Oid_Model
            AND IsActive = 1

    DECLARE @SQL NVARCHAR(MAX) = 'SELECT ID, '''
        + CAST(@Oid_Model AS VARCHAR(100))
        + ''', [Columns], [Values] FROM (SELECT ID,' + @Values + ' FROM '
        + @TableName + ' WHERE ' + @ModelPropertLink + ' = '
        + CAST(@ID_CurrentObject AS VARCHAR(100))
        + ') X UNPIVOT ( [Values] FOR [Columns]	 IN (' + @Columns + ')) U'

    EXEC sp_executesql @result = @SQL
GO
