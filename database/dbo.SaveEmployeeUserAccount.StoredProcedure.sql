﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE 
 PROC [dbo].[SaveEmployeeUserAccount] @CompanyID  INT,
                                           @EmployeeID INT = -1,
                                           @Username   VARCHAR(300),
                                           @Password   VARCHAR(300)
AS
  BEGIN
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @Name_Company VARCHAR(300) = '';
      DECLARE @Username_User VARCHAR(300) = '';

      SELECT @Name_Company = Name
      FROM   tCompany
      WHERE  ID = @CompanyID

      SELECT @Username_User = USername
      FROM   tUser
      WHERE  ID_Employee = @EmployeeID

      SET @Username = ISNULL(@Username, '')
      SET @Password = ISNULL(@Password, '')

      BEGIN TRY
          DECLARE @Count_EmployeeCompany INT =0
          DECLARE @Count_UserNameExist INT =0

          SELECT @Count_EmployeeCompany = COUNT(*)
          FROM   tEmployee
          WHERE  ID_Company = @CompanyID
                 AND ID = @EmployeeID

          if( @Count_EmployeeCompany = 0 )
            BEGIN
                SET @message = 'User is not included in ' + @Name_Company;

                THROW 50001, @message, 1;
            END

          SELECT @Count_UserNameExist = COUNT(*)
          FROM   vUser
          WHERE  LOWER(Username) = LOWER(@Username)
                 AND Username NOT IN ( @Username_User )

          if( @Count_UserNameExist > 0 )
            BEGIN
                SET @message = 'Username is already exist.';

                THROW 50001, @message, 1;
            END
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      if( @Success = 1 )
        BEGIN
            UPDATE tUser
            SET    Username = @Username,
                   Password = @Password
            WHERE  ID_Employee = @EmployeeID
        END

      SELECT '_'

      SELECT @Success Success,
             @message message;
  END

GO
