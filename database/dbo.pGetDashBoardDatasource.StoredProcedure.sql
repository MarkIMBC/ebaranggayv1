﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGetDashboardDataSource] (@ID_UserSession INT)
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_UserSession;

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      DECLARE @ClientDailyCount INT = 0
      DECLARE @ClientMonthlyCount INT = 0
      DECLARE @SalesDailyAmount DECIMAL(18, 2) = 0
      DECLARE @SalesMonthlyAmount DECIMAL(18, 2) = 0
	  
	  /* @@ClientDailyCount */
	  SELECT @ClientDailyCount =  COUNT(*)
      FROM   tClient
      WHERE  ID_Company = @ID_Company
             AND IsActive = 1
             AND Cast(DateCreated AS DATE) = Cast(GetDate() AS DATE)
      
	  /* @@ClientMonthlyCount */
      SELECT @ClientMonthlyCount = COUNT(*)
      FROM   tClient
      WHERE  ID_Company = @ID_Company
             AND IsActive = 1
             AND Cast(DateCreated AS DATE) BETWEEN Cast(DateAdd(month, DatedIff(month, 0, GetDate()), 0) AS DATE) AND Cast(eoMonth(GetDate()) AS DATE)
      
	  /* @@SalesDailyAmount */
      SELECT @SalesDailyAmount = SUM(IsNull(TotalAmount, 0))
      FROM   tBillingInvoice
      WHERE  ID_Company = @ID_Company
             AND ID_FilingStatus = 3
             AND Cast(Date AS DATE) = Cast(GetDate() AS DATE)

	  /* @@SalesMonthlyAmount */
      SELECT @SalesMonthlyAmount = SUM(IsNull(TotalAmount, 0))
      FROM   tBillingInvoice
      WHERE  ID_Company = @ID_Company
             AND ID_FilingStatus = 3
             AND Cast(Date AS DATE) BETWEEN Cast(DateAdd(month, DatedIff(month, 0, GetDate()), 0) AS DATE) AND Cast(eoMonth(GetDate()) AS DATE)

      SELECT '_'

      SELECT @ClientDailyCount   ClientDailyCount,
             @ClientMonthlyCount ClientMonthlyCount,
             @SalesDailyAmount   SalesDailyAmount,
             @SalesMonthlyAmount SalesMonthlyAmount,
			 FORMAT(@ClientDailyCount  , '#,#0') FormattedClientDailyCount,
             FORMAT(@ClientMonthlyCount, '#,#0') FormattedClientMonthlyCount,
             FORMAT(@SalesDailyAmount  , '#,#0.00') FormattedSalesDailyAmount,
             FORMAT(@SalesMonthlyAmount, '#,#0.00') FormattedSalesMonthlyAmount

  END

GO
