﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fGetToothLocationSurfaceString](@Location VARCHAR(MAX), @IDs_ToothSurface VARCHAR(MAX))
RETURNS VARCHAR(MAX)
AS 
BEGIN
	
	DECLARE @text VARCHAR(MAX) = ''

	SELECT @text = @text + @Location + '_' + Part + ','  FROM dbo.fGetSplitString(@IDs_ToothSurface, ',')

	IF LEN(@text) > 0
		SET @text = LEFT(@text,LEN(@text) - 1)

    RETURN @text
END;

GO
