﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fGetPDentalExamToothLocationSurfaceString](@ID_Patient_DentalExamination INT)
RETURNS VARCHAR(MAX)
AS 
BEGIN

	DECLARE @text VARCHAR(MAX) = ''
	DECLARE @toothInfo TABLE (Location VARCHAR(MAX), IDs_ToothSurface VARCHAR(MAX))

	INSERT @toothInfo
	(
		Location, 
		IDs_ToothSurface
	)
	SELECT 
		Location_Tooth, 
		IDs_ToothSurface
	FROM dbo.vPatient_DentalExamination_ToothInfo
	WHERE 
		ID_Patient_DentalExamination = @ID_Patient_DentalExamination

	SELECT
		@text = @text + dbo.fGetToothLocationSurfaceString(Location, IDs_ToothSurface) + ','  
	FROM @toothInfo

	IF LEN(@text) > 0
		SET @text = LEFT(@text,LEN(@text) - 1)

    RETURN @text
END;

GO
