﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGetForSendTextBlastMessage]
as
  BEGIN
      DECLARE @DateSending DateTime = GETDATE()

      exec pGetSendTextBlastMessage
        @DateSending
  END

GO
