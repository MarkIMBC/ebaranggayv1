﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  
 VIEW [dbo].[vResident_Business_ListvIew]
AS
  SELECT ID,
         Name,
         IsActive,
         ID_Company,
         Resident_BusinessOwner Name_Owner,
         RequestedBy,
         Address,
         CreatedBy,
         LastModifiedBy
  FROM   vResident_Business
  WHERE  IsActive = 1

GO
