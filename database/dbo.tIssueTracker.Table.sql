﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tIssueTracker](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[Issue] [varchar](300) NULL,
	[DeveloperSide] [varchar](300) NULL,
	[Solution] [varchar](300) NULL,
	[ID_FilingStatus] [int] NULL,
	[ID_ApprovedBy] [int] NULL,
	[DateApproved] [datetime] NULL,
 CONSTRAINT [PK_tIssueTracker] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tIssueTracker] ON [dbo].[tIssueTracker]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tIssueTracker] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tIssueTracker]  WITH CHECK ADD  CONSTRAINT [FK_tIssueTracker_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tIssueTracker] CHECK CONSTRAINT [FK_tIssueTracker_ID_Company]
GO
ALTER TABLE [dbo].[tIssueTracker]  WITH CHECK ADD  CONSTRAINT [FK_tIssueTracker_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tIssueTracker] CHECK CONSTRAINT [FK_tIssueTracker_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tIssueTracker]  WITH CHECK ADD  CONSTRAINT [FK_tIssueTracker_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tIssueTracker] CHECK CONSTRAINT [FK_tIssueTracker_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tIssueTracker]
		ON [dbo].[tIssueTracker]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tIssueTracker
			SET    DateCreated = GETDATE()
			FROM   dbo.tIssueTracker hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tIssueTracker] ENABLE TRIGGER [rDateCreated_tIssueTracker]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tIssueTracker]
		ON [dbo].[tIssueTracker]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tIssueTracker
			SET    DateModified = GETDATE()
			FROM   dbo.tIssueTracker hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tIssueTracker] ENABLE TRIGGER [rDateModified_tIssueTracker]
GO
