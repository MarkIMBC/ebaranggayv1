﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vSalesReturn_Detail]
AS
  SELECT detail.*,
         UC.Name   AS CreatedBy,
         UM.Name   AS LastModifiedBy,
         item.Name Name_Item
  FROM   tSalesReturn_Detail detail
         LEFT JOIN tUser UC
                ON detail.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON detail.ID_LastModifiedBy = UM.ID
         LEFT JOIN dbo.tItem item
                ON item.ID = detail.ID_Item;

GO
