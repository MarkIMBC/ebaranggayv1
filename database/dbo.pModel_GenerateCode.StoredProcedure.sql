﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pModel_GenerateCode] @ID_Model         UNIQUEIDENTIFIER,
                                     @ID_CurrentObject VARCHAR(10),
                                     @IsNew            BIT = 0
AS
  BEGIN
      DECLARE @ModelName VARCHAR(MAX) = '';

      SELECT @ModelName = Name
      FROM   dbo._tModel
      WHERE  Oid = @ID_Model;

      IF @IsNew = 1
        BEGIN
            DECLARE @sql VARCHAR(MAX) = '
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @ID_CurrentObject VARCHAR(10);
            DECLARE @Code VARCHAR(MAX);
            DECLARE @ID_Company INT;

            SELECT @ID_Company = ID_Company
            FROM   dbo.t/*ModelName*/
            WHERE  ID = /*ID_CurrentObject*/

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  Name = ''/*ModelName*/'';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0, NULL);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.t/*ModelName*/
            SET    Code = @Code
            WHERE  ID = /*ID_CurrentObject*/'

            SET @sql = REPLACE(@sql, '/*ModelName*/', @ModelName)
            SET @sql = REPLACE(@sql, '/*ID_CurrentObject*/', @ID_CurrentObject)

            --print @sql
            exec (@sql)
        END;;
  END;

GO
