﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE    
 VIEW [dbo].[vBarangayHealthWorker_Resident_ListvIew]
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         hed.IsDeceased,
         company.Guid Guid_Company,
         ID_HouseholdNumber,
         IsHeadofFamily,
         hed.Name_OccupationalStatus,
         hed.Name_Religion,
         hed.AssignedBHW_ID_Employee,
         hed.AssignedBHW_Name_Employee,
         company.Name Name_Company,
         company.MainRouteLink
  FROM   vResident hed
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  hed.IsActive = 1
         AND company.IsActive = 1
         AND ISNULL(IsDeceased, 0) = 0
         AND company.MainRouteLink IN ( 'Barangay' )

GO
