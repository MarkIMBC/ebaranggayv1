﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[_pCreateListView]
    @TableName VARCHAR(200) ,
    @ListViewName VARCHAR(300)
AS
    DECLARE @Oid_Model UNIQUEIDENTIFIER ,
        @ModelName VARCHAR(100)
    SELECT  @Oid_Model = Oid
    FROM    dbo.[_tModel]
    WHERE   TableName = @TableName

    IF ( @Oid_Model IS NULL )
        RETURN

    SELECT  @ModelName = ( CASE WHEN SUBSTRING(@TableName, 1, 1) = '_'
                                THEN SUBSTRING(@TableName, 3, LEN(@TableName))
                                ELSE SUBSTRING(@TableName, 2, LEN(@TableName))
                           END )

    DECLARE @Oid_ListView UNIQUEIDENTIFIER = NEWID()

    INSERT  INTO dbo.[_tListView]
            ( Oid ,
              Code ,
              Name ,
              IsActive ,
              Comment ,
              ID_CreatedBy ,
              DateCreated ,
              ID_Model ,
              DataSource
		    )
    VALUES  ( @Oid_ListView ,
              NULL ,
              @ListViewName ,
              1 ,
              NULL ,
              1 ,
              GETDATE() ,
              @Oid_Model ,
              'SELECT * FROM ' + @TableName
            )

    INSERT  INTO dbo.[_tListView_Detail]
            ( Oid ,
              Code ,
              Name ,
              IsActive ,
              Comment ,
              ID_ListView ,
              DateCreated ,
              ID_CreatedBy
		    )
            SELECT  NEWID() ,
                    NULL ,
                    P.Name ,
                    1 ,
                    NULL ,
                    @Oid_ListView ,
                    GETDATE() ,
                    1
            FROM    dbo.[_tModel_Property] P
                    LEFT JOIN dbo.[_tModel] M ON P.ID_Model = M.Oid
            WHERE   M.Oid = @Oid_Model
GO
