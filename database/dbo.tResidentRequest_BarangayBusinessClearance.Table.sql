﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tResidentRequest_BarangayBusinessClearance](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_ResidentRequest] [int] NULL,
	[BusinessName] [varchar](300) NULL,
	[RegistrationNumber] [varchar](300) NULL,
	[ImageCertificateofRegistration] [varchar](300) NULL,
	[Address] [varchar](300) NULL,
	[DateValidity] [datetime] NULL,
	[ID_Resident] [int] NULL,
 CONSTRAINT [PK_tResidentRequest_BarangayBusinessClearance] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tResidentRequest_BarangayBusinessClearance] ON [dbo].[tResidentRequest_BarangayBusinessClearance]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tResidentRequest_BarangayBusinessClearance] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tResidentRequest_BarangayBusinessClearance]  WITH CHECK ADD  CONSTRAINT [FK_tResidentRequest_BarangayBusinessClearance_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tResidentRequest_BarangayBusinessClearance] CHECK CONSTRAINT [FK_tResidentRequest_BarangayBusinessClearance_ID_Company]
GO
ALTER TABLE [dbo].[tResidentRequest_BarangayBusinessClearance]  WITH CHECK ADD  CONSTRAINT [FK_tResidentRequest_BarangayBusinessClearance_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tResidentRequest_BarangayBusinessClearance] CHECK CONSTRAINT [FK_tResidentRequest_BarangayBusinessClearance_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tResidentRequest_BarangayBusinessClearance]  WITH CHECK ADD  CONSTRAINT [FK_tResidentRequest_BarangayBusinessClearance_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tResidentRequest_BarangayBusinessClearance] CHECK CONSTRAINT [FK_tResidentRequest_BarangayBusinessClearance_ID_LastModifiedBy]
GO
ALTER TABLE [dbo].[tResidentRequest_BarangayBusinessClearance]  WITH CHECK ADD  CONSTRAINT [FKtResidentRequest_BarangayBusinessClearance_ID_ResidentRequest] FOREIGN KEY([ID_ResidentRequest])
REFERENCES [dbo].[tResidentRequest] ([ID])
GO
ALTER TABLE [dbo].[tResidentRequest_BarangayBusinessClearance] CHECK CONSTRAINT [FKtResidentRequest_BarangayBusinessClearance_ID_ResidentRequest]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tResidentRequest_BarangayBusinessClearance]
		ON [dbo].[tResidentRequest_BarangayBusinessClearance]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tResidentRequest_BarangayBusinessClearance
			SET    DateCreated = GETDATE()
			FROM   dbo.tResidentRequest_BarangayBusinessClearance hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tResidentRequest_BarangayBusinessClearance] ENABLE TRIGGER [rDateCreated_tResidentRequest_BarangayBusinessClearance]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tResidentRequest_BarangayBusinessClearance]
		ON [dbo].[tResidentRequest_BarangayBusinessClearance]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tResidentRequest_BarangayBusinessClearance
			SET    DateModified = GETDATE()
			FROM   dbo.tResidentRequest_BarangayBusinessClearance hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tResidentRequest_BarangayBusinessClearance] ENABLE TRIGGER [rDateModified_tResidentRequest_BarangayBusinessClearance]
GO
