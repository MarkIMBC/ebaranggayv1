﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pModel_AfterSaved_Resident] (@ID_CurrentObject VARCHAR(10),
                                               @IsNew            BIT = 0)
AS
  BEGIN
      /* Generate Document Series */
      DECLARE @Oid_Model UNIQUEIDENTIFIER;
      DECLARE @Code VARCHAR(MAX) = '';
      DECLARE @ID_Company INT;

      SELECT @ID_Company = ID_Company
      FROM   dbo.tResident
      WHERE  ID = @ID_CurrentObject;

      SELECT @Oid_Model = m.Oid
      FROM   dbo._tModel m
      WHERE  m.TableName = 'tResident';

      IF @IsNew = 1
        BEGIN
            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0, NULL);

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tResident
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;

      INSERT tResident
             (Name,
              Source_ID_Resident_Family,
              ID_Company,
              IsActive,
              DateCreated,
              DateModified,
              ID_CreatedBy,
              ID_LastModifiedBy)
      SELECT Name,
             ID,
             @ID_Company,
             1,
             GETDATE(),
             GETDATE(),
             1,
             1
      FROM   tResident_Family family
      WHERE  family.ID_Resident = @ID_CurrentObject
             and ISNULL(family.ID_FamilyMember, 0) = 0

      Update tResident_Family
      SET    ID_FamilyMember = resident.ID
      FRoM   tResident resident
             inner join tResident_Family family
                     on resident.Source_ID_Resident_Family = family.ID
      WHERE  ID_Resident = @ID_CurrentObject
             AND resident.ID_Company = @ID_Company
  END;

GO
