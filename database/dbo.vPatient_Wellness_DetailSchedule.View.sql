﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    CREATE VIEW [dbo].[vPatient_Wellness_DetailSchedule]    
AS    
  SELECT hed.ID                                     ID_Patient_Wellness,    
         hed.Code                                   Code,    
         wellDetail.ID                              ID_Patient_Wellness_Detail,    
         wellSched.ID                               ID_Patient_Wellness_Schedule,    
         hed.ID_FilingStatus                        ID_FilingStatus,    
         hed.ID_Client                              ID_Client,    
         hed.ID_Patient                             ID_Patient,    
         hed.ID_Company                             ID_Company,    
         CASE    
           WHEN len(ISNULL(wellSched.Comment, '')) = 0 THEN wellDetail.Name_Item    
           ELSE wellSched.Comment    
         END                                        Name_Item_Patient_Wellness_Detail,    
         wellDetail.DateCreated                     DateCreated_Patient_Wellness_Detail,    
         wellDetail.Comment                         Comment_Patient_Wellness_Detail,    
         wellSched.Date                             Date_Patient_Wellness_Schedule,    
         wellSched.DateSent,    
         wellSched.IsSentSMS,    
         wellSched.Appointment_ID_FilingStatus     Appointment_ID_FilingStatus,    
         fsAppointment.Name                         Appointment_Name_FilingStatus,    
         wellSched.Appointment_CancellationRemarks Appointment_CancellationRemarks    
  FROM   tPatient_Wellness hed    
         INNER JOIN vPatient_Wellness_Detail wellDetail    
                 ON hed.ID = wellDetail.ID_Patient_Wellness    
         LEFT JOIN tPatient_Wellness_Schedule wellSched    
                ON hed.ID = wellSched.ID_Patient_Wellness    
         LEFT JOIN dbo.tFilingStatus fsAppointment    
                ON fsAppointment.ID = wellSched.Appointment_ID_FilingStatus     
GO
