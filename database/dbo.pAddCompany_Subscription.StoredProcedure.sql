﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pAddCompany_Subscription](@ID_Company INT,
                                    @DateStart  Date,
                                    @DateEnd    Date)
AS
  BEGIN
      INSERT INTO [dbo].[tCompany_Subscription]
                  ([Code],
                   [Name],
                   [IsActive],
                   [ID_Company],
                   [Comment],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [DateStart],
                   [DateEnd])
      VALUES      (NULL,
                   NULL,
                   1,
                   @ID_Company,
                   NULL,
                   GETDATE(),
                   GETDATE(),
                   1,
                   1,
                   CONVERT(DATETIME, @DateStart),
                   CONVERT(DATETIME, @DateEnd))
  END

GO
