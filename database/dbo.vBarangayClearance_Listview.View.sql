﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vBarangayClearance_Listview]
AS
  SELECT hed.ID,
         hed.Date,
         hed.Code,
         hed.Name,
         hed.Name_Gender,
         hed.Name_CivilStatus,
         hed.Purpose,
         hed.ID_Company,
         hed.IsActive
  FROM   vBarangayClearance hed

GO
