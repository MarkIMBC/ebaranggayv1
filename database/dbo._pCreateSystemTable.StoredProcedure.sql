﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[_pCreateSystemTable]
    @TableName VARCHAR(500)
AS
    BEGIN
        EXEC 
        (
        'CREATE TABLE ['+@TableName+'] (
			[Oid] [UNIQUEIDENTIFIER] NOT NULL ,
			[Code] [varchar] (50),
			[Name] [varchar] (200),
			[IsActive] [bit] NULL CONSTRAINT [DF_'+@TableName+'_IsActive] DEFAULT (1),
			[Comment] [varchar] (200) NULL,
			[DateCreated] [Datetime] NULL ,
			[DateModified] [DateTime] NULL ,
			[ID_CreatedBy] [int] NULL ,
			[ID_LastModifiedBy] [int] NULL ,
        CONSTRAINT [PK_'+@TableName+'] PRIMARY KEY  CLUSTERED 
        (
        [Oid]
        )  ON [PRIMARY] ,
        ) ON [PRIMARY]

        CREATE  INDEX [IX_'+@TableName+'] ON [dbo].['+@TableName+']([Name]) ON [PRIMARY]'
        )

    END
GO
