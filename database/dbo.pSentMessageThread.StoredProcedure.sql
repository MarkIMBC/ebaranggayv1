﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pSentMessageThread]
(
    @Sender_ID_User INT,
    @Recipient_ID_User INT,
    @Message VARCHAR(MAX)
)
AS
BEGIN

    DECLARE @Success BIT = 1;

    INSERT INTO dbo.tMessageThread
    (
        Recipient_ID_User,
        Sender_ID_User,
        Message,
        DateSent,
        DateRead,
        IsRead
    )
    VALUES
    (   @Recipient_ID_User, -- Recipient_ID_User - int
        @Sender_ID_User,    -- Sender_ID_User - int
        @Message,           -- Message - varchar(3000)
        GETDATE(),          -- DateSent - datetime
        NULL,               -- DateRead - datetime
        0                   -- IsRead - bit
        );

    SELECT '_';

    SELECT @Success Success;

END;

GO
