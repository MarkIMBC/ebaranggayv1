﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE   
 VIEW [dbo].[vPatientWaitingList_ListView]  
as  
  SELECT MAX(ID) ID ,  
         ID_Company,  
         MAX(DateCreated) DateCreated,  
         Name_Client,  
         Name_Patient,  
         ID_Client,  
         ID_Patient,  
         WaitingStatus_ID_FilingStatus,  
         BillingInvoice_ID_FilingStatus,  
         WaitingStatus_Name_FilingStatus,  
         ISNULL(BillingInvoice_Name_FilingStatus, '---') BillingInvoice_Name_FilingStatus  
  FROM   vPatientWaitingList  
  WHERE  WaitingStatus_ID_FilingStatus NOT IN ( 13, 4 )  
  Group BY 
	  ID_Company,  
         Name_Client,  
         Name_Patient,  
         ID_Client,  
         ID_Patient,  
         WaitingStatus_ID_FilingStatus,  
         BillingInvoice_ID_FilingStatus,  
         WaitingStatus_Name_FilingStatus,  
         ISNULL(BillingInvoice_Name_FilingStatus, '---') 
GO
