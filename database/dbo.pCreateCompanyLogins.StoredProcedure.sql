﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pCreateCompanyLogins](@CompanyName       VARCHAR(MAX),
                                        @CompanyCode       VARCHAR(MAX),
                                        @MaxSMSCountPerDay INT = 25)
AS
  BEGIN
      DECLARE @ID_Company INT = 0

      SET @CompanyName = lTrim(rTrim(@CompanyName))
      SET @CompanyCode = TRIM(@CompanyCode)

      IF(SELECT Count(*)
         FROM   tCOmpany
         WHERE  Name = @CompanyName) = 0
        BEGIN
            EXEC pCreateNewCompanyAccess
              @CompanyName,
              @CompanyCode
        END

      SELECT @ID_Company = ID
      FROM   tCompany
      WHERE  Name = @CompanyName

      EXEC [dbo].[pAdd_SMSPatientSOAP_Company]
        @ID_Company

      UPDATE tCompany
      SET    GUID = NewId()
      WHERE  ID = @ID_Company
             AND GUID IS NULL

      UPDATE tCompany
      SET    ReceptionPortalGuid = NewId()
      WHERE  ID = @ID_Company
             AND ReceptionPortalGuid IS NULL

      EXEC [dbo].[pAdd_Company_SMSSetting]
        @ID_Company,
        @MaxSMSCountPerDay

      EXEC pAddCompany_Subscription
        @ID_Company,
        '2021-01-01',
        '9999-12-31'

      EXEC pCreateCompanySQLLogin
        @ID_Company

      SELECT ID_Company,
             Name_Company,
             Name_Employee,
             ID ID_User,
             Username,
             Password
      FROM   vUser
      WHERE  ID_Company = @ID_Company
      ORDER  BY Name_Company,
                Name_Employee
  END 
GO
