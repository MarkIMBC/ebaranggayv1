﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SqlQueryNotificationStoredProcedure-55b535df-dd1f-4aa8-b93c-31d4f507574e] AS BEGIN BEGIN TRANSACTION; RECEIVE TOP(0) conversation_handle FROM [SqlQueryNotificationService-55b535df-dd1f-4aa8-b93c-31d4f507574e]; IF (SELECT COUNT(*) FROM [SqlQueryNotificationService-55b535df-dd1f-4aa8-b93c-31d4f507574e] WHERE message_type_name = 'http://schemas.microsoft.com/SQL/ServiceBroker/DialogTimer') > 0 BEGIN if ((SELECT COUNT(*) FROM sys.services WHERE name = 'SqlQueryNotificationService-55b535df-dd1f-4aa8-b93c-31d4f507574e') > 0)   DROP SERVICE [SqlQueryNotificationService-55b535df-dd1f-4aa8-b93c-31d4f507574e]; if (OBJECT_ID('SqlQueryNotificationService-55b535df-dd1f-4aa8-b93c-31d4f507574e', 'SQ') IS NOT NULL)   DROP QUEUE [SqlQueryNotificationService-55b535df-dd1f-4aa8-b93c-31d4f507574e]; DROP PROCEDURE [SqlQueryNotificationStoredProcedure-55b535df-dd1f-4aa8-b93c-31d4f507574e]; END COMMIT TRANSACTION; END
GO
