﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vListView]
AS
    SELECT  L.*,
			M.Name AS Model
    FROM    dbo.[_tListView] L 
	LEFT JOIN dbo.[_tModel] M ON L.ID_Model = M.Oid
GO
