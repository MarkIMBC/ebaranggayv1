﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE 
 VIEW [dbo].[vResidentProgramString]
AS
  SELECT DIStinct progA.ID_Resident,
                  STUFF((SELECT N', ' + Name_Program
                         FROM   dbo.vResident_Program progB
                         WHERE  progB.ID_Resident = progA.ID_Resident Order by Name_Program
                         FOR XML PATH(''), TYPE).value(N'.[1]', N'nvarchar(max)'), 1, 2, N'') ProgramString
  FROM   vResident_Program progA 
GO
