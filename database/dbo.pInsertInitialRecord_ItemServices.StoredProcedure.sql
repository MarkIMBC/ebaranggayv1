﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   PROC [dbo].[pInsertInitialRecord_ItemServices](@ID_Company INT) AS
BEGIN

	Declare @ItemServices Table (ServiceName Varchar(MAX), ID_ItemCategory INT)

	INSERT @ItemServices 
	VALUES
	('3in1- RCP', 4)
	,('4in1- RCCP', 4)
	,('5in1- DHPPiL', 4)
	,('6in1- DHPPiL CV', 4)
	,('8in1- DHPPi L4', 4)
	,('Anal Gland Express D', 3)
	,('Anal Gland Express E', 1)
	,('Antirabies Vaccine', 4)
	,('Blood Test- Send out- CBC', 16)
	,('Blood Test- Send out- CBC, Basic Biochem', 16)
	,('Blood Test- Send out- CBC, Comprehensive Biochem', 16)
	,('Blood Test- Send out- CBC, Comprehensive Biochem, Na, K, Amy, Lip', 16)
	,('Blood Test- Send out- Liver Screen', 16)
	,('Blood Test- Send out- Pancreatic Profile', 16)
	,('Blood Test- Send out- Renal Profile', 16)
	,('Blood Test- Send out- Single Parameter', 16)
	,('Blood Test- Send out- Thyroid Screen', 16)
	,('Consult Fee', 13)
	,('Consult Fee (Discounted)', 13)
	,('Consult Fee- Follow up', 13)
	,('Deworming 1-10kg', 14)
	,('Deworming 10.1-20kg', 14)
	,('Deworming 20.1-30kg', 14)
	,('Deworming 30.1-40kg', 4)
	,('Deworming 40.1-50kg', 14)
	,('Ear Clean I', 1)
	,('Ear Clean N', 1)
	,('Euthanasia', 3)
	,('Kennel Cough', 4)
	,('Leptospirosis Vaccine', 4)
	,('Microscopy- Cytology', 17)
	,('Microscopy- Direct Microscopy', 17)
	,('Microscopy- Ear Swab', 17)
	,('Microscopy- Fecalysis', 17)
	,('Microscopy- Skin Scraping', 17)
	,('Microscopy- Sticky Tape Prep', 17)
	,('Microscopy- Vaginal Smear', 17)
	,('Nail Trim D', 1)
	,('Nail Trim E', 1)
	,('Proheart 1-5kg', 4)
	,('Proheart 10.1-15kg', 4)
	,('Proheart 15.1-20kg', 4)
	,('Proheart 5.1-10kg', 4)
	,('Puppy/Kitten Parasite Check (fecalysis and ear swab)', 3)
	,('Rapid Test- Canine Blood Para 3way', 18)
	,('Rapid Test- Canine Blood Para 3way+ CHW', 18)
	,('Rapid Test- Canine Distemper', 18)
	,('Rapid Test- Feline FIV-FeLV', 18)
	,('Rapid Test- Heartworm', 18)
	,('Rapid Test- Parvo', 18)
	,('Rapid Test- Parvo-Corona', 18)
	,('Sedation', 5)
	,('Subcut Fluids', 3)
	,('Urinalysis (Strip, Cytology and Refractometer)', 17)
	,('Wood''s Lamp', 17)
	,('Wound Cleaning and Bandaging', 3)
	,('Wound Cleaning D', 3)
	,('Wound Cleaning E', 3)

	INSERT INTO [dbo].[tItem]
	(
		[ID_Company]
		,[Code]
		,[Name]
		,[ID_ItemType]
		,[ID_ItemCategory]
		,[MinInventoryCount]
		,[MaxInventoryCount]
		,[UnitCost]
		,[UnitPrice]
		,[CurrentInventoryCount]
		,[ID_InventoryStatus]
		,[IsActive]
		,[Comment]
		,[DateCreated]
		,[DateModified]
		,[ID_CreatedBy]
		,[ID_LastModifiedBy]
		,[Old_item_id]
		,[Old_procedure_id]
		,[OtherInfo_DateExpiration]
	)
	SELECT  @ID_Company
			,NULL
			,ServiceName
			,1
			,ID_ItemCategory
			,0
			,0
			,0
			,0
			,0
			,1
			,1
			,NULL
			,GETDATE()
			,GETDATE()
			,1
			,1
			,NULL
			,NULL
			,NULL
	FROM    @ItemServices


END
GO
