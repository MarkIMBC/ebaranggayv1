﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vCardType] AS 
				SELECT 
					H.* ,
					UC.Name AS CreatedBy,
					UM.Name AS LastModifiedBy
				FROM tCardType H
				LEFT JOIN tUser UC ON H.ID_CreatedBy = UC.ID	
				LEFT JOIN tUser UM ON H.ID_LastModifiedBy = UM.ID
GO
