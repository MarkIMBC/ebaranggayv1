﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vLawSuit]
AS
  SELECT H.*,
         UC.Name                                              AS CreatedBy,
         UM.Name                                              AS LastModifiedBy,
         ISNULL(residentComplaintnat.Name, H.ComplainantName) Complainant_Name_Resident,
         ISNULL(residentRespondent.Name, H.RespondentName)    Respondent_Name_Resident,
         fs.Name                                              Name_FilingStatus,
         lawsuitStatus.Name                                   Name_LawSuitStatus
  FROM   tLawSuit H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tResident residentComplaintnat
                ON H.Complainant_ID_Resident = residentComplaintnat.ID
         LEFT JOIN tResident residentRespondent
                ON H.Respondent_ID_Resident = residentRespondent.ID
         LEFT JOIN tFilingStatus fs
                ON H.ID_FilingStatus = fs.ID
         LEFT JOIN tLawSuitStatus lawsuitStatus
                ON H.ID_LawSuitStatus = lawsuitStatus.ID

GO
