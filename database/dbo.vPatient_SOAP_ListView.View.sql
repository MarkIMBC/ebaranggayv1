﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vPatient_SOAP_ListView]
AS
  SELECT ID,
         ID_Company,
         ID_SOAPType,
         ID_FilingStatus,
         Date,
         Code,
         Name_SOAPType,
         AttendingPhysician_Name_Employee,
         ID_Client,
         Name_Client,
         Name_Patient,
         Name_FilingStatus,
         ID_Patient_Confinement,
         IsNull(History, '')                                   History,
         IsNull(ClinicalExamination, '')                       ClinicalExamination,
         IsNull(Interpretation, '')                            Interpretation,
         IsNull(Diagnosis, '')                                 Diagnosis,
         IsNull(Treatment, '')                                 Treatment,
         IsNull(ClientCommunication, '')                       ClientCommunication,
         IsNull(Prescription, '')                              Prescription,
         len(Trim(IsNull(History, '')))                        Count_History,
         len(Trim(IsNull(ClinicalExamination, '')))            Count_ClinicalExamination,
         IsNull(soapLabImgCount.TotalCount, 0)
         + len(Trim(IsNull(Interpretation, '')))               Count_LaboratoryImages,
         len(Trim(IsNull(Diagnosis, '')))                      Count_Diagnosis,
         len(Trim(IsNull(Treatment, '')))                      Count_Treatment,
         len(Trim(IsNull(ClientCommunication, '')))            Count_ClientCommunication,
         IsNull(soapPlanCount.TotalCount, 0)                   Count_Plan,
         IsNull(soapPrescription.TotalCount, 0)
         + len(Trim(IsNull(soap.Prescription, '')))            Count_Prescription,
         soap.DateCreated,
         soap.DateModified,
         'Date Created: '
         + Format(soap.DateCreated, 'MMM. dd, yyyy hh:mm tt')
         + Char(13) + 'Date Modified: '
         + Format(soap.DateModified, 'MMM. dd, yyyy hh:mm tt') TooltipText
  FROM   dbo.vPatient_SOAP soap
         LEFT JOIN vPatient_SOAP_Plan_Count soapPlanCount
                ON soap.ID = soapPlanCount.ID_Patient_SOAP
         LEFT JOIN vPatient_SOAP_Prescription_Count soapPrescription
                ON soap.ID = soapPrescription.ID_Patient_SOAP
         LEFT JOIN vPatient_SOAP_LaboratoryImages_Count soapLabImgCount
                ON soap.ID = soapLabImgCount.ID_Patient_SOAP
  WHERE  ID_FilingStatus NOT IN ( 4 ) 
GO
