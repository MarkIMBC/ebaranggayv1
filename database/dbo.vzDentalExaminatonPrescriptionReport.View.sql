﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vzDentalExaminatonPrescriptionReport]
AS
SELECT hed.ID,
       hed.Code,
       hed.Name_Patient,
       hed.Name_Doctor,
       hed.Date,
       hed.Name_FilingStatus,
       hed.Comment,
       hed.ApprovedBy_Name_User,
       patient.ID_Company,
       company.ImageLogoLocationFilenamePath,
       company.Name Name_Company,
       company.Address Address_Company
FROM dbo.vPatient_DentalExamination hed
    LEFT JOIN dbo.tPatient patient
        ON patient.ID = hed.ID_Patient
    LEFT JOIN dbo.vCompany company
        ON company.ID = patient.ID_Company;
GO
