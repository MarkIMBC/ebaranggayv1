﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vMessage]
AS
SELECT H.*,
       UC.Name AS CreatedBy,
       UM.Name AS LastModifiedBy,
       recipientUser.Name Recipient_Name_User
FROM tMessage H
    LEFT JOIN tUser UC
        ON H.ID_CreatedBy = UC.ID
    LEFT JOIN tUser UM
        ON H.ID_LastModifiedBy = UM.ID
    LEFT JOIN tUser recipientUser
        ON H.Recipient_ID_User = recipientUser.ID
GO
