﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CReate 
 PROC [dbo].[_tempGeneratePatient_SOAP](@ID_Company INT)
as
  BEGIN
      DECLARE @IDs_Patient_SOAP TABLE
        (
           RowID      INT,
           ID_Patient_SOAP INT
        )
      DECLARE @currentCounter INT = 1
      DECLARE @maxCounter INT = 1

      INSERT @IDs_Patient_SOAP
      SELECT ROW_NUMBER()
               OVER(
                 ORDER BY ID ASC) AS RowID,
             ID
      FROM   tPatient_SOAP
      WHERE  ID_Company = @ID_Company
             AND ISNULL(Code, '') = ''

      SELECT @maxCounter = COUNT(*)
      FROM   @IDs_Patient_SOAP

      IF( @maxCounter > 0 )
        SELECT Name,
               @maxCounter
        FROM   tCompany
        WHERE  ID = @ID_Company

      WHILE @currentCounter <= @maxCounter
        BEGIN
            DECLARE @ID_Patient_SOAP INT = 0

            SELECT @ID_Patient_SOAP = ID_Patient_SOAP
            FROM   @IDs_Patient_SOAP
            WHERE  RowID = @currentCounter

            exec [pModel_AfterSaved_Patient_SOAP]
              @ID_Patient_SOAP,
              1

            SET @currentCounter = @currentCounter + 1
        END
  END

GO
