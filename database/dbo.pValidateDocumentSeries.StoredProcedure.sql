﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pValidateDocumentSeries]
(
    @ID_CurrentObject INT,
    @ID_Model UNIQUEIDENTIFIER
)
AS
BEGIN

    DECLARE @isValid BIT = 1;
    DECLARE @message VARCHAR(MAX) = '';
    DECLARE @msgTHROW VARCHAR(MAX) = '';

    BEGIN TRY

        SET @ID_CurrentObject = ISNULL(@ID_CurrentObject, 0);

        IF (@ID_CurrentObject < 1)
            DECLARE @Count_Model INT = 0;

        SELECT @ID_Model = ID_Model
        FROM dbo.tDocumentSeries
        WHERE ID = @ID_CurrentObject;

        SELECT @Count_Model = COUNT(*)
        FROM dbo.tDocumentSeries
        WHERE ID_Model = @ID_Model;

        IF @Count_Model > 0
        BEGIN

            SET @msgTHROW = 'Model is already assigned from other Document Series record.';
            THROW 50005, @msgTHROW, 1;
        END;

    END TRY
    BEGIN CATCH

        SET @message = ERROR_MESSAGE();
        SET @isValid = 0;
    END CATCH;

    SELECT '_';
    SELECT @isValid isValid,
           @message message;

END;

GO
