﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pLog_BirthDateSMSGreeting](@ID_Client      INT,
                                     @ContactNumber  VARCHAR(MAX),
                                     @ID_Patient     INT,
                                     @iTextMo_Status INT,
                                     @Message        VARCHAR(MAX))
as
  BEGIN
      DECLARE @ID_Company int = 0
	  DECLARE @Success BIT = 1
	  DECLARE @DateSent DateTime
	  
	  if(@iTextMo_Status = 0)
		SET @DateSent = GETDATE()

      SELECT @ID_Company = ID_Company
      FROM   tPatient
      where  ID = @ID_Patient

      INSERT INTO [dbo].[tPatient_BirthDateSMSGreetingLog]
                  ([Code],
                   [Name],
                   [IsActive],
                   [ID_Company],
                   [Comment],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [ID_Client],
                   [ContactNumber],
                   [ID_Patient],
                   [iTextMo_Status],
                   [DateSent],
                   [Message])
      VALUES      (NULL,
                   NULL,
                   1,
                   @ID_Company,
                   '',
                   GETDATE(),
                   GETDATE(),
                   1,
                   1,
                   @ID_Client,
                   @ContactNumber,
                   @ID_Patient,
                   @iTextMo_Status,
                   @DateSent,
                   @Message)

	 SELECT '_'

      SELECT @Success Success;
  END

GO
