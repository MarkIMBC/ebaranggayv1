﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   PROC [dbo].[___pGetSendSOAPPlan](@Date      DateTime,
                                    @IsSMSSent Bit = NULL)
AS
  BEGIN
      DECLARE @Success BIT = 1;
      DECLARE @SMSSent Table
        (
           IsSMSSent bit
        )
          
      if @IsSMSSent IS NULL
        INSERT @SMSSent
        VALUES (0),
               (1)
      ELSE
        INSERT @SMSSent
        VALUES (@IsSMSSent)
          
      SET @Date = ISNULL(@Date, GETDATE())
          
      SELECT '_',
             '' AS summary,
             '' AS records;
          
      Declare @record TABLE
        (
           ID_Company           INT,
           Name_Company         VARCHAR(MAX),
           Name_Client          VARCHAR(MAX),
           ContactNumber_Client VARCHAR(MAX),
           DateReturn           DATETime,
           Name_Item            VARCHAR(MAX),
           Comment              VARCHAR(MAX),
           Message              VARCHAR(MAX),
           DateSending          DATETime,
           ID_Patient_SOAP_Plan INT,
           DateCreated          DATETime
        )
          
      INSERT @record
      SELECT c.ID                                                                                                                                                                                   ID_Company,
             c.Name                                                                                                                                                                                 Name_Company,
             client.Name                                                                                                                                                                            Name_Client,
             dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                                     ContactNumber_Client,
             vacSchedule.Date DateReturn,
             vac.Name_Item,
             ISNULL(patientSOAP.Comment, '')                                                                                                                                                        Comment,
             dbo.fGetSOAPLANMessage(c.Name, c.SOAPPlanSMSMessage, client.Name, ISNULL(c.ContactNumber, ''), patient.Name, vac.Name_Item, ISNULL(patientSOAP.Comment, ''), vacSchedule.Date) Message,
             CONVERT(DATE, DATEADD(DAY, -1, vacSchedule.Date ))                                                                                                                                   DateSending,
             vac.ID                                                                                                                                                                            ID_Patient_SOAP_Plan,
             patientSOAP.DateCreated
      FROM   dbo.tPatient_SOAP patientSOAP
             LEFT JOIN dbo.tPatient patient
                    ON patient.ID = patientSOAP.ID_Patient
             LEFT JOIN dbo.tClient client
                    ON client.ID = patient.ID_Client
             LEFT JOIN tCompany c
                    ON c.iD = patientSOAP.ID_Company
             INNER JOIN dbo.vPatient_Vaccination vac
                     ON vac.ID_Patient_SOAP = patientSOAP.ID
			 inner join tPatient_Vaccination_Schedule vacSchedule on vac.ID = vacSchedule.ID_Patient_Vaccination
      WHERE  patientSOAP.ID_FilingStatus IN ( 1, 3, 13 )
             AND ISNULL(vacSchedule.IsSentSMS, 0) IN (SELECT IsSMSSent
                          FROM   @SMSSent)
             AND ISNULL(vac.ID_CLient, 0) > 0
             AND ISNULL(patient.IsDeceased, 0) = 0
             AND ( ( CONVERT(DATE, DATEADD(DAY, -1, vacSchedule.Date)) = CONVERT(DATE, @Date) ) )
             AND patientSOAP.ID_Company IN (SELECT ID_Company
                                            FROM   tCompany_SMSSetting
                           WHERE  ISNULL(IsActive, 0) = 1)
      ORDER  BY c.Name
          
      SELECT @Success Success;
          
      SELECT FORMAT(DateSending, 'yyyy-MM-dd') DateSending,
             tbl.Name_Company,
             Count(*)                          Count,
             SUM(CASE
                   WHEN LEN(tbl.Message) <= 160 THEN 1
                   ELSE
                     CASE
                       WHEN LEN(tbl.Message) <= 306 THEN 2
                       ELSE
                         CASE
                           WHEN LEN(tbl.Message) <= 459 THEN 3
                           ELSE 4
                         END
                     END
                 END)                          TotalConsumedSMSCredit
      FROM   (SELECT *
              FROM   @record) tbl
      GROUP  BY FORMAT(DateSending, 'yyyy-MM-dd'),
                Name_Company
      Order  BY DateSending DESC,
                Name_Company
  END
SELECT rec.*,
       CASE
         WHEN LEN(Message) <= 160 THEN 1
         ELSE
           CASE
             WHEN LEN(Message) <= 306 THEN 2
             ELSE
               CASE
                 WHEN LEN(Message) <= 459 THEN 3
                 ELSE 4
               END
           END
       END          ConsumedSMSCredit,
       LEN(Message) CharLength
FROM   @record rec
       inner join tCompany com
               on com.ID = rec.ID_Company
Order  BY FORMAT(DateSending, 'yyyy-MM-dd') DESC,
          com.ID_PackagePlan DESC,
          Name_Company,
          Name_Client 
GO
