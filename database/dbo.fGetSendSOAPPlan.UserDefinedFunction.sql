﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fGetSendSOAPPlan](@Date             DateTime,
                                        @IsSMSSent        Bit = NULL,
                                        @IDsCompanyString VARCHAR(MAX))
RETURNS @table TABLE(
  ID_Company           INT,
  Name_Company         VARCHAR(MAX),
  Name_Client          VARCHAR(MAX),
  ContactNumber_Client VARCHAR(MAX),
  DateReturn           DATETime,
  Name_Item            VARCHAR(MAX),
  Comment              VARCHAR(MAX),
  Message              VARCHAR(MAX),
  DateSending          DATETime,
  DateCreated          DATETime,
  ID_Reference         INT,
  Oid_Model            VARCHAR(MAX),
  Code                 VARCHAR(MAX))
as
  BEGIN
      DECLARE @IDs_Company typIntList
      DECLARE @DayBeforeInterval INT =1
      DECLARE @Patient_Vaccination_Schedule_ID_Model VARCHAR(MAX) =''
      DECLARE @Patient_SOAP_Plan_ID_Model VARCHAR(MAX) =''
      DECLARE @Patient_Wellness_Schedule_ID_Model VARCHAR(MAX) =''

      select @Patient_Vaccination_Schedule_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_Vaccination_Schedule'

      select @Patient_SOAP_Plan_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_SOAP_Plan'

      select @Patient_Wellness_Schedule_ID_Model = Oid
      FRom   _tModel
      WHERE  TableName = 'tPatient_Wellness_Schedule'

      if( LEN(TRIM(@IDsCompanyString)) > 0 )
        BEGIN
            INSERT @IDs_Company
            Select Part
            FROM   dbo.fGetSplitString(@IDsCompanyString, ',')
        END
      ELSE
        BEGIN
            INSERT @IDs_Company
            SELECT ID_Company
            FROM   tCompany_SMSSetting
            WHERE  ISNULL(IsActive, 0) = 1
        END

      DECLARE @Success BIT = 1;
      DECLARE @SMSSent Table
        (
           IsSMSSent bit
        )

      if @IsSMSSent IS NULL
        INSERT @SMSSent
        VALUES (0),
               (1)
      ELSE
        INSERT @SMSSent
        VALUES (@IsSMSSent)

      SET @Date = ISNULL(@Date, GETDATE())
      SET @DayBeforeInterval = 0 - @DayBeforeInterval

      /*SOAP PLAN */
      INSERT @table
      SELECT c.ID                                                                                                                                                                                   ID_Company,
             c.Name                                                                                                                                                                                 Name_Company,
             client.Name                                                                                                                                                                            Name_Client,
             dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                                     ContactNumber_Client,
             soapPlan.DateReturn,
             soapPlan.Name_Item,
             ISNULL(patientSOAP.Comment, '')                                                                                                                                                        Comment,
             dbo.fGetSOAPLANMessage(c.Name, c.SOAPPlanSMSMessage, client.Name, ISNULL(c.ContactNumber, ''), patient.Name, soapPlan.Name_Item, ISNULL(patientSOAP.Comment, ''), soapPlan.DateReturn) Message,
             CONVERT(DATE, DATEADD(DAY, -1, soapPlan.DateReturn))                                                                                                                                   DateSending,
             patientSOAP.DateCreated,
             soapPlan.ID                                                                                                                                                                            ID_Patient_SOAP_Plan,
             @Patient_SOAP_Plan_ID_Model,
             patientSOAP.Code
      FROM   dbo.tPatient_SOAP patientSOAP
             LEFT JOIN dbo.tPatient patient
                    ON patient.ID = patientSOAP.ID_Patient
             LEFT JOIN dbo.tClient client
                    ON client.ID = patient.ID_Client
             LEFT JOIN tCompany c
                    ON c.iD = patientSOAP.ID_Company
             INNER JOIN dbo.vPatient_SOAP_Plan soapPlan
                     ON soapPlan.ID_Patient_SOAP = patientSOAP.ID
             INNER JOIN @IDs_Company idsCompany
                     on patientSOAP.ID_Company = idsCompany.ID
      WHERE  patientSOAP.ID_FilingStatus IN ( 1, 3, 13 )
             AND ISNULL(soapPlan.IsSentSMS, 0) IN (SELECT IsSMSSent
                                                   FROM   @SMSSent)
             AND ISNULL(ID_CLient, 0) > 0
             AND ISNULL(patient.IsDeceased, 0) = 0
             AND (( CONVERT(DATE, DATEADD(DAY, @DayBeforeInterval, soapPlan.DateReturn)) = CONVERT(DATE, @Date) ))
      ORDER  BY c.Name

      /*Patient Wellness Schedule*/
      INSERT @table
      SELECT DISTINCT c.ID                                                                                                                                                                                                                                                                ID_Company,
             c.Name                                                                                                                                                                                                                                                              Name_Company,
             client.Name                                                                                                                                                                                                                                                         Name_Client,
             dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                                                                                                                  ContactNumber_Client,
             wellDetSched.Date_Patient_Wellness_Schedule,
             Name_Item_Patient_Wellness_Detail    ,
             Name_Item_Patient_Wellness_Detail                                                                                                                                                                                                         Comment,
             dbo.fGetSOAPLANMessage(c.Name, c.SOAPPlanSMSMessage, client.Name, ISNULL(c.ContactNumber, ''), patient.Name, wellDetSched.Name_Item_Patient_Wellness_Detail, ISNULL(wellDetSched.Comment_Patient_Wellness_Detail, ''), wellDetSched.Date_Patient_Wellness_Schedule) Message,
             CONVERT(DATE, DATEADD(DAY, -1, wellDetSched.Date_Patient_Wellness_Schedule))                                                                                                                                                                                        DateSending,
             wellDetSched.DateCreated_Patient_Wellness_Detail,
             wellDetSched.ID_Patient_Wellness_Schedule                                                                                                                                                                                                                           ID_Patient_Wellness_Schedule,
             @Patient_Wellness_Schedule_ID_Model,
             wellDetSched.Code
      FROM   dbo.vPatient_Wellness_DetailSchedule wellDetSched
             LEFT JOIN dbo.tPatient patient
                    ON patient.ID = wellDetSched.ID_Patient
             LEFT JOIN dbo.tClient client
                    ON client.ID = wellDetSched.ID_Client
             LEFT JOIN tCompany c
                    ON c.iD = wellDetSched.ID_Company
             INNER JOIN @IDs_Company idsCompany
                     on wellDetSched.ID_Company = idsCompany.ID
      WHERE  wellDetSched.ID_FilingStatus IN ( 1, 3, 13 )
             AND ISNULL(wellDetSched.IsSentSMS, 0) IN (SELECT IsSMSSent
                                                       FROM   @SMSSent)
             AND ISNULL(wellDetSched.ID_CLient, 0) > 0
             AND ISNULL(patient.IsDeceased, 0) = 0
             AND (( CONVERT(DATE, DATEADD(DAY, @DayBeforeInterval, wellDetSched.Date_Patient_Wellness_Schedule)) = CONVERT(DATE, @Date) ))
      ORDER  BY c.Name

      /*Vaccination*/
      Declare @IDs_Vaccination_ExistingPatientWellness typIntList

      INSERT @IDs_Vaccination_ExistingPatientWellness
      SELECT hed.ID_Patient_Vaccination
      FROM   tPatient_Wellness hed
             INNER JOIN tCompany c
                     ON c.iD = hed.ID_Company
      WHERE  ID_Patient_Vaccination IS NOT NULL

      INSERT @table
      SELECT c.ID                                                                                                                                                                   ID_Company,
             c.Name                                                                                                                                                                 Name_Company,
             client.Name                                                                                                                                                            Name_Client,
             dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2)                                                                                                     ContactNumber_Client,
             vacSchedule.Date                                                                                                                                                       DateReturn,
             vac.Name_Item,
             ISNULL(vac.Comment, '')                                                                                                                                                Comment,
             dbo.fGetSOAPLANMessage(c.Name, c.SOAPPlanSMSMessage, client.Name, ISNULL(c.ContactNumber, ''), patient.Name, vac.Name_Item, ISNULL(vac.Comment, ''), vacSchedule.Date) Message,
             CONVERT(DATE, DATEADD(DAY, -1, vacSchedule.Date))                                                                                                                      DateSending,
             vac.DateCreated,
             vacSchedule.ID                                                                                                                                                         ID_Patient_Vaccination_Schedule,
             @Patient_Vaccination_Schedule_ID_Model,
             vac.Code
      FROM   dbo.vPatient_Vaccination vac
             LEFT JOIN dbo.tPatient patient
                    ON patient.ID = vac.ID_Patient
             LEFT JOIN dbo.tClient client
                    ON client.ID = patient.ID_Client
             LEFT JOIN tCompany c
                    ON c.iD = vac.ID_Company
             inner join tPatient_Vaccination_Schedule vacSchedule
                     on vac.ID = vacSchedule.ID_Patient_Vaccination
             INNER JOIN @IDs_Company idsCompany
                     on vac.ID_Company = idsCompany.ID
      WHERE  vac.ID_FilingStatus IN ( 1, 3, 13 )
             AND ISNULL(vacSchedule.IsSentSMS, 0) IN (SELECT IsSMSSent
                                                      FROM   @SMSSent)
             AND ISNULL(vac.ID_CLient, 0) > 0
             AND ISNULL(patient.IsDeceased, 0) = 0
             AND (( CONVERT(DATE, DATEADD(DAY, @DayBeforeInterval, vacSchedule.Date)) = CONVERT(DATE, @Date) ))
			 AND vac.ID NOT IN ( SELECT ID FROM @IDs_Vaccination_ExistingPatientWellness)

      RETURN
  end

GO
