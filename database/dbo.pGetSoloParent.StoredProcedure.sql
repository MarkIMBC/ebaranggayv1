﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROCEDURE [dbo].[pGetSoloParent] @ID         INT = -1,
                               @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' Reports

      DECLARE @ID_User      INT,
              @ID_Warehouse INT,
              @ID_Company   INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL      AS [_],
                           -1        AS [ID],
                           NULL      AS [Code],
                           NULL      AS [Name],
                           1         AS [IsActive],
                           NULL      AS [ID_Company],
                           NULL      AS [Comment],
                           NULL      AS [DateCreated],
                           NULL      AS [DateModified],
                           NULL      AS [ID_CreatedBy],
                           NULL      AS [ID_LastModifiedBy],
                           GETDATE() Date) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vSoloParent H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   dbo.fGetModelReportMenuItemsByCompany(@ID_Company, 'SoloParent')
  END

GO
