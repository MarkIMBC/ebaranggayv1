﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE     
 PROC [dbo].[pGetDashboardBarangayHealthWorkerResidentOccupationStatusPieChart] (@ID_UserSession INT,
                                                                                  @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      DECLARE @dataSource TABLE
        (
           Name  VARCHAR(MAX),
           Count INT
        )

      INSERT @dataSource
      SELECT ISNULL(Name_OccupationalStatus, 'Walang nakatalaga'),
             Count(*)
      FROM   vResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      WHERE  company.ID NOT IN ( 1 )
             AND resident.IsActive = 1
             and ISNULL(resident.IsDeceased, 0) = 0
      GROUP  BY Name_OccupationalStatus

      IF (SELECT COUNT(*)
          FROM   @dataSource) = 0
        BEGIN
            INSERT @dataSource
            SELECT 'No Record',
                   0
        END

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear DateYear

      SELECT *
      FROM   @dataSource
  END

GO
