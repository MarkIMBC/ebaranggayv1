﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE        
 PROC [dbo].[pGetDashboardBarangaySummary] @ID_UserSession INT = NULL
as
  BEGIN
      DECLARE @ID_User    INT,
              @ID_Company INT
      DECLARE @ResidentCount INT = 0
      DECLARE @TotalResidentMaleCount INT = 0
      DECLARE @TotalResidentFemaleCount INT = 0
      DECLARE @HouseholdCount INT = 0
      DECLARE @HeadOfFamilyCount INT = 0
      DECLARE @ResidentCountAge60Above INT = 0
      DECLARE @ResidentCountAge18to59 INT = 0
      DECLARE @ResidentCountAge01to17 INT = 0
      DECLARE @ResidentCountAge5to9 INT = 0
      DECLARE @ResidentCountAge24to84months INT = 0
      DECLARE @ResidentCountAge12to23months INT = 0
      DECLARE @ResidentCountAge0to11months INT = 0
      DECLARE @ResidentCountMale INT = 0
      DECLARE @ResidentCountFemale INT = 0
      DECLARE @ResidentVaccinatedCount INT = 0
      DECLARE @ResidentNonVaccinatedCount INT = 0
      DECLARE @ResidentNoOccupationCount INT = 0
      DECLARE @VoterCount INT = 0
      DECLARE @Male_ID_Gender INT = 1
      DECLARE @Female_ID_Gender INT = 2
      DECLARE @Summary TABLE
        (
           RowIndex int IDENTITY(1, 1) PRIMARY KEY,
           Name     VARCHAR(MAX),
           Count    Int,
		   Label   VARCHAR(MAX)
        )

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      INSERT @Summary
      SELECT 'ResidentCount',
             COUNT(*),
             'Bilang ng Residente'
      FROM   vResidentNonDeceased
      where  ID_Company = @ID_Company
             and IsActive = 1

      INSERT @Summary
      SELECT 'HouseholdCount',
             COUNT(*),
             'Bilang ng Household'
      FROM   tHouseholdNumber
      WHERE  IsActive = 1
             and ID_Company = @ID_Company

      INSERT @Summary
      SELECT 'HeadOfFamilyCount',
             COUNT(*),
             'Bilang ng Pamilya'
      FROM   vResidentNonDeceased
      where  ID_Company = @ID_Company
             and ISNULL(IsHeadofFamily, 0) = 1

      INSERT @Summary
      SELECT 'ResidentCountMale',
             COUNT(*),
             'Bilang ng Lalake'
      FROM   vResidentNonDeceased
      where  ID_Company = @ID_Company
             and IsActive = 1
             ANd ID_Gender = @Male_ID_Gender

      INSERT @Summary
      SELECT 'ResidentCountFemale',
             COUNT(*),
             'Bilang ng Babae'
      FROM   vResidentNonDeceased
      where  ID_Company = @ID_Company
             and IsActive = 1
             ANd ID_Gender = @Female_ID_Gender

      INSERT @Summary
      SELECT 'ResidentCountAge0to11months',
             COUNT(*),
             'Bilang ng Sanggol (0-11 Buwan)'
      FROM   vResidentNonDeceased
      where  ID_Company = @ID_Company
             and IsActive = 1
             and DateBirth IS NOT NULL
             AND DATEDIFF(MONTH, DateBirth, GETDATE()) BETWEEN 0 AND 11

      INSERT @Summary
      SELECT 'ResidentCountAge01to17',
             COUNT(*),
             'Bilang ng Bata / Kabataan (1-17 taon)'
      FROM   vResidentNonDeceased
      where  ID_Company = @ID_Company
             and IsActive = 1
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) BETWEEN 1 AND 17

      INSERT @Summary
      SELECT 'ResidentCountAge18to59',
             COUNT(*),
             'Bilang ng Hustong Gulang (18-59 taon)'
      FROM   vResidentNonDeceased
      where  ID_Company = @ID_Company
             and IsActive = 1
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) BETWEEN 18 AND 59

      INSERT @Summary
      SELECT 'ResidentCountAge60Above',
             COUNT(*),
             'Bilang ng Senior Citizen (60 pataas)'
      FROm   vResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) >= 60

      INSERT @Summary
      SELECT 'ResidentVaccinatedCount',
             COUNT(*),
             'Bilang ng Bakunado'
      FROM   vResidentNonDeceased
      where  ID_Company = @ID_Company
             and ISNULL(IsVaccinated, 0) = 1

      INSERT @Summary
      SELECT 'ResidentNonVaccinatedCount',
             COUNT(*),
             'Bilang ng Walang Bakuna'
      FROM   vResidentNonDeceased
      where  ID_Company = @ID_Company
             and ISNULL(IsVaccinated, 0) = 0

      INSERT @Summary
      SELECT 'ResidentNoOccupationCount',
             COUNT(*),
             'Bilang ng Walang Trabaho'
      FROM   vResidentNonDeceased
      where  ID_Company = @ID_Company
             and ISNULL(ID_OccupationalStatus, 0) IN ( 1, 3 )

      SELECT '_',
             '' AS Summary

      SELECT GETDATE() Date

      SELECT *
      FROM   @Summary
  END

GO
