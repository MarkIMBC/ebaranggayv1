﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE 
 PROC [dbo].[pSaveClientPatientRecords_validation] (@clientpatients TYPSAVECLIENTPATIENTRECORD READONLY,
                                                         @ID_UserSession INT)
AS
  BEGIN
      DECLARE @Name VARCHAR(MAX) = '';
      DECLARE @message VARCHAR(MAX) = '';
      DECLARE @ID_User INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User
      FROM   tUserSession
      WHERE  ID = @ID_UserSession

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      SELECT @Name = rTrim(lTrim(Name))
      FROM   @clientpatients

      IF ( len(@Name) = 0 )
        BEGIN
            SET @message = 'Client name is required';

            THROW 50001, @message, 1;
        END;

      IF(SELECT Count(*)
         FROM   tClient
         WHERE  ID_Company = @ID_Company
                AND rTrim(lTrim(Name)) = @Name
                AND IsActive = 1) > 0
        BEGIN
            SET @message = 'Client name is already exist.';

            THROW 50001, @message, 1;
        END
  END

GO
