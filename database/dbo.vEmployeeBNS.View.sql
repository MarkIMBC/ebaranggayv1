﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 VIEW [dbo].[vEmployeeBNS]
AS
  SELECT *
  FROM   vEmployeeActive
  WHERE  Name_Position IN ( 'BNS' )
         and ISNULL(IsActive, 0) = 1

GO
