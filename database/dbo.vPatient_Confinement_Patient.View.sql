﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vPatient_Confinement_Patient]
AS
  SELECT confinePatient.*,
         patient.Name Name_Patient
  FROM   tPatient_Confinement_Patient confinePatient
         LEFT JOIN tPatient patient
                ON confinePatient.ID_Patient = patient.ID

GO
