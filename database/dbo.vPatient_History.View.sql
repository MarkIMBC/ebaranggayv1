﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vPatient_History]
as
	
	SELECT 
		hed.*, 
		CONVERT(VARCHAR(100), hed.Date, 101) DateString,  
		doctor.Name Name_Doctor,
		filingStatus.Name Name_FilingStatus 
	FROM tPatient_History hed
	LEFT JOIN tEmployee doctor on hed.ID_Doctor = doctor.ID
	LEFT JOIN tFilingStatus filingStatus on hed.ID_FilingStatus = filingStatus.ID
GO
