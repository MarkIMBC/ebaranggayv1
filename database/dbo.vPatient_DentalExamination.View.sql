﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
    
    
CREATE VIEW [dbo].[vPatient_DentalExamination]        
AS        
SELECT hed.ID,        
       hed.Code,        
       hed.Name,        
       hed.IsActive,        
       hed.Comment,        
       hed.ID_Patient,        
       hed.ID_Doctor,        
       hed.Date,        
       CONVERT(VARCHAR(100), hed.Date, 101) DateString,        
       pt.Name Patient,     
	   pt.Name Name_Patient,  
       hed.ID_Dentition,        
       emp.Name Doctor,        
       emp.Name Name_Doctor,        
       dentition.Name Name_Dentition,        
       dentition.Name Dentition,    
	   hed.ID_FilingStatus ,  
	   status.Name Name_FilingStatus,
	   userApproved.Name ApprovedBy_Name_User
FROM dbo.tPatient_DentalExamination hed        
    LEFT JOIN dbo.tEmployee emp        
        ON emp.ID = hed.ID_Doctor        
    LEFT JOIN dbo.tDentition dentition        
        ON dentition.ID = hed.ID_Dentition        
 LEFT JOIN dbo.tFilingStatus status        
        ON status.ID = hed.ID_FilingStatus
	LEFT JOIN dbo.tUser userApproved 
		ON userApproved.ID = hed.ID_ApprovedBy
    INNER JOIN dbo.tPatient pt        
        ON pt.ID = hed.ID_Patient

GO
