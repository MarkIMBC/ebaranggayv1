﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[_pCreateForeignKey]
    @TableA VARCHAR(100) ,
    @TableA_Column VARCHAR(100) ,
    @TableB VARCHAR(100) ,
    @ForeignKey VARCHAR(100) = 'ID',
	@IsAggregrated BIT = 0
AS
    IF NOT EXISTS ( SELECT  rc.*
                FROM    INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS AS rc
                        LEFT JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE KCU1 ON KCU1.CONSTRAINT_CATALOG = rc.CONSTRAINT_CATALOG
                                                              AND KCU1.CONSTRAINT_SCHEMA = rc.CONSTRAINT_SCHEMA
                                                              AND KCU1.CONSTRAINT_NAME = rc.CONSTRAINT_NAME
                WHERE   KCU1.TABLE_NAME = @TableA
                        AND KCU1.COLUMN_NAME = @TableA_Column -- AND rc.CONSTRAINT_SCHEMA = 'dbo' -- Optional
)
        BEGIN

		
            DECLARE @SQL VARCHAR(MAX) = 'ALTER TABLE ' + @TableA + CHAR(13)
                + 'ADD CONSTRAINT FK' + @TableA + '_' + @TableA_Column
                + ' FOREIGN KEY (' + @TableA_Column + ') ' + 'REFERENCES '
                + @TableB + ' (' + ISNULL(@ForeignKey,'ID') + ')'
				+ (CASE WHEN @IsAggregrated = 1 THEN ' ON DELETE CASCADE ' ELSE '' END)


            EXEC(@SQL)
        END
GO
