﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tLocalShippingPermitIssuance](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ShippingShipper] [varchar](300) NULL,
	[ShippingAddress] [varchar](300) NULL,
	[ShippingCategory] [varchar](300) NULL,
	[ShippingSource] [varchar](300) NULL,
	[ShippingPurpose] [varchar](300) NULL,
	[ShippingProductSource] [varchar](300) NULL,
	[ShippingLicenceNumber] [varchar](300) NULL,
	[ShippingAccredationNumber] [varchar](300) NULL,
	[ShippingCountryOrigin] [varchar](300) NULL,
	[ProposedShippingDate] [datetime] NULL,
	[ProposedArrivalDate] [datetime] NULL,
	[OriginProvince] [varchar](300) NULL,
	[OriginMunicipality] [varchar](300) NULL,
	[OriginBarangay] [varchar](300) NULL,
	[OriginEstablishment] [varchar](300) NULL,
	[OriginFullAddress] [varchar](300) NULL,
	[OriginSender] [varchar](300) NULL,
	[OriginContactNumber] [varchar](300) NULL,
	[DestinationProvince] [varchar](300) NULL,
	[DestinationMunicipality] [varchar](300) NULL,
	[DestinationBarangay] [varchar](300) NULL,
	[DestinationEstablishment] [varchar](300) NULL,
	[DestinationFullAddress] [varchar](300) NULL,
	[DestinationSender] [varchar](300) NULL,
	[DestinationContactNumber] [varchar](300) NULL,
	[ModeOfTransfortationIsLand] [bit] NULL,
	[ModeOfTransfortationLandCarrierType] [varchar](300) NULL,
	[ModeOfTransfortationLandPlateNumber] [varchar](300) NULL,
	[ModeOfTransfortationIsAir] [bit] NULL,
	[ModeOfTransfortationAirCarrierType] [varchar](300) NULL,
	[ModeOfTransfortationAirPlateNumber] [varchar](300) NULL,
	[ModeOfTransfortationIsWater] [bit] NULL,
	[ModeOfTransfortationWaterCarrierType] [varchar](300) NULL,
	[ModeOfTransfortationWaterPlateNumber] [varchar](300) NULL,
	[ID_FilingStatus] [int] NULL,
 CONSTRAINT [PK_tLocalShippingPermitIssuance] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tLocalShippingPermitIssuance] ON [dbo].[tLocalShippingPermitIssuance]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tLocalShippingPermitIssuance] ADD  CONSTRAINT [DF__tLocalShi__IsAct__6C60F743]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tLocalShippingPermitIssuance]  WITH CHECK ADD  CONSTRAINT [FK_tLocalShippingPermitIssuance_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tLocalShippingPermitIssuance] CHECK CONSTRAINT [FK_tLocalShippingPermitIssuance_ID_Company]
GO
ALTER TABLE [dbo].[tLocalShippingPermitIssuance]  WITH CHECK ADD  CONSTRAINT [FK_tLocalShippingPermitIssuance_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tLocalShippingPermitIssuance] CHECK CONSTRAINT [FK_tLocalShippingPermitIssuance_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tLocalShippingPermitIssuance]  WITH CHECK ADD  CONSTRAINT [FK_tLocalShippingPermitIssuance_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tLocalShippingPermitIssuance] CHECK CONSTRAINT [FK_tLocalShippingPermitIssuance_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tLocalShippingPermitIssuance]
		ON [dbo].[tLocalShippingPermitIssuance]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tLocalShippingPermitIssuance
			SET    DateCreated = GETDATE()
			FROM   dbo.tLocalShippingPermitIssuance hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tLocalShippingPermitIssuance] ENABLE TRIGGER [rDateCreated_tLocalShippingPermitIssuance]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tLocalShippingPermitIssuance]
		ON [dbo].[tLocalShippingPermitIssuance]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tLocalShippingPermitIssuance
			SET    DateModified = GETDATE()
			FROM   dbo.tLocalShippingPermitIssuance hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tLocalShippingPermitIssuance] ENABLE TRIGGER [rDateModified_tLocalShippingPermitIssuance]
GO
