﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pGetUserRole]
    @ID INT = -1,
    @ID_Session INT = NULL
AS
BEGIN
    SELECT '_',
           '' AS UserRole_Detail,
           '' AS UserRole_Reports;

    DECLARE @ID_User INT,
            @ID_Warehouse INT;

    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM tUserSession
    WHERE ID = @ID_Session;

    IF (@ID = -1)
    BEGIN
        SELECT H.*
        FROM
        (
            SELECT NULL AS [_],
                   -1 AS [ID],
                   NULL AS [Code],
                   NULL AS [Name],
                   1 AS [IsActive],
                   NULL AS [Comment],
                   NULL AS [DateCreated],
                   NULL AS [DateModified],
                   NULL AS [ID_CreatedBy],
                   NULL AS [ID_LastModifiedBy],
                   NULL AS [Description]
        ) H
            LEFT JOIN dbo.tUser UC
                ON H.ID_CreatedBy = UC.ID
            LEFT JOIN dbo.tUser UM
                ON H.ID_LastModifiedBy = UM.ID;
    END;
    ELSE
    BEGIN
        SELECT H.*
        FROM dbo.vUserRole H
        WHERE H.ID = @ID;
    END;

    SELECT det.*,
           (CASE
                WHEN det.ID_Model = '-1' THEN
                    'All Modules'
                ELSE
                    ISNULL(tm.Caption, tm.Name)
            END
           ) AS ModelName,
           (CASE
                WHEN det.ID_Model = '-1' THEN
                    'All Modules'
                ELSE
                    ISNULL(tm.Caption, tm.Name)
            END
           ) AS Name_Model
    FROM dbo.tUserRole_Detail det
        LEFT JOIN dbo._tModel tm
            ON CAST(det.ID_Model AS VARCHAR(100)) = CAST(tm.Oid AS VARCHAR(100))
    WHERE ID_UserRole = @ID;

    SELECT det.*,
           (CASE
                WHEN det.ID_Report = '-1' THEN
                    'All Reports'
                ELSE
                    tm.Name
            END
           ) AS Name_Report
    FROM dbo.tUserRole_Reports det
        LEFT JOIN dbo._tReport tm
            ON CAST(det.ID_Report AS VARCHAR(100)) = CAST(tm.Oid AS VARCHAR(100))
    WHERE ID_UserRole = @ID;
END;
GO
