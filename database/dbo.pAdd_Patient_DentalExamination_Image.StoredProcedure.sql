﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      
Create PROC [dbo].[pAdd_Patient_DentalExamination_Image](@ID_Patient_DentalExamination int, @ImageValue Varchar(MAX), @Comment Varchar(MAX))        
AS        

	BEGIN TRANSACTION;   

	Declare @ID int	= 0
	Declare @Success bit = 0


	BEGIN TRY       

		Insert tPatient_DentalExamination_Image (ID_Patient_DentalExamination, ImageValue, Comment)
		Values(@ID_Patient_DentalExamination, @ImageValue, @Comment);

		SET @ID = @@IDENTITY;

		COMMIT TRANSACTION;   
		
		SET @Success = 1
        
	END TRY        
	BEGIN CATCH        
        
		ROLLBACK TRANSACTION;        
		
        SET @Success = 0
	END CATCH;   

	SELECT '_';    
        
	SELECT CONVERT(BIT, @Success) Success;    

GO
