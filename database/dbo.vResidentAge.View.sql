﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 VIew [dbo].[vResidentAge]
as
  select ID                                   ID_Resident,
         DATEDIFF(year, DateBirth, GETDATE()) Age,
         DateBirth
  FROM   tResident
  where  DateBirth IS NOT NULL

GO
