﻿CREATE TYPE [dbo].[typPatienDentalExamMedHistory] AS TABLE(
	[ID] [int] NULL,
	[ID_Patient_DentalExamination] [int] NULL,
	[ID_MedicalHistoryQuestionnaire] [int] NULL,
	[Comment] [varchar](max) NULL,
	[Answer] [varchar](max) NULL
)
GO
