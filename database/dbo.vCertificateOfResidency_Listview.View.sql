﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vCertificateOfResidency_Listview]
AS
  SELECT hed.ID,
         hed.Date,
         hed.Code,
         hed.Name,
         hed.Age,
         hed.Citizenship,
         hed.Address,
         hed.StayInDuration,
         hed.Requestor,
         hed.ID_Company,
         hed.IsActive
  FROM   vCertificateOfResidency hed

GO
