﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vResidentRequestTransaction_ListView]
AS
  SELECT hed.*,
         reRed.Date,
         reRed.Name    Name_ResidentRequest,
         reRed.Address Address_ResidentRequest,
         reRed.Purpose Purpose_ResidentRequest
  FROM   vResidentRequestTransaction hed
         INNER JOIN vResidentRequest reRed
                 ON hed.ID_ResidentRequest = reRed.ID 
GO
