﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[_fGetUserTypesParameters] ( @UserTypeID INT )
RETURNS VARCHAR(MAX)
AS
    BEGIN
        DECLARE @RETURN VARCHAR(MAX)
        SELECT  @RETURN = COALESCE(@RETURN + ' , ', '') + ( ColumnName + '$'
                                                            + CAST(TypeID AS VARCHAR(10)) )
        FROM    _vUserDefinedTypes
        WHERE   user_type_id = @UserTypeID
        ORDER BY ColumnId
        RETURN @RETURN
    END


GO
