﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

    
CREATE  
 VIEW [dbo].[vzResidentProgramReport]  
AS  
  SELECT hed.ID,  
         hed.Name,  
         hed.ID_Gender,  
         hed.Name_Gender,  
         hed.ContactNumber,
        dbo.fGetIntAge(hed.DateBirth)                           Age,
  REPLACE(rPrgString.ProgramString, ', ', '<br/>') ProgramString,  
         hed.ID_Company,  
         company.Name                                                                Name_Company,  
         company.ImageLogoLocationFilenamePath,  
         CASE  
           WHEN LEN(company.Address) > 0 THEN '' + company.Address  
           ELSE ''  
         END  
         + CASE  
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber  
             ELSE ''  
           END  
         + CASE  
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email  
             ELSE ''  
           END                                                                       HeaderInfo_Company  
  FROM   vResident hed  
      INNER JOIN vResidentProgramString rPrgString ON hed.ID = rPrgString.ID_Resident  
         LEFT JOIN dbo.vCompany company  
                ON company.ID = hed.ID_Company  
  WHERE  hed.IsActive = 1  
  
GO
