﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tDoc](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
 CONSTRAINT [PK_tDoc] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tDoc] ADD  CONSTRAINT [DF__tDoc__IsActive__444B1483]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tDoc]  WITH NOCHECK ADD  CONSTRAINT [FK_tDoc_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tDoc] CHECK CONSTRAINT [FK_tDoc_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tDoc]  WITH NOCHECK ADD  CONSTRAINT [FK_tDoc_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tDoc] CHECK CONSTRAINT [FK_tDoc_ID_LastModifiedBy]
GO
