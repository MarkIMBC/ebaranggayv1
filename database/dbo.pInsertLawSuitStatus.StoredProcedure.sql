﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pInsertLawSuitStatus](@LawSuitStatus VARCHAR(MAX),
                                @ID_Company    INT = 0)
as
  BEGIN
      IF(SELECT COUNT(*)
         FROM   tLawSuitStatus
         WHERE  Name = @LawSuitStatus) = 0
        BEGIN
            INSERT INTO [dbo].tLawSuitStatus
                        ([Name],
                         [IsActive],
                         ID_Company)
            VALUES      (@LawSuitStatus,
                         1,
                         @ID_Company)
        END
  END

GO
