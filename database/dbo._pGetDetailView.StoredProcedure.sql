﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[_pGetDetailView]
    @Oid UNIQUEIDENTIFIER
AS
    SELECT  '' AS [_]

    SELECT  H.Oid ,
            H.Code ,
            H.Name ,
            H.IsActive ,
            H.Comment ,
            H.ID_CreatedBy ,
            H.ID_LastModifiedBy ,
            H.DateCreated ,
            H.DateModified ,
            H.ID_Model ,
            ISNULL(H.Caption, ISNULL(M.Caption, M.Name)) AS Caption ,
            M.Name AS ModelName ,
            ISNULL(M.Icon, 'fa fa-cube') AS [Icon] ,
            M.Name AS ModelName ,
            M.ControllerPath AS ModelControllerPath ,
            M.PrimaryKey ,
            H.JsController ,
            H.Height ,
            H.Width ,
			ISNULL(M.IsEnableAuditTrail,0) AS IsEnableAuditTrail,
			ISNULL(M.IsEnableComment,0) AS IsEnableComment,
			ISNULL(M.IsReadOnly,0) AS IsReadOnly,
			ISNULL(M.IsEnableFileAttachment,0) AS IsEnableFileAttachment
    FROM    dbo.[_tDetailView] H
            LEFT JOIN dbo.[_vModel] M ON H.ID_Model = M.Oid
    WHERE   H.Oid = @Oid
GO
