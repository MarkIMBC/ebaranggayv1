﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fGetPaymentStatus] (@PayableAmount   DECIMAL(18, 4),
                                      @RemainingAmount DECIMAL(18, 4))
RETURNS INT
AS
  BEGIN
      DECLARE @ID_FillingStatus INT = 0;
      DECLARE @FILINGSTATUS_PENDING INT = 2;
      DECLARE @FILINGSTATUS_PARTIALLYPAID INT = 11;
      DECLARE @FILINGSTATUS_FULLYPAID INT = 12;
      DECLARE @FILINGSTATUS_DONE INT = 13;

      IF( @PayableAmount = 0 )
        BEGIN
            SET @ID_FillingStatus = @FILINGSTATUS_DONE;
        END
      ELSE
        BEGIN
            IF @PayableAmount = @RemainingAmount
              SET @ID_FillingStatus = @FILINGSTATUS_PENDING;
            ELSE IF @PayableAmount > @RemainingAmount
               AND @RemainingAmount <> 0
              SET @ID_FillingStatus = @FILINGSTATUS_PARTIALLYPAID;
            ELSE IF @RemainingAmount = 0
              SET @ID_FillingStatus = @FILINGSTATUS_FULLYPAID;
        END

      RETURN @ID_FillingStatus;
  END;

GO
