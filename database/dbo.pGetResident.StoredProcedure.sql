﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[pGetResident] @ID         INT = -1,
                             @ID_Session INT = NULL
AS
  BEGIN
      SELECT '_',
             '' AS Resident_Program,
             '' AS Resident_Family,
             '' AS Resident_MedicalRecord,
             '' AS Reports

      DECLARE @ID_User INT
      DECLARE @ID_Company INT
      DECLARE @ID_Warehouse INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse,
             @ID_Company = ID_Company
      FROM   tUserSession _session
             INNER JOIN vUSer _user
                     on _session.ID_User = _user.ID
      WHERE  _session.ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT NULL AS [_],
                           -1   AS [ID],
                           NULL AS [Code],
                           NULL AS [Name],
                           1    AS [IsActive],
                           NULL AS [ID_Company],
                           NULL AS [Comment],
                           NULL AS [DateCreated],
                           NULL AS [DateModified],
                           NULL AS [ID_CreatedBy],
                           NULL AS [ID_LastModifiedBy]) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tCompany company
                          ON company.ID = h.ID_Company
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vResident H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   vResident_Program
      WHERE  ID_Resident = @ID

      SELECT *
      FROM   vResident_Family
      WHERE  ID_Resident = @ID

      SELECT *
      FROM   vResident_MedicalRecord
      WHERE  ID_Resident = @ID
      Order  by ID DESC

      SELECT *
      FROm   vResidentDetailReportList
      order  by label
  END

GO
