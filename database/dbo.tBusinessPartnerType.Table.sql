﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tBusinessPartnerType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
 CONSTRAINT [PK_tBusinessPartnerType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tBusinessPartnerType] ADD  CONSTRAINT [DF__tBusiness__IsAct__015422C3]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tBusinessPartnerType]  WITH NOCHECK ADD  CONSTRAINT [FK_tBusinessPartnerType_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tBusinessPartnerType] CHECK CONSTRAINT [FK_tBusinessPartnerType_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tBusinessPartnerType]  WITH NOCHECK ADD  CONSTRAINT [FK_tBusinessPartnerType_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tBusinessPartnerType] CHECK CONSTRAINT [FK_tBusinessPartnerType_ID_LastModifiedBy]
GO
