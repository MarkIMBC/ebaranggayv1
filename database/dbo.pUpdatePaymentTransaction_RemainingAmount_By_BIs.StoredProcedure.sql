﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   PROC [dbo].[pUpdatePaymentTransaction_RemainingAmount_By_BIs]
(
    @IDs_BillingInvoice typIntList READONLY
)
AS
BEGIN

	DECLARE @maxID INT, @counter INT

	DECLARE @BillingInvoice TABLE(
		RowID int identity(1,1) primary key,
		ID_BillingInvoice Int
	)

	INSERT @BillingInvoice
		(ID_BillingInvoice)
	SELECT  ID
	FROM   @IDs_BillingInvoice 
	
	SET @counter = 1
	SELECT @maxID = COUNT(*) FROM @BillingInvoice

	WHILE (@counter <= @maxID)
	BEGIN

		DECLARE @ID_BillingInvoice INT = 0

		SELECT  
			@ID_BillingInvoice = ID_BillingInvoice
		FROM    @BillingInvoice 
		WHERE
			RowID = @counter

		exec dbo.[pUpdatePaymentTransaction_RemainingAmount_By_BI] @ID_BillingInvoice

		SET @counter = @counter + 1
	END
END

GO
