﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vzBillingInvoiceAgingReport]
AS
  SELECT biHed.ID,
         biHed.Date,
         biHed.Date                                                        Date_BillingInvoice,
         biHed.Code,
         biHed.ID_Client,
         LTRIM(RTRIM(ISNULL(biHed.Name_Client, biHed.WalkInCustomerName))) Name_Client,
         biHed.TotalAmount,
         biHed.RemainingAmount,
         biHed.Status,
         REPLACE(dbo.fGetAge(biHed.Date, GetDate(), ''), ' old', '')       Age,
         DATEDIFF(Day, biHed.Date, GetDate())                              AgeDays,
         company.ID                                                        ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.Name                                                      Name_Company,
         company.Address                                                   Address_Company,
         company.ContactNumber                                             ContactNumber_Company,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                                             HeaderInfo_Company
  FROM   vBillingInvoice biHed
         LEFT JOIN dbo.vCompany company
                ON company.ID = biHed.ID_Company
  WHERE  biHed.ID_FilingStatus IN ( 3 )
         and biHed.Payment_ID_FilingStatus IN ( 2, 11 )

GO
