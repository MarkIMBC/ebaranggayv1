﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 FUNCTION [dbo].[fGetCompany_CurrentSubscription](@ID_Company  INT)
RETURNS TABLE
    RETURN
      SELECT *
      FROM   dbo.fGetCompany_Subscription(@ID_Company, GETDATE()) 


GO
