﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   FUNCTION [dbo].[fGet_vzPatientSOAP_LaboratoryRemarks](  
    @ID_Patient_SOAP INT  
)  
RETURNS VARCHAR(MAX)  
AS   
BEGIN  
  
 DECLARE @Remarks VARCHAR(MAX) = '';    
  
 SELECT @Remarks = @Remarks + '<b>Image ' +  CONVERT(VARCHAR(MAX), RowIndex) + '</b>' + char(13) + '  ' + ISNULL(Remark, '') + char(13)  
 from   dbo.fGetPatient_SOAP_LaboratoryImages(@ID_Patient_SOAP)  
  
  
 SET @Remarks = REPLACE(@Remarks, CHAR(13), '<br/>')    
  
    RETURN @Remarks  
   
END;  
  
GO
