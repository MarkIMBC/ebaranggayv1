﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pGetDocumentSeries]
    @ID INT = -1,
    @ID_Session INT = NULL
AS
BEGIN
    SELECT '_';

    DECLARE @ID_User INT = 0,
            @ID_Warehouse INT = 0;

    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM dbo.tUserSession
    WHERE ID = @ID_Session;

    IF (@ID = -1)
    BEGIN
        SELECT H.*
        FROM
        (
            SELECT NULL AS [_],
                   -1 AS [ID],
                   '- New -' AS [Code],
                   NULL AS [Name],
                   1 AS [IsActive],
                   NULL AS [Comment],
                   NULL AS [DateCreated],
                   NULL AS [DateModified],
                   NULL AS [ID_CreatedBy],
                   NULL AS [ID_LastModifiedBy],
                   1 AS [Counter]
        ) H
            LEFT JOIN dbo.tUser UC
                ON H.ID_CreatedBy = UC.ID
            LEFT JOIN dbo.tUser UM
                ON H.ID_LastModifiedBy = UM.ID;
    END;
    ELSE
    BEGIN
        SELECT H.ID,
               H.Code,
               H.Name,
               H.IsActive,
               H.Comment,
               H.DateCreated,
               H.DateModified,
               H.ID_CreatedBy,
               H.ID_LastModifiedBy,
               H.ID_Model,
               H.Counter,
               H.Prefix,
               H.IsAppendCurrentDate,
               H.DigitCount,
               H.CreatedBy,
               H.LastModifiedBy
        FROM dbo.vDocumentSeries H
        WHERE H.ID = @ID;
    END;
END;
GO
