﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vsample]
as

SELECT a.ID,
                    a.Code,
                   a. Name,
                  a.  Comment,
                    FORMAT(ISNULL(a.UnitCost, 0), '#,###,##0.00') UnitCost,
					A.ID_ItemType
              FROM dbo.vActiveItem a

GO
