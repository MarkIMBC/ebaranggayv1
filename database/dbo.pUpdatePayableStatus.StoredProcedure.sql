﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pUpdatePayableStatus](@IDs_Payable typIntList READONLY)
AS
  BEGIN
      Declare @Payable table
        (
           ID_Payable    Int,
           PaymentAmount Decimal(18, 2)
        )

      INSERT @Payable
      SELECT ID,
             0
      FROM   @IDs_Payable


	  Update @Payable 
	  SET 
             PaymentAmount = ISNULL(tbl.PaymentAmount, 0)
      FROM   @Payable payable
             LEFT JOIN (SELECT pPay.ID_Payable,
                               SUM(ISNULL(pPay.TotalAmount, 0)) PaymentAmount
                        FROm   tPayablePayment pPay
                               JOIN @IDs_Payable idsPayable
                                 ON pPay.ID_Payable = idsPayable.ID
                        WHERE  pPay.ID_FilingStatus IN ( 3 )
                        GROUP  BY pPay.ID_Payable) tbl
                    ON tbl.ID_Payable = payable.ID_Payable

      Update tPayable
      SET    RemaningAmount = ISNULL(pAble.TotalAmount, 0) - ISNULL(payTotal.PaymentAmount, 0),
             PaidAmount = ISNULL(payTotal.PaymentAmount, 0)
      FROM   tPayable pAble
             inner JOIN @Payable payTotal
                     ON pAble.ID = payTotal.ID_Payable

      Update tPayable
      SET    Payment_ID_FilingStatus = dbo.fGetPaymentStatus(TotalAmount, RemaningAmount)
      FROM   tPayable pAble
             inner JOIN @Payable payTotal
                     ON pAble.ID = payTotal.ID_Payable
  END

GO
