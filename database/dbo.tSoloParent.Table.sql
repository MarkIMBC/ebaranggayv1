﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tSoloParent](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[Date] [datetime] NULL,
	[ID_RelationshipType] [int] NULL,
	[Age] [varchar](300) NULL,
	[ParentName] [varchar](300) NULL,
	[ParentAge] [varchar](300) NULL,
	[Purpose] [varchar](300) NULL,
	[Reason] [varchar](300) NULL,
 CONSTRAINT [PK_tSoloParent] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tSoloParent] ON [dbo].[tSoloParent]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tSoloParent] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tSoloParent]  WITH CHECK ADD  CONSTRAINT [FK_tSoloParent_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tSoloParent] CHECK CONSTRAINT [FK_tSoloParent_ID_Company]
GO
ALTER TABLE [dbo].[tSoloParent]  WITH CHECK ADD  CONSTRAINT [FK_tSoloParent_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tSoloParent] CHECK CONSTRAINT [FK_tSoloParent_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tSoloParent]  WITH CHECK ADD  CONSTRAINT [FK_tSoloParent_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tSoloParent] CHECK CONSTRAINT [FK_tSoloParent_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE TRIGGER [dbo].[rDateCreated_tSoloParent] ON [dbo].[tSoloParent] FOR INSERT
			AS
				DECLARE @ID INT
				SELECT @ID = ID FROM Inserted
				UPDATE dbo.tSoloParent SET DateCreated = GETDATE() WHERE ID = @ID
		
GO
ALTER TABLE [dbo].[tSoloParent] ENABLE TRIGGER [rDateCreated_tSoloParent]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE TRIGGER [dbo].[rDateModified_tSoloParent] ON [dbo].[tSoloParent] FOR UPDATE, INSERT
			AS
				DECLARE @ID INT
				SELECT @ID = ID FROM Inserted
				UPDATE dbo.tSoloParent SET DateModified = GETDATE() WHERE ID = @ID
		
GO
ALTER TABLE [dbo].[tSoloParent] ENABLE TRIGGER [rDateModified_tSoloParent]
GO
