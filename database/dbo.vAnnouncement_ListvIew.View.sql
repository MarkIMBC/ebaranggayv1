﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE     
 VIEW [dbo].[vAnnouncement_ListvIew]
AS
  SELECT ID,
         Name,
         DateStart,
         DateEnd,
         ID_Company,
         IsActive,
         DateCreated,
         DateModified
  FROM   vAnnouncement
  WHERE  IsActive = 1

GO
