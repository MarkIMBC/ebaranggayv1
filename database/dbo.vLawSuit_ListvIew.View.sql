﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vLawSuit_ListvIew]
as
  SELECT h.ID,
         Date,
         Complainant_ID_Resident,
         Complainant_Name_Resident,
         Respondent_ID_Resident,
         Respondent_Name_Resident,
         Name_FilingStatus,
         Name_LawSuitStatus,
         h.ID_Company,
         h.Comment,
         ID_LawSuitStatus,
         ComplainantName,
         RespondentName
  FROM   vLawSuit h
  where  ISNULL(h.IsActive, 0) = 1

GO
