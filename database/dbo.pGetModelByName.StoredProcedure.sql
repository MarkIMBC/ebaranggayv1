﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROC [dbo].[pGetModelByName](@name VARCHAR(300))
AS
BEGIN
	DECLARE @Oid_Model UNIQUEIDENTIFIER

	SELECT @Oid_Model = Oid FROM _tModel WHERE Name = @name

	exec [dbo].[pGetModel] @Oid_Model
END

GO
