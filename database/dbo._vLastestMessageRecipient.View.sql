﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[_vLastestMessageRecipient]
AS
SELECT DISTINCT
       Sender_ID_User,
       Recipient_ID_User,
       Recipient_Name_User,
       IsRead,
       MAX(LastDateSent) LastDateSent
FROM vOverallLatestMessageRecipient
GROUP BY Sender_ID_User,
         Recipient_ID_User,
         Recipient_Name_User,
         IsRead;


GO
