﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vSalesReturn]
AS
  SELECT H.*,
         UC.Name AS CreatedBy,
         UM.Name AS LastModifiedBy,
         fs.Name Name_FilingStatus
  FROM   tSalesReturn H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN dbo.tUser approvedUser
                ON H.ID_ApprovedBy = approvedUser.ID
         LEFT JOIN dbo.tUser cancelUser
                ON H.ID_CanceledBy = cancelUser.ID
         LEFT JOIN dbo.tFilingStatus fs
                ON fs.ID = H.ID_FilingStatus

GO
