﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[_tModelReport](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[Oid_Model] [varchar](300) NULL,
	[Oid_Report] [varchar](300) NULL,
 CONSTRAINT [PK__tModelReport] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX__tModelReport] ON [dbo].[_tModelReport]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[_tModelReport] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[_tModelReport]  WITH CHECK ADD  CONSTRAINT [FK__tModelReport_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[_tModelReport] CHECK CONSTRAINT [FK__tModelReport_ID_Company]
GO
ALTER TABLE [dbo].[_tModelReport]  WITH CHECK ADD  CONSTRAINT [FK__tModelReport_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[_tModelReport] CHECK CONSTRAINT [FK__tModelReport_ID_CreatedBy]
GO
ALTER TABLE [dbo].[_tModelReport]  WITH CHECK ADD  CONSTRAINT [FK__tModelReport_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[_tModelReport] CHECK CONSTRAINT [FK__tModelReport_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated__tModelReport]
		ON [dbo].[_tModelReport]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo._tModelReport
			SET    DateCreated = GETDATE()
			FROM   dbo._tModelReport hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[_tModelReport] ENABLE TRIGGER [rDateCreated__tModelReport]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified__tModelReport]
		ON [dbo].[_tModelReport]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo._tModelReport
			SET    DateModified = GETDATE()
			FROM   dbo._tModelReport hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[_tModelReport] ENABLE TRIGGER [rDateModified__tModelReport]
GO
