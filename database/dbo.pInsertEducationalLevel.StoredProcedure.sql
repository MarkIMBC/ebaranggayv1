﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  
CREATE     
 PROC [dbo].[pInsertEducationalLevel](@EducaltionalLevel VARCHAR(MAX), @ID_Company INT = 0)  
as  
  BEGIN  
      IF(SELECT COUNT(*)  
         FROM   tEducationalLevel  
         WHERE  Name = @EducaltionalLevel) = 0  
        BEGIN  
            INSERT INTO [dbo].tEducationalLevel  
                        ([Name],  
                         [IsActive], ID_Company)  
            VALUES      (@EducaltionalLevel,  
                         1, @ID_Company)  
        END  
  END   
GO
