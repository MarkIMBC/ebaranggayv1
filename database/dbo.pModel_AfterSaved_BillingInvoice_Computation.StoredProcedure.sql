﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pModel_AfterSaved_BillingInvoice_Computation](@IDs_BillingInvoice TYPINTLIST READONLY)
AS
  BEGIN
      DECLARE @ComputedBillingInvoice TABLE
        (
           RowIndex                 INT IDENTITY(1, 1),
           ID_BillingInvoice        INT,
           ID_TaxScheme             INT,
           IsComputeDiscountRate    BIT,
           ConfinementDepositAmount DECIMAL(18, 4),
           InitialSubtotalAmount    DECIMAL(18, 4),
           ConsumedDepositAmount    DECIMAL(18, 4),
           RemainingDepositAmount   DECIMAL(18, 4),
           SubTotal                 DECIMAL(18, 4),
           TotalAmount              DECIMAL(18, 4),
           DiscountRate             DECIMAL(18, 4),
           DiscountAmount           DECIMAL(18, 4),
           GrossAmount              DECIMAL(18, 4),
           VatAmount                DECIMAL(18, 4),
           NetAmount                DECIMAL(18, 4)
        )
      DECLARE @ComputedBillingInvoiceItems TABLE
        (
           ID_BillingInvoice        INT,
           ID_BillingInvoice_Detail INT,
           Quantity                 INT,
           UnitPrice                DECIMAL(18, 4),
           DiscountRate             DECIMAL(18, 4),
           DiscountAmount           DECIMAL(18, 4),
           IsComputeDiscountRate    BIT,
           Amount                   DECIMAL(18, 4)
        )

      INSERT @ComputedBillingInvoice
             (ID_BillingInvoice,
              ID_TaxScheme,
              IsComputeDiscountRate,
              DiscountRate,
              DiscountAmount,
              ConfinementDepositAmount)
      SELECT biHed.ID,
             biHed.ID_TaxScheme,
             bihed.IsComputeDiscountRate,
             biHed.DiscountRate,
             biHed.DiscountAmount,
             biHed.ConfinementDepositAmount
      FROM   dbo.tBillingInvoice biHed
             INNER JOIN @IDs_BillingInvoice ids
                     ON biHed.ID = ids.ID

      INSERT @ComputedBillingInvoiceItems
             (ID_BillingInvoice,
              ID_BillingInvoice_Detail,
              Quantity,
              UnitPrice,
              DiscountRate,
              DiscountAmount,
              IsComputeDiscountRate,
              Amount)
      SELECT ID_BillingInvoice,
             biDetail.ID,
             Quantity,
             UnitPrice,
             DiscountRate,
             DiscountAmount,
             IsComputeDiscountRate,
             Amount
      FROM   tBillingInvoice_Detail biDetail
             INNER JOIN @IDs_BillingInvoice ids
                     ON biDetail.ID_BillingInvoice = ids.ID

      -- Computed Billing Invoice Items 
      --     Amount 
      UPDATE @ComputedBillingInvoiceItems
      SET    Amount = CONVERT(DECIMAL(18, 4), Quantity) * IsNull(UnitPrice, 0)

      UPDATE @ComputedBillingInvoiceItems
      SET    Amount = Round(Amount, 2)

      --     Discount Amount ( IsComputeDiscountRate IS TRUE)  
      UPDATE @ComputedBillingInvoiceItems
      SET    DiscountAmount = Amount * ( DiscountRate / 100 )
      WHERE  IsComputeDiscountRate = 1

      UPDATE @ComputedBillingInvoiceItems
      SET    DiscountAmount = Round(DiscountAmount, 2)
      WHERE  IsComputeDiscountRate = 1

      --     Discount Amount ( IsComputeDiscountRate IS FALSE)  
      UPDATE @ComputedBillingInvoiceItems
      SET    DiscountRate = CASE
                              WHEN IsNull(Amount, 0) > 0 THEN ( DiscountAmount / Amount ) * 100
                              ELSE 0
                            END
      WHERE  IsNull(IsComputeDiscountRate, 0) = 0
             AND IsNull(Amount, 0) > 0

      UPDATE @ComputedBillingInvoiceItems
      SET    DiscountRate = Round(DiscountRate, 2)
      WHERE  IsNull(IsComputeDiscountRate, 0) = 0

      --    Recompute Amount (Less Discount Amount)
      UPDATE @ComputedBillingInvoiceItems
      SET    Amount = Amount - DiscountAmount

      UPDATE @ComputedBillingInvoiceItems
      SET    Amount = Round(Amount, 2)

      -- Computed Billing Invoice
      -- SubTotal
      UPDATE @ComputedBillingInvoice
      SET    SubTotal = b.TotalAmount 
      FROM   @ComputedBillingInvoice a
             INNER JOIN (SELECT computedBillingInvoice.ID_BillingInvoice,
                                Sum(Amount) TotalAmount
                         FROM   @ComputedBillingInvoice computedBillingInvoice
                                INNER JOIN @ComputedBillingInvoiceItems computedBillingInvoiceItems
                                        ON computedBillingInvoice.ID_BillingInvoice = computedBillingInvoiceItems.ID_BillingInvoice
                         GROUP  BY computedBillingInvoice.ID_BillingInvoice) b
                     ON a.ID_BillingInvoice = b.ID_BillingInvoice

      UPDATE @ComputedBillingInvoice
      SET    InitialSubtotalAmount = computedBI.InitialSubtotalAmount,
             ConsumedDepositAmount = computedBI.ConsumedDepositAmount,
             RemainingDepositAmount = computedBI.RemainingDepositAmount,
             SubTotal = computedBI.SubTotal,
             TotalAmount = computedBI.TotalAmount,
             DiscountRate = computedBI.DiscountRate,
             DiscountAmount = computedBI.DiscountAmount,
             GrossAmount = computedBI.GrossAmount,
             VatAmount = computedBI.VatAmount,
             NetAmount = computedBI.NetAmount
      FROM   @ComputedBillingInvoice a
             CROSS APPLY dbo.fGetBillingInvoiceComputatedColumns(a.SubTotal, a.DiscountRate, a.DiscountAmount, a.IsComputeDiscountRate, a.ID_TaxScheme, a.ConfinementDepositAmount) computedBI

      UPDATE tBillingInvoice_Detail
      SET    Quantity = computedBIItems.Quantity,
             UnitPrice = computedBIItems.UnitPrice,
             DiscountRate = computedBIItems.DiscountRate,
             DiscountAmount = computedBIItems.DiscountAmount,
             Amount = computedBIItems.Amount
      FROM   tBillingInvoice_Detail biDetail
             INNER JOIN @ComputedBillingInvoiceItems computedBIItems
                     ON biDetail.ID = computedBIItems.ID_BillingInvoice_Detail

      UPDATE tBillingInvoice
      SET    InitialSubtotalAmount = computedBI.InitialSubtotalAmount,
             ConsumedDepositAmount = computedBI.ConsumedDepositAmount,
             RemainingDepositAmount = computedBI.RemainingDepositAmount,
             SubTotal = computedBI.SubTotal,
             TotalAmount = computedBI.TotalAmount,
             DiscountRate = computedBI.DiscountRate,
             DiscountAmount = computedBI.DiscountAmount,
             GrossAmount = computedBI.GrossAmount,
             VatAmount = computedBI.VatAmount,
             NetAmount = computedBI.NetAmount
      FROM   tBillingInvoice bi
             INNER JOIN @ComputedBillingInvoice computedBI
                     ON bi.ID = computedBI.ID_BillingInvoice
  END

GO
