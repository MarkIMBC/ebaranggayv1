﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE 
 VIEW [dbo].[vClientCredit_ListView]
AS
  SELECT creditLogs.ID,
         creditLogs.Code,
         creditLogs.ID_Client,
         client.Name Name_Client,
         creditLogs.Date,
         creditLogs.Comment,
         creditLogs.CreditAmount,
         CASE
           WHEN ISNULL(creditLogs.CreditAmount, 0) > 0 THEN creditLogs.CreditAmount
           ELSE 0
         END         DepositAmount,
         CASE
           WHEN ISNULL(creditLogs.CreditAmount, 0) < 0 THEN abs(creditLogs.CreditAmount)
           ELSE 0
         END         WithdrawAmount
  FROM   vClient_CreditLogs creditLogs
         INNER JOIN tClient client
                 ON client.ID = creditLogs.ID_Client

GO
