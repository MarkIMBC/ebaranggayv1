﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 VIEW [dbo].[vEmployeeBarangayHealthWorker]
AS
  SELECT *
  FROM   vEmployeeActive
  WHERE  Name_Position IN ( 'Barangay Health Worker' )
         and ISNULL(IsActive, 0) = 1

GO
