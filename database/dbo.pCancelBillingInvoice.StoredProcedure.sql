﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pCancelBillingInvoice] (@IDs_BillingInvoice typIntList READONLY,
                                      @ID_UserSession     INT)
AS
  BEGIN
      DECLARE @Approved_ID_FilingStatus INT = 3
      DECLARE @Used_ID_FilingStatus INT = 17
      DECLARE @Inventoriable_ID_ItemType INT = 2;
      DECLARE @Canceled_ID_FilingStatus INT = 4;
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @ClientCredits typClientCredit

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pCancelBillingInvoice_validation
            @IDs_BillingInvoice,
            @ID_UserSession;

          INSERT @ClientCredits
          SELECT distinct bi.ID_Client,
                          bi.Date,
                          ( ISNULL(bi.ConfinementDepositAmount, 0) - ISNULL(bi.RemainingDepositAmount, 0) ),
                          bi.Code,
                          'Rollback Deposit '
          FROM   tBillingInvoice bi
                 inner join @IDs_BillingInvoice idsBI
                         on bi.ID = idsBI.ID
          WHERE  ISNULL(bi.ID_Patient_Confinement, 0) > 0
                 AND ID_FilingStatus = @Approved_ID_FilingStatus

          UPDATE dbo.tBillingInvoice
          SET    ID_FilingStatus = @Canceled_ID_FilingStatus,
                 DateCanceled = GETDATE(),
                 ID_CanceledBy = @ID_User
          FROM   dbo.tBillingInvoice bi
                 INNER JOIN @IDs_BillingInvoice ids
                         ON bi.ID = ids.ID;

          /*Inventory Trail */
          INSERT INTO tInventoryTrail
                      (Code,
                       ID_Company,
                       DateCreated,
                       ID_Item,
                       Quantity,
                       UnitPrice,
                       ID_FilingStatus,
                       Date,
                       DateExpired)
          SELECT hed.Code,
                 hed.ID_Company,
                 hed.DateCreated,
                 detail.ID_Item,
                 detail.Quantity,
                 detail.UnitPrice,
                 hed.ID_FilingStatus,
                 hed.Date,
                 detail.DateExpiration
          FROM   dbo.tBillingInvoice hed
                 LEFT JOIN dbo.tBillingInvoice_Detail detail
                        ON hed.ID = detail.ID_BillingInvoice
                 LEFT JOIN dbo.tItem item
                        ON item.ID = detail.ID_Item
                 INNER JOIN @IDs_BillingInvoice ids
                         ON hed.ID = ids.ID
          WHERE  ISNULL(hed.ID_ApprovedBy, 0) <> 0
                 AND item.ID_ItemType = @Inventoriable_ID_ItemType;

          EXEC dbo.pUpdateItemCurrentInventory;

          EXEC dbo.pUpdateBillingInvoicePayment
            @IDs_BillingInvoice;

          -- Rollback Deposit on Credit Logs  
          Declare @IDs_ClientDeposit typIntList

          INSERT @IDs_ClientDeposit
          SELECT cd.ID
          FROM   tBillingInvoice bi
                 inner join @IDs_BillingInvoice idsBI
                         on bi.ID = idsBI.ID
                 inner join tClientDeposit cd
                         on cd.ID_Patient_Confinement = bi.ID_Patient_Confinement
          WHERE  cd.ID_FilingStatus IN ( @Used_ID_FilingStatus )

          UPDATE tClientDeposit
          SET    ID_FilingStatus = @Approved_ID_FilingStatus
          WHERE  ID IN (SELECT ID
                        FROM   @IDs_ClientDeposit)

          exec pDoAdjustClientCredits
            @ClientCredits,
            @ID_UserSession
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO
