﻿CREATE TYPE [dbo].[ClientPet] AS TABLE(
	[Name] [varchar](300) NULL,
	[Address] [varchar](300) NULL,
	[Email] [varchar](300) NULL,
	[ContactNumber] [varchar](300) NULL,
	[PetName] [varchar](300) NULL,
	[Species] [varchar](300) NULL,
	[Birthday] [datetime] NULL
)
GO
