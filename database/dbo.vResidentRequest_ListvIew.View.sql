﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vResidentRequest_ListvIew]
AS
  SELECT ID,
         Name,
         ContactNumber,
         Address,
         Purpose,
         ID_Company,
         IsActive,
         DateCreated,
         DateModified
  FROM   vResidentRequest 
GO
