﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE    
 VIEW [dbo].[vzResidentFormReport]
AS
  SELECT Resident.Name                                           Name,
         Resident.ID                                             ID,
         Resident.Address                                        Address_Resident,
         ISNULL(civilStatus.TranslatedEngName, '')               TranslatedEngName_CivilStatus,
         ISNULL(FORMAT(DateBirth, 'MMMM dd, yyyy'), '')          FormatBirthDate_Resident,
         dbo.fGetYearAge(DateBirth, GETDATE(), '')               Age_Resident,
         company.ID                                              ID_Company,
         company.ImageLogoLocationFilenamePath                   ImageLogoLocationFilenamePath,
         company.Name                                            Name_Company,
         company.Address                                         Address_Company,
         company.ContactNumber                                   ContactNumber_Company,
         company.BarangayListSideReportHTMLString                BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                             BarangayCaptainName_Company,
         Resident.Name_CivilStatus                               Name_CivilStatus,
         company.BarangayCaptain_Name_Employee                   BarangayCaptain_Name_Employee,
         company.BarangayCaptain_ImageSignitureSpecimen_Employee BarangayCaptain_ImageSignitureSpecimen_Employee,
         municipality.Name                                       Name_MunicipalityCompany,
         municipality.ImageLogoLocationFilenamePath              ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                                    Address_MunicipalityCompany,
         Resident.Name_Gender,
         dbo.fGetAPILink() + '/Content/Thumbnail/'
         + Resident.ImageProfilePicFilename                      ImageProfilePicFilename
  FROM   vResident Resident
         INNER JOIN vCompanyActive company
                 ON company.ID = Resident.ID_Company
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = Resident.ID_Company
         Left JOIN tCivilStatus civilStatus
                on Resident.ID_CivilStatus = civilStatus.ID

GO
