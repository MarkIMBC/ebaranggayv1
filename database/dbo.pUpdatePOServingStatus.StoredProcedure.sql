﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pUpdatePOServingStatus](@IDs_PurchaseOrder typIntList READONLY)
AS
BEGIN

	DECLARE @Approved_ID_FilingStatus INT = 3

	DECLARE @rrDetail TABLE (
		ID_PurchaseOrder_Detail INT,
		Quantity INT
	)

	INSERT @rrDetail 
	(	
		ID_PurchaseOrder_Detail, 
		Quantity
	)
	SELECT 
		rrDetail.ID_PurchaseOrder_Detail, 
		rrDetail.Quantity
	FROM dbo.tReceivingReport rrHed 
	INNER JOIN dbo.tReceivingReport_Detail rrDetail 
		on rrHed.ID = rrDetail.ID_ReceivingReport
	INNER JOIN @IDs_PurchaseOrder idsPO 
		ON idsPO.ID = rrHed.ID_PurchaseOrder
	WHERE rrHed.ID_FilingStatus = @Approved_ID_FilingStatus

	-- Update Remaining Quantity --
	UPDATE dbo.tPurchaseOrder_Detail 
	SET RemainingQuantity = poDetail.Quantity - ISNULL(tbl.Quantity,0)
	FROM tPurchaseOrder_Detail poDetail 
	LEFT JOIN (
		SELECT 
			ID_PurchaseOrder_Detail, 
			SUM(Quantity) Quantity 
		FROM @rrDetail
		GROUP BY ID_PurchaseOrder_Detail
	) tbl 
		ON poDetail.ID = tbl.ID_PurchaseOrder_Detail

	-- Update Serving Status --
	UPDATE tPurchaseOrder
	SET ServingStatus_ID_FilingStatus = tbl.ServingStatus_ID_FilingStatus
	FROM dbo.tPurchaseOrder poHed inner JOIN 
	(
		SELECT 
			ID_PurchaseOrder, 
			dbo.fGetServingStatusIDByRemQuantity(SUM(Quantity), SUM(RemainingQuantity)) ServingStatus_ID_FilingStatus
		FROM dbo.tPurchaseOrder_Detail poDetail
		INNER JOIN @IDs_PurchaseOrder idsPO 
			ON idsPO.ID = poDetail.ID_PurchaseOrder
		GROUP BY ID_PurchaseOrder
	) tbl 
		ON poHed.ID = tbl.ID_PurchaseOrder

END

GO
