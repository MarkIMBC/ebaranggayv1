﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pValidate_AdjustClientCredits](@ClientCredits  typClientCredit READONLY,
                                  @ID_UserSession INT)
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @isValid BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      SELECT @ID_User = ID_User
      FROM   tUserSession
      WHERE  ID = @ID_UserSession

      BEGIN TRY
          EXEC pAdjustClientCredits_validation
            @ClientCredits,
            @ID_UserSession 
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @isValid = 0;

      END CATCH
	  
      SELECT '_';

      SELECT @isValid isValid,
             @message message;
  END

GO
