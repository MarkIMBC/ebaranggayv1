﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vResidentRequest_CommunityTaxCertificate]
AS
  SELECT H.*,
         ResidentRequest.Name Name_ResidentRequest,
         UC.NAME              AS CreatedBy,
         UM.NAME              AS LastModifiedBy
  FROM   tResidentRequest_CommunityTaxCertificate H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tResidentRequest ResidentRequest
                ON ResidentRequest.ID = h.ID_ResidentRequest

GO
