﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE       
 VIEW [dbo].[vResident_Listview_HeadOfFamily]  
AS  
  SELECT hed.ID,  
         hed.Name,  
         hed.ID_Gender,  
         hed.Name_Gender,  
         hed.ContactNumber,  
         hed.Address,  
         hed.ID_Company,  
         hed.IsActive,  
         hed.DateCreated,  
         hed.DateModified,  
         hed.IsVaccinated,  
         company.Guid Guid_Company,  
         ID_HouseholdNumber  , AssignedBHW_Name_Employee
  FROM   vResident hed  
         LEFT JOIN tCompany company  
                on company.ID = hed.ID_Company  
  WHERE  hed.IsActive = 1  
         AND ISNULL(hed.IsHeadofFamily, 0) = 1  
         ANd ISNULL(IsDeceased, 0) = 0  
GO
