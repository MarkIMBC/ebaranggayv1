﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[_pSetNewServiceBroker]  
AS  
BEGIN  
    DECLARE @dbName  AS VARCHAR(MAX)= ''  
  
 SET @dbName = DB_NAME()   
  
 EXEC ('  
   
  USE master  
  ALTER DATABASE '+ @dbName +' SET NEW_BROKER WITH ROLLBACK IMMEDIATE;  

 ')  
END  

GO
