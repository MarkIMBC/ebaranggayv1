﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SqlQueryNotificationStoredProcedure-847aa9e0-ead5-4619-8b3d-3d5975b206ce] AS BEGIN BEGIN TRANSACTION; RECEIVE TOP(0) conversation_handle FROM [SqlQueryNotificationService-847aa9e0-ead5-4619-8b3d-3d5975b206ce]; IF (SELECT COUNT(*) FROM [SqlQueryNotificationService-847aa9e0-ead5-4619-8b3d-3d5975b206ce] WHERE message_type_name = 'http://schemas.microsoft.com/SQL/ServiceBroker/DialogTimer') > 0 BEGIN if ((SELECT COUNT(*) FROM sys.services WHERE name = 'SqlQueryNotificationService-847aa9e0-ead5-4619-8b3d-3d5975b206ce') > 0)   DROP SERVICE [SqlQueryNotificationService-847aa9e0-ead5-4619-8b3d-3d5975b206ce]; if (OBJECT_ID('SqlQueryNotificationService-847aa9e0-ead5-4619-8b3d-3d5975b206ce', 'SQ') IS NOT NULL)   DROP QUEUE [SqlQueryNotificationService-847aa9e0-ead5-4619-8b3d-3d5975b206ce]; DROP PROCEDURE [SqlQueryNotificationStoredProcedure-847aa9e0-ead5-4619-8b3d-3d5975b206ce]; END COMMIT TRANSACTION; END
GO
