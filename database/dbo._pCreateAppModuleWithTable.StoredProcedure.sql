﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[_pCreateAppModuleWithTable] @TableName VARCHAR(100), @IsCreateNavigation BIT, @IsDetail BIT, @ID_Parent UNIQUEIDENTIFIER
  AS
  BEGIN
    DECLARE @IsCreateProc BIT = 1

  EXEC _pCreateTable @TableName

  EXEC _pCreateAppModule @TableName, @IsCreateProc, @IsCreateNavigation

  IF ( @ID_Parent IS NOT NULL AND @IsCreateNavigation = 1)
    BEGIN
      UPDATE N SET ID_Parent = @ID_Parent  FROM _tNavigation N INNER JOIN _tView V ON N.ID_View = V.Oid 
        INNER JOIN _tModel M ON V.ID_Model = M.Oid
        WHERE M.TableName = @TableName
    END

  EXEC _pFixDefaultProperties
  END
GO
