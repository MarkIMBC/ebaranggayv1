﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vNonSystemUseEmployee]
AS
  SELECT H.*
  FROM   vEmployee H
  WHERE  IsNull(IsSystemUsed, 0) = 0
         AND h.FirstName NOT IN ( 'Demo Sole', 'Receptionportal' )
         AND IsActive = 1 
GO
