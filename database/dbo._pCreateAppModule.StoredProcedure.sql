﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[_pCreateAppModule] @TableName VARCHAR(MAX),
@IsCreateProc BIT = 0,
@IsCreateNavigation BIT = 1
AS
	BEGIN TRANSACTION;
	BEGIN TRY

		SET NOCOUNT ON

		IF NOT EXISTS (SELECT
					object_id
				FROM sys.tables
				WHERE Name = @TableName)
			RETURN
		IF EXISTS (SELECT
					Oid
				FROM dbo.[_tModel]
				WHERE TableName = @TableName)
			RETURN

		DECLARE @Oid_Model UNIQUEIDENTIFIER = NEWID()
			   ,@ModelName VARCHAR(500) = NULL

		SELECT
			@ModelName = (CASE
				WHEN SUBSTRING(@TableName, 1, 1) = '_' THEN SUBSTRING(@TableName, 3,
					LEN(@TableName))
				ELSE SUBSTRING(@TableName, 2,
					LEN(@TableName))
			END)


		IF (@IsCreateProc = 1)
		BEGIN
			DECLARE @SQL VARCHAR(MAX) = dbo.[_fSQLPGetScript](@TableName)
			EXEC (@SQL)
		END

		DECLARE @ViewSource VARCHAR(500) = NULL

		SELECT
			@ViewSource = Name
		FROM sys.views
		WHERE Name = 'v' + @ModelName

		INSERT INTO dbo.[_tModel] (Oid,
		Name,
		IsActive,
		TableName,
		DateCreated,
		ID_CreatedBy,
		ViewSource)
			VALUES (@Oid_Model, @ModelName, 1, @TableName, GETDATE(), 1, @ViewSource)

		INSERT INTO dbo.[_tModel_Property] (Oid,
		Name,
		IsActive,
		Caption,
		ID_Model,
		ID_PropertyType,
		DateCreated,
		ID_CreatedBy)
			SELECT
				NEWID()
			   ,c.Name
			   ,1
			   ,NULL
			   ,@Oid_Model
			   ,(CASE t.system_type_id
					WHEN 35 THEN 1 --STRING
					WHEN 36 THEN 1
					WHEN 99 THEN 1 --STRING
					WHEN 167 THEN 1	--STRING
					WHEN 175 THEN 1	--STRING
					WHEN 231 THEN 1 --STRING
					WHEN 239 THEN 1	--STRING
					WHEN 106 THEN 3 -- DECIMAL
					WHEN 40 THEN 5 --DATE
					WHEN 61 THEN 5 --DATE
					WHEN 104 THEN 4 --BOOL
					WHEN 56 THEN 2 --INT
					ELSE 0
				END)
			   ,GETDATE()
			   ,1
			FROM sys.columns c
			LEFT JOIN sys.types t
				ON c.system_type_id = t.system_type_id
			WHERE OBJECT_NAME(c.object_id) = @TableName

		--
		--	LISTVIEW
		--
		DECLARE @Oid_ListView UNIQUEIDENTIFIER = NEWID()

		DECLARE @DataSource VARCHAR(1000) = 'SELECT * FROM ' + @TableName

		IF EXISTS (SELECT
					Name
				FROM sys.views
				WHERE Name = 'v' + @ModelName)
		BEGIN
			SET @DataSource = 'SELECT * FROM dbo.v' + @ModelName
		END


		INSERT INTO dbo.[_tListView] (Oid,
		Code,
		Name,
		IsActive,
		Comment,
		ID_CreatedBy,
		DateCreated,
		ID_Model,
		datasource)
			VALUES (@Oid_ListView, NULL, @ModelName + '_ListView', 1, NULL, 1, GETDATE(), @Oid_Model, @DataSource)

		INSERT INTO dbo.[_tListView_Detail] (Oid,
		Code,
		Name,
		IsActive,
		Comment,
		ID_ListView,
		DateCreated,
		ID_CreatedBy,
		ID_ModelProperty,
		IsVisible,
		VisibleIndex)
			SELECT
				NEWID()
			   ,NULL
			   ,P.Name
			   ,1
			   ,NULL
			   ,@Oid_ListView
			   ,GETDATE()
			   ,1
			   ,P.Oid
			   ,1
			   ,ROW_NUMBER() OVER (PARTITION BY M.Oid ORDER BY P.DateCreated ASC) * 100
			FROM dbo.[_tModel_Property] P
			LEFT JOIN dbo.[_tModel] M
				ON P.ID_Model = M.Oid
			WHERE M.Oid = @Oid_Model

		DECLARE @Oid_DetailView UNIQUEIDENTIFIER = NEWID()

		INSERT INTO dbo.[_tDetailView] (Oid,
		Code,
		Name,
		IsActive,
		Comment,
		ID_CreatedBy,
		DateCreated,
		ID_Model)
			VALUES (@Oid_DetailView, NULL, @ModelName + '_DetailView', 1, NULL, 1, GETDATE(), @Oid_Model)

		UPDATE dbo.[_tModel]
		SET ID_DetailView = @Oid_DetailView
		WHERE Oid = @Oid_Model

		DECLARE @OID_GENERAL_TAB UNIQUEIDENTIFIER = NEWID()
			   ,@OID_STATUS_TAB UNIQUEIDENTIFIER = NULL
			   ,@OID_COMMENT_TAB UNIQUEIDENTIFIER = NULL

		INSERT INTO dbo.[_tDetailView_Detail] (Oid,
		Code,
		Name,
		IsActive,
		Comment,
		Caption,
		ID_ModelProperty,
		ID_DetailView,
		ID_CreatedBy,
		ID_LastModifiedBy,
		DateCreated,
		DateModified,
		ID_ControlType,
		SeqNo)
			VALUES (@OID_GENERAL_TAB, NULL, 'GeneralTab', 1, NULL, 'General', NULL, @Oid_DetailView, 1, NULL, GETDATE(), NULL, 12, 100)

		DECLARE @INFO_SECTION_ID UNIQUEIDENTIFIER = NEWID()
		INSERT INTO dbo.[_tDetailView_Detail] (Oid,
		Code,
		Name,
		IsActive,
		Comment,
		Caption,
		ID_ModelProperty,
		ID_DetailView,
		ID_CreatedBy,
		ID_LastModifiedBy,
		DateCreated,
		DateModified,
		ID_ControlType,
		SeqNo,
		ID_Tab,
		ColCount,
		ColSpan)
			VALUES (@INFO_SECTION_ID, NULL, 'InfoSection', 1, NULL, 'Information', NULL, @Oid_DetailView, 1, NULL, GETDATE(), NULL, 13, 100, @OID_GENERAL_TAB, 1, 3)

		IF EXISTS (SELECT
					*
				FROM sys.columns
				WHERE OBJECT_NAME(object_id) = @TableName
				AND (Name LIKE '%ID_CreatedBy%'
				OR Name LIKE '%ID_LastModifiedBy%'
				))
		BEGIN
			SET @OID_STATUS_TAB = NEWID()
			INSERT INTO dbo.[_tDetailView_Detail] (Oid,
			Code,
			Name,
			IsActive,
			Comment,
			Caption,
			ID_ModelProperty,
			ID_DetailView,
			ID_CreatedBy,
			ID_LastModifiedBy,
			DateCreated,
			DateModified,
			ID_ControlType,
			SeqNo)
				VALUES (@OID_STATUS_TAB, NULL, 'StatusTab', 1, NULL, 'Status', NULL, @Oid_DetailView, 1, NULL, GETDATE(), NULL, 12, 200)
		END

		IF EXISTS (SELECT
					*
				FROM sys.columns
				WHERE OBJECT_NAME(object_id) = @TableName
				AND (Name LIKE '%Comment%'))
		BEGIN
			SET @OID_COMMENT_TAB = NEWID()
			INSERT INTO dbo.[_tDetailView_Detail] (Oid,
			Code,
			Name,
			IsActive,
			Comment,
			Caption,
			ID_ModelProperty,
			ID_DetailView,
			ID_CreatedBy,
			ID_LastModifiedBy,
			DateCreated,
			DateModified,
			ID_ControlType,
			SeqNo)
				VALUES (@OID_COMMENT_TAB, NULL, 'CommentTab', 1, NULL, 'Comment', NULL, @Oid_DetailView, 1, NULL, GETDATE(), NULL, 12, 300)
		END

		DECLARE @STATUS_SECTION_A UNIQUEIDENTIFIER = NEWID()
		INSERT INTO dbo.[_tDetailView_Detail] (Oid,
		Code,
		Name,
		IsActive,
		Comment,
		Caption,
		ID_ModelProperty,
		ID_DetailView,
		ID_CreatedBy,
		ID_LastModifiedBy,
		DateCreated,
		DateModified,
		ID_ControlType,
		SeqNo,
		ID_Tab,
		ColCount,
		ColSpan)
			VALUES (@STATUS_SECTION_A, NULL, 'StatusSectionA', 1, NULL, 'StatusSectionA', NULL, @Oid_DetailView, 1, NULL, GETDATE(), NULL, 13, 100, @OID_STATUS_TAB, 1, 5)

		DECLARE @STATUS_SECTION_B UNIQUEIDENTIFIER = NEWID()
		INSERT INTO dbo.[_tDetailView_Detail] (Oid,
		Code,
		Name,
		IsActive,
		Comment,
		Caption,
		ID_ModelProperty,
		ID_DetailView,
		ID_CreatedBy,
		ID_LastModifiedBy,
		DateCreated,
		DateModified,
		ID_ControlType,
		SeqNo,
		ID_Tab,
		ColCount,
		ColSpan)
			VALUES (@STATUS_SECTION_B, NULL, 'StatusSectionB', 1, NULL, 'StatusSectionB', NULL, @Oid_DetailView, 1, NULL, GETDATE(), NULL, 13, 200, @OID_STATUS_TAB, 1, 5)


		DECLARE @ID_LabelPosition_Top INT = 2

		INSERT INTO dbo.[_tDetailView_Detail] (Oid,
		Code,
		Name,
		IsActive,
		Comment,
		Caption,
		ID_ModelProperty,
		ID_DetailView,
		ID_CreatedBy,
		ID_LastModifiedBy,
		DateCreated,
		DateModified,
		ID_Tab,
		ID_Section,
		ID_PropertyType,
		ColSpan,
		ID_LabelLocation)
			SELECT
				NEWID()
			   ,NULL
			   ,P.Name
			   ,1
			   ,NULL
			   ,NULL
			   ,P.Oid
			   ,@Oid_DetailView
			   ,1
			   ,NULL
			   ,GETDATE()
			   ,NULL
			   ,(CASE P.Name
					WHEN 'ID_CreatedBy' THEN NULL
					WHEN 'ID_LastModifiedBy' THEN NULL
					WHEN 'DateCreated' THEN NULL
					WHEN 'DateModified' THEN NULL
					WHEN 'ID_FilingStatus' THEN NULL
					WHEN 'Comment' THEN @OID_COMMENT_TAB
					ELSE NULL
				END)
			   ,(CASE P.Name
					WHEN 'ID_CreatedBy' THEN @STATUS_SECTION_A
					WHEN 'ID_LastModifiedBy' THEN @STATUS_SECTION_B
					WHEN 'DateCreated' THEN @STATUS_SECTION_A
					WHEN 'DateModified' THEN @STATUS_SECTION_B
					WHEN 'ID_FilingStatus' THEN @STATUS_SECTION_A
					WHEN 'Comment' THEN NULL
					WHEN 'ID_Company' THEN @STATUS_SECTION_A
					ELSE @INFO_SECTION_ID
				END)
			   ,P.ID_PropertyType
			   ,(CASE
					WHEN P.Name = 'Comment' THEN 12
					ELSE 1
				END)
			   , --ColSpan
				(CASE
					WHEN P.Name = 'Comment' THEN @ID_LabelPosition_Top
					ELSE NULL
				END) --ID_Label_Location
			FROM dbo.[_tModel_Property] P
			LEFT JOIN dbo.[_tModel] M
				ON P.ID_Model = M.Oid
			WHERE M.Oid = @Oid_Model

		--
		--
		--
		DECLARE @ID_View UNIQUEIDENTIFIER = NEWID()
		INSERT INTO dbo.[_tView] (Oid,
		Code,
		Name,
		IsActive,
		ID_Model,
		ID_ListView,
		ID_Report,
		ID_Dashboard,
		ID_ViewType,
		CustomViewPath,
		ControllerPath,
		Comment,
		DateCreated,
		ID_CreatedBy,
		ID_LastModifiedBy,
		DateModified)
			SELECT TOP 1
				@ID_View
			   ,NULL
			   ,Name
			   ,1
			   ,@Oid_Model
			   ,@Oid_ListView
			   ,NULL
			   ,NULL
			   ,1
			   ,NULL
			   ,NULL
			   ,NULL
			   ,GETDATE()
			   ,1
			   ,NULL
			   ,NULL
			FROM dbo.[_tListView]
			WHERE Oid = @Oid_ListView

		IF @IsCreateNavigation = 1
		BEGIN
			INSERT INTO dbo.[_tNavigation] (Oid,
			Code,
			Name,
			IsActive,
			Comment,
			ID_CreatedBy,
			ID_LastModifiedBy,
			DateCreated,
			DateModified,
			ID_View)
				VALUES (NEWID(), -- Oid - uniqueidentifier
				NULL, -- Code - varchar(50)
				@ModelName + '_ListView', -- Name - varchar(200)
				1, -- IsActive - bit
				NULL, -- Comment - varchar(200)
				1, -- ID_CreatedBy - int
				NULL, -- ID_LastModifiedBy - int
				GETDATE(), -- DateCreated - datetime
				GETDATE(), -- DateModified - datetime
				@ID_View  -- ID_View - uniqueidentifier
				)
		END


		PRINT @ModelName + ' Model Successfully Created'
		SET NOCOUNT OFF

	END TRY
	BEGIN CATCH
		SELECT
			ERROR_NUMBER() AS ErrorNumber
		   ,ERROR_SEVERITY() AS ErrorSeverity
		   ,ERROR_STATE() AS ErrorState
		   ,ERROR_PROCEDURE() AS ErrorProcedure
		   ,ERROR_LINE() AS ErrorLine
		   ,ERROR_MESSAGE() AS ErrorMessage;

		IF @@trancount > 0
			ROLLBACK TRANSACTION;
	END CATCH;


	IF @@trancount > 0
		COMMIT TRANSACTION;

GO
