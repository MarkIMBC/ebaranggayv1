﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pUpdatePatientsLastVisitedDate](@IDs_Patient typIntList READONLY)
AS
  BEGIN
      DECLARE @tbl TABLE
        (
           ID_Patient      INT,
           DateLastVisited DATETIME
        )

      INSERT @tbl
             (ID_Patient)
      SELECT ID
      FROM   @IDs_Patient

      UPDATE @tbl
      SET    DateLastVisited = rec.DateLastVisited
      FROM   @tbl tbl
             inner join (SELECT MAX(Date) DateLastVisited,
                                soap.ID_Patient
                         FROM   tPatient_SOAP soap
                                INNER JOIN @tbl tbl
                                        ON tbl.ID_Patient = soap.ID_Patient
                         WHERE  ID_FilingStatus IN ( 1, 3, 13 )
                         GROUP  BY soap.ID_Patient) rec
                     ON rec.ID_Patient = tbl.ID_Patient

      UPDATE tPatient
      SET    DateLastVisited = tbl.DateLastVisited
      FROM   tPatient patient
             INNER JOIN @tbl tbl
                     ON tbl.ID_Patient = patient.ID

      Update tClient
      SET    DateLastVisited = tbl.DateLastVisited
      FROM   tClient client
             INNER JOIN (SELECT ID_Client,
                                MAX(patient.DateLastVisited) DateLastVisited
                         FROM   tPatient patient
                                INNER JOIN @tbl tbl
                                        ON tbl.ID_Patient = patient.ID
                         GROUP  BY ID_Client) tbl
                     ON client.ID = tbl.ID_Client
  END

GO
