﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pApproveTextBlast] (@IDs_TextBlast  typIntList READONLY,
                                      @ID_UserSession INT)
AS
  BEGIN
      DECLARE @Approved_ID_FilingStatus INT = 3;
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @IDs_Patient typIntList
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;
          DECLARE @NoAssignedClient_IDs_TextBlast TABLE
            (
               ID_TextBlast INT,
               ClientCount  INT
            )

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pApproveTextBlast_validation
            @IDs_TextBlast,
            @ID_UserSession;

          UPDATE dbo.tTextBlast
          SET    ID_FilingStatus = @Approved_ID_FilingStatus,
                 DateApproved = GETDATE(),
                 ID_ApprovedBy = @ID_User
          FROM   dbo.tTextBlast bi
                 INNER JOIN @IDs_TextBlast ids
                         ON bi.ID = ids.ID;

          -- Insert all Active Client to No Assigned Client IN TextBlast Record
          INSERT @NoAssignedClient_IDs_TextBlast
          SELECT ID,
                 0
          FROM   @IDs_TextBlast

          DELETE FROM @NoAssignedClient_IDs_TextBlast
          WHERE  ID_TextBlast IN (SELECT ID_TextBlast
                                  FROM   tTextBlast_Client textBlastClient
                                         INNER JOIN @IDs_TextBlast ids
                                                 ON textBlastClient.ID_TextBlast = ids.ID
                                  GROUP  BY textBlastClient.ID_TextBlast
                                  HAVING Count(*) > 0)

          INSERT INTO [dbo].[tTextBlast_Client]
                      ([IsActive],
                       [DateCreated],
                       [DateModified],
                       [ID_CreatedBy],
                       [ID_LastModifiedBy],
                       [ID_Company],
                       [ID_TextBlast],
                       [ID_Client])
          SELECT 1,
                 GETDATE(),
                 GETDATE(),
                 1,
                 1,
                 smsBlast.ID_Company,
                 smsBlast.ID ID_TextBlast,
                 client.ID   ID_Client
          FROM   tClient client
                 INNER JOIN tTextBlast smsBlast
                         on client.ID_Company = smsBlast.ID_Company
          WHERE  smsBlast.ID IN (SELECT ID_TextBlast
                                 FROM   @NoAssignedClient_IDs_TextBlast)
                 and client.IsActive = 1
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO
