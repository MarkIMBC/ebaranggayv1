﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vResidentRequest_CommunityTaxCertificate_ListvIew]
AS
  SELECT ID,
         Name,
         CompanyName,
         PositionName,
         AnnualIncomeAmount,
         ID_ResidentRequest,
         Name_ResidentRequest,
         ID_Company,
         IsActive,
         DateCreated,
         DateModified
  FROM   vResidentRequest_CommunityTaxCertificate
  WHERE  IsActive = 1

GO
