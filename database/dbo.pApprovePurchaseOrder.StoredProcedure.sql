﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pApprovePurchaseOrder]
(
    @IDs_PurchaseOrder typIntList READONLY,
    @ID_UserSession INT
)
AS
BEGIN

    DECLARE @Approved_ID_FilingStatus INT = 3;
    DECLARE @Pending_ID_FilingStatus INT = 2;

    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';

    BEGIN TRY

        DECLARE @ID_User INT = 0;

        SELECT @ID_User = ID_User
        FROM dbo.tUserSession
        WHERE ID = @ID_UserSession;

        EXEC dbo.pApprovePurchaseOrder_validation @IDs_PurchaseOrder,
                                                  @ID_UserSession;

        UPDATE dbo.tPurchaseOrder
        SET ID_FilingStatus = @Approved_ID_FilingStatus,
            DateApproved = GETDATE(),
            ID_ApprovedBy = @ID_User
        FROM dbo.tPurchaseOrder bi
            INNER JOIN @IDs_PurchaseOrder ids
                ON bi.ID = ids.ID;
        
        EXEC dbo.pUpdatePOServingStatus @IDs_PurchaseOrder;

    END TRY
    BEGIN CATCH

        SET @message = ERROR_MESSAGE();
        SET @Success = 0;

    END CATCH;


    SELECT '_';

    SELECT @Success Success,
           @message message;

END;

GO
