﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vResident_ProgramListview]
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid         Guid_Company,
         program.ID_Program   ID_Program,
         program.Name_Program Name_Program
  FROM   vResident hed
         INNER JOIN vResident_Program program
                 on hed.ID = program.ID_Resident
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company

GO
