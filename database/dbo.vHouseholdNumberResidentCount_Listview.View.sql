﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vHouseholdNumberResidentCount_Listview]
as
  Select household.ID,
         household.Name,
         summary.ID_Company,
         summary.Count
  FROM   tHouseholdNumber household
         lEFT join vHouseholdNumberResidentCount summary
                on household.ID = summary.ID
                   and household.ID_Company = summary.ID_Company

GO
