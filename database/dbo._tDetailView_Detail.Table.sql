﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[_tDetailView_Detail](
	[Oid] [uniqueidentifier] NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[SeqNo] [int] NULL,
	[Comment] [varchar](200) NULL,
	[Caption] [varchar](200) NULL,
	[ID_ModelProperty] [uniqueidentifier] NULL,
	[ID_DetailView] [uniqueidentifier] NOT NULL,
	[ID_Tab] [uniqueidentifier] NULL,
	[ID_Section] [uniqueidentifier] NULL,
	[ID_ControlType] [int] NULL,
	[ID_PropertyType] [int] NULL,
	[Format] [varchar](100) NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[DisplayProperty] [varchar](200) NULL,
	[DataSource] [varchar](200) NULL,
	[IsLoadData] [bit] NULL,
	[ColCount] [int] NULL,
	[ColSpan] [int] NULL,
	[IsDisabled] [bit] NULL,
	[Height] [int] NULL,
	[ID_ListView] [uniqueidentifier] NULL,
	[IsReadOnly] [bit] NULL,
	[ID_LabelLocation] [int] NULL,
	[IsShowLabel] [bit] NULL,
	[IsRequired] [bit] NULL,
	[ValueExpr] [varchar](300) NULL,
	[DisplayExpr] [varchar](300) NULL,
	[ID_LookUp_ListView] [uniqueidentifier] NULL,
	[LookUp_ListView_Caption] [varchar](300) NULL,
	[LookUp_ListView_DataSource] [varchar](300) NULL,
	[GroupIndex] [int] NULL,
	[SearchExpr] [varchar](300) NULL,
	[Precision] [int] NULL,
	[ID_Parent_Grid] [uniqueidentifier] NULL,
	[IsShowClearButton] [bit] NULL,
	[IsSearchEnabled] [bit] NULL,
	[InputMask] [varchar](300) NULL,
 CONSTRAINT [PK__tDetailView_Detail] PRIMARY KEY CLUSTERED 
(
	[Oid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[_tDetailView_Detail] ADD  CONSTRAINT [DF__tDetailView_Detail_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[_tDetailView_Detail]  WITH NOCHECK ADD  CONSTRAINT [FK_tDetailView_Detail_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[_tDetailView_Detail] CHECK CONSTRAINT [FK_tDetailView_Detail_ID_CreatedBy]
GO
ALTER TABLE [dbo].[_tDetailView_Detail]  WITH NOCHECK ADD  CONSTRAINT [FK_tDetailView_Detail_ID_DetailView] FOREIGN KEY([ID_DetailView])
REFERENCES [dbo].[_tDetailView] ([Oid])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[_tDetailView_Detail] CHECK CONSTRAINT [FK_tDetailView_Detail_ID_DetailView]
GO
ALTER TABLE [dbo].[_tDetailView_Detail]  WITH NOCHECK ADD  CONSTRAINT [FK_tDetailView_Detail_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[_tDetailView_Detail] CHECK CONSTRAINT [FK_tDetailView_Detail_ID_LastModifiedBy]
GO
ALTER TABLE [dbo].[_tDetailView_Detail]  WITH NOCHECK ADD  CONSTRAINT [FK_tDetailView_Detail_ID_ListView] FOREIGN KEY([ID_ListView])
REFERENCES [dbo].[_tListView] ([Oid])
GO
ALTER TABLE [dbo].[_tDetailView_Detail] CHECK CONSTRAINT [FK_tDetailView_Detail_ID_ListView]
GO
ALTER TABLE [dbo].[_tDetailView_Detail]  WITH NOCHECK ADD  CONSTRAINT [FK_tDetailView_Detail_ID_LookUp_ListView] FOREIGN KEY([ID_LookUp_ListView])
REFERENCES [dbo].[_tListView] ([Oid])
GO
ALTER TABLE [dbo].[_tDetailView_Detail] CHECK CONSTRAINT [FK_tDetailView_Detail_ID_LookUp_ListView]
GO
ALTER TABLE [dbo].[_tDetailView_Detail]  WITH NOCHECK ADD  CONSTRAINT [FK_tDetailView_Detail_ID_ModelProperty] FOREIGN KEY([ID_ModelProperty])
REFERENCES [dbo].[_tModel_Property] ([Oid])
GO
ALTER TABLE [dbo].[_tDetailView_Detail] CHECK CONSTRAINT [FK_tDetailView_Detail_ID_ModelProperty]
GO
ALTER TABLE [dbo].[_tDetailView_Detail]  WITH NOCHECK ADD  CONSTRAINT [FK_tDetailView_Detail_ID_Section] FOREIGN KEY([ID_Section])
REFERENCES [dbo].[_tDetailView_Detail] ([Oid])
GO
ALTER TABLE [dbo].[_tDetailView_Detail] CHECK CONSTRAINT [FK_tDetailView_Detail_ID_Section]
GO
ALTER TABLE [dbo].[_tDetailView_Detail]  WITH NOCHECK ADD  CONSTRAINT [FK_tDetailView_Detail_ID_Tab] FOREIGN KEY([ID_Tab])
REFERENCES [dbo].[_tDetailView_Detail] ([Oid])
GO
ALTER TABLE [dbo].[_tDetailView_Detail] CHECK CONSTRAINT [FK_tDetailView_Detail_ID_Tab]
GO
