﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fGetPatient_SOAP_String]
(
    @ID_Patient_SOAP INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN

    DECLARE @Rows TABLE
    (
        id_num INT IDENTITY(1, 1),
        DateReturn DATETIME,
        Name_Item VARCHAR(MAX),
        Comment VARCHAR(MAX)
    );

    DECLARE @msg VARCHAR(MAX) = '';

    INSERT @Rows
    (
        DateReturn,
        Name_Item,
        Comment
    )
    SELECT DateReturn,
           Name_Item,
           Comment
    FROM dbo.vPatient_SOAP_Plan
    WHERE ID_Patient_SOAP = @ID_Patient_SOAP
    ORDER BY DateReturn ASC;

    SELECT @msg
        = @msg + FORMAT(DateReturn, 'MMM. dd yyyy - dddd') + '<br/> &nbsp;&nbsp;&nbsp;&nbsp;' + ISNULL(Name_Item, '')
          + CASE
                WHEN LEN(ISNULL(Comment, '')) > 0 THEN
                    ' - '
                ELSE
                    ''
            END + ISNULL(Comment, '') + '<br/><br/>'
    FROM @Rows;

    RETURN @msg;
END;

GO
