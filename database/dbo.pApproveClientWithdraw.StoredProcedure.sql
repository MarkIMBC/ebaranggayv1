﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pApproveClientWithdraw] (@IDs_ClientWithdraw TYPINTLIST READONLY,
                                           @ID_UserSession     INT)
AS
  BEGIN
      DECLARE @Approved_ID_FilingStatus INT = 3;
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @IDs_Patient TYPINTLIST
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;
          DECLARE @NoAssignedClient_IDs_ClientWithdraw TABLE
            (
               ID_ClientWithdraw INT,
               ClientCount       INT
            )

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pApproveClientWithdraw_validation
            @IDs_ClientWithdraw,
            @ID_UserSession;

          UPDATE dbo.tClientWithdraw
          SET    ID_FilingStatus = @Approved_ID_FilingStatus,
                 DateApproved = GetDate(),
                 ID_ApprovedBy = @ID_User
          FROM   dbo.tClientWithdraw bi
                 INNER JOIN @IDs_ClientWithdraw ids
                         ON bi.ID = ids.ID;

          -- Add Withdraw on Credit Logs  
          DECLARE @ClientCredits TYPCLIENTCREDIT

          INSERT @ClientCredits
          SELECT ID_Client,
                 Date,
                 cd.WithdrawAmount * -1,
                 Code,
                 Comment
          FROM   tClientWithdraw cd
                 INNER JOIN @IDs_ClientWithdraw ids
                         ON cd.ID = ids.ID;

          EXEC pDoAdjustClientCredits
            @ClientCredits,
            @ID_UserSession
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;
GO
