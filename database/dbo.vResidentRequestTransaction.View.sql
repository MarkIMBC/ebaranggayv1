﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vResidentRequestTransaction]
AS
  SELECT hed.DateCreated,
         hed.ID_Company,
         hed.ID_ResidentRequest,
         hed.ID,
         hed.Name,
         _model.Oid     Oid_Model,
         _model.Caption Caption_Model
  FROM   vResidentRequest_Certificate_ListvIew hed,
         _tModel _model
  WHERE  _model.TableName = 'tResidentRequest_Certificate'
  UNION ALL
  SELECT hed.DateCreated,
         hed.ID_Company,
         hed.ID_ResidentRequest,
         hed.ID,
         hed.Name,
         _model.Oid     Oid_Model,
         _model.Caption Caption_Model
  FROM   vResidentRequest_CommunityTaxCertificate_ListvIew hed,
         _tModel _model
  WHERE  _model.TableName = 'tResidentRequest_CommunityTaxCertificate'
  UNION ALL
  SELECT hed.DateCreated,
         hed.ID_Company,
         hed.ID_ResidentRequest,
         hed.ID,
         hed.Name,
         _model.Oid     Oid_Model,
         _model.Caption Caption_Model
  FROM   vResidentRequest_BarangayBusinessClearance_ListvIew hed,
         _tModel _model
  WHERE  _model.TableName = 'tResidentRequest_BarangayBusinessClearance'
  UNION ALL
  SELECT hed.DateCreated,
         hed.ID_Company,
         hed.ID_ResidentRequest,
         hed.ID,
         hed.Name,
         _model.Oid     Oid_Model,
         _model.Caption Caption_Model
  FROM   vResidentRequest_IndigencyCertificate_ListvIew hed,
         _tModel _model
  WHERE  _model.TableName = 'tResidentRequest_IndigencyCertificate'

GO
