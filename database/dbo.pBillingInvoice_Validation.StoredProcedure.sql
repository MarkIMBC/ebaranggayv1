﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pBillingInvoice_Validation] (@ID_BillingInvoice      INT,
                                               @ID_Patient_Confinement INT,
                                               @ID_UserSession         INT)
AS
  BEGIN
      DECLARE @Filed_ID_FilingStatus INT = 1
      DECLARE @Approved_ID_FilingStatus INT = 3
      DECLARE @isValid BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @ConfinementCount INT = 0

      BEGIN TRY
          DECLARE @Code_Patient_Confinement VARCHAR(MAX) = ''
          DECLARE @Code_BillingInvoice VARCHAR(MAX) = ''
          DECLARE @BillingInvoices TABLE
            (
               Code              VARCHAR(MAX),
               Name_FilingStatus VARCHAR(MAX)
            )

          SELECT @ID_Patient_Confinement = confinement.ID,
                 @Code_Patient_Confinement = confinement.Code
          FROM   tPatient_Confinement confinement
          WHERE  confinement.ID = @ID_Patient_Confinement

          INSERT @BillingInvoices
          SELECT Code,
                 Name_FilingStatus
          FROM   vBillingInvoice
          WHERE  ID NOT IN ( @ID_BillingInvoice )
                 AND ID_Patient_Confinement = @ID_Patient_Confinement
                 AND ID_FilingStatus IN ( @Filed_ID_FilingStatus, @Approved_ID_FilingStatus )
          ORder  BY Date ASC

          IF (SELECT COUNT(*)
              FROM   @BillingInvoices) > 0
            BEGIN
                SET @message = 'Billing Invoice for Confinement '
                               + @Code_Patient_Confinement
                               + ' is already created.' + CHAR(13) + CHAR(10);

                SELECT @message = @message + Code + ' - ' + Name_FilingStatus
                                  + CHAR(13) + CHAR(10)
                FROM   @BillingInvoices;

                THROW 50001, @message, 1;
            END;
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @isValid = 0;
      END CATCH

      SELECT '_';

      SELECT @isValid isValid,
             @message message;
  END

GO
