﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vzCertificationIndigencyFormReport]
AS
  SELECT hed.ID                                                  ID,
         hed.Name                                                Name,
         hed.Code                                                Code,
         hed.Date                                                Date,
         hed.Address                                             Address,
         hed.Age                                                 Age,
         gender.TranslatedEngName                                TranslatedEngName_Gender,
         civilStatus.TranslatedEngName                           TranslatedEngName_CivilStatus,
         hed.Citizenship,
         hed.RequestOf,
         hed.Requestor,
         company.ID                                              ID_Company,
         company.ImageLogoLocationFilenamePath                   ImageLogoLocationFilenamePath,
         company.Name                                            Name_Company,
         REPLACE(company.Name, 'Brgy.', 'Barangay')              BarangayFullName_Company,
         company.Address                                         Address_Company,
         company.ContactNumber                                   ContactNumber_Company,
         company.BarangayListSideReportHTMLString                BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                             BarangayCaptainName_Company,
         company.BarangayCaptain_Name_Employee                   BarangayCaptain_Name_Employee,
         company.BarangayCaptain_ImageSignitureSpecimen_Employee BarangayCaptain_ImageSignitureSpecimen_Employee,
         company.BarangayCaptain_LastName_Employee               BarangayCaptain_LastName_Employee,
         company.BarangayCaptain_FirstName_Employee              BarangayCaptain_FirstName_Employee,
         company.BarangayCaptain_MiddleName_Employee             BarangayCaptain_MiddleName_Employee,
         municipality.Name                                       Name_MunicipalityCompany,
         municipality.ImageLogoLocationFilenamePath              ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                                    Address_MunicipalityCompany,
         [dbo].[fGetOrdinalNumber] (GETDATE())                   AS OrdinalNum
  FROM   vCertificateOfIndigency hed
         LEFT Join tCivilStatus civilStatus
                ON civilStatus.ID = hed.ID_CivilStatus
         LEFT Join tGender gender
                ON gender.ID = hed.ID_Gender
         INNER JOIN vCompanyActive company
                 ON company.ID = hed.ID_Company
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = hed.ID_Company

--LEFT Join tCivilStatus civilStatus    
--       ON civilStatus.ID = hed.ID_CivilStatus    
--LEFT Join tGender gender    
--       ON gender.ID = hed.ID_  
--db_ebarangayv1_dev
GO
