﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vResidentNonDeceased]
as
  SELECT *
  FROM   vResident
  WHERE  IsActive = 1
         ANd ISNULL(IsDeceased, 0) = 0

GO
