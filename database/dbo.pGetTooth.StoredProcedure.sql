﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pGetTooth] @ID INT = -1 , @ID_Session INT = NULL
		AS
		BEGIN
			SELECT '_'

			DECLARE @ID_User INT, @ID_Warehouse INT
			SELECT @ID_User = ID_User, @ID_Warehouse = ID_Warehouse FROM tUserSession WHERE ID = @ID_Session

			IF (@ID = -1)
				BEGIN
					SELECT 
						H.* 
					FROM ( 
						SELECT 	NULL AS [_] , 	-1 AS [ID] , NULL AS [Code] , NULL AS [Name] , 1 AS [IsActive] , NULL AS [Comment] , NULL AS [ToothNumber] , NULL AS [Location] , NULL AS [Top_ID_ToothSurface] , NULL AS [Left_ID_ToothSurface] , NULL AS [Middle_ID_ToothSurface] , NULL AS [Bottom_ID_ToothSurface] , NULL AS [Right_ID_ToothSurface] , NULL AS [ID_CreatedBy] , NULL AS [ID_LastModifiedBy] , NULL AS [DateCreated] , NULL AS [DateModified]
					) H
					LEFT JOIN tUser UC ON H.ID_CreatedBy = UC.ID
					LEFT JOIN tUser UM ON H.ID_LastModifiedBy = UM.ID
				END
			ELSE
				BEGIN
					SELECT 
						H.* 
					FROM vTooth H 
					WHERE H.ID = @ID
				END
		END
GO
