﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PetVetCentral_Items](
	[Items] [nvarchar](255) NULL,
	[Price] [float] NULL
) ON [PRIMARY]
GO
