﻿CREATE TYPE [dbo].[typActiveInactiveClient] AS TABLE(
	[ID_Client] [int] NULL,
	[IsActive] [bit] NULL
)
GO
