﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[addHelpDeskVideoTutorial](@Name        VARCHAR(MAX),
                                    @Descriotion VARCHAR(MAX),
                                    @Url         VARCHAR(MAX),
                                    @SearchTag   VARCHAR(MAX))
as
    INSERT INTO [dbo].[tHelpDeskVideoTutorial]
                ([Code],
                 [Name],
                 [IsActive],
                 [ID_Company],
                 [Comment],
                 [DateCreated],
                 [DateModified],
                 [ID_CreatedBy],
                 [ID_LastModifiedBy],
                 [VideoLink],
                 [ThumbnailImage],
                 [SearchTag])
    VALUES      (NULL,
                 @Name,
                 1,
                 1,
                 @Descriotion,
                 GetDate(),
                 GetDate(),
                 1,
                 1,
                 @Url,
                 '',
                 @SearchTag)

GO
