﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pUpdateAppointmentStatus](@Oid_Model                    VARCHAR(MAX),
                                    @Appointment_ID_CurrentObject INT,
                                    @Appointment_ID_FilingStatus  INT,
                                    @ID_UserSession               INT,
                                    @Remarks                      VARCHAR(MAX))
AS
  BEGIN
      DECLARE @TableName VARCHAR(MAX) = ''
      DECLARE @ID_Company INT = 0
      DECLARE @ID_User INT = 0

      SELECT @ID_User = ID_User
      FROM   tUserSession
      WHERE  ID = @ID_UserSession

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      SELECT @TableName = TableName
      FROM   _tModel
      WHERE  Oid = @Oid_Model

      IF( @TableName = 'tPatientAppointment' )
        BEGIN
            UPDATE tPatientAppointment
            SET    Appointment_ID_FilingStatus = @Appointment_ID_FilingStatus,
                   Appointment_CancellationRemarks = @Remarks
            WHERE  ID = @Appointment_ID_CurrentObject
        END
      ELSE IF( @TableName = 'tPatient_SOAP' )
        BEGIN
            UPDATE tPatient_SOAP_Plan
            SET    Appointment_ID_FilingStatus = @Appointment_ID_FilingStatus,
                   Appointment_CancellationRemarks = @Remarks
            WHERE  ID = @Appointment_ID_CurrentObject
        END
      ELSE IF( @TableName = 'tPatient_Wellness' )
        BEGIN
            UPDATE tPatient_Wellness_Schedule
            SET    Appointment_ID_FilingStatus = @Appointment_ID_FilingStatus,
                   Appointment_CancellationRemarks = @Remarks
            WHERE  ID = @Appointment_ID_CurrentObject
        END

      /*Logs*/
      INSERT INTO [dbo].[tAppointmentStatusLog]
                  ([IsActive],
                   [ID_Company],
                   [Comment],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [Oid_Model],
                   [Appointment_ID_CurrentObject],
                   [Appointment_ID_FilingStatus])
      VALUES      ( 1,
                    @ID_Company,
                    @Remarks,
                    GetDate(),
                    GetDate(),
                    @ID_User,
                    @ID_User,
                    @Oid_Model,
                    @Appointment_ID_CurrentObject,
                    @Appointment_ID_FilingStatus)
  END

GO
