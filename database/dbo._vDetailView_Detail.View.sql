﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[_vDetailView_Detail]
AS
    SELECT  D.*,
			H.Name AS DetailView,
			T.Name AS TabName,
			S.Name AS SectionName,
			PT.Name AS PropertyType,
			CT.Name AS ControlType,
			MP.Name AS ModelProperty
    FROM    dbo.[_tDetailView_Detail] D
            LEFT JOIN dbo.[_tDetailView] H ON D.ID_DetailView = H.Oid
			LEFT JOIN dbo.[_tDetailView_Detail] T ON D.ID_Tab = T.Oid
			LEFT JOIN dbo.[_tDetailView_Detail] S ON D.ID_Section = S.Oid
			LEFT JOIN _tPropertyType PT ON D.ID_PropertyType = PT.ID
			LEFT JOIN _tControlType CT ON D.ID_ControlType = CT.ID
			LEFT JOIN _tModel_Property MP ON D.ID_ModelProperty = MP.Oid
GO
