﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vSMSCountPerCompany_Patient_SOAP_Plan]
AS
  SELECT CONVERT(Date, DateSent) DateSent,
         ID_Company,
         COUNT(*)                TotalSMSCount
  FROM   tPatient_SOAP_Plan soapPlan
         INNER JOIN tPatient_SOAP soap
                 ON soapPlan.ID_Patient_SOAP = soap.ID
  WHERE  DateSent IS NOT NULL
  GROUP  BY ID_Company,
            CONVERT(Date, DateSent)

GO
