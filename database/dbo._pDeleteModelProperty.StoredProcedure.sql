﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[_pDeleteModelProperty]
    @ID_Model UNIQUEIDENTIFIER = NULL,
	@TableName VARCHAR(100) = NULL,
    @FieldName VARCHAR(100) ,
	@IsDeleteFromTable BIT = 0
AS
    DECLARE @ID_ModelProperty UNIQUEIDENTIFIER

	IF @TableName IS NOT NULL
	BEGIN
		SELECT  @ID_Model = Oid
		FROM    dbo.[_tModel]
		WHERE   TableName = @TableName

		IF ( @ID_Model IS NULL )
			RETURN
	END

	IF @ID_Model IS NOT NULL
	BEGIN
	SELECT  @TableName = TableName
		FROM    dbo.[_tModel]
		WHERE   Oid = @ID_Model

		IF ( @TableName IS NULL )
			RETURN
	END

    SELECT  @ID_ModelProperty = Oid
    FROM    dbo.[_tModel_Property]
    WHERE   ID_Model = @ID_Model
            AND Name = @FieldName

    IF ( @ID_ModelProperty IS NULL )
        RETURN


    UPDATE  dbo.[_tModel_Property]
    SET     ID_ModelProperty_Key = NULL
    WHERE   ID_ModelProperty_Key = @ID_ModelProperty

    DELETE  FROM dbo.[_tListView_Detail]
    WHERE   ID_ModelProperty = @ID_ModelProperty

    DELETE  FROM dbo.[_tDetailView_Detail]
    WHERE   ID_ModelProperty = @ID_ModelProperty

    DELETE  FROM dbo.[_tModel_Property]
    WHERE   Oid = @ID_ModelProperty

	IF (@IsDeleteFromTable = 1)
	BEGIN
		EXEC('ALTER TABLE ' + @TableName + ' DROP COLUMN ' + @FieldName)
		EXEC dbo.[_pRefreshAllViews]
	END
GO
