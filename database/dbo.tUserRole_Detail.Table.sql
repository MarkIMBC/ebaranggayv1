﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tUserRole_Detail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](max) NULL,
	[ID_UserRole] [int] NULL,
	[ID_Model] [varchar](300) NULL,
	[IsView] [bit] NULL,
	[IsCreate] [bit] NULL,
	[IsEdit] [bit] NULL,
	[IsDelete] [bit] NULL,
	[IsDeny] [bit] NULL,
	[SeqNo] [int] NULL,
 CONSTRAINT [PK_tUserRole_Detail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tUserRole_Detail] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tUserRole_Detail]  WITH CHECK ADD  CONSTRAINT [FKtUserRole_Detail_ID_UserRole] FOREIGN KEY([ID_UserRole])
REFERENCES [dbo].[tUserRole] ([ID])
GO
ALTER TABLE [dbo].[tUserRole_Detail] CHECK CONSTRAINT [FKtUserRole_Detail_ID_UserRole]
GO
