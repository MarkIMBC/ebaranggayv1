﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   PROC [dbo].[pValidateClient]
(
    @ID_Client INT,
	@Name VARCHAR(MAX),
	@ID_UserSession INT
)
AS
BEGIN

    DECLARE @isValid BIT = 1;
    DECLARE @IsWarning BIT = 0;
    DECLARE @message VARCHAR(MAX) = '';
    DECLARE @msgTHROW VARCHAR(MAX) = '';

    DECLARE @ID_User INT = 0;
    DECLARE @ID_Company INT = 0;

    SELECT 
		@ID_User = userSession.ID_User,
		@ID_Company = _user.ID_Company
    FROM dbo.tUserSession userSession INNER JOIN vUser _user ON userSession.ID_User = _user.ID
    WHERE userSession.ID = @ID_UserSession;

	SET @Name = LTRIM(RTRIM(ISNULL(@Name,'')))

	if ISNULL(@ID_Company, 0) = 0
	BEGIN

		SELECT  
			@ID_Company = ID_Company
		FROM tClient
		WHERE 
			ID = @ID_Client
	END

    BEGIN TRY

		DECLARE @ExistName Varchar(MAX) = ''
		DECLARE @Count INT = 0

		IF LEN(@Name) = 0
		BEGIN
			
			SET @msgTHROW = 'Client Name is required.';
			THROW 50005, @msgTHROW, 1;
		END
		
		
		SELECT  
			@ExistName = Name
		FROM tClient 
		WHERE 
			ID NOT IN (@ID_Client) AND (
			LTRIM(RTRIM(ISNULL(LOWER(Name), ''))) = LTRIM(RTRIM(ISNULL(LOWER(@Name), ''))) AND
			IsActive = 1 AND
			ID_Company = @ID_Company)


		IF LEN(LTRIM(RTRIM(ISNULL(LOWER(@ExistName), '')))) > 0
		BEGIN

			SET @msgTHROW = 'Client '''+ @Name +''' is already exist';

			SELECT  
				@Count = COUNT(*)
			FROM tClient 
			WHERE 
				(
				LTRIM(RTRIM(ISNULL(LOWER(Name), ''))) = LTRIM(RTRIM(ISNULL(LOWER(@Name), ''))) AND
				IsActive = 1 AND
				ID_Company = @ID_Company)
			GROUP BY
				Name
				 
			if(@Count > 1)
			BEGIN

				SET @IsWarning = 1
				SET @msgTHROW = @msgTHROW + ' ' + FORMAT(@Count,'0') + ' times'
			END;

			set @msgTHROW = @msgTHROW + '.';

			THROW 50005, @msgTHROW, 1;
		END;
		 
    END TRY
    BEGIN CATCH

        SET @message = ERROR_MESSAGE();
        SET @isValid = 0;
    END CATCH;

    SELECT '_'; 

    SELECT @isValid isValid,
		   @IsWarning isWarning,
           @message message;

END;

GO
