﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGetConfinmentDeposit](@ID_Patient_Confinement int)
AS
  BEGIN
      DECLARE @CurrentCreditAmount Decimal(18, 4)= 0
      DECLARE @ConfinementDepositAmount Decimal(18, 4)= 0
      DECLARE @RemainingDepositAmount Decimal(18, 4)= 0
      DECLARE @ID_Client INT = 0
      DECLARE @Used_ID_FilingStatus INT = 17
      DECLARE @Approved_ID_FilingStatus INT = 3
      DECLARE @MaxDate_ClientDeposit DateTime
      DECLARE @ClientDeposit TABLE
        (
           ID            INT,
           Date          DateTIme,
           Code          VARCHAR(50),
           DepositAmount Decimal(18, 4)
        )

      SELECT @ID_Client = ID_Client
      FROM   tPatient_Confinement
      where  ID = @ID_Patient_Confinement

      SELECT @CurrentCreditAmount = client.CurrentCreditAmount
      FROM   tclient client
      WHERE  ID = @ID_Client

      SET @ConfinementDepositAmount = dbo.fGetConfinmentDeposit(@ID_Patient_Confinement);
      SET @RemainingDepositAmount = ISNULL(@CurrentCreditAmount, 0) - ISNULL(@ConfinementDepositAmount, 0)

      if( @RemainingDepositAmount < 0 )
        SET @RemainingDepositAmount = 0

      SELECT @MaxDate_ClientDeposit = MAX(Date)
      FROm   tClient_CreditLogs
      where  ID_Client = @ID_Client
             AND Code NOT IN (SELECT Code
                              FROM   @ClientDeposit)

      INSERT @ClientDeposit
      SELECT NULL,
             @MaxDate_ClientDeposit,
             'Remaining',
             @RemainingDepositAmount

      INSERT @ClientDeposit
      SELECT ID,
             Date,
             Code,
             DepositAmount
      FROM   dbo.vClientDeposit
      WHERE  ID_Patient_Confinement = @ID_Patient_Confinement
             AND ID_FilingStatus IN ( @Approved_ID_FilingStatus, @Used_ID_FilingStatus )

      SELECT '_',
             '' AS ClientDeposits;

      SELECT @ID_Patient_Confinement              ID_Patient_Confinement,
             @RemainingDepositAmount              RemainingDepositAmount,
             ISNULL(@ConfinementDepositAmount, 0) ConfinementDepositAmount,
             ISNULL(@CurrentCreditAmount, 0)      TotalDepositAmount;

      SELECT *
      FROM   @ClientDeposit
  END

GO
