﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 FUNCTION [dbo].[fGetITextMessageCreditCount](@Message VARCHAR(255))
RETURNS INT
AS
  BEGIN
      DECLARE @Result INT = 0

      SET @Result = CASE
                      WHEN LEN(@Message) <= 160 THEN 1
                      ELSE
                        CASE
                          WHEN LEN(@Message) <= 306 THEN 2
                          ELSE
                            CASE
                              WHEN LEN(@Message) <= 459 THEN 3
                              ELSE 4
                            END
                        END
                    END

      RETURN @Result
  END

GO
