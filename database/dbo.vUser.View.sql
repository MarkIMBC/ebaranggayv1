﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vUser]
AS
  SELECT h.*,
         E.Name       AS Employee,
         E.Name       AS Name_Employee,
         UG.Name      AS UserGroup,
         UC.Name      AS CreatedBy,
         UM.Name      AS LastModifiedBy,
         E.ID_Company,
         company.Name Name_Company,
         company.Code Code_Company,
         e.ID_Position,
         company.Guid,
         company.ReceptionPortalGuid
  FROM   dbo.tUser h
         LEFT JOIN dbo.tEmployee E
                ON h.ID_Employee = E.ID
         LEFT JOIN dbo.tUserGroup UG
                ON UG.ID = h.ID_UserGroup
         LEFT JOIN tUser UC
                ON h.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON h.ID_LastModifiedBy = UM.ID
         LEFT JOIN dbo.tCompany company
                ON company.ID = E.ID_Company; 
GO
