﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pUpdateClientCredits]
AS
  BEGIN
      UPDATE tClient
      SET    CurrentCreditAmount = ISNULL(tbl.TotalCreditAmount, 0)
      FROm   tClient client
             LEFT JOIN (SELECT ID_Client,
                               SUM(CreditAmount) TotalCreditAmount
                        FROm   tClient_CreditLogs
                        GROUP  BY ID_Client) tbl
                    ON client.ID = tbl.ID_Client
  END

GO
