﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vCompany]
AS
  SELECT H.*,
         UC.Name                           AS CreatedBy,
         UM.Name                           AS LastModifiedBy,
         dbo.fGetAPILink() + '/Content/Image/'
         + H.ImageHeaderFilename           ImageHeaderLocationFilenamePath,
         dbo.fGetAPILink() + '/Content/Thumbnail/'
         + H.ImageHeaderFilename           ImageHeaderThumbNameLocationFilenamePath,
         dbo.fGetAPILink() + '/Content/Image/'
         + H.ImageLogoFilename             ImageLogoLocationFilenamePath,
         dbo.fGetAPILink() + '/Content/Thumbnail/'
         + H.ImageLogoFilename             ImageLogoThumbNameLocationFilenamePath,
         dbo.fGetAPILink() + '/' + h.Code + '/'
         + h.ReceptionPortalGuid
         + '/BaranggayRegistrationPortal'  BarangayPortalLink,
         employee.Name                     BarangayCaptain_Name_Employee,
         ISNULL(employee.LastName, '')     BarangayCaptain_LastName_Employee,
         ISNULL(employee.FirstName, '')    BarangayCaptain_FirstName_Employee,
         ISNULL(employee.MiddleName, '')   BarangayCaptain_MiddleName_Employee,
         dbo.fGetAPILink() + '/Content/Thumbnail/'
         + employee.ImageSignitureSpecimen BarangayCaptain_ImageSignitureSpecimen_Employee
  FROM   tCompany H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN vEmployee employee
                ON H.BarangayCaptain_ID_Employee = employee.ID

GO
