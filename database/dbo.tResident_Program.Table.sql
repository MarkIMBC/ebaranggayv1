﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tResident_Program](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_Program] [int] NULL,
	[ID_Resident] [int] NULL,
 CONSTRAINT [PK_tResident_Program] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tResident_Program] ON [dbo].[tResident_Program]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tResident_Program] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tResident_Program]  WITH CHECK ADD  CONSTRAINT [FK_tResident_Program_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tResident_Program] CHECK CONSTRAINT [FK_tResident_Program_ID_Company]
GO
ALTER TABLE [dbo].[tResident_Program]  WITH CHECK ADD  CONSTRAINT [FK_tResident_Program_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tResident_Program] CHECK CONSTRAINT [FK_tResident_Program_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tResident_Program]  WITH CHECK ADD  CONSTRAINT [FK_tResident_Program_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tResident_Program] CHECK CONSTRAINT [FK_tResident_Program_ID_LastModifiedBy]
GO
ALTER TABLE [dbo].[tResident_Program]  WITH CHECK ADD  CONSTRAINT [FKtResident_Program_ID_Resident] FOREIGN KEY([ID_Resident])
REFERENCES [dbo].[tResident] ([ID])
GO
ALTER TABLE [dbo].[tResident_Program] CHECK CONSTRAINT [FKtResident_Program_ID_Resident]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tResident_Program]
		ON [dbo].[tResident_Program]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tResident_Program
			SET    DateCreated = GETDATE()
			FROM   dbo.tResident_Program hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tResident_Program] ENABLE TRIGGER [rDateCreated_tResident_Program]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tResident_Program]
		ON [dbo].[tResident_Program]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tResident_Program
			SET    DateModified = GETDATE()
			FROM   dbo.tResident_Program hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tResident_Program] ENABLE TRIGGER [rDateModified_tResident_Program]
GO
