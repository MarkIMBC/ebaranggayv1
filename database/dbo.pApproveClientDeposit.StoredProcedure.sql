﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pApproveClientDeposit] (@IDs_ClientDeposit typIntList READONLY,
                                          @ID_UserSession    INT)
AS
  BEGIN
      DECLARE @Approved_ID_FilingStatus INT = 3;
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @IDs_Patient typIntList
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;
          DECLARE @NoAssignedClient_IDs_ClientDeposit TABLE
            (
               ID_ClientDeposit INT,
               ClientCount      INT
            )

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pApproveClientDeposit_validation
            @IDs_ClientDeposit,
            @ID_UserSession;

          UPDATE dbo.tClientDeposit
          SET    ID_FilingStatus = @Approved_ID_FilingStatus,
                 DateApproved = GETDATE(),
                 ID_ApprovedBy = @ID_User
          FROM   dbo.tClientDeposit bi
                 INNER JOIN @IDs_ClientDeposit ids
                         ON bi.ID = ids.ID;

          -- Add Deposit on Credit Logs
          DECLARE @ClientCredits typClientCredit

          INSERT @ClientCredits
          SELECT ID_Client,
                 Date,
                 cd.DepositAmount,
                 Code,
                 Comment
          FROM   tClientDeposit cd
                 INNER JOIN @IDs_ClientDeposit ids
                         ON cd.ID = ids.ID;

          exec pDoAdjustClientCredits
            @ClientCredits,
            @ID_UserSession
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END;

GO
