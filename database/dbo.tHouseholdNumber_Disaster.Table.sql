﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tHouseholdNumber_Disaster](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_Disaster] [int] NULL,
	[ID_HouseholdNumber] [int] NULL,
 CONSTRAINT [PK_tHouseholdNumber_Disaster] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tHouseholdNumber_Disaster] ON [dbo].[tHouseholdNumber_Disaster]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tHouseholdNumber_Disaster] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tHouseholdNumber_Disaster]  WITH CHECK ADD  CONSTRAINT [FK_tHouseholdNumber_Disaster_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tHouseholdNumber_Disaster] CHECK CONSTRAINT [FK_tHouseholdNumber_Disaster_ID_Company]
GO
ALTER TABLE [dbo].[tHouseholdNumber_Disaster]  WITH CHECK ADD  CONSTRAINT [FK_tHouseholdNumber_Disaster_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tHouseholdNumber_Disaster] CHECK CONSTRAINT [FK_tHouseholdNumber_Disaster_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tHouseholdNumber_Disaster]  WITH CHECK ADD  CONSTRAINT [FK_tHouseholdNumber_Disaster_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tHouseholdNumber_Disaster] CHECK CONSTRAINT [FK_tHouseholdNumber_Disaster_ID_LastModifiedBy]
GO
ALTER TABLE [dbo].[tHouseholdNumber_Disaster]  WITH CHECK ADD  CONSTRAINT [FKtHouseholdNumber_Disaster_ID_HouseholdNumber] FOREIGN KEY([ID_HouseholdNumber])
REFERENCES [dbo].[tHouseholdNumber] ([ID])
GO
ALTER TABLE [dbo].[tHouseholdNumber_Disaster] CHECK CONSTRAINT [FKtHouseholdNumber_Disaster_ID_HouseholdNumber]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE TRIGGER [dbo].[rDateCreated_tHouseholdNumber_Disaster] ON [dbo].[tHouseholdNumber_Disaster] FOR INSERT
			AS
				DECLARE @ID INT
				SELECT @ID = ID FROM Inserted
				UPDATE dbo.tHouseholdNumber_Disaster SET DateCreated = GETDATE() WHERE ID = @ID
		
GO
ALTER TABLE [dbo].[tHouseholdNumber_Disaster] ENABLE TRIGGER [rDateCreated_tHouseholdNumber_Disaster]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE TRIGGER [dbo].[rDateModified_tHouseholdNumber_Disaster] ON [dbo].[tHouseholdNumber_Disaster] FOR UPDATE, INSERT
			AS
				DECLARE @ID INT
				SELECT @ID = ID FROM Inserted
				UPDATE dbo.tHouseholdNumber_Disaster SET DateModified = GETDATE() WHERE ID = @ID
		
GO
ALTER TABLE [dbo].[tHouseholdNumber_Disaster] ENABLE TRIGGER [rDateModified_tHouseholdNumber_Disaster]
GO
