﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROC [dbo].[pDonePatientAppointment]
(
    @IDs_PatientAppointment typIntList READONLY,
    @ID_UserSession INT
)
AS
BEGIN

    DECLARE @Done_ID_FilingStatus INT = 13;

    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';

    BEGIN TRY

        DECLARE @ID_User INT = 0;

        SELECT @ID_User = ID_User
        FROM dbo.tUserSession
        WHERE ID = @ID_UserSession;

        EXEC dbo.pDonePatientAppointment_validation @IDs_PatientAppointment,
                                                  @ID_UserSession;

        UPDATE dbo.tPatientAppointment
        SET ID_FilingStatus = @Done_ID_FilingStatus,
            DateDone = GETDATE(),
            ID_DoneBy = @ID_User
        FROM dbo.tPatientAppointment bi
            INNER JOIN @IDs_PatientAppointment ids
                ON bi.ID = ids.ID;

    END TRY
    BEGIN CATCH

        SET @message = ERROR_MESSAGE();
        SET @Success = 0;

    END CATCH;
	 
    SELECT '_';

    SELECT @Success Success,
           @message message;

END;

GO
