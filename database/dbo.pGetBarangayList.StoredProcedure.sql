﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGetBarangayList]
AS
  BEGIN
      SELECT '_',
             '' DataSource;

      SELECT GetDate() CureentDate

      SELECT ID,
             Code,
             Name,
             Guid
      FROM   vCompanyActive
      WHERE  ID > 1
  END

GO
