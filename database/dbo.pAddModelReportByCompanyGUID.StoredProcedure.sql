﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pAddModelReportByCompanyGUID](@GUID_Company VARCHAR(MAX),
                                        @ModelName    Varchar(200),
                                        @ReportName   VARCHAR(200),
                                        @labelName    VARCHAR(200))
AS
  BEGIN
      IF(SELECT Count(*)
         FROM   vCompanyActive
         WHERE  Guid = @GUID_Company
                AND IsActive = 1) <> 1
        BEGIN ;
            THROW 51000, 'Company does not exist.', 1;
        END

      DECLARE @ID_Company INT

      SELECT @ID_Company = ID
      FROM   tCompany
      WHERE  Guid = @GUID_Company

      exec pAddModelReport
        @ID_Company,
        @ModelName,
        @ReportName,
        @labelName
  END

GO
