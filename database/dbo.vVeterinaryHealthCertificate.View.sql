﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vVeterinaryHealthCertificate]
AS
  SELECT H.*,
         UC.Name                   CreatedBy,
         UM.Name                   LastModifiedBy,
         client.Name               Name_Client,
         patient.Name              Name_Patient,
         fs.Name                   Name_FilingStatus,
         _AttendingPhysician.Name  AttendingPhysician_Name_Employee,
         item.Name                 Name_Item,
         vaccinationOption.Comment Comment_VaccinationOption
  FROM   tVeterinaryHealthCertificate H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tClient client
                ON client.ID = H.ID_Client
         LEFT JOIN tPatient patient
                ON patient.ID = H.ID_Patient
         LEFT JOIN tFilingStatus fs
                ON fs.ID = H.ID_FilingStatus
         LEFT JOIN tEmployee _AttendingPhysician
                ON _AttendingPhysician.ID = H.AttendingPhysician_ID_Employee
         LEFT JOIN tItem item
                ON item.ID = H.ID_Item
         LEFT JOIN tVaccinationOption vaccinationOption
                ON vaccinationOption.ID = H.ID_VaccinationOption

GO
