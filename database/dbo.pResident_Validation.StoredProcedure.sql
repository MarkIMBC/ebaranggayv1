﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  
 PROC [dbo].[pResident_Validation] (@ID_Resident    INT,
                                     @Name           VARCHAR(MAX),
                                     @ID_UserSession INT)
AS
  BEGIN
      DECLARE @isValid BIT = 1;
      DECLARE @isWarning BIT = 0;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @ID_Company INT = 0

      SELECT @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      where  _usersession.ID = @ID_UserSession

      BEGIN TRY
          DECLARE @Count_ResidentNames INT = 0

          SELECT @Count_ResidentNames = COUNT(*)
          FROm   tResident
          where  REPLACE(dbo.fGetCleanedString(Name), ' ', '') = REPLACE(dbo.fGetCleanedString(@Name), ' ', '')
                 and ID_Company = @ID_Company
                 and IsActive = 1
                 AND ID NOT IN ( @ID_Resident )

          IF ( @Count_ResidentNames ) > 0
            BEGIN
                if( @ID_Resident > 0 )
                  BEGIN
                      SET @isWarning = 1
                      SET @message = 'Resident '''
                                     + dbo.fGetCleanedString(@Name) + ''' '
                                     + CASE
                                         WHEN @Count_ResidentNames > 1 then 'have'
                                         else 'has'
                                       END
                                     + ' '
                                     + CONVERT(VARCHAR(MAX), @Count_ResidentNames)
                                     + ' already exist.';
                  END
                ELSE
                  BEGIN
                      SET @message = 'Resident Name is already exist.';
                  END;

                THROW 50001, @message, 1;
            END;
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @isValid = 0;
      END CATCH

      SELECT '_';

      SELECT @isValid   isValid,
             @isWarning isWarning,
             @message   message;
  END

GO
