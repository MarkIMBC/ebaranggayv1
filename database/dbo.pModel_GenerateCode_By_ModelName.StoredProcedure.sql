﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pModel_GenerateCode_By_ModelName] (@ModelName        VARCHAR(MAX),
                                                   @ID_CurrentObject VARCHAR(10),
                                                   @IsNew            BIT = 0)
AS
  BEGIN
      DECLARE @ID_Model UNIQUEIDENTIFIER

      SELECT @ID_Model = Oid
      FROM   dbo._tModel
      WHERE  Name = @ModelName

      exec pModel_GenerateCode
        @ID_Model,
        @ID_CurrentObject,
        @IsNew
  END;

GO
