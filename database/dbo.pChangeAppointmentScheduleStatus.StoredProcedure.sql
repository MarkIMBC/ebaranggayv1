﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pChangeAppointmentScheduleStatus](@IDs_AppointmentSchedule typIntList READONLY, @ID_FilingStatus int) 
as
BEGIN
   
    DECLARE @Success BIT = 1;        
    DECLARE @message VARCHAR(300) = '';   
	
	Update dbo.tAppointmentSchedule 
		SET AppointmentStatus_ID_FilingStatus = @ID_FilingStatus
	FROm dbo.tAppointmentSchedule hed 
	inner join @IDs_AppointmentSchedule ids 
		on ids.ID = hed.ID

    SELECT '_';        
        
    SELECT @Success Success,        
           @message message;        
END

GO
