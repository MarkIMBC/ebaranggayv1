﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pUpdateItemCurrentInventory]  
AS  
  BEGIN  
      UPDATE dbo.tItem  
      SET    CurrentInventoryCount = ISNULL(inventory.Qty, 0),  
             ID_InventoryStatus = dbo.fGetInventoryStatus(inventory.Qty, ISNULL(MinInventoryCount, 0), ISNULL(MaxInventoryCount, 0))  
      FROM   dbo.tItem item  
             LEFT JOIN (SELECT inventory.ID_Item,  
                               SUM(inventory.Quantity) Qty  
                        FROM   dbo.tInventoryTrail inventory  
                               INNER JOIN tItem item  
                                       on item.ID = inventory.ID_Item  
                        GROUP  BY inventory.ID_Item,  
                                  item.MinInventoryCount,  
                                  item.MaxInventoryCount) inventory  
                    ON inventory.ID_Item = item.ID;  
  END;   
GO
