﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vPatient_SOAP_LaboratoryImages_Count]
as
  select ID_Patient_SOAP,
         Count(*) TotalCount
  FROM   vPatient_SOAP_LaboratoryImages
  GROUP  BY ID_Patient_SOAP

GO
