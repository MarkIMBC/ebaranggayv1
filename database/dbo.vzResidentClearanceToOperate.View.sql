﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE    
 VIEW [dbo].[vzResidentClearanceToOperate]
AS
  SELECT Resident.Name                              Name,
         Resident.ID                                ID,
         Resident.Address                           Address_Resident,
         municipality.Name                          Name_MunicipalityCompany,
         municipality.ImageLogoLocationFilenamePath ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                       Address_MunicipalityCompany,
         company.ID                                 ID_Company,
         company.ImageLogoLocationFilenamePath      ImageLogoLocationFilenamePath,
         company.Name                               Name_Company,
         company.Address                            Address_Company,
         company.ContactNumber                      ContactNumber_Company,
         company.BarangayListSideReportHTMLString   BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                BarangayCaptainName_Company
  FROM   tResident Resident
         INNER JOIN vCompanyActive company
                 ON company.ID = Resident.ID_Company
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = Resident.ID_Company

GO
