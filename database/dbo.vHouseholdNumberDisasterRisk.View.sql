﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vHouseholdNumberDisasterRisk]
as
  SELECT ID_HouseholdNumber,
         DisasterRisk = LTRIM(RTRIM(STUFF((SELECT ', ' + Name_Disaster
                                           FROM   vHouseholdNumber_Disaster
                                           where  ID_HouseholdNumber = hed.ID_HouseholdNumber
                                           FOR XML PATH ('')), 1, 1, '')))
  FROM   vHouseholdNumber_Disaster hed
  GROUP  BY ID_HouseholdNumber

GO
