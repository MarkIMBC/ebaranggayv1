﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vSoloParent_Listview]
AS
  SELECT hed. ID,
         hed.Code,
         hed.Date,
         hed.Name,
         hed.Age,
         hed.ParentName,
         hed.ParentAge,
         hed.Purpose,
         hed.Reason,
         hed.Name_Relationship,
         hed.ID_Company,
         hed.IsActive
  FROM   vSoloParent hed

GO
