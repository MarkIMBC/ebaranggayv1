﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vLastestMessageRecipient]
AS
SELECT tbl.Sender_ID_User,
       tbl.Sender_Name_User,
       tbl.Recipient_ID_User,
       tbl.Recipient_Name_User,
       SUM(tbl.ReadCount) TotalReadCount,
       MAX(tbl.DateSent) LastDateSent,
       CASE
           WHEN SUM(tbl.ReadCount) > 0 THEN
               CONVERT(BIT, 1)
           ELSE
               CONVERT(BIT, 0)
       END IsRead
FROM
(
    SELECT Recipient_ID_User,
           Recipient_Name_User,
           Sender_ID_User,
           Sender_Name_User,
           CASE
               WHEN ISNULL(IsRead, 0) = 1 THEN
                   1
               ELSE
                   0
           END ReadCount,
           DateSent
    FROM dbo.vMessageThread
) tbl
GROUP BY tbl.Recipient_ID_User,
         tbl.Recipient_Name_User,
         tbl.Sender_ID_User,
         tbl.Sender_Name_User;
GO
