﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[_pFixDefaultProperties]
AS
	UPDATE  dbo.[_tDetailView_Detail]
    SET     DisplayProperty = 'Company' ,
            Caption = 'Company' ,
            IsReadOnly = 1
	WHERE Name = 'ID_Company'
    UPDATE  dbo.[_tDetailView_Detail]
    SET     DisplayProperty = 'CreatedBy' ,
            Caption = 'Created By' ,
            IsReadOnly = 1
    WHERE   Name = 'ID_CreatedBy'
    UPDATE  dbo.[_tDetailView_Detail]
    SET     DisplayProperty = 'LastModifiedBy' ,
            Caption = 'Last Modified By' ,
            IsReadOnly = 1
    WHERE   Name = 'ID_LastModifiedBy'
    UPDATE  dbo.[_tDetailView_Detail]
    SET     DisplayProperty = 'FilingStatus' ,
            Caption = 'Filing Status' ,
            IsReadOnly = 1
    WHERE   Name = 'ID_FilingStatus'
    UPDATE  dbo.[_tDetailView_Detail]
    SET     Caption = 'Active' ,
            ID_ControlType = 4
    WHERE   Name = 'IsActive'


    UPDATE  dbo.[_tDetailView_Detail]
    SET     Caption = 'Date Created' ,
            IsReadOnly = 1
    WHERE   Name = 'DateCreated'
    UPDATE  dbo.[_tDetailView_Detail]
    SET     Caption = 'Date Modified' ,
            IsReadOnly = 1
    WHERE   Name = 'DateModified'

    UPDATE  dbo.[_tListView_Detail]
    SET     Caption = 'Date Created'
    WHERE   Name = 'DateCreated'
    UPDATE  dbo.[_tListView_Detail]
    SET     Caption = 'Date Modified'
    WHERE   Name = 'DateModified'

    UPDATE  dbo.[_tListView_Detail]
    SET     Caption = 'Date Approved'
    WHERE   Name = 'DateApproved' 
    UPDATE  dbo.[_tListView_Detail]
    SET     Caption = 'Date Cancelled'
    WHERE   Name = 'DateCancelled'

    UPDATE  dbo.[_tDetailView_Detail]
    SET     Caption = 'Date Approved' ,
            IsDisabled = 1
    WHERE   Name = 'DateApproved'
    UPDATE  dbo.[_tDetailView_Detail]
    SET     Caption = 'Date Cancelled' ,
            IsDisabled = 1
    WHERE   Name = 'DateCancelled'
	
    UPDATE  dbo.[_tListView_Detail]
    SET     DisplayProperty = 'CreatedBy' ,
            Caption = 'Created By'
    WHERE   Name = 'ID_CreatedBy'
    UPDATE  dbo.[_tListView_Detail]
    SET     DisplayProperty = 'LastModifiedBy' ,
            Caption = 'Last Modified By'
    WHERE   Name = 'ID_LastModifiedBy'
    UPDATE  dbo.[_tListView_Detail]
    SET     DisplayProperty = 'FilingStatus' ,
            Caption = 'Filing Status'
    WHERE   Name = 'ID_FilingStatus'
    UPDATE  dbo.[_tListView_Detail]
    SET     Caption = 'Active'
    WHERE   Name = 'IsActive'


    UPDATE  dbo.[_tListView_Detail]
    SET     DisplayProperty = 'ApprovedBy' ,
            Caption = 'Approved By'
    WHERE   Name = 'ID_ApprovedBy'
    UPDATE  dbo.[_tListView_Detail]
    SET     DisplayProperty = 'CancelledBy' ,
            Caption = 'Cancelled By'
    WHERE   Name = 'ID_CancelledBy'

    UPDATE  dbo.[_tDetailView_Detail]
    SET     DisplayProperty = 'ApprovedBy' ,
            Caption = 'Approved By'
    WHERE   Name = 'ID_ApprovedBy'
    UPDATE  dbo.[_tDetailView_Detail]
    SET     DisplayProperty = 'CancelledBy' ,
            Caption = 'Cancelled By'
    WHERE   Name = 'ID_CancelledBy'

    UPDATE  dbo.[_tDetailView_Detail]
    SET     IsReadOnly = 1
    WHERE   Name IN ( 'ID', 'Oid' )
    UPDATE  dbo.[_tDetailView_Detail]
    SET     ID_ControlType = 2 ,
            Height = 250 ,
            ColSpan = 3 ,
            ColCount = NULL ,
            ID_LabelLocation = 2
    WHERE   Name IN ( 'Comment' )


    UPDATE  f
    SET     Caption = 'Cancellation  Reason' ,
            ID_Tab = ( SELECT TOP 1
                                t.Oid
                       FROM     dbo.[_tDetailView_Detail] t
                       WHERE    Name = 'StatusTab'
                                AND t.ID_DetailView = f.ID_DetailView
                     ) ,
            ID_ControlType = 2 ,
            IsDisabled = 1 ,
            IsReadOnly = 1 ,
            Height = 120 ,
            ID_LabelLocation = 2 ,
            ColSpan = 2 ,
            SeqNo = 10000
    FROM    dbo.[_tDetailView_Detail] f
    WHERE   Name = 'CancellationReason'

    UPDATE  dbo.[_tModel_Property]
    SET     ID_PropertyModel = '8A651C66-5CB8-4FFA-89A5-E7DF2AD0983F'
    WHERE   ID_PropertyModel IS NULL
            AND Name = 'ID_FilingStatus'

    UPDATE  dbo.[_tListView_Detail]
    SET     IsAddModelClass = 1
    WHERE   Name = 'ID_FilingStatus'
GO
