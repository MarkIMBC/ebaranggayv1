﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vPatient_Confinement]
AS
  SELECT H.*,
         UC.Name                        AS CreatedBy,
         UM.Name                        AS LastModifiedBy,
         client.Name                    Name_Client,
         CASE
           WHEN len(ISNULL(H.PatientNames, '')) > 0 THEN H.PatientNames
           ELSE patient.Name
         END                            Name_Patient,
         fs.Name                        Name_FilingStatus,
         ISNULL(fsBilling.Name, '----') BillingInvoice_Name_FilingStatus
  FROM   tPatient_Confinement H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tClient client
                ON client.ID = H.ID_Client
         LEFT JOIN tPatient patient
                ON patient.ID = H.ID_Patient
         LEFT JOIN tFilingStatus fs
                ON fs.ID = H.ID_FilingStatus
         LEFT JOIN tFilingStatus fsBilling
                ON fsBilling.ID = H.BillingInvoice_ID_FilingStatus

GO
