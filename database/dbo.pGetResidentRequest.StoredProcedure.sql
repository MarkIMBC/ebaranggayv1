﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[pGetResidentRequest] @ID                                         INT = -1,
                                    @ID_ResidentRequest_CommunityTaxCertificate INT,
                                    @ID_Resident                                INT = NULL,
                                    @ID_ResidentRequest                         INT = NULL,
                                    @ID_Session                                 INT = NULL
AS
  BEGIN
      SELECT '_',
             '' ResidentRequest_BarangayBusinessClearance,
             '' ResidentRequest_IndigencyCertificate,
             '' ResidentRequest_Certificate,
             '' ResidentRequest_CommunityTaxCertificate

      DECLARE @ID_User      INT,
              @ID_Warehouse INT
      DECLARE @Name_Resident VARCHAR(MAX) = ''
      DECLARE @ContactNumber_Resident VARCHAR(MAX) = ''
      DECLARE @Address_Resident VARCHAR(MAX) = ''
      DECLARE @ID_Gender_Resident INT
      DECLARE @Filed_ID_FilingStatus INT =1

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF( ISNULL(@ID_Resident, 0) > 0 )
        BEGIN
            SELECT @Name_Resident = Name,
                   @ContactNumber_Resident = ContactNumber,
                   @Address_Resident = Address,
                   @ID_Gender_Resident = ID_Gender
            FROM   vResident
            WHERE  ID = @ID_Resident
        END

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*, fs.Name Name_FilingStatus
            FROM   (SELECT NULL                    AS [_],
                           -1                      AS [ID],
                           NULL                    AS [Code],
                           @Name_Resident          AS [Name],
                           @ContactNumber_Resident ContactNumber,
                           @Address_Resident       Address,
                           @ID_Gender_Resident     ID_Gender,
                           GETDATE()               Date,
                           1                       AS [IsActive],
                           NULL                    AS [ID_Company],
                           NULL                    AS [Comment],
                           NULL                    AS [DateCreated],
                           NULL                    AS [DateModified],
                           NULL                    AS [ID_CreatedBy],
                           NULL                    AS [ID_LastModifiedBy],
                           @ID_ResidentRequest     AS ID_ResidentRequest,
                           @ID_Resident            AS ID_Resident,
                           @Filed_ID_FilingStatus  ID_FilingStatus) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tResident resident
                          ON H.ID_Resident = resident.ID
                   LEFT JOIN tFilingStatus fs
                          ON H.ID_FilingStatus = fs.ID
        END
      ELSE
        BEGIN
            SELECT H.*
            FROM   vResidentRequest H
            WHERE  H.ID = @ID
        END

      SELECT *
      FROM   vResidentRequest_BarangayBusinessClearance
      WHERE  ID_ResidentRequest = @ID

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*
            FROM   (SELECT -1        ID,
                           GetDate() DateCreated) H
        END
      ELSE
        BEGIN
            SELECT *
            FROM   vResidentRequest_IndigencyCertificate
            WHERE  ID_ResidentRequest = @ID
        END

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   gender.Name Name_Gender
            FROM   (SELECT -1                  ID,
                           @ID_Gender_Resident ID_Gender) H
                   LEFT JOIN tGender gender
                          on gender.ID = h.ID_Gender
        END
      ELSE
        BEGIN
            SELECT *
            FROM   vResidentRequest_Certificate
            WHERE  ID_ResidentRequest = @ID
        END

      SELECT *
      FROM   vResidentRequest_CommunityTaxCertificate
      WHERE  ID_ResidentRequest = @ID
  END

GO
