﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pAdd_Company_SMSSetting](@ID_Company        INT,
                                   @MaxSMSCountPerDay INT)
AS
  BEGIN
      Declare @Already_Exist_Count INT = 0
      DECLARE @CompanyName VARCHAR(400) = '';
      DECLARE @message VARCHAR(400) = '';

      /* Validate if Company Exis*/
      SELECT @Already_Exist_Count = COUNT(*)
      FROM   dbo.tCompany_SMSSetting
      WHERE  ID_Company = @ID_Company;

      IF @Already_Exist_Count > 0
        BEGIN
            SELECT @CompanyName = Name
            FROM   tCompany
            WHERE  ID = @ID_Company

            SET @message = @CompanyName + ' is already exist.';

            THROW 50001, @message, 1;
        END;

      INSERT INTO [dbo].[tCompany_SMSSetting]
                  ([ID_Company],
                   [MaxSMSCountPerDay],
                   [IsActive],
                   [DateCreated],
                   [DateModified])
      VALUES      ( @ID_Company,
                    @MaxSMSCountPerDay,
                    1,
                    GETDATE(),
                    GETDATE() )
  END

GO
