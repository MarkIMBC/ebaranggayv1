﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vSMSCountPerCompany_Patient_Vaccination_Schedule]
AS
  SELECT CONVERT(Date, DateSent) DateSent,
         vac.ID_Company,
         COUNT(*)                TotalSMSCount
  FROM   tPatient_Vaccination_Schedule vacSched
         INNER JOIN tPatient_Vaccination vac
                 ON vacSched.ID_Patient_Vaccination = vac.ID
  WHERE  DateSent IS NOT NULL
  GROUP  BY vac.ID_Company,
            CONVERT(Date, DateSent)

GO
