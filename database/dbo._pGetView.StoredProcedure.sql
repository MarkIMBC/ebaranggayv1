﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[_pGetView]
    @Oid UNIQUEIDENTIFIER
AS
    SELECT  '' AS [_]
    SELECT  H.Oid ,
            H.Code ,
            H.Name ,
            H.IsActive ,
            H.ID_ListView ,
            H.ID_Report ,
            H.ID_Dashboard ,
            H.ID_ViewType ,
            H.CustomViewPath ,
            H.ControllerPath ,
            H.Comment ,
            H.DateCreated ,
            H.ID_CreatedBy ,
            H.ID_LastModifiedBy ,
            H.DateModified ,
            H.ID_Model ,
            H.DataSource AS [$DataSource]
    FROM    dbo.[_tView] H
    WHERE   Oid = @Oid
GO
