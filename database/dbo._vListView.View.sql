﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[_vListView]
AS
    SELECT  
			H.DataSource AS [$DataSource],
			H.* ,
            M.Name AS Model,
			U.COLUMN_NAME AS PrimaryKey
    FROM    dbo.[_tListView] H
            LEFT JOIN dbo.[_tModel] M ON H.ID_Model = M.Oid
			LEFT JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE U ON OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA
                                                            + '.'
                                                            + QUOTENAME(CONSTRAINT_NAME)),
                                                            'IsPrimaryKey') = 1
                                                            AND U.TABLE_NAME = M.TableName
GO
