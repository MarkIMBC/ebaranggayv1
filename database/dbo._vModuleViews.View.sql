﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[_vModuleViews]
AS
SELECT Oid, Name, ID_Model , 2 AS ViewType FROM dbo._tDetailView tdv	
UNION ALL
SELECT Oid, Name, ID_Model , 1  FROM dbo._tListView tlv	


GO
