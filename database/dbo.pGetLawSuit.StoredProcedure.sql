﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROCEDURE [dbo].[pGetLawSuit] (@ID                      INT = -1,
                             @Complainant_ID_Resident INT = NULL,
                             @Respondent_ID_Resident  INT = NULL,
                             @ID_Session              INT = NULL)
AS
  BEGIN
      SELECT '_'

      DECLARE @ID_User      INT,
              @ID_Warehouse INT
      DECLARE @ID_FilingStatus INT = 1

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   tUserSession
      WHERE  ID = @ID_Session

      IF ( @ID = -1 )
        BEGIN
            SELECT H.*,
                   residentComplaintnat.Name Complainant_Name_Resident,
                   residentRespondent.Name   Respondent_Name_Resident,
                   fs.Name                   Name_FilingStatus,
                   lawsuitStatus.Name        Name_LawSuitStatus
            FROM   (SELECT NULL                     AS [_],
                           -1                       AS [ID],
                           NULL                     AS [Code],
                           NULL                     AS [Name],
                           GetDate()                AS [Date],
                           1                        AS [IsActive],
                           NULL                     AS [ID_Company],
                           NULL                     AS [Comment],
                           NULL                     AS [DateCreated],
                           NULL                     AS [DateModified],
                           NULL                     AS [ID_CreatedBy],
                           NULL                     AS [ID_LastModifiedBy],
                           ''                       AS ComplainantName,
                           ''                       AS RespondentName,
                           @Complainant_ID_Resident AS [Complainant_ID_Resident],
                           @Respondent_ID_Resident  AS [Respondent_ID_Resident],
                           @ID_FilingStatus         AS [ID_FilingStatus],
                           1                        ID_LawSuitStatus) H
                   LEFT JOIN tUser UC
                          ON H.ID_CreatedBy = UC.ID
                   LEFT JOIN tUser UM
                          ON H.ID_LastModifiedBy = UM.ID
                   LEFT JOIN tResident residentComplaintnat
                          ON H.Complainant_ID_Resident = residentComplaintnat.ID
                   LEFT JOIN tResident residentRespondent
                          ON H.Respondent_ID_Resident = residentRespondent.ID
                   LEFT JOIN tFilingStatus fs
                          ON H.ID_FilingStatus = fs.ID
                   LEFT JOIN tLawSuitStatus lawsuitStatus
                          ON H.ID_LawSuitStatus = lawsuitStatus.ID
        END
      ELSE
        BEGIN
            SELECT H.*,
                   H.ComplainantName _ComplainantName,
                   H.RespondentName  _RespondentName
            FROM   vLawSuit H
            WHERE  H.ID = @ID
        END
  END

GO
