﻿CREATE TYPE [dbo].[typActiveInactiveItem] AS TABLE(
	[ID_Item] [int] NULL,
	[IsActive] [bit] NULL
)
GO
