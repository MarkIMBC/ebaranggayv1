﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pImportService] (@records typImportService READONLY, @ID_UserSession INT)
AS
BEGIN

  DECLARE @ID_Company INT = 0;
  DECLARE @ID_User INT = 0;

  DECLARE @Success BIT = 1;
  DECLARE @message VARCHAR(300) = '';

  DECLARE @Old_procedure_ids TABLE (
    Old_procedure_id INT
  )


  SELECT
    @ID_User = ID_User
  FROM dbo.tUserSession
  WHERE ID = @ID_UserSession;

  SELECT
    @ID_Company = u.ID_Company
  FROM vUser u
  WHERE u.ID = @ID_User



  INSERT @Old_procedure_ids (Old_procedure_id)
    SELECT
      Old_procedure_id
    FROM @records
    EXCEPT
    SELECT
      i.Old_procedure_id
    FROM tItem i
    WHERE i.ID_Company = @ID_Company AND ISNULL(Old_procedure_id,0) <> 0 

 


  insert titem (old_procedure_id,
  id_itemtype,
  name,
  unitcost,
  unitprice,
  id_itemcategory,
  code,
  isactive,
  id_company,
  comment,
  datecreated,
  datemodified,
  id_createdby,
  id_lastmodifiedby)
    select
      rec.old_procedure_id
     ,1
     ,rec.procedure_name
     ,0
     ,rec.procedure_php
     ,null
     ,null
     ,1
     ,@id_company
     ,null
     ,getdate()
     ,getdate()
     ,@id_user
     ,@id_user
    from @records rec
    where old_procedure_id in (select
        old_procedure_id
      from @Old_procedure_ids)

  update titem
  set name = record.procedure_name
     ,unitprice = record.procedure_php
     ,datemodified = getdate()
  from titem i
  inner join @records record
    on i.old_procedure_id = record.old_procedure_id
  where i.id_company = @id_company


  SELECT
    '_';

  SELECT
    @Success Success
   ,@message message;


END

GO
