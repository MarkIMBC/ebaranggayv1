﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGetDashboardMunicipalityPregnancyDiagnosis] (@ID_UserSession INT,
                                                            @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Warehouse INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Warehouse = ID_Warehouse
      FROM   dbo.tUserSession
      WHERE  ID = @ID_UserSession;

      SELECT @ID_Company = ID_Company
      FROM   vUser
      WHERE  ID = @ID_User

      DECLARE @ClientDailyCount INT = 0
      DECLARE @ClientMonthlyCount INT = 0
      DECLARE @SalesDailyAmount DECIMAL(18, 2) = 0
      DECLARE @SalesMonthlyAmount DECIMAL(18, 2) = 0
      DECLARE @record TABLE
        (
           CountYear  INT,
           CountMonth VARCHAR(MAX),
           TotalCount DECIMAL(18, 2)
        )
      DECLARE @MonthNumber INT;

      SET @MonthNumber = 1;

      WHILE @MonthNumber <= 12
        BEGIN
            INSERT @record
            SELECT @DateYear,
                   DateName(month, DateAdd(month, @MonthNumber, -1)),
                   0.00

            SET @MonthNumber= @MonthNumber + 1
        END;

      DECLARE @PregnancyDiagnosis_ID_MedicalRecord INT = 0

      Update @record
      SET    TotalCount = tbl.TotalCount
      FROM   @record record
             INNER JOIN (SELECT YEAR(medicalRecord.Date)                                          as CountYear,
                                DateName(month, DateAdd(month, MONTH(medicalRecord.Date), 0) - 1) as CountMonth,
                                COUNT(*)                                                          AS TotalCount
                         FROM   vResident resident
                                inner join vResident_MedicalRecord medicalRecord
                                        on resident.ID = medicalRecord.ID_Resident
                                inner join vCompanyActive company
                                        on resident.ID_Company = company.ID
                         WHere  resident.ID_Company > 1
                                and resident.IsActive = 1
                                AND YEAR(medicalRecord.Date) = @DateYear
                         GROUP  BY YEAR(medicalRecord.Date),
                                   MONTH(medicalRecord.Date)) tbl
                     on tbl.CountMonth = record.CountMonth
                        and tbl.CountYear = record.CountYear

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear DateYear

      SELECT *
      FROM   @record
  END

GO
