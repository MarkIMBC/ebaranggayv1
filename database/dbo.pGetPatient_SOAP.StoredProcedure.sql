﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pGetPatient_SOAP] @ID                     INT = -1,  
                                    @ID_Client              INT = NULL,  
                                    @ID_Patient             INT = NULL,  
                                    @ID_SOAPType            INT = NULL,  
                                    @ID_Patient_Confinement INT = NULL,  
                                    @ID_Session             INT = NULL  
AS  
  BEGIN  
      SELECT '_',  
             '' AS LabImages,  
             '' AS Patient_SOAP_Plan,  
             '' AS Patient_SOAP_Prescription,  
             '' AS Patient_SOAP_Treatment;  
  
      DECLARE @FilingStatus_Confined INT = 14  
      DECLARE @FilingStatus_Discharged INT = 15  
      DECLARE @ID_User INT;  
      DECLARE @ID_Company INT;  
      DECLARE @AttendingPhysician_ID_Employee INT;  
      DECLARE @ID_Warehouse INT;  
      DECLARE @FILED_ID_FilingStatus INT = 1;  
      DECLARE @Consultation_ID_SOAPType INT = 1;  
      DECLARE @Confinement_ID_SOAPType INT = 2;  
      DECLARE @Patient_Confinement_ID_FilingStatus INT = 0;  
      DECLARE @IsDeceased BIT = 1;  
      DECLARE @PrimaryComplaintTemplate VARCHAR(MAX) = ''  
        + 'Eyes - Normal/Discharge/Infection/Sclelorosis/Inflamed/Eyelid tumor = '  
        + CHAR(9) + CHAR(13)  
        + 'Ears - Normal/Inflamed/Tumor/Dirty/Painful = '  
        + CHAR(9) + CHAR(13)  
        + 'Nose - Normal/Discharge = ' + CHAR(9)  
        + CHAR(13)  
        + 'Mouth,Teeth,Gums - Normal/Tumors/Gingivitis/Periodontitis/Tartar buildup/Loose teeth/Bite over/Under = '  
        + CHAR(9) + CHAR(13)  
        + 'Coat and skin - Normal/Scaly/Infection/Matted/Pruritus/Hair loss/Mass = '  
        + CHAR(9) + CHAR(13)  
        + 'Muskuloskeletal - Normal/Joint problems/Lameness/Nail Problems/Ligaments = '  
        + CHAR(9) + CHAR(13)  
        + 'Lungs - Normal/Breathing Difficulty/Rapid Respiration/ Tracheal Pinch + - / Congestion / Abn Sound = '  
        + CHAR(9) + CHAR(13)  
        + 'Heart - Normal/Murmur/Arrythmia/Muffled/Fast/Slow = '  
        + CHAR(9) + CHAR(13)  
        + 'GI System - Normal/Excessive Gas/Parasites/Abn Feces/Anorexia = '  
        + CHAR(9) + CHAR(13)  
        + 'Abdomen - Normal/Abnormal Mass/Tense/Painful/Bloated/Fluid/Hernia/Enlarged Organ = '  
        + CHAR(9) + CHAR(13)  
        + 'Urogentital - Normal/Abn Urination/Genital Discharge/Blood Seen/Abn Testicle = '  
        + CHAR(9) + CHAR(13)  
        + 'Neurological - Normal/Eye Reflex/Pain Reflex = '  
        + CHAR(9) + CHAR(13)  
        + 'Lymph Nodes - Normal/Submandubular/Prescapular/Axillary/Popiteal/Inguinal = '  
        + CHAR(9) + CHAR(13)  
      DECLARE @ObjectiveTemplate VARCHAR(MAX) = 'Heart Rate (bpm): ' + CHAR(9) + CHAR(13)  
        + 'Respiratory Rate (brpm): ' + CHAR(9)  
        + CHAR(13) + 'Weight (kg): ' + CHAR(9) + CHAR(13)  
        + 'Length (cm): ' + CHAR(9) + CHAR(13) + 'CRT: '  
        + CHAR(9) + CHAR(13) + 'BCS: ' + CHAR(9) + CHAR(13)  
        + 'Lymph Nodes: ' + CHAR(9) + CHAR(13)  
        + 'Palpebral Reflex: ' + CHAR(9) + CHAR(13)  
        + 'Temperature: ' + CHAR(9) + CHAR(13)  
      DECLARE @ClinicalExaminationTemplate VARCHAR(MAX) = 'Heart Rate (bpm): ' + CHAR(9) + CHAR(13)  
        + 'Respiratory Rate (brpm): ' + CHAR(9) + CHAR(13) 
		+ 'Weight (kg): ' + CHAR(9) + CHAR(13) 
        + 'Temperature: ' + CHAR(9) + CHAR(13)  
        + 'Length (cm): ' + CHAR(9) + CHAR(13) 
		+ 'CRT: '+ CHAR(9) + CHAR(13) + 'BCS: ' + CHAR(9) + CHAR(13)  
        + 'Lymph Nodes: ' + CHAR(9) + CHAR(13)  
        + 'Palpebral Reflex: ' + CHAR(9) + CHAR(13)  
		 
      DECLARE @AssessmentTemplate VARCHAR(MAX) ='Differential Diagnosis: ' + CHAR(9)  
       + CHAR(13) + 'Notes: ' + CHAR(9) + CHAR(13)  
       + 'Test Results: ' + CHAR(9) + CHAR(13)  
       + 'Final Diagnosis: ' + CHAR(9) + CHAR(13)  
       + 'Prognosis: ' + CHAR(9) + CHAR(13) + 'Category: '  
       + CHAR(9) + CHAR(13)  
      DECLARE @DiagnosisTemplate VARCHAR(MAX) ='Differential Diagnosis: ' + CHAR(9)  
       + CHAR(13) + 'Notes: ' + CHAR(9) + CHAR(13)  
       + 'Test Results: ' + CHAR(9) + CHAR(13)  
       + 'Final Diagnosis: ' + CHAR(9) + CHAR(13)  
       + 'Prognosis: ' + CHAR(9) + CHAR(13) + 'Category: '  
       + CHAR(9) + CHAR(13)  
      DECLARE @LaboratoryTemplate VARCHAR(MAX)= 'CBC: ' + CHAR(9) + CHAR(13) + '  Wbc= ' + CHAR(9)  
        + CHAR(13) + '  Lym= ' + CHAR(9) + CHAR(13)  
        + '  Mon= ' + CHAR(9) + CHAR(13) + '  Neu= ' + CHAR(9)  
        + CHAR(13) + '  Eos= ' + CHAR(9) + CHAR(13)  
        + '  Bas= ' + CHAR(9) + CHAR(13) + '  Rbc= ' + CHAR(9)  
        + CHAR(13) + '  Hgb= ' + CHAR(9) + CHAR(13)  
        + '  Hct= ' + CHAR(9) + CHAR(13) + '  Mcv= ' + CHAR(9)  
        + CHAR(13) + '  Mch= ' + CHAR(9) + CHAR(13)  
        + '  Mchc ' + CHAR(9) + CHAR(13) + '  Plt= ' + CHAR(9)  
        + CHAR(13) + '  Mpv= ' + CHAR(9) + CHAR(13) + CHAR(9)  
        + CHAR(13) + 'Blood Chem: ' + CHAR(9) + CHAR(13)  
        + '  Alt= ' + CHAR(9) + CHAR(13) + '  Alp= ' + CHAR(9)  
        + CHAR(13) + '  Alb= ' + CHAR(9) + CHAR(13)  
        + '  Amy= ' + CHAR(9) + CHAR(13) + '  Tbil= '  
        + CHAR(9) + CHAR(13) + '  Bun= ' + CHAR(9) + CHAR(13)  
        + '  Crea= ' + CHAR(9) + CHAR(13) + '  Ca= ' + CHAR(9)  
        + CHAR(13) + '  Phos= ' + CHAR(9) + CHAR(13)  
        + '  Glu= ' + CHAR(9) + CHAR(13) + '  Na= ' + CHAR(9)  
        + CHAR(13) + '  K= ' + CHAR(9) + CHAR(13) + '  TP= '  
        + CHAR(9) + CHAR(13) + '  Glob= ' + CHAR(9) + CHAR(13)  
        + CHAR(9) + CHAR(13) + 'Microscopic Exam: '  
        + CHAR(9) + CHAR(13)  
  
      SELECT @ID_User = ID_User,  
             @ID_Warehouse = ID_Warehouse  
      FROM   dbo.tUserSession  
      WHERE  ID = @ID_Session;  
  
      SELECT @ID_Company = ID_Company  
      FROM   vUser  
      where  ID = @ID_User  
  
      DECLARE @Compostela_ID_Company INT = 65  
      DECLARE @DandCVeterinaryClinic_ID_Company INT = 63  
      DECLARE @GarciaVeterinaryClinicVeterinaryClinic_ID_Company INT = 75  
      DECLARE @PetlinkGuiguintoID_Company INT = 29  
      DECLARE @VenVet_ID_Company INT = 12  
  
      IF( @ID_Company = @VenVet_ID_Company )  
        BEGIN  
            SET @PrimaryComplaintTemplate= '' + 'Eyes = ' + CHAR(9) + CHAR(13) + 'Ears = '  
                                           + CHAR(9) + CHAR(13) + 'Nose = ' + CHAR(9) + CHAR(13)  
                                           + 'Mouth,Teeth,Gums = ' + CHAR(9) + CHAR(13)  
                                           + 'Coat and skin = ' + CHAR(9) + CHAR(13)  
                                           + 'Muskuloskeletal = ' + CHAR(9) + CHAR(13)  
                                           + 'Lungs = ' + CHAR(9) + CHAR(13) + 'Heart = '  
                                           + CHAR(9) + CHAR(13) + 'GI System = ' + CHAR(9)  
                                           + CHAR(13) + 'Abdomen = ' + CHAR(9) + CHAR(13)  
                                           + 'Urogentital = ' + CHAR(9) + CHAR(13)  
                                           + 'Neurological = ' + CHAR(9) + CHAR(13)  
                                           + 'Lymph Nodes = ' + CHAR(9) + CHAR(13)  
        END  
  
      IF(SELECT COUNT(*)  
         FROM   vUSER  
         where  ID_COmpany = @Compostela_ID_Company  
                AND ID = @ID_User) > 0  
        BEGIN  
            SELECT @AttendingPhysician_ID_Employee = ID  
            FROM   tEmployee  
            WHERE  ID = 357  
        END  
  
      IF(SELECT COUNT(*)  
         FROM   vUSER  
         where  ID_COmpany = @DandCVeterinaryClinic_ID_Company  
                AND ID = @ID_User) > 0  
        BEGIN  
            SELECT @AttendingPhysician_ID_Employee = ID  
            FROM   tEmployee  
            WHERE  ID = 345  
        END    
      IF(SELECT COUNT(*)  
         FROM   vUSER  
         where  ID_COmpany = @GarciaVeterinaryClinicVeterinaryClinic_ID_Company  
                AND ID = @ID_User) > 0  
        BEGIN  
            SELECT @AttendingPhysician_ID_Employee = ID  
            FROM   tEmployee  
            WHERE  ID = 426  
        END  
  
      IF(SELECT COUNT(*)  
         FROM   vUSER  
         where  ID_COmpany = @PetlinkGuiguintoID_Company  
                AND ID = @ID_User) > 0  
        BEGIN  
            SELECT @AttendingPhysician_ID_Employee = ID  
            FROM   tEmployee  
            WHERE  ID = 216  
        END  
  
      /* Patient Confinement */  
      IF(SELECT COUNT(*)  
         FROM   tPatient_Confinement  
         WHERE  ID_Patient_SOAP = @ID  
                AND ID_FilingStatus IN ( @FilingStatus_Confined, @FilingStatus_Discharged )) > 0  
        BEGIN  
            SELECT @ID_Patient_Confinement = ID  
            FROM   tPatient_Confinement  
            WHERE  ID_Patient_SOAP = @ID  
                   AND ID_FilingStatus IN ( @FilingStatus_Confined, @FilingStatus_Discharged )  
        END  
  
      IF( ISNULL(@ID_Patient_Confinement, 0) > 0 )  
        BEGIN  
            SET @ID_SOAPType = @Confinement_ID_SOAPType  
  
            SELECT @ID_Client = patient.ID_Client,  
                   @ID_Patient = ID_Patient,  
                   @Patient_Confinement_ID_FilingStatus = hed.ID_FilingStatus  
            FROM   tPatient_Confinement hed  
                   LEFT JOIN tPatient patient  
                          ON hed.ID_Patient = patient.ID  
            WHERE  hed.ID = @ID_Patient_Confinement  
        END  
  
      /* Patient Confinement END*/  
      IF ISNULL(@ID_Patient, 0) <> 0  
        BEGIN  
            SELECT @IsDeceased = Isnull(IsDeceased, 0),  
                   @ID_Client = ID_Client  
            FROM   tPatient  
            WHERE  ID = @ID_Patient;  
        END  
  
      DECLARE @LabImage TABLE  
        (  
           ImageRowIndex INT,  
           RowIndex      INT,  
           ImageNo       VARCHAR(MAX),  
           FilePath      VARCHAR(MAX),  
           Remark        VARCHAR(MAX)  
        );  
  
      INSERT @LabImage  
             (ImageRowIndex,  
              RowIndex,  
              ImageNo,  
              FilePath,  
              Remark)  
      SELECT ImageRowIndex,  
             RowIndex,  
             ImageNo,  
             FilePath,  
             Remark  
      from   dbo.fGetPatient_SOAP_LaboratoryImages(@ID)  
  
      Declare @IsLastConfinmentSOAP BIT = 0  
      Declare @maxDate DateTime  
  
      SELECT @maxDate = MaxDate  
      FROM   vPatient_Confinement_MaxSOAP  
      where  ID_Patient_SOAP = @ID  
  
      if( @maxDate IS NOT NULL  
          AND @Patient_Confinement_ID_FilingStatus = @FilingStatus_Discharged )  
        SET @IsLastConfinmentSOAP = 1  
      ELSE  
        SET @IsLastConfinmentSOAP = 0  
  
      IF ( @ID = -1 )  
        BEGIN  
            SET @ID_SOAPType = ISNULL(@ID_SOAPType, @Consultation_ID_SOAPType)  
  
            SELECT H.*,  
                   patient.Name                               Name_Patient,  
                   patient.Name_Client,  
                   fs.Name                                    Name_FilingStatus,  
                   soapType.Name                              Name_SOAPType,  
                   AttendingPhysician.Name                    AttendingPhysician_Name_Employee,  
                   confinement.Code                           Code_Patient_Confinement,  
                   confinement.Date                           Date_Patient_Confinement,  
                   confinement.DateDischarge                  DateDischarge_Patient_Confinement,  
                   CASE  
                     WHEN @Patient_Confinement_ID_FilingStatus = 0 THEN confinement.ID_FilingStatus  
                     ELSE @Patient_Confinement_ID_FilingStatus  
                   END,  
                   confinement.ID_FilingStatus                ID_FilingStatus_Patient_Confinement,  
                   confinement.Name_FilingStatus              Name_FilingStatus_Patient_Confinement,  
                   dbo.fGetAge(patient.DateBirth, case  
                                                    WHEN ISNULL(patient.IsDeceased, 0) = 1 then patient.DateDeceased  
                                                    ELSE NULL  
                                                  END, 'N/A') Age_Patient  
            FROM   (SELECT NULL                                 AS [_],  
                           -1                                   AS [ID],  
                           '-New-'                              AS [Code],  
                           NULL                                 AS [Name],  
                           1                                    AS [IsActive],  
                           NULL                                 AS [ID_Company],  
                           NULL                                 AS [Comment],  
                           NULL                                 AS [DateCreated],  
                           NULL                                 AS [DateModified],  
                           @ID_User                             AS [ID_CreatedBy],  
                           NULL                                 AS [ID_LastModifiedBy],  
                           @ID_Client                           AS ID_Client,  
                           @ID_Patient                          AS ID_Patient,  
                           @AttendingPhysician_ID_Employee      AS AttendingPhysician_ID_Employee,  
                           @ID_Patient_Confinement              AS ID_Patient_Confinement,  
                           GETDATE()                            Date,  
                           @FILED_ID_FilingStatus               ID_FilingStatus,  
                           @IsDeceased                          IsDeceased,  
                           @ID_SOAPType                         ID_SOAPType,  
                           @Patient_Confinement_ID_FilingStatus Patient_Confinement_ID_FilingStatus,  
                           @PrimaryComplaintTemplate            PrimaryComplaintTemplate,  
                           @ObjectiveTemplate                   ObjectiveTemplate,  
                           @AssessmentTemplate                  AssessmentTemplate,  
                           @LaboratoryTemplate                  LaboratoryTemplate,  
                           @ClinicalExaminationTemplate         ClinicalExaminationTemplate,  
                           @DiagnosisTemplate                   DiagnosisTemplate,  
                           @IsLastConfinmentSOAP                IsLastConfinmentSOAP) H  
                   LEFT JOIN dbo.tUser UC  
                          ON H.ID_CreatedBy = UC.ID  
                   LEFT JOIN dbo.tUser UM  
                          ON H.ID_LastModifiedBy = UM.ID  
                   LEFT JOIN dbo.vClient client  
                          ON client.ID = H.ID_Client  
                   LEFT JOIN dbo.vPatient patient  
                          ON patient.ID = H.ID_Patient  
                   LEFT JOIN dbo.tFilingStatus fs  
                          ON fs.ID = H.ID_FilingStatus  
                   LEFT JOIN dbo.tSOAPType soapType  
                          ON soapType.ID = H.ID_SOAPType  
                   LEFT JOIN vPatient_Confinement confinement  
                          ON confinement.ID = H.ID_Patient_Confinement  
                   LEFT JOIN tEmployee AttendingPhysician  
                          on AttendingPhysician.ID = H.AttendingPhysician_ID_Employee  
        END;  
      ELSE  
        BEGIN  
            SELECT H.*,  
                   @Patient_Confinement_ID_FilingStatus       Patient_Confinement_ID_FilingStatus,  
                   patient.IsDeceased,  
                   @PrimaryComplaintTemplate                  PrimaryComplaintTemplate,  
                   @ObjectiveTemplate                         ObjectiveTemplate,  
                   @AssessmentTemplate                        AssessmentTemplate,  
                   @LaboratoryTemplate                        LaboratoryTemplate,  
                   @ClinicalExaminationTemplate               ClinicalExaminationTemplate,  
                   @DiagnosisTemplate                         DiagnosisTemplate,  
                   dbo.fGetAge(patient.DateBirth, case  
                                                    WHEN ISNULL(patient.IsDeceased, 0) = 1 then patient.DateDeceased  
                                                    ELSE NULL  
                                                  END, 'N/A') Age_Patient,  
                   @IsLastConfinmentSOAP                      IsLastConfinmentSOAP  
            FROM   dbo.vPatient_SOAP H  
                   LEFT JOIN tPatient patient  
                          on h.ID_Patient = patient.ID  
            WHERE  H.ID = @ID;  
        END;  
  
      SELECT *  
      FROM   @LabImage  
      ORDER  BY ImageRowIndex DESC;  
  
      SELECT *  
      FROM   dbo.vPatient_SOAP_Plan  
      WHERE  ID_Patient_SOAP = @ID  
      ORDER  BY DateReturn ASC;  
  
      SELECT *  
      FROM   dbo.vPatient_SOAP_Prescription  
      WHERE  ID_Patient_SOAP = @ID  
  
      SELECT *  
      FROM   dbo.vPatient_SOAP_Treatment  
      WHERE  ID_Patient_SOAP = @ID  
  END;   
GO
