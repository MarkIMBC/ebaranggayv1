﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tResident](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_Gender] [int] NULL,
	[ContactNumber] [varchar](300) NULL,
	[Address] [varchar](300) NULL,
	[IsVaccinated] [bit] NULL,
	[DateLastVaccination] [datetime] NULL,
	[IsPermanent] [bit] NULL,
	[IsMigrante] [bit] NULL,
	[IsTransient] [bit] NULL,
	[ID_CivilStatus] [int] NULL,
	[ID_EducationalLevel] [int] NULL,
	[ID_HomeOwnershipStatus] [int] NULL,
	[MigrantAddress] [varchar](300) NULL,
	[ID_OccupationalStatus] [int] NULL,
	[Occupation] [varchar](300) NULL,
	[Lastname] [varchar](300) NULL,
	[Firstname] [varchar](300) NULL,
	[Middlename] [varchar](300) NULL,
	[Suffix] [varchar](300) NULL,
	[ImportGuid] [varchar](300) NULL,
	[DateBirth] [datetime] NULL,
	[ImageProfilePicFilename] [varchar](300) NULL,
	[IsPregnant] [bit] NULL,
	[IsHeadofFamily] [bit] NULL,
	[IsVoter] [bit] NULL,
	[isSoloParent] [bit] NULL,
	[IsOFW] [bit] NULL,
	[IsPWD] [bit] NULL,
	[IsBoarder] [bit] NULL,
	[IsDeceased] [bit] NULL,
	[ReferenceID] [varchar](300) NULL,
	[ID_HouseholdNumber] [int] NULL,
	[AssignedBHW_ID_Employee] [int] NULL,
	[StayInDuration] [varchar](300) NULL,
	[ID_Religion] [int] NULL,
	[BirthPlace] [varchar](300) NULL,
	[SourceOfIncome] [varchar](300) NULL,
	[SourceOfIncomeAmount] [varchar](300) NULL,
	[VoteInfoPrecinctNumber] [varchar](300) NULL,
	[AssignedBNS_ID_Employee] [int] NULL,
	[Source_ID_Resident_Family] [int] NULL,
 CONSTRAINT [PK_tResident] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tResident] ON [dbo].[tResident]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tResident] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tResident]  WITH CHECK ADD  CONSTRAINT [FK_tResident_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tResident] CHECK CONSTRAINT [FK_tResident_ID_Company]
GO
ALTER TABLE [dbo].[tResident]  WITH CHECK ADD  CONSTRAINT [FK_tResident_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tResident] CHECK CONSTRAINT [FK_tResident_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tResident]  WITH CHECK ADD  CONSTRAINT [FK_tResident_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tResident] CHECK CONSTRAINT [FK_tResident_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tResident]
		ON [dbo].[tResident]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tResident
			SET    DateCreated = GETDATE()
			FROM   dbo.tResident hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tResident] ENABLE TRIGGER [rDateCreated_tResident]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tResident]
		ON [dbo].[tResident]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tResident
			SET    DateModified = GETDATE()
			FROM   dbo.tResident hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tResident] ENABLE TRIGGER [rDateModified_tResident]
GO
