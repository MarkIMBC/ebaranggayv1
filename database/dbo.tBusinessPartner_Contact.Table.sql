﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tBusinessPartner_Contact](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_BusinessPartner] [int] NULL,
	[FirstName] [varchar](300) NULL,
	[LastName] [varchar](300) NULL,
	[MiddleName] [varchar](300) NULL,
	[Position] [varchar](300) NULL,
 CONSTRAINT [PK_tBusinessPartner_Contact] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tBusinessPartner_Contact] ADD  CONSTRAINT [DF__tBusiness__IsAct__31F75A1E]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tBusinessPartner_Contact]  WITH NOCHECK ADD  CONSTRAINT [FK_tBusinessPartner_Contact_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tBusinessPartner_Contact] CHECK CONSTRAINT [FK_tBusinessPartner_Contact_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tBusinessPartner_Contact]  WITH NOCHECK ADD  CONSTRAINT [FK_tBusinessPartner_Contact_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tBusinessPartner_Contact] CHECK CONSTRAINT [FK_tBusinessPartner_Contact_ID_LastModifiedBy]
GO
ALTER TABLE [dbo].[tBusinessPartner_Contact]  WITH NOCHECK ADD  CONSTRAINT [FKtBusinessPartner_Contact_ID_BusinessPartner] FOREIGN KEY([ID_BusinessPartner])
REFERENCES [dbo].[tBusinessPartner] ([ID])
GO
ALTER TABLE [dbo].[tBusinessPartner_Contact] CHECK CONSTRAINT [FKtBusinessPartner_Contact_ID_BusinessPartner]
GO
