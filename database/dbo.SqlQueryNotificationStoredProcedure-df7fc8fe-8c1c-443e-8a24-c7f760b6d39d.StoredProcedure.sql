﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SqlQueryNotificationStoredProcedure-df7fc8fe-8c1c-443e-8a24-c7f760b6d39d] AS BEGIN BEGIN TRANSACTION; RECEIVE TOP(0) conversation_handle FROM [SqlQueryNotificationService-df7fc8fe-8c1c-443e-8a24-c7f760b6d39d]; IF (SELECT COUNT(*) FROM [SqlQueryNotificationService-df7fc8fe-8c1c-443e-8a24-c7f760b6d39d] WHERE message_type_name = 'http://schemas.microsoft.com/SQL/ServiceBroker/DialogTimer') > 0 BEGIN if ((SELECT COUNT(*) FROM sys.services WHERE name = 'SqlQueryNotificationService-df7fc8fe-8c1c-443e-8a24-c7f760b6d39d') > 0)   DROP SERVICE [SqlQueryNotificationService-df7fc8fe-8c1c-443e-8a24-c7f760b6d39d]; if (OBJECT_ID('SqlQueryNotificationService-df7fc8fe-8c1c-443e-8a24-c7f760b6d39d', 'SQ') IS NOT NULL)   DROP QUEUE [SqlQueryNotificationService-df7fc8fe-8c1c-443e-8a24-c7f760b6d39d]; DROP PROCEDURE [SqlQueryNotificationStoredProcedure-df7fc8fe-8c1c-443e-8a24-c7f760b6d39d]; END COMMIT TRANSACTION; END
GO
