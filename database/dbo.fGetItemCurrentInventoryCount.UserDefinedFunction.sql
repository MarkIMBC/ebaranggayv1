﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fGetItemCurrentInventoryCount]
(
    @ID_Item INT
)
RETURNS INT
AS
BEGIN

    DECLARE @CurrentInventoryCount INT = 0;

    SELECT @CurrentInventoryCount = SUM(Quantity)
    FROM dbo.tInventoryTrail
    WHERE ID_Item = @ID_Item;

    RETURN @CurrentInventoryCount;
END;
GO
