﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE    
 VIEW [dbo].[vzBarangayComplaintReport]
AS
  SELECT lawSuit.ID,
         lawSuit.Complainant_Name_Resident,
         lawSuit.Respondent_Name_Resident,
         lawSuit.Date,
         lawSuit.RespondentSMSNumber,
         lawSuit.ComplainantSMSNumber,
         lawSuit.Comment,
         company.ID            ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.Name          Name_Company,
         company.Address       Address_Company,
         company.ContactNumber ContactNumber_Company
  FROM   vLawSuit lawSuit
         INNER JOIN vCompany company
                 ON company.ID = lawSuit.ID_Company

GO
