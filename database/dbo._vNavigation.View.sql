﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[_vNavigation]
AS
SELECT hed.*,
       v.ID_ViewType
FROM _tNavigation hed
    LEFT JOIN dbo._tView v
        ON v.Oid = hed.ID_View;

GO
