﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pGetSendSOAPPlan](@Date      DATETIME,
                                    @IsSMSSent BIT = NULL)
AS
  BEGIN
      DECLARE @Success BIT = 1;

      SELECT '_',
             '' AS summary,
             '' AS records;

      DECLARE @record TABLE
        (
           ID_Company           INT,
           Name_Company         VARCHAR(MAX),
           Name_Client          VARCHAR(MAX),
           ContactNumber_Client VARCHAR(MAX),
           DateReturn           DATETIME,
           Name_Item            VARCHAR(MAX),
           Comment              VARCHAR(MAX),
           Message              VARCHAR(MAX),
           DateSending          DATETIME,
           DateCreated          DATETIME,
           ID_Reference         INT,
           Oid_Model            VARCHAR(MAX),
           Code                 VARCHAR(MAX)
        )
      DECLARE @CompanySMSSetting TABLE
        (
           ID_Company        INT,
           MaxSMSCountPerDay INT
        )

      INSERT @CompanySMSSetting
      SELECT ID_Company,
             Max(MaxSMSCountPerDay)
      FROM   tCompany_SMSSetting
      WHERE  IsActive = 1
      GROUP  BY ID_Company

      INSERT @record
      SELECT *
      FROM   dbo.[fGetSendSoapPlan](@Date, @IsSMSSent, '')

      SELECT @Success Success;

      SELECT Format(DateSending, 'yyyy-MM-dd') DateSending,
             tbl.Name_Company,
             Count(*)                          Count,
             Sum(CASE
                   WHEN len(tbl.Message) <= 160 THEN 1
                   ELSE
                     CASE
                       WHEN len(tbl.Message) <= 306 THEN 2
                       ELSE
                         CASE
                           WHEN len(tbl.Message) <= 459 THEN 3
                           ELSE 4
                         END
                     END
                 END)                          TotalConsumedSMSCredit
      FROM   (SELECT *
              FROM   @record) tbl
      GROUP  BY Format(DateSending, 'yyyy-MM-dd'),
                Name_Company
      ORDER  BY DateSending DESC,
                Name_Company

      SELECT rec.*,
             CASE
               WHEN len(Message) <= 160 THEN 1
               ELSE
                 CASE
                   WHEN len(Message) <= 306 THEN 2
                   ELSE
                     CASE
                       WHEN len(Message) <= 459 THEN 3
                       ELSE 4
                     END
                 END
             END          ConsumedSMSCredit,
             len(Message) CharLength
      FROM   @record rec
             INNER JOIN tCompany com
                     ON com.ID = rec.ID_Company
             INNER JOIN @CompanySMSSetting comSmsSetting
                     ON comSmsSetting.ID_Company = com.ID
      ORDER  BY Format(DateSending, 'yyyy-MM-dd') DESC,
                com.ID_PackagePlan DESC,
                Name_Company,
                Name_Client
  END

GO
