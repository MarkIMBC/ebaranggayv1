﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vSoloParent]
AS
  SELECT H.*,
         UC.Name     AS CreatedBy,
         UM.Name     AS LastModifiedBy,
         family.Name AS Name_Relationship
  FROM   tSoloParent H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tFamilyRelationshipType family
                ON H.ID_RelationshipType = family.ID

GO
