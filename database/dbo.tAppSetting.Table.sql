﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tAppSetting](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_PropertyType] [int] NULL,
	[StringValue] [varchar](1000) NULL,
	[IntValue] [int] NULL,
	[DecimalValue] [decimal](18, 4) NULL,
	[DateTimeValue] [datetime] NULL,
	[BoolValue] [bit] NULL,
	[ImageValue] [varchar](500) NULL,
	[ColorValue] [varchar](300) NULL,
 CONSTRAINT [PK_tAppSetting] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tAppSetting] ADD  CONSTRAINT [DF__tAppSetti__IsAct__379B24DB]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tAppSetting]  WITH NOCHECK ADD  CONSTRAINT [FK_tAppSetting_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tAppSetting] CHECK CONSTRAINT [FK_tAppSetting_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tAppSetting]  WITH NOCHECK ADD  CONSTRAINT [FK_tAppSetting_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tAppSetting] CHECK CONSTRAINT [FK_tAppSetting_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tAppSetting]
		ON [dbo].[tAppSetting]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tAppSetting
			SET    DateCreated = GETDATE()
			FROM   dbo.tAppSetting hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tAppSetting] ENABLE TRIGGER [rDateCreated_tAppSetting]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tAppSetting]
		ON [dbo].[tAppSetting]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tAppSetting
			SET    DateModified = GETDATE()
			FROM   dbo.tAppSetting hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tAppSetting] ENABLE TRIGGER [rDateModified_tAppSetting]
GO
