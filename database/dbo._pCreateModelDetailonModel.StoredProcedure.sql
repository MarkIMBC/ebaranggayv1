﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[_pCreateModelDetailonModel] @TableName VARCHAR(100), @Oid_Model UNIQUEIDENTIFIER, @IsDetail BIT
  AS
 BEGIN
  IF @IsDetail = 1
     BEGIN
      EXEC _pCreateTable_Detail @TableName
    END
  ELSE 
     BEGIN
      EXEC _pCreateTable @TableName 
     END

  EXEC _pCreateAppModule @TableName, 0 , 0

  DECLARE @ParentTableName VARCHAR(100), @ParentModelName VARCHAR(100), @PrimaryKey uniqueidentifier

  SELECT @ParentTableName = TableName, @ParentModelName = Name, @PrimaryKey = PrimaryKey  FROM _vModel WHERE Oid = @Oid_Model

  DECLARE @Key VARCHAR(100) = 'ID_' + @ParentModelName

  DECLARE	 @Type INT=  (CASE	 WHEN	 @PrimaryKey = 'ID' THEN 2 ELSE 8 END )

  EXEC _pAddModelProperty @TableName, @Key, @Type 

  EXEC _pCreateForeignKey @TableName, @Key, @ParentTableName, @PrimaryKey

  EXEC _pAddModelDetail @TableName, @Key, @ParentTableName, NULL
  
END
GO
