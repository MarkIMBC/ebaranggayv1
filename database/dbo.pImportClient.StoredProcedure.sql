﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pImportClient] (@records typImportClient READONLY, @ID_UserSession INT)
AS
BEGIN

  DECLARE @ID_Company INT = 0;
  DECLARE @ID_User INT = 0;

  DECLARE @Success BIT = 1;
  DECLARE @message VARCHAR(300) = '';

  DECLARE @Old_client_ids TABLE (
    Old_client_id INT
  )


  SELECT
    @ID_User = ID_User
  FROM dbo.tUserSession
  WHERE ID = @ID_UserSession;

  SELECT
    @ID_Company = u.ID_Company
  FROM vUser u
  WHERE u.ID = @ID_User

  INSERT @Old_client_ids (Old_client_id)
    SELECT
      CONVERT(INT, Old_client_id)
    FROM @records
    EXCEPT
    SELECT
      c.Old_client_id
    FROM tClient c
    WHERE c.ID_Company = @ID_Company


  INSERT dbo.tClient (Code,
  Name,
  IsActive,
  ID_Company,
  Comment,
  DateCreated,
  DateModified,
  ID_CreatedBy,
  ID_LastModifiedBy,
  ContactNumber,
  Email,
  Address,
  ContactNumber2,
  Old_client_id)
    SELECT
      Old_client_code
     ,name
     ,1
     ,@ID_Company
     ,''
     ,GETDATE()
     ,GETDATE()
     ,@ID_User
     ,@ID_User
     ,ContactNumber
     ,Email
     ,Address
     ,''
     ,CONVERT(INT, Old_client_id)
    FROM @records records
    WHERE CONVERT(INT, Old_client_id)  IN (SELECT
        CONVERT(INT, Old_client_id)
      FROM @Old_client_ids)

  SELECT
    '_';

  SELECT
    @Success Success
   ,@message message;

END

GO
