﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pGetPatient_DentalExamination]    
(    
    @ID INT = -1,    
    @ID_Session INT = NULL    
)    
AS    
BEGIN    
    
    SELECT '_';    
    
    
    IF (@ID = -1)    
    BEGIN    
        SELECT 
			H.*,  
			status.Name Name_FilingStatus
        FROM    
        (    
            SELECT NULL AS [_],    
                   -1 AS [ID],    
                   '--NEW--' AS [Code],    
                   NULL AS [Name],    
                   1 AS [IsActive],    
				   GetDate() as Date,
                   NULL AS [ID_Company],    
                   NULL AS [Comment],    
                   NULL AS [DateCreated],    
                   NULL AS [DateModified],    
                   NULL AS [ID_CreatedBy],    
                   NULL AS [ID_LastModifiedBy] ,  
					1 AS ID_FilingStatus,
					3 AS ID_Dentition
        ) H    
            LEFT JOIN tUser UC    
                ON H.ID_CreatedBy = UC.ID    
            LEFT JOIN tUser UM    
                ON H.ID_LastModifiedBy = UM.ID
			LEFT JOIN dbo.tFilingStatus status      
        ON status.ID = h.ID_FilingStatus   
    END;    
    ELSE    
    BEGIN    
        SELECT H.*    
        FROM dbo.vPatient_DentalExamination H    
        WHERE H.ID = @ID;    
    END;    
    
END;  
  
  
GO
