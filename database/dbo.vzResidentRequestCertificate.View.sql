﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  
 VIEW [dbo].[vzResidentRequestCertificate]
AS
  SELECT Resident.Name         NAME,
         Resident.ID           ID,
         company.ID            ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.Name          Name_Company,
         company.Address       Address_Company,
         company.ContactNumber ContactNumber_Company
  FROM   vResidentRequest_Certificate Resident
         INNER JOIN vCompany company
                 ON company.ID = Resident.ID_Company

GO
