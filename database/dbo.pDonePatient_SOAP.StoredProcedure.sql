﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pDonePatient_SOAP] (@IDs_Patient_SOAP typIntList READONLY,
                                      @ID_UserSession   INT)
AS
  BEGIN
      DECLARE @Doned_ID_FilingStatus INT = 13;
      DECLARE @Pending_ID_FilingStatus INT = 2;
      DECLARE @IDs_Patient typIntList
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      BEGIN TRY
          DECLARE @ID_User INT = 0;

          SELECT @ID_User = ID_User
          FROM   dbo.tUserSession
          WHERE  ID = @ID_UserSession;

          EXEC dbo.pDonePatient_SOAP_validation
            @IDs_Patient_SOAP,
            @ID_UserSession;

          INSERT @IDs_Patient
          SELECT patient.ID
          FROM   tPatient_SOAP soap
                 INNER JOIN tPatient patient
                         ON patient.ID = soap.ID_Patient
                 INNER JOIN @IDs_Patient_SOAP idsSOAP
                         ON idsSOAP.ID = soap.ID

          EXEC dbo.pUpdatePatientsLastVisitedDate
            @IDs_Patient

          UPDATE dbo.tPatient_SOAP
          SET    ID_FilingStatus = @Doned_ID_FilingStatus,
                 DateDone = GETDATE(),
                 ID_DoneBy = @ID_User
          FROM   dbo.tPatient_SOAP bi
                 INNER JOIN @IDs_Patient_SOAP ids
                         ON bi.ID = ids.ID;

          --exec pTransferConfinementItemsServicesFromSOAP
          --  @IDs_Patient_SOAP
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;
      END CATCH;

      SELECT '_';

      SELECT @Success Success,
             @message message;
  END; 
GO
