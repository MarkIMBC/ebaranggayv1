﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 FUNCTION [dbo].[fGetCompany_Subscription](@ID_Company  INT,
                                        @CurrentDate DateTIME)
RETURNS @table TABLE (
  ID_Company           INT,
  Name_Company         VARCHAR(MAX),
  DateStart            DateTime,
  DateEnd              DateTime,
  DayDuration          INT,
  DayRemainingDuration INT,
  IsSubscribed         BIT )
AS
  BEGIN
      DECLARE @Count INT = 0
      DECLARE @DateStart DateTIME
      DECLARE @DateEnd DateTIME
      DECLARE @IsSubscribe Bit = 0

      INSERT @table
      SELECT TOP 1 company.ID,
                   company.Name,
                   _subscription.DateStart,
                   _subscription.DateEnd,
                   DateDIFF(Day, _subscription.DateStart, _subscription.DateEnd),
                   DateDIFF(Day, @CurrentDate, _subscription.DateEnd),
                   1
      FROM   [tCompany_Subscription] _subscription
             inner JOIN tCompany company
                     on _subscription.ID_Company = company.ID
      where  ID_Company = @ID_Company
             AND CONVERT(DATE, @CurrentDate) BETWEEN CONVERT(DATE, DateStart) AND CONVERT(DATE, DateEnd)
             and _subscription.IsActive = 1
      ORDER  BY DateEnd DESC,
                DateStart DESC

      IF(SELECT COUNT(*)
         FROM   @table) = 0
        BEGIN
            INSERT @table
            SELECT ID,
                   Name,
                   NULL,
                   NULL,
                   0,
                   0,
                   0
            FROM   tCompany
            where  ID = @ID_Company
        END

      RETURN;
  END;

GO
