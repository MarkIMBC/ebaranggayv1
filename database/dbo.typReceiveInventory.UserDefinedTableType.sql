﻿CREATE TYPE [dbo].[typReceiveInventory] AS TABLE(
	[Code] [varchar](50) NULL,
	[ID_Item] [int] NULL,
	[Quantity] [int] NOT NULL,
	[UnitPrice] [decimal](18, 4) NULL DEFAULT ((0)),
	[DateExpired] [datetime] NULL,
	[BatchNo] [int] NULL,
	[ID_FilingStatus] [int] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[IsAddInventory] [bit] NULL
)
GO
