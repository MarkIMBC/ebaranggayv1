﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tResidentRequest](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ContactNumber] [varchar](300) NULL,
	[Address] [varchar](300) NULL,
	[Purpose] [varchar](300) NULL,
	[ID_Resident] [int] NULL,
	[Date] [datetime] NULL,
	[ID_FilingStatus] [int] NULL,
	[ID_ApprovedBy] [int] NULL,
	[DateApproved] [datetime] NULL,
	[DateCanceled] [datetime] NULL,
 CONSTRAINT [PK_tResidentRequest] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tResidentRequest] ON [dbo].[tResidentRequest]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tResidentRequest] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tResidentRequest]  WITH CHECK ADD  CONSTRAINT [FK_tResidentRequest_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tResidentRequest] CHECK CONSTRAINT [FK_tResidentRequest_ID_Company]
GO
ALTER TABLE [dbo].[tResidentRequest]  WITH CHECK ADD  CONSTRAINT [FK_tResidentRequest_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tResidentRequest] CHECK CONSTRAINT [FK_tResidentRequest_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tResidentRequest]  WITH CHECK ADD  CONSTRAINT [FK_tResidentRequest_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tResidentRequest] CHECK CONSTRAINT [FK_tResidentRequest_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tResidentRequest]
		ON [dbo].[tResidentRequest]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tResidentRequest
			SET    DateCreated = GETDATE()
			FROM   dbo.tResidentRequest hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tResidentRequest] ENABLE TRIGGER [rDateCreated_tResidentRequest]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tResidentRequest]
		ON [dbo].[tResidentRequest]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tResidentRequest
			SET    DateModified = GETDATE()
			FROM   dbo.tResidentRequest hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tResidentRequest] ENABLE TRIGGER [rDateModified_tResidentRequest]
GO
