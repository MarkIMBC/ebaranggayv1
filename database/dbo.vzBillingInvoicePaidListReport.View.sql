﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vzBillingInvoicePaidListReport]
as
  SELECT biHed.ID                                                          ID_BillingInvoice,
         biHed.Code                                                        Code_BillingInvoice,
         biHed.Date                                                        Date_BillingInvoice,
         piHed.Date                                                        Date_PaymentTransaction,
         LTRIM(RTRIM(ISNULL(biHed.Name_Client, biHed.WalkInCustomerName))) Name_Client,
         ISNULL(biHed.PatientNames, biHed.Name_Patient)                    Name_Patient,
         biHed.Payment_ID_FilingStatus,
         biHed.Payment_Name_FilingStatus,
         biHed.ID_Company,
         ISNULL(biHed.DiscountAmount, 0)                                   DiscountAmount,
         ISNULL(biHed.TotalAmount, 0)                                      TotalAmount,
         CASE
           WHEN ISNULL(piHed.ChangeAmount, 0) > 0 THEN piHed.PayableAmount
           ELSE piHed.PaymentAmount
         END                                                               PaymentAmount,
         company.ImageLogoLocationFilenamePath,
         company.Name                                                      Name_Company,
         company.Address                                                   Address_Company,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                                             HeaderInfo_Company,
         ISNULL(biHed.AttendingPhysician_ID_Employee, '')                  AttendingPhysician_ID_Employee,
         ISNULL(biHed.Name, '(No Assigned)')                               AttendingPhysician_Name_Employee
  FROM   vBillingInvoice biHed
         LEFT JOIN tPaymentTransaction piHed
                on piHed.ID_BillingInvoice = biHed.ID
         LEFT JOIN dbo.vCompany company
                ON company.ID = biHed.ID_Company
  WHERE  biHed.Payment_ID_FilingStatus IN ( 11, 12 )
         AND biHed.ID_FilingStatus IN ( 3 )
         AND piHed.ID_FilingStatus IN ( 3 )
  GROUP  BY biHed.ID,
            biHed.Code,
            biHed.Date,
            piHed.Date,
            piHed.ID_FilingStatus,
            biHed.Name_Client,
            biHed.WalkInCustomerName,
            biHed.PatientNames,
            bihed.Name_Patient,
            biHed.Payment_ID_FilingStatus,
            biHed.Payment_Name_FilingStatus,
            biHed.ID_Company,
            biHed.DiscountAmount,
            biHed.TotalAmount,
            piHed.ChangeAmount,
            piHed.PayableAmount,
            piHed.PaymentAmount,
            company.ImageLogoLocationFilenamePath,
            company.Name,
            company.Address,
            company.ContactNumber,
            company.Email,
            ISNULL(biHed.AttendingPhysician_ID_Employee, ''),
            ISNULL(biHed.Name, '(No Assigned)')
  UNION ALL
  SELECT biHed.ID                                                          ID_BillingInvoice,
         biHed.Code                                                        Code_BillingInvoice,
         biHed.Date                                                        Date_BillingInvoice,
         NULL                                                              Date_PaymentTransaction,
         LTRIM(RTRIM(ISNULL(biHed.Name_Client, biHed.WalkInCustomerName))) Name_Client,
         ISNULL(biHed.PatientNames, biHed.Name_Patient)                    Name_Patient,
         biHed.Payment_ID_FilingStatus,
         biHed.Payment_Name_FilingStatus,
         biHed.ID_Company,
         ISNULL(biHed.DiscountAmount, 0)                                   DiscountAmount,
         ISNULL(biHed.TotalAmount, 0)                                      TotalAmount,
         0.00                                                              PaymentAmount,
         company.ImageLogoLocationFilenamePath,
         company.Name                                                      Name_Company,
         company.Address                                                   Address_Company,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                                             HeaderInfo_Company,
         ISNULL(biHed.AttendingPhysician_ID_Employee, '')                  AttendingPhysician_ID_Employee,
         ISNULL(biHed.Name, '(No Assigned)')                               AttendingPhysician_Name_Employee
  FROM   vBillingInvoice biHed
         LEFT JOIN dbo.vCompany company
                ON company.ID = biHed.ID_Company
  WHERE  biHed.Payment_ID_FilingStatus IN ( 2, 13 )
         AND biHed.ID_FilingStatus IN ( 3 )
  GROUP  BY biHed.ID,
            biHed.Code,
            biHed.Date,
            biHed.Name_Client,
            biHed.WalkInCustomerName,
            biHed.PatientNames,
            bihed.Name_Patient,
            biHed.Payment_ID_FilingStatus,
            biHed.Payment_Name_FilingStatus,
            biHed.ID_Company,
            biHed.DiscountAmount,
            biHed.TotalAmount,
            company.ImageLogoLocationFilenamePath,
            company.Name,
            company.Address,
            company.ContactNumber,
            company.Email,
            ISNULL(biHed.AttendingPhysician_ID_Employee, ''),
            ISNULL(biHed.Name, '(No Assigned)')

GO
