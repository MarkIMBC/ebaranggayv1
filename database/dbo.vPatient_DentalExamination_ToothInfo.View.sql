﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vPatient_DentalExamination_ToothInfo]
AS
SELECT H.ID,
       H.Code,
       H.Name,
       H.IsActive,
       H.ID_Company,
       H.Comment,
       H.DateCreated,
       H.DateModified,
       H.ID_CreatedBy,
       H.ID_LastModifiedBy,
       H.ID_Patient_DentalExamination,
       H.ID_Tooth,
       H.IDs_ToothSurface,
       H.ID_ToothStatus,
       UC.Name AS CreatedBy,
       UM.Name AS LastModifiedBy,
	   tooth.ToothNumber ToothNumber_Tooth,
	   tooth.Location Location_Tooth,
	   ts.Name Name_ToothStatus,
	   ts.Code Code_ToothStatus
FROM tPatient_DentalExamination_ToothInfo H
    LEFT JOIN tUser UC
        ON H.ID_CreatedBy = UC.ID
    LEFT JOIN tUser UM
        ON H.ID_LastModifiedBy = UM.ID
		LEFT JOIN dbo.tTooth tooth ON tooth.ID= h.ID_Tooth
		LEFT JOIN dbo.tToothStatus ts ON ts.ID = H.ID_ToothStatus
GO
