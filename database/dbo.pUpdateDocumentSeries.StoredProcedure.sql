﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pUpdateDocumentSeries]
(
    @ID_Model UNIQUEIDENTIFIER,
    @plusCount INT = 1
)
AS
BEGIN

    UPDATE dbo.tDocumentSeries
    SET Counter = Counter + @plusCount
    WHERE ID_Model = @ID_Model;

END;

GO
