﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROC [dbo].[pInsertItemCategory](@itemCateogry VARCHAR(MAX),
                                @ID_ItemType  INT = 0)
as
  BEGIN
      IF(SELECT COUNT(*)
         FROM   tItemCategory
         WHERE  Name = @itemCateogry
                AND ID_ItemType = @ID_ItemType) = 0
        BEGIN
            INSERT INTO [dbo].tItemCategory
                        ([Name],
                         ID_ItemType,
                         [IsActive],
                         ID_Company)
            VALUES      (@itemCateogry,
                         @ID_ItemType, 1,
                         1)
        END
  END 
GO
