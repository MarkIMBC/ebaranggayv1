﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[_tNavigation](
	[Oid] [uniqueidentifier] NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](200) NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_View] [uniqueidentifier] NULL,
	[Caption] [varchar](100) NULL,
	[Icon] [varchar](100) NULL,
	[SeqNo] [int] NULL,
	[ID_Parent] [uniqueidentifier] NULL,
	[Route] [varchar](300) NULL,
 CONSTRAINT [PK__tNavigation] PRIMARY KEY CLUSTERED 
(
	[Oid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[_tNavigation] ADD  CONSTRAINT [DF__tNavigation_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[_tNavigation]  WITH NOCHECK ADD  CONSTRAINT [FK_tNavigation_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[_tNavigation] CHECK CONSTRAINT [FK_tNavigation_ID_CreatedBy]
GO
ALTER TABLE [dbo].[_tNavigation]  WITH NOCHECK ADD  CONSTRAINT [FK_tNavigation_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[_tNavigation] CHECK CONSTRAINT [FK_tNavigation_ID_LastModifiedBy]
GO
ALTER TABLE [dbo].[_tNavigation]  WITH NOCHECK ADD  CONSTRAINT [FK_tNavigation_ID_Parent] FOREIGN KEY([ID_Parent])
REFERENCES [dbo].[_tNavigation] ([Oid])
GO
ALTER TABLE [dbo].[_tNavigation] CHECK CONSTRAINT [FK_tNavigation_ID_Parent]
GO
ALTER TABLE [dbo].[_tNavigation]  WITH NOCHECK ADD  CONSTRAINT [FK_tNavigation_ID_View] FOREIGN KEY([ID_View])
REFERENCES [dbo].[_tView] ([Oid])
GO
ALTER TABLE [dbo].[_tNavigation] CHECK CONSTRAINT [FK_tNavigation_ID_View]
GO
