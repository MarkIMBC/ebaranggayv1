﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE      
 VIEW [dbo].[vResident_ListvIew_MedicalRecord]
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         hed.IsDeceased,
         company.Guid      Guid_Company,
         hed.ID_HouseholdNumber,
         IsHeadofFamily,
         hed.Name_OccupationalStatus,
         hed.Name_Religion,
         medRecord.Name_MedicalRecordType,
         medRecord.Date    Resident_MedicalRecord_Date,
         medRecord.Comment Resident_MedicalRecord_Comment
  FROM   vResident hed
         Inner JOIN tCompany company
                 on company.ID = hed.ID_Company
         Inner JOIN vResident_MedicalRecord medRecord
                 on hed.ID = medRecord.ID_Resident
  WHERE  hed.IsActive = 1
         AND ISNULL(IsDeceased, 0) = 0

GO
