﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   VIEW [dbo].[vItem]
AS
SELECT H.*,
       UC.Name AS CreatedBy_Name_User,
       UM.Name AS LastModifiedBy_Name_User,
       itemType.Name Name_ItemType,
       itemCategory.Name Name_ItemCategory,
	   inventStatus.Name Name_InventoryStatus,
       H.CurrentInventoryCount _CurrentInventoryCount
FROM tItem H
    LEFT JOIN tUser UC
        ON H.ID_CreatedBy = UC.ID
    LEFT JOIN tUser UM
        ON H.ID_LastModifiedBy = UM.ID
    LEFT JOIN tItemType itemType
        ON itemType.ID = H.ID_ItemType
    LEFT JOIN tItemCategory itemCategory
        ON itemCategory.ID = H.ID_ItemCategory
	LEFT JOIN tInventoryStatus inventStatus
		ON inventStatus.ID = H.ID_InventoryStatus

GO
