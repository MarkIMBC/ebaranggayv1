﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fGetUserModules]
(
    @ID_User INT
)
RETURNS TABLE
AS
RETURN SELECT MV.Oid AS ID_View,
              MV.Name,
              N.Oid AS ID_Navigation,
              HED.Oid AS ID_Model,
              ISNULL(HED.SeqNo, N.SeqNo) AS SeqNo,
              HED.IsView,
              HED.IsCreate,
              HED.IsEdit,
              HED.IsDelete
       FROM
       (
           SELECT M.Name,
                  M.Oid,
                  ISNULL(H.IsView, 1) AS IsView,
                  ISNULL(H.IsCreate, 1) AS IsCreate,
                  ISNULL(H.IsEdit, 1) AS IsEdit,
                  ISNULL(H.IsDelete, 1) AS IsDelete,
                  ISNULL(H.IsDeny, 0) AS IsDeny,
                  (CASE
                       WHEN EXISTS
                            (
                                SELECT TOP 1
                                       *
                                FROM tUser_Roles R
                                    LEFT JOIN dbo.tUserRole USR
                                        ON R.ID_UserRole = USR.ID
                                WHERE USR.IsFullAccess = 1
                                      AND R.ID_User = @ID_User
                                      AND R.IsActive = 1
                            ) THEN
                           @ID_User
                       ELSE
                           NULL
                   END
                  ) AS ID_User,
                  H.SeqNo
           FROM dbo._tModel M
               LEFT JOIN
               (
                   SELECT D.ID,
                          D.ID_Model,
                          ISNULL(URS.ID_User, @ID_User) AS ID_User,
                          D.IsDeny,
                          --ISNULL(D.IsOwner,0) AS IsOwner ,
                          ISNULL(D.IsView, 0) AS IsView,
                          ISNULL(D.IsCreate, 0) AS IsCreate,
                          ISNULL(D.IsEdit, 0) AS IsEdit,
                          ISNULL(D.IsDelete, 0) AS IsDelete,
                          --ISNULL(d.IsAllow,0) AS IsAllow,
                          D.SeqNo,
                          1 AS IsQuickAccess
                   FROM dbo.tUserRole_Detail D
                       LEFT JOIN dbo.tUserRole R
                           ON D.ID_UserRole = R.ID
                       LEFT JOIN dbo.tUser_Roles URS
                           ON R.ID = URS.ID_UserRole
                   WHERE R.IsFullAccess = 1
                         AND URS.ID_User = @ID_User
                         AND URS.IsActive = 1
               ) H
                   ON M.Oid = H.ID_Model
           WHERE ISNULL(H.IsDeny, 0) = 0
           UNION ALL
           SELECT M.Name,
                  M.Oid,
                  ISNULL(H.IsView, 0) AS IsCanView,
                  ISNULL(H.IsCreate, 0) AS IsCanWrite,
                  ISNULL(H.IsEdit, 0) AS IsCanEdit,
                  ISNULL(H.IsDelete, 0) AS IsCanDelete,
                  ISNULL(H.IsDeny, 0) AS IsDeny,
                  @ID_User,
                  H.SeqNo
           FROM dbo.[_tModel] M
               INNER JOIN
               (
                   SELECT D.ID,
                          D.ID_Model,
                          URS.ID_User,
                          D.IsDeny,
                          D.IsView,
                          D.IsCreate,
                          D.IsEdit,
                          D.IsDelete,
                          D.SeqNo
                   FROM dbo.tUserRole_Detail D
                       LEFT JOIN dbo.tUserRole R
                           ON D.ID_UserRole = R.ID
                       LEFT JOIN dbo.tUser_Roles URS
                           ON R.ID = URS.ID_UserRole
                   WHERE ISNULL(D.IsDeny, 0) = 0
                         AND ISNULL(R.IsFullAccess, 0) = 0
                         AND URS.ID_User = @ID_User
                         AND URS.IsActive = 1
               ) H
                   ON M.Oid = H.ID_Model
           WHERE ISNULL(H.IsDeny, 0) = 0
       ) HED
           LEFT JOIN dbo._tView MV
               ON HED.Oid = MV.ID_Model
           LEFT JOIN dbo._tNavigation N
               ON N.ID_View = MV.Oid
       WHERE HED.ID_User = @ID_User
             AND ISNULL(N.IsActive, 1) = 1
       UNION ALL
       SELECT DISTINCT
              MV.Oid,
              R.Name,
              tn.Oid,
              NULL,
              tn.SeqNo, 
              1,
              NULL,
              NULL,
              NULL
       FROM dbo.tUserRole_Reports URE
           INNER JOIN dbo._tView MV
               ON MV.ID_Report = URE.ID_Report
                  AND MV.ID_ViewType = 3
           INNER JOIN dbo._tReport R
               ON URE.ID_Report = R.Oid
           INNER JOIN dbo.tUserRole UR
               ON URE.ID_UserRole = UR.ID
           INNER JOIN dbo.tUser_Roles URO
               ON UR.ID = URO.ID_UserRole
           INNER JOIN dbo._tNavigation tn
               ON MV.Oid = tn.ID_View
       WHERE tn.IsActive = 1
             AND URE.IsActive = 1
             AND URO.ID_User = @ID_User

GO
