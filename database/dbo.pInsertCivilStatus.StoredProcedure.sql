﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE           
 PROC [dbo].[pInsertCivilStatus](@CivilStatus VARCHAR(MAX), @ID_Company INT = 0)      
as      
  BEGIN      
      IF(SELECT COUNT(*)      
         FROM   tCivilStatus      
         WHERE  Name = @CivilStatus) = 0      
        BEGIN      
            INSERT INTO [dbo].tCivilStatus      
                        ([Name],      
                         [IsActive], ID_Company)      
            VALUES      (@CivilStatus,      
                         1, @ID_Company)      
        END      
  END 
GO
