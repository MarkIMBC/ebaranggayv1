﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vBillingInvoice_Patient]
AS
  SELECT H.*,
         UC.Name      AS CreatedBy,
         UM.Name      AS LastModifiedBy,
         patient.Name As Name_Patient
  FROM   tBillingInvoice_Patient H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tPatient patient
                ON H.ID_Patient = patient.ID

GO
