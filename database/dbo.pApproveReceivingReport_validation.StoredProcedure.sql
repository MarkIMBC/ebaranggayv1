﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
CREATE PROC [dbo].[pApproveReceivingReport_validation]    
(    
    @IDs_ReceivingReport typIntList READONLY,    
    @ID_UserSession INT    
)    
AS    
BEGIN    
    
    DECLARE @Filed_ID_FilingStatus INT = 1;    
    DECLARE @message VARCHAR(400) = '';    
    
    DECLARE @ValidateNotFiled TABLE    
    (    
        Code VARCHAR(30),    
        Name_FilingStatus VARCHAR(30)    
    );    
    DECLARE @Count_ValidateNotFiled INT = 0;    
    
    
    /* Validate Billing Invoices Status is not Filed*/    
    INSERT @ValidateNotFiled    
    (    
        Code,    
        Name_FilingStatus    
    )    
    SELECT Code,    
           Name_FilingStatus    
    FROM dbo.vReceivingReport bi    
    WHERE EXISTS    
    (    
        SELECT ID FROM @IDs_ReceivingReport ids WHERE ids.ID = bi.ID    
    )    
          AND bi.ID_FilingStatus NOT IN ( @Filed_ID_FilingStatus );    
    
    SELECT @Count_ValidateNotFiled = COUNT(*)    
    FROM @ValidateNotFiled;    
    
    IF (@Count_ValidateNotFiled > 0)    
    BEGIN    
    
        SET @message = 'The following record' + CASE    
                                                    WHEN @Count_ValidateNotFiled > 1 THEN    
                                                        's are'    
                                                    ELSE    
                                                        ' is '    
                                                END + 'not allowed to approved:';    
    
        SELECT @message = @message + CHAR(10) + Code + ' - ' + Name_FilingStatus    
        FROM @ValidateNotFiled;    
        THROW 50001, @message, 1;    
    
    END;
	

	/* Validate if PO item has Remaining Quantity*/
	Declare @ValidatePORemQty TABLE (Code varchar(30), 
										POCode varchar(30), 
										ItemName varchar(MAX), 
										RemQuantity int, 
										RRQuantity INT)
	Declare @Count_ValidatePORemQty int 

	INSERT @ValidatePORemQty
	(
		Code,
		POCode,
		ItemName,
		RemQuantity,
		RRQuantity
	)
	SELECT rrHed.Code,
			poHed.Code,
			rrDetail.Name_Item,
			poDetail.RemainingQuantity,
			SUM(rrDetail.Quantity) TotalRRQuantity
	FROM dbo.vReceivingReport_Detail rrDetail 
	INNER JOIN @IDs_ReceivingReport ids
		ON ids.ID = rrDetail.ID_ReceivingReport
	INNER JOIN dbo.tReceivingReport rrHed
		ON rrHed.ID = rrDetail.ID_ReceivingReport
	INNER JOIN dbo.tPurchaseOrder_Detail poDetail
		ON rrDetail.ID_PurchaseOrder_Detail = poDetail.ID
	INNER JOIN dbo.tPurchaseOrder poHed
		ON poDetail.ID_PurchaseOrder = poHed.ID
	Group BY 
		rrHed.Code,
			poHed.Code,
			rrDetail.Name_Item,
		rrDetail.ID_PurchaseOrder_Detail, 
		poDetail.RemainingQuantity
	HAVING 
		SUM(rrDetail.Quantity) > poDetail.RemainingQuantity

	SELECT @Count_ValidatePORemQty = COUNT(*) FROM @ValidatePORemQty

	if(@Count_ValidatePORemQty) > 0
	BEGIN

	 SET @message = 'The following record' + CASE    
												WHEN @Count_ValidatePORemQty > 1 THEN    
													's have '    
												ELSE    
													' has '    
											END + 'insuffiecient PO Rem. Qty:';    
    
		SELECT @message = @message + CHAR(10) + Code + ' - '+ ItemName + ': RR Qty - '+ CONVERT(VARCHAR(MAX), RRQuantity) + ' from Rem. Qty - ' + CONVERT(VARCHAR(MAX), RemQuantity) 
		FROM @ValidatePORemQty;    

		THROW 50001, @message, 1;    
	END

END;    
    
GO
