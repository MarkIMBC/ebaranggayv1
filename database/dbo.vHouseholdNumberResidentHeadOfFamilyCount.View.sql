﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vHouseholdNumberResidentHeadOfFamilyCount]
as
  Select household.ID,
         household.Name,
         resident.ID_Company,
         COUNT(*) Count
  FROM   tHouseholdNumber household
         inner join tResident resident
                 on household.ID = resident.ID_HouseholdNumber
  WHERE  resident.IsActive = 1
         and ISNULL(IsDeceased, 0) = 0
         And IsHeadofFamily = 1
  GROUP  BY household.ID,
            household.Name,
            resident.ID_Company

GO
