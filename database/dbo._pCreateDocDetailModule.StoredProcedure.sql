﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[_pCreateDocDetailModule]
    @TableName VARCHAR(100),
	@DocType VARCHAR(100) = NULL,
	@Prefix VARCHAR(100) = NULL
AS
    DECLARE @ModelName VARCHAR(150)
    SELECT  @ModelName = ( CASE WHEN SUBSTRING(@TableName, 1, 1) = '_'
                                THEN SUBSTRING(@TableName, 3, LEN(@TableName))
                                ELSE SUBSTRING(@TableName, 2, LEN(@TableName))
                           END )
--
--	CREATE MAIN TABLE
--
    EXEC('
    CREATE TABLE [dbo].[t' + @ModelName + '](
    [ID] [INT] IDENTITY(1,1) NOT NULL,
    [Code] [VARCHAR](50) NULL,
    [Name] [VARCHAR](200) NOT NULL,
    [IsActive] [BIT] NULL DEFAULT ((1)),
    [Comment] [VARCHAR](MAX) NULL,
    [DateCreated] [DATETIME] NULL,
    [DateModified] [DATETIME] NULL,
    [ID_CreatedBy] [INT] NULL,
    [ID_LastModifiedBy] [INT] NULL,
    [ID_BusinessPartner] [INT] NULL,
    [ID_Warehouse] [INT] NULL,
    [ID_TaxScheme] [INT] NULL,
    [ID_TransactionType] [INT] NULL,
    [VATRate] [DECIMAL](18, 4) NULL,
    [VATAmount] [DECIMAL](18, 4) NULL,
    [TotalQty] [DECIMAL](18, 4) NULL,
    [Subtotal] [DECIMAL](18, 4) NULL,
    [TotalAmount] [DECIMAL](18, 4) NULL,
    [ID_FilingStatus] [INT] NULL,
    [ID_DocStatus] [INT] NULL,
    [ID_ApprovedBy] [INT] NULL,
    [ID_CancelledBy] [INT] NULL,
    [DateApproved] [DATETIME] NULL,
    [DateCancelled] [DATETIME] NULL,
    [Date] [DATETIME] NULL,
    [ID_Currency] [INT] NULL,
    [ExchangeRate] [DECIMAL](18, 4) NULL,
    [Address] [VARCHAR](300) NULL,
    CONSTRAINT [PK_t' + @ModelName + '] PRIMARY KEY CLUSTERED 
    (
    [ID] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
    ')

    EXEC('ALTER TABLE [dbo].[t' + @ModelName + ']  WITH CHECK ADD  CONSTRAINT [FK_t' + @ModelName + '_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy]) REFERENCES [dbo].[tUser] ([ID])');
    EXEC('ALTER TABLE [dbo].[t' + @ModelName + '] CHECK CONSTRAINT [FK_t' + @ModelName + '_ID_CreatedBy]')
    EXEC('ALTER TABLE [dbo].[t' + @ModelName + ']  WITH CHECK ADD  CONSTRAINT [FK_t' + @ModelName + '_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy]) REFERENCES [dbo].[tUser] ([ID])')
    EXEC('ALTER TABLE [dbo].[t' + @ModelName + '] CHECK CONSTRAINT [FK_t' + @ModelName + '_ID_LastModifiedBy]')
    EXEC('ALTER TABLE [dbo].[t' + @ModelName + ']  WITH CHECK ADD  CONSTRAINT [FKt' + @ModelName + '_ID_BusinessPartner] FOREIGN KEY([ID_BusinessPartner]) REFERENCES [dbo].[tBusinessPartner] ([ID])')
    EXEC('ALTER TABLE [dbo].[t' + @ModelName + '] CHECK CONSTRAINT [FKt' + @ModelName + '_ID_BusinessPartner]')
    EXEC('ALTER TABLE [dbo].[t' + @ModelName + ']  WITH CHECK ADD  CONSTRAINT [FKt' + @ModelName + '_ID_FilingStatus] FOREIGN KEY([ID_FilingStatus]) REFERENCES [dbo].[tFilingStatus] ([ID])')
    EXEC('ALTER TABLE [dbo].[t' + @ModelName + '] CHECK CONSTRAINT [FKt' + @ModelName + '_ID_FilingStatus]')
    EXEC('ALTER TABLE [dbo].[t' + @ModelName + ']  WITH CHECK ADD  CONSTRAINT [FKt' + @ModelName + '_ID_Warehouse] FOREIGN KEY([ID_Warehouse]) REFERENCES [dbo].[tWarehouse] ([ID])')
    EXEC('ALTER TABLE [dbo].[t' + @ModelName + '] CHECK CONSTRAINT [FKt' + @ModelName + '_ID_Warehouse]')
--
--	CREATE TABLE DETAIL
--
    EXEC('
    CREATE TABLE [dbo].[t' + @ModelName + '_Detail](
    [ID] [INT] IDENTITY(1,1) NOT NULL,
    [Code] [VARCHAR](50) NULL,
    [Name] [VARCHAR](500) NOT NULL,
    [IsActive] [BIT] NULL DEFAULT ((1)),
    [Comment] [VARCHAR](MAX) NULL,
    [DateCreated] [DATETIME] NULL,
    [DateModified] [DATETIME] NULL,
    [ID_CreatedBy] [INT] NULL,
    [ID_LastModifiedBy] [INT] NULL,
    [ID_Item] [INT] NULL,
    [ID_UOM] [INT] NULL,
    [ID_DocStatus] [INT] NULL,
    [Quantity] [DECIMAL](18, 4) NULL,
    [UnitPrice] [DECIMAL](18, 4) NULL,
    [VATRate] [DECIMAL](18, 4) NULL,
    [VATAmount] [DECIMAL](18, 4) NULL,
    [Subtotal] [DECIMAL](18, 4) NULL,
    [DiscountAmount] [DECIMAL](18, 4) NULL,
    [TotalAmount] [DECIMAL](18, 4) NULL,
    [ID_' + @ModelName + '] [INT] NULL,
    CONSTRAINT [PK_t' + @ModelName + '_Detail] PRIMARY KEY CLUSTERED 
    (
    [ID] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
    ')

    EXEC('ALTER TABLE [dbo].[t' + @ModelName + '_Detail]  WITH CHECK ADD  CONSTRAINT [FK_' + @ModelName + '_Detail_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy]) REFERENCES [dbo].[tUser] ([ID])')
    EXEC('ALTER TABLE [dbo].[t' + @ModelName + '_Detail] CHECK CONSTRAINT [FK_' + @ModelName + '_Detail_ID_CreatedBy]')
    EXEC('ALTER TABLE [dbo].[t' + @ModelName + '_Detail]  WITH CHECK ADD  CONSTRAINT [FK_' + @ModelName + '_Detail_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy]) REFERENCES [dbo].[tUser] ([ID])')
    EXEC('ALTER TABLE [dbo].[t' + @ModelName + '_Detail] CHECK CONSTRAINT [FK_' + @ModelName + '_Detail_ID_LastModifiedBy]')
    EXEC('ALTER TABLE [dbo].[t' + @ModelName + '_Detail]  WITH CHECK ADD  CONSTRAINT [FK' + @ModelName + '_Detail_ID_Item] FOREIGN KEY([ID_Item]) REFERENCES [dbo].[tItem] ([ID])')
    EXEC('ALTER TABLE [dbo].[t' + @ModelName + '_Detail] CHECK CONSTRAINT [FK' + @ModelName + '_Detail_ID_Item]')
    EXEC('ALTER TABLE [dbo].[t' + @ModelName + '_Detail]  WITH CHECK ADD  CONSTRAINT [FK' + @ModelName + '_Detail_ID_' + @ModelName + '] FOREIGN KEY([ID_' + @ModelName  + ']) REFERENCES [dbo].[t' + @ModelName + '] ([ID])')
    EXEC('ALTER TABLE [dbo].[t' + @ModelName + '_Detail] CHECK CONSTRAINT [FK' + @ModelName + '_Detail_ID_' + @ModelName + ']')
--
--	 CREATE VIEW
--
    EXEC('
    CREATE VIEW dbo.v' + @ModelName + '
    AS
    SELECT  H.* ,
    BP.Name AS Supplier ,
    T.Name AS Type ,
    WH.Name AS Warehouse,
    TS.Name AS TaxScheme ,
    FS.Name AS FilingStatus ,
    UC.Name AS CreatedBy ,
    UM.Name AS LastModifiedBy
    FROM    t' + @ModelName + ' H
    LEFT JOIN dbo.tBusinessPartner BP ON H.ID_BusinessPartner = BP.ID
    LEFT JOIN dbo.tTransactionType T ON H.ID_TransactionType = T.ID
    LEFT JOIN dbo.tTaxScheme TS ON H.ID_TaxScheme = TS.ID
    LEFT JOIN dbo.tFilingStatus FS ON H.ID_FilingStatus = FS.ID
    LEFT JOIN dbo.tWarehouse WH ON H.ID_Warehouse = WH.ID
    LEFT JOIN tUser UC ON H.ID_CreatedBy = UC.ID
    LEFT JOIN tUser UM ON H.ID_LastModifiedBy = UM.ID
    LEFT JOIN dbo.tUser UAY ON H.ID_ApprovedBy = UAY.ID
    LEFT JOIN dbo.tUser UAC ON H.ID_CancelledBy = UAC.ID
    ')
    EXEC('
    CREATE VIEW dbo.v' + @ModelName + '_Detail
    AS
    SELECT  H.* ,
    I.Name AS Item,
    UC.Name AS CreatedBy ,
    UM.Name AS LastModifiedBy
    FROM    t' + @ModelName + '_Detail H
    LEFT JOIN dbo.tItem I ON H.ID_Item = I.ID
    LEFT JOIN dbo.tUnitOfMeasure UOM ON H.ID_UOM = UOM.ID
    LEFT JOIN dbo.tDocumentStatus DS ON H.ID_DocStatus = DS.ID
    LEFT JOIN tUser UC ON H.ID_CreatedBy = UC.ID
    LEFT JOIN tUser UM ON H.ID_LastModifiedBy = UM.ID
    ')
	--
	--	CREATE PROCEDURE
	--
EXEC('
	CREATE PROCEDURE dbo.pGet' + @ModelName + '
    @ID INT = -1 ,
    @ID_Session INT = NULL
	AS
    BEGIN
        SELECT  ''_'', '''' AS ' + @ModelName + '_Detail

        DECLARE @ID_User INT ,
            @ID_Warehouse INT , @TaxRate DECIMAL(18,2) = 0.00
        SELECT  @ID_User = ID_User ,
                @ID_Warehouse = ID_Warehouse
        FROM    tUserSession
        WHERE   ID = @ID_Session

		SELECT  @TaxRate = Rate
        FROM    dbo.tTaxRate
        WHERE   IsActive = 1

        IF ( @ID = -1 )
            BEGIN
                SELECT  H.*,
						FS.Name AS FilingStatus,
						UC.Name AS CreatedBy,
						UM.Name AS ModifiedBy
                FROM    ( SELECT    NULL AS [_] ,
                                    -1 AS [ID] ,
                                    NULL AS [Code] ,
                                    ''(Auto)'' AS [Name] ,
                                    1 AS [IsActive] ,
                                    NULL AS [Comment] ,
                                    NULL AS [DateCreated] ,
                                    NULL AS [DateModified] ,
                                    NULL AS [ID_CreatedBy] ,
                                    NULL AS [ID_LastModifiedBy] ,
                                    NULL AS [ID_BusinessPartner] ,
                                    @ID_Warehouse AS [ID_Warehouse] ,
                                    1 AS [ID_TaxScheme] ,
                                    1 AS [ID_TransactionType] ,
                                    @TaxRate AS [VATRate] ,
                                    0.00 AS [VATAmount] ,
                                    0.00 AS [TotalQty] ,
                                    0.00 AS [Subtotal] ,
                                    0.00 AS [TotalAmount] ,
                                    1 AS [ID_FilingStatus] ,
                                    NULL AS [ID_DocStatus] ,
                                    NULL AS [ID_ApprovedBy] ,
                                    NULL AS [ID_CancelledBy] ,
                                    NULL AS [DateApproved] ,
                                    NULL AS [DateCancelled] ,
                                    GETDATE() AS [Date],
									1 AS [ID_Currency]
                        ) H
                        LEFT JOIN tUser UC ON H.ID_CreatedBy = UC.ID
                        LEFT JOIN tUser UM ON H.ID_LastModifiedBy = UM.ID
						LEFT JOIN dbo.tFilingStatus FS ON H.ID_FilingStatus = FS.ID
            END
        ELSE
            BEGIN
                SELECT  H.*
                FROM    v' + @ModelName + ' H
                WHERE   H.ID = @ID
            END
    END

	SELECT D.* FROM dbo.v' + @ModelName + '_Detail D WHERE ID_' + @ModelName +' = @ID
')

EXEC('
CREATE PROCEDURE dbo.pGet' + @ModelName + '_Detail
    @ID INT = -1 ,
    @ID_Session INT = NULL
AS
    BEGIN
        SELECT  ''_''

        DECLARE @ID_User INT ,
            @ID_Warehouse INT

        SELECT  @ID_User = ID_User ,
                @ID_Warehouse = ID_Warehouse
        FROM    tUserSession
        WHERE   ID = @ID_Session

        IF ( @ID = -1 )
            BEGIN
                SELECT  H.*
                FROM    ( SELECT    NULL AS [_] ,
                                    -1 AS [ID] ,
                                    NULL AS [Code] ,
                                    NULL AS [Name] ,
                                    1 AS [IsActive] ,
                                    NULL AS [Comment] ,
                                    NULL AS [DateCreated] ,
                                    NULL AS [DateModified] ,
                                    NULL AS [ID_CreatedBy] ,
                                    NULL AS [ID_LastModifiedBy] ,
                                    NULL AS [ID_Item] ,
                                    NULL AS [ID_UOM] ,
                                    NULL AS [ID_DocStatus] ,
                                    NULL AS [Quantity] ,
                                    0.00 AS [UnitPrice] ,
                                    0.00 AS [VATRate] ,
                                    0.00 AS [VATAmount] ,
                                    0.00 AS [Subtotal] ,
                                    0.00 AS [DiscountAmount] ,
                                    0.00 AS [TotalAmount]
                        ) H
                        LEFT JOIN tUser UC ON H.ID_CreatedBy = UC.ID
                        LEFT JOIN tUser UM ON H.ID_LastModifiedBy = UM.ID
            END
        ELSE
            BEGIN
                SELECT  H.*
                FROM    v' + @ModelName + '_Detail H
                WHERE   H.ID = @ID
            END
    END
')

DECLARE @MainTable VARCHAR(100) = 't' + @ModelName
DECLARE @DetailTable VARCHAR(100) = 't' + @ModelName + '_Detail'
DECLARE @Link VARCHAR(100) = 'ID_' + @ModelName

EXEC dbo.[_pCreateAppModule] @TableName = @MainTable, -- varchar(max)
    @IsCreateProc = NULL, -- bit
    @IsCreateNavigation = 1 -- bit

EXEC dbo.[_pCreateAppModule] @TableName = @DetailTable, -- varchar(max)
    @IsCreateProc = NULL, -- bit
    @IsCreateNavigation = NULL -- bit

EXEC dbo.[_pAddModelDetail] @TableName = @DetailTable, -- varchar(300)
    @FieldKey = @Link, -- varchar(300)
    @ParentTable = @MainTable -- varchar(300)

DECLARE @Oid_DetailView UNIQUEIDENTIFIER, @Oid_DetailView_Detail UNIQUEIDENTIFIER, @GeneralTab UNIQUEIDENTIFIER = NULL
SELECT @Oid_DetailView = Oid FROM dbo.[_tDetailView] WHERE Name = @ModelName + '_DetailView'
SELECT @Oid_DetailView_Detail = Oid FROM dbo.[_tDetailView_Detail] WHERE Name = @ModelName + '_Detail_DetailView'
DECLARE @OtherSection UNIQUEIDENTIFIER = NEWID(),
		@TotalSection UNIQUEIDENTIFIER = NEWID(),
		@DetailTab UNIQUEIDENTIFIER = NEWID() 
SELECT @GeneralTab = Oid FROM dbo.[_tDetailView_Detail] WHERE ID_DetailView = @DetailTab AND Name = 'GeneralTab' AND ID_ControlType = 12

DECLARE @Oid_ListView UNIQUEIDENTIFIER, @Oid_ListView_Detail UNIQUEIDENTIFIER, @Oid_Detail_ListView UNIQUEIDENTIFIER
SELECT @Oid_ListView = Oid FROM dbo.[_tListView] WHERE Name = @ModelName + '_ListView'
SELECT @Oid_ListView_Detail = Oid FROM dbo.[_tListView] WHERE Name = @ModelName + '_Detail_ListView'
SELECT @Oid_Detail_ListView = Oid FROM dbo.[_tListView] WHERE Name = @ModelName + '_Detail_ListView_Detail'

--OtherSection
INSERT [dbo].[_tDetailView_Detail] ([Oid], [Code], [Name], [IsActive], [SeqNo], [Comment], [Caption], [ID_ModelProperty], [ID_DetailView], [ID_Tab], [ID_Section], [ID_ControlType], [ID_PropertyType], [Format], [ID_CreatedBy], [ID_LastModifiedBy], [DateCreated], [DateModified], [DisplayProperty], [DataSource], [IsLoadData], [ColCount], [ColSpan], [IsDisabled], [Height], [ID_ListView], [IsReadOnly], [ID_LabelLocation], [IsShowLabel], [IsRequired], [ValueExpr], [DisplayExpr], [ID_LookUp_ListView], [LookUp_ListView_Caption], [LookUp_ListView_DataSource], [GroupIndex], [SearchExpr], [Precision]) VALUES (@OtherSection, NULL, N'OtherSection', 1, 500, NULL, N'Others', NULL,@Oid_DetailView, @GeneralTab, NULL, 13, NULL, NULL, 1, 1, CAST(N'2018-01-30 11:08:38.587' AS DateTime), NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
--TotalSection
INSERT [dbo].[_tDetailView_Detail] ([Oid], [Code], [Name], [IsActive], [SeqNo], [Comment], [Caption], [ID_ModelProperty], [ID_DetailView], [ID_Tab], [ID_Section], [ID_ControlType], [ID_PropertyType], [Format], [ID_CreatedBy], [ID_LastModifiedBy], [DateCreated], [DateModified], [DisplayProperty], [DataSource], [IsLoadData], [ColCount], [ColSpan], [IsDisabled], [Height], [ID_ListView], [IsReadOnly], [ID_LabelLocation], [IsShowLabel], [IsRequired], [ValueExpr], [DisplayExpr], [ID_LookUp_ListView], [LookUp_ListView_Caption], [LookUp_ListView_DataSource], [GroupIndex], [SearchExpr], [Precision]) VALUES (@TotalSection, NULL, N'TotalSection', 1, 1000, NULL, N'Summary', NULL,@Oid_DetailView, @GeneralTab, NULL, 13, NULL, NULL, 1, 1, CAST(N'2018-01-30 10:20:58.697' AS DateTime), NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
--Detail Tab
INSERT [dbo].[_tDetailView_Detail] ([Oid], [Code], [Name], [IsActive], [SeqNo], [Comment], [Caption], [ID_ModelProperty], [ID_DetailView], [ID_Tab], [ID_Section], [ID_ControlType], [ID_PropertyType], [Format], [ID_CreatedBy], [ID_LastModifiedBy], [DateCreated], [DateModified], [DisplayProperty], [DataSource], [IsLoadData], [ColCount], [ColSpan], [IsDisabled], [Height], [ID_ListView], [IsReadOnly], [ID_LabelLocation], [IsShowLabel], [IsRequired], [ValueExpr], [DisplayExpr], [ID_LookUp_ListView], [LookUp_ListView_Caption], [LookUp_ListView_DataSource], [GroupIndex], [SearchExpr], [Precision]) VALUES (@DetailTab, NULL, N'DetailTab', 1, 1000, NULL, N'Details', NULL,@Oid_DetailView, NULL, NULL, 12, NULL, NULL, 1, 1, CAST(N'2018-01-31 11:28:37.380' AS DateTime), NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL)

UPDATE dbo.[_tDetailView_Detail] SET ID_Tab = @DetailTab, ColSpan = 3, IsShowLabel = 0 WHERE ID_DetailView = @Oid_DetailView AND Name = @ModelName + '_Detail'

UPDATE dbo.[_tListView_Detail] SET IsVisible = 0, VisibleIndex = NULL WHERE Name = 'ID_' + @ModelName AND ID_ListView = @Oid_Detail_ListView

UPDATE  L
SET     L.ID_PropertyType = R.ID_PropertyType ,
        L.ID_PropertyModel = R.ID_PropertyModel
FROM    dbo.[_tModel_Property] L
        INNER JOIN dbo.[_tModel] M ON M.Oid = L.ID_Model
                                      AND ( M.TableName = @MainTable
                                            AND L.ID_PropertyType <> 10
                                          )
        INNER JOIN ( SELECT TD.*
                     FROM   dbo.[_tModel_Property] TD
                            INNER JOIN dbo.[_tModel] MR ON TD.ID_Model = MR.Oid
                                                           AND MR.TableName = 'tDoc'
                   ) R ON R.Name = L.Name
                          AND R.ID_PropertyType <> 10
WHERE   R.Name NOT IN ( 'ID_Doc', 'Doc_Detail' )

UPDATE  L
SET     L.ID_PropertyType = R.ID_PropertyType ,
        L.ID_PropertyModel = R.ID_PropertyModel
FROM    dbo.[_tModel_Property] L
        INNER JOIN dbo.[_tModel] M ON M.Oid = L.ID_Model
                                      AND ( M.TableName = @DetailTable
                                            AND L.ID_PropertyType <> 10
                                          )
        INNER JOIN ( SELECT TD.*
                     FROM   dbo.[_tModel_Property] TD
                            INNER JOIN dbo.[_tModel] MR ON TD.ID_Model = MR.Oid
                                                           AND MR.TableName = 'tDoc_Detail'
                   ) R ON R.Name = L.Name
                          AND R.ID_PropertyType <> 10
WHERE   R.Name NOT IN ( 'ID_Doc', 'Doc_Detail' )

UPDATE L SET 
       L.IsActive = R.IsActive,
       L.SeqNo = R.SeqNo,
       L.Caption = (CASE L.Name 
					WHEN 'Name' THEN ISNULL(@Prefix + ' No.','Document No.') 
					WHEN 'Date' THEN ISNULL(@Prefix + ' Date','Document Date') 
					WHEN 'ID_BusinessPartner' THEN (CASE @DocType WHEN 'AP' THEN 'Supplier' WHEN 'AR' THEN 'Customer' ELSE 'Business Partner' END)
					ELSE  R.Caption END),
       L.ID_Tab =  ( SELECT TOP 1
                    D.Oid
          FROM      dbo.[_tDetailView_Detail] D
                    INNER JOIN dbo.[_tDetailView] H ON D.ID_DetailView = H.Oid AND D.Name = TAB.Name
          WHERE     H.Name = @ModelName + '_DetailView'
                    AND D.ID_ControlType = 12
        ),
       L.ID_Section  = ( SELECT TOP 1
                    D.Oid
          FROM      dbo.[_tDetailView_Detail] D
                    INNER JOIN dbo.[_tDetailView] H ON D.ID_DetailView = H.Oid AND D.Name = SECTION.Name
          WHERE     H.Name = @ModelName + '_DetailView'
                    AND D.ID_ControlType = 13
        ),
       L.ID_ControlType = R.ID_ControlType,
       L.ID_PropertyType = R.ID_PropertyType,
       L.Format = R.Format,
       L.DisplayProperty = R.DisplayProperty,
       L.DataSource = R.DataSource,
       L.IsLoadData = R.IsLoadData,
       L.ColCount = R.ColCount ,
       L.ColSpan = R.ColSpan,
       L.IsDisabled = R.IsDisabled,
       L.Height = R.Height,
       L.ID_ListView = R.ID_ListView,
       L.IsReadOnly = R.IsReadOnly,
       L.ID_LabelLocation = R.ID_LabelLocation,
       L.IsShowLabel = R.IsShowLabel,
       L.IsRequired = R.IsRequired,
       L.ValueExpr = R.ValueExpr,
       L.DisplayExpr = R.DisplayExpr,
       L.ID_LookUp_ListView = R.ID_LookUp_ListView,
       L.LookUp_ListView_Caption = R.LookUp_ListView_Caption,
       L.LookUp_ListView_DataSource = R.LookUp_ListView_DataSource,
       L.GroupIndex = R.GroupIndex,
       L.SearchExpr =R.SearchExpr,
       L.Precision = R.Precision
FROM    dbo.[_tDetailView_Detail] L
        INNER JOIN dbo.[_tDetailView] DL ON L.ID_DetailView = DL.Oid
                                            AND DL.Name = @ModelName + '_DetailView'
        INNER JOIN ( SELECT TD.*
                     FROM   dbo.[_tDetailView_Detail] TD
                            INNER JOIN dbo.[_tDetailView] T ON TD.ID_DetailView = T.Oid
                                                              AND T.Name = 'Doc_DetailView'
                   ) R ON R.Name = L.Name
		LEFT JOIN dbo.[_tDetailView_Detail] TAB ON R.ID_Tab = TAB.Oid
		LEFT JOIN dbo.[_tDetailView_Detail] SECTION ON R.ID_Section = SECTION.Oid

UPDATE L SET 
       L.IsActive = R.IsActive,
       L.SeqNo = R.SeqNo,
       L.Caption = (CASE L.Name 
					WHEN 'Name' THEN ISNULL(@Prefix + ' No.','Document No.') 
					WHEN 'Date' THEN ISNULL(@Prefix + ' Date','Document Date') 
					WHEN 'ID_BusinessPartner' THEN (CASE @DocType WHEN 'AP' THEN 'Supplier' WHEN 'AR' THEN 'Customer' ELSE 'Business Partner' END)
					ELSE  R.Caption END),
       L.ID_Tab =  ( SELECT TOP 1
                    D.Oid
          FROM      dbo.[_tDetailView_Detail] D
                    INNER JOIN dbo.[_tDetailView] H ON D.ID_DetailView = H.Oid AND D.Name = TAB.Name
          WHERE     H.Name = @ModelName + '_Detail_DetailView'
                    AND D.ID_ControlType = 12
        ),
       L.ID_Section  = ( SELECT TOP 1
                    D.Oid
          FROM      dbo.[_tDetailView_Detail] D
                    INNER JOIN dbo.[_tDetailView] H ON D.ID_DetailView = H.Oid AND D.Name = SECTION.Name
          WHERE     H.Name = @ModelName + '_Detail_DetailView'
                    AND D.ID_ControlType = 13
        ),
       L.ID_ControlType = R.ID_ControlType,
       L.ID_PropertyType = R.ID_PropertyType,
       L.Format = R.Format,
       L.DisplayProperty = R.DisplayProperty,
       L.DataSource = R.DataSource,
       L.IsLoadData = R.IsLoadData,
       L.ColCount = R.ColCount ,
       L.ColSpan = R.ColSpan,
       L.IsDisabled = R.IsDisabled,
       L.Height = R.Height,
       L.ID_ListView = R.ID_ListView,
       L.IsReadOnly = R.IsReadOnly,
       L.ID_LabelLocation = R.ID_LabelLocation,
       L.IsShowLabel = R.IsShowLabel,
       L.IsRequired = R.IsRequired,
       L.ValueExpr = R.ValueExpr,
       L.DisplayExpr = R.DisplayExpr,
       L.ID_LookUp_ListView = R.ID_LookUp_ListView,
       L.LookUp_ListView_Caption = R.LookUp_ListView_Caption,
       L.LookUp_ListView_DataSource = R.LookUp_ListView_DataSource,
       L.GroupIndex = R.GroupIndex,
       L.SearchExpr =R.SearchExpr,
       L.Precision = R.Precision
FROM    dbo.[_tDetailView_Detail] L
        INNER JOIN dbo.[_tDetailView] DL ON L.ID_DetailView = DL.Oid
                                            AND DL.Name = @ModelName + '_Detail_DetailView'
        INNER JOIN ( SELECT TD.*
                     FROM   dbo.[_tDetailView_Detail] TD
                            INNER JOIN dbo.[_tDetailView] T ON TD.ID_DetailView = T.Oid
                                                              AND T.Name = 'Doc_Detail_DetailView'
                   ) R ON R.Name = L.Name
		LEFT JOIN dbo.[_tDetailView_Detail] TAB ON R.ID_Tab = TAB.Oid
		LEFT JOIN dbo.[_tDetailView_Detail] SECTION ON R.ID_Section = SECTION.Oid

UPDATE L SET	 
       L.IsActive = R.IsActive,
       L.Caption = (CASE L.Name 
					WHEN 'Name' THEN ISNULL(@Prefix + ' No.','Document No.') 
					WHEN 'Date' THEN ISNULL(@Prefix + ' Date','Document Date') 
					WHEN 'ID_BusinessPartner' THEN (CASE @DocType WHEN 'AP' THEN 'Supplier' WHEN 'AR' THEN 'Customer' ELSE 'Business Partner' END)
					ELSE  R.Caption END),
       L.DisplayProperty = R.DisplayProperty,
       L.DataSource = R.DataSource,
       L.Format = R.Format,
       L.Width = R.Width,
       L.Fixed = R.Fixed,
       L.VisibleIndex = R.VisibleIndex,
       L.IsAllowEdit = R.IsAllowEdit,
       L.ID_ControlType = R.ID_ControlType,
       L.ID_ColumnAlignment = R.ID_ColumnAlignment,
       L.IsVisible  = R.IsVisible,
       L.FixedPosition  = R.FixedPosition,
       L.IsRequired = R.IsRequired,
       L.ID_SummaryType  = R.ID_SummaryType,
       L.Precision = R.PRECISION,
       L.ID_PropertyType = R.ID_PropertyType
FROM    dbo.[_tListView_Detail] L
        INNER JOIN ( SELECT TD.*
                     FROM   dbo.[_tListView_Detail] TD
                            INNER JOIN dbo.[_tListView] T ON TD.ID_ListView = T.Oid AND T.Name = 'Doc_ListView'
                   ) R ON L.ID_ListView = @Oid_ListView AND R.Name = L.Name

UPDATE L SET	 
       L.IsActive = R.IsActive,
       L.Caption = R.Caption,
       L.DisplayProperty = R.DisplayProperty,
       L.DataSource = R.DataSource,
       L.Format = R.Format,
       L.Width = R.Width,
       L.Fixed = R.Fixed,
       L.VisibleIndex = R.VisibleIndex,
       L.IsAllowEdit = R.IsAllowEdit,
       L.ID_ControlType = R.ID_ControlType,
       L.ID_ColumnAlignment = R.ID_ColumnAlignment,
       L.IsVisible  = R.IsVisible,
       L.FixedPosition  = L.FixedPosition,
       L.IsRequired = R.IsRequired,
       L.ID_SummaryType  = R.ID_SummaryType,
       L.Precision = R.PRECISION,
       L.ID_PropertyType = R.ID_PropertyType
FROM    dbo.[_tListView_Detail] L
        INNER JOIN ( SELECT TD.*
                     FROM   dbo.[_tListView_Detail] TD
                            INNER JOIN dbo.[_tListView] T ON TD.ID_ListView = T.Oid AND T.Name = 'Doc_Detail_ListView'
                   ) R ON L.ID_ListView = @Oid_ListView_Detail AND R.Name = L.Name

 

 UPDATE L SET	 
       L.IsActive = R.IsActive,
       L.Caption = R.Caption,
       L.DisplayProperty = R.DisplayProperty,
       L.DataSource = R.DataSource,
       L.Format = R.Format,
       L.Width = R.Width,
       L.Fixed = R.Fixed,
       L.VisibleIndex = R.VisibleIndex,
       L.IsAllowEdit = R.IsAllowEdit,
       L.ID_ControlType = R.ID_ControlType,
       L.ID_ColumnAlignment = R.ID_ColumnAlignment,
       L.IsVisible  = R.IsVisible,
       L.FixedPosition  = R.FixedPosition,
       L.IsRequired = R.IsRequired,
       L.ID_SummaryType  = R.ID_SummaryType,
       L.Precision = R.PRECISION,
       L.ID_PropertyType = R.ID_PropertyType
FROM    dbo.[_tListView_Detail] L
        INNER JOIN ( SELECT TD.*
                     FROM   dbo.[_tListView_Detail] TD
                            INNER JOIN dbo.[_tListView] T ON TD.ID_ListView = T.Oid AND T.Name = 'Doc_Detail_ListView_Detail'
                   ) R ON L.ID_ListView = @Oid_Detail_ListView AND R.Name = L.Name
GO
