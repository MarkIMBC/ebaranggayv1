﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vPatient_Confinement_MaxSOAP]
as
  SELECT soap.ID                     ID_Patient_SOAP,
         MaxDate                     MaxDate,
         soap.ID_Patient_Confinement ID_Patient_Confinement,
         soap.ID_FilingStatus        ID_FilingStatus
  FROm   tPatient_SOAP soap
         inner JOIN (SELECT MAX(Date) MaxDate,
                            ID_Patient_Confinement
                     FROM   tPatient_SOAP
                     WHERE  ID_FilingStatus IN ( 1, 3, 13 )
                     group  by ID_Patient_Confinement) tbl
                 on soap.Date = tbl.MaxDate
                    and soap.ID_Patient_Confinement = tbl.ID_Patient_Confinement

GO
