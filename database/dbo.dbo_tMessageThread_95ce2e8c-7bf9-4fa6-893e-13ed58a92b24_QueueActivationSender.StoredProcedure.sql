﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_QueueActivationSender] 
WITH EXECUTE AS SELF
AS 
BEGIN 
    SET NOCOUNT ON;
    DECLARE @h AS UNIQUEIDENTIFIER;
    DECLARE @mt NVARCHAR(200);

    RECEIVE TOP(1) @h = conversation_handle, @mt = message_type_name FROM [dbo].[dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Sender];

    IF @mt = N'http://schemas.microsoft.com/SQL/ServiceBroker/EndDialog'
    BEGIN
        END CONVERSATION @h;
    END

    IF @mt = N'http://schemas.microsoft.com/SQL/ServiceBroker/DialogTimer' OR @mt = N'http://schemas.microsoft.com/SQL/ServiceBroker/Error'
    BEGIN 
        

        END CONVERSATION @h;

        DECLARE @conversation_handle UNIQUEIDENTIFIER;
        DECLARE @schema_id INT;
        SELECT @schema_id = schema_id FROM sys.schemas WITH (NOLOCK) WHERE name = N'dbo';

        
        IF EXISTS (SELECT * FROM sys.triggers WITH (NOLOCK) WHERE object_id = OBJECT_ID(N'[dbo].[tr_dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Sender]')) DROP TRIGGER [dbo].[tr_dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Sender];

        
        IF EXISTS (SELECT * FROM sys.service_queues WITH (NOLOCK) WHERE schema_id = @schema_id AND name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Sender') EXEC (N'ALTER QUEUE [dbo].[dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Sender] WITH ACTIVATION (STATUS = OFF)');

        
        SELECT conversation_handle INTO #Conversations FROM sys.conversation_endpoints WITH (NOLOCK) WHERE far_service LIKE N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_%' ORDER BY is_initiator ASC;
        DECLARE conversation_cursor CURSOR FAST_FORWARD FOR SELECT conversation_handle FROM #Conversations;
        OPEN conversation_cursor;
        FETCH NEXT FROM conversation_cursor INTO @conversation_handle;
        WHILE @@FETCH_STATUS = 0 
        BEGIN
            END CONVERSATION @conversation_handle WITH CLEANUP;
            FETCH NEXT FROM conversation_cursor INTO @conversation_handle;
        END
        CLOSE conversation_cursor;
        DEALLOCATE conversation_cursor;
        DROP TABLE #Conversations;

        
        IF EXISTS (SELECT * FROM sys.services WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Receiver') DROP SERVICE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Receiver];
        
        IF EXISTS (SELECT * FROM sys.services WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Sender') DROP SERVICE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Sender];

        
        IF EXISTS (SELECT * FROM sys.service_queues WITH (NOLOCK) WHERE schema_id = @schema_id AND name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Receiver') DROP QUEUE [dbo].[dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Receiver];
        
        IF EXISTS (SELECT * FROM sys.service_queues WITH (NOLOCK) WHERE schema_id = @schema_id AND name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Sender') DROP QUEUE [dbo].[dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_Sender];

        
        IF EXISTS (SELECT * FROM sys.service_contracts WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24') DROP CONTRACT [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24];
        
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/StartMessage/Insert') DROP MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/StartMessage/Insert];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/StartMessage/Update') DROP MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/StartMessage/Update];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/StartMessage/Delete') DROP MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/StartMessage/Delete];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/ID') DROP MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/ID];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Recipient_ID_User') DROP MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Recipient_ID_User];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Sender_ID_User') DROP MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Sender_ID_User];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Message') DROP MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/Message];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/DateSent') DROP MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/DateSent];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/DateRead') DROP MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/DateRead];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/IsRead') DROP MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/IsRead];
        IF EXISTS (SELECT * FROM sys.service_message_types WITH (NOLOCK) WHERE name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/EndMessage') DROP MESSAGE TYPE [dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24/EndMessage];

        
        IF EXISTS (SELECT * FROM sys.objects WITH (NOLOCK) WHERE schema_id = @schema_id AND name = N'dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_QueueActivationSender') DROP PROCEDURE [dbo].[dbo_tMessageThread_95ce2e8c-7bf9-4fa6-893e-13ed58a92b24_QueueActivationSender];

        
    END
END
GO
