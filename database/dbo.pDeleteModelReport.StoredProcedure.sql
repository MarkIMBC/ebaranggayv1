﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pDeleteModelReport](@ID_Company INT, @ModelName  Varchar(200),
                              @ReportName VARCHAR(200))
AS
  BEGIN
      DECLARE @Oid_Model VARCHAR(MAX)
      DECLARE @Oid_Report VARCHAR(MAX)
      DECLARE @Count_Validation INT = 0
      DECLARE @msg VARCHAR(MAX) = ''

      SELECT @Oid_Model = Oid
      FROM   _tModel
      WHERE  Name = @ModelName

      SELECT @Oid_Report = Oid
      FROM   _tModel
      WHERE  Name = @ReportName

      DELETE FROM [_tModelReport]
      WHERE  ISNULL(OId_Model, '') = ISNULL(@Oid_Model, '')
             AND ISNULL(Oid_Report, '') = ISNULL(@Oid_Report, '')
			 AND ID_Company = @ID_Company;
  END

GO
