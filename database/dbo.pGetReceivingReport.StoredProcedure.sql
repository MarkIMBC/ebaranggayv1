﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[pGetReceivingReport]
    @ID INT = -1,
    @ID_Session INT = NULL
AS
BEGIN
    SELECT '_',
           '' ReceivingReport_Detail;

    DECLARE @ID_User INT,
            @ID_Warehouse INT;
    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM dbo.tUserSession
    WHERE ID = @ID_Session;

    IF (@ID = -1)
    BEGIN
        SELECT H.*,
               fs.Name Name_FilingStatus
        FROM
        (
            SELECT NULL AS [_],
                   -1 AS [ID],
                   '- NEW -' AS [Code],
                   NULL AS [Name],
                   1 AS [IsActive],
                   GETDATE() AS Date,
                   NULL AS [ID_Company],
                   NULL AS [Comment],
                   NULL AS [DateCreated],
                   NULL AS [DateModified],
                   0 AS [ID_CreatedBy],
                   0 AS [ID_LastModifiedBy],
                   1 AS [ID_FilingStatus],
                   0 AS [ID_Taxscheme]
        ) H
            LEFT JOIN dbo.tUser UC
                ON H.ID_CreatedBy = UC.ID
            LEFT JOIN dbo.tUser UM
                ON H.ID_LastModifiedBy = UM.ID
            LEFT JOIN dbo.tFilingStatus fs
                ON H.ID_FilingStatus = fs.ID;
    END;
    ELSE
    BEGIN
        SELECT H.*
        FROM vReceivingReport H
        WHERE H.ID = @ID;
    END;

    SELECT *
    FROM dbo.vReceivingReport_Detail
    WHERE ID_ReceivingReport = @ID;
END;
GO
