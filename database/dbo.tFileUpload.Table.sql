﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tFileUpload](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_Model] [uniqueidentifier] NULL,
	[Path] [varchar](300) NULL,
	[ID_CurrentObject] [varchar](100) NULL,
	[FileName] [varchar](100) NULL,
 CONSTRAINT [PK_tFileUpload] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tFileUpload] ADD  CONSTRAINT [DF__tFileUplo__IsAct__567ED357]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tFileUpload]  WITH NOCHECK ADD  CONSTRAINT [FK_tFileUpload_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tFileUpload] CHECK CONSTRAINT [FK_tFileUpload_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tFileUpload]  WITH NOCHECK ADD  CONSTRAINT [FK_tFileUpload_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tFileUpload] CHECK CONSTRAINT [FK_tFileUpload_ID_LastModifiedBy]
GO
