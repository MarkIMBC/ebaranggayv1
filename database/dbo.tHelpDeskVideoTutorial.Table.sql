﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tHelpDeskVideoTutorial](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[VideoLink] [varchar](300) NULL,
	[SearcgTag] [varchar](300) NULL,
	[ThumbnailImage] [varchar](300) NULL,
	[SearchTag] [varchar](300) NULL,
 CONSTRAINT [PK_tHelpDeskVideoTutorial] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tHelpDeskVideoTutorial] ON [dbo].[tHelpDeskVideoTutorial]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tHelpDeskVideoTutorial] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tHelpDeskVideoTutorial]  WITH CHECK ADD  CONSTRAINT [FK_tHelpDeskVideoTutorial_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tHelpDeskVideoTutorial] CHECK CONSTRAINT [FK_tHelpDeskVideoTutorial_ID_Company]
GO
ALTER TABLE [dbo].[tHelpDeskVideoTutorial]  WITH CHECK ADD  CONSTRAINT [FK_tHelpDeskVideoTutorial_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tHelpDeskVideoTutorial] CHECK CONSTRAINT [FK_tHelpDeskVideoTutorial_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tHelpDeskVideoTutorial]  WITH CHECK ADD  CONSTRAINT [FK_tHelpDeskVideoTutorial_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tHelpDeskVideoTutorial] CHECK CONSTRAINT [FK_tHelpDeskVideoTutorial_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tHelpDeskVideoTutorial]
		ON [dbo].[tHelpDeskVideoTutorial]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tHelpDeskVideoTutorial
			SET    DateCreated = GETDATE()
			FROM   dbo.tHelpDeskVideoTutorial hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tHelpDeskVideoTutorial] ENABLE TRIGGER [rDateCreated_tHelpDeskVideoTutorial]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tHelpDeskVideoTutorial]
		ON [dbo].[tHelpDeskVideoTutorial]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tHelpDeskVideoTutorial
			SET    DateModified = GETDATE()
			FROM   dbo.tHelpDeskVideoTutorial hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tHelpDeskVideoTutorial] ENABLE TRIGGER [rDateModified_tHelpDeskVideoTutorial]
GO
