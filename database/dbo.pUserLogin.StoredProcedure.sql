﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pUserLogin]
    @Username VARCHAR(100) ,
    @Password VARCHAR(100)
AS
    BEGIN
        DECLARE @ID_User INT
        SELECT  @ID_User = ID
        FROM    dbo.tUser
        WHERE   UPPER(Username) = @Username 

        DECLARE @Msg VARCHAR(500) =  'Username (' + ISNULL(@Username,'Username') + ') does not exists.'	

        IF ( @ID_User IS NULL )
            BEGIN

				SELECT  '' AS [_]
					  
				SELECT CONVERT(bit,0) success, @Msg message
		
                RETURN
            END	

        IF NOT EXISTS ( SELECT  ID
                    FROM    dbo.tUser
                    WHERE   ID = @ID_User
                            AND IsActive = 1 )
            BEGIN

				SELECT  '' AS [_]
					  
				SELECT CONVERT(bit,0) success, 'User is not currently active.' message

                RETURN
            END;

        IF NOT	EXISTS ( SELECT  ID
                    FROM    dbo.tUser
                    WHERE   ID = @ID_User
                            AND ISNULL(Password, '') = ISNULL(Password, '') COLLATE SQL_Latin1_General_CP1_CS_AS )
            BEGIN

			    SELECT  '' AS [_]
					  
				SELECT CONVERT(bit,0) success, 'Username and password does not match.' message
                RETURN
            END;

		INSERT INTO dbo.tUserSession
		        ( Code ,
		          Name ,
		          IsActive ,
		          Comment ,
		          ID_User ,
				  DateCreated ,
		          ID_Warehouse
		        )
		VALUES  ( NULL , -- Code - varchar(50)
		          NULL , -- Name - varchar(200)
		          1 , -- IsActive - bit
		          NULL , -- Comment - varchar(200)
		          @ID_User , -- ID_User - int
				  GETDATE() ,
		          NULL  -- ID_Warehouse - int
		        )

		DECLARE @ID_Session INT = @@IDENTITY

        SELECT  '' AS [_]

        SELECT  U.ID ,
				E.Name ,
                U.IsActive ,
                U.Comment ,
                U.ID_Employee ,
                U.Username ,
                U.ID_UserGroup ,
                E.Name AS Employee ,
				EP.Name AS Position,
				E.ImageFile,
				ISNULL(U.IsRequiredPasswordChangedOnLogin,0) AS IsRequiredPasswordChangedOnLogin,
				CAST(( CASE WHEN LEN(ISNULL(U.Password,'')) > 0 THEN 1 ELSE 0 END ) AS BIT) AS HasPassword,
				@ID_Session AS ID_Session,
				E.ID_Company,
				company.Name Name_Company
        FROM    dbo.tUser U
                LEFT JOIN dbo.tEmployee E ON U.ID_Employee = E.ID
				LEFT JOIN dbo.tEmployeePosition EP ON E.ID_EmployeePosition = EP.ID
				LEFT JOIN dbo.tCompany company on E.ID_Company = company.ID
        WHERE   U.ID = @ID_User

    END
GO
