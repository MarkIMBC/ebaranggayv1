﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vMessageThread]
AS
SELECT H.ID,
       H.Recipient_ID_User,
       H.Sender_ID_User,
       H.Message,
       H.DateSent,
       H.DateRead,
       H.IsRead,
	   recipientUser.Name Recipient_Name_User,
	   senderUser.Name Sender_Name_User
FROM tMessageThread H
    LEFT JOIN tUser recipientUser
        ON H.Recipient_ID_User = recipientUser.ID
    LEFT JOIN tUser senderUser
        ON H.Sender_ID_User = senderUser.ID
GO
