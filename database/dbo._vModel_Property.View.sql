﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[_vModel_Property]
AS
    SELECT  H.*,
			PT.Name AS PropertyType,
			M.Name AS Model,
			MP.Name AS PropertyModel
    FROM    dbo.[_tModel_Property] H
	LEFT JOIN dbo.[_tPropertyType] PT ON H.ID_PropertyType = PT.ID
	LEFT JOIN dbo.[_tModel] M ON H.ID_Model = M.Oid
	LEFT JOIN dbo.[_tModel] MP ON H.ID_PropertyModel = MP.Oid
GO
