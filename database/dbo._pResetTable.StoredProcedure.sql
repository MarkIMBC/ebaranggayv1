﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[_pResetTable]
    @TableName VARCHAR(300) ,
    @Reseed INT = NULL
AS
IF @Reseed IS NULL
BEGIN
	

	DECLARE @out TABLE ( LastID INT )
    INSERT  INTO @out
    EXEC ( 'SELECT TOP 1 ID FROM ' + @TableName + ' ORDER BY ID DESC')
	SELECT TOP 1 @Reseed = LastID FROM @out
END

IF @Reseed IS NULL
	SET @Reseed = 0

EXEC('DELETE FROM ' + @TableName)
DBCC CHECKIDENT (@TableName, RESEED, @Reseed)
GO
