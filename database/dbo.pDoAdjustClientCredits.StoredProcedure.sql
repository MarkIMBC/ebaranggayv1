﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pDoAdjustClientCredits](@ClientCredits  typClientCredit READONLY,
                                  @ID_UserSession INT)
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @Success BIT = 1;
      DECLARE @message VARCHAR(300) = '';

      SELECT @ID_User = ID_User
      FROM   tUserSession
      WHERE  ID = @ID_UserSession

      BEGIN TRY
          EXEC pAdjustClientCredits_validation
            @ClientCredits,
            @ID_UserSession

          exec pAdjustClientCredits
            @ClientCredits,
            @ID_UserSession
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @Success = 0;

      END CATCH
	  
      SELECT '_';

      SELECT @Success Success,
             @message message;
  END

GO
