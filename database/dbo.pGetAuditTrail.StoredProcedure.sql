﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pGetAuditTrail] @ID INT
AS
    SELECT  '' AS [_] ,
            '' AS [AuditTrail_Detail]

    SELECT  *
    FROM    dbo.vAuditTrail
    WHERE   ID = @ID


    SELECT  D.*,
			AT.Name AS AuditTrailType
    FROM    dbo.tAuditTrail_Detail D
            LEFT  JOIN dbo.tAuditTrailType AT ON D.ID_AuditTrailType = AT.ID
    WHERE   D.ID_AuditTrail = @ID
GO
