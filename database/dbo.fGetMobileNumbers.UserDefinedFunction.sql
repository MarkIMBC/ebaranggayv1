﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE   
 FUNCTION [dbo].[fGetMobileNumbers](@Number1 VARCHAR(20),  
                                       @Number2 VARCHAR(20))  
RETURNS VARCHAR(MAX)  
AS  
  BEGIN  
      DECLARE @result VARCHAR(MAX) = ''  
  
      SET @Number1 = REPLACE(REPLACE(REPLACE(REPLACE(LTRIM(RTRIM(ISNULL(@Number1, ''))), ' ', ''), CHAR(10), ''), CHAR(9), ''), CHAR(13), '')  
      SET @Number2 = REPLACE(REPLACE(REPLACE(REPLACE(LTRIM(RTRIM(ISNULL(@Number2, '0'))), ' ', ''), CHAR(10), ''), CHAR(9), ''), CHAR(13), '')  
      SET @Number1 = TRIM(@Number1)
	  SET @Number2 = TRIM(@Number2)

	    SET @Number1 = REPLACE(@Number1, CHAR(160), '')
	  SET @Number2 =  REPLACE(@Number2, CHAR(160), '')
	  
	    SET @Number1 = REPLACE(@Number1, CHAR(32), '')
	  SET @Number2 =  REPLACE(@Number2, CHAR(32), '')

	    SET @Number1 = REPLACE(@Number1, ' ', '')
	  SET @Number2 =  REPLACE(@Number2, ' ', '')
	  
	  SET @result = @Number1  
  
      if LEN(@result) = 0  
        SET @result = @Number2  

		SET @result = REPLACE(LTRIM(RTRIM(ISNULL(@result, '0'))), '-', '')

  
      RETURN @result  
  END  
  
GO
