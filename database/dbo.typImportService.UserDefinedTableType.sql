﻿CREATE TYPE [dbo].[typImportService] AS TABLE(
	[Old_procedure_id] [int] NULL,
	[procedure_name] [varchar](max) NULL,
	[procedure_php] [decimal](18, 2) NULL
)
GO
