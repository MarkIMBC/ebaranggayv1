﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[_vListView_Detail]
AS

    SELECT  
			D.Oid ,
            D.Code ,
            D.Name ,
            D.IsActive ,
            D.Comment ,
            D.Caption ,
            D.ID_ModelProperty ,
            D.ID_ListView ,
            D.ID_CreatedBy ,
            D.ID_LastModifiedBy ,
            D.DateCreated ,
            D.DateModified ,
            D.DisplayProperty ,
            D.DataSource AS [$DataSource],
            D.Format ,
            D.Width ,
            D.Fixed ,
            D.VisibleIndex ,
            D.IsAllowEdit ,
            D.ID_ControlType ,
            D.ID_ColumnAlignment ,
            D.IsVisible ,
            D.FixedPosition ,
            D.IsRequired ,
            D.ID_SummaryType ,
            D.Precision ,
            ISNULL(D.ID_PropertyType, MP.ID_PropertyType) AS ID_PropertyType,
			--MP.ID_PropertyType,
			MP.Name AS ModelProperty,
			D.IsAddModelClass,
			D.DataSource
    FROM    dbo.[_tListView_Detail] D
            LEFT JOIN dbo.[_tModel_Property] MP ON D.ID_ModelProperty = MP.Oid
GO
