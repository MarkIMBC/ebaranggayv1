﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vResident_Program]
AS
  SELECT H.*,
         UC.Name   AS CreatedBy,
         UM.Name   AS LastModifiedBy,
         prog.Name Name_Program
  FROM   tResident_Program H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tProgram prog
                ON prog.ID = h.ID_Program

GO
