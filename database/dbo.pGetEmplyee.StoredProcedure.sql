﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pGetEmplyee] @ID INT = -1, @ID_Session INT = NULL
AS
BEGIN
  SELECT
    '_'

  DECLARE @ID_User INT
         ,@ID_Warehouse INT;

  DECLARE @ID_Company INT;
  DECLARE @ID_Employee INT;

  SELECT
    @ID_User = ID_User
   ,@ID_Warehouse = ID_Warehouse
  FROM tUserSession
  WHERE ID = @ID_Session;

  SELECT
    @ID_Employee = ID_Employee
   ,@ID_Company = emp.ID_Company
  FROM dbo.tUser _user
  INNER JOIN dbo.tEmployee emp
    ON emp.ID = _user.ID_Employee
  WHERE _user.ID = @ID_User;


  IF (@ID = -1)
  BEGIN
    SELECT
      H.*
    FROM (SELECT
        NULL AS [_]
       ,-1 AS [ID]
       ,112312123 AS [Code]
       ,NULL AS [Name]
       ,1 AS [IsActive]
       ,NULL AS [Comment]
       ,NULL AS [DateCreated]
       ,NULL AS [DateModified]
       ,NULL AS [ID_CreatedBy]
       ,NULL AS [ID_LastModifiedBy]
       ,@ID_Company [ID_Company]
       ,'sfsadfasdf' [Company]) H
    LEFT JOIN tUser UC
      ON H.ID_CreatedBy = UC.ID
    LEFT JOIN tUser UM
      ON H.ID_LastModifiedBy = UM.ID
  END
  ELSE
  BEGIN
    SELECT
      H.*
    FROM vEmplyee H
    WHERE H.ID = @ID
  END
END

GO
