﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pAuditTrail]
	@Name VARCHAR(300) = NULL,
	@Description VARCHAR(300) = NULL,
    @ID_AuditTrailType INT = NULL,
	@ID_Session INT = NULL,
    @ID_Model VARCHAR(MAX) = NULL,
    @ID_CurrentObject VARCHAR(MAX) ,
    @Details typAuditTrail READONLY
AS
	DECLARE @ID_User INT
	SELECT @ID_User = ID_User FROM dbo.tUserSession WHERE ID = @ID_Session

    INSERT  INTO dbo.tAuditTrail
            ( Code ,
              Name ,
              IsActive ,
              Comment ,
              ID_Model ,
              Date ,
              ID_User ,
              ID_CurrentObject ,
              ID_AuditType ,
              ID_DetailView ,
              ID_Session ,
			  Description
            )
    VALUES  ( NULL , -- Code - varchar(50)
              @Name , -- Name - varchar(max)
              NULL , -- IsActive - bit
              NULL , -- Comment - varchar(200)
              @ID_Model , -- ID_Model - uniqueidentifier
              GETDATE() , -- Date - datetime
              @ID_User , -- ID_User - int
              @ID_CurrentObject , -- ID_CurrentObject - varchar(50)
              @ID_AuditTrailType , -- ID_AuditType - int
              NULL , -- ID_DetailView - int
              @ID_Session ,  -- ID_Session - int
			  @Description
			)

	DECLARE @ID INT = @@IDENTITY

	INSERT INTO dbo.tAuditTrail_Detail
	        ( Code ,
	          Name ,
			  ModelProperty,
	          IsActive ,
	          Comment ,
	          ID_Model ,
	          ID_AuditTrail ,
	          ID_CurrentObject ,
	          OldValue ,
	          NewValue,
			  ID_AuditTrailType
	        )
	SELECT NULL, PropertyName, ModelProperty, 1, NULL, ID_Model, @ID, @ID_CurrentObject, OldValue, NewValue, ID_AuditTrailType FROM @Details
GO
