﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vRpt_ItemList]
AS
SELECT H.ID,
       H.Code,
       H.Name,
       H.IsActive,
       H.Comment,
       H.DateCreated,
       H.DateModified,
       H.ID_CreatedBy,
       H.ID_LastModifiedBy,
       UC.Name AS CreatedBy,
       UM.Name AS LastModifiedBy,
       H.ID_Company,
       company.ImageLogoLocationFilenamePath,
       company.Name Name_Company,
       company.Address Address_Company
FROM dbo.tItem H
    LEFT JOIN dbo.tUser UC
        ON H.ID_CreatedBy = UC.ID
    LEFT JOIN dbo.tUser UM
        ON H.ID_LastModifiedBy = UM.ID
    LEFT JOIN dbo.vCompany company
        ON company.ID = H.ID_Company;
GO
