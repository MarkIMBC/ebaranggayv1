﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pTransferConfinementItemsServicesFromSOAP](@IDs_Patient_SOAP typINtList READONLY)
AS
  BEGIN
      DECLARE @IDs_Patient_Confinement typINtList

      INSERT @IDs_Patient_Confinement
      SELECT soap.ID_Patient_Confinement
      FRom   tPatient_SOAP_Prescription soapPrescription
             INNER JOIN tPatient_SOAP soap
                     on soap.ID = soapPrescription.ID_Patient_SOAP
             INNER JOIN @IDs_Patient_SOAP ids_SOAP
                     on soapPrescription.ID_Patient_SOAP = ids_SOAP.ID
      WHERE  ISNULL(soap.ID_Patient_Confinement, 0) <> 0

      INSERT INTO [dbo].[tPatient_Confinement_ItemsServices]
                  ([Code],
                   [Name],
                   [IsActive],
                   [ID_Company],
                   [Comment],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [ID_Patient_Confinement],
                   [ID_Patient_SOAP],
                   [ID_Patient_SOAP_Prescription],
                   [ID_Item],
                   [Quantity],
                   [Date],
                   [DateExpiration],
                   [UnitPrice],
                   [UnitCost],
                   [Amount])
      select '',
             '',
             1,
             item.ID_Company,
             soapPrescription.Comment,
             GETDATE(),
             GETDATE(),
             soap.ID_CreatedBy,
             soap.ID_CreatedBy,
             soap.ID_Patient_Confinement,
             soap.ID,
             soapPrescription.ID,
             soapPrescription.ID_Item,
             soapPrescription.Quantity,
             soap.Date,
             item.OtherInfo_DateExpiration,
             item.UnitPrice,
             item.UnitCost,
             0
      FRom   tPatient_SOAP_Prescription soapPrescription
             INNER JOIN tPatient_SOAP soap
                     on soap.ID = soapPrescription.ID_Patient_SOAP
             INNER JOIN @IDs_Patient_SOAP ids_SOAP
                     on soapPrescription.ID_Patient_SOAP = ids_SOAP.ID
             LEFT JOIN tItem item
                    on soapPrescription.ID_Item = item.ID
      WHERE  ISNULL(soap.ID_Patient_Confinement, 0) <> 0

      INSERT INTO [dbo].[tPatient_Confinement_ItemsServices]
                  ([Code],
                   [Name],
                   [IsActive],
                   [ID_Company],
                   [Comment],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [ID_Patient_Confinement],
                   [ID_Patient_SOAP],
                   [ID_Patient_SOAP_Treatment],
                   [ID_Item],
                   [Quantity],
                   [Date],
                   [DateExpiration],
                   [UnitPrice],
                   [UnitCost],
                   [Amount])
      select '',
             '',
             1,
             item.ID_Company,
             soapTreatment.Comment,
             GETDATE(),
             GETDATE(),
             soap.ID_CreatedBy,
             soap.ID_CreatedBy,
             soap.ID_Patient_Confinement,
             soap.ID,
             soapTreatment.ID,
             soapTreatment.ID_Item,
             soapTreatment.Quantity,
             soap.Date,
             item.OtherInfo_DateExpiration,
             item.UnitPrice,
             item.UnitCost,
             0
      FRom   tPatient_SOAP_Treatment soapTreatment
             INNER JOIN tPatient_SOAP soap
                     on soap.ID = soapTreatment.ID_Patient_SOAP
             INNER JOIN @IDs_Patient_SOAP ids_SOAP
                     on soapTreatment.ID_Patient_SOAP = ids_SOAP.ID
             LEFT JOIN tItem item
                    on soapTreatment.ID_Item = item.ID
      WHERE  ISNULL(soap.ID_Patient_Confinement, 0) <> 0

      UPDATE [tPatient_Confinement_ItemsServices]
      SET    Amount = ( ( Convert(Decimal(18, 4), ISNULL(Quantity, 0)) ) * ( ISNULL(UnitPrice, 0) ) )
      FROM   [tPatient_Confinement_ItemsServices] itemServices
             INNER JOIN @IDs_Patient_Confinement IDsConfinement
                     ON IDsConfinement.ID = itemServices.ID_Patient_Confinement

      UPDATE tPatient_Confinement
      SET    SubTotal = tbl.TotalAmount,
             TotalAmount = tbl.TotalAmount
      FROM   tPatient_Confinement confinement
             INNER JOIn @IDs_Patient_Confinement ids_Confinement
                     ON confinement.ID = ids_Confinement.ID
             INNER JOIN (SELECT itemServices.ID_Patient_Confinement,
                                SUM(Amount) TotalAmount
                         FROM   [tPatient_Confinement_ItemsServices] itemServices
                         GROUP  BY itemServices.ID_Patient_Confinement) tbl
                     ON confinement.ID = tbl.ID_Patient_Confinement
  END 
GO
