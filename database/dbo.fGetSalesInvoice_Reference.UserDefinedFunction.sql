﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fGetSalesInvoice_Reference]
    (
      @ID_DeliveryReceipt INT
    )
RETURNS VARCHAR(500)
    BEGIN
        DECLARE @Reference VARCHAR(500)
        SELECT  @Reference = STUFF(( SELECT ', ' + hed.ReferenceNo
                                     FROM   dbo.tSalesInvoice hed
                                            INNER JOIN ( 
														---- DR
                                                         SELECT  DISTINCT
                                                              ID_Doc ,
                                                              ID_SalesInvoice
                                                         FROM dbo.tSalesInvoice_Detail
                                                         WHERE
                                                              ID_DocModel = '0D6280D1-891A-47FA-BFB9-121FF43FD269'
                                                              AND ID_Doc = @ID_DeliveryReceipt
                                                         UNION ALL
					 
														 --- SO
                                                         SELECT DISTINCT
                                                              ID_Doc ,
                                                              ID_SalesInvoice
                                                         FROM dbo.tSalesInvoice_Detail
                                                         WHERE
                                                              ID_Doc IN (
                                                              SELECT
                                                              ID_Doc
                                                              FROM
                                                              dbo.tDeliveryReceipt_Detail
                                                              WHERE
                                                              ID_DeliveryReceipt = @ID_DeliveryReceipt )
                                                       ) det ON det.ID_SalesInvoice = hed.ID
                                     WHERE  hed.ID_FilingStatus NOT IN ( 1, 3 )
                                   FOR
                                     XML PATH('')
                                   ), 1, 1, '')
        RETURN @Reference
    END
GO
