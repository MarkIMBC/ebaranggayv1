﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pResetPassword] @ID_User INT, @OldPassword VARCHAR(300), @NewPassword VARCHAR(300)
AS
BEGIN
	IF NOT EXISTS( SELECT * FROM dbo.tUser WHERE ID = @ID_User AND ISNULL(Password,'') = ISNULL(@OldPassword,'') COLLATE SQL_Latin1_General_CP1_CS_AS)
	BEGIN
		THROW 51000,'The Old Password entered was invalid',1;
	END

	UPDATE dbo.tUser SET Password = @NewPassword, IsRequiredPasswordChangedOnLogin = 0 WHERE ID = @ID_User
END
GO
