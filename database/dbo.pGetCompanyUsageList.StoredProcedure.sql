﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE     Proc [dbo].[pGetCompanyUsageList]  
AS  
  BEGIN  
      Declare @ComapanyUsageList TABLE  
        (  
           ID_Company       INT,  
           Name_company     VARCHAR(MAX),  
           LastDateAccess   DateTime,  
           TotalAccessCount INT,  
           LastBillDate     DateTime,  
           TotalBillCount   INT,  
           LastSOAPDate     DateTime,  
           TotalSOAPCount   INT  
        )  
  
      INSERT @ComapanyUsageList  
             (ID_Company,  
              Name_company)  
      SELECT ID,  
             Name  
      FROM   tCompany c where IsActive = 1  
  
      Update @ComapanyUsageList  
      SET    LastDateAccess = tbl.LastDateAccess,  
             TotalAccessCount = tbl.TotalAccessCount  
      FROM   @ComapanyUsageList record  
             inner join (SELECT c.ID                                ID_Company,  
                                c.Name                              Name_Company,  
                                CONVERT(DATE, Max(auditTrail.Date)) LastDateAccess,  
                                Count(auditTrail.ID)                TotalAccessCount  
                         FROM   tCompany c  
                                LEFT JOIN vUser _user  
                                       on c.ID = _user.ID_Company  
                                LEFT JOIN tAuditTrail auditTrail  
                                       on auditTrail.ID_User = _user.ID  
                         GROUP  BY c.ID,  
                                   c.Name) tbl  
                     on record.ID_Company = tbl.ID_Company  
  
      Update @ComapanyUsageList  
      SET    LastSOAPDate = tbl.LastSOAPDate,  
             TotalSOAPCount = tbl.TotalSOAPCount  
      FROM   @ComapanyUsageList record  
             inner join (SELECT c.ID                                ID_Company,  
                                c.Name                              Name_Company,  
                                CONVERT(DATE, Max(auditTrail.Date)) LastSOAPDate,  
                                Count(auditTrail.ID)                TotalSOAPCount  
                         FROM   tCompany c  
                                LEFT JOIN tPatient_SOAP auditTrail  
                                       on auditTrail.ID_Company = c.ID  
                         GROUP  BY c.ID,  
                                   c.Name) tbl  
                     on record.ID_Company = tbl.ID_Company  
  
      Update @ComapanyUsageList  
      SET    LastBillDate = tbl.LastBillDate,  
             TotalBillCount = tbl.TotalBillCount  
      FROM   @ComapanyUsageList record  
             inner join (SELECT c.ID                                ID_Company,  
                                c.Name                              Name_Company,  
                                CONVERT(DATE, Max(auditTrail.Date)) LastBillDate,  
                                Count(auditTrail.ID)                TotalBillCount  
                         FROM   tCompany c  
                                LEFT JOIN tBillingInvoice auditTrail  
                                       on auditTrail.ID_Company = c.ID  
                         GROUP  BY c.ID,  
                                   c.Name) tbl  
                     on record.ID_Company = tbl.ID_Company  
  
  
 SELECT * FROM @ComapanyUsageList ORDER BY  Name_Company  
  
  END   
  
  
  
GO
