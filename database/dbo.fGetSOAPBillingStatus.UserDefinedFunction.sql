﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 FUNCTION [dbo].[fGetSOAPBillingStatus](@Filed_Count         INT = 0,
                                     @Approved_Count      INT = 0,
                                     @Done_Count     INT = 0,
                                     @Pending_Count       INT = 0,
                                     @PartiallyPaid_Count INT = 0,
                                     @FullyPaid_Count INT = 0)
ReTURNS INT
AS
  BEGIN

	DECLARE @ID_FilingStatus INT = NULL

	Declare @TotalCount INT = ISNULL(@Filed_Count, 0) +
	ISNULL(@Approved_Count, 0) +
	 ISNULL(@Done_Count, 0) +
	ISNULL(@Pending_Count, 0) +
	ISNULL(@PartiallyPaid_Count, 0) +
	ISNULL(@FullyPaid_Count, 0)

	SET @Filed_Count = ISNULL(@Filed_Count, 0)
	SET @Approved_Count = ISNULL(@Approved_Count, 0)
	SET @Done_Count = ISNULL(@Done_Count, 0)
	SET @Pending_Count = ISNULL(@Pending_Count, 0)
	SET @PartiallyPaid_Count = ISNULL(@PartiallyPaid_Count, 0)
	SET @FullyPaid_Count = ISNULL(@FullyPaid_Count, 0)

	  IF( @TotalCount = @FullyPaid_Count AND @FullyPaid_Count > 0 )
        SET @ID_FilingStatus = 12
      ELSE IF( @TotalCount = 0 )
        SET @ID_FilingStatus = 16
      ELSE IF( @TotalCount = @Done_Count  AND @Done_Count > 0 )
        SET @ID_FilingStatus = 12
      ELSE IF ( @PartiallyPaid_Count > 0 )
        SET @ID_FilingStatus = 11
      ELSE IF ( @Pending_Count > 0 )
        SET @ID_FilingStatus = 2
      ELSE IF ( @Filed_Count > 0 )
        SET @ID_FilingStatus = 1


      return @ID_FilingStatus
  END

GO
