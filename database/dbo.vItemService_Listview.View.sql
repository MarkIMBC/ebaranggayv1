﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   VIEW [dbo].[vItemService_Listview]
AS  
	SELECT  item.ID  
		   ,item.Name  
		   ,item.ID_Company  
		   ,ISNULL(item.UnitPrice, 0) UnitPrice
		   ,item.IsActive  
	FROM dbo.titem item  
	WHERE   
		item.ID_ItemType = 1  AND
		ISNULL(item.IsActive, 0) = 1
GO
