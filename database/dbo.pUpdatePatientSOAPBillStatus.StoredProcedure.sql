﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pUpdatePatientSOAPBillStatus](@IDs_Patient_SOAP               TYPINTLIST READONLY,
                                        @BillingInvoice_ID_FilingStatus INT,
                                        @ID_UserSession                 INT)
AS
    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';
    DECLARE @ID_User INT
    DECLARE @ID_Warehouse INT
    DECLARE @ID_Company INT

    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM   tUserSession
    WHERE  ID = @ID_UserSession

    SELECT @ID_Company = ID_Company
    FROM   vUser
    WHERE  ID = @ID_User

    BEGIN TRY
        /* Update Waiting List Status */
        UPDATE tPatient_SOAP
        SET    BillingInvoice_ID_FilingStatus = @BillingInvoice_ID_FilingStatus
        FROM   tPatient_SOAP _SOAP
               INNER JOIN @IDs_Patient_SOAP ids
                       ON _SOAP.ID = ids.ID;
							   
    END TRY
    BEGIN CATCH
        SET @message = ERROR_MESSAGE();
        SET @Success = 0;
    END CATCH;


    SELECT '_';

    SELECT @Success Success,
           @message message; 
GO
