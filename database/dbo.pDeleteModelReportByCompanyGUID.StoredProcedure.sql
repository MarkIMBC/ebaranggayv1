﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pDeleteModelReportByCompanyGUID](@GUID_Company VARCHAR(MAX),
                                           @ModelName    Varchar(200),
                                           @ReportName   VARCHAR(200))
AS
  BEGIN
      DECLARE @OID_Report VARCHAR(MAX) = ''
      DECLARE @OID_Model VARCHAR(MAX) = ''
      DECLARE @ID_Company INT

      SELECT @ID_Company = ID
      FROM   tCompany
      WHERE  Guid = @GUID_Company

      SELECT @OID_Report = Oid
      FROM   _tReport
      where  Name = @ReportName

      SELECT @OID_Model = Oid
      FROM   _tModel
      where  Name = @ModelName

      DELETE FROM _tModelReport
      WHERE  ID_Company = @ID_Company
             and Oid_Report = @OID_Report
  END

GO
