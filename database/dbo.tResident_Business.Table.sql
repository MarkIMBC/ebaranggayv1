﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tResident_Business](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[Resident_BusinessOwner] [varchar](300) NULL,
	[Address] [varchar](300) NULL,
	[DateValid] [datetime] NULL,
	[RequestedBy] [varchar](300) NULL,
	[BusinessName] [varchar](300) NULL,
	[ContactNumber] [varchar](300) NULL,
 CONSTRAINT [PK_tResident_Business] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tResident_Business] ON [dbo].[tResident_Business]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tResident_Business] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tResident_Business]  WITH CHECK ADD  CONSTRAINT [FK_tResident_Business_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tResident_Business] CHECK CONSTRAINT [FK_tResident_Business_ID_Company]
GO
ALTER TABLE [dbo].[tResident_Business]  WITH CHECK ADD  CONSTRAINT [FK_tResident_Business_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tResident_Business] CHECK CONSTRAINT [FK_tResident_Business_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tResident_Business]  WITH CHECK ADD  CONSTRAINT [FK_tResident_Business_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tResident_Business] CHECK CONSTRAINT [FK_tResident_Business_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE TRIGGER [dbo].[rDateCreated_tResident_Business] ON [dbo].[tResident_Business] FOR INSERT
			AS
				DECLARE @ID INT
				SELECT @ID = ID FROM Inserted
				UPDATE dbo.tResident_Business SET DateCreated = GETDATE() WHERE ID = @ID
		
GO
ALTER TABLE [dbo].[tResident_Business] ENABLE TRIGGER [rDateCreated_tResident_Business]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE TRIGGER [dbo].[rDateModified_tResident_Business] ON [dbo].[tResident_Business] FOR UPDATE, INSERT
			AS
				DECLARE @ID INT
				SELECT @ID = ID FROM Inserted
				UPDATE dbo.tResident_Business SET DateModified = GETDATE() WHERE ID = @ID
		
GO
ALTER TABLE [dbo].[tResident_Business] ENABLE TRIGGER [rDateModified_tResident_Business]
GO
