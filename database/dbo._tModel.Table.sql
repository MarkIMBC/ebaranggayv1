﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[_tModel](
	[Oid] [uniqueidentifier] NOT NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](1000) NULL,
	[TableName] [varchar](150) NULL,
	[DisplayName] [varchar](150) NULL,
	[Color1] [varchar](50) NULL,
	[Color2] [varchar](50) NULL,
	[Color3] [varchar](50) NULL,
	[IsReadOnly] [bit] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_DetailView] [uniqueidentifier] NULL,
	[Caption] [varchar](300) NULL,
	[ViewSource] [varchar](300) NULL,
	[ControllerPath] [varchar](300) NULL,
	[Icon] [varchar](300) NULL,
	[IsLoadData] [bit] NULL,
	[IsEnableAuditTrail] [bit] NULL,
	[IsEnableComment] [bit] NULL,
	[IsEnableFileAttachment] [bit] NULL,
	[IsSearchEnabled] [bit] NULL,
 CONSTRAINT [PK__tModel] PRIMARY KEY CLUSTERED 
(
	[Oid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[_tModel]  WITH NOCHECK ADD  CONSTRAINT [FK_tModel_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[_tModel] CHECK CONSTRAINT [FK_tModel_ID_CreatedBy]
GO
ALTER TABLE [dbo].[_tModel]  WITH NOCHECK ADD  CONSTRAINT [FK_tModel_ID_DetailView] FOREIGN KEY([ID_DetailView])
REFERENCES [dbo].[_tDetailView] ([Oid])
GO
ALTER TABLE [dbo].[_tModel] CHECK CONSTRAINT [FK_tModel_ID_DetailView]
GO
ALTER TABLE [dbo].[_tModel]  WITH NOCHECK ADD  CONSTRAINT [FK_tModel_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[_tModel] CHECK CONSTRAINT [FK_tModel_ID_LastModifiedBy]
GO
