﻿CREATE TYPE [dbo].[typAuditTrail] AS TABLE(
	[ModelProperty] [varchar](300) NULL,
	[PropertyName] [varchar](300) NULL,
	[OldValue] [varchar](max) NULL,
	[NewValue] [varchar](max) NULL,
	[ID_PropertyType] [int] NULL,
	[ID_AuditTrailType] [int] NULL,
	[ID_Model] [uniqueidentifier] NULL
)
GO
