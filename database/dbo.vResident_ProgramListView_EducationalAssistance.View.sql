﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vResident_ProgramListView_EducationalAssistance]
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid         Guid_Company,
         program.ID_Program   ID_Program,
         program.Name_Program Name_Program
  FROM   vResident hed
         INNER JOIN vResident_Program program
                 on hed.ID = program.ID_Resident
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  WHERE  program.Name_Program IN ( 'Educational Financial Assistance (Senior High)', 'Educational Financial Assistance (College)', 'Senior High Educational Assistance', 'College Educational Assistance' )
         AND hed.IsActive = 1
         AND ISNULL(hed.IsDeceased, 0) = 0

GO
