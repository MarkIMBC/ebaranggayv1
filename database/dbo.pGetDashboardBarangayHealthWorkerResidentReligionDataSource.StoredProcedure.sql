﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create   
 PROC [dbo].[pGetDashboardBarangayHealthWorkerResidentReligionDataSource] (@ID_UserSession INT,
                                                                            @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear                DateYear,
             'Relihiyon ' + @DateYear Label

      SELECT Name_Religion,
             Count(*) Count
      FROM   vResident resident
             inner join vCompanyActive c
                     on resident.ID_Company = c.ID
      WHERE  resident.IsActive = 1
             AND c.ID NOT IN ( 1 )
             AND YEAR(resident.DateCreated) = @DateYear
             and ISNULL(resident.IsDeceased, 0) = 0
             and ISNULL(resident.ID_Religion, 0) > 0
      GROUP  BY Name_Religion
  END

GO
