﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tSample1](
	[RowIndex] [bigint] NULL,
	[ID_Company] [int] NULL,
	[Name_Company] [varchar](max) NULL,
	[Name_Client] [varchar](max) NULL,
	[ContactNumber_Client] [varchar](max) NULL,
	[DateReturn] [datetime] NULL,
	[Name_Item] [varchar](max) NULL,
	[Comment] [varchar](max) NULL,
	[Message] [varchar](max) NULL,
	[DateSending] [datetime] NULL,
	[DateCreated] [datetime] NULL,
	[ID_Reference] [int] NULL,
	[Oid_Model] [varchar](max) NULL,
	[Code] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
