﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tTeacher](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[FirstName] [varchar](300) NULL,
	[LastName] [varchar](300) NULL,
	[MiddleName] [varchar](300) NULL,
	[Address] [varchar](300) NULL,
	[Email] [varchar](300) NULL,
	[PhoneNumber] [varchar](300) NULL,
 CONSTRAINT [PK_tTeacher] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tTeacher] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tTeacher]  WITH CHECK ADD  CONSTRAINT [FK_tTeacher_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tTeacher] CHECK CONSTRAINT [FK_tTeacher_ID_Company]
GO
ALTER TABLE [dbo].[tTeacher]  WITH CHECK ADD  CONSTRAINT [FK_tTeacher_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tTeacher] CHECK CONSTRAINT [FK_tTeacher_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tTeacher]  WITH CHECK ADD  CONSTRAINT [FK_tTeacher_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tTeacher] CHECK CONSTRAINT [FK_tTeacher_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tTeacher]
		ON [dbo].[tTeacher]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tTeacher
			SET    DateCreated = GETDATE()
			FROM   dbo.tTeacher hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tTeacher] ENABLE TRIGGER [rDateCreated_tTeacher]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tTeacher]
		ON [dbo].[tTeacher]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tTeacher
			SET    DateModified = GETDATE()
			FROM   dbo.tTeacher hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tTeacher] ENABLE TRIGGER [rDateModified_tTeacher]
GO
