﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pCancelPurchaseOrder]
(
    @IDs_PurchaseOrder typIntList READONLY,
    @ID_UserSession INT
)
AS
BEGIN

    DECLARE @Canceled_ID_FilingStatus INT = 4;
    DECLARE @Pending_ID_FilingStatus INT = 2;

    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';

    BEGIN TRY

        DECLARE @ID_User INT = 0;

        SELECT @ID_User = ID_User
        FROM dbo.tUserSession
        WHERE ID = @ID_UserSession;

        EXEC dbo.pCancelPurchaseOrder_validation @IDs_PurchaseOrder,
                                                  @ID_UserSession;

        UPDATE dbo.tPurchaseOrder
        SET ID_FilingStatus = @Canceled_ID_FilingStatus,
            DateCanceled = GETDATE(),
            ID_CanceledBy = @ID_User
        FROM dbo.tPurchaseOrder bi
            INNER JOIN @IDs_PurchaseOrder ids
                ON bi.ID = ids.ID;

    END TRY
    BEGIN CATCH

        SET @message = ERROR_MESSAGE();
        SET @Success = 0;

    END CATCH;


    SELECT '_';

    SELECT @Success Success,
           @message message;

END;

GO
