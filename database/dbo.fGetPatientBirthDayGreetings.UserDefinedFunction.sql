﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fGetPatientBirthDayGreetings] (@CompanyName   VARCHAR(MAX),
                                                     @template      VARCHAR(MAX),
                                                     @Client        VARCHAR(MAX),
                                                     @ContactNumber VARCHAR(MAX),
                                                     @Patient       VARCHAR(MAX),
                                                     @DateBirth     DATETIME)
RETURNS VARCHAR(MAX)
  BEGIN
      DECLARE @TodayDate DATETIME = GetDate()
      DECLARE @TodayBirthDate DATETIME
      DECLARE @YearBirth INT = 0
      DECLARE @YearToday INT = 0
      DECLARE @YearAge INT = 0

      SET @YearBirth = Year(@DateBirth)
      SET @YearToday = Year(@TodayDate)
      SET @YearAge = @YearToday - @YearBirth
      SET @TodayBirthDate = DateAdd(YEAR, @YearAge, @DateBirth)

      DECLARE @DateReturnString VARCHAR(MAX) = Format(@TodayBirthDate, 'dddd MMM. dd, yyyy')
      DECLARE @message VARCHAR(MAX) = @template

      SET @Patient = IsNull(@Patient, 'your Pet')
      SET @Client = IsNull(@Client, '')
      SET @message = Replace(@message, '/*CompanyName*/', lTrim(rTrim(@CompanyName)))
      SET @message = Replace(@message, '/*Client*/', lTrim(rTrim(@Client)))
      SET @message = Replace(@message, '/*ContactNumber*/', lTrim(rTrim(@ContactNumber)))
      SET @message = Replace(@message, '/*Pet*/', lTrim(rTrim(@Patient)))
      SET @message = Replace(@message, '/*DateBirth*/', lTrim(rTrim(IsNull(@DateReturnString, ''))))

      RETURN @message
  END

GO
