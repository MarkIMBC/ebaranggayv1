﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pUpdateBillingInvoiceItemsByPatientConfinement](@IDs_Patient_Confinement TYPINTLIST READONLY)
AS
  BEGIN
      DECLARE @IDs_BillingInvoice TYPINTLIST
      DECLARE @Filed_ID_FilingStatus INT = 1

      INSERT @IDs_BillingInvoice
      SELECT biHed.ID
      FROM   tBillingInvoice biHed
             INNER JOIN @IDs_Patient_Confinement ids_Patient_Confinement
                     ON biHed.ID_Patient_Confinement = ids_Patient_Confinement.ID
      WHERE  ID_FilingStatus = @Filed_ID_FilingStatus

      -- Remove Deleted Confinement Items and Services on BIlling Invoice Items
      DECLARE @ForRemove_IDs_Patient_Confinement_ItemsServices TYPINTLIST

      INSERT @ForRemove_IDs_Patient_Confinement_ItemsServices
      SELECT ID_Patient_Confinement_ItemsServices
      FROM   (SELECT biDetail.ID_Patient_Confinement_ItemsServices
              FROM   tBillingInvoice_Detail biDetail
                     INNER JOIN tBillingInvoice biHed
                             ON biDetail.ID_BillingInvoice = biHed.ID
                     INNER JOIN @IDs_BillingInvoice IDsBI
                             ON biHed.ID = IDsBI.ID
                     INNER JOIN @IDs_Patient_Confinement ids_Patient_Confinement
                             ON biHed.ID_Patient_Confinement = ids_Patient_Confinement.ID
              WHERE  ID_FilingStatus = @Filed_ID_FilingStatus
              EXCEPT
              SELECT itemsServices.ID ID_Patient_Confinement_ItemsServices
              FROM   tPatient_Confinement_ItemsServices itemsServices
                     INNER JOIN @IDs_Patient_Confinement ids
                             ON itemsServices.ID_Patient_Confinement = ids.ID) tbl

      DELETE FROM tBillingInvoice_Detail
      WHERE  ID_Patient_Confinement_ItemsServices IN (SELECT ID
                                                      FROM   @ForRemove_IDs_Patient_Confinement_ItemsServices)
             AND ID_BillingInvoice IN (SELECT ID
                                       FROM   @IDs_BillingInvoice)

      -- Update Confinement Items and Services to BIlling Invoice Items
      UPDATE tBillingInvoice_Detail
      SET    ID_Item = confItemsServices.ID_Item,
             Quantity = confItemsServices.Quantity,
             UnitCost = confItemsServices.UnitCost,
             UnitPrice = confItemsServices.UnitPrice,
             DateExpiration = confItemsServices.DateExpiration
      FROM   tBillingInvoice_Detail biDetail
             INNER JOIN tPatient_Confinement_ItemsServices confItemsServices
                     ON biDetail.ID_Patient_Confinement_ItemsServices = confItemsServices.ID
             INNER JOIN @IDs_Patient_Confinement ids_Patient_Confinement
                     ON confItemsServices.ID_Patient_Confinement = ids_Patient_Confinement.ID
             INNER JOIN @IDs_BillingInvoice IDsBI
                     ON biDetail.ID_BillingInvoice = IDsBI.ID

      -- Insert Confinement Items and Services to BIlling Invoice Items
      -- TODO: Dapat able to insert sa seperate patient Confinement
      DECLARE @ForInsert_IDs_Patient_Confinement_ItemsServices TYPINTLIST

      INSERT @ForInsert_IDs_Patient_Confinement_ItemsServices
      SELECT ID_Patient_Confinement_ItemsServices
      FROM   (SELECT itemsServices.ID ID_Patient_Confinement_ItemsServices
              FROM   tPatient_Confinement_ItemsServices itemsServices
                     INNER JOIN @IDs_Patient_Confinement ids
                             ON itemsServices.ID_Patient_Confinement = ids.ID
              EXCEPT
              SELECT biDetail.ID_Patient_Confinement_ItemsServices
              FROM   tBillingInvoice_Detail biDetail
                     INNER JOIN tBillingInvoice biHed
                             ON biDetail.ID_BillingInvoice = biHed.ID
                     INNER JOIN @IDs_BillingInvoice IDsBI
                             ON biHed.ID = IDsBI.ID
                     INNER JOIN @IDs_Patient_Confinement ids_Patient_Confinement
                             ON biHed.ID_Patient_Confinement = ids_Patient_Confinement.ID
              WHERE  ID_FilingStatus = @Filed_ID_FilingStatus) tbl

      INSERT INTO [dbo].[tBillingInvoice_Detail]
                  ([IsActive],
                   [ID_Company],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [ID_BillingInvoice],
                   [ID_Item],
                   [Quantity],
                   [UnitPrice],
                   [UnitCost],
                   [DateExpiration],
                   [Amount],
                   [IsComputeDiscountRate],
                   [DiscountAmount],
                   [DiscountRate],
                   [ID_Patient_Confinement_ItemsServices])
      SELECT 1,
             bihed.ID_Company,
             GetDate(),
             GetDate(),
             1,
             1,
             bihed.ID,
             itemsServices.ID_Item,
             itemsServices.Quantity,
             itemsServices.UnitPrice,
             itemsServices.UnitCost,
             itemsServices.DateExpiration,
             0,
             0,
             0,
             0,
             itemsServices.ID
      FROM   tPatient_Confinement_ItemsServices itemsServices
             INNER JOIN tPatient_Confinement confinement
                     ON itemsServices.ID_Patient_Confinement = confinement.ID
             INNER JOIN @ForInsert_IDs_Patient_Confinement_ItemsServices forINsertItemsServices
                     ON itemsServices.ID = forINsertItemsServices.ID
             INNER JOIN tBillingInvoice bihed
                     ON bihed.ID_Patient_Confinement = confinement.ID
             INNER JOIN @IDs_BillingInvoice idsBI
                     ON bihed.ID = idsBI.ID

      EXEC dbo.pModel_AfterSaved_BillingInvoice_Computation
        @IDs_BillingInvoice
  END

GO
