﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[_pAddModelDetail]
    @TableName VARCHAR(300) ,
    @FieldKey VARCHAR(300) ,
    @ParentTable VARCHAR(300) ,
    @ParentTableModel VARCHAR(300) = NULL
AS
    SET NOCOUNT OFF

    BEGIN TRANSACTION;
    BEGIN TRY

        DECLARE @ID_Model UNIQUEIDENTIFIER = NULL ,
            @ModelName VARCHAR(300) ,
            @ID_Model_Parent UNIQUEIDENTIFIER = NULL ,
            @Oid_FieldKey UNIQUEIDENTIFIER ,
            @ID_Model_Parent_Key VARCHAR(200)

        SELECT  @ID_Model = Oid ,
                @ModelName = Name
        FROM    dbo.[_tModel]
        WHERE   TableName = @TableName

        SELECT  @ID_Model_Parent = Oid ,
                @ID_Model_Parent_Key = PrimaryKey
        FROM    dbo.[_vModel]
        WHERE   TableName = @ParentTable

        SELECT  @Oid_FieldKey = Oid
        FROM    dbo.[_tModel_Property]
        WHERE   ID_Model = @ID_Model
                AND Name = @FieldKey

        IF ( @ID_Model IS NULL )
            BEGIN
                PRINT 'Model (' + @TableName + ') not found'
                RETURN
            END

        IF ( @ID_Model_Parent IS NULL )
            BEGIN
                PRINT 'Model (' + @ParentTable + ') not found'
                RETURN
            END

        IF ( @Oid_FieldKey IS NULL )
            BEGIN
                PRINT 'ModelProperty (' + @FieldKey + ') not found'
                RETURN
            END

        DECLARE @ID_ParentTable_Model UNIQUEIDENTIFIER = NULL
        IF ( @ParentTableModel IS NOT NULL )
            BEGIN
                SELECT  @ID_ParentTable_Model = Oid
                FROM    dbo.[_tModel]
                WHERE   TableName = @ParentTableModel
                IF ( @ID_ParentTable_Model IS NULL )
                    BEGIN
                        PRINT 'Parent Table Model (' + @ParentTableModel
                            + ') not found'
                        RETURN
                    END
            END

        DECLARE @Oid_Property UNIQUEIDENTIFIER = NEWID()
        INSERT  INTO dbo.[_tModel_Property]
                ( Oid ,
                  Name ,
                  IsActive ,
                  Caption ,
                  ID_Model ,
                  ID_PropertyType ,
                  ID_CreatedBy ,
                  ID_LastModifiedBy ,
                  DateCreated ,
                  DateModified ,
                  ID_PropertyModel ,
                  ID_ModelProperty_Key
                )
        VALUES  ( @Oid_Property , -- Oid - uniqueidentifier
                  @ModelName , -- Name - varchar(200)
                  1 , -- IsActive - bit
                  NULL , -- Caption - varchar(200)
                  @ID_Model_Parent , -- ID_Model - uniqueidentifier
                  10 , -- ID_PropertyType - int
                  NULL , -- ID_CreatedBy - int
                  NULL , -- ID_LastModifiedBy - int
                  GETDATE() , -- DateCreated - datetime
                  GETDATE() , -- DateModified - datetime
                  @ID_Model , -- ID_PropertyModel - uniqueidentifier
                  @Oid_FieldKey  -- ID_ModelProperty_Key - uniqueidentifier
                )


        DECLARE @Oid_ListView UNIQUEIDENTIFIER = NEWID()

        INSERT  INTO dbo.[_tListView]
                ( Oid ,
                  Code ,
                  Name ,
                  IsActive ,
                  Comment ,
                  ID_CreatedBy ,
                  DateCreated ,
                  ID_Model ,
                  DataSource
		        )
        VALUES  ( @Oid_ListView ,
                  NULL ,
                  @ModelName + '_ListView_Detail' ,
                  1 ,
                  NULL ,
                  1 ,
                  GETDATE() ,
                  @ID_Model ,
                  NULL
                )

        INSERT  INTO dbo.[_tListView_Detail]
                ( Oid ,
                  Code ,
                  Name ,
                  IsActive ,
                  Comment ,
                  ID_ListView ,
                  DateCreated ,
                  ID_CreatedBy ,
                  ID_ModelProperty,
                  IsVisible,
                  VisibleIndex
		        )
                SELECT  NEWID() ,
                        NULL ,
                        P.Name ,
                        1 ,
                        NULL ,
                        @Oid_ListView ,
                        GETDATE() ,
                        1 ,
                        P.Oid,
                        1,
                        10
                FROM    dbo.[_tModel_Property] P
                        LEFT JOIN dbo.[_tModel] M ON P.ID_Model = M.Oid
                WHERE   M.Oid = @ID_Model

        DECLARE @ID_DetailView_Control UNIQUEIDENTIFIER	= NEWID()
        INSERT  INTO dbo.[_tDetailView_Detail]
                ( Oid ,
                  Code ,
                  Name ,
                  IsActive ,
                  SeqNo ,
                  Comment ,
                  Caption ,
                  ID_ModelProperty ,
                  ID_DetailView ,
                  ID_ControlType ,
                  ID_ListView,
                  ColSpan
                )
                SELECT  @ID_DetailView_Control ,
                        NULL ,
                        @ModelName ,
                        1 ,
                        NULL ,
                        NULL ,
                        NULL ,
                        @Oid_Property ,
                        Oid ,
                        11 ,
                        @Oid_ListView,
                        12
                FROM    dbo.[_tDetailView]
                WHERE   ID_Model = @ID_Model_Parent

        IF @ID_ParentTable_Model IS NOT NULL
            BEGIN
                INSERT  INTO dbo.[_tDetailView_Detail]
                        ( Oid ,
                          Code ,
                          Name ,
                          IsActive ,
                          SeqNo ,
                          Comment ,
                          Caption ,
                          ID_ModelProperty ,
                          ID_DetailView ,
                          ID_ControlType ,
                          ID_ListView ,
                          ID_Parent_Grid
                        )
                        SELECT  NEWID() ,
                                NULL ,
                                @ModelName ,
                                1 ,
                                NULL ,
                                NULL ,
                                NULL ,
                                @Oid_Property ,
                                Oid ,
                                11 ,
                                @Oid_ListView ,
                                @ID_DetailView_Control
                        FROM    dbo.[_tDetailView]
                        WHERE   ID_Model = @ID_ParentTable_Model
            END

        EXEC dbo.[_pCreateForeignKey] @TableA = @TableName, -- varchar(100)
            @TableA_Column = @FieldKey, -- varchar(100)
            @TableB = @ParentTable, -- varchar(100)
            @ForeignKey = @ID_Model_Parent_Key -- varchar(100)

        SET NOCOUNT ON	

    END TRY	
    BEGIN CATCH
        SELECT  ERROR_NUMBER() AS ErrorNumber ,
                ERROR_SEVERITY() AS ErrorSeverity ,
                ERROR_STATE() AS ErrorState ,
                ERROR_PROCEDURE() AS ErrorProcedure ,
                ERROR_LINE() AS ErrorLine ,
                ERROR_MESSAGE() AS ErrorMessage;

        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;
    END CATCH;


    IF @@TRANCOUNT > 0
        COMMIT TRANSACTION;
GO
