﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pInsertDisaster](@Disaster   VARCHAR(MAX),
                           @ID_Company INT = 1)
as
  BEGIN
      IF(SELECT COUNT(*)
         FROM   tDisaster
         WHERE  Name = @Disaster) = 0
        BEGIN
            INSERT INTO [dbo].tDisaster
                        ([Name],
                         [IsActive],
                         ID_Company)
            VALUES      (@Disaster,
                         1,
                         @ID_Company)
        END
  END

GO
