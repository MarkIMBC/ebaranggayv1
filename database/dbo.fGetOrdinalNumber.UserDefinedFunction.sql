﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 CREATE FUNCTION [dbo].[fGetOrdinalNumber] (@Date DATETIME)
RETURNS VARCHAR(15)
AS
  BEGIN
      DECLARE @day INT;
      DECLARE @strday VARCHAR (15);

      SELECT @day = DAY(@Date);

      SET @strday = CASE
                      WHEN @day IN ( 11, 12, 13 ) THEN CAST(@day AS VARCHAR(10)) + 'th'
                      WHEN @day % 10 = 1 THEN CAST(@day AS VARCHAR(10)) + 'st'
                      WHEN @day % 10 = 2 THEN CAST(@day AS VARCHAR(10)) + 'nd'
                      WHEN @day % 10 = 3 THEN CAST(@day AS VARCHAR(10)) + 'rd'
                      ELSE CAST(@day AS VARCHAR(10)) + 'th'
                    END

      RETURN @strday
  END 

GO
