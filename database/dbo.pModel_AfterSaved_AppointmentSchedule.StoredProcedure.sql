﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pModel_AfterSaved_AppointmentSchedule]
(
    @ID_CurrentObject VARCHAR(10),
    @IsNew BIT = 0
)
AS
BEGIN
    DECLARE @IDs_Schedule typIntList;

    INSERT @IDs_Schedule
    (
        ID
    )
    SELECT ID_Schedule
    FROM dbo.tAppointmentSchedule
    WHERE ID = @ID_CurrentObject;

    EXEC pComputeScheduleTotalAccmodation @IDs_Schedule;
END;

GO
