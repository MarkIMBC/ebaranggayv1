﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[_pGetTableReference] 
AS
SELECT  OBJECT_NAME(fk.referenced_object_id), t.name, OBJECT_NAME(o.object_id), c.name
FROM    sys.foreign_key_columns AS fk
        INNER JOIN sys.tables AS t ON fk.parent_object_id = t.object_id
        INNER JOIN sys.columns AS c ON fk.parent_object_id = c.object_id
                                       AND fk.parent_column_id = c.column_id
        INNER JOIN sys.objects AS o ON fk.constraint_object_id = o.object_id
		LEFT JOIN dbo.[_tModel] m ON UPPER(m.TableName) = UPPER(t.name)
WHERE   fk.referenced_object_id = ( SELECT  object_id
                                    FROM    sys.tables
                                    WHERE   name = '_tModel'
                                  ) 
									AND	m.Oid IS NOT NULL
GO
