﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[_pDelete_DetailView_Detail] @Oid UNIQUEIDENTIFIER
AS
  BEGIN
    UPDATE _tDetailView_Detail SET ID_Tab = NULL WHERE ID_Tab = @Oid
    UPDATE _tDetailView_Detail SET ID_Section = NULL WHERE ID_Section = @Oid

    DELETE FROM _tDetailView_Detail WHERE Oid = @Oid AND ID_ModelProperty IS NULL
  END
GO
