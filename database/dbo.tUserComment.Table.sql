﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tUserComment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_Model] [uniqueidentifier] NULL,
	[ID_CurrentObject] [int] NULL,
 CONSTRAINT [PK_tUserComment] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tUserComment] ADD  CONSTRAINT [DF__tUserComm__IsAct__1387E197]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tUserComment]  WITH NOCHECK ADD  CONSTRAINT [FK_tUserComment_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tUserComment] CHECK CONSTRAINT [FK_tUserComment_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tUserComment]  WITH NOCHECK ADD  CONSTRAINT [FK_tUserComment_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tUserComment] CHECK CONSTRAINT [FK_tUserComment_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tUserComment]
		ON [dbo].[tUserComment]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tUserComment
			SET    DateCreated = GETDATE()
			FROM   dbo.tUserComment hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tUserComment] ENABLE TRIGGER [rDateCreated_tUserComment]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tUserComment]
		ON [dbo].[tUserComment]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tUserComment
			SET    DateModified = GETDATE()
			FROM   dbo.tUserComment hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tUserComment] ENABLE TRIGGER [rDateModified_tUserComment]
GO
