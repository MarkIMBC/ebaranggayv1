﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[__vNavigation]
AS
    SELECT  N.Oid ,
			N.Name,
			N.ID_Parent,
			V.Oid AS ID_View,
            V.Name AS [View] ,
			ISNULL(N.Caption,ISNULL(M.Caption,M.Name)) AS [Text],
			ISNULL(N.Icon,'fa fa-cube') AS [icon],
			N.IsActive,
            V.ID_ViewType,
			V.ID_Model ,
			V.ID_ListView,
		--.ID_DetailView,
			V.ID_Report,
			M.Name AS Model,
			ISNULL(N.SeqNo,1000) AS SeqNo,
			N.Route
    FROM    dbo.[_tNavigation] N
            LEFT JOIN dbo.[_tView] V ON N.ID_View = V.Oid
            LEFT JOIN dbo.[_tViewType] VT ON V.ID_ViewType = VT.ID
            LEFT JOIN dbo.[_tModel] M ON V.ID_Model = M.Oid
	WHERE n.IsActive = 1
GO
