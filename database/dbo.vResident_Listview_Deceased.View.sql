﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vResident_Listview_Deceased]
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         hed.IsDeceased,
         company.Guid Guid_Company
  FROM   vResident hed
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  Where  hed.IsActive = 1
         and hed.IsDeceased = 1

GO
