﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create 
 PROC [dbo].[pAddFilingStatus](@FilingStatus Varchar(200))
as
    INSERT INTO [dbo].[tFilingStatus]
                ([Code],
                 [Name],
                 [IsActive],
                 [ID_Company],
                 [Comment],
                 [DateCreated],
                 [DateModified],
                 [ID_CreatedBy],
                 [ID_LastModifiedBy])
    VALUES      (NULL,
                 @FilingStatus,
                 1,
                 1,
                 Null,
                 GETDATE(),
                 GETDATE(),
                 1,
                 1 )

GO
