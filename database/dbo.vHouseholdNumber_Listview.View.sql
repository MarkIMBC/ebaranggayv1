﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE    
 VIEW [dbo].[vHouseholdNumber_Listview]
as
  Select household.ID,
         household.Name,
         household.ID_Company,
         ISNULL(summary.Count, 0) Count,
         DateCreated,
         AssignedBHW_Name_Employee
  FROM   vHouseholdNumber household
         lEFT join vHouseholdNumberResidentHeadOfFamilyCount summary
                on household.ID = summary.ID
  WHERE  household.IsActive = 1

GO
