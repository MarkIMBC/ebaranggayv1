﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vPatient_Vaccination_Listview]  
AS  
  SELECT vaccination.ID,  
         vaccination.Date,  
         vaccination.Code,  
         ID_Client,  
         Name_Client,  
         ID_Patient,  
         Name_Patient,  
         ID_Item,  
         Name_Item,  
         ID_FilingStatus,  
         Name_FilingStatus,  
         DateExpiration,  
         Comment,  
         ID_Company,  
   ID_Patient_SOAP  
  FROM   vPatient_Vaccination vaccination   
  WHERE ID_FIlingSTATUS NOT IN (4)
GO
