﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 FUNCTION [dbo].[fGetVeterinaryHealthCertificateVaccinationOptionComment](@VaccinationOptionComment VARCHAR(MAX),
                                                                               @DateVaccinated           DateTime,
                                                                               @Name_Item                VARCHAR(MAX),
                                                                               @LotNumber                VARCHAR(MAX))
RETURNS VARCHAR(MAX)
AS
  BEGIN
      SET @VaccinationOptionComment = REPLACE(@VaccinationOptionComment, '/*****Date*****/', '<u>'
                                                                                             + IsNULL(FORMAT(@DateVaccinated, 'MM/dd/yyyy'), '')
                                                                                             + '</u>')
      SET @VaccinationOptionComment = REPLACE(@VaccinationOptionComment, '/*****Vaccination*****/', '<u>' + IsNULL(@Name_Item, '') + '</u>')
      SET @VaccinationOptionComment = REPLACE(@VaccinationOptionComment, '/*****Serial/Lot Number*****/', '<u>' + IsNULL(@LotNumber, '') + '</u>')

      RETURN @VaccinationOptionComment
  END

GO
