﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SqlQueryNotificationStoredProcedure-8de5424d-0244-4de4-8ce2-715d26b9e949] AS BEGIN BEGIN TRANSACTION; RECEIVE TOP(0) conversation_handle FROM [SqlQueryNotificationService-8de5424d-0244-4de4-8ce2-715d26b9e949]; IF (SELECT COUNT(*) FROM [SqlQueryNotificationService-8de5424d-0244-4de4-8ce2-715d26b9e949] WHERE message_type_name = 'http://schemas.microsoft.com/SQL/ServiceBroker/DialogTimer') > 0 BEGIN if ((SELECT COUNT(*) FROM sys.services WHERE name = 'SqlQueryNotificationService-8de5424d-0244-4de4-8ce2-715d26b9e949') > 0)   DROP SERVICE [SqlQueryNotificationService-8de5424d-0244-4de4-8ce2-715d26b9e949]; if (OBJECT_ID('SqlQueryNotificationService-8de5424d-0244-4de4-8ce2-715d26b9e949', 'SQ') IS NOT NULL)   DROP QUEUE [SqlQueryNotificationService-8de5424d-0244-4de4-8ce2-715d26b9e949]; DROP PROCEDURE [SqlQueryNotificationStoredProcedure-8de5424d-0244-4de4-8ce2-715d26b9e949]; END COMMIT TRANSACTION; END
GO
