﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vResidentRequest_Certificate_ListvIew]
AS
  SELECT ID,
         Name,
         DateBirth,
         ID_Gender,
         ID_Company,
         IsActive,
         ID_ResidentRequest,
         Name_ResidentRequest,
         DateCreated,
         DateModified
  FROM   vResidentRequest_Certificate
  WHERE  IsActive = 1

GO
