﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vSMSCountPerCompany]
as
  SELECT DateSchedule,
         ID_Company,
         Count(*) TotalSMSCount
  FROM   vSentSMSRecord
  GROUP  BY ID_Company,
            DateSchedule

GO
