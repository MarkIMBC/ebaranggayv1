﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE        
 PROC [dbo].[pInsertProgram](@Program VARCHAR(MAX), @ID_Company INT = 0)    
as    
  BEGIN    
      IF(SELECT COUNT(*)    
         FROM   tProgram    
         WHERE  Name = @Program) = 0    
        BEGIN    
            INSERT INTO [dbo].tProgram    
                        ([Name],    
                         [IsActive], ID_Company)    
            VALUES      (@Program,    
                         1, @ID_Company)    
        END    
  END 
GO
