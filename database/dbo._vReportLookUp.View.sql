﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_vReportLookUp]
AS

SELECT Oid,
       Name,
       Oid ID
FROM dbo._tReport;

GO
