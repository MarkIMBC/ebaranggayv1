﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fGetInventoryStatus] (@Count int, @MinCount int, @MaxCount int)
RETURNS int	 AS
BEGIN
	Declare @ID_InventoryStatus_NoStock int = 1
	Declare @ID_InventoryStatus_LOW int = 2
	Declare @ID_InventoryStatus_Medium int = 3
	Declare @ID_InventoryStatus_High int = 4


	Declare @ID_InventoryStatus int = 0

	set @Count  = ISNULL(@Count, 0)
	set @MinCount  = ISNULL(@MinCount, 0)
	set @MaxCount  = ISNULL(@MaxCount, 0)


	if(@Count = 0)
		set @ID_InventoryStatus = @ID_InventoryStatus_NoStock
	else if(@Count BETWEEN @MinCount AND @MaxCount)
		set @ID_InventoryStatus = @ID_InventoryStatus_Medium
	else if(@Count > @MaxCount)
		set @ID_InventoryStatus = @ID_InventoryStatus_High
	else if(@Count < @MinCount)
		set @ID_InventoryStatus = @ID_InventoryStatus_LOW


    RETURN @ID_InventoryStatus
END

GO
