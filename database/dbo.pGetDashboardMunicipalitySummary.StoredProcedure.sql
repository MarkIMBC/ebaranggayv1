﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE 
 PROC [dbo].[pGetDashboardMunicipalitySummary] @ID_UserSession INT = NULL
as
  BEGIN
      DECLARE @ID_User    INT,
              @ID_Company INT
      DECLARE @ResidentCount INT = 0
      DECLARE @VoterCount INT = 0
      DECLARE @PermanentResidentCount INT = 0
      DECLARE @MigrateResidentCount INT = 0
      DECLARE @TrancientResidentCount INT = 0
      DECLARE @HomeOwnershipStatusOwnerCount INT = 0
      DECLARE @EducationalLevelGraduatedCount INT = 0
      DECLARE @TotalResidentMaleCount INT = 0
      DECLARE @TotalResidentFemaleCount INT = 0
      DECLARE @ResidentCountAge60Above INT = 0
      DECLARE @ResidentCountAge18to59 INT = 0
      DECLARE @ResidentCountAge10to17 INT = 0
      DECLARE @ResidentCountAge5to9 INT = 0
      DECLARE @ResidentCountAge24to84months INT = 0
      DECLARE @ResidentCountAge12to23months INT = 0
      DECLARE @ResidentCountAge0to11months INT = 0
      DECLARE @ResidentCountMale INT = 0
      DECLARE @ResidentCountFemale INT = 0
      DECLARE @ResidentCount4Ps INT = 0
      DECLARE @ResidentCountSeniorCitizen INT = 0
      DECLARE @ResidentCountScholar INT = 0
      DECLARE @MayAriNgBahayAtLupaID_HomeOwnershipStatus INT = 1
      DECLARE @GraduateSchool_ID_EducationalLevel INT = 6
      DECLARE @Male_ID_Gender INT = 1
      DECLARE @Female_ID_Gender INT = 2

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      SELECT @ResidentCount = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1

    SELECT @VoterCount = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1
			 and resident.IsVoter = 1

      SELECT @PermanentResidentCount = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1
             AND IsPermanent = 1

      SELECT @MigrateResidentCount = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1
             AND IsMigrante = 1

      SELECT @TrancientResidentCount = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1
             AND IsTransient = 1

      SELECT @HomeOwnershipStatusOwnerCount = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1
             ANd ID_HomeOwnershipStatus = @MayAriNgBahayAtLupaID_HomeOwnershipStatus

      SELECT @EducationalLevelGraduatedCount = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1
             ANd ID_EducationalLevel = @GraduateSchool_ID_EducationalLevel

      SELECT @ResidentCountMale = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1
             ANd ID_Gender = @Male_ID_Gender

      SELECT @ResidentCountFemale = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1
             ANd ID_Gender = @Female_ID_Gender

      SELECT @ResidentCountAge60Above = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) >= 60

      SELECT @ResidentCountAge18to59 = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) BETWEEN 18 AND 59

      SELECT @ResidentCountAge10to17 = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) BETWEEN 10 AND 17

      SELECT @ResidentCountAge5to9 = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1
             and DateBirth IS NOT NULL
             and DATEDIFF(year, DateBirth, GETDATE()) BETWEEN 5 AND 9

      SELECT @ResidentCountAge24to84months = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  ID_Company > 1
             and resident.IsActive = 1
             and DateBirth IS NOT NULL
             AND DATEDIFF(MONTH, DateBirth, GETDATE()) BETWEEN 24 AND 84

      SELECT @ResidentCountAge12to23months = COUNT(*)
      FROm   tResident
      where  ID_Company = @ID_Company
             and IsActive = 1
             and DateBirth IS NOT NULL
             AND DATEDIFF(MONTH, DateBirth, GETDATE()) BETWEEN 12 AND 23

      SELECT @ResidentCountAge0to11months = COUNT(*)
      FROM   tResident resident
             inner join vCompanyActive company
                     on resident.ID_Company = company.ID
      where  resident.ID_Company > 1
             and resident.IsActive = 1
             and DateBirth IS NOT NULL
             AND DATEDIFF(MONTH, DateBirth, GETDATE()) BETWEEN 0 AND 11

      SELECT @ResidentCount4Ps = COUNT(*)
      FROM   (SELECT DISTINCT resident.ID
              FROM   tResident resident
                     inner join tResident_Program residentprogram
                             on resident.ID = residentprogram.ID_Resident
                     Inner join tProgram program
                             on residentprogram.ID_Program = program.ID
                     inner join vCompanyActive company
                             on resident.ID_Company = company.ID
              where  resident.ID_Company > 1
                     and resident.IsActive = 1
                     AND program.Name IN ( '4Ps' )
              GROUP  BY resident.ID) tbl

      SELECT @ResidentCountSeniorCitizen = COUNT(*)
      FROM   (SELECT DISTINCT resident.ID
              FROM   tResident resident
                     inner join tResident_Program residentprogram
                             on resident.ID = residentprogram.ID_Resident
                     Inner join tProgram program
                             on residentprogram.ID_Program = program.ID
                     inner join vCompanyActive company
                             on resident.ID_Company = company.ID
              where  resident.ID_Company = @ID_Company
                     and resident.IsActive > 1
                     AND program.Name IN ( 'Senior Citizen' )
              GROUP  BY resident.ID) tbl

      SELECT @ResidentCountScholar = COUNT(*)
      FROM   (SELECT DISTINCT resident.ID
              FROM   tResident resident
                     inner join tResident_Program residentprogram
                             on resident.ID = residentprogram.ID_Resident
                     Inner join tProgram program
                             on residentprogram.ID_Program = program.ID
                     inner join vCompanyActive company
                             on resident.ID_Company = company.ID
              where  resident.ID_Company > 1
                     and resident.IsActive = 1
                     AND program.Name IN ( 'Scholarship' )
              GROUP  BY resident.ID) tbl

      SELECT '_'

      SELECT ISNULL(@ResidentCount, 0)                  ResidentCount,
	  ISNULL(@VoterCount, 0)                  VoterCount,
             ISNULL(@PermanentResidentCount, 0)         PermanentResidentCount,
             ISNULL(@MigrateResidentCount, 0)           MigrateResidentCount,
             ISNULL(@TrancientResidentCount, 0)         TrancientResidentCount,
             ISNULL(@HomeOwnershipStatusOwnerCount, 0)  HomeOwnershipStatusOwnerCount,
             ISNULL(@EducationalLevelGraduatedCount, 0) EducationalLevelGraduatedCount,
             ISNULL(@ResidentCountAge60Above, 0)        ResidentCountAge60Above,
             ISNULL(@ResidentCountAge18to59, 0)         ResidentCountAge18to59,
             ISNULL(@ResidentCountAge10to17, 0)         ResidentCountAge10to17,
             ISNULL(@ResidentCountAge5to9, 0)           ResidentCountAge5to9,
             ISNULL(@ResidentCountAge24to84months, 0)   ResidentCountAge24to84months,
             ISNULL(@ResidentCountAge12to23months, 0)   ResidentCountAge12to23months,
             ISNULL(@ResidentCountAge0to11months, 0)    ResidentCountAge0to11months,
             ISNULL(@ResidentCountMale, 0)              ResidentCountMale,
             ISNULL(@ResidentCountFemale, 0)            ResidentCountFemale,
             ISNULL(@ResidentCount4Ps, 0)               ResidentCount4Ps,
             ISNULL(@ResidentCountSeniorCitizen, 0)     ResidentCountSeniorCitizen,
             ISNULL(@ResidentCountScholar, 0)           ResidentCountScholar
  END 
GO
