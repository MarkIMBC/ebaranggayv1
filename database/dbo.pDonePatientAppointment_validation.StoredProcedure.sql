﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROC [dbo].[pDonePatientAppointment_validation]
(
    @IDs_PatientAppointment typIntList READONLY,
    @ID_UserSession INT
)
AS
BEGIN

    DECLARE @Filed_ID_FilingStatus INT = 1;
    DECLARE @message VARCHAR(400) = '';

    DECLARE @ValidateNotFiled TABLE
    (
        Code VARCHAR(30),
        Name_FilingStatus VARCHAR(30)
    );
    DECLARE @Count_ValidateNotFiled INT = 0;


    /* Validate Billing Invoices Status is not Filed*/
    INSERT @ValidateNotFiled
    (
        Code,
        Name_FilingStatus
    )
    SELECT Code,
           Name_FilingStatus
    FROM dbo.vPatientAppointment bi
    WHERE EXISTS
    (
        SELECT ID FROM @IDs_PatientAppointment ids WHERE ids.ID = bi.ID
    )
          AND bi.ID_FilingStatus NOT IN ( @Filed_ID_FilingStatus );

    SELECT @Count_ValidateNotFiled = COUNT(*)
    FROM @ValidateNotFiled;

    IF (@Count_ValidateNotFiled > 0)
    BEGIN

        SET @message = 'The following record' + CASE
                                                    WHEN @Count_ValidateNotFiled > 1 THEN
                                                        's are'
                                                    ELSE
                                                        ' is '
                                                END + 'not allowed to Doned:';

        SELECT @message = @message + CHAR(10) + Code + ' - ' + Name_FilingStatus
        FROM @ValidateNotFiled;
        THROW 50001, @message, 1;

    END;

END;


GO
