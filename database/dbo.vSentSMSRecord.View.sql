﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vSentSMSRecord]
as
  SELECT soap.Code,
         CONVERT(Date, DateSent)                     DateSent,
         CONVERT(Date, DateReturn)                   DateSchedule,
         CONVERT(Date, DATEADD(DAY, -1, DateReturn)) DateSending,
         ID_Company
  FROM   tPatient_SOAP_Plan soapPlan
         INNER JOIN tPatient_SOAP soap
                 ON soapPlan.ID_Patient_SOAP = soap.ID
  WHERE  DateSent IS NOT NULL
  UNION ALL
  SELECT vac.Code,
         CONVERT(Date, DateSent)                        DateSent,
         vacSched.Date                                  DateSchedule,
         CONVERT(Date, DATEADD(DAY, -1, vacSched.Date)) DateSending,
         vac.ID_Company
  FROM   tPatient_Vaccination_Schedule vacSched
         INNER JOIN tPatient_Vaccination vac
                 ON vacSched.ID_Patient_Vaccination = vac.ID
  WHERE  DateSent IS NOT NULL
  UNION ALL
  SELECT well.Code,
         CONVERT(Date, DateSent)                                                   DateSent,
         wellSched.Date_Patient_Wellness_Schedule                                  DateSchedule,
         CONVERT(Date, DATEADD(DAY, -1, wellSched.Date_Patient_Wellness_Schedule)) DateSending,
         well.ID_Company
  FROM   vPatient_Wellness_DetailSchedule wellSched
         INNER JOIN tPatient_Wellness well
                 ON wellSched.ID_Patient_Wellness = well.ID
  WHERE  DateSent IS NOT NULL

GO
