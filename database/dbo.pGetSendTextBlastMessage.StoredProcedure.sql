﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pGetSendTextBlastMessage](@DateSending DateTime)
as
  BEGIN
      DECLARE @Success bit = 1

      SELECT '_',
             '' AS records;

      SELECT @Success Success;

      SELECT tbBlast.Date                                                       DateSending,
             company.ID                                                         ID_Company,
             company.Name                                                       Name_Company,
             tbBlastClient.ID_TextBlast,
             tbBlastClient.ID                                                   ID_TextBlast_Client,
             tbBlast.Code                                                       Code_TextBlast,
             client.Name                                                        Name_Client,
             dbo.[fGetTextBlastMessage](tbBlast.Message, client.Name)           TextBlastMessage,
             dbo.fGetMobileNumbers(client.ContactNumber, client.ContactNumber2) ContactNumber
      FROM   tTextBlast_Client tbBlastClient
             inner join tTextBlast tbBlast
                     ON tbBlast.ID = tbBlastClient.ID_TextBlast
             inner join tCompany company
                     ON company.ID = tbBlast.ID_Company
             inner join tClient client
                     ON tbBlastClient.ID_Client = client.ID
      WHERE  ID_FilingStatus = 3
             AND company.ID <> 1
             and ISNULL(tbBlastClient.IsSent, 0) = 0
             AND Convert(Date, tbBlast.Date) = Convert(Date, @DateSending)
      Order  By company.Name ASC,
                client.Name ASC
  END 
GO
