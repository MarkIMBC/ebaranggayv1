﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vHouseholdNumberrResidentList_Listview]
as
  Select household.ID,
         household.Name,
         resident.ID_Company,
         COUNT(*) Count
  FROM   tHouseholdNumber household
         inner join vResident_Listview_HeadOfFamily resident
                 on household.ID = resident.ID_HouseholdNumber
  WHERE  resident.IsActive = 1
  GROUP  BY household.ID,
            household.Name,
            resident.ID_Company

GO
