﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   VIEW [dbo].[vzClientDepositReport]
AS
  SELECT cd.ID,
               cd.Code,
               cd.Date,
               cd.CreatedBy,
               cd.ApprovedBy_Name_User,
               cd.Name_FilingStatus,
               cd.Name_Client,
               cd.Name_Patient,
               cd.DepositAmount,
               FORMAT(cd.DepositAmount, '#,#0.00') FormattedDepositAmount,
               cd.Code_Patient_Confinement,
               company.ID                          ID_Company,
               company.ImageLogoLocationFilenamePath,
               company.Name                        Name_Company,
               company.Address                     Address_Company,
               company.ContactNumber               ContactNumber_Company,
               CASE
                 WHEN LEN(company.Address) > 0 THEN '' + company.Address
                 ELSE ''
               END
               + CASE
                   WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
                   ELSE ''
                 END
               + CASE
                   WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
                   ELSE ''
                 END                               HeaderInfo_Company
  FROM   dbo.vClientDeposit cd
         LEFT JOIN dbo.vCompany company
                ON company.ID = cd.ID_Company
  where  Name_Client is NOT NULL

GO
