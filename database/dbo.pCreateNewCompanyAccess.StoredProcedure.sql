﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pCreateNewCompanyAccess] (@CompanyName VARCHAR(MAX),
                                             @code        VARCHAR(MAX))
AS
  BEGIN
      DECLARE @ID_Company INT = 0;
      DECLARE @Admin_ID_UserRole INT = 5;
      DECLARE @Sole_ID_UserRole INT = 11;
      DECLARE @Standard_ID_UserRole INT = 12;
      DECLARE @Receptionportal_ID_UserRole INT = 47;
      DECLARE @Default_IDs_Employee TYPINTLIST;
	  
	  SET @CompanyName = LTRIM(RTRIM(@CompanyName))
	  SET @code = TRIM(@code)

      EXEC dbo.pCreateNewCompanyAccess_validation
        @CompanyName,
        @code;

      INSERT @Default_IDs_Employee
             (ID)
      VALUES (1 ),
             (13),
             (14),
             (923);

      INSERT dbo.tCompany
             (Code,
              Name,
              IsActive,
              Comment,
              DateCreated,
              DateModified,
              ID_CreatedBy,
              ID_LastModifiedBy,
              ID_Country,
              Address,
              SOAPPlanSMSMessage,
              IsRemoveBoldText,
              SecurityPIN)
      VALUES ( @code,-- Code - varchar(50)        
               @CompanyName,-- Name - varchar(200)        
               1,-- IsActive - bit        
               '',-- Comment - varchar(max)        
               GetDate(),-- DateCreated - datetime        
               GetDate(),-- DateModified - datetime        
               1,-- ID_CreatedBy - int        
               1,-- ID_LastModifiedBy - int        
               1,-- ID_Country - int        
               '',-- Address - varchar(300)        
               'Hi /*Client*/, /*Pet*/ has an appointment for /*Service*/ on /*DateReturn*/.    Pls. contact /*CompanyName*/ /*ContactNumber*/',
               1,
               LEFT(NewId(), 4) );

      SET @ID_Company = @@IDENTITY;

      --exec dbo.pInsertInitialRecord_ItemServices @ID_Company        
      INSERT dbo.tEmployee
             (Code,
              IsActive,
              ID_CreatedBy,
              ID_LastModifiedBy,
              DateCreated,
              DateModified,
              ImageFile,
              ID_Department,
              ID_EmployeePosition,
              ID_Position,
              LastName,
              FirstName,
              MiddleName,
              ID_Gender,
              ID_EmployeeStatus,
              FullAddress,
              Email,
              ContactNumber,
              Name,
              ID_Company,
              IsSystemUsed)
      SELECT Code,
             IsActive,
             ID_CreatedBy,
             ID_LastModifiedBy,
             DateCreated,
             DateModified,
             ImageFile,
             ID_Department,
             ID_EmployeePosition,
             ID_Position,
             LastName,
             FirstName,
             MiddleName,
             ID_Gender,
             ID_EmployeeStatus,
             FullAddress,
             Email,
             ContactNumber,
             Name,
             @ID_Company,
             CASE
               WHEN ids.ID = 1 THEN 1
               ELSE 0
             END
      FROM   dbo.tEmployee
             INNER JOIN @Default_IDs_Employee ids
                     ON ids.ID = tEmployee.ID;

      INSERT dbo.tUser
             (Code,
              Name,
              IsActive,
              Comment,
              DateCreated,
              DateModified,
              ID_CreatedBy,
              ID_LastModifiedBy,
              ID_Employee,
              Username,
              ID_UserGroup,
              Password,
              IsRequiredPasswordChangedOnLogin,
              ID_Patient)
      SELECT '',
             emp.Name,
             1,
             '',
             GetDate(),
             GetDate(),
             1,
             1,
             emp.ID,
             Lower(@code) + '-' + Lower(emp.LastName),
             1,
             LEFT(NewId(), 4),
             1,
             NULL
      FROM   dbo.tEmployee emp
      WHERE  emp.ID_Company = @ID_Company;

      INSERT dbo.tUser_Roles
             (Code,
              Name,
              IsActive,
              Comment,
              ID_User,
              ID_UserRole,
              SeqNo)
      SELECT '',
             userRole.Name,
             1,
             '',
             _user.ID,
             userRole.ID,
             1
      FROM   dbo.tUser _user
             INNER JOIN dbo.tEmployee emp
                     ON emp.ID = _user.ID_Employee
             INNER JOIN dbo.tUserRole userRole
                     ON Lower(userRole.Name) = Lower(emp.LastName)
      WHERE  emp.ID_Company = @ID_Company;

      INSERT dbo.tDocumentSeries
             (Code,
              Name,
              IsActive,
              Comment,
              DateCreated,
              DateModified,
              ID_CreatedBy,
              ID_LastModifiedBy,
              ID_Model,
              Counter,
              Prefix,
              IsAppendCurrentDate,
              DigitCount,
              ID_Company)
      SELECT Code,
             Name,
             IsActive,
             Comment,
             DateCreated,
             DateModified,
             ID_CreatedBy,
             ID_LastModifiedBy,
             ID_Model,
             1,
             Prefix,
             IsAppendCurrentDate,
             5,
             @ID_Company
      FROM   dbo.tDocumentSeries
      WHERE  ID_Company = 1;
  END; 

GO
