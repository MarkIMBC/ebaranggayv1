﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vResidentRequest_IndigencyCertificate]
AS
  SELECT H.*,
         ResidentRequest.Name          Name_ResidentRequest,
         ResidentRequest.ContactNumber ContactNumber_ResidentRequest,
         gender.NAME                   Name_Gender,
         UC.NAME                       AS CreatedBy,
         UM.NAME                       AS LastModifiedBy
  FROM   tResidentRequest_IndigencyCertificate H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tGender gender
                ON gender.ID = h.ID_Gender
         LEFT JOIN tResidentRequest ResidentRequest
                ON ResidentRequest.ID = h.ID_ResidentRequest

GO
