﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPatient_Vaccination](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_Company] [int] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[ID_Client] [int] NULL,
	[ID_Patient] [int] NULL,
	[Date] [datetime] NULL,
	[Temparature] [varchar](300) NULL,
	[HeartRate] [varchar](300) NULL,
	[Weight] [varchar](300) NULL,
	[ID_Item] [int] NULL,
	[ID_FilingStatus] [int] NULL,
	[AttendingPhysician] [varchar](300) NULL,
	[DateExpiration] [datetime] NULL,
	[DateCanceled] [datetime] NULL,
	[ID_CanceledBy] [int] NULL,
	[UnitCost] [decimal](18, 4) NULL,
	[UnitPrice] [decimal](18, 4) NULL,
	[AttendingPhysician_ID_Employee] [int] NULL,
	[ID_Patient_SOAP] [int] NULL,
	[Appointment_ID_FilingStatus] [int] NULL,
	[Appointment_CancellationRemarks] [varchar](300) NULL,
 CONSTRAINT [PK_tPatient_Vaccination] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_tPatient_Vaccination] ON [dbo].[tPatient_Vaccination]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tPatient_Vaccination] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tPatient_Vaccination] ADD  DEFAULT ((2)) FOR [Appointment_ID_FilingStatus]
GO
ALTER TABLE [dbo].[tPatient_Vaccination]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Vaccination_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Vaccination] CHECK CONSTRAINT [FK_tPatient_Vaccination_ID_Company]
GO
ALTER TABLE [dbo].[tPatient_Vaccination]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Vaccination_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Vaccination] CHECK CONSTRAINT [FK_tPatient_Vaccination_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tPatient_Vaccination]  WITH CHECK ADD  CONSTRAINT [FK_tPatient_Vaccination_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tPatient_Vaccination] CHECK CONSTRAINT [FK_tPatient_Vaccination_ID_LastModifiedBy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateCreated_tPatient_Vaccination]
		ON [dbo].[tPatient_Vaccination]
		FOR INSERT
		AS
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPatient_Vaccination
			SET    DateCreated = GETDATE()
			FROM   dbo.tPatient_Vaccination hed
				   INNER JOIN @IDs ids
						   on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPatient_Vaccination] ENABLE TRIGGER [rDateCreated_tPatient_Vaccination]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		CREATE   TRIGGER [dbo].[rDateModified_tPatient_Vaccination]
		ON [dbo].[tPatient_Vaccination]
		FOR UPDATE, INSERT
		AS
			DECLARE @ID INT
			DECLARE @IDs typIntList

			INSERT @IDs
			SELECT ID
			FROM   Inserted

			UPDATE dbo.tPatient_Vaccination
			SET    DateModified = GETDATE()
			FROM   dbo.tPatient_Vaccination hed
					INNER JOIN @IDs ids
							on hed.ID = IDs.ID;
GO
ALTER TABLE [dbo].[tPatient_Vaccination] ENABLE TRIGGER [rDateModified_tPatient_Vaccination]
GO
