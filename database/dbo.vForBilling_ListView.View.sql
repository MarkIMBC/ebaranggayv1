﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE   
 View [dbo].[vForBilling_ListView]  
as  
  Select confinement.ID,  
         confinement.Code RefNo,  
         confinement.ID   ID_CurrentObject,  
         m.Oid            Oid_Model,  
         confinement.Date,  
         confinement.ID_Company,  
         confinement.ID_Client,  
         confinement.ID_Patient,  
         confinement.BillingInvoice_ID_FilingStatus,  
         confinement.Name_Client,  
         confinement.Name_Patient,  
         confinement.BillingInvoice_Name_FilingStatus,  
         billConfinement.[1],  
         billConfinement.[2],  
         billConfinement.[3],  
         billConfinement.[4],  
         billConfinement.[5],  
         billConfinement.[6],  
         billConfinement.[7],  
         billConfinement.[8],  
         billConfinement.[9],  
         billConfinement.[10]  
  FROM   vPatient_Confinement confinement  
         LEFT JOIN vpivotBilledPatientConfinement billConfinement  
                 ON confinement.ID = billConfinement.ID_Patient_Confinement,  
         _tModel m  
  where  m.TableName = 'tPatient_Confinement'  
         AND ID_FilingStatus NOT IN ( 4 )  
         and BillingInvoice_ID_FilingStatus IN ( 16, 1, 3, 11, 2 )  
  UNION ALL  
  Select soap.ID,  
         soap.Code RefNo,  
         soap.ID   ID_CurrentObject,  
         m.Oid     Oid_Model,  
         soap. Date,  
         soap.ID_Company,  
         soap.ID_Client,  
         soap.ID_Patient,  
         soap.BillingInvoice_ID_FilingStatus,  
         soap.Name_Client,  
         soap.Name_Patient,  
         soap.BillingInvoice_Name_FilingStatus,  
         billSOAP.[1],  
         billSOAP.[2],  
         billSOAP.[3],  
         billSOAP.[4],  
         billSOAP.[5],  
         billSOAP.[6],  
         billSOAP.[7],  
         billSOAP.[8],  
         billSOAP.[9],  
         billSOAP.[10]  
  FROM   vPatient_SOAP soap  
         LEFT JOIN vpivotBilledPatientSOAP billSOAP  
                 ON soap.ID = billSOAP.ID_Patient_SOAP,  
         _tModel m  
  where  m.TableName = 'tPatient_SOAP'  
         AND ID_FilingStatus NOT IN ( 4 )  
         and BillingInvoice_ID_FilingStatus IN ( 16, 1, 3, 11, 2 )  
		 and ISNULL(soap.ID_Patient_Confinement,'')  = 0
GO
