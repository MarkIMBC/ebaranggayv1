﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pModel_AfterSaved_PurchaseOrder] (@ID_CurrentObject VARCHAR(10),
                                                @IsNew            BIT = 0)
AS
  BEGIN
      IF @IsNew = 1
        BEGIN
            /* Generate Document Series */
            DECLARE @Oid_Model UNIQUEIDENTIFIER;
            DECLARE @Code VARCHAR(MAX) = '';
            DECLARE @ID_Company INT = 0;

            SELECT @ID_Company = ID_Company
            FROM   dbo.tPurchaseOrder
            WHERE  ID = @ID_CurrentObject;

            SELECT @Oid_Model = m.Oid
            FROM   dbo._tModel m
            WHERE  Name = 'PurchaseOrder';

            SET @Code = dbo.fGenerateDocumentSeries(@Oid_Model, @ID_Company, 0, NULL);

            SELECT @Code,
                   @ID_Company,
                   @Oid_Model;

            UPDATE dbo.tDocumentSeries
            SET    Counter = Counter + 1
            WHERE  ID_Model = @Oid_Model
                   AND ID_Company = @ID_Company;

            UPDATE dbo.tPurchaseOrder
            SET    Code = @Code
            WHERE  ID = @ID_CurrentObject;
        END;
  END;

GO
