﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE PROC [dbo].[pCancelReceivingReport_validation]  
(  
    @IDs_ReceivingReport typIntList READONLY,  
    @ID_UserSession INT  
)  
AS  
BEGIN  
  
    DECLARE @Filed_ID_FilingStatus INT = 1;  
    DECLARE @Approved_ID_FilingStatus INT = 3;  
    DECLARE @message VARCHAR(400) = '';  
  
    DECLARE @ValidateNotApproved TABLE  
    (  
        Code VARCHAR(30),  
        Name_FilingStatus VARCHAR(30)  
    );  
    DECLARE @Count_ValidateNotApproved INT = 0;  
  
    /* Validate Billing Invoices Status is not Approved*/  
    INSERT @ValidateNotApproved  
    (  
        Code,  
        Name_FilingStatus  
    )  
    SELECT Code,  
           Name_FilingStatus  
    FROM dbo.vReceivingReport bi  
    WHERE EXISTS  
    (  
        SELECT ID FROM @IDs_ReceivingReport ids WHERE ids.ID = bi.ID  
    )  
          AND bi.ID_FilingStatus NOT IN ( @Approved_ID_FilingStatus, @Filed_ID_FilingStatus );  
  
    SELECT @Count_ValidateNotApproved = COUNT(*)  
    FROM @ValidateNotApproved;  
  
    IF (@Count_ValidateNotApproved > 0)  
    BEGIN  
  
        SET @message = 'The following record' + CASE  
                                                    WHEN @Count_ValidateNotApproved > 1 THEN  
                                                        's are'  
                                                    ELSE  
                                                        ' is '  
                                                END + 'not allowed to cancel:';  
  
        SELECT @message = @message + CHAR(10) + Code + ' - ' + Name_FilingStatus  
        FROM @ValidateNotApproved;  
        THROW 50001, @message, 1;  
    END; 
	
	/* Validate Remaining Inventory */
	DECLARE @Inventoriable_ID_ItemType INT = 2;   
	DECLARE @ValidateItemInventory TABLE (
									ID_Item INT, 
									ItemName VARCHAR(MAX),
									RRQty INT,
									RemQty INT
								 )
	Declare @Count_ValidateItemInventory INT = 0

	INSERT @ValidateItemInventory
	(
		ID_Item,
		ItemName,
		RRQty,
		RemQty
	)
	SELECT 
		rrDetail.ID_Item, 
		item.Name,
		SUM(rrDetail.Quantity), 
		0
	FROM dbo.tReceivingReport_Detail rrDetail
	INNER JOIN dbo.tReceivingReport rrHed
		ON rrDetail.ID_ReceivingReport = rrHed.ID
	INNER JOIN dbo.tItem item
		ON item.ID = rrDetail.ID_Item
	INNER JOIN @IDs_ReceivingReport ids
		ON ids.ID = rrHed.ID
	WHERE 
		item.ID_ItemType = @Inventoriable_ID_ItemType
	GROUP BY
		rrDetail.ID_Item,
		item.Name

	UPDATE @ValidateItemInventory 
		SET RemQty = tbl.TotalRemQty
	FROM @ValidateItemInventory invtItem
		INNER JOIN 
	(
		SELECT 
			ID_Item, 
			SUM(Quantity) TotalRemQty
		FROM tInventoryTrail
		GROUP BY ID_Item
	) tbl ON invtItem.ID_Item = tbl.ID_Item

	DELETE FROM @ValidateItemInventory WHERE (ISNULL(RemQty, 0) - ISNULL(RRQty, 0) ) >= 0

	SELECT @Count_ValidateItemInventory = COUNT(*) FROM @ValidateItemInventory 

		IF (@Count_ValidateItemInventory > 0)  
		BEGIN  
  
			SET @message = 'The following item' + CASE  
														WHEN @Count_ValidateItemInventory > 1 THEN  
															's have '  
														ELSE  
															' has '  
													END + 'insufficient inventory count:';  
  
			SELECT @message = @message + CHAR(10) + ItemName + ': Rem. Qty - ' + CONVERT(varchar(MAX), RemQty) + ' RR Qty - ' + CONVERT(varchar(MAX), RRQty) 
			FROM @ValidateItemInventory;  
			THROW 50001, @message, 1;  
  
		END;  
  

  
END;  
  
  

GO
