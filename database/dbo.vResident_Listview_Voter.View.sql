﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vResident_Listview_Voter]
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid Guid_Company
  FROM   vResident hed
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
  where  hed.IsActive = 1
         and hed.IsVoter = 1
         AND ISNULL(hed.IsDeceased, 0) = 0

GO
