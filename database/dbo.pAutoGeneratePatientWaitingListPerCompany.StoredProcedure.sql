﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CReate 
 PROC [dbo].[pAutoGeneratePatientWaitingListPerCompany](@ID_Company INT)
as
  BEGIN
      DECLARE @IDs_PatientWaitingList TABLE
        (
           RowID                 INT,
           ID_PatientWaitingList INT
        )
      DECLARE @currentCounter INT = 1
      DECLARE @maxCounter INT = 1

      INSERT @IDs_PatientWaitingList
      SELECT ROW_NUMBER()
               OVER(
                 ORDER BY ID ASC) AS RowID,
             ID
      FROM   tPatientWaitingList
      WHERE  ID_Company = @ID_Company
             AND ISNULL(Code, '') = ''

      SELECT @maxCounter = COUNT(*)
      FROM   @IDs_PatientWaitingList

      --IF( @maxCounter > 0 )
      --  SELECT Name,
      --         @maxCounter
      --  FROM   tCompany
      --  WHERE  ID = @ID_Company
      WHILE @currentCounter <= @maxCounter
        BEGIN
            DECLARE @ID_PatientWaitingList INT = 0

            SELECT @ID_PatientWaitingList = ID_PatientWaitingList
            FROM   @IDs_PatientWaitingList
            WHERE  RowID = @currentCounter

            exec [pModel_AfterSaved_PatientWaitingList]
              @ID_PatientWaitingList,
              1

            SET @currentCounter = @currentCounter + 1
        END
  END

GO
