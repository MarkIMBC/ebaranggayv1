﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 VIEW [dbo].[vBarangayClearance]
AS
  SELECT H.*,
         UC.Name          AS CreatedBy,
         UM.Name          AS LastModifiedBy,
         civilStatus.Name Name_CivilStatus,
         gender.Name      Name_Gender
  FROM   tBarangayClearance H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tCivilStatus civilStatus
                ON H.ID_CivilStatus = civilStatus.ID
         LEFT JOIN tGender gender
                ON H.ID_Gender = gender.ID

GO
