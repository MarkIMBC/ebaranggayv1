﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create    Proc [dbo].[_pCancelPatient_Confinement_temp]
as


DECLARE @IDs_Patient_Confinement typIntList

INSERT @IDs_Patient_Confinement
select ID FROm vPatient_Confinement where ID_Client IS NULL and ID_FilingStatus NOT IN (4)

exec pCancelPatient_Confinement @IDs_Patient_Confinement, 4610


GO
