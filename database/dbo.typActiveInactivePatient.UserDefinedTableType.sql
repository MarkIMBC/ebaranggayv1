﻿CREATE TYPE [dbo].[typActiveInactivePatient] AS TABLE(
	[ID_Patient] [int] NULL,
	[IsActive] [bit] NULL
)
GO
