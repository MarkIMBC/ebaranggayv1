﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Proc [dbo].[InactiveCompanyAccess](@IDs_Company typIntList Readonly)
AS
  BEGIN
      update tUser
      SET    IsActive = 0
      FRom   tUser _user
             inner join vUser __user
                     on _user.ID = __user.ID
             inner join @IDs_Company ids
                     on __user.ID_Company = ids.ID

      Update tCompany_SMSSetting
      set    IsActive = 0,
             MaxSMSCountPerDay = 0
      FROm   tCompany_SMSSetting comSms
             inner join @IDs_Company ids
                     on comSms.ID_Company = ids.ID

      Update tCOmpany
      SET    IsActive = 0
      From   tCompany company
             inner join @IDs_Company ids
                     on company.ID = ids.ID
  END 
GO
