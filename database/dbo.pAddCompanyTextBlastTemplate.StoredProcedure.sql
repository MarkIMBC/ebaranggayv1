﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC  [dbo].[pAddCompanyTextBlastTemplate](@ID_Company INT,@TemplateName varchar(MAX) ,@Template varchar(MAX))
AS

INSERT INTO [dbo].[tCompanyTextBlastTemplate]
           ([Code]
           ,[Name]
           ,[IsActive]
           ,[ID_Company]
           ,[Comment]
           ,[DateCreated]
           ,[DateModified]
           ,[ID_CreatedBy]
           ,[ID_LastModifiedBy]
           ,[Template])
     VALUES
           (NULL
           ,@TemplateName
           ,1
           ,@ID_Company
           ,NULL
           ,GETDATE()
           ,GETDATE()
           ,1
           ,1
           ,@Template)
GO
