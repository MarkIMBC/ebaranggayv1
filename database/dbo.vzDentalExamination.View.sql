﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vzDentalExamination]
AS
SELECT pDentalExam.ID,
       pDentalExam.Code,
       pDentalExam.Name_FilingStatus,
       pDentalExam.Date,
       pDentalExam.Name_Doctor,
       pDentalExam.Name_Patient,
       pDentalExam.ApprovedBy_Name_User,
       dbo.fGetPDentalExamToothLocationSurfaceString(pDentalExam.ID) ToothLocationSurfaceString,
       tooth.ToothNumber ToothNumber_Tooth,
       tooth.Location Location_Tooth,
       '' ToothSurfaceNames,
       pDentalExamToothInfo.Name_ToothStatus,
       pDentalExamToothInfo.Comment,
       patient.ID_Company,
       company.ImageLogoLocationFilenamePath,
       company.Name Name_Company,
       company.Address Address_Company
FROM dbo.vPatient_DentalExamination pDentalExam
    LEFT JOIN dbo.tPatient patient
        ON patient.ID = pDentalExam.ID_Patient
    LEFT JOIN dbo.vPatient_DentalExamination_ToothInfo pDentalExamToothInfo
        ON pDentalExam.ID = pDentalExamToothInfo.ID_Patient_DentalExamination
    LEFT JOIN dbo.tTooth tooth
        ON pDentalExamToothInfo.ID_Tooth = tooth.ID
    LEFT JOIN dbo.vCompany company
        ON company.ID = patient.ID_Company;


GO
