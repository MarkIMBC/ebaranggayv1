﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fGenerateDocumentSeries] (@ID_Model      UNIQUEIDENTIFIER,
                                                @ID_Company    INT,
                                                @plusCount     INT = 0,
                                                @CustomCounter INT = NULL)
RETURNS VARCHAR(40)
  BEGIN
      DECLARE @ID_DocumentSeries INT = 0;
      DECLARE @Prefix VARCHAR(50);
      DECLARE @IsAppendCurrentDate BIT = 0;
      DECLARE @DigitCount INT = 0;
      DECLARE @Counter INT = 0;
      DECLARE @GenerateDocSeries VARCHAR(20) = '';

      SELECT @ID_DocumentSeries = ID
      FROM   dbo.tDocumentSeries ds
      WHERE  ds.ID_Model = @ID_Model
             AND ID_Company = @ID_Company;

      SELECT @Prefix = Prefix,
             @IsAppendCurrentDate = IsNull(IsAppendCurrentDate, 0),
             @DigitCount = DigitCount,
             @Counter = Counter
      FROM   dbo.tDocumentSeries
      WHERE  ID = @ID_DocumentSeries
             AND ID_Company = @ID_Company;

      IF @CustomCounter IS NOT NULL
        SET @Counter = @CustomCounter

      SET @Counter = @Counter + @plusCount;

      IF len(CONVERT(VARCHAR(50), @Counter)) > @DigitCount
        SET @DigitCount = len(CONVERT(VARCHAR(50), @Counter))

      SET @Counter = @Counter + @plusCount;
      SET @GenerateDocSeries = @Prefix + '-'
                               + Replicate('0', @DigitCount - len(@Counter))
                               + CONVERT(VARCHAR(50), @Counter);

      RETURN @GenerateDocSeries;
  END; 
GO
