﻿CREATE TYPE [dbo].[typPatienDentalExamTooth] AS TABLE(
	[ID_Patient_DentalExamination] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[ID_Patient] [int] NOT NULL,
	[ID_Doctor] [int] NOT NULL,
	[ID_Patient_DentalExamination_ToothInfo] [int] NOT NULL,
	[ID_Tooth] [int] NOT NULL,
	[IDs_ToothSurface] [varchar](15) NULL,
	[ID_ToothStatus] [int] NOT NULL,
	[Comment] [varchar](max) NULL,
	[Prescription] [varchar](max) NULL,
	[GUID_Patient_DentalExamination] [varchar](300) NULL,
	[GUID_Patient_DentalExamination_ToothInfo] [varchar](300) NULL,
	[ID_Dentition] [int] NOT NULL,
	[IsDelete] [bit] NULL DEFAULT ((0))
)
GO
