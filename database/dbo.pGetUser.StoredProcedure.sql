﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pGetUser]
    @ID INT = -1,
    @ID_Session INT = NULL
AS
BEGIN
    SELECT '_',
           '' AS User_Roles;

    DECLARE @ID_User INT,
            @ID_Warehouse INT;
    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM tUserSession
    WHERE ID = @ID_Session;

    IF (@ID = -1)
    BEGIN
        SELECT H.*
        FROM
        (
            SELECT NULL AS [_],
                   -1 AS [ID],
                   NULL AS [Code],
                   NULL AS [Name],
                   1 AS [IsActive],
                   NULL AS [Comment],
                   NULL AS [DateCreated],
                   NULL AS [DateModified],
                   NULL AS [ID_CreatedBy],
                   NULL AS [ID_LastModifiedBy],
                   NULL AS [ID_Employee]
        ) H
            LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
            LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID;
    END;
    ELSE
    BEGIN
        SELECT H.ID,
               H.Code,
               H.Name,
               H.IsActive,
               H.Comment,
               H.DateCreated,
               H.DateModified,
               H.ID_CreatedBy,
               H.ID_LastModifiedBy,
               H.ID_Employee,
               H.Username,
               H.IsRequiredPasswordChangedOnLogin,
               H.ID_Patient,
               H.Employee,
               H.Name_Employee,
               H.UserGroup,
               H.CreatedBy,
               H.LastModifiedBy,
               H.ID_Company,
               H.Name_Company
        FROM vUser H
        WHERE H.ID = @ID;
    END;

    SELECT UR.*,
           R.Name AS RoleName,
           R.Name AS Name_UserRole
    FROM dbo.tUser_Roles UR
        LEFT JOIN dbo.tUserRole R
            ON UR.ID_UserRole = R.ID
    WHERE UR.ID_User = @ID;
END;
GO
