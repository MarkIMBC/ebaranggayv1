﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pGetDashboardMunicipalityMonthlyClientsDataSource] (@ID_UserSession INT,
                                                                  @DateYear       VARCHAR(10))
AS
  BEGIN
      DECLARE @ID_User INT
      DECLARE @ID_Company INT

      SELECT @ID_User = ID_User,
             @ID_Company = ID_Company
      FROM   tUserSession _usersession
             inner join vUser _user
                     on _usersession.ID_User = _user.ID
      WHERE  _usersession.ID = @ID_UserSession

      DECLARE @ClientDailyCount INT = 0
      DECLARE @ClientMonthlyCount INT = 0
      DECLARE @SalesDailyAmount DECIMAL(18, 2) = 0
      DECLARE @SalesMonthlyAmount DECIMAL(18, 2) = 0

      SELECT '_' AS _,
             ''  AS DataSource

      SELECT @DateYear                          DateYear,
             'Mga Bagong Resident ' + @DateYear Label

      SELECT SUM(CASE datepart(month, resident. DateCreated)
                   WHEN 1 THEN 1
                   ELSE 0
                 END) AS 'January',
             SUM(CASE datepart(month, resident. DateCreated)
                   WHEN 2 THEN 1
                   ELSE 0
                 END) AS 'February',
             SUM(CASE datepart(month, resident. DateCreated)
                   WHEN 3 THEN 1
                   ELSE 0
                 END) AS 'March',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 4 THEN 1
                   ELSE 0
                 END) AS 'April',
             SUM(CASE datepart(month, resident. DateCreated)
                   WHEN 5 THEN 1
                   ELSE 0
                 END) AS 'May',
             SUM(CASE datepart(month, resident. DateCreated)
                   WHEN 6 THEN 1
                   ELSE 0
                 END) AS 'June',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 7 THEN 1
                   ELSE 0
                 END) AS 'July',
             SUM(CASE datepart(month, resident. DateCreated)
                   WHEN 8 THEN 1
                   ELSE 0
                 END) AS 'August',
             SUM(CASE datepart(month, resident.DateCreated)
                   WHEN 9 THEN 1
                   ELSE 0
                 END) AS 'September',
             SUM(CASE datepart(month, resident. DateCreated)
                   WHEN 10 THEN 1
                   ELSE 0
                 END) AS 'October',
             SUM(CASE datepart(month, resident. DateCreated)
                   WHEN 11 THEN 1
                   ELSE 0
                 END) AS 'November',
             SUM(CASE datepart(month, resident. DateCreated)
                   WHEN 12 THEN 1
                   ELSE 0
                 END) AS 'December',
             SUM(CASE datepart(year, resident.DateCreated)
                   WHEN 2012 THEN 1
                   ELSE 0
                 END) AS 'TOTAL'
      FROM   tResident resident
             INNER JOIN vCompanyActive company
                     on resident.ID_Company = company.ID
      WHERE  resident . IsActive = 1
             AND YEAR(resident.DateCreated) = @DateYear
             AND ID_Company > 1
  END

GO
