﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vCertificateOfIndigency_Listview]
AS
  SELECT hed.ID,
         hed.Date,
         hed.Code,
         hed.Name,
         hed.Age,
         hed.Citizenship,
         hed.RequestOf,
         hed.ID_Company,
         hed.IsActive
  FROM   vCertificateOfIndigency hed

GO
