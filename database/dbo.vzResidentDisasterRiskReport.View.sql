﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 VIEW [dbo].[vzResidentDisasterRiskReport]
AS
  SELECT hed.ID,
         hed.Name,
         hed.Name_Gender,
         hed.ID_Gender,
         hed.ContactNumber,
         hed.VoteInfoPrecinctNumber,
         dbo.fGetIntAge(hed.DateBirth)                           Age,
         householdNumber.Name                                    Name_HouseholdNumber,
         householdNumber.AssignedBHW_Name_Employee,
         disasterRisk.Name_Disaster,
         disasterRisk.ID_Disaster,
         ISNULL(disasterRiskList.DisasterRisk, '<i>None</i>')    DisasterRisk,
         company.ID                                              ID_Company,
         company.ImageLogoLocationFilenamePath                   ImageLogoLocationFilenamePath,
         company.Name                                            Name_Company,
         company.Address                                         Address_Company,
         company.ContactNumber                                   ContactNumber_Company,
         company.BarangayListSideReportHTMLString                BarangayListSideReportHTMLString_Company,
         company.BarangayCaptainName                             BarangayCaptainName_Company,
         company.BarangayCaptain_Name_Employee                   BarangayCaptain_Name_Employee,
         company.BarangayCaptain_LastName_Employee               BarangayCaptain_LastName_Employee,
         company.BarangayCaptain_FirstName_Employee              BarangayCaptain_FirstName_Employee,
         company.BarangayCaptain_MiddleName_Employee             BarangayCaptain_MiddleName_Employee,
         company.BarangayCaptain_ImageSignitureSpecimen_Employee BarangayCaptain_ImageSignitureSpecimen_Employee,
         municipality.Name                                       Name_MunicipalityCompany,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' <br/>Email: ' + company.Email
             ELSE ''
           END                                                   HeaderInfo_Company,
         municipality.ImageLogoLocationFilenamePath              ImageLogoLocationFilenamePath_MunicipalityCompany,
         municipality.Address                                    Address_MunicipalityCompany
  FROM   vResident hed
         iNNER JOIN vHouseholdNumber householdNumber
                 on hed.ID_HouseholdNumber = householdNumber.ID
         INNER JOIN vHouseholdNumberDisasterRisk disasterRiskList
                 on disasterRiskList.ID_HouseholdNumber = householdNumber.ID
         INNER JOIN vHouseholdNumber_Disaster disasterRisk
                 on disasterRisk.ID_HouseholdNumber = householdNumber.ID
         INNER JOIN vCompanyActive company
                 ON company.ID = hed.ID_Company
         INNER JOIN vMunicipalityCompany municipality
                 ON company.ID = hed.ID_Company
  WHERE  hed.IsActive = 1
         and hed.IsVoter = 1
         and ISNULL(IsDeceased, 0) = 0
         And IsHeadofFamily = 1

GO
