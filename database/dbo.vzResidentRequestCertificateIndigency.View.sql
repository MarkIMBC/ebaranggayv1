﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  
 VIEW [dbo].[vzResidentRequestCertificateIndigency]
AS
  SELECT Resident.Name         NAME,
         Resident.ID           ID,
         company.ID            ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.Name          Name_Company,
         company.Address       Address_Company,
         company.ContactNumber ContactNumber_Company
  FROM   vResidentRequest_IndigencyCertificate Resident
         INNER JOIN vCompany company
                 ON company.ID = Resident.ID_Company

GO
