﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pInsertReligion] (@Religion   VARCHAR(MAX),
                            @ID_Company INT = 1)
as
  BEGIN
      IF(SELECT COUNT(*)
         FROM   tReligion
         WHERE  Name = @Religion) = 0
        BEGIN
            INSERT INTO [dbo].tReligion
                        ([Name],
                         [IsActive],
                         ID_Company)
            VALUES      (@Religion,
                         1,
                         @ID_Company)
        END
  END

GO
