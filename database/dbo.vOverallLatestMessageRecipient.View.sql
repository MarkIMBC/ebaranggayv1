﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vOverallLatestMessageRecipient]
as
SELECT Sender_ID_User,
       Sender_Name_User,
       Recipient_ID_User,
       Recipient_Name_User,
       TotalReadCount,
       LastDateSent,
       IsRead
FROM vLastestMessageRecipient
UNION All
SELECT
       Recipient_ID_User Sender_ID_User,
       Recipient_Name_User Sender_Name_User,
	    Sender_ID_User Recipient_ID_User,
       Sender_Name_User Recipient_Name_User,
       TotalReadCount,
       LastDateSent,
       IsRead
FROM vLastestMessageRecipient
GO
