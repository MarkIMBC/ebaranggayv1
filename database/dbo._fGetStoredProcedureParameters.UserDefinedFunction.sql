﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[_fGetStoredProcedureParameters]
    (
      @ProcedureName VARCHAR(300)
    )
RETURNS TABLE
RETURN
    SELECT  'Name' = p.name ,
            'Type' = TYPE_NAME(p.user_type_id) ,
            'Length' = p.max_length ,
            'Param_order' = p.parameter_id ,
            p.system_type_id ,
            dbo._fGetUserTypesParameters(p.user_type_id) AS TypeParameters
    FROM    sys.parameters p
            LEFT JOIN sys.types t ON p.user_type_id = t.user_type_id
    WHERE   p.object_id = OBJECT_ID(@ProcedureName)
GO
