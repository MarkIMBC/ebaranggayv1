﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SqlQueryNotificationStoredProcedure-885f83a9-ada2-4bb0-a6e5-9a8973178033] AS BEGIN BEGIN TRANSACTION; RECEIVE TOP(0) conversation_handle FROM [SqlQueryNotificationService-885f83a9-ada2-4bb0-a6e5-9a8973178033]; IF (SELECT COUNT(*) FROM [SqlQueryNotificationService-885f83a9-ada2-4bb0-a6e5-9a8973178033] WHERE message_type_name = 'http://schemas.microsoft.com/SQL/ServiceBroker/DialogTimer') > 0 BEGIN if ((SELECT COUNT(*) FROM sys.services WHERE name = 'SqlQueryNotificationService-885f83a9-ada2-4bb0-a6e5-9a8973178033') > 0)   DROP SERVICE [SqlQueryNotificationService-885f83a9-ada2-4bb0-a6e5-9a8973178033]; if (OBJECT_ID('SqlQueryNotificationService-885f83a9-ada2-4bb0-a6e5-9a8973178033', 'SQ') IS NOT NULL)   DROP QUEUE [SqlQueryNotificationService-885f83a9-ada2-4bb0-a6e5-9a8973178033]; DROP PROCEDURE [SqlQueryNotificationStoredProcedure-885f83a9-ada2-4bb0-a6e5-9a8973178033]; END COMMIT TRANSACTION; END
GO
