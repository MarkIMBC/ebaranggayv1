﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pGetSchedule]
    @ID INT = -1,
    @ID_Session INT = NULL,
	@ID_Doctor INT = NULL,
	@DateStart DATETIME = NULL,
	@DateEnd DATETIME = NULL
AS
BEGIN
    SELECT '_',
           '' AS Schedule_PatientAppointment;

    DECLARE @ID_User INT;
    DECLARE @ID_Warehouse INT;
    DECLARE @ID_Employee INT;
    DECLARE @ID_ScheduleType_Work INT = 1;

    SELECT @ID_User = ID_User,
           @ID_Warehouse = ID_Warehouse
    FROM tUserSession
    WHERE ID = @ID_Session;

    SELECT @ID_Employee = u.ID_Employee
    FROM tUser u
    WHERE ID = @ID_User;

    SET @ID_Employee = 1;

    IF (@ID = -1)
    BEGIN
        SELECT H.*,
               e.Name Doctor
        FROM
        (
            SELECT NULL AS [_],
                   -1 AS [ID],
                   NULL AS [Code],
                   NULL AS [Name],
                   1 AS [IsActive],
                   NULL AS [ID_Company],
                   NULL AS [Comment],
                   NULL AS [DateCreated],
                   NULL AS [DateModified],
                   NULL AS [ID_CreatedBy],
                   NULL AS [ID_LastModifiedBy],
                   @ID_ScheduleType_Work AS [ID_ScheduleType],
                   @ID_Doctor AS [ID_Doctor],
                   0 VacantCount,
                   0 AccommodateCount,
				   @DateStart DateStart,
				   @DateEnd DateEnd
        ) H
            LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
            LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
            LEFT JOIN tEmployee e
                ON H.ID_Doctor = e.ID
            LEFT JOIN tScheduleType st
                ON st.ID = H.ID_ScheduleType;
    END;
    ELSE
    BEGIN
        SELECT H.*
        FROM vSchedule H
        WHERE H.ID = @ID;
    END;

    SELECT ID,
           ID_Patient,
           Patient,
           DateCreated
    FROM vAppointmentSchedule
    WHERE ID_Schedule = @ID;

END;

GO
