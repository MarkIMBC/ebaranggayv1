﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vHouseholdNumber]
AS
  SELECT H.*,
         UC.Name                 AS CreatedBy,
         UM.Name                 AS LastModifiedBy,
         AssignedBHW.Name        AssignedBHW_Name_Employee,
         residentRespondent.Name Representative_Name_Resident
  FROM   tHouseholdNumber H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tEmployee AssignedBHW
                ON AssignedBHW.ID = h.AssignedBHW_ID_Employee
         LEFT JOIN tResident residentRespondent
                ON h.Representative_ID_Resident = residentRespondent.ID

GO
