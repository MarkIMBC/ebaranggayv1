﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SqlQueryNotificationStoredProcedure-2b2290f3-1c20-4ec3-977b-cccf48a3fb54] AS BEGIN BEGIN TRANSACTION; RECEIVE TOP(0) conversation_handle FROM [SqlQueryNotificationService-2b2290f3-1c20-4ec3-977b-cccf48a3fb54]; IF (SELECT COUNT(*) FROM [SqlQueryNotificationService-2b2290f3-1c20-4ec3-977b-cccf48a3fb54] WHERE message_type_name = 'http://schemas.microsoft.com/SQL/ServiceBroker/DialogTimer') > 0 BEGIN if ((SELECT COUNT(*) FROM sys.services WHERE name = 'SqlQueryNotificationService-2b2290f3-1c20-4ec3-977b-cccf48a3fb54') > 0)   DROP SERVICE [SqlQueryNotificationService-2b2290f3-1c20-4ec3-977b-cccf48a3fb54]; if (OBJECT_ID('SqlQueryNotificationService-2b2290f3-1c20-4ec3-977b-cccf48a3fb54', 'SQ') IS NOT NULL)   DROP QUEUE [SqlQueryNotificationService-2b2290f3-1c20-4ec3-977b-cccf48a3fb54]; DROP PROCEDURE [SqlQueryNotificationStoredProcedure-2b2290f3-1c20-4ec3-977b-cccf48a3fb54]; END COMMIT TRANSACTION; END
GO
