﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pGetModelDateColumns](@Oid_Model VARCHAR(500))
AS
  BEGIN
      DECLARE @Success BIT = 1;
      DECLARE @PropertyList TABLE
        (
           Oid  VARCHAR(MAX),
           Name VARCHAR(MAX)
        )
      DECLARE @PropertyListDateColumns TABLE
        (
           ParentName     VARCHAR(MAX),
           DateColumnName VARCHAR(MAX)
        )

      INSERT @PropertyListDateColumns
      SELECT '',
             Name
      FROM   _tModel_Property
      WHERE  ID_Model = @Oid_Model
             AND ID_PropertyType IN ( 5, 6 )

      INSERT @PropertyList
      SELECT ID_PropertyModel,
             Name
      FROM   _tModel_Property
      WHERE  Lower(ID_Model) = Lower(@Oid_Model)
             AND ID_PropertyType IN ( 10 )

      INSERT @PropertyListDateColumns
      SELECT propList.Name,
             modelProp.Name
      FROM   _tModel_Property modelProp
             INNER JOIN @PropertyList propList
                     ON modelProp.ID_Model = propList.OId
      WHERE  ID_PropertyType IN ( 5, 6 )
      ORDER  BY propList.Name

      SELECT '_' [_],
             ''  PropertyList,
             ''  DateColumns

      SELECT @Success Success

      SELECT DISTINCT ParentName
      FROM   @PropertyListDateColumns

      SELECT DISTINCT DateColumnName
      FROM   @PropertyListDateColumns
  END

GO
