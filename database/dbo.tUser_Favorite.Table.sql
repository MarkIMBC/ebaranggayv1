﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tUser_Favorite](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](max) NULL,
	[ID_User] [int] NOT NULL,
	[ID_Navigation] [uniqueidentifier] NOT NULL,
	[DateCreated] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
 CONSTRAINT [PK_tUser_Favorites] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tUser_Favorite] ADD  CONSTRAINT [DF__tUser_Fav__IsAct__4183B671]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tUser_Favorite]  WITH NOCHECK ADD  CONSTRAINT [FKtUser_Favorite_ID_Navigation] FOREIGN KEY([ID_Navigation])
REFERENCES [dbo].[_tNavigation] ([Oid])
GO
ALTER TABLE [dbo].[tUser_Favorite] CHECK CONSTRAINT [FKtUser_Favorite_ID_Navigation]
GO
ALTER TABLE [dbo].[tUser_Favorite]  WITH NOCHECK ADD  CONSTRAINT [FKtUser_Favorite_ID_User] FOREIGN KEY([ID_User])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tUser_Favorite] CHECK CONSTRAINT [FKtUser_Favorite_ID_User]
GO
