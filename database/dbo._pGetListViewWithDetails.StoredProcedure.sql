﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[_pGetListViewWithDetails] @Oid UNIQUEIDENTIFIER
AS
     SELECT '' AS [_], 
            '' AS [Details];

     SELECT H.Oid,
            --H.Code ,
            H.Name,
            --H.IsActive ,
            H.ID_Model, 
            H.DataSource, 
            H.Comment,
            --H.Caption ,
            --H.ID_CreatedBy ,
            --H.ID_LastModifiedBy ,
            --H.DateCreated ,
            --H.DateModified ,
            M.ID_DetailView, 
            M.Name AS Model, 
		    U.COLUMN_NAME AS PrimaryKey,
            ISNULL(H.Caption, ISNULL(M.Caption, M.Name)) AS Caption
     FROM dbo.[_tListView] H
          LEFT JOIN dbo.[_tModel] M ON H.ID_Model = M.Oid
		  LEFT JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE U ON OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA
                                                              + '.'
                                                              + QUOTENAME(CONSTRAINT_NAME)),
                                                              'IsPrimaryKey') = 1
                                                              AND U.TABLE_NAME = M.TableName
     WHERE H.Oid = @Oid;

     SELECT ISNULL(D.ID_PropertyType, MP.ID_PropertyType) AS ID_PropertyType, 
            (CASE ISNULL(D.ID_PropertyType, MP.ID_PropertyType)
                 WHEN 3
                 THEN ISNULL(D.Precision, 2)
                 ELSE NULL
             END) AS [Precision], 
            D.DataSource AS [$DataSource], 
            ISNULL(D.DisplayProperty, MP.DisplayProperty) AS DisplayProperty, 
            D.*, 
            MP.ID_PropertyModel, 
            PM.Name AS PropertyModel, 
            ST.Name AS SummaryType
     FROM dbo.[_tListView_Detail] D
          LEFT JOIN dbo.[_tModel_Property] MP ON D.ID_ModelProperty = MP.Oid
          LEFT JOIN dbo.[_tModel] PM ON MP.ID_PropertyModel = PM.Oid
          LEFT JOIN dbo.[_tSummaryType] ST ON D.ID_SummaryType = ST.ID
     WHERE D.ID_ListView = @Oid
           AND D.IsActive = 1
     ORDER BY D.DateCreated, 
              d.Oid;
GO
