﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   VIEW [dbo].[vIssueTracker] 
AS 
	SELECT 
		H.* ,
		UC.Name AS CreatedBy,
		UM.Name AS LastModifiedBy,
		fs.Name Name_FilingStatus,
		approvedBy.Name Name_ApprovedBy
	FROM tIssueTracker H
	LEFT JOIN tUser UC 
		ON H.ID_CreatedBy = UC.ID	
	LEFT JOIN tUser 
		UM ON H.ID_LastModifiedBy = UM.ID
	LEFT JOIN tUser approvedBy
		 ON H.ID_ApprovedBy = approvedBy.ID
	LEFT JOIN tFilingStatus fs
		on fs.ID = H.ID_FilingStatus

GO
