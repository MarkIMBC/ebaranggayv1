﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  
  
CREATE VIEW [dbo].[vReceivingReport_Detail]  
AS  
SELECT detail.ID,  
       detail.Code,  
       detail.Name,  
       detail.IsActive,  
       detail.Comment,  
       detail.ID_ReceivingReport,  
       detail.ID_Item,  
       detail.Quantity,  
       detail.UnitPrice,  
       detail.Amount,  
       item.Name Name_Item,
	   detail.ID_PurchaseOrder_Detail
FROM dbo.tReceivingReport_Detail detail  
    LEFT JOIN dbo.tItem item  
        ON item.ID = detail.ID_Item;  
  
GO
