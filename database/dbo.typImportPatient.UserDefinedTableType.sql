﻿CREATE TYPE [dbo].[typImportPatient] AS TABLE(
	[Name] [varchar](max) NULL,
	[Old_client_id] [int] NULL,
	[Old_patient_code] [varchar](max) NULL,
	[Old_patient_id] [int] NULL,
	[Species] [varchar](max) NULL,
	[Breed] [varchar](max) NULL,
	[Gender] [varchar](max) NULL,
	[Color] [varchar](max) NULL,
	[Remarks] [varchar](max) NULL,
	[BirthDate] [datetime] NULL,
	[IsDeceased] [varchar](max) NULL,
	[IsNeutered] [varchar](max) NULL
)
GO
