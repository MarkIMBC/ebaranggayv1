﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   FUNCTION [dbo].[fGetAge] (@dateBirth DATETIME, @dateDateStart DateTime = NULL, @InvalidDateCaption VARCHAR(MAX) = NULL)
RETURNS VARCHAR(MAX)
AS
BEGIN

  DECLARE @result VARCHAR(MAX) = ''

  DECLARE @date DATETIME
         ,@tmpdate DATETIME
         ,@years INT
         ,@months INT
         ,@days INT

  IF @dateBirth IS NULL
    RETURN ''

  IF @dateDateStart IS NULL
	SET @dateDateStart = getdate()	

  SELECT
    @date = @dateBirth

  SELECT
    @tmpdate = @date

  SELECT
    @years = DATEDIFF(yy, @tmpdate, @dateDateStart) -
    CASE
      WHEN (MONTH(@date) > MONTH(@dateDateStart)) OR
        (MONTH(@date) = MONTH(@dateDateStart) AND
        DAY(@date) > DAY(@dateDateStart)) THEN 1
      ELSE 0
    END
  SELECT
    @tmpdate = DATEADD(yy, @years, @tmpdate)
  SELECT
    @months = DATEDIFF(m, @tmpdate, @dateDateStart) -
    CASE
      WHEN DAY(@date) > DAY(@dateDateStart) THEN 1
      ELSE 0
    END
  SELECT
    @tmpdate = DATEADD(m, @months, @tmpdate)
  SELECT
    @days = DATEDIFF(d, @tmpdate, @dateDateStart)
	 
  IF (@years > 0)
    SET @result = CONVERT(VARCHAR(MAX), @years) + ' year' +
    CASE
      WHEN @years > 1 THEN 's'
      ELSE ''
    END + ' '
  IF (@years >= 0 AND @months > 0)
    SET @result = @result +  CONVERT(VARCHAR(MAX), @months) + ' month' +
    CASE
      WHEN @months > 1 THEN 's'
      ELSE ''
    END + ' '
  IF (@years >= 0 AND @months >= 0 AND @days > 0)
    SET @result = @result + CONVERT(VARCHAR(MAX), @days) + ' day' +
    CASE
      WHEN @days > 1 THEN 's'
      ELSE ''
    END 

	if(LEN(@result) = 0)
		set @result = @InvalidDateCaption
	ELSE
		set @result = @result + ' old'

   RETURN @result
END

GO
