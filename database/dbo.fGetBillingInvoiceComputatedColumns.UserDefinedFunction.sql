﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 FUNCTION [dbo].[fGetBillingInvoiceComputatedColumns] (@DetailTotalAmount        DECIMAL(18, 4),
                                                        @DiscountRate             DECIMAL(18, 4),
                                                        @DiscountAmount           DECIMAL(18, 4),
                                                        @IsComputeDiscountRate    BIT,
                                                        @ID_TaxScheme             INT,
                                                        @ConfinementDepositAmount DECIMAL(18, 4))
RETURNS @table TABLE (
  InitialSubtotalAmount  DECIMAL(18, 4),
  ConsumedDepositAmount  DECIMAL(18, 4),
  RemainingDepositAmount DECIMAL(18, 4),
  SubTotal               DECIMAL(18, 4),
  TotalAmount            DECIMAL(18, 4),
  DiscountRate           DECIMAL(18, 4),
  DiscountAmount         DECIMAL(18, 4),
  GrossAmount            DECIMAL(18, 4),
  VatAmount              DECIMAL(18, 4),
  NetAmount              DECIMAL(18, 4))
AS
  BEGIN
      DECLARE @InitialSubtotalAmount DECIMAL(18, 4) = 0
      DECLARE @ConsumedDepositAmount DECIMAL(18, 4) = 0
      DECLARE @RemainingDepositAmount DECIMAL(18, 4) = 0
      DECLARE @SubTotal DECIMAL(18, 4) = 0
      DECLARE @TotalAmount DECIMAL(18, 4) = 0
      DECLARE @GrossAmount DECIMAL(18, 4) = 0
      DECLARE @VatAmount DECIMAL(18, 4) = 0
      DECLARE @NetAmount DECIMAL(18, 4) = 0
      DECLARE @ID_TaxScheme_TaxExclusive INT = 1
      DECLARE @ID_TaxScheme_TaxInclusive INT = 2
      DECLARE @ID_TaxScheme_ZeroRated INT = 3

      SET @ID_TaxScheme = IsNull(@ID_TaxScheme, 0);
      SET @IsComputeDiscountRate = IsNull(@IsComputeDiscountRate, 0);
      SET @InitialSubtotalAmount = @DetailTotalAmount
      SET @SubTotal = @DetailTotalAmount
      /*Confinement Deposit Computation*/
      SET @RemainingDepositAmount = @ConfinementDepositAmount - @SubTotal

      IF @RemainingDepositAmount < 0
        BEGIN
            SET @RemainingDepositAmount = 0
        END

      SET @RemainingDepositAmount = Round(@RemainingDepositAmount, 2)
      SET @ConsumedDepositAmount = @ConfinementDepositAmount - @RemainingDepositAmount
      SET @SubTotal = @SubTotal - @ConfinementDepositAmount

      ------------------------------------------------------------
      /*Subtotal Total Amount*/
      IF @SubTotal <= 0
        BEGIN
            SET @SubTotal = 0;
        END

      SET @SubTotal = Round(@SubTotal, 2)
      SET @TotalAmount = @SubTotal

      ------------------------------------------------------------
      /*Discount Amount and Disount Rate*/
      IF @SubTotal > 0
        BEGIN
            IF( @IsComputeDiscountRate = 1 )
              BEGIN
                  SET @DiscountAmount = @TotalAmount * ( @DiscountRate / 100 )
                  SET @DiscountAmount = Round(@DiscountAmount, 2)
              END
            ELSE
              BEGIN
                  SET @DiscountRate = ( @DiscountAmount / @TotalAmount ) * 100
                  SET @DiscountRate = Round(@DiscountRate, 2)
              END
        END
      ELSE
        BEGIN
            SET @DiscountRate = 0
            SET @DiscountAmount = 0
        END

      ------------------------------------------------------------
      /*GrossAmount, VatAmount, NetAmount*/
      IF @SubTotal > 0
        BEGIN
            SET @TotalAmount = @TotalAmount - @DiscountAmount;
            SET @TotalAmount = Round(@TotalAmount, 2)
            SET @GrossAmount = @TotalAmount
            SET @VatAmount = ( @GrossAmount / 1.12 ) * 0.12
            SET @VatAmount = Round(@VatAmount, 2)

            IF( @ID_TaxScheme = 0
                 OR @ID_TaxScheme = @ID_TaxScheme_ZeroRated )
              BEGIN
                  SET @VatAmount = 0
                  SET @NetAmount = @GrossAmount
              END
            ELSE IF( @ID_TaxScheme = @ID_TaxScheme_TaxExclusive )
              BEGIN
                  SET @NetAmount = @GrossAmount + @VatAmount
              END
            ELSE IF( @ID_TaxScheme = @ID_TaxScheme_TaxInclusive )
              BEGIN
                  SET @NetAmount = @GrossAmount - @VatAmount
              END
        END

      INSERT @table
             (InitialSubtotalAmount,
              ConsumedDepositAmount,
              RemainingDepositAmount,
              SubTotal,
              TotalAmount,
              DiscountRate,
              DiscountAmount,
              GrossAmount,
              VatAmount,
              NetAmount)
      SELECT @InitialSubtotalAmount,
             @ConsumedDepositAmount,
             @RemainingDepositAmount,
             @SubTotal,
             @TotalAmount,
             @DiscountRate,
             @DiscountAmount,
             @GrossAmount,
             @VatAmount,
             @NetAmount

      ------------------------------------------------------------
      RETURN
  END

GO
