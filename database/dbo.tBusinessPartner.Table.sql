﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tBusinessPartner](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[Comment] [varchar](max) NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[TelNoA] [varchar](50) NULL,
	[TelNoB] [varchar](50) NULL,
	[MobilePhone] [varchar](50) NULL,
	[Fax] [varchar](50) NULL,
	[EmailAddress] [varchar](50) NULL,
	[WebSite] [varchar](50) NULL,
	[ID_Industry] [int] NULL,
	[AliasName] [varchar](50) NULL,
	[TIN] [varchar](50) NULL,
	[IsCustomer] [bit] NULL,
	[IsSupplier] [bit] NULL,
 CONSTRAINT [PK_tBusinessPartner] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tBusinessPartner] ADD  CONSTRAINT [DF__tBusiness__IsAct__79B300FB]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tBusinessPartner]  WITH NOCHECK ADD  CONSTRAINT [FK_tBusinessPartner_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tBusinessPartner] CHECK CONSTRAINT [FK_tBusinessPartner_ID_CreatedBy]
GO
ALTER TABLE [dbo].[tBusinessPartner]  WITH NOCHECK ADD  CONSTRAINT [FK_tBusinessPartner_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[tBusinessPartner] CHECK CONSTRAINT [FK_tBusinessPartner_ID_LastModifiedBy]
GO
