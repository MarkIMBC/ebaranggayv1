﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
CREATE VIEW [dbo].[vRemainingQuantityPurchaseOrderDetail]  
AS  
  
	SELECT detail.ID,   
		detail.Name_Item,   
		detail.Quantity,   
		detail.RemainingQuantity,  
		detail.UnitPrice,  
		detail.ID_PurchaseOrder,  
		hed.ID_FilingStatus ,
		hed.Name_FilingStatus,
		hed.ServingStatus_Name_FilingStatus
    FROM vPurchaseOrder_Detail detail  
		INNER JOIN vPurchaseOrder hed   
			On hed.ID = detail.ID_PurchaseOrder  
    WHERE   
    ISNULL(detail.RemainingQuantity, 0) > 0  
GO
