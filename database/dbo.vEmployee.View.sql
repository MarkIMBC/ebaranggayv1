﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vEmployee]
AS
SELECT H.*,
       UC.Name AS CreatedBy,
       UM.Name AS LastModifiedBy,
       position.Name Name_Position
FROM tEmployee H
    LEFT JOIN tUser UC
        ON H.ID_CreatedBy = UC.ID
    LEFT JOIN tUser UM
        ON H.ID_LastModifiedBy = UM.ID
    LEFT JOIN dbo.tPosition position
        ON position.ID = H.ID_Position;

GO
