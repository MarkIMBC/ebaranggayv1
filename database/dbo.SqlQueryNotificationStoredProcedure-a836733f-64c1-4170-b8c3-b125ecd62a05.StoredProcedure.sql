﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SqlQueryNotificationStoredProcedure-a836733f-64c1-4170-b8c3-b125ecd62a05] AS BEGIN BEGIN TRANSACTION; RECEIVE TOP(0) conversation_handle FROM [SqlQueryNotificationService-a836733f-64c1-4170-b8c3-b125ecd62a05]; IF (SELECT COUNT(*) FROM [SqlQueryNotificationService-a836733f-64c1-4170-b8c3-b125ecd62a05] WHERE message_type_name = 'http://schemas.microsoft.com/SQL/ServiceBroker/DialogTimer') > 0 BEGIN if ((SELECT COUNT(*) FROM sys.services WHERE name = 'SqlQueryNotificationService-a836733f-64c1-4170-b8c3-b125ecd62a05') > 0)   DROP SERVICE [SqlQueryNotificationService-a836733f-64c1-4170-b8c3-b125ecd62a05]; if (OBJECT_ID('SqlQueryNotificationService-a836733f-64c1-4170-b8c3-b125ecd62a05', 'SQ') IS NOT NULL)   DROP QUEUE [SqlQueryNotificationService-a836733f-64c1-4170-b8c3-b125ecd62a05]; DROP PROCEDURE [SqlQueryNotificationStoredProcedure-a836733f-64c1-4170-b8c3-b125ecd62a05]; END COMMIT TRANSACTION; END
GO
