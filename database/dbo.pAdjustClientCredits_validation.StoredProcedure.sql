﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pAdjustClientCredits_validation] (@ClientCredits  typClientCredit READONLY,
                                                  @ID_UserSession INT)
AS
  BEGIN
      DECLARE @isValid BIT = 1;
      DECLARE @message VARCHAR(300) = '';
      DECLARE @ConfinementCount INT = 0
      DECLARE @_ClientCredits typClientCredit

      INSERT @_ClientCredits
      SELECT *
      FROm   @ClientCredits

      DECLARE @ClientCurrentCredit TABLE
        (
           ClientName          VARCHAR(MAX),
           CreditAmount        Decimal(18, 4),
           CurrentCreditAmount Decimal(18, 4)
        )

      INSERT @ClientCurrentCredit
      SELECT ClientName,
             TotalWitdrawalAmount,
             CurrentCreditAmount
      FROM   (SELECT clientcredit.ID_Client     ID_Client,
                     client.Name                ClientName,
                     SUM(CreditAmount)          TotalWitdrawalAmount,
                     ISNULL(client.CurrentCreditAmount, 0) CurrentCreditAmount
              FROM   @_ClientCredits clientcredit
                     inner join tClient client
                             on client.ID = clientcredit.ID_Client
              WHERE  clientcredit.CreditAmount < 0
              GROUP  BY clientcredit.ID_Client,
                        client.Name,
                        client.CurrentCreditAmount) tbl
      WHERE  CurrentCreditAmount + TotalWitdrawalAmount < 0

      IF (SELECT COUNT(*)
          FROM   @ClientCurrentCredit) > 0
        BEGIN
            SET @message = 'The ff. clients are not able to consume credit:' +
                           + CHAR(13) + CHAR(10)

            SELECT @message = @message + ClientName + ' Rem: '
                              + FORMAT(CurrentCreditAmount, '#,#0.00')
                              + CHAR(13) + CHAR(10)
            FROM   @ClientCurrentCredit;

            THROW 50001, @message, 1;
        END;

  END

GO
