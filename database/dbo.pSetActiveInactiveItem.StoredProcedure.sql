﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pSetActiveInactiveItem]
(
	@record typActiveInactiveItem READONLY, 
	@ID_UserSession INT
) AS
BEGIN

    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';
	
	DECLARE @ID_User INT = 0;
	DECLARE @ID_Company INT = 0;

	SELECT @ID_User = userSession.ID_User,
		   @ID_Company = _user.ID_Company
	FROM dbo.tUserSession userSession 
	INNER JOIN vUser _user 
		on userSession.ID_User = _user.ID
	WHERE userSession.ID = @ID_UserSession;

	UPDATE tItem SET IsActive = record.IsActive
	FROM tItem item 
	INNER JOIN @record record 
		ON item.ID = record.ID_Item
	WHERE item.ID_Company = @ID_Company

    SELECT '_';

    SELECT @Success Success,
           @message message;
END

GO
