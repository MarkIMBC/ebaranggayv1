﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SqlQueryNotificationStoredProcedure-735be9a4-90ef-4b7e-93d2-ef585fdfe086] AS BEGIN BEGIN TRANSACTION; RECEIVE TOP(0) conversation_handle FROM [SqlQueryNotificationService-735be9a4-90ef-4b7e-93d2-ef585fdfe086]; IF (SELECT COUNT(*) FROM [SqlQueryNotificationService-735be9a4-90ef-4b7e-93d2-ef585fdfe086] WHERE message_type_name = 'http://schemas.microsoft.com/SQL/ServiceBroker/DialogTimer') > 0 BEGIN if ((SELECT COUNT(*) FROM sys.services WHERE name = 'SqlQueryNotificationService-735be9a4-90ef-4b7e-93d2-ef585fdfe086') > 0)   DROP SERVICE [SqlQueryNotificationService-735be9a4-90ef-4b7e-93d2-ef585fdfe086]; if (OBJECT_ID('SqlQueryNotificationService-735be9a4-90ef-4b7e-93d2-ef585fdfe086', 'SQ') IS NOT NULL)   DROP QUEUE [SqlQueryNotificationService-735be9a4-90ef-4b7e-93d2-ef585fdfe086]; DROP PROCEDURE [SqlQueryNotificationStoredProcedure-735be9a4-90ef-4b7e-93d2-ef585fdfe086]; END COMMIT TRANSACTION; END
GO
