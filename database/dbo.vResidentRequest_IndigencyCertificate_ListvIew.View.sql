﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vResidentRequest_IndigencyCertificate_ListvIew]
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.ID_Company,
         hed.IsActive,
         hed.ID_ResidentRequest,
         hed.Name_ResidentRequest,
         hed.DateCreated,
         hed.DateModified
  FROM   vResidentRequest_IndigencyCertificate hed
  WHERE  IsActive = 1 
GO
