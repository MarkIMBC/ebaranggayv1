﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 FUNCTION [dbo].[fGetPOSReceiptBillingInvoiceItemsLayout] (@Name_Item      VARCHAR(MAX),
                                                              @QuantityString VARCHAR(MAX),
                                                              @AmountString   VARCHAR(MAX))
RETURNS VARCHAR(MAX)
AS
  BEGIN
      Declare @layout VARCHAR(MAX) = '  
  <div class="display-block clearfix">                    
   <span class="bold">  
    /*Name_Item*/  
   </span>  
   <br/>                    
   <span class="float-left">                    
    &nbsp;&nbsp;&nbsp;/*QuantityString*/                   
   </span>                    
   <span class="float-right">  
    /*AmountString*/  
   </span>                    
  </div>  
'

      SET @layout = REPLACE(@layout, '/*Name_Item*/', @Name_Item)
      SET @layout = REPLACE(@layout, '/*QuantityString*/', @QuantityString)
      SET @layout = REPLACE(@layout, '/*AmountString*/', @AmountString)

      RETURN @layout
  END

GO
