﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pGetModel_Property]
    @Oid UNIQUEIDENTIFIER,
    @ID_Session INT = NULL
AS
    BEGIN
        SELECT  '_'

        DECLARE @ID_User INT ,
            @ID_Warehouse INT
        SELECT  @ID_User = ID_User ,
                @ID_Warehouse = ID_Warehouse
        FROM    tUserSession
        WHERE   ID = @ID_Session

        IF ( @Oid IS NULL )
            BEGIN
                SELECT  H.*
                FROM    ( SELECT    NULL AS [_] ,
                                    NULL AS [Oid] ,
                                    NULL AS [Name] ,
                                    1 AS [IsActive] ,
                                    NULL AS [Caption] ,
                                    NULL AS [ID_Model] ,
                                    NULL AS [ID_PropertyType] ,
                                    NULL AS [ID_CreatedBy] ,
                                    NULL AS [ID_LastModifiedBy] ,
                                    NULL AS [DateCreated] ,
                                    NULL AS [DateModified] ,
                                    NULL AS [ID_PropertyModel] ,
                                    NULL AS [ID_ModelProperty_Key]
                        ) H
                        LEFT JOIN tUser UC ON H.ID_CreatedBy = UC.ID
                        LEFT JOIN tUser UM ON H.ID_LastModifiedBy = UM.ID
            END
        ELSE
            BEGIN
                SELECT  H.*
                FROM    vModel_Property H
                WHERE   H.Oid = @Oid
            END
    END
GO
