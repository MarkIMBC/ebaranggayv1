﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vAvailableModules]
AS
SELECT '-1' AS Oid, 'All Modules' AS Name
UNION ALL
SELECT CAST(M.Oid AS VARCHAR(100)), ISNULL(M.Caption,M.Name) FROM _tModel M
INNER JOIN _tView VM ON M.Oid = VM.ID_Model
INNER JOIN _tNavigation N ON VM.Oid = N.ID_View
WHERE M.TableName NOT LIKE '_t%'

GO
