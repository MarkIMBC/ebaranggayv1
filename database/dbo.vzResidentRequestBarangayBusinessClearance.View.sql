﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vzResidentRequestBarangayBusinessClearance]
AS
  SELECT bz.Name               NAME,
         bz.ID                 ID,
         bz.Code,
         bz.BusinessName,
         bz.Address,
         bz.RegistrationNumber,
         bz.Name_ResidentRequest,
         bz.DateValidity,
         company.ID            ID_Company,
         company.ImageLogoLocationFilenamePath,
         company.Name          Name_Company,
         company.Address       Address_Company,
         company.ContactNumber ContactNumber_Company
  FROM   vResidentRequest_BarangayBusinessClearance bz
         INNER JOIN vCompany company
                 ON company.ID = bz.ID_Company

GO
