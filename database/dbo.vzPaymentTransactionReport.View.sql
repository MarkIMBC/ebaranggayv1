﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vzPaymentTransactionReport]
AS
  SELECT ptHed.ID,
         ptHed.Code,
         ptHed.Date,
         ptHed.CreatedBy,
         ptHed.ApprovedBy_Name_User,
         biHed.Code                                                        Code_BillingInvoice,
         biHed.BillingAddress,
         ISNULL(biHed.PatientNames, biHed.Name_Patient)                    Name_Patient,
         ptHed.Name_FilingStatus,
         ptHed.Name_PaymentMethod,
         ptHed.CheckNumber,
         ptHed.PayableAmount,
         ptHed.PaymentAmount,
         ptHed.ChangeAmount,
         ptHed.PayableAmount - ptHed.PaymentAmount                         RemainingAmount,
         FORMAT(ptHed.PaymentAmount, '#,#0.00')                            PaymentAmountString,
         biHed.ID_Company,
         LTRIM(RTRIM(ISNULL(biHed.Name_Client, biHed.WalkInCustomerName))) Name_Client,
         company.ImageLogoLocationFilenamePath,
         company.Name                                                      Name_Company,
         company.Address                                                   Address_Company
  FROM   dbo.vPaymentTransaction ptHed
         INNER JOIN dbo.vBillingInvoice biHed
                 ON biHed.ID = ptHed.ID_BillingInvoice
         LEFT JOIN dbo.vCompany company
                ON company.ID = ptHed.ID_Company; 
GO
