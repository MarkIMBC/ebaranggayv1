﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      
Create PROC [dbo].[pDelete_Patient_DentalExamination_Image](@ID_Patient_DentalExamination_Image int)        
AS        

	BEGIN TRANSACTION;   

	Declare @Success bit = 0

	BEGIN TRY       

		Delete tPatient_DentalExamination_Image where ID = @ID_Patient_DentalExamination_Image

		COMMIT TRANSACTION;   
		
		SET @Success = 1
        
	END TRY        
	BEGIN CATCH        
        
		ROLLBACK TRANSACTION;        
		
        SET @Success = 0
	END CATCH;   

	SELECT '_';    
        
	SELECT CONVERT(BIT, @Success) Success;    

GO
