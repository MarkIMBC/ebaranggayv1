﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_vListViewLookUp]
AS
SELECT Oid ID,
       Oid,
       Name
FROM _tListView;
GO
