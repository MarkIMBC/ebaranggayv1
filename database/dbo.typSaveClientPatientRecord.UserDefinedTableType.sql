﻿CREATE TYPE [dbo].[typSaveClientPatientRecord] AS TABLE(
	[Name] [varchar](250) NULL,
	[Email] [varchar](250) NULL,
	[ContactNumber] [varchar](250) NULL,
	[ContactNumber2] [varchar](250) NULL,
	[Address] [varchar](250) NULL,
	[Comment] [varchar](250) NULL,
	[Name_Patient] [varchar](250) NULL,
	[Species_Patient] [varchar](250) NULL,
	[ID_Gender_Patient] [int] NULL
)
GO
