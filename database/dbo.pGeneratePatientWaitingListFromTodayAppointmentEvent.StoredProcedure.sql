﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pGeneratePatientWaitingListFromTodayAppointmentEvent]  
AS  
    DECLARE @AppointmentIDs_Patient typIntList  
    DECLARE @WaitingList_Patient typIntList  
    DECLARE @OudatedWaiting_PatientWaitingList typIntList  
    DECLARE @Waiting_ID_FilingStatus INT = 8  
    DECLARE @Ongoing_ID_FilingStatus INT = 9  
    DECLARE @Cancelled_ID_FilingStatus INT = 4  
  
    INSERT @OudatedWaiting_PatientWaitingList  
    select DIstinct ID  
    FROm   vPatientWaitingList  
    where  CONVERT(Date, DateCreated) < CONVERT(Date, GETDATE())  
           and ISNULL(ID_Patient, 0) <> 0  
           AND WaitingStatus_ID_FilingStatus IN ( @Waiting_ID_FilingStatus, @Ongoing_ID_FilingStatus )  
  
    INSERT @AppointmentIDs_Patient  
    select DIstinct ID_Patient  
    FROm   vAppointmentEvent  
    where  CONVERT(Date, DateStart) = CONVERT(Date, GETDATE())  
           and ISNULL(ID_Patient, 0) <> 0  
  
    INSERT @WaitingList_Patient  
    select DIstinct ID_Patient  
    FROm   vPatientWaitingList  
    where  CONVERT(Date, DateCreated) = CONVERT(Date, GETDATE())  
           and ISNULL(ID_Patient, 0) <> 0  
  
    Delete FROM @AppointmentIDs_Patient  
    WHERE  ID IN (SELECT ID  
                  FROM   @WaitingList_Patient)  
  
    Update tPatient  
    set    WaitingStatus_ID_FilingStatus = NULL  
    FROM   tPatient patient  
           inner join @OudatedWaiting_PatientWaitingList outdate  
                   on patient.ID = outdate.ID  
  
    exec pAddPatientToWaitingList  
      @AppointmentIDs_Patient,  
      23926  
  
    exec pUpdatePatientToWaitingListStatus  
      @OudatedWaiting_PatientWaitingList,  
      @Cancelled_ID_FilingStatus,  
      23926   
GO
