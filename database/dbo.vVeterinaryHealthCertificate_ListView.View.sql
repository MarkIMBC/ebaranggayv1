﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vVeterinaryHealthCertificate_ListView]
AS
  SELECT ID,
         Date,
         Code,
         ID_Client,
		 Name_Client,
         ID_Patient,
		 Name_Patient,
		 AttendingPhysician_ID_Employee,
		 AttendingPhysician_Name_Employee,
		 ID_FilingStatus,
		 Name_FilingStatus,
		 DestinationAddress,
		 ID_Company
  FROM   vVeterinaryHealthCertificate

GO
