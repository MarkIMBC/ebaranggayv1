﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pCheckCompanyTextBlastSMSSending](@ID_Company          INT,
                                            @DateSending         DATE,
                                            @ID_TextBlast_Client INT)
AS
    DECLARE @IsAllowedToSendSMS BIT = 0
    DECLARE @MaxSMSCountPerDay INT = 0
    DECLARE @TotalSMSCount INT = 0
    DECLARE @NOTAllowed_TextBlastCount INT = 0

    SELECT @MaxSMSCountPerDay = MaxSMSCountPerDay
    FROM   tCompany_SMSSetting
    WHERE  ID_Company = @ID_Company

    SELECT @TotalSMSCount = TotalSMSCount
    FROM   vSMSCountPerCompany_Patient_SOAP_Plan
    WHERE  ID_Company = @ID_Company
           AND CONVERT(Date, DateSent) = CONVERT(Date, @DateSending)

    SET @IsAllowedToSendSMS = CASE
                                WHEN ( @TotalSMSCount < @MaxSMSCountPerDay ) THEN 1
                                ELSE 0
                              END

    IF( @IsAllowedToSendSMS = 1 )
      BEGIN
          SELECT @NOTAllowed_TextBlastCount = COUNT(*)
          FROM   tTextBlast_Client textBlastClient
                 INNER JOIN tTextBlast textBlast
                         ON textBlastClient.ID_TextBlast = textBlast.ID
          WHERE  ID_TextBlast = @ID_TextBlast_Client
                 AND ID_FilingStatus NOT IN ( 3 )

          SET @IsAllowedToSendSMS = CASE
                                      WHEN ( @NOTAllowed_TextBlastCount = 0 ) THEN 1
                                      ELSE 0
                                    END
      END

    SELECT '_'

    SELECT @ID_Company     ID_Company,
           @DateSending    DateSending,
           Convert(BIT, 1) IsAllowedToSendSMS,
           0               TotalSMSCount,
           0               MaxSMSCountPerDay

GO
