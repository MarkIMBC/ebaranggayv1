﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[_vView]
AS
SELECT Oid,
       Code,
       Name,
       IsActive,
       ID_ListView,
       ID_Report,
       ID_Dashboard,
       ID_ViewType,
       CustomViewPath,
       ControllerPath,
       Comment,
       DateCreated,
       ID_CreatedBy,
       ID_LastModifiedBy,
       DateModified,
       ID_Model,
       DataSource
FROM _tView;
GO
