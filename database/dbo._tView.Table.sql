﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[_tView](
	[Oid] [uniqueidentifier] NOT NULL,
	[Code] [varchar](50) NULL,
	[Name] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[ID_ListView] [uniqueidentifier] NULL,
	[ID_Report] [uniqueidentifier] NULL,
	[ID_Dashboard] [uniqueidentifier] NULL,
	[ID_ViewType] [int] NULL,
	[CustomViewPath] [varchar](500) NULL,
	[ControllerPath] [varchar](300) NULL,
	[Comment] [varchar](200) NULL,
	[DateCreated] [datetime] NULL,
	[ID_CreatedBy] [int] NULL,
	[ID_LastModifiedBy] [int] NULL,
	[DateModified] [datetime] NULL,
	[ID_Model] [uniqueidentifier] NULL,
	[DataSource] [varchar](300) NULL,
 CONSTRAINT [PK__tView] PRIMARY KEY CLUSTERED 
(
	[Oid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[_tView] ADD  CONSTRAINT [DF__tView_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[_tView]  WITH NOCHECK ADD  CONSTRAINT [FK_tView_ID_CreatedBy] FOREIGN KEY([ID_CreatedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[_tView] CHECK CONSTRAINT [FK_tView_ID_CreatedBy]
GO
ALTER TABLE [dbo].[_tView]  WITH NOCHECK ADD  CONSTRAINT [FK_tView_ID_LastModifiedBy] FOREIGN KEY([ID_LastModifiedBy])
REFERENCES [dbo].[tUser] ([ID])
GO
ALTER TABLE [dbo].[_tView] CHECK CONSTRAINT [FK_tView_ID_LastModifiedBy]
GO
ALTER TABLE [dbo].[_tView]  WITH NOCHECK ADD  CONSTRAINT [FK_tView_ID_Model] FOREIGN KEY([ID_Model])
REFERENCES [dbo].[_tModel] ([Oid])
GO
ALTER TABLE [dbo].[_tView] CHECK CONSTRAINT [FK_tView_ID_Model]
GO
