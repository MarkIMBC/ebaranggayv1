﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE 
 VIEW [dbo].[vPatient_Lodging_Listview]
AS
  SELECT ID,
         ID_Client,
         ID_Patient,
         Name_Client,
         Name_Patient,
         DateCheckIn,
         DateCheckOut,
		 HourCount,
		 PayableAmount,
		 Name_FilingStatus,
         ID_Company
  FROm   vPatient_Lodging

GO
