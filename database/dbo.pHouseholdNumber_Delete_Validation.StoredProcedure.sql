﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 PROC [dbo].[pHouseholdNumber_Delete_Validation] (@ID_HouseholdNumber INT,
                                                   @ID_UserSession     INT)
AS
  BEGIN
      DECLARE @message VARCHAR(MAX) = ''
      DECLARE @isValid BIT = 1
      DECLARE @ID_User Int
      DECLARE @ID_Company Int

      SELECT @ID_User = ID_User
      FROM   tUserSession
      WHERE  ID = @ID_UserSession

      SELECT @ID_Company = ID_Company
      FROm   vUser
      WHERE  ID = @ID_User

      BEGIN TRY
          DECLARE @Count_Resident INT = 0

          SELECT @Count_Resident = COUNT(*)
          FROM   tResident
          WHERE  ID_Company = @ID_Company
                 and ID_HouseholdNumber = @ID_HouseholdNumber
                 and IsActive = 1
                 and IsHeadofFamily = 1

          IF ( @Count_Resident ) > 0
            BEGIN
                SET @message = 'Household has '
                               + CONVERT(VARCHAR(MAX), @Count_Resident)
                               + ' resident'
                               + CASE
                                   WHEN @Count_Resident > 1 THEN 's'
                                   ELSE ''
                                 END
                               + ' need'
                               + CASE
                                   WHEN @Count_Resident = 1 THEN 's'
                                   ELSE ''
                                 END
                               + ' to be remove.';

                THROW 50001, @message, 1;
            END;
      END TRY
      BEGIN CATCH
          SET @message = ERROR_MESSAGE();
          SET @isValid = 0;
      END CATCH

      SELECT '_';

      SELECT @isValid isValid,
             @message message;
  END

GO
