﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tCompany_SMSSetting](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Company] [int] NULL,
	[MaxSMSCountPerDay] [int] NULL,
	[IsActive] [bit] NULL,
	[DateCreated] [datetime] NULL,
	[DateModified] [datetime] NULL,
 CONSTRAINT [PK_tCompany_SMSSetting] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tCompany_SMSSetting] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tCompany_SMSSetting]  WITH CHECK ADD  CONSTRAINT [FK_tCompany_SMSSetting_ID_Company] FOREIGN KEY([ID_Company])
REFERENCES [dbo].[tCompany] ([ID])
GO
ALTER TABLE [dbo].[tCompany_SMSSetting] CHECK CONSTRAINT [FK_tCompany_SMSSetting_ID_Company]
GO
