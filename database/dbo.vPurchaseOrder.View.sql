﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vPurchaseOrder]
AS
SELECT H.*,
       UC.Name AS CreatedBy_Name_User,
       UM.Name AS LastModifiedBy_Name_User,
       approvedUser.Name AS ApprovedBy_Name_User,
       cancelUser.Name AS CanceledBy_Name_User,
       status.Name Name_FilingStatus,
       servingStatus.Name ServingStatus_Name_FilingStatus,
       taxScheme.Name Name_TaxScheme,
       supplier.Name Name_Supplier,
       CONVERT(VARCHAR, H.Date, 0) DateString,
       CONVERT(VARCHAR, H.DateCreated, 0) DateCreatedString,
       CONVERT(VARCHAR, H.DateModified, 0) DateModifiedString,
       CONVERT(VARCHAR, H.DateApproved, 0) DateApprovedString,
       CONVERT(VARCHAR, H.DateCanceled, 0) DateCanceledString
FROM tPurchaseOrder H
    LEFT JOIN tUser UC
        ON H.ID_CreatedBy = UC.ID
    LEFT JOIN tUser UM
        ON H.ID_LastModifiedBy = UM.ID
    LEFT JOIN tSupplier supplier
        ON supplier.ID = H.ID_Supplier
    LEFT JOIN tUser approvedUser
        ON H.ID_ApprovedBy = approvedUser.ID
    LEFT JOIN tUser cancelUser
        ON H.ID_CanceledBy = cancelUser.ID
    LEFT JOIN tFilingStatus status
        ON H.ID_FilingStatus = status.ID
    LEFT JOIN tTaxScheme taxScheme
        ON H.ID_TaxScheme = taxScheme.ID
    LEFT JOIN tFilingStatus servingStatus
        ON H.ServingStatus_ID_FilingStatus = servingStatus.ID;

GO
