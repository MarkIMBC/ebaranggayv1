﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   
 VIEW [dbo].[vAnnouncement]
AS
  SELECT H.*,
         UC.NAME AS CreatedBy,
         UM.NAME AS LastModifiedBy
  FROM   tAnnouncement H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID

GO
