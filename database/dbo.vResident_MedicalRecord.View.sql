﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vResident_MedicalRecord]
AS
  SELECT H.*,
         UC.Name                AS CreatedBy,
         UM.Name                AS LastModifiedBy,
         medicalRecordType.Name Name_MedicalRecordType
  FROM   tResident_MedicalRecord H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tMedicalRecordType medicalRecordType
                ON H.ID_MedicalRecordType = medicalRecordType.ID

GO
