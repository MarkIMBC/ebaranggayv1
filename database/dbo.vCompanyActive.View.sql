﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create 
 VIEW [dbo].[vCompanyActive]
AS
  SELECT H.*
  FROM   [vCompany] H
  WHERE  h.IsActive = 1

GO
