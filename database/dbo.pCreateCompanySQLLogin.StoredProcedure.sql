﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pCreateCompanySQLLogin](@ID_Company INT)
AS
  BEGIN
      DECLARE @companyCode VARCHAR(MAX) = ''
      DECLARE @password VARCHAR(MAX) = 'sql123'
      DECLARE @databaseName VARCHAR(MAX) = db_Name()

      SELECT @companyCode = Code
      FROM   tCompany
      WHERE  ID = @ID_Company

      DECLARE @sql NVARCHAR(MAX) = '
	  USE master;
	  CREATE LOGIN [/*username*/] WITH PASSWORD=N''/*password*/'', DEFAULT_DATABASE=[/*databasename*/], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF;
	  ALTER LOGIN [/*username*/] ENABLE;
	  ALTER SERVER ROLE [sysadmin] ADD MEMBER [/*username*/];
	  ALTER SERVER ROLE [serveradmin] ADD MEMBER [/*username*/];
'

      SET @sql = Replace(@sql, '/*username*/', @companyCode)
      SET @sql = Replace(@sql, '/*password*/', @password)
      SET @sql = Replace(@sql, '/*databasename*/', @databaseName);

      UPDATE tCompany
      SET    dbusername = @companyCode,
             dbpassword = @password
      WHERE  ID = @ID_Company

      EXEC (@sql)
  END

GO
