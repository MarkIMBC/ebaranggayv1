﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pCancelReceivingReport]
(
    @IDs_ReceivingReport typIntList READONLY,
    @ID_UserSession INT
)
AS
BEGIN

    DECLARE @Canceled_ID_FilingStatus INT = 4;

    DECLARE @Success BIT = 1;
    DECLARE @message VARCHAR(300) = '';

    BEGIN TRY

        DECLARE @ID_User INT = 0;
        DECLARE @IDs_PurchaseOrder typIntList;

        SELECT @ID_User = ID_User
        FROM dbo.tUserSession
        WHERE ID = @ID_UserSession;

        EXEC dbo.pCancelReceivingReport_validation @IDs_ReceivingReport,
                                                   @ID_UserSession;

        UPDATE dbo.tReceivingReport
        SET ID_FilingStatus = @Canceled_ID_FilingStatus,
            DateCanceled = GETDATE(),
            ID_CanceledBy = @ID_User
        FROM dbo.tReceivingReport bi
            INNER JOIN @IDs_ReceivingReport ids
                ON bi.ID = ids.ID;

        /*Inventory Trail */
        INSERT INTO tInventoryTrail
        (
            Code,
            ID_Company,
            DateCreated,
            ID_Item,
            Quantity,
            UnitPrice,
            ID_FilingStatus,
            Date
        )
        SELECT hed.Code,
               hed.ID_Company,
               hed.DateCreated,
               detail.ID_Item,
               0 - detail.Quantity,
               detail.UnitPrice,
               hed.ID_FilingStatus,
               hed.Date
        FROM dbo.tReceivingReport hed
            LEFT JOIN dbo.tReceivingReport_Detail detail
                ON hed.ID = detail.ID_ReceivingReport
            INNER JOIN @IDs_ReceivingReport ids
                ON hed.ID = ids.ID
        WHERE ISNULL(hed.ID_ApprovedBy, 0) <> 0;

        /* Update PO Serving Status */
        INSERT @IDs_PurchaseOrder
        (
            ID
        )
        SELECT DISTINCT
               rrHed.ID_PurchaseOrder
        FROM dbo.tReceivingReport rrHed
            INNER JOIN @IDs_ReceivingReport ids
                ON rrHed.ID = ids.ID;

        EXEC dbo.pUpdatePOServingStatus @IDs_PurchaseOrder;

        EXEC dbo.pUpdateItemCurrentInventory;

    END TRY
    BEGIN CATCH

        SET @message = ERROR_MESSAGE();
        SET @Success = 0;

    END CATCH;


    SELECT '_';

    SELECT @Success Success,
           @message message;

END;
GO
