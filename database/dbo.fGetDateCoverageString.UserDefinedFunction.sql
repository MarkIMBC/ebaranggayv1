﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   FUNCTION [dbo].[fGetDateCoverageString](@DateStart DateTime, @DateEnd DateTime, @IsShowTimeOnly Bit)
RETURNS VARCHAR(1000) AS
BEGIN 

	DECLARE @result VARCHAR(MAX) = ''

	IF FORMAT(@DateStart, 'yyyy-MM-dd') = FORMAT(@DateEnd, 'yyyy-MM-dd') 
	BEGIN

		IF @IsShowTimeOnly = 0
			SET @result = FORMAT(@DateStart, 'yyyy-MM-dd') + CHAR(10) + CHAR(13)

		IF(FORMAT(@DateStart, 'hh:mm tt') <> FORMAT(@DateEnd, 'hh:mm tt'))
		BEGIN
			SET @result = @result + ' ' + FORMAT(@DateStart, 'hh:mm tt')
			SET @result = @result + '  to ' 
			SET @result = @result + ' ' + FORMAT(@DateEnd, 'hh:mm tt')
		END
		ELSE
		BEGIN

			SET @result = @result + ' ' + FORMAT(@DateEnd, 'hh:mm tt')
		END
	END
	ELSE
	BEGIN 

		IF @IsShowTimeOnly = 0 
			SET @result = FORMAT(@DateStart, 'yyyy-MM-dd')

		SET @result = @result + ' ' + FORMAT(@DateStart, 'hh:mm tt')
		SET @result = @result + '  to ' + CHAR(10) + CHAR(13)
		SET @result = @result + FORMAT(@DateEnd, 'MM/dd/yyyy')
		SET @result = @result + ' ' + FORMAT(@DateEnd, 'hh:mm tt')
	END


	RETURN @result
END

GO
