﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROC [dbo].[_pChangeFullAccessPassword]
AS
BEGIN

	DECLARE @IDs_User typIntList

	INSERT @IDs_User
	SELECT ID_User
	FROM   tUser_Roles user_roles
		   INNER JOIN tUserRole role
				   ON user_roles.ID_UserRole = role.ID
	where  role.IsFullAccess = 1

	update tUser
	set    Password = LEFT(NEWID(), 4), DateModified = GETDATE()
	where  ID IN (SELECT ID
				  FROM   @IDs_User)

END
GO
