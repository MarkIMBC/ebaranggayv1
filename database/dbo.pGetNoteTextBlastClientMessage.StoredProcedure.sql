﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 PROC [dbo].[pGetNoteTextBlastClientMessage](@ID_TextBlast   INT,
                                          @iTextMo_Status INT)
as
  BEGIN
      DECLARE @DateSending DateTime = GETDATE()

      exec pGetSendTextBlastMessage
        @DateSending
  END

GO
