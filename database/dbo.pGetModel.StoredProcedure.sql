﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[pGetModel]
    @Oid UNIQUEIDENTIFIER = NULL
    --@ID_Session INT = NULL
AS
    BEGIN
        SELECT  '_' AS [_], '' AS ModelProperties

        --DECLARE @ID_User INT ,
        --    @ID_Warehouse INT
        --SELECT  @ID_User = ID_User ,
        --        @ID_Warehouse = ID_Warehouse
        --FROM    tUserSession
        --WHERE   ID = @ID_Session

        IF ( @Oid IS NULL )
            BEGIN
                SELECT  H.*
                FROM    ( SELECT    NULL AS [_] ,
                                    NULL AS [Oid] ,
                                    NULL AS [Name] ,
                                    1 AS [IsActive] ,
                                    NULL AS [Comment] ,
                                    NULL AS [TableName] ,
                                    NULL AS [DisplayName] ,
                                    NULL AS [Color1] ,
                                    NULL AS [Color2] ,
                                    NULL AS [Color3] ,
                                    NULL AS [IsReadOnly] ,
                                    NULL AS [ID_CreatedBy] ,
                                    NULL AS [ID_LastModifiedBy] ,
                                    NULL AS [DateCreated] ,
                                    NULL AS [DateModified]
                        ) H
                        LEFT JOIN tUser UC ON H.ID_CreatedBy = UC.ID
                        LEFT JOIN tUser UM ON H.ID_LastModifiedBy = UM.ID
            END
        ELSE
            BEGIN
                SELECT  H.*
                FROM    _vModel H
                WHERE   H.Oid = @Oid
            END
    END

	SELECT * FROM dbo.[_tModel_Property] WHERE ID_Model = @Oid
GO
