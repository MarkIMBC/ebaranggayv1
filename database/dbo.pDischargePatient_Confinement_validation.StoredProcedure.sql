﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pDischargePatient_Confinement_validation] (@IDs_Patient_Confinement typIntList READONLY,
                                                             @ID_UserSession          INT)
AS
  BEGIN
      DECLARE @Confined_ID_FilingStatus INT = 14;
      DECLARE @Cancelled_ID_FilingStatus INT = 4;
      DECLARE @Filed_ID_FilingStatus INT = 1;
      DECLARE @message VARCHAR(400) = '';
      DECLARE @ValidateNotConfined TABLE
        (
           Code              VARCHAR(30),
           Name_FilingStatus VARCHAR(30)
        );
      DECLARE @Count_ValidateNotConfined INT = 0;

      /* Validate Patient_Confinement Status is not Confined*/
      INSERT @ValidateNotConfined
             (Code,
              Name_FilingStatus)
      SELECT Code,
             Name_FilingStatus
      FROM   dbo.vPatient_Confinement bi
      WHERE  EXISTS (SELECT ID
                     FROM   @IDs_Patient_Confinement ids
                     WHERE  ids.ID = bi.ID)
             AND bi.ID_FilingStatus NOT IN ( @Confined_ID_FilingStatus );

      SELECT @Count_ValidateNotConfined = COUNT(*)
      FROM   @ValidateNotConfined;

      IF ( @Count_ValidateNotConfined > 0 )
        BEGIN
            SET @message = 'The following record'
                           + CASE
                               WHEN @Count_ValidateNotConfined > 1 THEN 's are'
                               ELSE ' is '
                             END
                           + 'not allowed to discharged:';

            SELECT @message = @message + CHAR(10) + Code + ' - '
                              + Name_FilingStatus
            FROM   @ValidateNotConfined;

            THROW 50001, @message, 1;
        END;

      /* Validate SOAP Status is not yet done.*/
      DECLARE @ConfinementSOAP TABLE
        (
           Code                           VARCHAR(MAX),
           Code_Patient_SOAP              VARCHAR(MAX),
           Name_FilingStatus_Patient_SOAP VARCHAR(MAX)
        )

      INSERT @ConfinementSOAP
      SELECT confinement.Code,
             soap.Code,
             soap.Name_FilingStatus
      FROM   vPatient_Confinement confinement
             INNER JOIN @IDs_Patient_Confinement ids
                     ON confinement.ID = ids.ID
             INNER JOIN vPatient_SOAP soap
                     ON confinement.ID = soap.ID_Patient_Confinement
      WHERE  soap.ID_FilingStatus = @Filed_ID_FilingStatus

      if(SELECT COUNT(*)
         FROM   @ConfinementSOAP) > 0
        BEGIN
            SET @message = 'The following record not allowed to discharged:';

            SELECT @message = @message + CHAR(13) + CHAR(10)
                              + Code_Patient_SOAP + ' (' +
                              + Name_FilingStatus_Patient_SOAP + ') '
            FROM   @ConfinementSOAP;

            THROW 50001, @message, 1;
        END
  END; 
GO
