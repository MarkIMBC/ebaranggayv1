﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vVeterinaryCertificate_ListView]
AS
  SELECT ID,
         Code,
		 ID_Company,
         ID_Client,
         Name_Client,
         ID_Patient,
         Name_Patient,
         AttendingPhysician_ID_Employee,
         AttendingPhysician_Name_Employee,
         DestinationAddress,
         ID_Item,
         Name_Item
  FROm   vVeterinaryCertificate

GO
