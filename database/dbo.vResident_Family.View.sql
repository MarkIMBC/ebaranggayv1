﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vResident_Family]
AS
  SELECT H.*,
         rel.Name AS Name_Relationship,
         res.Name AS Name_FamilyMember,
         UC.Name  AS CreatedBy,
         UM.Name  AS LastModifiedBy
  FROM   tResident_Family H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tFamilyRelationshipType rel
                ON rel.ID = H.ID_RelationshipType
         LEFT JOIN tResident res
                ON res.ID = H.ID_FamilyMember

GO
