﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vDispute_ListvIew]
AS
  SELECT hed.ID,
         hed.Name,
         hed.Comment,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified
  FROM   vDispute hed

GO
