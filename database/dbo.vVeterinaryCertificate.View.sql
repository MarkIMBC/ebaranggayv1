﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vVeterinaryCertificate]
AS
  SELECT H.*,
         UC.Name                        AS CreatedBy,
         UM.Name                        AS LastModifiedBy,
         client.Name                    Name_Client,
         patient.Name                   Name_Patient,
         attendingPhysicianEmloyee.Name AttendingPhysician_Name_Employee,
         item.Name                      Name_Item
  FROM   tVeterinaryCertificate H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN dbo.tClient client
                ON client.ID = H.ID_Client
         LEFT JOIN dbo.tPatient patient
                ON patient.ID = H.ID_Patient
         LEFT JOIN dbo.tEmployee attendingPhysicianEmloyee
                ON attendingPhysicianEmloyee.ID = H.AttendingPhysician_ID_Employee
         LEFT JOIN dbo.tItem item
                ON item.ID = H.ID_Item

GO
