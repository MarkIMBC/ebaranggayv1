﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SqlQueryNotificationStoredProcedure-0e88b4d7-a4e3-41e2-8b65-7f0ab2e8564c] AS BEGIN BEGIN TRANSACTION; RECEIVE TOP(0) conversation_handle FROM [SqlQueryNotificationService-0e88b4d7-a4e3-41e2-8b65-7f0ab2e8564c]; IF (SELECT COUNT(*) FROM [SqlQueryNotificationService-0e88b4d7-a4e3-41e2-8b65-7f0ab2e8564c] WHERE message_type_name = 'http://schemas.microsoft.com/SQL/ServiceBroker/DialogTimer') > 0 BEGIN if ((SELECT COUNT(*) FROM sys.services WHERE name = 'SqlQueryNotificationService-0e88b4d7-a4e3-41e2-8b65-7f0ab2e8564c') > 0)   DROP SERVICE [SqlQueryNotificationService-0e88b4d7-a4e3-41e2-8b65-7f0ab2e8564c]; if (OBJECT_ID('SqlQueryNotificationService-0e88b4d7-a4e3-41e2-8b65-7f0ab2e8564c', 'SQ') IS NOT NULL)   DROP QUEUE [SqlQueryNotificationService-0e88b4d7-a4e3-41e2-8b65-7f0ab2e8564c]; DROP PROCEDURE [SqlQueryNotificationStoredProcedure-0e88b4d7-a4e3-41e2-8b65-7f0ab2e8564c]; END COMMIT TRANSACTION; END
GO
