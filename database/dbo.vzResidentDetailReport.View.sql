﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vzResidentDetailReport]
AS
  SELECT hed.Name,
         hed.IsActive,
         hed.ID_Company,
         hed.ID_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.Name_Gender,
         hed.Name_Company,
         ISNULL(hed.IsTransient, 0)               IsTransient,
         ISNULL(hed.IsPermanent, 0)               IsPermanent,
         ISNULL(hed.IsMigrante, 0)                IsMigrante,
         ISNULL(hed.IsPregnant, 0)                IsPregnant,
         ISNULL(hed.IsVaccinated, 0)              IsVaccinated,
         company.ImageLogoLocationFilenamePath,
         CASE
           WHEN LEN(company.Address) > 0 THEN '' + company.Address
           ELSE ''
         END
         + CASE
             WHEN LEN(company.ContactNumber) > 0 THEN '<br>Contact #: ' + company.ContactNumber
             ELSE ''
           END
         + CASE
             WHEN LEN(company.Email) > 0 THEN ' Email: ' + company.Email
             ELSE ''
           END                                    HeaderInfo_Company,
         [dbo].[fGetOrdinalNumber] (GETDATE())    AS OrdinalNum,
         ISNULL(dbo.fGetIntAge(hed.DateBirth), 0) AS Age,
         hed.AssignedBHW_ID_Employee,
         hed.AssignedBHW_Name_Employee
  FROM   vResident hed
         LEFT JOIN dbo.vCompany company
                ON company.ID = hed.ID_Company
  WHERE  hed.IsActive = 1 
GO
