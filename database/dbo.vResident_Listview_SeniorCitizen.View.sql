﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  
 VIEW [dbo].[vResident_Listview_SeniorCitizen]
AS
  SELECT hed.ID,
         hed.Name,
         hed.ID_Gender,
         hed.Name_Gender,
         hed.ContactNumber,
         hed.Address,
         hed.ID_Company,
         hed.IsActive,
         hed.DateCreated,
         hed.DateModified,
         hed.IsVaccinated,
         company.Guid Guid_Company,
         redAge.Age
  FROM   vResident hed
         LEFT JOIN tCompany company
                on company.ID = hed.ID_Company
         LEFT JOIN vResidentAge redAge
                on redAge.ID_Resident = hed.ID
  WHERE  redAge.Age >= 60
         AND hed.IsActive = 1
         AND ISNULL(hed.IsDeceased, 0) = 0

GO
