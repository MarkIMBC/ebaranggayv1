﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE 
 PROC [dbo].[LoadEmployeeUserAccount] @EmployeeID INT = -1
AS
  BEGIN
      DECLARE @Username VARCHAR(MAX) = '';

	  SELECT @Username = Username
      FROM   tUser
      WHERE  ID_Employee = @EmployeeID

      SELECT '_'

	  SELECT @Username Username

  END

GO
