﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 FUNCTION [dbo].[fGetModelReportMenuItemsByCompany](@ID_Company INT,
                                                     @Name_Model VARCHAR(MAX))
RETURNS @table TABLE(
  label       varchar(MAX),
  Name_Report varchar(MAX),
  name        varchar(MAX))
as
  BEGIN
      INSERT @table
      select Name_ModelReport            label,
             Name_Report,
             'ModelReport-' + Oid_Report name
      FROM   vModelReport
      WHERE  ID_Company = @ID_Company
             AND Name_Model = @Name_Model

      IF(SELECT COUNT(*)
         FROM   @table) = 0
        BEGIN
            INSERT @table
            select Name_ModelReport            label,
                   Name_Report,
                   'ModelReport-' + Oid_Report name
            FROM   vModelReport
            WHERE  ID_Company = 1
                   AND Name_Model = @Name_Model
        END

      RETURN
  end

GO
