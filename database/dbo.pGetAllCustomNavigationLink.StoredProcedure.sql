﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[pGetAllCustomNavigationLink] (@ID_UserSession int = NULL)
as

	SELECT '_', 
	'' CustomNavigationLink

	SELECT Convert(BIT, 1) Success

	SELECT * FROM tCustomNavigationLink WHERE ISNULL(IsActive,0) = 1

GO
