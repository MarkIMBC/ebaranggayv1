﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 VIEW [dbo].[vCompanyActiveUser]
as
  SELECT user_.ID_Company,
         user_.Name_Company,
         user_.UserName,
         user_.Password
  FROm   vUser user_
         inner join vCompanyActive c
                 on user_.ID_Company = c.ID

GO
