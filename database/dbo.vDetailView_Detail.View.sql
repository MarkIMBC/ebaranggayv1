﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vDetailView_Detail]
AS
    SELECT  D.*,
			MP.Name + '(' + MP.Name + ')' AS ModelProperty
    FROM    dbo.[_tDetailView_Detail] D
	LEFT JOIN dbo.[_tModel_Property] MP ON D.ID_ModelProperty = MP.Oid
	LEFT JOIN dbo.[_tModel] M ON MP.ID_Model = M.Oid
GO
