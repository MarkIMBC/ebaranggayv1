﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[pImportPatient] (@records typImportPatient READONLY, @ID_UserSession INT)
AS
BEGIN

  DECLARE @ID_Company INT = 0;
  DECLARE @ID_User INT = 0;

  DECLARE @Success BIT = 1;
  DECLARE @message VARCHAR(300) = '';

  DECLARE @Old_patient_ids TABLE (
    Old_patient_id INT
  )


  SELECT
    @ID_User = ID_User
  FROM dbo.tUserSession
  WHERE ID = @ID_UserSession;

  SELECT
    @ID_Company = u.ID_Company
  FROM vUser u
  WHERE u.ID = @ID_User

  INSERT @Old_patient_ids (Old_patient_id)
    SELECT
      CONVERT(INT, Old_patient_id)
    FROM @records
    EXCEPT
    SELECT
      p.Old_patient_id
    FROM tPatient p
    WHERE p.ID_Company = @ID_Company


  DECLARE @FORimport AS TABLE (
    Name VARCHAR(MAX)
   ,Old_client_id INT
   ,Old_patient_code VARCHAR(MAX)
   ,Old_patient_id INT
   ,Species VARCHAR(MAX)
   ,Breed VARCHAR(MAX)
   ,Gender INT
   ,Color VARCHAR(MAX)
   ,Remarks VARCHAR(MAX)
   ,BirthDate DATETIME
   ,IsDeceased BIT
   ,IsNeutered BIT
  )

  INSERT @FORimport (Name, Old_client_id, Old_patient_code, Old_patient_id, Species, Breed, Gender, Color, Remarks, BirthDate, IsDeceased, IsNeutered)
    SELECT
      Name
     ,Old_client_id
     ,Old_patient_code
     ,Old_patient_id
     ,Species
     ,Breed
     ,CASE
        WHEN ISNULL(record.Gender, '') = 'Male' THEN 1
        ELSE CASE
            WHEN ISNULL(record.Gender, '') = 'Female' THEN 2
            ELSE 0
          END
      END
     ,Color
     ,Remarks
     ,BirthDate
     ,CASE
        WHEN ISNULL(record.IsDeceased, 0) = 1 THEN 1
        ELSE 0
      END
     ,CASE
        WHEN ISNULL(record.IsNeutered, 'No') = 'Yes' THEN 1
        ELSE 0
      END
    FROM @records record

  INSERT tPatient (ID_Company,
  ID_Client,
  Old_patient_id,
  Code,
  Name,
  IsNeutered,
  IsDeceased,
  Comment,
  Species,
  ID_Gender,
  IsActive,
  DateCreated,
  DateModified,
  ID_CreatedBy,
  ID_LastModifiedBy,
  FirstName,
  LastName,
  MiddleName,
  Email,
  FullAddress,
  ID_Country,
  ContactNumber)
    SELECT
      c.ID_Company
     ,c.ID
     ,record.Old_patient_id
     ,record.Old_patient_code
     ,record.Name
     ,record.IsDeceased
     ,record.IsNeutered
     ,record.Remarks
     ,record.Species + ' - ' + record.Breed
     ,record.Gender
     ,1
     ,GETDATE()
     ,GETDATE()
     ,1
     ,1
     ,NULL
     ,NULL
     ,NULL
     ,NULL
     ,NULL
     ,NULL
     ,NULL
    FROM tClient c
    INNER JOIN @FORimport record
      ON c.Old_client_id = CONVERT(INT, record.Old_client_id)
    WHERE c.ID_Company = @ID_Company
    AND CONVERT(INT, Old_patient_id) IN (SELECT
        CONVERT(INT, Old_patient_id)
      FROM @Old_patient_ids)

  UPDATE tPatient
  SET Name = record.Name
     ,IsNeutered = record.IsNeutered
     ,IsDeceased = record.IsDeceased
     ,Species = record.Species
     ,ID_Gender = record.Gender
     ,Comment = record.Remarks
     ,DateModified = GETDATE()
  FROM tPatient p
  INNER JOIN @FORimport record
    ON p.Old_patient_id = record.Old_patient_id
  WHERE p.ID_Company = @ID_Company

  SELECT
    '_';

  SELECT
    @Success Success
   ,@message message;

END

GO
