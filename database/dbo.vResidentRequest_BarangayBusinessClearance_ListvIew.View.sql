﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vResidentRequest_BarangayBusinessClearance_ListvIew]
AS
  SELECT ID,
         Name,
         RegistrationNumber,
         ImageCertificateofRegistration,
         ID_ResidentRequest,
         Name_ResidentRequest,
         ID_Company,
         IsActive,
         DateCreated,
         DateModified
  FROM   vResidentRequest_BarangayBusinessClearance
  WHERE  IsActive = 1

GO
