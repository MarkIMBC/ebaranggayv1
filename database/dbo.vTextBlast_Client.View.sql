﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vTextBlast_Client]
AS
  SELECT H.*,
         UC.Name AS CreatedBy,
         UM.Name AS LastModifiedBy,
		 client.Name AS Name_Client
  FROM   tTextBlast_Client H
         LEFT JOIN tUser UC
                ON H.ID_CreatedBy = UC.ID
         LEFT JOIN tUser UM
                ON H.ID_LastModifiedBy = UM.ID
         LEFT JOIN tClient client
                on H.ID_Client = client.ID 
GO
