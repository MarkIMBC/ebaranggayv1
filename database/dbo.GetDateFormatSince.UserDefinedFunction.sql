﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 FUNCTION [dbo].[GetDateFormatSince](@TempDate DATETIME)
RETURNS NVARCHAR(200)
AS
  BEGIN
      DECLARE @TimeFormat NVARCHAR(200)
      DECLARE @second INT
      DECLARE @Minutes INT
      DECLARE @Hours INT
      DECLARE @Days INT
      DECLARE @Months INT
      DECLARE @Years INT
      DECLARE @Date DATETIME = GETDATE()

      SET @Years = DATEDIFF(yy, @TempDate, @Date)
      SET @Months = DATEDIFF(mm, @TempDate, @Date)
      SET @Days = DATEDIFF(dd, @TempDate, @Date)
      SET @Hours = DATEDIFF(hh, @TempDate, @Date)
      SET @Minutes = DATEDIFF(mi, @TempDate, @Date)
      SET @Minutes = @Minutes - ( @Hours * 60 )
      SET @second = DATEDIFF(ss, @TempDate, @Date)
      SET @second = @second - ( @Minutes * 60 )

      IF @Years = 0
         AND @Months = 0
         AND @Days = 0
         AND @Hours = 0
         AND @Minutes = 0
         AND @second >= 0
        BEGIN
            SET @TimeFormat = 'a second ago'
        END
      else IF @Years = 0
         AND @Months = 0
         AND @Days = 0
         AND @Hours = 0
         AND @Minutes = 1
        BEGIN
            SET @TimeFormat = 'a minute ago'
        END
      else IF @Years = 0
         AND @Months = 0
         AND @Days = 0
         AND @Hours = 0
         AND @Minutes > 0
        BEGIN
            SET @TimeFormat = CONVERT(VARCHAR, @Minutes) + ' minutes ago'
        END
      ELSE IF @Years = 0
         AND @Months = 0
         AND @Days = 0
         AND @Hours = 1
        BEGIN
            SET @TimeFormat = 'a hour ago'
        END
      ELSE IF @Years = 0
         AND @Months = 0
         AND @Days = 0
         AND @Hours > 1
        BEGIN
            SET @TimeFormat = CONVERT(VARCHAR, @Hours) + ' hours ago'
        END
      ELSE IF @Years = 0
         AND @Months = 0
         AND @Days = 1
        BEGIN
            SET @TimeFormat = 'a day ago'
        END
      ELSE IF @Years = 0
         AND @Months = 0
         AND @Days > 1
        BEGIN
            SET @TimeFormat = CONVERT(VARCHAR, @Days) + ' days ago'
        END
      ELSE IF @Months = 1
        BEGIN
            SET @TimeFormat = 'a month ago'
        END
      ELSE IF @Months BETWEEN 2 and 11
        BEGIN
            SET @TimeFormat = CONVERT(VARCHAR, @Months) + ' months ago'
        END
      ELSE IF @Months = 12
        BEGIN
            SET @TimeFormat = 'a year ago'
        END
      ELSE IF @Months > 12
        BEGIN
            SET @TimeFormat = CONVERT(VARCHAR, @Years) + ' year ago'
        END

      RETURN @TimeFormat;
  END

GO
