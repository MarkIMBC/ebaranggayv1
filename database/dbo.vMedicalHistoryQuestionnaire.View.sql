﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vMedicalHistoryQuestionnaire]
AS
SELECT H.*,
       UC.Name AS CreatedBy,
       UM.Name AS LastModifiedBy,
       qType.Name AS Name_QuestionType
FROM tMedicalHistoryQuestionnaire H
    LEFT JOIN tUser UC
        ON H.ID_CreatedBy = UC.ID
    LEFT JOIN tUser UM
        ON H.ID_LastModifiedBy = UM.ID
    LEFT JOIN dbo.tQuestionType qType
        ON qType.ID = H.ID_QuestionType;
GO
