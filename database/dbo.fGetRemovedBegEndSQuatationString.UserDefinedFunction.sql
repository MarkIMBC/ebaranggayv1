﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE 
 FUNCTION [dbo].[fGetRemovedBegEndSQuatationString](@Value VARCHAR(MAX))
RETURNS VARCHAR(MAX)
  BEGIN
      SET @Value = ISNULL(@Value, '')

      IF( LEN(@Value) > 0 )
        BEGIN
            IF( LEFT(@Value, 1) = '''' )
              SET @Value = right(@Value, len(@Value) - 1)
        END

      IF( LEN(@Value) > 0 )
        BEGIN
            IF( right(@Value, 1) = '''' )
              SET @Value = LEFT(@Value, len(@Value) - 1)
        END

      RETURN @Value
  END

GO
