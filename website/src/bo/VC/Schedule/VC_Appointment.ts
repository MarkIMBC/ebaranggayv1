import { Appointment } from './../../APP_MODELS';
import { VC_BaseView } from 'src/bo/VC/Base/VC_BaseView';
import { APP_MODEL } from 'src/bo/APP_MODELS';
import { VC_Model } from 'src/bo/VC_Model.decorator';


@VC_Model({
    viewId: [
        "Appointment_DetailView",
        "Appointment_ListView"
    ]
  })
export class VC_Appointment extends VC_BaseView {
    
    public CurrentAppointment: Appointment;
    
    onCurrentObject_Load() : Promise<void> {
        var appointment = this.CurrentObject as Appointment;
        if ( appointment.ID == -1 ) {
           var info = this.ViewParams as CalendarInfo; 
            if (info) { 
                appointment.DateStart = info.startDate;
                appointment.DateEnd = info.endDate;
            }
        }
        this.CurrentAppointment = appointment;
        return super.onCurrentObject_Load();
    }


}

export interface CalendarInfo {
    startDate:Date;
    endDate:Date;
}