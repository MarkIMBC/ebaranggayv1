import { IAppSelectBoxOption } from './../../../utils/controls/appSelectBox/appSelectBox.component';
import { QueryList } from '@angular/core';
import { PurchaseOrder_Detail, UnitOfMeasure, FilingStatus, APP_MODEL } from './../../APP_MODELS';
import { AppDataTableComponent, DataTableColumn, DataTableOption, IRowValueChange, IDataRow } from './../../../utils/controls/appDataTable/appDataTable.component';
import { VC_BaseView } from 'src/bo/VC/Base/VC_BaseView';
import { ViewMenuItem } from 'src/app/View';
import { VC_Model, App_Control } from 'src/bo/VC_Model.decorator';
import { PurchaseOrder, Item } from 'src/bo/APP_MODELS'
import { isNullOrUndefined } from 'util';
import * as TS from 'linq-typescript';
import { ControlTypeEnum, FilingStatusEnum, TaxSchemeEnum, PurchaseOrder_Detail_DTO, Item_DTO, PurchaseOrder_DTO } from 'src/utils/controls/class_helper';
import { IControlValueChanged } from 'src/utils/controls/_base/baseAppControl';
import { IFormValidation } from 'src/utils/controls/appModal/appModal.component';

@VC_Model({
  viewId: "PurchaseOrder_DetailView"
})
export class VC_PurchaseOrder extends VC_BaseView { 

    PurchaseOrder: PurchaseOrder_DTO;

    @App_Control({ name: "PurchaseOrder_Detail" })
    detailGrid: AppDataTableComponent;

    UOMs: UnitOfMeasure[] = [];

    onViewMenus_Initialized(menuItems: ViewMenuItem[]): void { }

    onFormValue_Changed(evt: IControlValueChanged): Promise<void> {
        if (evt.name == "ID_TaxScheme") {
            this.compute();
        }
        return super.onFormValue_Changed(evt)
    }

    onForm_Validations() : Promise<IFormValidation[]> {
        var validations : IFormValidation[] = [];
        if ( this.PurchaseOrder.PurchaseOrder_Detail.length == 0 ) { 
            validations.push({
                message : "Please input Purchase Order Details"
            });
        }
        return Promise.resolve(validations);
    }

    onCurrentObject_Saving() : Promise<void> {
        if ( this.PurchaseOrder.ID_FilingStatus == FilingStatusEnum.Filed ) {
            this.PurchaseOrder.PurchaseOrder_Detail.forEach((d) => {
                d.Balance = d.Quantity;
            });
        };
        return Promise.resolve();
    }

    onCurrentObject_Load() : Promise<void> {
        this.PurchaseOrder = this.CurrentObject;
        if ( this.PurchaseOrder.ID == -1 ) {
            this.PurchaseOrder.ID_FilingStatus = FilingStatusEnum.Filed;
            this.PurchaseOrder.ID_TaxScheme = TaxSchemeEnum.TaxInclusive;
            this.PurchaseOrder.PurchaseOrder_Detail = [];
        };
        if ( this.PurchaseOrder.ID > 0 ) {
            if ( this.PurchaseOrder.ID_FilingStatus == FilingStatusEnum.Filed ) {
                this.addMenu({
                    label : "Submit",
                    icon: "mdi mdi-check-underline-circle green-text",
                    command: async () => { 
                        if ( await this.msgBox.confirm("Are you sure you want to submit?", "Purchase Order", "Yes", "No" ) != true ) return;
                        await this.dataService.saveObject(APP_MODEL.PURCHASEORDER,{
                            ID : this.PurchaseOrder.ID,
                            ID_FilingStatus : FilingStatusEnum.Pending
                        });
                        this.reload();
                    }
                })
            };

            if ( this.PurchaseOrder.ID_FilingStatus == FilingStatusEnum.Pending ) {
                this.addMenu({
                    label : "Approve",
                    icon : "mdi mdi-check-circle-outline",
                    command: async () => { 
                        if ( await this.msgBox.confirm("Are you sure you want to approve?", "Purchase Order", "Yes", "No" ) != true ) return;
                        await this.dataService.execSP("pPurchaseOrder_Approve", {
                            ID : this.PurchaseOrder.ID
                        })
                        this.reload();
                    }
                });

                this.addMenu({
                    label : "Cancel Document",
                    icon : "mdi mdi-cancel red-text",
                    command: async () => { 
                        if ( await this.msgBox.confirm("Are you sure you want to cancel?", "Purchase Order", "Yes", "No" ) != true ) return;
                        await this.dataService.execSP("pPurchaseOrder_Cancel", {
                            ID : this.PurchaseOrder.ID
                        })
                        this.reload();
                    }
                });
                this.appForm.setFormDisabled(true);
            } else if ( [FilingStatusEnum.Cancelled ,FilingStatusEnum.Approved ].includes(this.PurchaseOrder.ID_FilingStatus) == true ) {
                this.appForm.setFormDisabled(true);
            }
        }
        return Promise.resolve();
    }
    
    async ngAfterViewInit(): Promise<void> {    
        this.detailGrid.newRowEditing = false;
        this.detailGrid.option.onRow_ValueChange = (det: IRowValueChange) => { 
            this.compute();
        };
        this.detailGrid.option.onColumns_Initialized = (cols: DataTableColumn[]) => { 
            var EditableColumns : string[] = ["ID_Item","Quantity", "ID_UOM", "UnitCost", "DiscountAmount"];
            cols.forEach((c) => { 
                if ( EditableColumns.includes(c.dataField) == true ) {
                    c.allowEdit = true;
                }
                if ( c.dataField == "ID_Item") {
                    c.displayDataField = "Name"; 
                };
                if ( c.dataField == "ID_UOM" ) {
                    c.editorType = ControlTypeEnum.SelectBox;
                    c.displayDataField = "UOM";
                    c.onEditorOption_Initialized = (row:IDataRow, opt: IAppSelectBoxOption ) => {
                        opt.items = this.UOMs;
                    };
                    c.onColumn_ValueChange = (det: IRowValueChange) => { 
                       var currentUOM = det.itemSelected as UnitOfMeasure;
                       if ( currentUOM ) { 
                         ( det.itemKey as PurchaseOrder_Detail_DTO ).UOM = currentUOM.Name;
                       } else {
                         ( det.itemKey as PurchaseOrder_Detail_DTO ).UOM = null;
                       }
                    }
                }
            });
        };
        this.UOMs = await this.dataService.query<UnitOfMeasure>(this.encrypt("SELECT * FROM tUnitOfMeasure WHERE isActive = 1"));
        //console.log("UOMs--->",this.UOMs)
        return Promise.resolve();
    }

    onDetail_ViewMenus_Initialized(name:string, viewMenuItems: ViewMenuItem[]) : Promise<void> {
        viewMenuItems.push({
            label : "Add Items",
            icon : "mdi mdi-plus",
            IsVisible: () => { 
                if  ( this.PurchaseOrder ) {
                    if (this.PurchaseOrder.ID_FilingStatus == FilingStatusEnum.Filed ) return true;
                }
                return false;
            },
            command: async () => { 

                var items = await this.lvModal.ListLookUp<Item_DTO>({
                    sql :  "SELECT * FROM vItem",
                    onGridOption_Initialized : (gridOption:DataTableOption) => void {  },
                    title : "Select Items..."
                });

                if ( items.length == 0 ) return;
                
                var newDetails: PurchaseOrder_Detail_DTO[] = [];
                
                items.forEach((i) => {

                    var uom = this.UOMs.find(d => d.ID == 1);
                    
                    newDetails.push({
                        ID_Item : i.ID,
                        Name: i.Name,
                        Quantity : 0,
                        ID_UOM : uom.ID,
                        UOM : uom.Name,
                        UnitCost : 0.00,
                        GrossAmount : 0.00,
                        VATAmount : 0.00,
                        DiscountAmount : 0.00,
                        NetAmount : 0.00
                    });
                });

                if ( isNullOrUndefined(this.PurchaseOrder.PurchaseOrder_Detail) ) this.PurchaseOrder.PurchaseOrder_Detail = [];
                
                this.PurchaseOrder.PurchaseOrder_Detail = this.PurchaseOrder.PurchaseOrder_Detail.concat(newDetails);
                
                this.compute();
            }
        })
        return Promise.resolve();
    };

    compute(): void { 
        var Details = this.PurchaseOrder.PurchaseOrder_Detail;
        var taxScheme: TaxSchemeEnum = this.PurchaseOrder.ID_TaxScheme;
        var TotalQuantity:number = 0;
        Details.forEach(d => { 
            var TotalAmount = d.Quantity * d.UnitCost;
            d.VATAmount = ( TotalAmount / 1.12 ) * 0.12;
            if ( taxScheme == TaxSchemeEnum.ZeroRated ) {
                d.VATAmount = 0;
            } else if ( taxScheme == TaxSchemeEnum.TaxExclusive ) {
                TotalAmount = TotalAmount + d.VATAmount;
            };
            d.GrossAmount = TotalAmount;
            d.NetAmount = d.GrossAmount - d.DiscountAmount;
            TotalQuantity += Number.parseFloat(d.Quantity + "");
        });
        var TotalVATAmount:number = new TS.List(Details).sum(d=>d.VATAmount ?? 0);
        var TotalGrossAmount:number = new TS.List(Details).sum(d=>d.GrossAmount ?? 0);
        var TotalDiscountAmount: number = new TS.List(Details).sum(d=>d.DiscountAmount ?? 0);;
        var TotalNetAmount: number = new TS.List(Details).sum(d=>d.NetAmount ?? 0);
        this.PurchaseOrder.TotalQuantity = TotalQuantity;
        this.PurchaseOrder.TotalVatAmount = TotalVATAmount;
        this.PurchaseOrder.TotalGrossAmount = TotalGrossAmount;
        this.PurchaseOrder.TotalNetAmount = TotalNetAmount;
    }
}
