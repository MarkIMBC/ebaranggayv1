import { QueryList } from '@angular/core';
import { AppTextBoxComponent } from '../../utils/controls/appTextBox/appTextBox.component';
import { User, View, DetailView_Detail } from '../APP_MODELS';
import { APP_MODEL } from 'src/bo/APP_MODELS';
import { VC_BaseView } from './Base/VC_BaseView';
import { VC_Model, App_Control } from 'src/bo/VC_Model.decorator';
import { PropertyTypeEnum, ControlTypeEnum } from 'src/utils/controls/class_helper';

@VC_Model({
  viewId: [
      "View_DetailView",
      "View_ListView"
  ]
})
export class VC_View extends VC_BaseView {

  ngAfterViewInit(): Promise<void> {

    return Promise.resolve();
  }

  async onFilterControls_Initialized(details: DetailView_Detail[]): Promise<void> {
      details.push(
        {
          Name: "Name",
          Caption: "Name",
          ID_PropertyType: PropertyTypeEnum.String
        }, 
        {
          Name: "Caption",
          Caption: "Caption",
          ID_PropertyType: PropertyTypeEnum.String
        } 
      );
  }
}
