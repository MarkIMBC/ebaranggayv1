import { VC_Model, App_Control } from "../VC_Model.decorator";
import { VC_BaseView } from "./Base/VC_BaseView";
import {
  AppDataTableComponent,
  DataTableColumn,
  IDataRow,
  IRowValueChange,
} from "src/utils/controls/appDataTable/appDataTable.component";
import { ViewMenuItem } from "src/app/View";
import { Model, Item, Item_Supplier, DetailView_Detail } from "../APP_MODELS";
import { isNullOrUndefined } from "util";
import {
  ControlTypeEnum,
  FilingStatusEnum,
  PropertyTypeEnum,
  ReceiveInventory,
  ViewTypeEnum,
} from "src/utils/controls/class_helper";
import { IAppPickListOption } from "src/utils/controls";
import { Guid } from "guid-typescript";
import * as TS from "linq-typescript";
import { MenuItem } from "primeng/api";

@VC_Model({
  viewId: ["Item_DetailView"],
})
export class VC_ItemView extends VC_BaseView {
  @App_Control({ name: "Item_Supplier" })
  Item_SupplierDetailGrid: AppDataTableComponent;

  onDetail_ViewMenus_Initialized(
    name: string,
    detail: ViewMenuItem[]
  ): Promise<void> {
    detail.push({
      label: "Add Supplier",
      command: () => {
        alert("aasqsdqwe");
      },
      icon: "mdi mdi-plus",
    });

    return Promise.resolve();
  }

  ngAfterViewInit(): Promise<void> {
    if (this.ViewType == ViewTypeEnum.DetailView) {
      this.Item_SupplierDetailGrid.option.multiSelect = false;
      this.Item_SupplierDetailGrid.option.onColumns_Initialized = (
        cols: DataTableColumn[]
      ) => {
        var editableColumns: string[] = ["ID_Supplier"];
        cols.forEach((c) => {
          if (c.dataField == "ID_Supplier") {
            c.caption = "Supplier";
            c.editorType = ControlTypeEnum.ListBox;
          }
        });
      };
    }

    return Promise.resolve();
  }

  async onCurrentObject_Load(): Promise<void> {
    var item: Item = this.CurrentObject;
    var userSession = this.userAuth.getDecodedToken();

    var sql = `SELECT TOP 1 dbo.fGetItemCurrentInventoryCount(${item.ID}) CurrentInventoryCount FROM tItem`;

    sql = this.encrypt(sql);

    var result = await this.dataService.query<any>(sql);

    if (result.length > 0) {
      this.appForm.CurrentObject.CurrentInventoryCount =
        result[0].CurrentInventoryCount;
    } else {
      this.appForm.CurrentObject.CurrentInventoryCount = null;
    }

    this.addMenu({
      label: "Add Inventory",
      icon: "mdi mdi-houzz ",
      command: async () => {
        var currentObject = {
          Quantity: null,
        };

        var title = `Add Inventory`;

        var details = [
          {
            Name: "Quantity",
            Caption: "Quantity",
            ID_ControlType: ControlTypeEnum.NumberBox,
            ID_LabelLocation: 2,
            IsRequired: true,
          },
        ];

        var options = {
          PositiveButton_Label: "Add",
        };

        this.dvFormSvc
          .open(currentObject, title, details, options)
          .then(async (res) => {
            if (res == false) return false;

            var obj = {
              ID_Item: item.ID,
              Quantity: res["Quantity"],
              DateExpired: res["DateExpired"],
              BatchNo: res["BatchNo"],
              ID_FilingStatus: FilingStatusEnum.Approved,
            };

            //await this.receiveInventory(obj);
            await this.reload();
          });
      },
    });

    return Promise.resolve();
  }

  async receiveInventory(record: ReceiveInventory): Promise<any> {
    var item: Item = this.CurrentObject;

    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.dataService.execSP(
        "pReceiveInventory",
        {
          record: [record],
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${item.Name} has been received successfully.`,
          `Received Done`
        );
        res(obj);
      } else {
        this.msgBox.error(obj.message, `Failed to Receive Inventory`);
        rej(obj);
      }
    });
  }

  async onFilterControls_Initialized(
    details: DetailView_Detail[]
  ): Promise<void> {
    details.push(
      {
        Name: "Name",
        Caption: "Item Name",
        ID_PropertyType: PropertyTypeEnum.String,
      },
      {
        Name: "ID_ItemType",
        Caption: "Item Type",
        ID_PropertyType: PropertyTypeEnum.Int,
        ID_ControlType: ControlTypeEnum.MultipleListBox,
        IsLoadData: true,
        DataSource: this.encrypt("SELECT ID, Name FROM tItemType"),
      },
      {
        Name: "ID_Category",
        Caption: "Category",
        ID_PropertyType: PropertyTypeEnum.Int,
        DataSource: this.encrypt("SELECT ID, Name FROM tItemCategory"),
        ID_ControlType: ControlTypeEnum.MultipleSelectBox,
      }
    );
  }
}
