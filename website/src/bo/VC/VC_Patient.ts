import { Patient } from './../APP_MODELS';
import { AppTextBoxComponent } from '../../utils/controls/appTextBox/appTextBox.component';
import { VC_BaseView } from './Base/VC_BaseView';
import { VC_Model, App_Control } from 'src/bo/VC_Model.decorator';
import { ViewTypeEnum } from 'src/utils/controls/class_helper';
import { DataTableOption, DataTableColumn, AppDataTableComponent } from 'src/utils/controls/appDataTable/appDataTable.component';
import { IControlValueChanged } from 'src/utils/controls/_base/baseAppControl';
import { async } from '@angular/core/testing';
import { IFormValidation } from 'src/utils/controls/appModal/appModal.component';
import { ValidationService } from 'src/utils/service/validation.service';
import { ViewMenuItem } from 'src/app/View';

@VC_Model({
  viewId: [
      "Patient_DetailView",
      "Patient_ListView"
  ]
})
export class VC_Patient_View extends VC_BaseView {

    @App_Control({ name: "Patient_DentalExamination" })
    dentalExaminationGrid: AppDataTableComponent;

    onDataTableOption_Initialized(opt:DataTableOption) : void {

    }

    onCurrentObject_Load() : Promise<void> {

        return Promise.resolve();
    }

    onDetail_ViewMenus_Initialized(name:string, viewMenuItems: ViewMenuItem[]) : Promise<void> {

        if(name == "Patient_DentalExamination"){

            var patient = this.CurrentObject as Patient;

            viewMenuItems.push({
                label:'Dental Examination',
                icon: "mdi mdi-check-underline-circle green-text",
                routerLink: `../../../../DentalExamination/${ patient.ID }`,
            })
        }

        return Promise.resolve();
    };

    onForm_Validations() : Promise<IFormValidation[]> {
        
        var patient = this.CurrentObject as Patient;
        var validations : IFormValidation[] = [];

        /* Validate Email Address */
        var isValidEmail = this.validationService.validateEmail(patient.Email);
        if(isValidEmail != true){

            validations.push({
                message: 'Invalid Email Address Format'
            });
        }
        return Promise.resolve(validations)
    };

    onFormValue_Changed(evt : IControlValueChanged) : Promise<void> { 
        var patient = this.CurrentObject as Patient;
        if ( ["FirstName","MiddleName","LastName"].includes(evt.name) == true ) { 

            patient.Name = ( patient.LastName ?? "" ) + ", " + ( patient.FirstName ?? "" )   + " " + ( patient.MiddleName ?? "" );
        }
        return Promise.resolve();
    }

    async ngAfterViewInit(): Promise<void> {

        if(this.ViewType == ViewTypeEnum.DetailView){

            this.dentalExaminationGrid.multiSelect = false;
            this.dentalExaminationGrid.showFilterRow = true;
        }

        return Promise.resolve();
    };

}
