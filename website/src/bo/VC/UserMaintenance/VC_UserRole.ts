import { Guid } from 'guid-typescript';
import { UserRole_Detail, Model, UserRole } from './../../APP_MODELS';
import { AppDataTableComponent, DataTableColumn, IRowValueChange, IDataRow } from './../../../utils/controls/appDataTable/appDataTable.component';
import { ViewMenuItem } from './../../../app/View/index';
import { APP_MODEL } from 'src/bo/APP_MODELS';
import { VC_Model, App_Control } from 'src/bo/VC_Model.decorator';
import { VC_BaseView } from '../Base/VC_BaseView';
import { DataTableOption } from 'src/utils/controls/appDataTable/appDataTable.component';
import { isNullOrUndefined } from 'util';
import { IAppPickListOption } from 'src/utils/controls';
import { ControlTypeEnum } from 'src/utils/controls/class_helper';
import * as TS from "linq-typescript";

@VC_Model({
    viewId:"UserRole_DetailView"
})
export class VC_User_Role_DetailView extends VC_BaseView {

    @App_Control({ name: "UserRole_Detail" })
    userRoleDetailGrid : AppDataTableComponent;

    onDetail_ViewMenus_Initialized(name:string, detail:ViewMenuItem[] ) : Promise<void> {
        if ( name == "UserRole_Detail") {
            detail.push({
                label: "Add Modules",
                command: () => { 

                },
                icon: "mdi mdi-plus"
            })
        }
        return Promise.resolve();
    }

    ngAfterViewInit(): Promise<void> {
        this.userRoleDetailGrid.option.multiSelect = false;
        this.userRoleDetailGrid.option.allowCell_Edit = (data: any, col: DataTableColumn): Promise<boolean> => {
            var userDetail = data as UserRole_Detail;
            if (col.dataField !== "ID_Model") {
            if (isNullOrUndefined(userDetail.ID_Model) == true) {
                    this.msgBox.showValidationBox("Please input module.");
                    return Promise.resolve(false);
                }
            }
            return Promise.resolve(true);
        };
        this.userRoleDetailGrid.option.onColumns_Initialized = (cols:DataTableColumn[])  => {
            var editableColumns : string[] = ["ID_Model","IsView","IsEdit", "IsActive", "IsCreate", "IsDelete", "IsDeny"];
            cols.forEach(c=>{
                if ( editableColumns.includes(c.dataField) ) {
                    c.allowEdit = true;
                }
                if ( c.dataField == "ID_Model" ) {
                    c.required = true;
                    c.editorType = ControlTypeEnum.ListBox;
                    c.displayText = (data: UserRole_Detail_DTO) => {
                        var _value = "";
                        if (data) _value = data.ModelName;
                        return _value;
                    };
                    c.onEditorOption_Initialized = (row:IDataRow, opt: IAppPickListOption): void => {
                        var UserRole = this.CurrentObject as UserRole_DTO;
                        opt.columns = [{
                            dataField: "Name",
                            caption: "Module Name"
                        }];
                        var CurrentOid = "'" + Guid.create().toString() + "'";  
                        if  ( isNullOrUndefined(row) != true ) {
                            var CurrentRole = row.value as UserRole_Detail_DTO;
                            CurrentOid = isNullOrUndefined(CurrentRole.ID_Model) != true ? "'" + CurrentRole.ID_Model + "'" : CurrentOid;
                        }
                        var ExistingModels:string = "'" + Guid.create().toString() + "'";
                        if (UserRole.UserRole_Detail) {
                          var Models =  new TS.List(UserRole.UserRole_Detail).select(d=>d.ID_Model).toArray();
                          if (Models.length > 0 ) {
                            ExistingModels = new TS.List(Models).select(s=>`'${s}'`).toArray().join(",");
                          }
                        }
                        //
                        //
                        //
                        opt.sourceKey =  this.encrypt(`SELECT Oid, Name FROM vAvailableModules WHERE ( Oid NOT IN (${ExistingModels}) OR Oid IN (${CurrentOid}) )`);
                        opt.propertyKey = "Oid";
                        opt.displayKey = "Name";
                    }
                    c.onColumn_ValueChange = (det: IRowValueChange) => {
                       var userRole_Detail =  det.itemKey as UserRole_Detail_DTO;
                       var SelectedModel = det.itemSelected as Model;
                       if (SelectedModel ) {
                            userRole_Detail.ModelName = SelectedModel.Name;
                            userRole_Detail.IsActive = true;
                       } else {
                            userRole_Detail.ModelName = null;
                       }
                    }
                }
            });
        }
        return Promise.resolve();
    }
    
}

class UserRole_Detail_DTO extends UserRole_Detail {
    ModelName:string;
}

class UserRole_DTO extends UserRole {

}