import { QueryList } from '@angular/core';
import { AppTextBoxComponent } from '../../utils/controls/appTextBox/appTextBox.component';
import { Position } from '../APP_MODELS';
import { APP_MODEL } from 'src/bo/APP_MODELS';
import { VC_BaseView } from './Base/VC_BaseView';
import { VC_Model, App_Control } from 'src/bo/VC_Model.decorator';
import { ViewTypeEnum } from 'src/utils/controls/class_helper';
import { DataTableOption, DataTableColumn } from 'src/utils/controls/appDataTable/appDataTable.component';
import { IFormValidation } from 'src/utils/controls/appModal/appModal.component';
import { isNullOrUndefined } from 'util';
import * as moment from 'moment';
import { ViewMenuItem } from './../../app/View/index';

@VC_Model({
  viewId: [
      "Position_DetailView",
      "Position_ListView"
  ]
})
export class VC_Position_View extends VC_BaseView {

  async onCurrentObject_Load() : Promise<any> {

 
  };

  async ngAfterViewInit(): Promise<void> {

    var postionRecord = this.CurrentObject as Position;
  
    return Promise.resolve();
  }
}

