import { QueryList } from '@angular/core';
import { AppTextBoxComponent } from '../../utils/controls/appTextBox/appTextBox.component';
import { User, Schedule } from '../APP_MODELS';
import { APP_MODEL } from 'src/bo/APP_MODELS';
import { VC_BaseView } from './Base/VC_BaseView';
import { VC_Model, App_Control } from 'src/bo/VC_Model.decorator';
import { ViewTypeEnum } from 'src/utils/controls/class_helper';
import { DataTableOption, DataTableColumn } from 'src/utils/controls/appDataTable/appDataTable.component';
import { IFormValidation } from 'src/utils/controls/appModal/appModal.component';
import { isNullOrUndefined } from 'util';
import * as moment from 'moment';
import { ViewMenuItem } from './../../app/VIew/index';
import { CalendarInfo } from './Schedule/VC_Appointment';
import { CalendarOptions, EventApi, DateSelectArg, EventClickArg, EventInput, Calendar, FullCalendarComponent, DatesSetArg } from '@fullcalendar/angular';

@VC_Model({
  viewId: [
      "Schedule_DetailView",
      "Schedule_ListView"
  ]
})
export class VC_Schedule extends VC_BaseView {

  public CurrentSchedule: Schedule;
    
  onForm_Validations() : Promise<IFormValidation[]> {

    var Sched = this.CurrentObject as Schedule;
    var validations : IFormValidation[] = [];

    if ( isNullOrUndefined(Sched.DateStart) != true && isNullOrUndefined(Sched.DateEnd) != true ) {

      var isValidDateRange = this.validationService.validateDateTimeRange(Sched.DateStart, Sched.DateEnd, true);

      if ( isValidDateRange != true ) {
        validations.push({
          message : "Invalid Date Schedule"
        })
      }
    };



    return Promise.resolve(validations)
  };

  onCurrentObject_Load() : Promise<void> {

    var schedule = this.CurrentObject as Schedule;
    if ( schedule.ID == -1 ) {
       var info = this.ViewParams ; 

        if (info) { 

            schedule.ID_Doctor = info.ID_Doctor;
            schedule.DateStart = info.DateStart;
            schedule.DateEnd = info.DateEnd;
        }
    }

    this.CurrentSchedule = schedule;

    if(isNullOrUndefined(this.CurrentObject.Schedule_PatientAppointment)){

      this.CurrentObject.Schedule_PatientAppointment = [];
    }
    
    return super.onCurrentObject_Load();
}

}
