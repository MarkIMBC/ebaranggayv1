import { PatientAppointment } from '../APP_MODELS';
import { AppTextBoxComponent } from '../../utils/controls/appTextBox/appTextBox.component';
import { VC_BaseView } from './Base/VC_BaseView';
import { VC_Model, App_Control } from 'src/bo/VC_Model.decorator';
import { ViewTypeEnum } from 'src/utils/controls/class_helper';
import { DataTableOption, DataTableColumn, IDataRow } from 'src/utils/controls/appDataTable/appDataTable.component';
import { IControlValueChanged } from 'src/utils/controls/_base/baseAppControl';
import { async } from '@angular/core/testing';
import { IFormValidation } from 'src/utils/controls/appModal/appModal.component';
import { ValidationService } from 'src/utils/service/validation.service';
import { isNullOrUndefined, isNull } from 'util';
import { VC_DocListView } from './Transaction/VC_DocListView';

@VC_Model({
  viewId: [
      "PatientAppointment_DetailView",
      "PatientAppointment_ListView"
  ]
})
export class VC_PatientAppointment extends VC_BaseView {

    ngAfterViewInit(): Promise<void> {

        if (this.ViewType == ViewTypeEnum.ListView){

            var index = -1;

            this.menuItems.shift();
      
        }
        return Promise.resolve();
    }
    
}
