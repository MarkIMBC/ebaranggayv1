import { DetailView_Detail } from './../../../utils/controls/class_helper';
import { UserAuthenticationService, TokenSessionFields } from './../../../app/AppServices/UserAuthentication.service';
import { AppFormComponent } from 'src/utils/controls/appForm/appForm.component';
import { MessageBoxService, IFormValidation } from 'src/utils/controls/appModal/appModal.component';
import { ViewMenuItem } from './../../../app/View/index';
import { AppDataTableComponent, DataTableOption } from 'src/utils/controls/appDataTable/appDataTable.component';
import { MenuItem } from 'primeng/api/menuitem';

import { DataService } from 'src/utils/service/data.service';
import { ValidationService } from 'src/utils/service/validation.service';
import { ViewTypeEnum } from 'src/utils/controls/class_helper';
import { ListViewModalService } from 'src/app/View/ListView/ListView.service';
import { DetailViewDialogService } from 'src/app/View/DetailViewDialog/DetailViewDialog.component';
import { IControlValueChanged, BaseAppControlOption } from 'src/utils/controls/_base/baseAppControl';
import * as TS from 'linq-typescript';
import { FormHelperDialogService } from 'src/app/View/FormHelperView/FormHelperView.component';

export abstract class VC_BaseView {

  currentUser: TokenSessionFields;

  ViewType : ViewTypeEnum;

  ViewName: string;

  ClassName: string;

  CurrentObject: any;

  menuItems: ViewMenuItem[] = [];

  appDataTable: AppDataTableComponent;

  ViewParams?: any;

  msgBox?: MessageBoxService;

  dataService:DataService;
  validationService: ValidationService;
  userAuth:  UserAuthenticationService;
  dvFormSvc: FormHelperDialogService;
  lvModal : ListViewModalService;
  dvModal : DetailViewDialogService;
  appForm : AppFormComponent;

  refreshMenus? : () => void = null;

  _reload?: (ID_CurrentObject?: string, params?: any) => void = null;
  //
  //
  //

  encrypt(str:string) : string {
    return this.dataService.crypter.encrypt(str);
  }

  addMenu(viewMenu:ViewMenuItem, unshift?:boolean) : void {
    if ( new TS.List(this.menuItems).where(d=>d.label == viewMenu.label ).toArray().length > 0 ) return;
    if ( unshift == true ) {
      this.menuItems.unshift(viewMenu);
    } else {
      this.menuItems.push(viewMenu);
    }
  }

  reload() : void { 
    if ( this.reload ) {
      this._reload(this.CurrentObject.ID + "");
    }
  }

  onCurrentObject_Load(): Promise<void> {
    return Promise.resolve();
  }
  
  ngAfterViewInit(): Promise<void> {
    return Promise.resolve();
  }

  onDataTableOption_Initialized(opt: DataTableOption): void { }

  onViewMenus_Initialized(viewMenuItems: ViewMenuItem[]): void { }

  onDetail_ViewMenus_Initialized(name:string, viewMenuItems: ViewMenuItem[]) : Promise<void> {
    return Promise.resolve();
  }

  onForm_Validations() : Promise<IFormValidation[]> {
    return Promise.resolve([]);
  }

  onFormValue_Changed(evt : IControlValueChanged) : Promise<void> { 
    return Promise.resolve();
  }

  async onControlOption_Initialized(name?: string, opt?: BaseAppControlOption): Promise<void> {
    return Promise.resolve();
   }

  onCurrentObject_Saving() : Promise<void> {
    return Promise.resolve();
  }

  onFilterControls_Initialized(detail: DetailView_Detail[]): void {
    
  }

  openDetailView(ModelName: string, ID_CurrentObject: string): void {}

}
