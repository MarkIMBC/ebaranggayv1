import { DetailView_Detail, PropertyTypeEnum } from './../../../utils/controls/class_helper';
import { VC_BaseView } from 'src/bo/VC/Base/VC_BaseView';
import { AppDataTableComponent, DataTableColumn, IDataRow, IRowValueChange } from 'src/utils/controls/appDataTable/appDataTable.component';
import { ViewMenuItem } from 'src/app/View';
import { isNullOrUndefined } from 'util';
import { ControlTypeEnum } from 'src/utils/controls/class_helper';
import { IAppPickListOption } from 'src/utils/controls';
import { Guid } from 'guid-typescript';
import * as TS from "linq-typescript";
import { VC_Model } from 'src/bo/VC_Model.decorator';


@VC_Model({
    viewId: "ItemMasterFile"
})
export class VC_ItemMasterFile extends VC_BaseView {

    async onFilterControls_Initialized(details: DetailView_Detail[]): Promise<void> {
        // details.push({
        //     Name: "Name",
        //     Caption: "Item Name",
        //     ID_PropertyType: PropertyTypeEnum.String
        // }, {
        //     Name: "ID_ItemType",
        //     Caption: "Item Type",
        //     ID_PropertyType: PropertyTypeEnum.Int,
        //     ID_ControlType: ControlTypeEnum.MultipleListBox,
        //     IsLoadData: true,
        //     DataSource: "SELECT ID, Name FROM tItemType"
        // }, {
        //     Name : "ID_Category",
        //     Caption : "Category",
        //     ID_PropertyType : PropertyTypeEnum.Int,
        //     DataSource: "SELECT * FROM tItemCategory",
        //     ID_ControlType: ControlTypeEnum.MultipleSelectBox,
        // }, {
        //     Name: "DateCreated",
        //     Caption : "Date Created",
        //     ID_PropertyType: PropertyTypeEnum.Date,
        // }, {
        //     Name : "IsActive",
        //     Caption : "Active",
        //     ID_PropertyType : PropertyTypeEnum.Bit
        // })

    }

}