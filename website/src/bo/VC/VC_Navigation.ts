import { QueryList } from '@angular/core';
import { AppTextBoxComponent } from '../../utils/controls/appTextBox/appTextBox.component';
import { User, Navigation, DetailView_Detail } from '../APP_MODELS';
import { APP_MODEL } from 'src/bo/APP_MODELS';
import { VC_BaseView } from './Base/VC_BaseView';
import { VC_Model, App_Control } from 'src/bo/VC_Model.decorator';
import { PropertyTypeEnum, ControlTypeEnum } from 'src/utils/controls/class_helper';

@VC_Model({
  viewId: [
      "Navigation_DetailView",
      "Navigation_ListView"
  ]
})
export class VC_Navigation extends VC_BaseView {

  ngAfterViewInit(): Promise<void> {

    return Promise.resolve();
  }

  async onFilterControls_Initialized(details: DetailView_Detail[]): Promise<void> {
      details.push(
        {
          Name: "Name",
          Caption: "Name",
          ID_PropertyType: PropertyTypeEnum.String
        }, 
        {
          Name: "Caption",
          Caption: "Caption",
          ID_PropertyType: PropertyTypeEnum.String
        },
        {
          Name : "ID_ViewType",
          Caption : "View Type",
          ID_PropertyType : PropertyTypeEnum.Int,
          DataSource: this.encrypt("SELECT ID, Name FROM _tViewType"),
          ID_ControlType: ControlTypeEnum.MultipleSelectBox,
        }
      );
  }
}
