import { IDataRow } from './../../../utils/controls/appDataTable/appDataTable.component';
import { IAppSelectBoxOption } from './../../../utils/controls/appSelectBox/appSelectBox.component';
import { VC_BaseView } from '../Base/VC_BaseView';
import { DataTableOption, DataTableColumn, TextAlignmentEnum } from 'src/utils/controls/appDataTable/appDataTable.component';
import { VC_Model } from 'src/bo/VC_Model.decorator';
import { APP_MODEL, DetailView_Detail } from 'src/bo/APP_MODELS';
import { ControlTypeEnum } from 'src/utils/controls/class_helper';
import { IAppPickListOption } from 'src/utils/controls';
import { isNullOrUndefined } from 'util';

@VC_Model({
    viewId: "DetailView_Detail_ListView"
})
export class VC_DetailView_Detail_ListView extends VC_BaseView {

    onDataTableOption_Initialized(opt: DataTableOption): void { 
        opt.actionRowItems = []
        var Oid_Control = this.ViewParams.Oid_Control;
        var Oid_DetailView = this.ViewParams.Oid_DetailView;
        var IsRootOnly = this.ViewParams.IsRootOnly == true ? true : false;
        opt.allowEditing = true;
        opt.remoteEditing = true;
        opt.Oid_Model = APP_MODEL.DETAILVIEW_DETAIL;
        opt.propertyKey = "Oid";
        opt.newRowEditing = true;
        if ( isNullOrUndefined(Oid_Control) == true ) Oid_Control = "-";
        opt.apiUrl = `Model/DetailControl/${Oid_Control ?? "-"}/${Oid_DetailView ?? "-"}/${IsRootOnly}`;
        opt.multiSelect = true;
        opt.actionRowItems = [
          {
            class: "btn-danger",
            icon: "mdi mdi-delete-forever",
            command: async (evt: any, item: DetailView_Detail) => {
              var res = await this.msgBox.confirm(
                "Are you sure you want to delete this item?",
                item.Caption,
                "Yes", "No"
              );
              if ( res != true ) return;
              await this.dataService.execSP('_pDelete_DetailView_Detail',{
                Oid : item.Oid
              })
              this.appDataTable.reloadData(true);
            },
          },
        ];
        opt.customizeColumns = (cols: DataTableColumn[]) => { 
            var _tempCols : DataTableColumn[] = [];
            var ColNames = ["Name", "IsActive", "ID_ModelProperty", "Caption", "SeqNo", 
                    "ID_ControlType", "ID_PropertyType", "ID_Tab", "ID_Section", "ColSpan", "ColCount", "GroupIndex", "IsReadOnly" ];
            cols.forEach(c => { 
                if ( c.dataField == "ID_Tab" ) c.displayDataField = "TabName";
                if ( c.dataField == "ID_Section" ) c.displayDataField = "SectionName";
                if ( c.dataField == "ID_ControlType" ) {
                    c.displayDataField = "ControlType";
                    c.editorType = ControlTypeEnum.SelectBox;
                    c.onEditorOption_Initialized = (row:IDataRow, opt: IAppSelectBoxOption) => { 
                        opt.items = [{ "ID": 17, "Name": "AutoComplete" }, { "ID": 4, "Name": "CheckBox" }, { "ID": 10, "Name": "ColorPicker" }, { "ID": 6, "Name": "DatePicker" }, { "ID": 5, "Name": "DateTimePicker" }, { "ID": 16, "Name": "FileUploader" }, { "ID": 15, "Name": "ImageBox" }, { "ID": 9, "Name": "ListBox" }, { "ID": 11, "Name": "ListView" }, { "ID": 3, "Name": "NumberBox" }, { "ID": 14, "Name": "RichTextBox" }, { "ID": 13, "Name": "Section" }, { "ID": 8, "Name": "SelectBox" }, { "ID": 12, "Name": "Tab" }, { "ID": 2, "Name": "TextArea" }, { "ID": 1, "Name": "TextBox" }, { "ID": 7, "Name": "TimePicker" }];
                        opt.propertyKey = "ID";
                        opt.displayKey = "Name";
                     
                    }
                }
                if ( c.dataField == "ID_PropertyType" ) {
                    c.displayDataField = "PropertyType";
                    c.editorType = ControlTypeEnum.SelectBox;
                    c.onEditorOption_Initialized = (row: IDataRow,opt: IAppSelectBoxOption) => { 
                        opt.items = [{ "ID": 1, "Name": "String" }, { "ID": 2, "Name": "Int" }, { "ID": 3, "Name": "Decimal" }, { "ID": 4, "Name": "Bit" }, { "ID": 5, "Name": "DateTime" }, { "ID": 6, "Name": "Date" }, { "ID": 7, "Name": "Time" }, { "ID": 8, "Name": "Uniqueidentifier" }, { "ID": 9, "Name": "Object" }, { "ID": 10, "Name": "List" }, { "ID": 11, "Name": "Color" }, { "ID": 12, "Name": "Image" }];
                    };
                }
                if ( c.dataField == "ID_Tab" ) {
                    c.editorType = ControlTypeEnum.ListBox;
                    c.onEditorOption_Initialized = (row:IDataRow,opt: IAppPickListOption) => { 
                        opt.apiUrl = `Model/GetTabs/${Oid_DetailView}`;
                        opt.propertyKey = "Oid";
                    }
                }
                if ( c.dataField == "ID_Section" ) {
                    c.editorType = ControlTypeEnum.ListBox;
                    c.onEditorOption_Initialized = (row:IDataRow, opt: IAppPickListOption) => { 
                        opt.apiUrl = `Model/GetSections/${Oid_DetailView}`;
                        opt.propertyKey = "Oid";
                    }
                }

                if ( c.dataField == "IsActive" || c.dataField == "IsReadOnly") {
                    c.textAlignment = TextAlignmentEnum.Center;
                }
                if (ColNames.includes(c.dataField)) {
                    //
                    //
                    //
                    c.allowEdit = true;
                    if ( c.dataField == "Name" ) {
                        c.fixed = true;
                        c.allowEdit = false;
                    };
                    _tempCols.push(c);
                }
                if (c.dataField == "ID_ModelProperty" ) {
                    c.allowEdit = false;
                    c.displayDataField = "ModelProperty";
                    c.caption = "Model Property"
                }       
            });
            var seqNo = 1;
            ColNames.forEach(s => { 
                var col = _tempCols.find(d => d.dataField == s);
                col.seqNo = ( seqNo++ ) * 100;
                seqNo++;
            });
            return _tempCols;
        };
    };

    async ngAfterViewInit(): Promise<void> {
        this.menuItems = [];
        var Oid_Control = this.ViewParams.Oid_Control;
        var control = await this.dataService.loadObject(APP_MODEL.DETAILVIEW_DETAIL,Oid_Control) as DetailView_Detail;
        this.CurrentObject = control;
        if ( control.ID_ControlType == ControlTypeEnum.Section || control.ID_ControlType == ControlTypeEnum.Tab ) {
            this.menuItems.push({
                label: "Add Section",
                icon: "mdi mdi-shape-square-plus",
                command: async () => {
                  var res = await this.dataService.execSP("_pAddGroupControl",{
                        ID_DetailView : control.ID_DetailView,
                        ID_Control : ControlTypeEnum.Section,
                        ID_Parent : Oid_Control
                    });
                this.appDataTable.reload(true);
                },
            });

            this.menuItems.push({
                label: "Add Tab",
                icon: "mdi mdi-tab-plus",
                command: async () => {
                  var res = await this.dataService.execSP("_pAddGroupControl",{
                        ID_DetailView : control.ID_DetailView,
                        ID_Control : ControlTypeEnum.Tab,
                        ID_Parent : Oid_Control
                    });
                this.appDataTable.reload(true);
                },
            });
        };
        return Promise.resolve();
    }

}