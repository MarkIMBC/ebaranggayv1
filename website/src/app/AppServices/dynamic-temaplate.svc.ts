import { AppDataTableComponent } from 'src/utils/controls/appDataTable/appDataTable.component';
import { ViewChild } from '@angular/core';
import { AppFormComponent } from './../../utils/controls/appForm/appForm.component';
import { AppImageBoxComponent } from './../../utils/controls/appImageBox/appImageBox.component';
import { MenuItem } from 'primeng/api/menuitem';
import { Injector, NgModuleRef, ComponentRef, OnInit, ViewChildren, QueryList } from '@angular/core';
import { SharedModule } from 'src/utils/shared.module';
import { CommonModule } from '@angular/common';
import { Compiler, Inject, Injectable, ModuleWithComponentFactories, NgModuleFactory, Type } from '@angular/core';
import { Component, NgModule } from '@angular/core';


import { DataService } from 'src/utils/service/data.service';

import { BaseAppControl } from 'src/utils/controls';
import { TabPanel, TabView } from 'primeng/tabview';
import { Fieldset } from 'primeng/fieldset';

import { CompilerFactory, CompilerOptions, COMPILER_OPTIONS, ViewEncapsulation, StaticProvider } from '@angular/core';
import { JitCompilerFactory } from '@angular/platform-browser-dynamic';

import { REGISTRY_VC_MODEL_CONTROLS } from 'src/bo/VC_Model.decorator';
import { VC_BaseView } from 'src/bo/VC/Base/VC_BaseView';
import { isNullOrUndefined } from 'util';



@Injectable({
  providedIn: 'root'
})
export class DynamicTemplateService {

  constructor(private _compiler: Compiler) { }

  public async createAndCompileComponents(_template: string, _injector: Injector, _m: NgModuleRef<any>): Promise<ComponentRef<any>> {
    let cls = class extends DetailViewHolder { }
    var component = Component({ template: _template })(cls);
    var components: any[] = [component];
    const examplesModule = NgModule({
      declarations: [components],
      exports: [components],
      entryComponents: [components],
      imports: [CommonModule, SharedModule]
    })(class TemplateExamplesModule {
    });

    var _compiledModule: ModuleWithComponentFactories<any> = await this._compiler.compileModuleAndAllComponentsAsync(examplesModule);
    return _compiledModule.ngModuleFactory.create(_injector).componentFactoryResolver.resolveComponentFactory(component).create(_injector, [], null, _m);
  }

}

export class DetailViewHolder implements OnInit {

  ds: DataService;

  CurrentObject: any = null;

  @ViewChild('appForm')
  appForm: AppFormComponent;

  vcModels: VC_BaseView[] = [];

  @ViewChildren(BaseAppControl)
  appControls: QueryList<BaseAppControl<any>>;

  @ViewChildren(TabPanel)
  tabPanel: QueryList<TabPanel>;

  @ViewChildren(TabView)
  tabViews: QueryList<TabView>;

  @ViewChildren(Fieldset)
  fieldSet: QueryList<Fieldset>;

  menuItems: MenuItem[] = [];

  @ViewChildren(AppImageBoxComponent)
  imageBoxes: QueryList<AppImageBoxComponent>;

  
  @ViewChildren(AppDataTableComponent)
  detailGrids: QueryList<AppDataTableComponent>;

  filterActiveIndex?: number = null;

  ngOnInit(): void { }

  onView_Initialzied: () => void = null;

  ngAfterViewInit(): void {

    if (this.appControls) {

      this.appControls.forEach(control => {
        
        this.vcModels.forEach((c: any) => {
          var classKeys = getKeys(c);
          classKeys.forEach((ck: string) => {
            var keys = ck.split('.');
            if (keys.length != 2) return;
            var clsName = keys[0];
            var propertName = keys[1];
            var map = REGISTRY_VC_MODEL_CONTROLS.get(clsName);
            var values = Array.from(map.values()).find(d => d == propertName);
            var controlName = map.get(control.name);
            if (values && controlName) {
              //console.log(propertName, control);
              if ( isNullOrUndefined(c[propertName]) == true) {
                c[propertName] = control;
              }
            }
          });
        })
      });
    };
    this.vcModels.forEach(async (c: VC_BaseView) => {
      c.menuItems = this.menuItems;
      c.appForm = this.appForm;
      c.onCurrentObject_Load();
      await c.ngAfterViewInit();
    });
    //
    //
    //
    if (!isNullOrUndefined(this.onView_Initialzied)) this.onView_Initialzied();
  }

}

const compilerOptions: CompilerOptions = {
  useJit: true,
  defaultEncapsulation: ViewEncapsulation.None
};

export function createCompiler(compilerFactory: CompilerFactory) {
  return compilerFactory.createCompiler([compilerOptions]);
}

export const RUNTIME_COMPILER_PROVIDERS: StaticProvider[] = [
  { provide: COMPILER_OPTIONS, useValue: compilerOptions, multi: true },
  { provide: CompilerFactory, useClass: JitCompilerFactory, deps: [COMPILER_OPTIONS] },
  { provide: Compiler, useFactory: createCompiler, deps: [CompilerFactory] }
];

function getKeys(obj) {
  var myKeys = [];//Object.keys(obj);
  var protoKeys: string[] = [];
  Object.keys(obj.constructor.prototype).forEach(d => {
    protoKeys.push(`${obj.constructor.name}.${d}`)
  });
  if (Object.getPrototypeOf(obj)) {
    protoKeys = protoKeys.concat(getKeys(Object.getPrototypeOf(obj)));
  }
  return [...new Set(myKeys.concat(protoKeys))].filter(c => c !== 'constructor');
}