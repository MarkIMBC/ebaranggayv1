import { MenuItem } from 'primeng/api/menuitem';
import { DataService } from './../../utils/service/data.service';
import { Injectable, Input, Directive } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class TabService {

  tabs: IAppTab[] = [];

  constructor(ds: DataService) { }

  addTab(menuItem:MenuItem) {
    this.tabs.push({
      id: menuItem.id,
      label: menuItem.label,
      isInit: false
    })
   }

  closeTab() { }

}


export interface IAppTab {
  id?: string;
  label?: string;
  Oid_Model?: string;
  ID_CurrentObject?: string;
  isInit?:boolean;
}


@Directive({
  selector: 'ngInit',
  exportAs: 'ngInit'
})
export class NgInitDirective {

  isCalled: boolean = false;

  @Input() ngInit: () => any;

  ngOnInit() {
    if (typeof this.ngInit === 'function') {
      this.ngInit();
    } else {
      // preventing re-evaluation (described below)
      throw 'something';
    }
  }
}