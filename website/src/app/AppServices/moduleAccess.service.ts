import { MenuItem } from 'primeng/api/menuitem';
import { DataService } from '../../utils/service/data.service';
import { Injectable, Input, Directive } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class ModuleAccessService {

  CurrentAccess: IModuleAccess = {
    ID_Model: null,
    IsView: false,
    IsCreate: false,
    IsEdit: false,
    IsDelete: false
  }

  set(access: IModuleAccess) {
    this.CurrentAccess = access;
  }

}


export interface IModuleAccess {
  ID_Model: string;
  IsView: boolean;
  IsCreate: boolean;
  IsEdit: boolean;
  IsDelete: boolean;
}
