import { Component, OnInit, Input } from '@angular/core';
import { Patient } from 'src/bo/APP_MODELS';

@Component({
  selector: 'app-patient-info-panel',
  templateUrl: './patient-info-panel.component.html',
  styleUrls: ['./patient-info-panel.component.less']
})
export class PatientInfoPanelComponent implements OnInit {

  constructor() { }

  @Input()
  patient: Patient;

  ngOnInit(): void {
  }

}
