import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientInfoPanelComponent } from './patient-info-panel.component';

describe('PatientInfoPanelComponent', () => {
  let component: PatientInfoPanelComponent;
  let fixture: ComponentFixture<PatientInfoPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientInfoPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientInfoPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
