import { Component, Input, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import { MenuItem } from "primeng/api";
import { TokenSessionFields, UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { DialogBoxComponent } from "src/app/dialog-box/dialog-box";
import { FormHelperDialogService } from "src/app/View/FormHelperView/FormHelperView.component";
import { AppointmentSchedule, APP_MODEL } from "src/bo/APP_MODELS";
import { IFormValidation, MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import {
  AppointmentSchedule_DTO,
  ControlTypeEnum,
  FilingStatusEnum,
} from "src/utils/controls/class_helper";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { isNullOrUndefined, isNumber } from "util";

@Component({
  selector: "app-appointmentschedule-step-status",
  templateUrl: "./appointmentschedule-step-status.component.html",
  styles: [
    `
      .ui-steps {
        height: 100px;
      }

      .ui-steps .ui-steps-item {
        width: 33.33%;
      }

      .ui-steps.steps-custom {
        margin-bottom: 30px;
      }

      .ui-steps.steps-custom .ui-steps-item .ui-menuitem-link {
        padding: 0 1em;
        overflow: visible;
      }

      .ui-steps.steps-custom .ui-steps-item .ui-steps-number {
        background-color: #0081c2;
        color: #ffffff;
        display: inline-block;
        width: 36px;
        border-radius: 50%;
        margin-top: -14px;
        margin-bottom: 10px;
      }

      .ui-steps.steps-custom .ui-steps-item .ui-steps-title {
        color: #555555;
      }

      .control-separator-bottom {
        margin-bottom: 11px;
      }
    `,
  ],
  encapsulation: ViewEncapsulation.None,
})
export class AppointmentscheduleStepStatusComponent implements OnInit {
  items: MenuItem[];
  activeIndex: number = 0;

  @ViewChild("onGoingDialogBox")
  onGoingDialogBoxCmp: DialogBoxComponent;

  @ViewChild("cancelDialogBox")
  cancelDialogBoxCmp: DialogBoxComponent;

  @Input()
  ID_AppointmentSchedule: number = null;

  currentUser: TokenSessionFields;

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    private dvFormSvc: FormHelperDialogService,
    protected cs: CrypterService
  ) {

    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  CurrentObject: AppointmentSchedule_DTO = new AppointmentSchedule_DTO();

  private _stepItem_Waiting: MenuItem = {
    id: FilingStatusEnum.Waiting + "",
    label: "Waiting",
    command: async (event: any) => {
      if (
        this.CurrentObject.AppointmentStatus_ID_FilingStatus ==
        FilingStatusEnum.Waiting
      )
        return;

      var result = await this.msgBox.confirm(
        "Would you like to change appointment status as Waiting?",
        "Change Status",
        "Yes",
        "No"
      );

      if (!result) {
        this.setActiveIndex();
        return false;
      }

      await this.ChangeStatus(FilingStatusEnum.Waiting);

      this.refresh();
    },
  };

  private _stepItem_Ongoing: MenuItem = {
    id: FilingStatusEnum.Ongoing + "",
    label: "Ongoing",
    command: (event: any) => {

      this.openOnGoingDIalogBox();
    },
  };

  private _stepItem_Canceled: MenuItem = {
    id: FilingStatusEnum.Cancelled + "",
    label: "Canceled",
    command: (event: any) => {

      this.openOnGoingDIalogBox();
    },
  };

  private _stepItem_Payment: MenuItem = {
    id: FilingStatusEnum.Payment + "",
    label: "Payment",
    command: async (event: any) => {
      if (
        this.CurrentObject.AppointmentStatus_ID_FilingStatus ==
        FilingStatusEnum.Payment
      )
        return;

      var result = await this.msgBox.confirm(
        "Would you like to change appointment status as Payment?",
        "Change Status",
        "Yes",
        "No"
      );

      if (!result) {
        this.setActiveIndex();
        return false;
      }

      await this.ChangeStatus(FilingStatusEnum.Payment);

      this.refresh();
    },
  };

  ngOnInit() {
    this.refresh();
  }

  async refresh() {

    
    this.items = [];

    if (isNullOrUndefined(this.ID_AppointmentSchedule)) return;

    await this.loadCurrentAppointmentRecord();

    if (this.CurrentObject["ID_Company"] != undefined) {

      if (this.CurrentObject.ID_Company != this.currentUser.ID_Company) {
 
        return;
      }
    }

    this.items.push(this._stepItem_Waiting);

    if (
      this.CurrentObject.AppointmentStatus_ID_FilingStatus ===
        FilingStatusEnum.Waiting ||
      this.CurrentObject.AppointmentStatus_ID_FilingStatus ===
        FilingStatusEnum.Ongoing ||
      this.CurrentObject.AppointmentStatus_ID_FilingStatus ===
        FilingStatusEnum.Payment
    ) {
      this.items.push(this._stepItem_Ongoing);
    }

    if (
      this.CurrentObject.AppointmentStatus_ID_FilingStatus ===
      FilingStatusEnum.Cancelled
    ) {
      this.items.push(this._stepItem_Canceled);
    }

    if (
      this.CurrentObject.AppointmentStatus_ID_FilingStatus !=
      FilingStatusEnum.Cancelled
    ) {
      this.items.push(this._stepItem_Payment);
    }

    this.setActiveIndex();
  }

  setActiveIndex() {
    this.items.forEach((item, index) => {
      if (
        this.CurrentObject.AppointmentStatus_ID_FilingStatus + "" ==
        item.id
      ) {
        this.activeIndex = index;
      }
    });
  }

  private async loadCurrentAppointmentRecord() {
    this.CurrentObject = await this.ds.loadObject<AppointmentSchedule_DTO>(
      APP_MODEL.APPOINTMENTSCHEDULE,
      this.ID_AppointmentSchedule + ""
    );
  }

  async ChangeStatus(ID_FilingStatus: number): Promise<any> {
    return new Promise<any>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pChangeAppointmentScheduleStatus",
        {
          IDs_AppointmentSchedule: [this.ID_AppointmentSchedule],
          ID_FilingStatus: ID_FilingStatus,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `Appointment Status has been changed successfully.`,
          `Appointment Schedule`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(obj.message, `Failed to change appointment status.`);
        rej(obj);
      }
    });
  }

  /* On Going */
  OngoingCurrentObject: AppointmentSchedule_DTO = new AppointmentSchedule_DTO();

  Ongoing_ID_AppointmentStatus_ID_FilingStatus_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt(`
        SELECT ID, 
              Name 
        FROM tFilingStatus
        WHERE 
        ID IN (
                ${FilingStatusEnum.Ongoing},
                ${FilingStatusEnum.Cancelled}
              )
        `),
  };

  openOnGoingDIalogBox() {

    this.OngoingCurrentObject = new AppointmentSchedule_DTO(); 
    this.OngoingCurrentObject.AppointmentStatus_ID_FilingStatus = this.CurrentObject.AppointmentStatus_ID_FilingStatus;

    this.onGoingDialogBoxCmp.closeOnPositiveButton = false;
    this.onGoingDialogBoxCmp.title = "Change Status";
    this.onGoingDialogBoxCmp.size = "md";
    this.onGoingDialogBoxCmp.captionPositiveButton = "Change";
    this.onGoingDialogBoxCmp.open();
  }

  async OngoingDialogBox_onPositiveButtonClick(modal) {

    var validationsAppForm: IFormValidation[] = [];
    var AppointmentStatus_ID_FilingStatus = this.OngoingCurrentObject.AppointmentStatus_ID_FilingStatus;

    if(isNullOrUndefined(AppointmentStatus_ID_FilingStatus)) AppointmentStatus_ID_FilingStatus = 0;
    if(!isNumber(AppointmentStatus_ID_FilingStatus)) AppointmentStatus_ID_FilingStatus = 0;

    if(AppointmentStatus_ID_FilingStatus == 0) validationsAppForm.push({ message:"Select an Appointment Status..." })

    if (validationsAppForm.length > 0) {

      this.msgBox.showValidationsBox(validationsAppForm);
      return;
    }

    var obj = await this.ChangeStatus(this.OngoingCurrentObject.AppointmentStatus_ID_FilingStatus);
    if(obj.Success){

      await this.refresh();
      modal.close();
    }
  }

  async OngoingDialogBox_onClosed() {

    await this.refresh();
  }
}
