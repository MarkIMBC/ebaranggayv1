import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupUploadExcelComponent } from './popup-upload-excel.component';

describe('PopupUploadExcelComponent', () => {
  let component: PopupUploadExcelComponent;
  let fixture: ComponentFixture<PopupUploadExcelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupUploadExcelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupUploadExcelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
