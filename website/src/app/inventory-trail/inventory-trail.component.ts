import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MenuItem } from 'primeng/api';
import { Table } from 'primeng/table';
import { APP_REPORTVIEW } from 'src/bo/APP_MODELS';
import { FilterCriteriaType } from 'src/utils/controls/appControlContainer/appControlContainer.component';
import { IFilterFormValue } from 'src/utils/controls/appForm/appForm.component';
import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { InventoryTrail_DTO, Item_DTO, PropertyTypeEnum } from 'src/utils/controls/class_helper';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { UserAuthenticationService } from '../AppServices/UserAuthentication.service';
import { ListViewModalService } from '../View/ListView/ListView.service';

@Component({
  selector: 'app-inventory-trail',
  templateUrl: './inventory-trail.component.html',
  styleUrls: ['./inventory-trail.component.less']
})
export class InventoryTrailComponent implements OnInit {
  
  inventoryTrails: InventoryTrail_DTO[] = []
  menuItems: MenuItem[] = [];
  item: Item_DTO

  _ID_Item: number = -1;

  @ViewChild('dt') table: Table;

  constructor( 
    private ds: DataService, 
    private globalFx: GlobalfxService, 
    private msgBox: MessageBoxService,
    private userAuthSvc: UserAuthenticationService,
    private lvModal: ListViewModalService,
    private route: ActivatedRoute,
    private router: Router,
    private cs: CrypterService,
    private toastr: ToastrService
    ) { }

  async ngOnInit(): Promise<void> {

    this._ID_Item = parseInt(this.route.snapshot.params['ID_Item']);

    await this.loadItem();

    this.menuItems.push({
      label: 'Refresh',
      icon: 'pi pi-fw pi-refresh green-text',
      command: async () => {

        this.loadInventoryTrails();
      }
    })
    
    this.menuItems.push({
      label: 'Report',
      icon: 'pi pi-fw pi-file blue-text',
      items: [
        {
          label: 'Inventory Detail', 
          icon: 'pi pi-file-o',
          command: async () => {
            
            var filterFormValue: IFilterFormValue[] = [];
            var options = {

              backLink: this.router.url
            }
            
            filterFormValue.push({
              dataField: "ID_Item",
              filterCriteriaType: FilterCriteriaType.Equal,
              propertyType: PropertyTypeEnum.Int,
              value: this._ID_Item
            });

            this.globalFx.navigateReport(
              'INVENTORYDETAILREPORT',
              APP_REPORTVIEW.INVENTORYDETAILREPORT,
              filterFormValue,
              options
            );
          }
        }
      ]
    });
  }


  async loadItem(){

    var ID_Item = parseInt(this.route.snapshot.params['ID_Item']);

    var sql = `SELECT * FROM vItem WHERE ID = ${ID_Item}`;

    sql = this.cs.encrypt(sql);
    var records = await this.ds.query<Item_DTO>(sql);

    if(records.length > 0){

      this.item = records[0];
    }else{

      this.item = new Item_DTO();
    }
  }

  async loadInventoryTrails(){

    var ID_Item = parseInt(this.route.snapshot.params['ID_Item']);

    var sql = `
      SELECT * FROM vInventoryTrail WHERE ID_Item = ${ID_Item}
      ORDER BY Date DESC
      `;

    sql = this.cs.encrypt(sql);
    this.inventoryTrails = await this.ds.query<InventoryTrail_DTO>(sql);
  }

  gotoInventory(){

    this.router.navigate([`/Main/Inventory`], {
    });
  }

  ngAfterViewInit() {
  
    this.loadInventoryTrails();
  }
}
