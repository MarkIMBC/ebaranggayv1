import { GlobalfxService } from "src/utils/service/globalfx.service";
import { APP_MODEL } from "src/bo/APP_MODELS";
import { AppointmentScheduleDialogComponent } from "./appointment-schedule-dialog/appointment-schedule-dialog.component";
import {
  IMenuItem,
  PositionEnum,
  ScheduleColorEnum,
  UserGroupEnum,
  Dentist_DTO,
  Schedule_DTO,
  AppointmentEvent,
} from "./../../utils/controls/class_helper";
import { SafeStyle } from "@angular/platform-browser";
import {
  APP_DETAILVIEW,
  Appointment,
  Employee,
  Schedule,
} from "./../../bo/APP_MODELS";
import { DetailViewDialogService } from "./../View/DetailViewDialog/DetailViewDialog.component";
import { DataService } from "./../../utils/service/data.service";
import {
  CalendarOptions,
  EventApi,
  DateSelectArg,
  EventClickArg,
  EventInput,
  Calendar,
  FullCalendarComponent,
  DatesSetArg,
  EventChangeArg,
} from "@fullcalendar/angular";
import { Dropdown } from "primeng/dropdown";
import { ContextMenuModule } from "primeng/contextmenu";
import { Component, OnInit, ViewChild } from "@angular/core";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { CalendarInfo } from "src/bo/VC/Schedule/VC_Appointment";
import * as TS from "linq-typescript";
import * as moment from "moment";
import { MenuItem, SelectItem } from "primeng/api";
import { ButtonModule } from "primeng/button";
import {
  UserAuthenticationService,
  TokenSessionFields,
} from "../AppServices/UserAuthentication.service";
import { CrypterService } from "src/utils/service/Crypter.service";

@Component({
  selector: "app-appointment-schedule",
  templateUrl: "./appointment-schedule.component.html",
  styleUrls: ["./appointment-schedule.component.less"],
})
export class AppointmentScheduleComponent implements OnInit {
  currentModelID: string = APP_MODEL.APPOINTMENTSCHEDULE;

  @ViewChild("appointmentscheduledialog")
  appointmentscheduledialogCmp: AppointmentScheduleDialogComponent;

  @ViewChild("fullCalendar")
  fullCalendar: FullCalendarComponent;

  @ViewChild("dentistListDropdown")
  dropdown: Dropdown;

  @ViewChild("btnRefresh")
  btnRefresh: ButtonModule;

  isShowLookup: boolean = true;

  dentistList: Dentist_DTO[];
  selectedDentist: Dentist_DTO = new Dentist_DTO();
  appointmentEvents: AppointmentEvent[];

  currentUser: TokenSessionFields;

  constructor(
    private ds: DataService,
    private userAuthSvc: UserAuthenticationService,
    private dvModalSvc: DetailViewDialogService,
    private msgBox: MessageBoxService,
    protected globalFx: GlobalfxService,
    private cs: CrypterService
  ) {
    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  dateSetArgs: DatesSetArg = null;
  currentEvents: EventApi[] = [];

  calendarOptions: CalendarOptions = {
    headerToolbar: {
      left: "prev,next today",
      center: "title",
      right: "dayGridMonth,listMonth",
    },
    initialView: "dayGridMonth",
    initialEvents: [],
    weekends: true,
    editable: false,
    selectable: true,
    selectMirror: true,
    dayMaxEvents: true,
    eventClick: this.handleEventClick.bind(this),
    eventsSet: this.handleEvents.bind(this),
    datesSet: (p: DatesSetArg) => {
      this.dateSetArgs = p;
      this.refreshEvents();
    },
  };

  async handleEventClick(clickInfo: EventClickArg) {

    var eventID = clickInfo.event.id;

    this.goto(eventID);
  }

  goto(eventID) {

    var routeLink = [];
    var config = {};
    var Oid_Model = "";
    var ID_CurrentObject = 0;
    var splitted = eventID.split("|");

    Oid_Model = splitted[0];
    ID_CurrentObject = splitted[1];

    config = {};

    switch (Oid_Model) {
      case APP_MODEL.PATIENT_SOAP:

        routeLink = [`/Main`, 'Patient_SOAP', ID_CurrentObject];
        break;
    }
  
    this.globalFx.customNavigate(routeLink, config);
  }

  handleEvents(events: EventApi[]) {
    this.currentEvents = events;
  }

  dentistListDropdown_onChange(event: EventChangeArg) {
    this.refreshEvents();
  }

  btnRefresh_onClick(event: EventClickArg) {
    this.refreshEvents();
  }

  async refreshEvents(): Promise<void> {
    const calendarApi = this.fullCalendar.getApi();

    calendarApi.removeAllEvents();

    var appointmentEvents = await this.getAppointmentEvent();

    if (appointmentEvents) {
      appointmentEvents.forEach((a) => {
        var event: EventInput = {
          editable: false,

          start: a.FormattedDateStart,
          end: a.FormattedDateEnd,
          id: a.UniqueID + "",
          title: a.Paticular + " - " + a.Description,
        };

        event.backgroundColor = "#116fbf";

        calendarApi.addEvent(event);
      });
    }

    return Promise.resolve();
  }

  async getAppointmentEvent(): Promise<AppointmentEvent[]> {
    const calendarApi = this.fullCalendar.getApi();

    var currentDate = calendarApi.getDate();
    const startOfMonth = moment(currentDate)
      .clone()
      .startOf("month")
      .format("YYYY-MM-DD hh:mm:ss");
    const endOfMonth = moment(currentDate)
      .clone()
      .endOf("month")
      .format("YYYY-MM-DD hh:mm:ss");

    var sql = this.cs.encrypt(`
      SELECT * 
      FROM vAppointmentEvent 
      WHERE 
        ID_Company = ${this.currentUser.ID_Company} AND
        DateStart BETWEEN '${startOfMonth}' AND '${endOfMonth}'
    `);

    var appointmentEvents = await this.ds.query<any>(sql);

    this.appointmentEvents = appointmentEvents;

    return Promise.resolve(appointmentEvents);
  }

  ngOnInit(): void {}
}
