import { APP_MODEL } from 'src/bo/APP_MODELS';
import { PositionEnum, AppointmentSchedule_DTO, FilingStatusEnum } from 'src/utils/controls/class_helper';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { resolve } from 'dns';
import { UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { IFormValidation, MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { IAppSelectBoxOption } from 'src/utils/controls/appSelectBox/appSelectBox.component';
import { MenuItem } from 'primeng/api';
import { isNullOrUndefined } from 'util';
import { AppFormComponent } from 'src/utils/controls/appForm/appForm.component';
import { BaseCustomDetailDialog } from 'src/app/CustomDetailDialog/BaseCustomDetailDialog';
import { IAppLookupBoxOption } from 'src/utils/controls/appLookupBox/appLookupBox.component';

@Component({
  selector: 'appointment-schedule-dialog',
  templateUrl: './appointment-schedule-dialog.component.html',
  styleUrls: ['./appointment-schedule-dialog.component.less']
})
export class AppointmentScheduleDialogComponent extends BaseCustomDetailDialog {
  
  currentModelID: string = APP_MODEL.APPOINTMENTSCHEDULE;
  CurrentObject: any = {};
  PreviousObject: any = {};

  constructor(protected modalService: NgbModal, protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected cs: CrypterService) {

    super(modalService, ds,  globalFx, msgBox, userAuthSvc, cs);
  }
  
  ID_Doctor_LookupBoxOption: IAppLookupBoxOption = {

    apiUrl: 'Model/GetList/null',
    sourceKey: this.cs.encrypt(`
      SELECT ID, Name FROM vEmployee WHERE ID_Position = ${PositionEnum.Dentist}
    `)
  };

  async ID_Doctor_LookUpBox_onSelectedItem(event: any) {

    this.CurrentObject.ID_Doctor = event.ID;
  }

  ID_Patient_LookupBoxOption: IAppLookupBoxOption = {

    apiUrl: 'Model/GetList/null',
    sourceKey: this.cs.encrypt(`
      SELECT ID, Name FROM tPatient
    `)
  };

  async ID_Patient_LookUpBox_onSelectedItem(event: any) {

    console.log(event);
    this.CurrentObject.ID_Patient = event.ID
  }

  protected async validation(): Promise<IFormValidation[]> {

    var validations: IFormValidation[] = [];

    if(isNullOrUndefined(this.CurrentObject.ID_Patient)) this.CurrentObject.ID_Patient = null;
    if(isNullOrUndefined(this.CurrentObject.ID_Doctor)) this.CurrentObject.ID_Doctor = null;

    if(this.CurrentObject.ID_Patient == null) validations.push({
      message: 'Patient is required.'
    })
    
    if(this.CurrentObject.ID_Doctor == null) validations.push({
      message: 'Doctor is required.'
    })

    return Promise.resolve(validations);
  };


  async open<T>(option): Promise<T[]> {

    var _option = {

      ID: option.ID,
      ID_Schedule: option.ID_Schedule,
      ID_Session: this.currentUser.ID_UserSession
    }

    await this.loadRecord(_option);

    return await this._open()
  }

}