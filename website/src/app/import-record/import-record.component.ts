import { Component, OnInit, ViewChild } from "@angular/core";
import { NgForm } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import readXlsxFile from "read-excel-file";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import {
  TokenSessionFields,
  UserAuthenticationService,
} from "../AppServices/UserAuthentication.service";
import { FormHelperDialogService } from "../View/FormHelperView/FormHelperView.component";
import { ListViewModalService } from "../View/ListView/ListView.service";
import * as moment from "moment";
import { isNull, isNullOrUndefined, isUndefined } from "util";

@Component({
  selector: "app-import-record",
  templateUrl: "./import-record.component.html",
  styleUrls: ["./import-record.component.less"],
})
export class ImportRecordComponent implements OnInit {
  @ViewChild("form") form: NgForm;

  currentUser: TokenSessionFields;

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected lvModal: ListViewModalService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected cs: CrypterService,
    protected toastr: ToastrService,
    private dvFormSvc: FormHelperDialogService
  ) {
    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  ngOnInit(): void {}

  async btnImportClient_OnClick(event) {
    var records = [];
    var file: File = event.target.files[0];

    const schema = {
      name: {
        prop: "Name",
        type: String,
      },
      client_id: {
        prop: "Old_client_id",
        type: Number,
      },
      client_ids: {
        prop: "Old_client_code",
        type: String,
      },
      mobile: {
        prop: "ContactNumber",
        type: String,
      },
      email: {
        prop: "Email",
        type: String,
      },
      address: {
        prop: "Address",
        type: String,
      },
    };

    await readXlsxFile(file, { schema }).then(async (rows) => {
      records = rows.rows;
    });

    var obj = await this.ds.execSP(
      "pImportClient",
      {
        records: records,
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
        isTransaction: true,
      }
    );

    if (obj.Success) {
      this.msgBox.success("Client Record has been imported.", "Import Done");
    } else {
      this.msgBox.warning(
        "Client Record has not been imported.",
        "Import Failed"
      );
    }

    this.form.resetForm();
  }

  async btnImportPatient_OnClick(event) {
    var records = [];
    var file: File = event.target.files[0];

    const schema = {
      pet_name: {
        prop: "Name",
        type: String,
      },
      client_id: {
        prop: "Old_client_id",
        type: Number,
      },
      pet_ids: {
        prop: "Old_patient_code",
        type: String,
      },
      pet_id: {
        prop: "Old_patient_id",
        type: Number,
      },
      species: {
        prop: "Species",
        type: String,
      },
      breed: {
        prop: "Breed",
        type: String,
      },
      pet_gender: {
        prop: "Gender",
        type: String,
      },
      color: {
        prop: "Color",
        type: String,
      },
      remarks: {
        prop: "Remarks",
        type: String,
      },
      birthdate: {
        prop: "BirthDate",
        type: Date,
      },
      deceased: {
        prop: "IsDeceased",
        type: String,
      },
      neutered: {
        prop: "IsNeutered",
        type: String,
      },
    };

    await readXlsxFile(file, { schema }).then(async (rows) => {
      for (let row of rows.rows) {
        var validDate = moment("1900-01-01");

        if (row.BirthDate < validDate) row.BirthDate = validDate;

        records.push(row);
      }
    });

    var obj = await this.ds.execSP(
      "pImportPatient",
      {
        records: records,
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
        isTransaction: true,
      }
    );

    if (obj.Success) {
      this.msgBox.success("Patient Record has been imported.", "Import Done");
    } else {
      this.msgBox.warning(
        "Patient Record has not been imported.",
        "Import Failed"
      );
    }

    this.form.resetForm();
  }

  async btnImportItem_OnClick(event) {
    var records = [];
    var file: File = event.target.files[0];

    const schema = {
      product_id: {
        prop: "product_id",
        type: String,
      },
      generic_name: {
        prop: "generic_name",
        type: String,
      },
      brand_name: {
        prop: "brand_name",
        type: String,
      },
      packaging_dosage_form: {
        prop: "packaging_dosage_form",
        type: String,
      },
      price: {
        prop: "price",
        type: Number,
      },
      selling_price: {
        prop: "selling_price",
        type: String,
      },
    };

    await readXlsxFile(file, { schema }).then(async (rows) => {
      records = rows.rows;
    });

    var obj = await this.ds.execSP(
      "pImportItem",
      {
        records: records,
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
        isTransaction: true,
      }
    );

    if (obj.Success) {
      this.msgBox.success("Item Record has been imported.", "Import Done");
    } else {
      this.msgBox.warning(
        "Item Record has not been imported.",
        "Import Failed"
      );
    }

    this.form.resetForm();
  }

  async btnImportItemService_OnClick(event) {
    var records = [];
    var file: File = event.target.files[0];

    const schema = {
      procedure_id: {
        prop: "Old_procedure_id",
        type: String,
      },
      procedure_name: {
        prop: "procedure_name",
        type: String,
      },
      procedure_php: {
        prop: "procedure_php",
        type: Number,
      },
    };

    await readXlsxFile(file, { schema }).then(async (rows) => {
      records = rows.rows;
    });

    var obj = await this.ds.execSP(
      "pImportService",
      {
        records: records,
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
        isTransaction: true,
      }
    );

    if (obj.Success) {
      this.msgBox.success("Item Record has been imported.", "Import Done");
    } else {
      this.msgBox.warning(
        "Item Record has not been imported.",
        "Import Failed"
      );
    }

    this.form.resetForm();
  }

  async btnImportPatientSOAP_OnClick(event) {
    var records = [];
    var file: File = event.target.files[0];

    const schema = {
      soap_id: {
        prop: "soap_id",
        type: Number,
      },
      pet_id: {
        prop: "pet_id",
        type: Number,
      },
      date: {
        prop: "date",
        type: Date,
      },
      subj: {
        prop: "subj",
        type: String,
      },
      heart_rate: {
        prop: "heart_rate",
        type: String,
      },
      respiratory_rate: {
        prop: "respiratory_rate",
        type: String,
      },
      weight: {
        prop: "weight",
        type: String,
      },
      weight_unit: {
        prop: "weight_unit",
        type: String,
      },
      length: {
        prop: "length",
        type: String,
      },
      bcs: {
        prop: "bcs",
        type: String,
      },
      temperature: {
        prop: "temperature",
        type: String,
      },
      temperature_degrees: {
        prop: "temperature_degrees",
        type: String,
      },
      diagnosis: {
        prop: "diagnosis",
        type: String,
      },
      notes: {
        prop: "notes",
        type: String,
      },
      test_result: {
        prop: "test_result",
        type: String,
      },
      final_diagnosis: {
        prop: "final_diagnosis",
        type: String,
      },
      prognosis: {
        prop: "prognosis",
        type: String,
      },
      category: {
        prop: "category",
        type: String,
      },
      treatment: {
        prop: "treatment",
        type: String,
      },
      remarks: {
        prop: "remarks",
        type: String,
      },
      date_return: {
        prop: "date_return",
        type: Date,
      },
      reason: {
        prop: "reason",
        type: String,
      },
      priority: {
        prop: "priority",
        type: String,
      },
      prescribed: {
        prop: "prescribed",
        type: String,
      },
      prescribed_id: {
        prop: "prescribed_id",
        type: String,
      },
      status: {
        prop: "status",
        type: String,
      },
    };

    await readXlsxFile(file, { schema }).then(async (rows) => {
      for (let row of rows.rows) {
        var validDate = moment("1900-01-01");

        if (row.date < validDate) row.date = validDate;
        if (row.date_return < validDate) row.date_return = validDate;

        if (isUndefined(row.heart_rate)) row.heart_rate = null;
        if (isUndefined(row.respiratory_rate)) row.respiratory_rate = null;
        if (isUndefined(row.length)) row.length = null;
        if (isUndefined(row.status)) row.status = null;

        records.push(row);
      }
    });

    var obj = await this.ds.execSP(
      "pImportPatientSOAP",
      {
        records: records,
        ID_UserSession: this.currentUser.ID_UserSession,
      },
      {
        isReturnObject: true,
        isTransaction: true,
      }
    );

    if (obj.Success) {
      this.msgBox.success("Patient Record has been imported.", "Import Done");
    } else {
      this.msgBox.warning(
        "Patient SOAP Record has not been imported.",
        "Import Failed"
      );
    }

    this.form.resetForm();
  }
}

enum ClientFileIndexEnum {
  client_id = 0,
  client_ids = 1,
  name = 2,
  client_profile = 3,
  birthdate = 4,
  age = 5,
  gender = 6,
  address = 7,
  house_number = 8,
  building_name = 9,
  street = 10,
  district = 11,
  city = 12,
  province = 13,
  country = 14,
  zipcode = 15,
  mobile = 16,
  mobile_country_code = 17,
  email = 18,
  password = 19,
  landline = 20,
  landline_country_code = 21,
  landline_area_code = 22,
  id_presented = 23,
  id_number = 24,
  company_name = 25,
  company_details = 26,
  company_landline_country_code = 27,
  company_landline_area_code = 28,
  company_landline = 29,
  company_mobile_country_code = 30,
  company_mobile = 31,
  user_id = 32,
  date = 33,
  verification = 34,
  date_minute = 35,
}

enum PatientFileIndexEnum {
  pet_id = 0,
  pet_ids = 1,
  client_id = 2,
  pet_profile = 3,
  pet_name = 4,
  birthdate = 5,
  age = 6,
  species = 7,
  pet_gender = 8,
  breed = 9,
  other_breed = 10,
  neutered = 11,
  color = 12,
  microchip = 13,
  remarks = 14,
  deceased = 15,
  status = 16,
  user_id = 17,
  date = 18,
}
