import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToothInfoComponent } from './tooth-info.component';

describe('ToothInfoComponent', () => {
  let component: ToothInfoComponent;
  let fixture: ComponentFixture<ToothInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToothInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToothInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
