import { Component, OnInit, AfterContentInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { DentalExamination_Tooth_Condition, ToothSurfaceSection, Tooth_DTO, ToothInfo_DTO } from 'src/utils/controls/class_helper';
import { APP_MODEL, Tooth, ToothStatus, ToothInfo } from 'src/bo/APP_MODELS';
import { PanelModule } from 'primeng/panel';
import { FieldsetModule } from 'primeng/fieldset';
import { TabViewModule } from 'primeng/tabview';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { MenuItem } from 'primeng/api';
import { ToothComponent } from '../tooth/tooth.component';
import { MenubarModule } from 'primeng/menubar';
import { DataService } from 'src/utils/service/data.service';
import { EventChangeArg } from '@fullcalendar/angular';
import { CrypterService } from 'src/utils/service/Crypter.service';

@Component({
  selector: 'app-tooth-info',
  templateUrl: './tooth-info.component.html',
  styleUrls: ['./tooth-info.component.less']
})
export class ToothInfoComponent implements OnInit,  AfterContentInit {
 
  old_toothInfo:ToothInfo_DTO = new ToothInfo_DTO();

  @Input()
  toothInfo:ToothInfo_DTO;

  tooth:Tooth_DTO;
  toothStatuses: ToothStatus[] = [];
  selectedToothStatus: ToothStatus;

  @ViewChild('toothComponent')
  toothComponent: ToothComponent;

  constructor(
    private cs: CrypterService,
    private ds: DataService) {
 

  }

  async ngOnInit(){

    var sql = this.cs.encrypt(`
              Select 0 ID,
                     '' Code,
                     '' Name,
                     '-- Select --' DisplayName,
                     '' Comment
              UNION
              SELECT ID,
                  Code,
                  Name,
                  Code + ' - ' + Name DisplayName,
                  Comment
              FROM tToothStatus
              WHERE IsActive = 1;`);
    this.toothStatuses = await this.ds.query<ToothStatus>(sql);

    return Promise.resolve();
  }

  async loadTooth(ID: number){

    this.tooth = await this.ds.loadObject(APP_MODEL.TOOTH, ID + "") as Tooth_DTO;
    this.toothComponent.setToothInfo(this.tooth);

    this.toothInfo.ID_Tooth = this.tooth.ID;
    this.toothInfo.Location_Tooth = this.tooth.Location;
    this.toothInfo.ToothNumber_Tooth = this.tooth.ToothNumber;
    this.toothInfo.IDs_ToothSurface = this.tooth.IDs_ToothSurface;

    return Promise.resolve();
  }

  async loadToothInfo(toothInfo: ToothInfo_DTO){

    this.toothInfo.ID_Tooth = toothInfo.ID_Tooth
    this.toothInfo.IDs_ToothSurface = toothInfo.IDs_ToothSurface
    this.toothInfo.ID_ToothStatus = toothInfo.ID_ToothStatus
    this.toothInfo.Code_ToothStatus = toothInfo.Code_ToothStatus
    this.toothInfo.Name_ToothStatus = toothInfo.Name_ToothStatus
    this.toothInfo.Comment = toothInfo.Comment

    this.old_toothInfo.ID_Tooth = toothInfo.ID_Tooth;
    this.old_toothInfo.IDs_ToothSurface = toothInfo.IDs_ToothSurface;
    this.old_toothInfo.ID_ToothStatus = toothInfo.ID_ToothStatus;
    this.old_toothInfo.Code_ToothStatus = toothInfo.Code_ToothStatus;
    this.old_toothInfo.Name_ToothStatus = toothInfo.Name_ToothStatus;
    this.old_toothInfo.Comment = toothInfo.Comment;

    this.selectedToothStatus = this.toothStatuses.find( toothStatus => toothStatus.ID ===  this.toothInfo.ID_ToothStatus )

    this.tooth = await this.ds.loadObject(APP_MODEL.TOOTH, this.toothInfo.ID_Tooth + "") as Tooth_DTO;
    this.tooth.IDs_ToothSurface = this.toothInfo.IDs_ToothSurface;
    this.toothComponent.setToothInfo(this.tooth);

    return Promise.resolve();
  }

  tooth_onSelectedChanged(): void {

    this.toothInfo.IDs_ToothSurface = this.tooth.IDs_ToothSurface;
  }

  toothStatusDropdown_onChange(event: any) {

    var toothStatus = event.value as ToothStatus;

    this.toothInfo.ID_ToothStatus = toothStatus.ID;
    this.toothInfo.Code_ToothStatus = toothStatus.Code;
    this.toothInfo.Name_ToothStatus = toothStatus.Name;
  }

  async ngAfterContentInit() {

    var sql = this.cs.encrypt(`
        Select 0 ID,
              '' Code,
              '' Name,
              '-- Select --' DisplayName,
              '' Comment
        UNION
        SELECT ID,
            Code,
            Name,
            Code + ' - ' + Name DisplayName,
            Comment
        FROM tToothStatus
        WHERE IsActive = 1;`);
        
    this.toothStatuses = await this.ds.query<ToothStatus>(sql);

      this.loadToothInfo(this.toothInfo);
  }
}
