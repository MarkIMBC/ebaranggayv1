import { Component, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import { ToothInfoComponent } from '../tooth-info/tooth-info.component';
import { ToothInfo } from 'src/bo/APP_MODELS';
import { ToothInfo_DTO, ToothInfoEventArg } from 'src/utils/controls/class_helper';
import { isNullOrUndefined } from 'util';
import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';


@Component({
  selector: 'app-tooth-info-dialog',
  templateUrl: './tooth-info-dialog.component.html',
  styleUrls: ['./tooth-info-dialog.component.less']
})
export class ToothInfoDialogComponent{

  title = 'ng-bootstrap-modal-demo';
  closeResult: string;
  modalOptions:NgbModalOptions;

  @ViewChild('toothInfo')
  toothInfoComponent: ToothInfoComponent

  @ViewChild('modalComponent')
  modalComponent: NgbModal

  @Input()
  IsHideButton:boolean = false;

  @Input()
  toothInfo:ToothInfo_DTO;
  
  @Output() onSelectedChanged = new EventEmitter();
  @Output() onClosedbyBtnOK: EventEmitter<ToothInfoEventArg>  = new EventEmitter();

  constructor(
    private modalService: NgbModal,
    private msgBox: MessageBoxService
  ){
    this.modalOptions = {
      backdrop:'static',
      backdropClass:'customBackdrop',
      size: 'lg',
    }
  }
  
   open(toothInfo: ToothInfo) {

    var promise = new Promise((resolve, reject) => {
    var toothInfoEventArg: ToothInfoEventArg = new ToothInfoEventArg();

    this.toothInfo = toothInfo;

    this.modalService.open(this.modalComponent, this.modalOptions).result.then((isCloseByBtnOK) => {
      
        if(isCloseByBtnOK){
      
          toothInfoEventArg.toothInfo = this.toothInfo;

          if(isNullOrUndefined(this.toothInfo.ID_ToothStatus) ) toothInfoEventArg.toothInfo.ID_ToothStatus = 0;
          if(isNullOrUndefined(this.toothInfo.Code_ToothStatus) ) toothInfoEventArg.toothInfo.Code_ToothStatus = "";
          if(isNullOrUndefined(this.toothInfo.Name_ToothStatus) ) toothInfoEventArg.toothInfo.Name_ToothStatus = "";
          if(isNullOrUndefined(this.toothInfo.IDs_ToothSurface) ) toothInfoEventArg.toothInfo.IDs_ToothSurface = "";
          if(isNullOrUndefined(this.toothInfo.Comment) ) toothInfoEventArg.toothInfo.Comment = "";

          this.toothInfo.Location_Tooth = this.toothInfo.Location_Tooth;
          this.toothInfo.ToothNumber_Tooth = this.toothInfo.ToothNumber_Tooth;

          this.validate(this.toothInfo).then(async () => {

            this.onClosedbyBtnOK.emit(toothInfoEventArg);
            resolve(toothInfoEventArg);
          }, (reason) => {
  
            this.msgBox.showValidationBox(reason);
            reject();
          });
      
        }else{

          reject();
        }
      }, (reason) => {
  
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        reject();
      });
    });

    return promise;
  }

 validate(toothInfo: ToothInfo){

    var promise = new Promise((resolve, reject) => {

      var isValid = true;
      var msg = ""

      if(isValid == true){

        if(toothInfo.ID_ToothStatus == 0){

          isValid = false;
          msg = "Tooth Status is required";
        }
      }

      if(isValid == true){

        if(toothInfo.ID_ToothStatus == null){

          isValid = false;
          msg = "Tooth Status is required";
        }
      }

      if(isValid){

        resolve();
      }else{

        reject(msg);
      }
    });

    return promise;
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
