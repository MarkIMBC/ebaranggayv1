import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToothSurfaceComponent } from './tooth-surface.component';

describe('ToothSurfaceComponent', () => {
  let component: ToothSurfaceComponent;
  let fixture: ComponentFixture<ToothSurfaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToothSurfaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToothSurfaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
