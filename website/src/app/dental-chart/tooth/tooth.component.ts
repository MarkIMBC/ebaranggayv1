import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { ToothSurfaceSection, Tooth_DTO, ToothSurfaceEnum,  PositionEnum, ToothStatusTypeEnum } from 'src/utils/controls/class_helper';
import { ToothSurface, Tooth } from 'src/bo/APP_MODELS';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-tooth',
  templateUrl: './tooth.component.html',
  styleUrls: ['./tooth.component.less']
})

export class ToothComponent implements OnInit{

  @Input()
  tooth:Tooth_DTO;

  @Input()
  isSmall: boolean = false;

  @Input()
  isAllowSetSufarce = true;

  toothSurfaceSection_Top: ToothSurfaceSection = new ToothSurfaceSection({
    bgColor: '#b5c3c1'
  });

  toothSurfaceSection_Left: ToothSurfaceSection = new ToothSurfaceSection({
    bgColor: 'rgb(181 182 195)'
  });

  toothSurfaceSection_Middle: ToothSurfaceSection = new ToothSurfaceSection({
    bgColor: 'rgb(181 166 166)'
  });

  toothSurfaceSection_Bottom: ToothSurfaceSection = new ToothSurfaceSection({
    bgColor: 'rgb(134 159 187)'
  });

  toothSurfaceSection_Right: ToothSurfaceSection = new ToothSurfaceSection({
    bgColor: 'rgb(165 149 162)'
  });

  toothSurfaceSections: ToothSurfaceSection[] = [
    this.toothSurfaceSection_Top,
    this.toothSurfaceSection_Left,
    this.toothSurfaceSection_Middle,
    this.toothSurfaceSection_Bottom,
    this.toothSurfaceSection_Right
  ]

  @Output() onSelectedChanged = new EventEmitter();
  @Output() onClick = new EventEmitter<Tooth_DTO>();

  constructor(){

  }

  ngOnInit(): void {

  }

  setToothInfo(tooth:Tooth_DTO): void {

    this.tooth = tooth;

    this.toothSurfaceSection_Top.toothSurface ={
      ID: this.tooth.Top_ID_ToothSurface,
      Name: this.tooth.Top_Name_ToothSurface
    }

    this.toothSurfaceSection_Left.toothSurface ={
      ID: this.tooth.Left_ID_ToothSurface,
      Name: this.tooth.Left_Name_ToothSurface
    }

    this.toothSurfaceSection_Middle.toothSurface ={
      ID: this.tooth.Middle_ID_ToothSurface,
      Name: this.tooth.Middle_Name_ToothSurface
    }

    this.toothSurfaceSection_Right.toothSurface ={
      ID: this.tooth.Right_ID_ToothSurface,
      Name: this.tooth.Right_Name_ToothSurface
    }

    this.toothSurfaceSection_Bottom.toothSurface ={
      ID: this.tooth.Bottom_ID_ToothSurface,
      Name: this.tooth.Bottom_Name_ToothSurface
    } 
 
    if(isNullOrUndefined(this.tooth.IDs_ToothSurface) != true){
    
      var toothSurfacesIDs = this.tooth.IDs_ToothSurface.split(",");

      this.toothSurfaceSections.forEach(toothSurfaceSection => {

        toothSurfaceSection.isSelected = false;
      })

      toothSurfacesIDs.forEach(toothSurfacesID => {
  
        var ID = parseInt(toothSurfacesID);
  
        this.selectByID(ID);
      });
    }
  }

  private selectByID(ID_ToothSurface: number): void {

    this.toothSurfaceSections.forEach(toothSurfaceSection => {

      if(toothSurfaceSection.toothSurface.ID == ID_ToothSurface){
        
        toothSurfaceSection.isSelected = true;
        return;
      }
    })
  }

  private setIDs_ToothSurface(): void {

    var IDs_ToothSurface:number[] = [];

    this.toothSurfaceSections.forEach(toothSurfaceSection => {

      var toothSurface = toothSurfaceSection.toothSurface;
      if(toothSurfaceSection.isSelected == true){

        IDs_ToothSurface.push(toothSurface.ID);
      }
    })

    this.tooth.IDs_ToothSurface = IDs_ToothSurface.toString();
  }

  toothContainer_onCLick(): void {

    this.onClick.emit(this.tooth);
  }

  toothSurfaceSection_onClick(position: PositionEnum): void {

    this.selectToothSurfaceSection(position);
  }

  selectToothSurfaceSection(position: PositionEnum): void {

    if(!this.isAllowSetSufarce) return;

    this.toothSurfaceSections[position].isSelected = this.toothSurfaceSections[position].isSelected ? false: true;
    this.setIDs_ToothSurface();
    this.onSelectedChanged.emit();
  }

}
