import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tooth-small',
  templateUrl: './tooth-small.component.html',
  styleUrls: ['./tooth-small.component.less']
})
export class ToothSmallComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  topSurface_OnClick(): void {

  }

  leftSurface_OnClick(): void {
    
  }
 
  middleSurface_OnClick(): void {
    
  }

  rightSurface_OnClick(): void {
    
  }

  bottomSurface_OnClick(): void {
    
  }

}
