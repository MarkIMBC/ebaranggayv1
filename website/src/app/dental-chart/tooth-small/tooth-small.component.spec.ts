import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToothSmallComponent } from './tooth-small.component';

describe('ToothSmallComponent', () => {
  let component: ToothSmallComponent;
  let fixture: ComponentFixture<ToothSmallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToothSmallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToothSmallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
