import { Component, OnInit, NgModule, ViewChild, Input, EventEmitter, Output, ViewChildren, QueryList } from '@angular/core';
import { ToothComponent } from './tooth/tooth.component';
import { ToothInfoDialogComponent } from './tooth-info-dialog/tooth-info-dialog.component';
import { DialogModule } from 'primeng/dialog';
import { MenubarModule } from 'primeng/menubar';
import { MenuItem } from 'primeng/api'
import { ToothInfo_DTO, Patient_DentalExamination_DTO, Doctor, Patient_DentalExamination_ToothInfo_DTO, ToothSurfaceEnum, ToothInfoEventArg, Tooth_DTO, DentitionEnum, SavingPatientDentalExamToothInfo, TeethQuadrantEnum, ITooth_ToothStatusArg, ToothStatus_DTO } from 'src/utils/controls/class_helper';
import { APP_MODEL, Patient, ToothSurface, ToothInfo } from 'src/bo/APP_MODELS';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { isNullOrUndefined } from 'util';
import { ListViewModalService } from 'src/app/View/ListView/ListView.service';
import { DataTableOption } from 'src/utils/controls/appDataTable/appDataTable.component';
import { UserAuthenticationService, TokenSessionFields } from '../AppServices/UserAuthentication.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { Enumerable } from 'linq-typescript';

@Component({
  selector: 'app-dental-chart',
  templateUrl: './dental-chart.component.html',
  styleUrls: ['./dental-chart.component.less'],
})

export class DentalChartComponent {
 
  constructor(
    private ds: DataService, 
    private globalFx: GlobalfxService, 
    private msgBox: MessageBoxService,
    private userAuthSvc: UserAuthenticationService,
    private lvModal: ListViewModalService,
    private route: ActivatedRoute,
    private cs: CrypterService,
    private toastr: ToastrService) {
  }

  currentUser: TokenSessionFields;
  teeth: Tooth_DTO[] = [];

  @ViewChildren('toothComponent')toothComponents: QueryList<ToothComponent>;

  @Input()
  isAllowClick = false;

  @Output() onToothClick = new EventEmitter<Tooth_DTO>();
  @Output() onToothStatusClick = new EventEmitter<ITooth_ToothStatusArg>();
  @Output() onToothStatusBtnCloseClick = new EventEmitter<ITooth_ToothStatusArg>();

  _DentitionID: DentitionEnum = DentitionEnum.Mixed;

  teeth_quantrand_1: Tooth_DTO[] = []; 
  teeth_quantrand_2: Tooth_DTO[] = []; 
  teeth_quantrand_3: Tooth_DTO[] = []; 
  teeth_quantrand_4: Tooth_DTO[] = []; 
  teeth_quantrand_5: Tooth_DTO[] = []; 
  teeth_quantrand_6: Tooth_DTO[] = []; 
  teeth_quantrand_7: Tooth_DTO[] = []; 
  teeth_quantrand_8: Tooth_DTO[] = []; 

  isToothSmall: boolean = true;

  async ngOnInit() {

    this.currentUser = this.userAuthSvc.getDecodedToken();

    await this.loadToothInfos();
  }

  async loadToothInfos(){

    var sql = this.cs.encrypt(`
              SELECT ID,
                    Code,
                    Name,
                    IsActive,
                    Comment,
                    ToothNumber,
                    Location,
                    Top_ID_ToothSurface,
                    Left_ID_ToothSurface,
                    Middle_ID_ToothSurface,
                    Bottom_ID_ToothSurface,
                    Right_ID_ToothSurface,
                    Top_Name_ToothSurface,
                    Left_Name_ToothSurface,
                    Middle_Name_ToothSurface,
                    Bottom_Name_ToothSurface,
                    Right_Name_ToothSurface,
                    ID_Dentition,
                    ID_TeethQuandrant
              FROM dbo.vTooth 
              WHERE ID_Dentition ${this._DentitionID == DentitionEnum.Mixed ? 'IN (1, 2)' : ' = ' + this._DentitionID}

        `);

    this.teeth = await this.ds.query<Tooth_DTO>(sql);

    this.setTeethQuandrant();    
  }

  setTeethQuandrant(){

    this.teeth_quantrand_1 = Enumerable.fromSource(this.teeth)
                            .where(r => r.ID_TeethQuandrant == TeethQuadrantEnum.Quandrant1 )
                            .where(r => r.ID_Dentition != DentitionEnum.Primary )
                            .orderBy(r => r.Location)
                            .toArray();

    this.teeth_quantrand_2 = Enumerable.fromSource(this.teeth)
                            .where(r => r.ID_TeethQuandrant == TeethQuadrantEnum.Quandrant2 )
                            .where(r => r.ID_Dentition != DentitionEnum.Primary )
                            .orderBy(r => r.Location)
                            .toArray();         

    this.teeth_quantrand_4 = Enumerable.fromSource(this.teeth)
                            .where(r => r.ID_TeethQuandrant == TeethQuadrantEnum.Quandrant4 )
                            .where(r => r.ID_Dentition != DentitionEnum.Primary )
                            .orderBy(r => r.Location)
                            .toArray(); 
                            
    this.teeth_quantrand_3 = Enumerable.fromSource(this.teeth)
                            .where(r => r.ID_TeethQuandrant == TeethQuadrantEnum.Quandrant3 )
                            .where(r => r.ID_Dentition != DentitionEnum.Primary )
                            .orderBy(r => r.Location)
                            .toArray();   
        
    this.teeth_quantrand_5 = Enumerable.fromSource(this.teeth)
                            .where(r => r.ID_TeethQuandrant == TeethQuadrantEnum.Quandrant5 )
                            .where(r => r.ID_Dentition != DentitionEnum.Permanent )
                            .orderBy(r => r.Location)
                            .toArray();

    this.teeth_quantrand_6 = Enumerable.fromSource(this.teeth)
                            .where(r => r.ID_TeethQuandrant == TeethQuadrantEnum.Quandrant6 )
                            .where(r => r.ID_Dentition != DentitionEnum.Permanent )
                            .orderBy(r => r.Location)
                            .toArray();   

    this.teeth_quantrand_7 = Enumerable.fromSource(this.teeth)
                            .where(r => r.ID_TeethQuandrant == TeethQuadrantEnum.Quandrant7 )
                            .where(r => r.ID_Dentition != DentitionEnum.Permanent )
                            .orderBy(r => r.Location)
                            .toArray();

    this.teeth_quantrand_8 = Enumerable.fromSource(this.teeth)
                            .where(r => r.ID_TeethQuandrant == TeethQuadrantEnum.Quandrant8 )
                            .where(r => r.ID_Dentition != DentitionEnum.Permanent )
                            .orderBy(r => r.Location)
                            .toArray(); 
  }

  async loadToothInfosByID(ID: number){

    this._DentitionID = ID;
    this.setTeethQuandrant();
  }
 
  getToothByLocation(Location: string): Tooth_DTO {

    var tooth: Tooth_DTO;
    var index: number = -1; 

    index = this.globalFx.findIndexByKeyValue(this.teeth, 'Location', Location);

    if(index > -1) tooth = this.teeth[index];

    return tooth;
  }

  getToothByID(ID_Tooth: number): Tooth_DTO {

    var tooth: Tooth_DTO;
    var index: number = -1; 

    index = this.globalFx.findIndexByKeyValue(this.teeth, 'ID', ID_Tooth + "");

    if(index > -1) tooth = this.teeth[index];

    return tooth;
  }

  getToothComponentsByID(ID_Tooth: number): ToothComponent {

    var toothComponent: ToothComponent;

    this.toothComponents.forEach(_toothComponent => {

      if(_toothComponent.tooth.ID == ID_Tooth){

        toothComponent = _toothComponent;
      } 
    });

    return toothComponent;
  }

  setToothStatuses(ID_Tooth: number, toothStatuses: ToothStatus_DTO[]): void{

    var tooth = this.getToothByID(ID_Tooth);

    tooth.ToothStatuses = toothStatuses;
  }

  setToothSurfaces(ID_Tooth: number, IDs_Surface: string): void{

    var tooth = this.getToothByID(ID_Tooth);
    var toothComponent = this.getToothComponentsByID(ID_Tooth);

    tooth.IDs_ToothSurface = IDs_Surface;
    toothComponent.setToothInfo(tooth);
  }

  clearToothStatuses(): void{

    this.teeth.forEach(tooth => {

      tooth.ToothStatuses = [];
      tooth.IDs_ToothSurface = ""
    });

    this.toothComponents.forEach(toothComponent => {

      toothComponent.tooth.IDs_ToothSurface = "";
      toothComponent.setToothInfo(toothComponent.tooth);
    });
  }

  tooth_onClick(tooth: Tooth_DTO){

    if(this.isAllowClick != true) return;

    this.onToothClick.emit(tooth);
  }

  toothStatus_onClick(eventArg: ITooth_ToothStatusArg){

    this.onToothStatusClick.emit(eventArg);
  }

  toothStatusClose_onClick(eventArg: ITooth_ToothStatusArg){

    this.onToothStatusBtnCloseClick.emit(eventArg);
  }
} 