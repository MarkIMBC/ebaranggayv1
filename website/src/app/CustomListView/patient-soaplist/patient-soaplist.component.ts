import { ControlTypeEnum, FilingStatusEnum, PropertyTypeEnum } from "./../../../utils/controls/class_helper";
import { Component, ElementRef, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { IFilterFormValue } from "src/utils/controls/appForm/appForm.component";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomListView } from "../BaseCustomListView";
import { isNullOrUndefined } from "util";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { MenuItem } from "primeng/api";
import * as moment from "moment";
import { DetailView_Detail } from "src/bo/APP_MODELS";

@Component({
  selector: 'app-patient-soaplist',
  templateUrl: './patient-soaplist.component.html',
  styleUrls: ['./patient-soaplist.component.less']
})

export class PatientSOAPListComponent extends BaseCustomListView {

  MinMaxColumn: string = "Date";

  AccordionFieldTabs: any = {

    IsActive: true,
    Name: '',
    ID_SOAPType: 0,
    Date: [
      new Date(),
      new Date()
    ]
  }

  ID_SOAPType_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt(`
      /*encryptsqlstart*/
      SELECT  ID,
              Name
      FROM tSOAPType
      /*encryptsqlend*/
    `),
  };

  ID_FilingStatus_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt(`
      /*encryptsqlstart*/
      SELECT  ID,
              Name
      FROM tFilingStatus
      WHERE ID IN (
        ${FilingStatusEnum.Filed},
        ${FilingStatusEnum.Approved},
        ${FilingStatusEnum.Cancelled}
      )
      /*encryptsqlend*/
    `),
  };


  async loadRecords() {

    this.initFilterFields();

    var sql = '';
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `/*encryptsqlstart*/
            SELECT *
            FROM dbo.vPatient_SOAP_ListView
          /*encryptsqlend*/
            WHERE 
                ID_Company = ${this.currentUser.ID_Company} 
                ${filterString}
          `;

    this.getRecordPaging(sql);

  }

  dataSource_InitLoad(obj: any) {

    obj.Records.forEach(record => {

      record['SOAPRecords'] = [];

      record.SOAPRecords.push({
        label: 'Primary Complaint / History',
        value: record.History,
        hasValue: (record.Count_History > 0)
      });

      record.SOAPRecords.push({
        label: 'Clinical Exam',
        value: record.ClinicalExamination,
        hasValue: (record.Count_ClinicalExamination > 0)
      });

      record.SOAPRecords.push({
        label: 'Laboratory / Interpretation',
        value: record.Interpretation,
        hasValue: (record.Count_LaboratoryImages > 0)
      });

      record.SOAPRecords.push({
        label: 'Plan',
        value: '',
        hasValue: (record.Count_Plan > 0)
      });

      record.SOAPRecords.push({
        label: 'Diagnosis',
        value: record.Diagnosis,
        hasValue: (record.Count_Diagnosis > 0)
      });

      record.SOAPRecords.push({
        label: 'Treatment',
        value: record.Treatment,
        hasValue: (record.Count_Treatment > 0)
      });

      record.SOAPRecords.push({
        label: 'Prescription',
        value: record.Prescription,
        hasValue: (record.Count_Prescription > 0)
      });

      record.SOAPRecords.push({
        label: 'Client Communication',
        value: record.ClientCommunication,
        hasValue: (record.Count_ClientCommunication > 0)
      });

    });
  }

  initFilterFields() {
    var details: DetailView_Detail[] = [
      {
        ID_PropertyType: PropertyTypeEnum.Date,
        ID_ControlType: ControlTypeEnum.DatePicker,
        Name: "Date",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Code",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name_Client",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name_Patient",
      },
    ];

    this.appForm.details = details;
  }

  getIsIncludeFilterList(fieldName): boolean {

    return this.appForm.IncludeFilterList.includes(fieldName);
  }
  getFilterString(): string {

    var filterString = this.getDefaultFilterString();

    if (!isNullOrUndefined(this.CurrentObject["ID_SOAPType"])) {

      if (filterString.length > 0) filterString = filterString + " AND ";
      filterString += `ISNULL(ID_SOAPType, 0) = ${this.CurrentObject["ID_SOAPType"]}`;
    }

    if (!isNullOrUndefined(this.CurrentObject["ID_FilingStatus"])) {

      if (filterString.length > 0) filterString = filterString + " AND ";
      filterString += `ISNULL(ID_FilingStatus, 0) = ${this.CurrentObject["ID_FilingStatus"]}`;
    }

    if (!isNullOrUndefined(this.CurrentObject["Date"])) {

      var _dateStart = this.CurrentObject["Date"][0];
      var _dateEnd = this.CurrentObject["Date"][1];

      if (filterString.length > 0) filterString = filterString + " AND ";

      if (_dateStart != null && _dateEnd == null) {

        filterString += `CONVERT(DATE, Date) = '${moment(_dateStart).format('YYYY-MM-DD')}'`;
      } else {

        filterString += `
            CONVERT(DATE, Date) BETWEEN
               '${moment(_dateStart).format('YYYY-MM-DD')}' AND
               '${moment(_dateEnd).format('YYYY-MM-DD')}'
        `;
      }
    } else {

      if (filterString.length == 0) {

        filterString += `
        CONVERT(DATE, Date) BETWEEN
          '${moment(_dateStart).format('YYYY-MM-DD')}' AND
          '${moment(_dateEnd).format('YYYY-MM-DD')}'
      `;
      }
    }

    return filterString;
  }

  getLabel(): string {

    var _label = "";

    var _dateStart;
    var _dateEnd;

    if (!this.MinMax) {

      _dateStart = null
    }
    else {

      _dateStart = this.MinMax["MinValue"];
      _dateEnd = this.MinMax["MaxValue"];
    }

    if (_dateStart == null) _dateStart = new Date()

    if (_dateStart != null && _dateEnd == null) {

      _dateEnd = _dateStart;
    }

    _label = `From ${moment(_dateStart).format('MM/DD/YYYY')} to ${moment(_dateEnd).format('MM/DD/YYYY')}`;
    return _label;
  }

  async newRecord() {
    this.gotoCustomDetailViewRecord(-1);
  }

  tableRowDetail_onClick(record: any) {
    this.gotoCustomDetailViewRecord(record.ID, null, record);
  }

  btnNewTab_OnClick(record: any) {
    this.gotoCustomDetailViewRecord(record.ID, true);
  }

  gotoCustomDetailViewRecord(id, isNewTab?, record?: any) {
    var routeLink = [];

    var config = {
      BackRouteLink: [`/Main`, "PatientSOAPList"],
    };

    if (record) config["ID_Client"] = record.ID_Client;

    if (isNullOrUndefined(isNewTab)) isNewTab = false;
    routeLink = [`/Main`, "Patient_SOAP", id];

    if (isNewTab) {

      var url = this.globalFx.customserializeUrl(routeLink, config);
      window.open(url, '_blank');
    } else {

      this.globalFx.customNavigate(routeLink, config);
    }
  }

  initializeMenuItems() {
    this.menuItems.push(this._InitMenuItem_New);
    this.menuItems.push(this._InitMenuItem_Refresh);
  }

}
