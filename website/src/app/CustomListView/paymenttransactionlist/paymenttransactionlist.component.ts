import { AppointmentSchedule, APP_DETAILVIEW } from 'src/bo/APP_MODELS';
import { Patient_DTO, PropertyTypeEnum } from 'src/utils/controls/class_helper';
import { Component, ElementRef, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { ListViewModalService } from 'src/app/View/ListView/ListView.service';
import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { BaseCustomListView } from '../BaseCustomListView';

@Component({
  selector: 'app-paymenttransactionlist',
  templateUrl: './paymenttransactionlist.component.html',
  styleUrls: ['./paymenttransactionlist.component.less']
})
export class PaymenttransactionlistComponent extends BaseCustomListView {

  initFilterFields() {
    var details = [
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Code",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name_Client",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name_Patient",
      },
    ];

    this.appForm.details = details;
  }

  async loadRecords() {
    this.initFilterFields();

    var sql = "";
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `/*encryptsqlstart*/
            SELECT  ID,
                    Date,
                    Code,
                    DateString,
                    Name_Client,
                    Name_Patient,
                    Name_PaymentMethod,
                    PayableAmount,
                    PaymentAmount,
                    ChangeAmount,
                    Name_FilingStatus
            FROM dbo.vPaymentTransaction
            /*encryptsqlend*/
            WHERE 
              ID_Company = ${this.currentUser.ID_Company} 
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  async newRecord() {

    this.gotoCustomDetailViewRecord(-1);
  }

  tableRowDetail_onClick(record: any){

    this.gotoCustomDetailViewRecord(record.ID);
  }

  gotoCustomDetailViewRecord(id){

    var routeLink = [];
    var config = {
      
      BackRouteLink: [`/Main`, "PaymentTransactions"],
    };
    
    routeLink = [`/Main`, 'PaymentTransaction', id];
    this.globalFx.customNavigate(routeLink, config);
  }

}
