import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { DialogBoxComponent } from 'src/app/dialog-box/dialog-box';
import { IFormValidation, MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { IAppSelectBoxOption } from 'src/utils/controls/appSelectBox/appSelectBox.component';
import { FilingStatusEnum } from 'src/utils/controls/class_helper';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';

@Component({
  selector: 'appointment-schedule-status',
  templateUrl: './appointment-schedule-status.component.html',
  styleUrls: ['./appointment-schedule-status.component.less']
})
export class AppointmentScheduleStatusComponent implements OnInit {

  @ViewChild("CustomDialogBox")
  CustomDialogBox: DialogBoxComponent;

  adjustCreditOption: string = 'Deposit';
  IsDeposit: boolean = true;

  private currentUser: TokenSessionFields = new TokenSessionFields();

  CurrentObject: any = {}

  Appointment_ID_FilingStatus_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt(`Select
                                  ID,
                                  Name
                                FROM vAppointment_FilingStatus
                                `),
  };

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected cs: CrypterService,
    protected toastr: ToastrService
  ) {

    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  ngOnInit(): void {


  }

  async open<T>(obj: any): Promise<T[]> {

    this.CurrentObject = obj;

    return this.CustomDialogBox.open();
  }

  async onPositiveButtonClick(modal: any) {

    var isvalid = await this.validateRecord();
    if (!isvalid) return;

    await this.updateAppointmentStatus();

    modal.close()
  }

  private async validateRecord() {

    var isValid = true;
    var validationsAppForm: IFormValidation[] = [];

    var Appointment_ID_FilingStatus = this.CurrentObject.Appointment_ID_FilingStatus;
    var Appointment_CancellationRemarks = this.CurrentObject.Appointment_CancellationRemarks;

    if (Appointment_ID_FilingStatus == null) Appointment_ID_FilingStatus = 0;
    if (Appointment_CancellationRemarks == null) Appointment_CancellationRemarks = '';

    Appointment_CancellationRemarks = Appointment_CancellationRemarks.trim();

    if (Appointment_ID_FilingStatus == 0) {

      validationsAppForm.push({
        message: 'Appointment Status is required.'
      });
    }

    if (this.CurrentObject.Appointment_ID_FilingStatus == FilingStatusEnum.Cancelled) {

      if (Appointment_CancellationRemarks.length == 0) {

        validationsAppForm.push({
          message: 'Reason for Cancellation is required.'
        });
      }
    }

    if (validationsAppForm.length > 0) {
      this.msgBox.showValidationsBox(validationsAppForm);
      isValid = false;
    }

    return isValid;
  }

  async updateAppointmentStatus(): Promise<any> {

    if (this.CurrentObject.Appointment_ID_FilingStatus != FilingStatusEnum.Cancelled) {

      this.CurrentObject.Appointment_CancellationRemarks = "";
    }

    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pDoUpdateAppointmentStatus",
        {
          Oid_Model: this.CurrentObject.Oid_Model,
          Appointment_ID_CurrentObject: this.CurrentObject.Appointment_ID_CurrentObject,
          Appointment_ID_FilingStatus: this.CurrentObject.Appointment_ID_FilingStatus,
          ID_UserSession: this.currentUser.ID_UserSession,
          Remarks: this.CurrentObject.Appointment_CancellationRemarks
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {

        this.msgBox.success(
          `Status has been change successfully.`,
          `Appointment Status`
        );
        res(obj);
      } else {
        this.msgBox.error(
          obj.message,
          `Failed to Change Status`
        );
        rej(obj);
      }
    });
  }
}
