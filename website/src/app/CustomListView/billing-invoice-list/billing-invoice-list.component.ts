import { AppointmentSchedule, APP_DETAILVIEW } from "src/bo/APP_MODELS";
import { FilingStatusEnum, Patient_DTO, PropertyTypeEnum } from "src/utils/controls/class_helper";
import { Component, ElementRef, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomListView } from "../BaseCustomListView";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { isNullOrUndefined } from "util";

@Component({
  templateUrl: "./billing-invoice-list.component.html",
  styleUrls: ["./billing-invoice-list.component.less"],
})
export class BillingInvoiceListComponent extends BaseCustomListView {
 
  initFilterFields() {
    var details = [
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Code",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "AttendingPhysician_Name_Employee",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name_Client",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "PatientNames",
      },
      {
        ID_PropertyType: PropertyTypeEnum.Int,
        Name: "ID_FilingStatus",
      },
    ];

    this.appForm.details = details;
  }

  ID_FilingStatus_SelectBoxOption: IAppSelectBoxOption = {

    sourceKey: this.cs.encrypt("Select ID, Name FROM tFilingStatus WHERE ID IN(1, 4, 2, 11, 12, 13)"),
  };

  async loadRecords() {
    this.initFilterFields();

    var sql = "";
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    sql = `/*encryptsqlstart*/
            SELECT ID,
                Date,
                Code,
                Name_Client,
                Name_Patient,
                PatientNames,
                GrossAmount,
                VatAmount,
                NetAmount,
                SubTotal,
                DiscountAmount,
                TotalAmount,
                RemainingAmount,
                DateCreated,
                CreatedBy_Name_User,
                DateModified,
                LastModifiedBy_Name_User,
                DateApproved,
                AttendingPhysician_Name_Employee,
                ApprovedBy_Name_User,
                CanceledBy_Name_User,
                DateCanceled,
                ID_FilingStatus,
                Payment_ID_FilingStatus,
                Name_FilingStatus,
                Payment_Name_FilingStatus,
                DateString,
                DateCreatedString,
                DateModifiedString,
                DateApprovedString,
                DateCanceledString
            FROM dbo.vBillingInvoice
            /*encryptsqlend*/
            WHERE 
              ID_Company = ${this.currentUser.ID_Company} AND 
              ISNULL(IsWalkIn, 0) = 0
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {

    var filterString = this.getDefaultFilterString();

    this.appForm.IncludeFilterList.forEach((filterName) => {
      if (filterName == "ID_FilingStatus") {
 
        if (filterString.length > 0) filterString = filterString + " AND ";

        if (isNullOrUndefined(this.CurrentObject["ID_FilingStatus"]))
          this.CurrentObject["ID_FilingStatus"] = 0;

        if(this.CurrentObject["ID_FilingStatus"] == FilingStatusEnum.Filed || 
           this.CurrentObject["ID_FilingStatus"] == FilingStatusEnum.Cancelled){
        
          filterString += `ISNULL(ID_FilingStatus, 0) = ${this.CurrentObject["ID_FilingStatus"]} `;
     
        }else if(
          this.CurrentObject["ID_FilingStatus"] == FilingStatusEnum.Pending || 
          this.CurrentObject["ID_FilingStatus"] == FilingStatusEnum.Done || 
          this.CurrentObject["ID_FilingStatus"] == FilingStatusEnum.PartiallyPaid ||
          this.CurrentObject["ID_FilingStatus"] == FilingStatusEnum.FullyPaid
        ){

          filterString += `ISNULL(ID_FilingStatus, 0) = 3 AND 
                           ISNULL(Payment_ID_FilingStatus, 0) = ${this.CurrentObject["ID_FilingStatus"]} `;
        }
      }
      
    });

    return filterString;
  }

  async newRecord() {
    this.gotoCustomDetailViewRecord(-1);
  }

  tableRowDetail_onClick(record: any) {
    this.gotoCustomDetailViewRecord(record.ID);
  }

  gotoCustomDetailViewRecord(id) {
    var routeLink = [];
    var config = {
      BackRouteLink: [`/Main`, "BillingInvoices"],
    };

    routeLink = [`/Main`, "BillingInvoice", id];
    this.globalFx.customNavigate(routeLink, config);
  }
}
