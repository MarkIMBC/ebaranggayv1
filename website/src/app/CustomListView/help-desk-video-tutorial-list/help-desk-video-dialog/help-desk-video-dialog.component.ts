import { Component, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { DialogBoxComponent } from 'src/app/dialog-box/dialog-box';
import { APP_MODEL } from 'src/bo/APP_MODELS';
import { IFormValidation, MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { ClientCredit } from 'src/utils/controls/class_helper';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'help-desk-video-dialog',
  templateUrl: './help-desk-video-dialog.component.html',
  styleUrls: ['./help-desk-video-dialog.component.less']
})
export class HelpDeskVideoDialogComponent implements OnInit {

  @ViewChild("CustomDialogBox")
  CustomDialogBox: DialogBoxComponent;

  private currentUser: TokenSessionFields = new TokenSessionFields();
  id: number = 0;
  videoURL: SafeResourceUrl; 

  CurrentObject: any = {}

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected cs: CrypterService,
    protected toastr: ToastrService,
    protected sanitizer: DomSanitizer
  ) {

    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  ngOnInit(): void {


  }

  async Open<T>(ID_HelpDeskVideoTutorial: number): Promise<T[]> {

    this.id = ID_HelpDeskVideoTutorial;

    this.loadRecord();

    return this.CustomDialogBox.open();
  }

  async onPositiveButtonClick(modal: any) {

    modal.close()
  }

  async loadRecord(): Promise<any> {

    return new Promise<any[]>(async (res, rej) => {

      var obj = await this.ds.execSP(
        "pGetHelpDeskVideoTutorial",
        {
          ID: this.id,
          ID_Session: this.currentUser.ID_Session
        },
        {
          isReturnObject: true,
        }
      );

      this.CurrentObject = obj;

      this.videoURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.CurrentObject.VideoLink);
    });
  }

}

