import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { DialogBoxComponent } from 'src/app/dialog-box/dialog-box';
import { ListViewModalService } from 'src/app/View/ListView/ListView.service';
import { Payable, PayablePayment, Payable_Detail } from 'src/bo/APP_MODELS';
import { AppFormComponent } from 'src/utils/controls/appForm/appForm.component';
import { IFormValidation, MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { IAppSelectBoxOption } from 'src/utils/controls/appSelectBox/appSelectBox.component';
import { FilingStatusEnum, PayablePayment_DTO } from 'src/utils/controls/class_helper';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'payable-payment-dialog-box',
  templateUrl: './payable-payment-dialog-box.component.html',
  styleUrls: ['./payable-payment-dialog-box.component.less']
})
export class PayablePaymentDialogBoxComponent implements OnInit {

  CurrentObject: PayablePayment_DTO = new PayablePayment_DTO();
  PreviousObject: PayablePayment_DTO = new PayablePayment_DTO();

  title: string = '';

  selectedPayableDetail: Payable_Detail = new Payable_Detail();

  @ViewChild("CustomDialogBox")
  CustomDialogBox: DialogBoxComponent;

  @ViewChild("appForm")
  appForm: AppFormComponent;

  private currentUser: TokenSessionFields = new TokenSessionFields();

  private __ID_CurrentObject: number = -1
  private _ID_Payable: number = -1

  ID_ExpenseCategory_SelectBoxOption: IAppSelectBoxOption = {

    sourceKey: this.cs.encrypt("Select ID, Name FROM tExpenseCategory")
  };

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected lvModal: ListViewModalService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected cs: CrypterService,
    protected toastr: ToastrService
  ) {

    this.currentUser = this.userAuthSvc.getDecodedToken();

  }

  ngOnInit(): void {
  }

  async getRecord() {

    var obj = await this.ds.execSP(
      "pGetPayablePayment",
      {
        ID: this.__ID_CurrentObject,
        ID_Payable: this._ID_Payable,
        ID_Session: this.currentUser.ID_Session
      },
      {
        isReturnObject: true,
      }
    );

    this.CurrentObject = obj;
    this.__ID_CurrentObject = obj.ID;

    if (this.CurrentObject.ID > 0) {

      this.title = '';
    } else {

      this.title = 'Pay ' + this.CurrentObject.Name_Payable_Detail + '';
    }

    this.PreviousObject = this.globalFx.cloneObject(obj);

    var isformDisabled = this.formDisabled();
    if (!isNullOrUndefined(this.appForm))
      this.appForm.setFormDisabled(isformDisabled);
  }

  protected formDisabled(): boolean {
    var isDisabled: boolean = false;

    if (this.CurrentObject.ID_FilingStatus != FilingStatusEnum.Filed) {
      isDisabled = true;
    }

    return isDisabled;
  }

  public async save() {

    if (
      isNullOrUndefined(this.CurrentObject["ID_Company"]) &&
      this.CurrentObject.ID < 1
    ) {
      this.CurrentObject.ID_Company = this.currentUser.ID_Company;
    }

    var r = await this.ds.saveObject(
      'EFD5E1AC-D635-403D-9FDF-561135C50B49',
      this.CurrentObject,
      this.PreviousObject,
      [],
      this.currentUser
    );
    var id = (r.key + "").replace("'", "");
    var ID_CurrentObject = parseInt(id);
    this.__ID_CurrentObject = ID_CurrentObject;

    this.CurrentObject.ID = this.__ID_CurrentObject;


    return true;
  }

  async CreateByPayable<T>(id: number): Promise<T[]> {

    this.__ID_CurrentObject = -1;
    this._ID_Payable = id;

    this.getRecord();

    return this.CustomDialogBox.open();
  }

  async onPositiveButtonClick(modal: any) {

    var isvalid;


    this.compute();

    isvalid = await this.validateRecord();
    if (!isvalid) return;

    var result = await this.msgBox.confirm(
      "Would you like to Payment?",
      `Payment`,
      "Yes",
      "No"
    );

    if (!result) return;

    var isSave = await this.save();

    if (isSave) {

      await this.doApprove();
      modal.close()
    }
  }

  async doApprove(): Promise<any> {
    return new Promise<any>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pApprovePayablePayment",
        {
          IDs_PayablePayment: [this.__ID_CurrentObject],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `Payment Successfull`,
          `Pay`
        );
        res(obj);
      } else {

        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(
          `Payment failed`,
          `Pay`
        );
        rej(obj);
      }
    });
  }

  private async validateRecord() {
    var isValid = true;

    var validationsAppForm: IFormValidation[] = [];

    if (!isNullOrUndefined(this.appForm)) {
      validationsAppForm = await this.appForm.getValidations();
    }

    if (this.CurrentObject.TotalAmount < 1) {

      validationsAppForm.push({
        message: `Amount must be valid.`
      });
    }

    if (this.CurrentObject.RemaningAmount < this.CurrentObject.TotalAmount) {

      validationsAppForm.push({
        message: `Amount must not more than Remaining Amount.`
      });
    }

    if (validationsAppForm.length > 0) {
      this.msgBox.showValidationsBox(validationsAppForm);
      isValid = false;
    }

    return isValid;
  }

  private compute() {

    var totalAmount: number = 0;

    totalAmount = this.CurrentObject.CashAmount;
    this.CurrentObject.TotalAmount = totalAmount;
  }

}
