import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { MenuItem } from "primeng/api";
import { Model } from "src/bo/APP_MODELS";
import {
  AppFormComponent,
  IFilterFormValue,
} from "src/utils/controls/appForm/appForm.component";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { isNullOrUndefined } from "util";
import {
  TokenSessionFields,
  UserAuthenticationService,
} from "../AppServices/UserAuthentication.service";
import { ListViewModalService } from "../View/ListView/ListView.service";
import { ResizedEvent } from "angular-resize-event";
import { PropertyTypeEnum } from "src/utils/controls/class_helper";
import { PagingOption } from "../../utils/look-up-dialog-box/look-up-dialog-box.component";
import { Paginator } from "primeng/paginator";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { LoaderService } from "../AppServices/LoaderInterceptor";

@Component({
  template: ''
})
export abstract class BaseCustomListView implements OnInit {

  
  model: Model;

  InitCurentObject: any = {};
  CurrentObject: any = {};

  hasBackLinkConfigOption: boolean = true;

  currentUser: TokenSessionFields;

  menuItems: MenuItem[] = [];

  configOptions: any = {};

  dataSource: any[] = [];
  MinMax: any = {};

  AccordionFieldTabs: any = {};

  selectedRecord: any;

  showFilterBox: boolean = false;

  pagingOption: PagingOption = new PagingOption();

  TotalRecordCount: number;

  LoadedRecordCount: number;

  mainboxHeight: string = "300px";

  OrderByString: string = "";

  MinMaxColumn: string = "";

  @ViewChild("appForm")
  appForm: AppFormComponent;

  @ViewChild("mainbox", { read: ElementRef, static: false })
  targetElement: ElementRef;

  @ViewChild('paginator', { static: true }) paginator: Paginator

  isLoading: boolean = false;

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected lvModal: ListViewModalService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected cs: CrypterService,
    protected toastr: ToastrService,
    protected loaderService: LoaderService,
    protected elRef?: ElementRef
  ) {
    this.currentUser = this.userAuthSvc.getDecodedToken();

    window.onresize = (e) => {
      this.resize();
    };
  }

  protected resize() {

    var height =
      parseInt(this.elRef.nativeElement.parentNode.offsetHeight) - 200;
    this.mainboxHeight = height.toString() + "px";
  }

  label: string = ''

  ngOnInit(): void {

    this.loaderService.isLoading.subscribe((r) => {
      this.isLoading = r;
    })

    this.initializeMenuItems();

    this.CurrentObject.IsActive = 1;

    this.pagingOption.DisplayCount = 50;

    this.CurrentObject = JSON.parse(JSON.stringify(this.InitCurentObject));
  }

  getconfigOptions() {

    this.configOptions = {

      showFilterBox: this.showFilterBox,
      CurrentObject: this.CurrentObject,
      IncludeFilterList: this.appForm.IncludeFilterList
    };

    return this.configOptions;
  }

  IsActive_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt(`
      /*encryptsqlstart*/
      SELECT  t.ID, 
              t.Name
      FROM    ( 
        SELECT -1 ID, 'All' Name UNION 
        SELECT 1 ID, 'Active' Name UNION 
        SELECT 0 ID, 'Inactive' Name  
      ) t 
      /*encryptsqlend*/
    `),
  };


  initFilterFields() { }

  private loadConfigOption() {
    var configOptionsString = this.route.snapshot.params["configOptions"];

    if (isNullOrUndefined(configOptionsString)) return;

    configOptionsString = this.cs.decrypt(configOptionsString);
    this.configOptions = JSON.parse(configOptionsString);

    if (!isNullOrUndefined(this.configOptions['CurrentObject'])) this.CurrentObject = this.configOptions['CurrentObject'];
    if (!isNullOrUndefined(this.configOptions['showFilterBox'])) this.showFilterBox = this.configOptions['showFilterBox'];

    if (!isNullOrUndefined(this.configOptions['IncludeFilterList'])) {

      this.appForm.IncludeFilterList = this.configOptions['IncludeFilterList'];
    }

    this.hasBackLinkConfigOption = !isNullOrUndefined(
      this.configOptions["BackRouteLink"]
    );
  }

  toggeFilterBox(): void {
    this.showFilterBox = !this.showFilterBox;
  }

  clearFilter() {

    this.CurrentObject = JSON.parse(JSON.stringify(this.InitCurentObject));
    this.appForm.clearFilter();
    this.loadRecords();
  }

  async searchFilter() {

    await this.loadRecords();
    this.paginator.changePage(0);
  }

  protected _InitMenuItem_Refresh: MenuItem = {
    label: "Refresh",
    icon: "pi pi-fw pi-refresh green-text",
    command: async () => {

      await this.loadRecords();
    },
  };

  protected _InitMenuItem_New: MenuItem = {
    label: "New",
    icon: "pi pi-fw pi-plus",
    command: async () => {
      await this.newRecord();
    },
  };

  protected getDefaultFilterString(): string {
    var filterString = "";
    var formFilters: IFilterFormValue[] = [];

    formFilters = this.appForm.getFilterFormValue();

    formFilters.forEach((formFilter) => {
      switch (formFilter.propertyType) {
        case PropertyTypeEnum.String:
          filterString =
            filterString + this.getFilterStringByName(formFilter.dataField);
          break;
      }
    });

    return filterString;
  }

  protected getFilterStringByName(filterName: string) {
    var filterString = "";

    if (filterString.length > 0) filterString = filterString + " AND ";

    if (isNullOrUndefined(this.CurrentObject[filterName]))
      this.CurrentObject[filterName] = "";

    if (this.CurrentObject[filterName].toString().length > 0) {

      var value = this.CurrentObject[filterName];

      value = value.replace(/^\s+/, "");
      value = value.replace(/\s+$/, "");

      filterString += `${filterName} LIKE '%${value}%' `;
    }

    return filterString;
  }

  getFilterString(): string {
    var filterString = this.getDefaultFilterString();

    return filterString;
  }

  initializeMenuItems() {
    this.menuItems.push(this._InitMenuItem_New);
    this.menuItems.push(this._InitMenuItem_Refresh);
  }

  loadRecords() { }

  async getRecordPaging(query) {

    var obj = await this.ds.execSP(
      "pGetRecordPaging",
      {
        sql: query,
        PageNumber: this.pagingOption.PageNumber,
        DisplayCount: this.pagingOption.DisplayCount,
        OrderByString: this.OrderByString,
        MinMaxColumn: this.MinMaxColumn
      },
      {
        isReturnObject: true,
      }
    );

    if (isNullOrUndefined(obj)) {

      obj = {};
      obj.Records = [];
      obj.TotalRecord = 0;
      obj.TotalPageNumber = 0;
    }

    if (isNullOrUndefined(obj.Records)) obj.Records = [];

    this.pagingOption.TotalRecord = obj.TotalRecord;
    this.pagingOption.TotalPageNumber = obj.TotalPageNumber;

    this.dataSource_InitLoad(obj);

    this.dataSource = obj.Records;
    this.TotalRecordCount = obj.TotalRecord;

    this.LoadedRecordCount = this.dataSource.length < this.pagingOption.DisplayCount ? this.dataSource.length : this.pagingOption.DisplayCount;
    this.LoadedRecordCount = (this.pagingOption.DisplayCount * (this.pagingOption.PageNumber - 1)) + this.dataSource.length;

    if(obj['MinMax']){

      this.MinMax = {
        MinValue: obj['MinMax'][0]['MinValue'],
        MaxValue: obj['MinMax'][0]['MaxValue'],
      }
    }

    this.label = this.getLabel();
  }

  dataSource_InitLoad(obj: any) {


  }

  getLabel(): string {

    return '';
  }

  filterOnKeyUp(event) {

    if (event.keyCode == 13) this.loadRecords();
  }

  filterOnChange(event) {

    this.loadRecords();
  }

  accordionfilterClick(event) {


  }

  newRecord() { }

  paginate(event) {

    this.pagingOption.PageNumber = event.page + 1;

    this.loadRecords();
  }

  tableHeader_onClick(columnName: string){

    if(this.OrderByString.length == 0 || this.OrderByString != columnName){
      
        this.OrderByString = this.OrderByString.search(" DESC") < 0 ? columnName: '';
    }else{

      this.OrderByString = columnName + ' DESC';
    }

    this.loadRecords();
  }

  tableRowDetail_onClick(record) { }

  async ngAfterViewInit() {

    this.loadConfigOption();

    /*this.appForm.controls.forEach((c) => {

      var isHasFilterList = false;

      if(!isNullOrUndefined(this.configOptions["IncludeFilterList"])){

        isHasFilterList = this.configOptions.IncludeFilterList.includes(c.name);
      }

      this.AccordionFieldTabs[c.name] = isHasFilterList;
    });*/

    await this.loadRecords();

    this.resize();
  }
}
