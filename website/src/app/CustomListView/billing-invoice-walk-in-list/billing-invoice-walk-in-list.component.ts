import { AppointmentSchedule, APP_DETAILVIEW } from "src/bo/APP_MODELS";
import {
  FilingStatusEnum,
  ItemTypeEnum,
  Patient_DTO,
  PropertyTypeEnum,
} from "src/utils/controls/class_helper";
import { Component, ElementRef, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomListView } from "../BaseCustomListView";
import { IFilterFormValue } from "src/utils/controls/appForm/appForm.component";
import { isNullOrUndefined } from "util";
import { MenuItem } from "primeng/api";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";

@Component({
  selector: 'app-billing-invoice-walk-in-list',
  templateUrl: './billing-invoice-walk-in-list.component.html',
  styleUrls: ['./billing-invoice-walk-in-list.component.less']
})
export class BillingInvoiceWalkInListComponent extends BaseCustomListView {

  initFilterFields() {
    var details = [
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Code",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "AttendingPhysician_Name_Employee",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "Name_Client",
      },
      {
        ID_PropertyType: PropertyTypeEnum.String,
        Name: "PatientNames",
      },
      {
        ID_PropertyType: PropertyTypeEnum.Int,
        Name: "ID_FilingStatus",
      },
    ];

    this.appForm.details = details;
  }

  InitCurentObject: any = {

    Name: '',
    ID_FilingStatus: FilingStatusEnum.Pending,
  }

  ID_FilingStatus_SelectBoxOption: IAppSelectBoxOption = {

    sourceKey: this.cs.encrypt("Select ID, Name FROM tFilingStatus WHERE ID IN (2, 14, 15, 4)"),
  };

  async loadRecords() {
    this.initFilterFields();

    var formFilters: IFilterFormValue[] = [];
    var filterString = this.getFilterString();

    if (filterString.length > 0) filterString = " AND " + filterString;

    var sql = `/*encryptsqlstart*/
            SELECT ID,
                Date,
                Code,
                Name_Client,
                Name_Patient,
                PatientNames,
                GrossAmount,
                VatAmount,
                NetAmount,
                SubTotal,
                DiscountAmount,
                TotalAmount,
                RemainingAmount,
                DateCreated,
                CreatedBy_Name_User,
                DateModified,
                LastModifiedBy_Name_User,
                DateApproved,
                AttendingPhysician_Name_Employee,
                ApprovedBy_Name_User,
                CanceledBy_Name_User,
                DateCanceled,
                ID_FilingStatus,
                Payment_ID_FilingStatus,
                Name_FilingStatus,
                Payment_Name_FilingStatus,
                DateString,
                DateCreatedString,
                DateModifiedString,
                DateApprovedString,
                DateCanceledString,
                WalkInCustomerName
            FROM dbo.vBillingInvoice
            /*encryptsqlend*/
            WHERE 
              ID_Company = ${this.currentUser.ID_Company} AND 
              IsWalkIn = 1
              ${filterString}
          `;

    this.getRecordPaging(sql);
  }

  getFilterString(): string {

    var filterString = this.getDefaultFilterString();

    this.appForm.IncludeFilterList.forEach((filterName) => {
      if (filterName == "ID_FilingStatus") {

        if (filterString.length > 0) filterString = filterString + " AND ";

        if (isNullOrUndefined(this.CurrentObject["ID_FilingStatus"]))
          this.CurrentObject["ID_FilingStatus"] = 0;

        if (this.CurrentObject["ID_FilingStatus"] == FilingStatusEnum.Filed ||
          this.CurrentObject["ID_FilingStatus"] == FilingStatusEnum.Cancelled) {

          filterString += `ISNULL(ID_FilingStatus, 0) = ${this.CurrentObject["ID_FilingStatus"]} `;

        } else if (
          this.CurrentObject["ID_FilingStatus"] == FilingStatusEnum.Pending ||
          this.CurrentObject["ID_FilingStatus"] == FilingStatusEnum.Done ||
          this.CurrentObject["ID_FilingStatus"] == FilingStatusEnum.PartiallyPaid ||
          this.CurrentObject["ID_FilingStatus"] == FilingStatusEnum.FullyPaid
        ) {

          filterString += `ISNULL(ID_FilingStatus, 0) = 3 AND 
                           ISNULL(Payment_ID_FilingStatus, 0) = ${this.CurrentObject["ID_FilingStatus"]} `;
        }
      }

    });

    return filterString;
  }

  async newRecord() {
    this.gotoCustomDetailViewRecord(-1);
  }

  tableRowDetail_onClick(record: any) {
    this.gotoCustomDetailViewRecord(record.ID);
  }

  btnNewTab_OnClick(record: any) {
    this.gotoCustomDetailViewRecord(record.ID, true);
  }

  gotoCustomDetailViewRecord(id, isNewTab?) {
    var routeLink = [];

    var config = {
      BackRouteLink: [`/Main`, "BillingInvoiceWalkInList"],
      IsWalkIn: true
    };

    if (isNullOrUndefined(isNewTab)) isNewTab = false;
    routeLink = [`/Main`, "BillingInvoice", id];

    if (isNewTab) {
      var url = this.globalFx.customserializeUrl(routeLink, config);
      window.open(url, "_blank");
    } else {
      this.globalFx.customNavigate(routeLink, config);
    }
  }

  initializeMenuItems() {
    this.menuItems.push(this._InitMenuItem_New);
    this.menuItems.push(this._InitMenuItem_Refresh);
  }

}
