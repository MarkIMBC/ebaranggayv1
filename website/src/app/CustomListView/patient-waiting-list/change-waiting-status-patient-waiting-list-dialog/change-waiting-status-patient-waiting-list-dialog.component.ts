import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { DialogBoxComponent } from 'src/app/dialog-box/dialog-box';
import { APP_MODEL } from 'src/bo/APP_MODELS';
import { IAppSelectBoxOption } from 'src/utils/controls/appSelectBox/appSelectBox.component';
import { FilingStatusEnum } from 'src/utils/controls/class_helper';
import { MessageBoxService } from '../../../../utils/controls/appModal/appModal.component';
import { CrypterService } from '../../../../utils/service/Crypter.service';
import { DataService } from '../../../../utils/service/data.service';
import { GlobalfxService } from '../../../../utils/service/globalfx.service';

@Component({
  selector: 'change-waiting-status-patient-waiting-list-dialog',
  templateUrl: './change-waiting-status-patient-waiting-list-dialog.component.html',
  styleUrls: ['./change-waiting-status-patient-waiting-list-dialog.component.less']
})
export class ChangeWaitingStatusPatientWaitingListDialogComponent implements OnInit {

  @ViewChild("CustomDialogBox")
  CustomDialogBox: DialogBoxComponent;
  private CurrentObject: any = {};

  private _ID_PatientWaitingList: number;
  _WaitingStatus_ID_FilingStatus: number;
  private currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected cs: CrypterService,
    protected toastr: ToastrService
  ) {

    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  ngOnInit(): void {
  }

  WaitingStatus_ID_FilingStatus_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt(`Select 
                                  ID, 
                                  Name 
                                FROM tFilingStatus
                                WHERE ID IN (
                                  ${FilingStatusEnum.Ongoing},
                                  ${FilingStatusEnum.Waiting},
                                  ${FilingStatusEnum.Done},
                                  ${FilingStatusEnum.Cancelled}
                                )
                                `),
  };

  async WaitingStatus_ID_FilingStatus_SelectBox_ValueChanged(event: any) {

  }

  async open<T>(ID_PatientWaitingList: number): Promise<T[]> {

    var obj = await this.ds.loadObject<any>(APP_MODEL.PATIENTWAITINGLIST, ID_PatientWaitingList + '', {});
    this.CurrentObject = obj;


    return this.CustomDialogBox.open();
  }

  async onPositiveButtonClick(modal: any) {

    await this.updateStatus();

    modal.close()
  }

  async updateStatus(): Promise<any> {

    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pUpdatePatientToWaitingListStatus",
        {
          IDs_PatientWaitingList: [this.CurrentObject.ID],
          WaitingStatus_ID_FilingStatus: this.CurrentObject.WaitingStatus_ID_FilingStatus,
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {

        this.msgBox.success(
          `Status has been change successfully.`,
          `Waiting Status`
        );
        res(obj);
      } else {
        this.msgBox.error(
          obj.message,
          `Failed to Change Status`
        );
        rej(obj);
      }
    });
  }

}
