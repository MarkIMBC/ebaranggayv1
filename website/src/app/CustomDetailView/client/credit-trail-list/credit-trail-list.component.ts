import { Component, EventEmitter, OnInit, Output, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { MenuItem } from "primeng/api";
import {
  TokenSessionFields,
  UserAuthenticationService,
} from "src/app/AppServices/UserAuthentication.service";
import { FormHelperDialogService } from "src/app/View/FormHelperView/FormHelperView.component";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { Client } from "src/bo/APP_MODELS";
import { ClientCreditAdjustmentDialogBoxComponent } from "src/utils/client-credit-adjustment/client-credit-adjustment.component";
import { MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import {
  ClientDeposit_DTO,
  CreditModeEnum,
} from "src/utils/controls/class_helper";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";

@Component({
  selector: "credit-trail-list",
  templateUrl: "./credit-trail-list.component.html",
  styleUrls: ["./credit-trail-list.component.less"],
})
export class CreditTrailListComponent implements OnInit {
  private currentUser: TokenSessionFields = new TokenSessionFields();
  private _ID_Client: number = 0;

  @Output() onSavedCreditAmount = new EventEmitter<any>();

  @ViewChild("clientcreditadjustmentdialogbox")
  clientcreditadjustmentdialogbox: ClientCreditAdjustmentDialogBoxComponent;

  menuItems: MenuItem[] = [];
  CurrentCreditAmount: number = 0;

  dataSource: ClientDeposit_DTO[] = [];
  selectedRecord: ClientDeposit_DTO = null;

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected lvModal: ListViewModalService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected cs: CrypterService,
    protected toastr: ToastrService,
    private dvFormSvc: FormHelperDialogService
  ) {
    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  ngOnInit(): void {}

  async loadByID(ID_Client: number) {
    this._ID_Client = ID_Client;

    this.loadMenuItems();

    this.loadRecords();
    this.loadRemaningCredit();
  }

  private loadMenuItems() {
    this.menuItems = [];

    var menuItemDepositCredit: MenuItem = {
      label: "Deposit Credit",
      icon: "pi pi-fw pi-plus blue-text",
      command: async () => {
        if (!this.clientcreditadjustmentdialogbox) return;

        this.clientcreditadjustmentdialogbox.setCreditMode(
          CreditModeEnum.Deposit
        );
        this.clientcreditadjustmentdialogbox.Load(this._ID_Client).then(() => {
          this.loadRecords();
          this.loadRemaningCredit();
        });
      },
    };

    var menuItemWithdrawCredit: MenuItem = {
      label: "Withdraw Credit",
      icon: "pi pi-fw pi-minus red-text",
      command: async () => {
        if (!this.clientcreditadjustmentdialogbox) return;

        this.clientcreditadjustmentdialogbox.setCreditMode(
          CreditModeEnum.Withdraw
        );
        this.clientcreditadjustmentdialogbox.Load(this._ID_Client).then(() => {
          this.loadRecords();
          this.loadRemaningCredit();
        });
      },
    };

    this.menuItems.push(menuItemDepositCredit);
    this.menuItems.push(menuItemWithdrawCredit);
  }

  async loadRecords() {
    this.dataSource = [];

    if (this._ID_Client < 0) return;

    var sql = `SELECT *
               FROM dbo.vClientCredit_ListView
               WHERE 
                 ID_Client = ${this._ID_Client}
    `;

    sql = this.cs.encrypt(sql);
    this.dataSource = await this.ds.query<any>(sql);
  }

  async loadRemaningCredit() {
    this.dataSource = [];

    if (this._ID_Client < 0) return;

    var sql = `SELECT CurrentCreditAmount
               FROM dbo.vClient
               WHERE 
                 ID = ${this._ID_Client}
    `;

    sql = this.cs.encrypt(sql);
    var records = await this.ds.query<Client>(sql);

    this.CurrentCreditAmount = 0;
    if (records.length > 0) {
      this.CurrentCreditAmount = records[0].CurrentCreditAmount;
    }
  }

  List_onRowSelect(e: any) {}

  List_onRowUnselect(e: any) {}

  clientcreditadjustmentdialogbox_onSavedCreditAmount(e: any){

    this.onSavedCreditAmount.emit(e);
  }
}
