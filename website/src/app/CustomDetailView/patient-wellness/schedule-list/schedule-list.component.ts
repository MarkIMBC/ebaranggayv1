import { Component, OnInit, ChangeDetectionStrategy, Input, SimpleChanges, ViewChild, EventEmitter, Output } from '@angular/core';
import { Enumerable } from 'linq-typescript';
import { ToastrService } from 'ngx-toastr';
import { MenuItem } from 'primeng/api';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { Item, Patient_Wellness_Schedule } from 'src/bo/APP_MODELS';
import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { Item_DTO } from 'src/utils/controls/class_helper';
import { LookUpDialogBoxComponent } from 'src/utils/look-up-dialog-box/look-up-dialog-box.component';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'schedule-list',
  templateUrl: './schedule-list.component.html',
  styleUrls: ['./schedule-list.component.less'],
})
export class ScheduleListComponent implements OnInit {

  @ViewChild("itemServiceDialog")
  itemServiceDialogCmp: LookUpDialogBoxComponent;

  _wellnesss: Patient_Wellness_Schedule[] = [];

  @Input() IsDisabled: boolean = false;

  @Input() set Wellnesss(objs: Patient_Wellness_Schedule[]) {

    this.loadMenuItems();

    if (!objs) return;

    this._wellnesss = objs;
  }

  get Wellnesss() {

    return this._wellnesss;
  }

  @Output() onChanged = new EventEmitter<any>();

  private currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected cs: CrypterService,
    protected toastr: ToastrService
  ) {

    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  menuItems: MenuItem[] = [];

  ngOnInit(): void {

    this.loadMenuItems();
  }

  loadMenuItems() {

    var menuItems_AddSchedule: MenuItem = {
      label: 'Add Schedule',
      icon: "pi pi-fw pi-plus",
      command: () => {

        var obj: Patient_Wellness_Schedule = {};

        if (!this._wellnesss) this._wellnesss = [];

        this._wellnesss.push(obj);
      }
    }

    this.menuItems = [];
    this.menuItems.push(menuItems_AddSchedule);
  }

  onChange() {

    this.onChanged.emit({

      data: this._wellnesss
    });
  }

  onRowDeleting(treatment: Patient_Wellness_Schedule) {

    if (this.IsDisabled) return;

    var index = this.globalFx.findIndexByKeyValue(
      this._wellnesss,
      "ID",
      treatment.ID + ""
    );

    this._wellnesss.splice(index, 1);

    this.onChanged.emit({

      data: this._wellnesss
    });
  }


}

