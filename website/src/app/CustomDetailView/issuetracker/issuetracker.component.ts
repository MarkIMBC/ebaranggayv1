import { DialogBoxComponent } from "../../dialog-box/dialog-box";
import {
  ControlTypeEnum,
  FilingStatusEnum,
  ItemTypeEnum,
  ReceiveInventory,
} from "./../../../utils/controls/class_helper";
import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { APP_MODEL, Item } from "src/bo/APP_MODELS";
import { IFormValidation, MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { Item_DTO } from "src/utils/controls/class_helper";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomDetailView } from "../BaseCustomDetailView";
import { PurchaseOrderDetailNames } from "../purchase-order/purchase-order.component";
import { MenuItem } from "primeng/api";
import { FormHelperDialogService } from "src/app/View/FormHelperView/FormHelperView.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { isNullOrUndefined, isNumber } from "util";

@Component({
  selector: 'app-issuetracker',
  templateUrl: './issuetracker.component.html',
  styleUrls: ['./issuetracker.component.less']
})
export class IssuetrackerComponent extends BaseCustomDetailView implements OnInit {

  currentModelID: string = APP_MODEL.ISSUETRACKER;

  CurrentObjectDetailNames: string[] = [];
  
  loadMenuItem() {

    var menuItemApprove: MenuItem = {
      label: "Approve",
      icon: "pi pi-fw pi-save blue-text",
      command: async () => {
       
        if (this.appForm.isDirty) {
          this.msgBox.warning(
            `${this.CurrentObject.Code} changes is not yet saved.`
          );
          return;
        }

        var result = await this.msgBox.confirm(
          `Would you like to approve ${this.CurrentObject.Code}?`,
          "Billing Invoice",
          "Yes",
          "No"
        );
        if (!result) return;

        var obj = await this.approve();

        if (obj.Success) {
          await this.loadCurrentRecord();
        }
      },
    };

    this.addMenuItem(menuItemApprove);
  }

  async approve(): Promise<any> {
    return new Promise<Item[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pApproveIssueTracker",
        {
          IDs_IssueTracker: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.CurrentObject.Code} has been approved successfully.`,
          `Approved ${this.model.Caption}`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(
          obj.message,
          `Failed to Approve ${this.model.Caption}`
        );
        rej(obj);
      }
    });
  }

  CurrentObject_onLoad(){

    this.loadMenuItem();
  }

  protected formDisabled(): boolean {
    var isDisabled: boolean = false;

    if (this.CurrentObject.ID_FilingStatus != FilingStatusEnum.Filed) {
      isDisabled = true;
    }

    return isDisabled;
  }
}
