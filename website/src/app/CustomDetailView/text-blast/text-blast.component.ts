import { DialogBoxComponent } from "../../dialog-box/dialog-box";
import {
  ControlTypeEnum,
  FilingStatusEnum,
  ItemTypeEnum,
  ReceiveInventory,
  TextBlast_Client_DTO,
} from "./../../../utils/controls/class_helper";
import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { APP_MODEL, Client, Item } from "src/bo/APP_MODELS";
import { IFormValidation, MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { Item_DTO } from "src/utils/controls/class_helper";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomDetailView } from "../BaseCustomDetailView";
import { PurchaseOrderDetailNames } from "../purchase-order/purchase-order.component";
import { MenuItem } from "primeng/api";
import { FormHelperDialogService } from "src/app/View/FormHelperView/FormHelperView.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { isNullOrUndefined, isNumber } from "util";
import { Enumerable } from "linq-typescript";
import { LookUpDialogBoxComponent } from "src/utils/look-up-dialog-box/look-up-dialog-box.component";
import { LoaderService } from "src/app/AppServices/LoaderInterceptor";

@Component({
  selector: 'app-text-blast',
  templateUrl: './text-blast.component.html',
  styleUrls: ['./text-blast.component.less']
})
export class TextBlastComponent extends BaseCustomDetailView implements OnInit {

  currentModelID: string = APP_MODEL.TEXTBLAST;

  CurrentObjectDetailNames: string[] = [
    TextBlastDetailNames.TextBlast_Client,
  ];

  clientMenuItems: MenuItem[] = [];

  selClient: TextBlast_Client_DTO;

  @ViewChild("LookUpDialog")
  LookUpDialogCmp: LookUpDialogBoxComponent;

  ID_CompanyTextBlastTemplate_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt(`Select ID, Name, Template FROM tCompanyTextBlastTemplate  WHERE ID_Company = ${this.currentUser.ID_Company}`),
  };

  async validation(): Promise<IFormValidation[]> {

    var validations: IFormValidation[] = [];

    if (isNullOrUndefined(this.CurrentObject.Message))
      this.CurrentObject.Message = '';

    if (this.CurrentObject.Message.length == 0) {
      validations.push({
        message: "Message is required",
      });
    }

    if (this.CurrentObject.Message.length > 160) {
      validations.push({
        message: "Message must be atleast 160 characters and below.",
      });
    }

    return Promise.resolve(validations);
  }

  protected loadInitMenu() {
    this.addMenuItem(this._InitMenuItem_New);

    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
      this.addMenuItem(this._InitMenuItem_Save);
    }

    if (this.CurrentObject.ID > 0) {
      this.addMenuItem(this._InitMenuItem_Refresh);
    }
  }

  protected formDisabled(): boolean {
    var isDisabled: boolean = false;

    if (this.CurrentObject.ID_FilingStatus != FilingStatusEnum.Filed) {
      isDisabled = true;
    }

    return isDisabled;
  }

  loadClientMenuItems() {

    this.clientMenuItems = [];

    var menuItemAddClient: MenuItem = {
      label: "Add Client",
      icon: "pi pi-fw pi-file blue-text",
      command: async () => {

        this.doAddClient();
      },
    };
    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {

      this.clientMenuItems.push(menuItemAddClient);
    }
  }

  loadMenuItem() {
    var menuItemApprove: MenuItem = {
      label: "Approve",
      icon: "pi pi-fw pi-save blue-text",
      command: async () => {

        var validations: IFormValidation[] = [];

        if (this.appForm.isDirty) {
          this.msgBox.warning(
            `${this.CurrentObject.Code} changes is not yet saved.`
          );
          return;
        }

        if (validations.length > 0) {

          var validation = validations[0];

          this.msgBox.warning(
            `${validation.message}`,
            `Validate ${this.model.Caption}`
          );
          return;
        }

        var result = await this.msgBox.confirm(
          `Would you like to Send ${this.CurrentObject.Code}?`,
          "Text Blast",
          "Yes",
          "No"
        );
        if (!result) return;

        var obj = await this.approve();

        if (obj.Success) {
          await this.loadCurrentRecord();
        }
      },
    };

    var menuItemCancel: MenuItem = {
      label: "Cancel",
      icon: "pi pi-fw pi-times red-text",
      command: async () => {
        if (this.appForm.isDirty) {
          this.msgBox.warning(
            `${this.CurrentObject.Code} changes is not yet saved.`
          );
          return;
        }

        var result = await this.msgBox.confirm(
          `Would you like to cancel ${this.CurrentObject.Code}?`,
          "Text Blast",
          "Yes",
          "No"
        );
        if (!result) return;

        var obj = await this.cancel();

        if (obj.Success) {
          await this.loadCurrentRecord();
        }
      },
    };

    if (this.CurrentObject.ID < 1) return;
    if (this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Filed) {
      this.addMenuItem(menuItemApprove);
      this.addMenuItem(menuItemCancel);
    } else if (
      this.CurrentObject.ID_FilingStatus == FilingStatusEnum.Approved
    ) {

      this.addMenuItem(menuItemCancel);
    }
  }

  async approve(): Promise<any> {
    return new Promise<Item[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pApproveTextBlast",
        {
          IDs_TextBlast: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.CurrentObject.Code} has been queued for sending.`,
          `Approved Text Blast`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(
          obj.message,
          `Failed to Send ${this.model.Caption}`
        );
        rej(obj);
      }
    });
  }

  async cancel(): Promise<any> {
    return new Promise<Item[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "pCancelTextBlast",
        {
          IDs_TextBlast: [this.CurrentObject.ID],
          ID_UserSession: this.currentUser.ID_UserSession,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `${this.CurrentObject.Code} has been canceled successfully.`,
          `Cancel Text Blast`
        );
        res(obj);
      } else {
        obj.message = this.globalFx.convertStringToHTML(obj.message);

        this.msgBox.error(
          obj.message,
          `Failed to Cancel ${this.model.Caption}`
        );
        rej(obj);
      }
    });
  }

  browseClient(): Promise<Client[]> {
    return new Promise<Client[]>(async (res, rej) => {

      var strIDs_currentIDClients = Enumerable.fromSource(this.CurrentObject.TextBlast_Client)
        .select((r) => r['ID_Client'])
        .toArray()
        .toString();

      if (!strIDs_currentIDClients) strIDs_currentIDClients = "0"

      var selectedRecords = await this.LookUpDialogCmp.open<Client>({
        sql: `SELECT 
                      ID, 
                      Name
              FROM dbo.vClient 
              WHERE 
                  IsActive = 1 AND
                  ID NOT IN (${strIDs_currentIDClients})    
              `,
        height: "300px",
        filterColumns: [{ name: "Name" }],
        columns: [
          {
            name: "ID",
            width: "50px",
            align: "center",
          },
          {
            name: "Name",
            width: "200px",
          },
        ],
      });

      if (isNullOrUndefined(selectedRecords)) selectedRecords = [];

      if (selectedRecords.length == 0) {
        res([]);
      } else {
        var strIDs_Item = Enumerable.fromSource(selectedRecords)
          .select((r) => r.ID)
          .toArray()
          .toString();

        var queryString = this.cs.encrypt(`
          SELECT  *
          FROM tClient
          WHERE 
            ID IN (${strIDs_Item})
        `);

        var items = await this.ds.query<Client>(queryString);

        res(items);
      }
    });
  }
  async doAddClient() {
    var selectedItems = await this.browseClient();

    selectedItems.forEach((record) => {
      var obj = {
        ID: this.globalFx.getTempID(),
        ID_Client: record.ID,
        Name_Client: record.Name,
        ID_Company: this.currentUser.ID_Company
      };

      this.CurrentObject.TextBlast_Client.push(obj);
    });

  }

  async ID_CompanyTextBlastTemplate_SelectBox_ValueChanged(event: any) {

    this.CurrentObject.Message = event['Template'];
  }

  CurrentObject_onLoad() {

    this.loadClientMenuItems();
    this.loadMenuItem();


    this.ID_CompanyTextBlastTemplate_SelectBoxOption.sourceKey =  this.cs.encrypt(`
                                                                                    Select ID, Name, Template 
                                                                                    FROM tCompanyTextBlastTemplate   
                                                                                    WHERE ID_Company = ${this.currentUser.ID_Company}
                                                                                  `);

  }

  onRowDeletingTxbClient(detail: TextBlast_Client_DTO) {

    if (this.appForm.isDisabled) return;

    var index = this.globalFx.findIndexByKeyValue(
      this.CurrentObject.TextBlast_Client,
      "ID",
      detail.ID + ""
    );

    this.CurrentObject.TextBlast_Client.splice(index, 1);
  }
}
export class TextBlastDetailNames {
  static readonly TextBlast_Client?: string = "TextBlast_Client";
}