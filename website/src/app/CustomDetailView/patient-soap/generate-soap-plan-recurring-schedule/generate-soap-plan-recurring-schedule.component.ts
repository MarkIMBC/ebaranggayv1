import { Component, OnInit, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { DialogBoxComponent } from 'src/app/dialog-box/dialog-box';
import { IFormValidation, MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { IAppSelectBoxOption } from 'src/utils/controls/appSelectBox/appSelectBox.component';
import { Patient_SOAP_Plan_DTO } from 'src/utils/controls/class_helper';
import { IValueChanged } from 'src/utils/controls/_base/baseAppControl';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'generate-soap-plan-recurring-schedule',
  templateUrl: './generate-soap-plan-recurring-schedule.component.html',
  styleUrls: ['./generate-soap-plan-recurring-schedule.component.less']
})
export class GenerateSoapPlanRecurringScheduleComponent implements OnInit {

  @ViewChild("CustomDialogBox")
  CustomDialogBox: DialogBoxComponent;

  private currentUser: TokenSessionFields = new TokenSessionFields();
  private _ID_Employee: number = 0;

  scheduleDataSource: Schedule[]

  _dayCount: number = 0;

  _selectedSOAP: any = {};

  CurrentObject: any = {}

  ID_RecurScheduleTypeEnum_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt(`
      Select ID,
             Name,
             DayCount 
      FROM tRecurScheduleType
    `),
  };

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected cs: CrypterService,
    protected toastr: ToastrService
  ) {

    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  ngOnInit(): void { }

  async Load<Patient_SOAP_Plan_DTO>(selectedSOAP: Patient_SOAP_Plan_DTO): Promise<any[]> {

    this._selectedSOAP = selectedSOAP;

    this.CurrentObject = {

      ID_Employee: this._ID_Employee
    };
 
    return this.CustomDialogBox.open();
  }

  DateReturn_Changed(e: any){

    this.loadGenerateScheduleList();
  }

  async ID_RecurScheduleType_SelectBox_ValueChanged(e: any) {

    if(event){

      this._dayCount = e.DayCount;
    }

    this.loadGenerateScheduleList();
  }

  onPositiveButtonClick(e: any){


  }

  loadGenerateScheduleList(){

    this.scheduleDataSource = [];

    var dateReturn = this.CurrentObject.DateReturn;
    var date = moment(dateReturn).add(this._dayCount, 'days');

    console.log(dateReturn, date)
  }
}

class Schedule{

  ID?: number;
  Date: Date;
}