import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Output, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { MenuItem } from 'primeng/api';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { Patient_Vaccination_DTO, FilingStatusEnum } from 'src/utils/controls/class_helper';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';

@Component({
  selector: 'patient-vaccination-list',
  templateUrl: './patient-vaccination-list.component.html',
  styleUrls: ['./patient-vaccination-list.component.less']
})
export class PatientVaccinationListComponent implements OnInit {

  _ID_Pattent_SOAP: number = 0;
  Records: Patient_Vaccination_DTO[] = []
  SelectedRecord: Patient_Vaccination_DTO

  @Input() CurrentObject: any = {};
  @Input() DetailViewConfigOptions: any = {};
  @Input() IsDirty:boolean = false;
  @Input() IsDisabled:boolean = false;

  @Output() onChanged = new EventEmitter<any>();

  private currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected cs: CrypterService,
    protected toastr: ToastrService
  ) {

    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  menuItems: MenuItem[] = [];

  ngOnInit(): void {

    this.loadMenuItems();
  }

  async LoadBySOAP(ID_Patient_SOAP: number) {

    this._ID_Pattent_SOAP = ID_Patient_SOAP;

    this.loadMenuItems();

    var sql = `SELECT *
               FROM dbo.vPatient_Vaccination_Listview
               WHERE 
                ID_Patient_SOAP = ${this._ID_Pattent_SOAP} AND
                ID_FilingStatus IN (${FilingStatusEnum.Filed})
      `;

    sql = this.cs.encrypt(sql);
    this.Records = await this.ds.query<any>(sql);

    this.loadMenuItems();

  }

  loadMenuItems() {

    var menuItems_New: MenuItem = {
      label: 'Create',
      icon: "pi pi-fw pi-plus",
      command: () => {

        if (this.IsDirty) {
          this.msgBox.warning(
            `Patient Medical Record changes is not yet saved.`
          );
          return;
        }

        var routeLink = [];
        var config = {

          prevConfig: this.DetailViewConfigOptions,
          ID_Patient_SOAP: this._ID_Pattent_SOAP,
          BackRouteLink: [`/Main`, 'Patient_SOAP', this._ID_Pattent_SOAP]
        };

        routeLink = [`/Main`, 'Patient_Vaccination', -1];
        this.globalFx.customNavigate(routeLink, config);
      }
    }

    var menuItems_View: MenuItem = {
      label: 'View',
      icon: 'pi pi-fw pi-search',
      command: () => {

        if (this.IsDirty) {
          this.msgBox.warning(
            `Patient Medical Record changes is not yet saved.`
          );
          return;
        }

        if (!this.SelectedRecord) return;

        var routeLink = [];
        var config = {

          prevConfig: this.DetailViewConfigOptions,
          ID_Patient_SOAP: this._ID_Pattent_SOAP,
          BackRouteLink: [`/Main`, 'Patient_SOAP', this._ID_Pattent_SOAP]
        };

        routeLink = [`/Main`, 'Patient_Vaccination', this.SelectedRecord.ID];
        this.globalFx.customNavigate(routeLink, config);
      }
    }

    var menuItems_Refresh: MenuItem = {
      label: 'Refresh',
      icon: 'pi pi-fw pi-refresh',
      command: () => {

        this.LoadBySOAP(this._ID_Pattent_SOAP);
      }
    }

    this.menuItems = [];
    
    if(this.CurrentObject.ID < 1) return;

    this.menuItems.push(menuItems_New);

    this.menuItems.push(menuItems_View);

    this.menuItems.push(menuItems_Refresh);
  }

  recordGrid_onDblClick() {

    if (!this.SelectedRecord) return;

    var routeLink = [];
    var config = {

      prevConfig: this.DetailViewConfigOptions,
      ID_Patient_SOAP: this._ID_Pattent_SOAP,
      BackRouteLink: [`/Main`, 'Patient_SOAP', this._ID_Pattent_SOAP]
    };

    routeLink = [`/Main`, 'Patient_Vaccination', this.SelectedRecord.ID];
    this.globalFx.customNavigate(routeLink, config);
  }

  recordGrid_onRowSelect() {

    this.loadMenuItems();
  }

  recordGrid_onRowUnselect() {

    this.loadMenuItems();
  }
}
