import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import {
  IFormValidation,
  MessageBoxService,
} from "src/utils/controls/appModal/appModal.component";
import {
  UserAuthenticationService,
  TokenSessionFields,
} from "../AppServices/UserAuthentication.service";
import { ListViewModalService } from "../View/ListView/ListView.service";
import {
  ActivatedRoute,
  ActivationEnd,
  NavigationEnd,
  NavigationStart,
  Router,
} from "@angular/router";
import { CrypterService } from "src/utils/service/Crypter.service";
import { ToastrService } from "ngx-toastr";
import {
  BillingInvoice_DTO,
  FilingStatusEnum,
  IFile,
} from "src/utils/controls/class_helper";
import { Model } from "src/bo/APP_MODELS";
import { Component, OnInit, ViewChild } from "@angular/core";
import { AppFormComponent } from "src/utils/controls/appForm/appForm.component";
import { MenuItem } from "primeng/api";
import { isNull, isNullOrUndefined } from "util";
import * as moment from "moment";
import { DetailViewFooterComponent } from "./detail-view-footer/detail-view-footer.component";
import { BaseAppControl, BaseAppControlOption } from "src/utils/controls";
import { Breadcrumb } from "primeng/breadcrumb";
import { IControlValueChanged } from "src/utils/controls/_base/baseAppControl";
import { LoaderService } from "../AppServices/LoaderInterceptor";
import { ValidationService } from "src/utils/service/validation.service";
import { FormHelperDialogService } from "../View/FormHelperView/FormHelperView.component";

@Component({
  template: "",
})
export abstract class BaseCustomDetailView implements OnInit {
  currentModelID: string = "";

  protected __ID_CurrentObject: number = 0;

  qrCodeValue: string = "";
  currentRouterLink: string = "";

  isLoading: boolean = false;

  breadCrumbs: MenuItem[] = [];
  homeBreadCrumb: MenuItem = { icon: "pi pi-home", routerLink: "/Main" };
  gotoMenuItems: MenuItem[] = [];

  hasBackLinkConfigOption: boolean = true;

  CurrentObjectFiles: IFile[] = [];

  ModelDisplayName: string = "";

  footerLeftMessageHtml: String = "";
  footerRightMessageHtml: String = "";

  CurrentObject: any = {};
  PreviousObject: any = {};

  configOptions: any = {};

  CurrentObjectDetailNames: string[] = [];

  model: Model;
  currentUser: TokenSessionFields;

  @ViewChild("appForm")
  appForm: AppFormComponent;

  @ViewChild("detailviewfooter")
  detailviewfooter: DetailViewFooterComponent;

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected lvModal: ListViewModalService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected cs: CrypterService,
    protected toastr: ToastrService,
    protected loaderService: LoaderService,
    protected validationService: ValidationService,
    protected dvFormSvc: FormHelperDialogService
  ) {
    this.msgBox.setToastTimeOut(5000);
    this.currentUser = this.userAuthSvc.getDecodedToken();
    this.formatCurrentObjectDetails();

    this.gotoMenuItems = [];
  }

  async ngOnInit(): Promise<void> {
    this.loaderService.isLoading.subscribe((r) => {
      this.isLoading = r;
    });

    this.loadConfigOption();

    this.__ID_CurrentObject = await this._getDefault__ID_CurrentObject();

    await this.loadCurrentModel();
    await this.loadCurrentRecord();

    if (this.CurrentObject["ID_Company"] != undefined) {
      if (this.CurrentObject.ID_Company != this.currentUser.ID_Company) {
        this.router.navigate(["/Main", "NoAccess"]);
        return;
      }
    }

    this.CustomDetailView_onInit();
  }

  setqrCodeValue() {
    var value = `{ 	"ID_CurrentObject": "${this.CurrentObject.ID}", 	"ID_Model": "${this.currentModelID}" }`;

    this.qrCodeValue = value;
  }

  protected async _getDefault__ID_CurrentObject() {
    return parseInt(this.route.snapshot.params["ID_CurrentObject"]);
  }

  private loadConfigOption() {
    var configOptionsString = this.route.snapshot.params["configOptions"];

    if (isNullOrUndefined(configOptionsString)) return;

    configOptionsString = this.cs.decrypt(configOptionsString);

    this.configOptions = JSON.parse(configOptionsString);

    console.log(this.configOptions);

    this.hasBackLinkConfigOption = !isNullOrUndefined(
      this.configOptions["BackRouteLink"]
    );
  }

  public async LoadCurrentObjectRecord(ID_CurrentObject: number) {
    this.__ID_CurrentObject = ID_CurrentObject;
    await this.loadCurrentModel();
    await this.loadRecord(ID_CurrentObject);
  }

  isLoadCurrrentObject: boolean = true;

  protected async loadRecord(ID_CurrentObject: number) {
    this.isLoadCurrrentObject = true;

    var recordOptions = await this.pGetRecordOptions();

    recordOptions["ID"] = ID_CurrentObject;
    recordOptions["ID_Session"] = this.currentUser.ID_UserSession;

    var obj = await this.ds.execSP(`pGet${this.model.Name}`, recordOptions, {
      isReturnObject: true,
    });

    this.PreviousObject = this.globalFx.cloneObject(obj);

    obj = await this.getInitCurrentObject(obj);
    this.CurrentObject = this.globalFx.cloneObject(obj);

    this.formatCurrentObjectDetails();

    if (!isNullOrUndefined(this.appForm)) this.appForm.setDirty(false);

    this.menuItems = [];
    this.loadInitMenu();

    if (!isNullOrUndefined(this.detailviewfooter)) {
      if (this.detailviewfooter.reload)
        this.detailviewfooter.reload(this.CurrentObject);
    }

    var isformDisabled = this.formDisabled();
    if (!isNullOrUndefined(this.appForm))
      this.appForm.setFormDisabled(isformDisabled);

    this.CurrentObject_onLoad();

    if (ID_CurrentObject > 0) this.setqrCodeValue();

    this.isLoadCurrrentObject = false;
  }

  protected getInitCurrentObject(obj): any {
    return obj;
  }

  protected pGetRecordOptions(): any {
    return {};
  }

  protected formDisabled(): boolean {
    var isDisabled: boolean = false;

    return isDisabled;
  }

  protected _InitMenuItem_New: MenuItem = {
    label: "New",
    icon: "pi pi-fw pi-plus",
    command: async () => {
      var result = await this.msgBox.confirm(
        "Would you like to create new record?",
        `Create ${
          isNull(this.model.DisplayName)
            ? this.model.Name
            : this.model.DisplayName
        }`,
        "Yes",
        "No"
      );

      if (!result) return;

      this.gotoSelfNavigate(-1);

      this.loadNewRecord();
    },
  };

  protected gotoSelfNavigate(ID_CurrentObject) {
    if (isNullOrUndefined(this.route.snapshot.params["ID_CurrentObject"]))
      return;

    var routeLink = [];
    routeLink = [`/Main`, this.model.Name, ID_CurrentObject];

    this.globalFx.customNavigate(routeLink, this.configOptions);
  }

  protected _InitMenuItem_Refresh: MenuItem = {
    label: "Refresh",
    icon: "pi pi-fw pi-refresh green-text",
    command: async () => {
      if (this.CurrentObject.ID < 0) return;

      var result = await this.msgBox.confirm(
        "Would you reload current record?",
        `Refresh ${
          isNull(this.model.DisplayName)
            ? this.model.Name
            : this.model.DisplayName
        } Record`,
        "Yes",
        "No"
      );

      if (!result) return;

      this.loadCurrentRecord();
    },
  };

  private _IsSaving: boolean = true;

  protected _InitMenuItem_Save: MenuItem = {
    label: "Save",
    icon: "pi pi-fw pi-save green-text",
    command: async () => {
      var isvalid = true;

      isvalid = await this.validateRecord();
      if (!isvalid) return;

      this.CustomDetailView_onBeforeSaving();

      var isSaving = await this.confirmSaving();
      if (!isSaving) return;

      var isSavingSuccessfull = true;
      var isLoadSuccessfull = false;

      try {
        await this.save();
      } catch (error) {

        this.msgBox.error('Unable to save record. Please try again.', this.model.Caption);
        isSavingSuccessfull = false;
      }

      if (isSavingSuccessfull) {
        try {
          await this.loadRecord(this.__ID_CurrentObject);
        } catch (error) {
          location.reload();
        }

        await this.CustomDetailView_onAfterSaved();
        this.gotoSelfNavigate(this.__ID_CurrentObject);
      }
    },
  };

  public async confirmSaving(): Promise<boolean> {
    var result = await this.msgBox.confirm(
      "Do you want to save changes?",
      this.ModelDisplayName,
      "Yes",
      "No"
    );

    return result;
  }

  public async validateRecord() {
    var isValid = true;

    var validationsAppForm: IFormValidation[] = [];
    var CustomValidations: IFormValidation[] = await this.validation();

    if (!isNullOrUndefined(this.appForm)) {
      validationsAppForm = await this.getFormValidations();
    }

    CustomValidations.forEach((customValidation) => {
      validationsAppForm.push(customValidation);
    });

    if (validationsAppForm.length > 0) {
      this.msgBox.showValidationsBox(validationsAppForm);
      isValid = false;
    }

    return isValid;
  }

  public async save() {
    if (
      isNullOrUndefined(this.CurrentObject["ID_Company"]) &&
      this.CurrentObject.ID < 1
    ) {
      this.CurrentObject.ID_Company = this.currentUser.ID_Company;
    }

    this.CurrentObjectFiles = [];

    if (this.appForm != undefined) {
      this.appForm.controls.forEach((i) => {
        if (i["imageFile"]) {
          this.CurrentObjectFiles.push({
            file: i["imageFile"],
            dataField: i.name,
            isImage: true,
          });
        }
      });
    }

    var r = await this.ds.saveObject(
      this.model.Oid,
      this.CurrentObject,
      this.PreviousObject,
      this.CurrentObjectFiles,
      this.currentUser
    );
    var id = (r.key + "").replace("'", "");
    var ID_CurrentObject = parseInt(id);
    this.__ID_CurrentObject = ID_CurrentObject;

    await this.CustomDetailView_onAfterSaving();
  }

  protected loadInitMenu() {
    this.addMenuItem(this._InitMenuItem_New);
    this.addMenuItem(this._InitMenuItem_Save);

    if (this.CurrentObject.ID > 0) {
      this.addMenuItem(this._InitMenuItem_Refresh);
    }
  }

  protected addMenuItem(item: MenuItem) {
    this.menuItems.push(item);
  }

  private async loadNewRecord() {
    this.__ID_CurrentObject = -1;

    await this.loadRecord(this.__ID_CurrentObject);
  }

  async loadCurrentRecord() {
    await this.loadRecord(this.__ID_CurrentObject);
  }

  protected formatCurrentObjectDetails() {
    this.CurrentObjectDetailNames.forEach((name) => {
      if (isNullOrUndefined(this.CurrentObject[name]))
        this.CurrentObject[name] = [];

      if (isNullOrUndefined(this.PreviousObject[name]))
        this.PreviousObject[name] = [];

      if (this.CurrentObject[name] == undefined) this.CurrentObject[name] = [];

      if (this.PreviousObject[name] == undefined)
        this.PreviousObject[name] = [];
    });
  }

  protected _ModelDisplayName: string = "";

  private async loadCurrentModel() {
    var obj = await this.ds.execSP(
      "pGetModel",
      {
        Oid: this.currentModelID,
      },
      {
        isReturnObject: true,
      }
    );

    this.model = obj;

    this._ModelDisplayName = isNull(this.model.DisplayName)
      ? this.model.Name
      : this.model.DisplayName;

    this.ModelDisplayName = this._ModelDisplayName;
  }

  menuItems: MenuItem[] = [];

  private initMenus: MenuItem[] = [];

  private async getFormValidations(): Promise<IFormValidation[]> {
    var validations = this.appForm.getValidations();
    return Promise.resolve(validations);
  }

  protected async validation(): Promise<IFormValidation[]> {
    var validations: IFormValidation[] = [];

    return Promise.resolve(validations);
  }

  private initControls: any[] = [];

  ngAfterViewInit() {
    this.appForm.controls.forEach((c) => {
      this.initControls.push({
        name: c.name,
        readOnly: c.readOnly,
      });
    });

    this.appForm.initControls = this.initControls;

    //console.log('ngAfterViewInit', this.initControls);

    this.CustomDetailView_onLoad();
  }

  btnBack_onClick() {
    var routeLink = [];
    var prevConfig;

    if (!isNullOrUndefined(this.configOptions["prevConfig"])) {
      prevConfig = this.configOptions["prevConfig"];
    }

    if (isNullOrUndefined(this.configOptions["BackRouteLink"])) return;

    routeLink = this.configOptions["BackRouteLink"];
    this.globalFx.customNavigate(routeLink, prevConfig);
  }

  CustomDetailView_onInit() {}

  CustomDetailView_onLoad() {}

  CurrentObject_onLoad() {}

  CustomDetailView_onBeforeSaving() {}

  CustomDetailView_onAfterSaving() {}

  async CustomDetailView_onAfterSaved() {
    this.msgBox.success("Save successfully", this.model.Caption);
  }
}
