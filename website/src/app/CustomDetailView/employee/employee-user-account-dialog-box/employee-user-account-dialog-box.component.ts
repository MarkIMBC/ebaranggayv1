import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { TokenSessionFields, UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';
import { DialogBoxComponent } from 'src/app/dialog-box/dialog-box';
import { IFormValidation, MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'employee-user-account-dialog-box',
  templateUrl: './employee-user-account-dialog-box.component.html',
  styleUrls: ['./employee-user-account-dialog-box.component.less']
})
export class EmployeeUserAccountDialogBoxComponent implements OnInit {
  
  @ViewChild("CustomDialogBox")
  CustomDialogBox: DialogBoxComponent;

  private currentUser: TokenSessionFields = new TokenSessionFields();

  private _ID_Employee: number = 0;

  CurrentObject: any = {}

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected cs: CrypterService,
    protected toastr: ToastrService
  ) {

    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  ngOnInit(): void {


  }

  async Load<T>(ID_Employee: number): Promise<T[]> {

    this._ID_Employee = ID_Employee;

    this.CurrentObject = {

      ID_Employee: this._ID_Employee
    };

    this.loadUserAccount();

    return this.CustomDialogBox.open();
  }

  async saveEmployeeUserAccount(): Promise<any> {
    return new Promise<any[]>(async (res, rej) => {
      var obj = await this.ds.execSP(
        "SaveEmployeeUserAccount",
        {
          CompanyID: this.currentUser.ID_Company,
          EmployeeID: this._ID_Employee,
          Username: this.CurrentObject.Username,
          Password: this.CurrentObject.Password,
        },
        {
          isReturnObject: true,
          isTransaction: true,
        }
      );

      if (obj.Success) {
        this.msgBox.success(
          `Saving successfully.`,
          `User Account`
        );
        res(obj);
      } else {
        this.msgBox.error(
          obj.message,
          `Failed to save User Account`
        );
        rej(obj);
      }
    });
  }

  async loadUserAccount(): Promise<any> {
    
    return new Promise<any[]>(async (res, rej) => {

      var obj = await this.ds.execSP(
        "LoadEmployeeUserAccount",
        {
          EmployeeID: this.CurrentObject.ID_Employee
        },
        {
          isReturnObject: true,
        }
      );

      this.CurrentObject = obj;
      this.CurrentObject.ID_Employee = this._ID_Employee;
    });
  }

  async onPositiveButtonClick(modal: any) {

    var isvalid;


    if(!this.CurrentObject['Username']) this.CurrentObject.Username = '';
    if(!this.CurrentObject['Password']) this.CurrentObject.Password = '';

    this.CurrentObject['Username'] = this.CurrentObject['Username'].trim();
    this.CurrentObject['Password'] = this.CurrentObject['Password'].trim();
    

    isvalid = await this.validateRecord();
    if (!isvalid) return;

    var result = await this.msgBox.confirm(
      "Would you like save user account?",
      `User Account`,
      "Yes",
      "No"
    );

    if(!result) return;


    await this.saveEmployeeUserAccount();
    modal.close()
  }

  private async validateRecord() {

    var isValid = true;
    var validationsAppForm: IFormValidation[] = [];

    if(this.CurrentObject.Username.trim().length == 0){

      validationsAppForm.push({
        message: 'Username is required.'
      });
    }

    if(this.CurrentObject.Password.trim().length == 0){

      validationsAppForm.push({
        message: 'Password is required.'
      });
    }

    if (validationsAppForm.length > 0) {
      this.msgBox.showValidationsBox(validationsAppForm);
      isValid = false;
    }

    return isValid;
  }

}
