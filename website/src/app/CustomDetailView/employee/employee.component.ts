import { Company, Employee } from './../../../bo/APP_MODELS';
import { DialogBoxComponent } from "../../dialog-box/dialog-box";
import {
  FilingStatusEnum,
  PositionEnum,
  ReceiveInventory,
} from "./../../../utils/controls/class_helper";
import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { UserAuthenticationService } from "src/app/AppServices/UserAuthentication.service";
import { ListViewModalService } from "src/app/View/ListView/ListView.service";
import { APP_MODEL, Item } from "src/bo/APP_MODELS";
import { IFormValidation, MessageBoxService } from "src/utils/controls/appModal/appModal.component";
import { IAppSelectBoxOption } from "src/utils/controls/appSelectBox/appSelectBox.component";
import { Item_DTO } from "src/utils/controls/class_helper";
import { CrypterService } from "src/utils/service/Crypter.service";
import { DataService } from "src/utils/service/data.service";
import { GlobalfxService } from "src/utils/service/globalfx.service";
import { BaseCustomDetailView } from "../BaseCustomDetailView";
import { MenuItem } from "primeng/api";
import { isNullOrUndefined, isNumber } from "util";
import { AppFormComponent } from 'src/utils/controls/appForm/appForm.component';
import { ValidationService } from 'src/utils/service/validation.service';
import { EmployeeUserAccountDialogBoxComponent } from './employee-user-account-dialog-box/employee-user-account-dialog-box.component';
import { LoaderService } from 'src/app/AppServices/LoaderInterceptor';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.less']
})
export class EmployeeComponent extends BaseCustomDetailView implements OnInit {

  currentModelID: string = APP_MODEL.EMPLOYEE;
  _IsEmployeeInfo: boolean = false;

  @ViewChild('employeeuseraccountdialogbox')
  employeeuseraccountdialogbox: EmployeeUserAccountDialogBoxComponent;

  @ViewChild('appForm')
  appForm: AppFormComponent;

  protected async _getDefault__ID_CurrentObject(){

    if(!isNullOrUndefined(this.route.snapshot.params["ID_CurrentObject"])){

      return parseInt(
        this.route.snapshot.params["ID_CurrentObject"]
      );
    }else{

      this._IsEmployeeInfo = true;
      this.hasBackLinkConfigOption = false;

      return this.currentUser.ID_Employee;
    }
  }
  
  ID_Position_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt("Select ID, Name FROM tPosition"),
  };

  async ID_Position_SelectBox_ValueChanged(event: any) {
    this.CurrentObject.Name_Position = event.Name;
  }

  ID_Gender_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt("Select ID, Name FROM tGender"),
  };

  async ID_Gender_SelectBox_ValueChanged(event: any) {
    this.CurrentObject.Name_Gender = event.Name;
  }
  
  ID_EmployeeStatus_SelectBoxOption: IAppSelectBoxOption = {
    sourceKey: this.cs.encrypt("Select ID, Name FROM tEmploymentStatus"),
  };

  async ID_EmployeeStatus_SelectBox_ValueChanged(event: any) {
    this.CurrentObject.Name_EmploymentStatus = event.Name;
  }

  async Name_TextBox_Changed() {

    var Employee = this.CurrentObject as Employee;

    Employee.Name = ( Employee.LastName ?? "" ) + ", " + ( Employee.FirstName ?? "" )   + " " + ( Employee.MiddleName ?? "" );
  }

  protected async validation(): Promise<IFormValidation[]> {

    var validations: IFormValidation[] = [];
    var Employee = this.CurrentObject as Employee;
    var validations : IFormValidation[] = [];

    if(isNullOrUndefined(Employee.Email)) Employee.Email = '';

    if(Employee.Email.length > 0){

      /* Validate Email Address */
      var isValidEmail = this.validationService.validateEmail(Employee.Email);
      if(isValidEmail != true){

        validations.push({
            message: 'Invalid Email Address Format'
        });
      }
    }

    return Promise.resolve(validations);
  };

  protected loadInitMenu() {

    if(!this._IsEmployeeInfo){

      this.addMenuItem(this._InitMenuItem_New);
      this.addMenuItem(this._InitMenuItem_Save);
      this.addMenuItem(this._InitMenuItem_Refresh);
    }else{
      
      this.addMenuItem(this._InitMenuItem_Save);
      this.addMenuItem(this._InitMenuItem_Refresh);
    }
  }
  
  loadMenuItem() {

    var menuItemCreateUser: MenuItem = {

      label: 'Create User Account',
      icon: 'pi pi-fw pi-user-plus text-green',
      command: async () => {

        this.employeeuseraccountdialogbox.Load(this.CurrentObject.ID);
      }
    };

    var menuItemEditUser: MenuItem = {

      label: 'Edit User Account',
      icon: 'pi pi-fw pi-user-edit text-green',
      command: async () => {

        this.employeeuseraccountdialogbox.Load(this.CurrentObject.ID);
      }
    };

    if(this.CurrentObject['UserAccount_ID_User']){

      this.addMenuItem(menuItemEditUser);
    }

  }

  CurrentObject_onLoad() {

    this.ModelDisplayName = this._IsEmployeeInfo ? this.CurrentObject.Name : this._ModelDisplayName;

    this.loadMenuItem();
  }
}
