import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { MenuItem } from 'primeng/api';
import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from 'src/utils/service/data.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { TokenSessionFields, UserAuthenticationService } from '../AppServices/UserAuthentication.service';

@Component({
  selector: 'app-base-list-view',
  templateUrl: './base-list-view.component.html',
  styleUrls: ['./base-list-view.component.less']
})
export class BaseListViewComponent implements OnInit {

  touchtime=0;
  
  @Output() onChanged = new EventEmitter<any>();

  private currentUser: TokenSessionFields = new TokenSessionFields();

  constructor(
    protected ds: DataService,
    protected globalFx: GlobalfxService,
    protected msgBox: MessageBoxService,
    protected userAuthSvc: UserAuthenticationService,
    protected cs: CrypterService,
    protected toastr: ToastrService
  ) {

    this.currentUser = this.userAuthSvc.getDecodedToken();
  }

  menuItems: MenuItem[] = [];

  ngOnInit(): void {

    this.loadMenuItems();
  }

  loadMenuItems(){


  }

  _recordGrid_onDblClick(){
    
    if (this.touchtime === 0) {
      this.touchtime = new Date().getTime();
    } else {
      if (new Date().getTime() - this.touchtime < 400) {
        this.recordGrid_onDblClick();
        this.touchtime = 0;
      } else {
        this.touchtime = new Date().getTime();
      }
    }
  }

  recordGrid_onDblClick() {


  }
  
  recordGrid_onRowSelect() {

    this.loadMenuItems();
  }

  recordGrid_onRowUnselect() {

    this.loadMenuItems();
  }
}
