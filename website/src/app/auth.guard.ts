import { MessageBoxService } from './../utils/controls/appModal/appModal.component';
import { ListViewComponent } from './View/ListView/ListView.component';
import { DetailViewComponent } from './View/DetailView/DetailView.component';
import { DataService } from 'src/utils/service/data.service';
import { UserAuthenticationService } from './AppServices/UserAuthentication.service';
import { Injectable, ViewContainerRef } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild, CanDeactivate } from '@angular/router';
import { isNullOrUndefined } from 'util';
import { ModuleAccessService, IModuleAccess } from './AppServices/moduleAccess.service';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate, CanActivateChild, CanDeactivate<any>  {
    constructor(
        private router: Router,
        private userAuth: UserAuthenticationService,
        private ds: DataService,
        private modAccess: ModuleAccessService
    ) { }

    canDeactivate(component: any, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): Promise<boolean> {
       
        //console.log('component',component);
        return Promise.resolve(false);
    }

    async canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot):  Promise<boolean> {
        var cmp = childRoute.component;
        var Oid_View:string = null;
        if (isNullOrUndefined(childRoute.params.Oid_ListView) != true) {
            Oid_View = childRoute.params.Oid_ListView;
        } else if (isNullOrUndefined(childRoute.params.Oid_DetailView) != true) {
            Oid_View = childRoute.params.Oid_DetailView;
        } else {

        }
        if ( Oid_View ) {
            Oid_View = atob(Oid_View);
            var mod = await this.ds.get(`Model/GetModuleRights/${Oid_View}`) as IModuleAccess;
            this.modAccess.set(mod);
            return Promise.resolve(true);
        }
        var p = childRoute.params;
        return Promise.resolve(true);
    }

    Deac

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return true;
    }
}   


@Injectable()
export class ModelDirtyGuardService implements CanDeactivate<IDirty> {
    constructor(private msgBox: MessageBoxService) { }
    public async canDeactivate(
        component: IDirty,
        currentRoute: ActivatedRouteSnapshot,
        currentState: RouterStateSnapshot,
        nextState: RouterStateSnapshot
    ): Promise<boolean> {
        let isDirty: boolean = component.isDirty();
        if ( isDirty == true ) {
            if (await this.msgBox.confirm("Do you want to discard changes?", component.getDisplayName(), "Yes", "No") == true) { 
                   return Promise.resolve(true);
            } else{
                return Promise.resolve(false);
            }
        }
        return Promise.resolve(true);
    }
}


export interface IDirty {
    isDirty(): boolean;
    getRef(): ViewContainerRef;
    getDisplayName() : string;
}