import { Component, OnInit } from '@angular/core';
import {ChipsModule} from 'primeng/chips';
import { DataService } from 'src/utils/service/data.service';
import { ListViewModalService } from 'src/app/View/ListView/ListView.service';
import { Employee, User } from 'src/bo/APP_MODELS';

@Component({
  selector: 'app-message-form',
  templateUrl: './message-form.component.html',
  styleUrls: ['./message-form.component.less']
})
export class MessageFormComponent implements OnInit {

  users: User[] = [];

  constructor(
    private lvModal: ListViewModalService,
    private ds: DataService) {

  }

  ngOnInit(): void {

    this.users.push({
      ID:1,
      Name: 'Joseph'
    });

    this.users.push({
      ID:2,
      Name: 'Rose'
    });
  }

  browseUser(searchTag) {

    var promise = new Promise(async (resolve, reject) => {

      var sql = `SELECT _user.ID, 
                        _user.Name
                  FROM dbo.tEmployee emp
                  INNER JOIN dbo.tUser _user
                  ON _user.ID_Employee = emp.ID
                `;

      var selectedRecords = await this.lvModal.ListLookUp<any>({
          sql : sql,
          title : "Select Items..."
      });

      if(selectedRecords.length == 0){

        reject();
      }else{

        resolve(selectedRecords);
      }

    });

    return promise;
  }

  onAdd(event: any) : void {

    this.browseUser(event.value).then((result) => {

      this.users.push(result);
    });
  }

  btnSend_onClick() : void {

  }
}
