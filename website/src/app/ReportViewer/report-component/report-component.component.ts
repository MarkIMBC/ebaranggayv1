import { PropertyTypeEnum } from 'src/utils/controls/class_helper';
import { APP_REPORTVIEW, PropertyType } from './../../../bo/APP_MODELS';
import { BaseReportComponent } from './../BaseReportViewer';
import { Component, OnInit } from '@angular/core';
import { extend } from 'jquery';
import { FilterCriteriaType } from 'src/utils/controls/appControlContainer/appControlContainer.component';
import { IFilterFormValue } from 'src/utils/controls/appForm/appForm.component';
import * as moment from 'moment';

@Component({
  selector: 'report-component',
  templateUrl: './report-component.component.html',
  styleUrls: ['./report-component.component.less']
})
export class ReportComponentComponent extends BaseReportComponent {

  ID_Report: string = APP_REPORTVIEW.POSSUMMARY;

  protected getDefaultFormFilter(): IFilterFormValue[]{

    var formFilters: IFilterFormValue[] =[]

    formFilters.push({
      dataField: "ID_Company_QPARAM",
      filterCriteriaType: FilterCriteriaType.Equal,
      propertyType: PropertyTypeEnum.Int,
      value: this.currentUser.ID_Company,
    });

    return formFilters;
  }

  async getCustomValidation(): Promise<any[]> {

    var validations = [];

    var filterFormValues = this.appForm.getFilterFormValue();

    filterFormValues.forEach(filterFormValue => {

      if(filterFormValue.dataField == 'Date_QPARAM'){

        if(filterFormValue.filterCriteriaType == FilterCriteriaType.Between){

          var dateStart = new Date(filterFormValue.value[0]);
          var dateEnd = new Date(filterFormValue.value[1]);

          if(!moment(dateStart).isValid()){

            validations.push({
              message: "Invalid Date Start."
            });
          }

          if(!moment(dateEnd).isValid()){

            validations.push({
              message: "Invalid Date End."
            });
          }

          if(validations.length == 0){

            if(dateStart > dateEnd){
      
              validations.push({
                message: "Invalid Date Range.",
              });
            }
          }
        }
      }
    });
   
    return validations;
  }


}
