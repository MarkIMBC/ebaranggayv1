import { PropertyTypeEnum } from 'src/utils/controls/class_helper';
import { APP_REPORTVIEW, PropertyType } from './../../../bo/APP_MODELS';
import { BaseReportComponent } from './../BaseReportViewer';
import { Component, OnInit } from '@angular/core';
import { extend } from 'jquery';

@Component({
  selector: 'inventory-detail-report-viewer',
  templateUrl: './inventory-detail-report-viewer.component.html',
  styleUrls: ['./inventory-detail-report-viewer.component.less']
})
export class InventoryDetailReportViewerComponent extends BaseReportComponent {

  ID_Report: string = APP_REPORTVIEW.INVENTORYDETAILREPORT;
}
