import { PropertyTypeEnum } from 'src/utils/controls/class_helper';
import { APP_REPORTVIEW, PropertyType } from './../../../bo/APP_MODELS';
import { BaseReportComponent } from './../BaseReportViewer';
import { Component, OnInit } from '@angular/core';
import { extend } from 'jquery';

@Component({
  selector: 'inventory-summary-report-viewer',
  templateUrl: './inventory-summary-report-viewer.component.html',
  styleUrls: ['./inventory-summary-report-viewer.component.less']
})
export class InventorySummaryReportViewerComponent extends BaseReportComponent {

  ID_Report: string = APP_REPORTVIEW.INVENTORYSUMMARYREPORT;
}
