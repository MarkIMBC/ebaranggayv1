import { PropertyTypeEnum } from 'src/utils/controls/class_helper';
import { APP_REPORTVIEW, PropertyType } from './../../../bo/APP_MODELS';
import { BaseReportComponent } from './../BaseReportViewer';
import { Component, OnInit } from '@angular/core';
import { extend } from 'jquery';
import { FilterCriteriaType } from 'src/utils/controls/appControlContainer/appControlContainer.component';
import { IFilterFormValue } from 'src/utils/controls/appForm/appForm.component';
import * as moment from 'moment';

@Component({
  selector: 'possummary-report-viewer',
  templateUrl: './possummary-report-viewer.component.html',
  styleUrls: ['./possummary-report-viewer.component.less']
})
export class POSSummaryReportViewerComponent extends BaseReportComponent {

  ID_Report: string = APP_REPORTVIEW.POSSUMMARY;

  protected getDefaultFormFilter(formFilters): IFilterFormValue[]{

    formFilters.push({
      dataField: "ID_Company_QPARAM",
      filterCriteriaType: FilterCriteriaType.Equal,
      propertyType: PropertyTypeEnum.Int,
      value: this.currentUser.ID_Company,
    });

    var index = -1;

    index = this.globalFx.findIndexByKeyValue(formFilters, 'dataField', 'Date_QPARAM');
    if(index < 0){

      formFilters.push({
        dataField: "Date_QPARAM",
        filterCriteriaType: FilterCriteriaType.Between,
        propertyType: PropertyTypeEnum.String,
        value: [
          moment().format("YYYY-MM-DD"),
          moment().format("YYYY-MM-DD")
        ]
      });
    }

    /* Header_CustomCaption */
    index = this.globalFx.findIndexByKeyValue(formFilters, 'dataField', 'Date_QPARAM');
    if(index > -1){

      var formFilter =  formFilters[index];

      var dateStart = new Date(formFilter.value[0]);
      var dateEnd = new Date(formFilter.value[1]);

      var caption = `From ${moment(dateStart).format("MM/DD/YYYY")} To ${moment(dateEnd).format("MM/DD/YYYY")} `;

      formFilters.push({
        dataField: "Header_CustomCaption",
        filterCriteriaType: FilterCriteriaType.Equal,
        propertyType: PropertyTypeEnum.String,
        value: caption,
      });
    }

    return formFilters;
  }

  async getCustomValidation(): Promise<any[]> {

    var validations = [];

    var filterFormValues = this.appForm.getFilterFormValue();

    filterFormValues.forEach(filterFormValue => {

      if(filterFormValue.dataField == 'Date_QPARAM'){

        if(filterFormValue.filterCriteriaType == FilterCriteriaType.Between){

          var dateStart = new Date(filterFormValue.value[0]);
          var dateEnd = new Date(filterFormValue.value[1]);

          if(!moment(dateStart).isValid()){

            validations.push({
              message: "Invalid Date Start."
            });
          }

          if(!moment(dateEnd).isValid()){

            validations.push({
              message: "Invalid Date End."
            });
          }

          if(validations.length == 0){

            if(dateStart > dateEnd){
      
              validations.push({
                message: "Invalid Date Range.",
              });
            }
          }
        }
      }
    });
   
    return validations;
  }


}
