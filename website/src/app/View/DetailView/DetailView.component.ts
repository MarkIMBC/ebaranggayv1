import { CrypterService } from 'src/utils/service/Crypter.service';
import { async } from '@angular/core/testing';
import { UserAuthenticationService, TokenSessionFields } from './../../AppServices/UserAuthentication.service';
import { DetailViewHolder } from './../../AppServices/dynamic-temaplate.svc';
import { AppFormComponent } from './../../../utils/controls/appForm/appForm.component';
import { ListViewModalService } from './../ListView/ListView.service';
import { MessageBoxService, IFormValidation } from '../../../utils/controls/appModal/appModal.component';
import { DynamicTemplateService } from '../../AppServices/dynamic-temaplate.svc';
import { DataService } from 'src/utils/service/data.service';
import { ValidationService } from 'src/utils/service/validation.service';
import {
  Component, Injector, NgModuleRef, ViewChild,
  ViewContainerRef, OnInit, ChangeDetectorRef, ElementRef, Input, isDevMode
} from "@angular/core";
import '@angular/compiler';
import * as TS from "linq-typescript";
import { AppDataTableComponent, IContextMenuEvent } from 'src/utils/controls/appDataTable/appDataTable.component';
import { DetailViewModel, DetailView_Detail, ControlTypeEnum, LabelLocaltionEnum, IFile, PropertyTypeEnum, IMenuItem, ViewTypeEnum, DEBUG, UserGroupEnum } from 'src/utils/controls/class_helper';
import { MenuItem } from 'primeng/api/menuitem';
import { Router, ActivatedRoute } from '@angular/router';

import { ContextMenu } from 'primeng/contextmenu/contextmenu';
import * as $ from 'jquery';
import { VC_BaseView } from 'src/bo/VC/Base/VC_BaseView';
import { REGISTRY_VC_MODEL } from 'src/bo/VC_Model.decorator';

import { APP_LISTVIEW, APP_MODEL, APP_DETAILVIEW } from 'src/bo/APP_MODELS';
import { ViewMenuItem } from '..';
import { Location } from '@angular/common';
import { BaseAppControl, IControlValueChanged, BaseAppControlOption } from 'src/utils/controls/_base/baseAppControl'

import { DetailViewDialogService } from '../DetailViewDialog/DetailViewDialog.component';
import { EventEmitter } from '@angular/core';
import { FormHelperDialogService, generateFormHtmlText } from '../FormHelperView/FormHelperView.component';
import { isNullOrUndefined } from 'util';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';
import { IDirty } from 'src/app/auth.guard';

@Component({
  selector: 'app-detail-view',
  templateUrl: './DetailView.component.html',
  styleUrls: ['./DetailView.component.less']
})
export class DetailViewComponent implements OnInit , IDirty {

  currentUser: TokenSessionFields;

  @ViewChild('vc', { read: ViewContainerRef })
  _container: ViewContainerRef;

  @ViewChild('container')
  containerHolder: ElementRef;

  CurrentObject: any = {};
  PreviousObject: any = {};

  @Input()
  showTitle: boolean = true;

  @Input()
  ID_CurrentObject: string = null;

  @Input()
  ID_DetailView: string = ''; //PurchaseOrder DetailView

  Oid_Model: string = null;
  title: string = '';
  viewName: string = '';

  @ViewChild("contextMenu")
  contextMenu: ContextMenu;

  detailViewOpt: DetailViewModel;

  @Input()
  isModal: boolean = false;

  @Input()
  isHideNewButton?:boolean = false;

  onSaved?: EventEmitter<boolean> = new EventEmitter();

  dvViewHolder: DetailViewHolder = null;
  reload: (ID_CurrentObject: string) => void = null;

  AppForm: AppFormComponent = null;

  @Input()
  dvParams?: any = null;

  footerLeftMessageHtml:String = '';
  footerRightMessageHtml:String = '';

  breadCrumbMenus: MenuItem[] = [];
  home: MenuItem = { icon: 'black-text  pi pi-home', routerLink: '/Home' };

  vcModels: VC_BaseView[] = [];
  contextMenus: MenuItem[] = [];
  menuItems: ViewMenuItem[] = []
  defaultMenuItems: ViewMenuItem[] = [
    {
      label: "New",
      icon: "pi pi-fw pi-plus",
      IsVisible : () => {
        if (  this.isHideNewButton == true ) return false;
        return true;
      },
      command: async () => {
        var result = await this.msgBox.confirm("Create new?", this.detailViewOpt.Caption, "Yes", "No");
        if (result !== true) return;
        this.reload("-1");
      },
    },
    {
      label: "Save",
      icon: "pi pi-fw pi-save green-text lighten-2",
      IsVisible: () => {
        if (this.CurrentObject == null) return false;
        if (this.AppForm) {
          if (this.AppForm.isDisabled == true) return false;
        }
        return true;
      },
      command: async () => {
        this.saveForm();
      }
    }
    // ,
    // {
    //   label: "Refresh",
    //   icon: "pi pi-fw pi-refresh"
    // }
  ];
  
  userToken: any = {};
  IsShowContextMenu: boolean = false;
  DetailViewJSON: any = null;

  constructor(private _injector: Injector, private _m: NgModuleRef<any>, private ds: DataService,
    private route: ActivatedRoute, private tpl: DynamicTemplateService,
    private changeDetector: ChangeDetectorRef, private msgBox: MessageBoxService,
    private el: ElementRef,
    private lvModalSvc: ListViewModalService,
    private dvModalSvc: DetailViewDialogService,
    private dvFormSvc: FormHelperDialogService,
    private location: Location,
    private http: HttpClient,
    private _validationService: ValidationService,
    private userAuth: UserAuthenticationService,
    private cs: CrypterService

  ) {
    
    this.userToken = this.userAuth.getDecodedToken();
    this.IsShowContextMenu =this.userToken.ID_UserGroup == UserGroupEnum.System;

    this.currentUser = userAuth.getDecodedToken();
  }
  isDirty(): boolean {
    return this.AppForm.isDirty;
  }
  getRef(): ViewContainerRef {
    return null;
  }

  getDisplayName() : string {
    return this.detailViewOpt.Caption;
  }

  async getFormValidations(): Promise<IFormValidation[]> {
    var validations = this.AppForm.getValidations();
    return Promise.resolve(validations);
  };

  async saveForm(): Promise<boolean> {

    var validations = await this.getFormValidations();
    var validationFn: Promise<IFormValidation[]>[] = [];

    this.vcModels.forEach((d) => {
      var _validations = d.onForm_Validations();
      validationFn.push(_validations);
    });

    var _validations = await Promise.all(validationFn);
    _validations.forEach(vs => {
      validations = validations.concat(vs);
    });

    if (validations.length > 0) {
      this.msgBox.showValidationsBox(validations);
      return;
    }

    var result = await this.msgBox.confirm("Do you want to save changes?", this.title, "Yes", "No");
    
    if (result != true) return Promise.resolve(false);
    
    var files: IFile[] = [];

    if (this.dvViewHolder.imageBoxes) {

      this.dvViewHolder.imageBoxes.forEach(i => {

        if (i.imageFile) {

          files.push({
            file: i.imageFile,
            dataField: i.name,
            isImage: true
          });
        }
      });
    };

    if('ID_Company' in this.CurrentObject && this.CurrentObject.ID < 1){

      this.CurrentObject.ID_Company = this.currentUser.ID_Company;
    }

    var deletedItems = {}

    if (this.dvViewHolder.detailGrids) {

      this.dvViewHolder.detailGrids.forEach(d => {

        if (d.deletedItems.length > 0) {
          deletedItems[d.name] = d.deletedItems;
        }
      });
    };

    this.vcModels.forEach(async d => {
      await d.onCurrentObject_Saving();
    });
 
    var r = await this.ds.saveObject(this.Oid_Model, this.CurrentObject, this.PreviousObject, files);
    var ID_CurrentObject = (r.key + "").replace("'", "");
    
    this.reload(ID_CurrentObject);
    this.AppForm.setDirty(false);
    this.msgBox.success("Save successfully", this.title);
    this.onSaved.emit(true);
    return Promise.resolve(true);
  }

  async ngOnInit() {

  }

  contextMenu_X: number = 0;
  contextMenu_Y: number = 0;
  contextMenu_Target: any = null;

  onRight_Click($event: any): void {
    this.contextMenu_X = $event.clientX;
    this.contextMenu_Y = $event.clientY;
    this.contextMenu_Target = event.target;
  }

  getControl<T extends BaseAppControl<any>>(name: string): T {
    return this.AppForm.controls.find(d => d.name == name) as T;
  }

  onContextMenu_Show(): void {


    //alert(this.contextMenu_X);
    this.contextMenus = [];
    var oidElements = $(this.el.nativeElement).find(".oid-control");
    var Oids: string[] = [];
    var leftParent = 0;// $(this.el.nativeElement).offset().left;
    var topParent = 0//$(this.el.nativeElement).offset().top;
    $.each(oidElements, (i, el) => {
      var e = $(el);
      var offset = e.offset();
      var top = offset.top + topParent;
      var bottom = top + e.outerHeight();
      var left = offset.left + leftParent;
      var right = left + e.outerWidth();
      //console.log(e, this.contextMenu_X, left, right, this.contextMenu_X >= left && this.contextMenu_X <= right );
      if ((this.contextMenu_Y >= top && this.contextMenu_Y <= bottom) && (this.contextMenu_X >= left && this.contextMenu_X <= right)) {
        Oids.push(e.attr("id"));
      }
    });

    this.contextMenus.push({
      label: "Reload",
      icon: "mdi mdi-refresh",
      command: async () => {
        await this.initView(this.ID_DetailView);
        this.msgBox.success(`Reload view successfully`);
      }
    });

    //if (Oids.length == 0) return;
    var ctrls = new TS.List(this.detailViewOpt.Details).where(d => Oids.includes(d.Oid)).orderBy(d => d.ID_ControlType).toArray();
    ctrls.forEach(d => {
      this.initDetailContextMenu(d, this.contextMenus);
    });
    this.contextMenus.push({
      label: "Edit Controls",
      icon: "mdi mdi-table",
      command: async () => {
        this.lvModalSvc.open(APP_LISTVIEW.DETAILVIEW_DETAIL_LISTVIEW, {
          Oid_DetailView: this.detailViewOpt.Oid
        }, {
          title : `${this.viewName} Edit Controls...`
        });
      }
    });


    if (isNullOrUndefined(this.detailViewOpt.ID_Model) != true) {
      var modelMenus = this.getModelContextMenu(this.detailViewOpt.ModelName, this.detailViewOpt.ID_Model);
      this.contextMenus.push({
        label: `${this.detailViewOpt.ModelName} Model`,
        icon: "mdi mdi-hexagon-slice-6",
        items: modelMenus
      });
    }

    this.contextMenus.push({
      label: "Root",
      items: [{
        label: "Add Section",
        icon: "mdi mdi-shape-square-plus",
        command: async () => {
          await this.ds.execSP("_pAddGroupControl", {
            ID_DetailView: this.detailViewOpt.Oid,
            ID_Control: ControlTypeEnum.Section,
            ID_Parent: null
          });
          await this.initView(this.detailViewOpt.Oid);
          this.msgBox.success("Add new section successfully.");
        }
      }, {
        label: "Add Tab",
        icon: "mdi mdi-tab-plus",
        command: async () => {
          await this.ds.execSP("_pAddGroupControl", {
            ID_DetailView: this.detailViewOpt.Oid,
            ID_Control: ControlTypeEnum.Tab,
            ID_Parent: null
          });
          await this.initView(this.detailViewOpt.Oid);
          this.msgBox.success("Add new tab successfully.");
        }
      }, {
        label: "Edit Roots",
        command: () => {
          this.lvModalSvc.open(APP_LISTVIEW.DETAILVIEW_DETAIL_LISTVIEW, {
            Oid_Control: null,
            IsRootOnly: true,
            Oid_DetailView: this.detailViewOpt.Oid
          }, {
            title:`${this.viewName} Edit Controls...`
          });
        }
      }
      ]
    })

    if (this.contextMenus.length == 0) this.contextMenu.hide();
  }

  async showCommonProperties(d:DetailView_Detail) : Promise<void> {
    var res = await this.dvFormSvc.open({
      Name: d.Name,
      Caption: d.Caption ? d.Caption : null,
      ColSpan: d.ColSpan ? d.ColSpan : null,
      ColCount: d.ColCount ? d.ColCount : null,
      SeqNo: d.SeqNo ? d.SeqNo : null,
      GroupIndex: d.GroupIndex > -1 ? d.GroupIndex : null,
      IsRequired: d.IsRequired,
      Height: d.Height ? d.Height : null,
      ID_ControlType: d.ID_ControlType ? d.ID_ControlType : null,
      ID_PropertyType: d.ID_PropertyType ? d.ID_PropertyType : null,
      IsShowLabel: d.IsShowLabel,
      DisplayProperty : d.DisplayProperty
    }, `${d.Name} Properties`, [
      {
        Name: "Name",
        Caption: "Name",
        ID_PropertyType: PropertyTypeEnum.String,
        IsReadOnly: [ControlTypeEnum.Section, ControlTypeEnum.Tab].includes(d.ID_ControlType) ? false : true
      },
      {
        Name: "Caption",
        Caption: "Caption",
        ID_PropertyType: PropertyTypeEnum.String
      },
      {
        Name: "DisplayProperty",
        Caption: "Display Property",
        ID_PropertyType: PropertyTypeEnum.String
      },
      {
        Name: "IsShowLabel",
        Caption: "Show Label",
        ID_PropertyType: PropertyTypeEnum.Bit
      },
      {
        Name: "SeqNo",
        Caption: "Seq No",
        ID_PropertyType: PropertyTypeEnum.Int
      },
      {
        Name: "ColSpan",
        Caption: "Column Span",
        ID_PropertyType: PropertyTypeEnum.Int,
      },
      {
        Name: "ColCount",
        Caption: "Column Count",
        ID_PropertyType: PropertyTypeEnum.Int
      },
      {
        Name: "GroupIndex",
        Caption: "Group Index",
        ID_PropertyType: PropertyTypeEnum.Int
      },
      {
        Name: "Height",
        Caption: "Height",
        ID_PropertyType: PropertyTypeEnum.Int
      },
      {
        Name: "IsRequired",
        Caption: "Required",
        ID_PropertyType: PropertyTypeEnum.Bit
      },
      {
        Name: "ID_PropertyType",
        Caption: "Property Type",
        ID_PropertyType: PropertyTypeEnum.Int,
        ID_ControlType: ControlTypeEnum.SelectBox,
        DataSource: this.cs.encrypt("SELECT ID, Name FROM _tPropertyType"),
        IsLoadData: true,
        IsRequired: isNullOrUndefined(d.ID_ModelProperty) != true
      },
      {
        Name: "ID_ControlType",
        Caption: "Control Type",
        ID_PropertyType: PropertyTypeEnum.Int,
        ID_ControlType: ControlTypeEnum.SelectBox,
        DataSource: this.cs.encrypt("SELECT ID, Name FROM _tControlType"),// WHERE ID NOT IN (11,12,13)",
        IsLoadData: true,
        IsRequired: false
      }
    ]);
    if (res == false) return;
    var data: any = {
      Oid: d.Oid,
      Caption: res.Caption,
      ColSpan: res.ColSpan,
      ColCount: res.ColCount,
      SeqNo: res.SeqNo,
      Height: res.Height,
      GroupIndex: res.GroupIndex,
      IsRequired: res.IsRequired,
      IsShowLabel: res.IsShowLabel,
      ID_PropertyType: res.ID_PropertyType,
      ID_ControlType: res.ID_ControlType
    };
    if ([ControlTypeEnum.Section, ControlTypeEnum.Tab].includes(d.ID_ControlType)) {
      data.Name = res.Name;
    }
    await this.ds.saveObject(APP_MODEL.DETAILVIEW_DETAIL, data);
    await this.initView(d.ID_DetailView);
  }

  private getModelContextMenu(ModelName: string, Oid_Model: string): IMenuItem[] {
    var modelMenus: IMenuItem[] = [];
    modelMenus.push({
      label: "Edit Model...",
      command: async () => {
        var res = await this.dvFormSvc.open({
          Caption: ModelName,
          Icon: null
        }, `Edit ${ModelName} Model`, [
          {
            Name: "Caption",
            IsRequired: false
          }
        ]);
        if (res == true) return;
        await this.ds.saveObject(APP_MODEL.MODEL, {
          Caption: res.Caption,
          Icon: res.Icon,
          Oid: Oid_Model
        });
        this.initView(this.ID_DetailView);
      }
    })

    modelMenus.push({
      label: "Add Property/Field",
      icon: "mdi mdi-hexagon-slice-6",
      command: async () => {
        var res = await this.dvFormSvc.open({
          Name: null,
          ID_PropertyType: null,
          ID_Model: null
        }, `Add ${ModelName} Property/Field`, [
          {
            Name: "Name",
            IsRequired: true
          },
          {
            Name: "ID_PropertyType",
            Caption: "Property Type",
            ID_PropertyType: PropertyTypeEnum.Int,
            ID_ControlType: ControlTypeEnum.SelectBox,
            DataSource: this.cs.encrypt("SELECT ID, Name FROM _tPropertyType"),
            IsLoadData: true,
            IsRequired: true
          },
          {
            Name: "ID_ControlType",
            Caption: "Control Type",
            ID_PropertyType: PropertyTypeEnum.Int,
            ID_ControlType: ControlTypeEnum.SelectBox,
            DataSource: this.cs.encrypt("SELECT ID, Name FROM _tControlType WHERE ID NOT IN (11,12,13)"),
            IsLoadData: true,
            IsRequired: false
          },
          {
            Name: "ID_Model",
            Caption: "Reference Table",
            ID_PropertyType: PropertyTypeEnum.String,
            ID_ControlType: ControlTypeEnum.SelectBox,
            DataSource: this.cs.encrypt("SELECT Oid AS ID, TableName AS Name FROM _tModel WHERE TableName NOT LIKE '_t%'")
          }
          // {
          //   Name : "ID_Model",
          //   Caption : "Control Type",
          //   ID_PropertyType : PropertyTypeEnum.String,
          //   ID_ControlType : ControlTypeEnum.ListBox,
          //   DataSource : "SELECT Oid, Name FROM _tModel",

          //   IsLoadData : true,
          //   IsRequired : false
          // }
        ]);
        //
        //
        //
        if (res == false) return;
        await this.ds.execSP("_pAddFieldPropertyOnModel", {
          Oid_Model: Oid_Model,
          Name: res.Name,
          ID_PropertyType: res.ID_PropertyType,
          ID_ControlType: res.ID_ControlType,
          ID_ReferenceModel: res.ID_Model
        });
        this.initView(this.ID_DetailView);
      }
    });

    modelMenus.push({
      label: "Add new Model/Table detail",
      icon: "mdi mdi-hexagon-slice-6",
      command: async () => {
        var res = await this.dvFormSvc.open({
          TableName: `t${ModelName}_`,
          IsDetail: true
        }, `Add  ${ModelName} new Model/Table detail`, [
          {
            Name: "TableName",
            Caption: "Table Name",
            IsRequired: true
          },
          {
            Name: "IsDetail",
            Caption: 'Is Detail',
            ID_PropertyType: PropertyTypeEnum.Bit
          }
        ]);
        if (res == false) return;
        if (await this.msgBox.confirm("Are you sure you want to add detail", `${res.TableName}`, 'Yes', 'No') == false) return;
        await this.ds.execSP("_pCreateModelDetailonModel", {
          Oid_Model: Oid_Model,// this.detailViewOpt.ID_Model,
          TableName: res.TableName,
          IsDetail: res.IsDetail
        });
        this.initView(this.ID_DetailView);
      },
    });
    return modelMenus;
  }

  initDetailContextMenu(d: DetailView_Detail, contextMenus: IMenuItem[]): void {
    var childMenus: MenuItem[] = [];
    if (d.ID_ControlType == ControlTypeEnum.ListBox || d.ID_ControlType == ControlTypeEnum.SelectBox) {
      childMenus.push({
        label: "DataSource Properties...",
        icon: "",
        command: async () => {
          var details: DetailView_Detail[] = [
            {
              Name: "DataSource",
              Caption: "Data Source",
              ID_ControlType: ControlTypeEnum.TextArea
            },
            {
              Name: "IsLoadData",
              Caption: "Load Data",
              ID_ControlType: ControlTypeEnum.CheckBox
            }
          ];
          var res = await this.dvFormSvc.open({
            DataSource: this.cs.decrypt(d.DataSourceKey),
            IsLoadData: d.IsLoadData
          }, "DataSource Properties", details);
          if (res == false) return;
          await this.ds.saveObject(APP_MODEL.DETAILVIEW_DETAIL, {
            Oid: d.Oid,
            DataSource: res.DataSource,
            IsLoadData: res.IsLoadData
          });
          await this.initView(d.ID_DetailView);
        }
      });
    };

    childMenus.push(
      {
        label: "Edit Properties on DetailView",
        icon: "mdi mdi-id-card",
        command: async () => {
          this.dvModalSvc.open(APP_DETAILVIEW.DETAILVIEW_DETAIL_DETAILVIEW, d.Oid, {
            title: "Edit " + d.Name
          });
        },
      });

    childMenus.push({
      label: "Edit Properties on TableView",
      icon: "mdi mdi-table-edit",
      command: () => {
        this.lvModalSvc.open(APP_LISTVIEW.DETAILVIEW_DETAIL_LISTVIEW, {
          Oid_Control: d.Oid,
          Oid_DetailView: this.detailViewOpt.Oid
        }, {
          title: `${this.viewName} Edit Controls...`
        });
      }
    });

    if (d.ID_ControlType == ControlTypeEnum.Tab || d.ID_ControlType == ControlTypeEnum.Section) {
      childMenus.push({
        label: "Add Section",
        icon: "mdi mdi-shape-square-plus",
        command: async () => {
          await this.ds.execSP("_pAddGroupControl", {
            ID_DetailView: d.ID_DetailView,
            ID_Control: ControlTypeEnum.Section,
            ID_Parent: d.Oid
          });
          await this.initView(d.ID_DetailView);
          this.msgBox.success("Add new section successfully.");
        }
      });

      childMenus.push({
        label: "Add Tab",
        icon: "mdi mdi-tab-plus",
        command: async () => {
          await this.ds.execSP("_pAddGroupControl", {
            ID_DetailView: d.ID_DetailView,
            ID_Control: ControlTypeEnum.Tab,
            ID_Parent: d.Oid
          });
          await this.initView(d.ID_DetailView);
          this.msgBox.success("Add new tab successfully.");
        }
      });
    }

    if (d.ID_Section != null) {
      childMenus.push({
        label: "Remove on Section",
        icon: "mdi mdi-sticker-remove-outline",
        command: async () => {
          await this.ds.saveObject(APP_MODEL.DETAILVIEW_DETAIL, {
            Oid: d.Oid,
            ID_Section: null
          });
          await this.initView(d.ID_DetailView);
          this.msgBox.success(`Remove from section successfully`);
        },
      });
    }

    if (d.ID_Tab != null) {
      childMenus.push({
        label: "Remove on Tab",
        icon: "mdi mdi-tab-remove",
        command: async () => {
          await this.ds.saveObject(APP_MODEL.DETAILVIEW_DETAIL, {
            Oid: d.Oid,
            ID_Tab: null
          });
          await this.initView(d.ID_DetailView);
          this.msgBox.success(`Remove from tab successfully`);
        },
      });
    };

    // if (d.ID_ControlType == ControlTypeEnum.ListView) {
    //   if (isNullOrUndefined(d.ID_ListView) != true) {
    //     this.contextMenus.push({
    //       label: "Edit Columns",
    //       command: async () => {
    //         this.lvModalSvc.open(APP_LISTVIEW.LISTVIEW_DETAIL_LISTVIEW, {
    //           Oid_ListView: d.ID_ListView,
    //           Oid: null
    //         }, {
    //           title: `${d.Name} Edit Columns...`
    //         });
    //       }
    //     })
    //   }
    // }
    childMenus.push({
      label: "Common Properties...",
      icon: "mdi mdi-hammer-wrench",
      command: async () => {
        this.showCommonProperties(d);
      },
    });
    var tabs = new TS.List(this.detailViewOpt.Details).where(det => det.ID_ControlType == ControlTypeEnum.Tab && det.Oid != d.ID_Tab && det.IsActive == true && det.Oid != d.Oid).toArray();
    var section = new TS.List(this.detailViewOpt.Details).where(det => det.ID_ControlType == ControlTypeEnum.Section && det.Oid != d.ID_Section && det.IsActive == true && det.Oid != d.Oid).toArray();
    if (tabs.length > 0) {
      var childTabMenus: IMenuItem[] = [];
      tabs.forEach((det) => {
        childTabMenus.push({
          label: det.Name,
          icon: "mdi mdi-tab",
          command: async () => {
            await this.ds.saveObject(APP_MODEL.DETAILVIEW_DETAIL, {
              Oid: d.Oid,
              ID_Tab: det.Oid,
              ID_Section: null
            });
            await this.initView(d.ID_DetailView);
            this.msgBox.success(`Change to tab '${det.Name}'`);
          },
        });
      });
      var changeTabMenu: IMenuItem = {
        label: "Move to Tab",
        icon: "mdi mdi-tab",
        command: () => { },
        items: childTabMenus
      };
      childMenus.push(changeTabMenu);
    };

    if (section.length > 0) {
      var childTabMenus: IMenuItem[] = [];
      section.forEach((det) => {
        childTabMenus.push({
          label: det.Name,
          icon: "mdi mdi-shape-rectangle-plus",
          command: async () => {
            await this.ds.saveObject(APP_MODEL.DETAILVIEW_DETAIL, {
              Oid: d.Oid,
              ID_Tab: null,
              ID_Section: det.Oid
            });
            await this.initView(d.ID_DetailView);
            this.msgBox.success(`Change to section '${det.Name}'`);
          },
        });
      });
      var changeSectionMenu: IMenuItem = {
        label: "Move to Section",
        icon: "mdi mdi-shape-rectangle-plus",
        command: () => { },
        items: childTabMenus
      };
      childMenus.push(changeSectionMenu);
    };

    childMenus.push({
      label: "Move to Root",
      icon: "mdi mdi-minus-circle-outline",
      command: async () => {
        await this.ds.saveObject(APP_MODEL.DETAILVIEW_DETAIL, {
          Oid: d.Oid,
          ID_Tab: null,
          ID_Section: null
        });
        await this.initView(d.ID_DetailView);
      }
    });

    childMenus.push({
      label: "Set InActive",
      icon: "mdi mdi-minus-circle-outline",
      command: async () => {
        await this.ds.saveObject(APP_MODEL.DETAILVIEW_DETAIL, {
          Oid: d.Oid,
          IsActive: false
        });
        await this.initView(d.ID_DetailView);
        this.msgBox.success(`Set '${d.Name}' InActive Successfully`);
      }
    });

    if (d.ID_ControlType != ControlTypeEnum.Tab && d.ID_ControlType != ControlTypeEnum.Section) {

      var changeLabelPosition = async (ID_LabelLocation?: number) => {
        await this.ds.saveObject(APP_MODEL.DETAILVIEW_DETAIL, {
          Oid: d.Oid,
          ID_LabelLocation: ID_LabelLocation,
        });
        await this.initView(d.ID_DetailView);
        this.msgBox.success(`Change label postion succesfully.`);
      }

      childMenus.push({
        label: "Label Position",
        icon: "mdi mdi-alphabetical",
        items: [
          {
            label: "Top",
            icon: "mdi mdi-arrow-collapse-up",
            command: () => {
              changeLabelPosition(LabelLocaltionEnum.Top);
            },
          },
          {
            label: "Left",
            icon: "mdi mdi-arrow-collapse-left",
            command: () => {
              changeLabelPosition(LabelLocaltionEnum.Left);
            },
          },
          {
            label: "Right",
            icon: "mdi mdi-arrow-collapse-right",
            command: () => {
              changeLabelPosition(LabelLocaltionEnum.Right);
            },
          },
        ],
      });

      childMenus.push({
        label: d.IsDisabled == true ? "Set Enabled" : "Set Read only",
        icon: "mdi mdi-stop-circle",
        command: async () => {
          var value = d.IsDisabled == true ? false : true;
          await this.ds.saveObject(APP_MODEL.DETAILVIEW_DETAIL, {
            Oid: d.Oid,
            IsReadOnly: value,
            //IsDisabled : value,
          });
          await this.initView(d.ID_DetailView);
        }
      });
    }

    switch (d.ID_ControlType) {
      case ControlTypeEnum.Tab:
      case ControlTypeEnum.Section:
        childMenus.push({
          label: "Delete " + d.Name,
          icon: "mdi mdi-delete-forever",
          command: async () => {
            var res = await this.msgBox.confirm(
              "Are you sure you want to delete this item?",
              d.Caption,
              "Yes", "No"
            );
            if (res != true) return;
            await this.ds.execSP('_pDelete_DetailView_Detail', {
              Oid: d.Oid
            });
            await this.initView(d.ID_DetailView);
            this.msgBox.success(`Delete ${d.Name} Successfully`);
          },
        });
        break;
      default:
        break;
    }

    if (isNullOrUndefined(d.ID_ModelProperty) != true && isNullOrUndefined(this.detailViewOpt.ID_Model) != true) {
      childMenus.push({
        label: "Delete Property",
        command: async () => {
          if (await this.msgBox.confirm("Are you sure you want to delete " + d.Name, `Property ${d.Name}`, "Yes", "No") != true) return;
          await this.ds.execSP("_pDeleteModelProperty", {
            ID_Model: this.detailViewOpt.ID_Model,
            FieldName: d.Name
          });
          await this.initView(d.ID_DetailView);
          this.msgBox.success(`'${d.Name}' Deleted Successfully`);
        }
      })
    }
    var controlMenu: MenuItem = {
      label: d.Name + " Control",
      icon: "mdi mdi-cube-outline",
      items: childMenus
    };
    contextMenus.push(controlMenu);
  }

  ngOnDestroy(): void {
    this._container.clear();
  };

  refreshMenus(): void {
    this.menuItems.forEach(m => {
      var visble = m.visible;
      if (m.IsVisible) {
        visble = m.IsVisible();
      }
      m.visible = visble;
      //
      //
      //
      var disable = m.disabled;
      if (m.IsDisabled) {
        disable = m.IsDisabled();
      }
      m.disabled = m.disabled;
    });
    var details = new TS.List(this.detailViewOpt.Details).where(d => d.ID_ControlType == ControlTypeEnum.ListView).toArray();
    details.forEach(d => {
      var menuItems = this.dvViewHolder[`${d.Name}_MenuItems`] as ViewMenuItem[];
      if (menuItems) {
        if (menuItems.length > 0) {
          menuItems.forEach(m => {
            var visble = m.visible;
            if (m.IsVisible) {
              visble = m.IsVisible();
            }
            m.visible = visble;
            //
            //
            //
            var disable = m.disabled;
            if (m.IsDisabled) {
              disable = m.IsDisabled();
            }
            m.disabled = m.disabled;
          });
        }
      }
    });
  };

  async initView(Oid_DetailView: string) {
    this._container.clear();
    this.title = '';
    this.ID_DetailView = Oid_DetailView;
    var cmpHolder = this as any;
    cmpHolder.ControlOption = {};
   
    this.vcModels = [];
    this.menuItems = [];
    this.changeDetector.detectChanges();
    //console.log("ID_DetailView",this.ID_DetailView);
    if (this.DetailViewJSON == null) {
      this.DetailViewJSON = await this.http.get<any>("assets/detailview/_Index.json?q=" + new Date().getTime()).toPromise();
      //console.log(this.DetailViewJSON);
    }
    var detailView: DetailViewModel = null;
    if (Object.keys(this.DetailViewJSON).includes(this.ID_DetailView.toUpperCase()) && isDevMode() != true) {
      detailView = await this.http.get<DetailViewModel>(`assets/detailview/${this.DetailViewJSON[this.ID_DetailView.toUpperCase()]}.json?q=` + new Date().getTime()).toPromise();
    } else {
      detailView = await this.ds.get(`Model/DetailView/${this.ID_DetailView}`);
    }
    this.viewName = detailView.Name;
    this.detailViewOpt = detailView;
    this.menuItems = this.menuItems.concat(this.defaultMenuItems);
    if ( isNullOrUndefined(this.callerLv) != true ) {
      this.breadCrumbMenus.push({
        label: "List",
        icon: 'black-text  pi pi-bars',
        routerLink : "/Main/lv/" + detailView.ModelName + "/" + this.callerLv
      })
    }
    var getCurrentObject = async (ID_CurrentObject?: string): Promise<any> => {
      var params = {
        ID_CurrentObject: ID_CurrentObject
      }
      if( isNullOrUndefined(this.dvParams) != true ) { 
        Object.keys(this.dvParams).forEach((k) => { 
          params[k] = this.dvParams[k];
        });
      }
      
      var obj = await this.ds.loadObject<any>(detailView.ID_Model, ID_CurrentObject, params);
      this.PreviousObject = JSON.parse(JSON.stringify(obj)); //Make Carbon Copy
      var bMenu = new TS.List(this.breadCrumbMenus).singleOrDefault(d=>d.id == "-10000");
      if ( bMenu ) {
        bMenu.label = `${obj.ID > 0 ? "Edit " : "New "}` +  detailView.Caption;
      } else {
        this.breadCrumbMenus.push({
          id: "-10000",
          icon: "black-text pi pi-pencil",
          label: `${obj.ID > 0 ? "Edit " : "New "}` +  detailView.Caption
        })
      }

      if (!(obj.ID > 0)) {
        this.footerLeftMessageHtml = `<b><span class="mdi mdi-information blue-text"></span> New ${this.detailViewOpt.Caption}</b>`;
        this.footerRightMessageHtml = '';
      } else {
        if (isNullOrUndefined(obj.DateCreated) != true) { 
          this.footerLeftMessageHtml = `<b><span class="mdi mdi-information blue-text"></span> 
              Created on ${moment(obj.DateCreated).format('LLLL')}</b>`;
        }
        if (isNullOrUndefined(obj.DateModified) != true) { 
          this.footerRightMessageHtml = `<b><span class="mdi mdi-information blue-text"></span> 
          Last modified on ${moment(obj.DateModified).format('LLLL')}</b>`;
        } else {
          this.footerRightMessageHtml = '';
        }
      }
      return obj;
    };
    this.Oid_Model = detailView.ID_Model;
    var view_name = detailView.Name;
    var ModelName = detailView.ModelName;
    var vcClasses = REGISTRY_VC_MODEL.get(view_name);
    //console.log("VC_Classes", vcClasses);
    this.reload = async (ID_CurrentObject?: string) => {
      this.ID_CurrentObject = ID_CurrentObject.replace("'", "");
      this.CurrentObject = await getCurrentObject(this.ID_CurrentObject);
      //console.log("Current Object -->", this.CurrentObject);
      this.dvViewHolder.CurrentObject = this.CurrentObject;
      this.menuItems = this.defaultMenuItems;
      this.vcModels.forEach(d => {
        d.menuItems = this.menuItems;
        d.appForm = this.AppForm;
        d.CurrentObject = this.CurrentObject;
        d.onCurrentObject_Load();
      });
      if (this.ID_CurrentObject == "-1") {
        this.AppForm.setFormDisabled(false);
      };

      this.refreshMenus();
      if (this.isModal != true) {
        this.location.replaceState(`Admin/dv/${ModelName}/${btoa(Oid_DetailView)}/${this.ID_CurrentObject}`);
      }
      this.changeDetector.detectChanges();
    }
    if (vcClasses) {
      vcClasses.forEach(c => { 
        var newInstance = Object.create(c.prototype) as VC_BaseView;
        newInstance.ViewType = ViewTypeEnum.DetailView;
        newInstance.msgBox = this.msgBox;
        //console.log(c.name);
        newInstance.ClassName = c.name;
        newInstance.ViewParams = this.dvParams;
        newInstance.dataService = this.ds;
        newInstance.validationService = this._validationService;
        newInstance.userAuth = this.userAuth;
        newInstance.dvFormSvc = this.dvFormSvc;
        newInstance.lvModal = this.lvModalSvc;
        newInstance.dvModal = this.dvModalSvc;
        newInstance._reload = this.reload;
        this.vcModels.push(newInstance);
      });
    };
    
    this.CurrentObject = await getCurrentObject(this.ID_CurrentObject);
    this.vcModels.forEach(d => {
      d.CurrentObject = this.CurrentObject;
    });
    this.vcModels.forEach(d => {
      d.onViewMenus_Initialized(this.menuItems);
    });
    this.title = detailView.Caption;

    var html = generateFormHtmlText(detailView, cmpHolder, false, {
 
      onControlOption_Initialized : async (name: string, opt: BaseAppControlOption) => { 
        this.vcModels.forEach(async d => {
          d.onControlOption_Initialized(name,opt);
        });
        return Promise.resolve();
      }
    });

    //console.log('generateFormHtmlText', html);

    var cmpRef = await this.tpl.createAndCompileComponents(html, this._injector, this._m);//.then(cmpRef => {
    var instance = cmpRef.instance as DetailViewHolder;
    this.dvViewHolder = instance;
    //console.log("html compiled")
    // this.AppForm = instance.appForm;
    instance.ds = this.ds;
    var OptionKeys = Object.keys(cmpHolder.ControlOption);
    OptionKeys.forEach(key => {
      instance[key] = cmpHolder.ControlOption[key];
    });
    var details = new TS.List(this.detailViewOpt.Details).where(d => d.ID_ControlType == ControlTypeEnum.ListView).toArray();
    details.forEach(c => {
      var detailMenus: ViewMenuItem[] = [];
      instance[`${c.Name}_MenuItems`] = detailMenus;
      this.vcModels.forEach(d => {
        d.onDetail_ViewMenus_Initialized(c.Name, detailMenus);
      });
    });
    instance.vcModels = this.vcModels;
    instance["menuItems"] = this.menuItems;
    this.refreshMenus();
    instance.CurrentObject = this.CurrentObject;
    instance.onView_Initialzied = () => {
      //console.log('on view initialied');
      this.AppForm = instance.appForm;
      this.vcModels.forEach(d => {
        d.appForm = this.AppForm;
        d.refreshMenus = this.refreshMenus;
      });
      instance.appControls.forEach(d => {
        if (d instanceof AppDataTableComponent) {
          (d as AppDataTableComponent).parentSrollView = this.containerHolder;

          (d as AppDataTableComponent).option.onContextMenu_Show.subscribe((opt: IContextMenuEvent) => {
            var ID_ListView = opt.grid.option.Oid_ListView;
            var det: DetailView_Detail = this.detailViewOpt.Details.find(d => d.Oid == opt.grid.oid_Control);

            if (opt.isHeader == true) {

              if (opt.grid.option.Oid_ListView) {
                opt.menus.push({
                  label: "Edit Columns",
                  command: () => {

                    this.lvModalSvc.open(APP_LISTVIEW.LISTVIEW_DETAIL_LISTVIEW, {
                      Oid_ListView: ID_ListView
                    }, {
                      title: `${view_name} Edit Columns...`
                    });

                  }
                });

                opt.menus.push({
                  label : `Edit ${opt.column.dataField} Column...`,
                  command: async () => { 
                    var col = opt.column;
                    var res = await this.dvFormSvc.open({
                      Name: col.dataField,
                      Caption: col.caption,
                    }, `Column ${col.dataField} Properties`, [
                      {
                        Name: "Name",
                        Caption: "Name",
                        ID_PropertyType: PropertyTypeEnum.String,
                        IsReadOnly: true
                      },
                      {
                        Name: "Caption",
                        Caption: "Caption",
                        ID_PropertyType: PropertyTypeEnum.String
                      }
                    ]);
                    if (res == false) return;
                    var data: any = {
                      Oid: col.oid,
                      Caption: res.Caption,
                    };
                    await this.ds.saveObject(APP_MODEL.LISTVIEW_DETAIL, data);
                    opt.grid.refreshColumns();
                  }
                });
              }

              if (det){
                this.initDetailContextMenu(det, opt.menus);
                if ( isNullOrUndefined(det.ID_PropertyModel) !== true ) { 
                  var modelMenus = this.getModelContextMenu(det.PropertyModel, det.ID_PropertyModel);
                  if (det.ID_PropertyModel_ID_DetailView) {
                    opt.menus.push({
                      label: "Open DetailView",
                      command: () => {
                        this.dvModalSvc.open(det.ID_PropertyModel_ID_DetailView, "-1", {
                          title :`${det.PropertyModel} Detail View`
                        });
                      }
                    })
                  }
                  opt.menus.push({
                    label: det.PropertyModel + " Model",
                    items: modelMenus,
                    icon: "mdi mdi-hexagon-slice-6",
                  });
                }
              };

            }
          });
        }

        d.KeyDown.subscribe((evt: KeyboardEvent) => {
          if ( evt.altKey == true && evt.keyCode == 13 ) {
            var detail = new TS.List(this.detailViewOpt.Details).where(x=>x.Oid == d.oid_Control).toArray();
            if( detail.length == 0 ) return;
            this.showCommonProperties(detail[0]);
          }
         });
      });
      this.AppForm.onFormValueChanged.subscribe((evt: IControlValueChanged) => {
        this.vcModels.forEach(async d => {
          await d.onFormValue_Changed(evt);
        });
      });
      this.refreshMenus();
      return Promise.resolve();
    };
    this._container.insert(cmpRef.hostView);
    //});
    // });
  };

  callerLv: string = null;

  ngAfterViewInit(): void {



    if (this.ID_DetailView) {
      this.initView(this.ID_DetailView)
    } else {
      this.route.paramMap.subscribe((params) => {
        this._container.clear();
        this.title = '';
        var pOid_DetailView = params.get("Oid_DetailView");
        if (pOid_DetailView == null) return;
        this.ID_DetailView = atob(pOid_DetailView);//this.route.snapshot.paramMap.get("Oid_Model");
        this.ID_CurrentObject = params.get("ID_CurrentObject");
        var lvParam = this.route.snapshot.queryParamMap.get("lv");
        if ( isNullOrUndefined(lvParam)!= true) {
          this.callerLv = lvParam;
        }

        this.initView(this.ID_DetailView)
      });
    }
  }
}

