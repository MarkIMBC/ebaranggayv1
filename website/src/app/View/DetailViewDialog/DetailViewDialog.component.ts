import { Component, OnInit, ViewChild, Inject, Injectable, ElementRef } from '@angular/core';
import { DetailViewComponent } from '../DetailView/DetailView.component';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { isNullOrUndefined } from 'util';

@Component({
  selector: "app-DetailViewDialog",
  templateUrl: "./DetailViewDialog.component.html",
  styleUrls: ["./DetailViewDialog.component.less"],
})
export class DetailViewDialogComponent implements OnInit {
  
  title:string;

  isSaved: boolean = false;

  @ViewChild('detailView')
  detailView:DetailViewComponent;

  ID_CurrentObject: string = null;

  height: number = 600;

  ID_DetailView: string = null;

  dvParams:any = null;

  isHideNewButton:boolean = false;

  constructor(private actModal: NgbActiveModal) { }

  ngOnInit() {
  
  }

  ngAfterViewInit(): void {
    this.detailView.isModal = true;
    this.detailView.onSaved.subscribe(() => {
      this.isSaved = true;
    });
  }

  Close(): void {
    if ( this.isSaved == true ) {
      this.actModal.close(true);
    } else {
      this.actModal.dismiss()
    }
    
  }

  async SaveAndClose() : Promise<boolean> {
    this.detailView.saveForm().then((r)=>{
      if ( r !== true ) return;
      this.actModal.close(true);
    });
    return Promise.resolve(true);
  }
}


@Injectable({
  providedIn: "root"
})
export class DetailViewDialogService {
  constructor(private modalSvc: NgbModal) {}

  open(Oid_DetailView: string, ID_CurrentObject?: string, opt?: IDetailViewDialog): Promise<void> {
    var cmp = this.modalSvc.open(DetailViewDialogComponent, {
      windowClass: "detail-view-dialog" + ( isNullOrUndefined(opt.width) != true ? "custom-width-dialog" : "" ),
      size: "lg",
      centered: true,
      backdrop: "static",
    });

    
    if ( isNullOrUndefined(opt.width) !== true ) {
      var parent = $(((cmp as any)._windowCmptRef.instance._elRef as ElementRef).nativeElement);
      //console.log("modaldialog",parent);
      setTimeout(() => {
        var modalDialog = parent.find('.modal-dialog');
        modalDialog.css("cssText",`width:${opt.width}px!important`);
        modalDialog.css("cssText",`max-width:${opt.width}px!important`) 
        setTimeout(() => {
          parent.css("cssText", "opacity:1!important");
        });
      });
    }
    
    var cmpDV = cmp.componentInstance as DetailViewDialogComponent;
    cmpDV.ID_DetailView = Oid_DetailView;
    cmpDV.ID_CurrentObject = ID_CurrentObject;
    cmpDV.title = opt?.title;
    cmpDV.dvParams = opt?.params;
    if ( opt.height ) {
      cmpDV.height = opt.height;
    }
    if ( opt.isHideNewButton ) {
      cmpDV.isHideNewButton = opt.isHideNewButton;
    }
    //cmpDV.lvParams = params;
    return cmp.result;
  }
}

export interface IDetailViewDialog {
  title?: string;
  params?: any;
  width?: number;
  height?:number;
  isHideNewButton?:boolean;
}