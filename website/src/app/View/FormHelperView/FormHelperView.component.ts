import { async } from '@angular/core/testing';
import { Component, OnInit, Injector, NgModuleRef, ViewChild, ViewContainerRef, Injectable, EventEmitter } from '@angular/core';
import { DetailViewModel, ControlTypeEnum, PropertyTypeEnum, LabelLocaltionEnum, DetailView_Detail, IGenerateHtmlOpt } from 'src/utils/controls/class_helper';
import { DataService } from 'src/utils/service/data.service';
import { DynamicTemplateService, DetailViewHolder } from 'src/app/AppServices/dynamic-temaplate.svc';
import { AppFormComponent } from 'src/utils/controls/appForm/appForm.component';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { isNullOrUndefined } from 'util';
import { Guid } from 'guid-typescript';


import * as TS from "linq-typescript";
import { BaseAppControlOption } from 'src/utils/controls';
import { DataTableOption, IContextMenuEvent } from 'src/utils/controls/appDataTable/appDataTable.component';
import { IAppSelectBoxOption } from 'src/utils/controls/appSelectBox/appSelectBox.component';
import { IAppLookupBoxOption } from 'src/utils/controls/appLookupBox/appLookupBox.component';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { IAppMultipleListBoxOption } from 'src/utils/controls/appMultipleListBox/appMultipleListBox.component';
import { IAppMultipleSelectBoxOption } from 'src/utils/controls/appMultipleSelectBox/appMultipleSelectBox.component';


@Component({
  selector: "app-FormHelperView",
  templateUrl: "./FormHelperView.component.html",
  styleUrls: ["./FormHelperView.component.less"],
})
export class FormHelperViewComponent implements OnInit {
  CurrentObject: any = {};
  DetailView_Details: DetailView_Detail[] = [];
  title: string = "";
  PositiveButton_Label: string = "Save & Close";

  constructor(
    private _injector: Injector,
    private _m: NgModuleRef<any>,
    private ds: DataService,
    private tpl: DynamicTemplateService,
    private actModal: NgbActiveModal,
    private crypterSvc: CrypterService
  ) { }

  @ViewChild("vc", { read: ViewContainerRef })
  _container: ViewContainerRef;

  AppForm: AppFormComponent = null;
  dvViewHolder: DetailViewHolder = null;

  ngOnInit() { }

  async ngAfterViewInit() {
    if (isNullOrUndefined(this.DetailView_Details) == true)
      this.DetailView_Details = [];
    var sectionId = Guid.create().toString();
    if (this.DetailView_Details.length == 0) {
      var props = Object.keys(this.CurrentObject);
      props.forEach((p) => {
        var value = this.CurrentObject[p];
        var type = typeof value;
        var ID_PropertyType: PropertyTypeEnum = PropertyTypeEnum.String;
        switch (type) {
          case "number":
            ID_PropertyType = PropertyTypeEnum.Decimal;
            break;
        }
        var d: DetailView_Detail = {
          Name: p,
          Oid: Guid.create().toString(),
          Caption: p,
          ColSpan: 3,
          ID_PropertyType: ID_PropertyType,
          ID_Section: sectionId,
        };
        this.DetailView_Details.push(d);
      });
    }
    this.DetailView_Details.forEach((d) => {
      d.ID_Tab = null;
      d.ID_Section = null;
      d.ID_Section = sectionId;
    });
    this.DetailView_Details.push({
      Oid: sectionId,
      Name: "Section",
      Caption: "Section",
      ColCount: 1,
      ColSpan: 12,
      ID_ControlType: ControlTypeEnum.Section,
    });

    this._container.clear();
    var cmpHolder = this as any;
    cmpHolder.ControlOption = {};
    cmpHolder.CurrentObject = this.CurrentObject;
    var html: string = generateFormHtmlText(
      {
        Name: Guid.create().toString(),
        Oid: Guid.create().toString(),
        Details: this.DetailView_Details,
      },
      cmpHolder
    );
    var cmpRef = await this.tpl.createAndCompileComponents(
      html,
      this._injector,
      this._m
    ); //.then(cmpRef => {
    var instance = cmpRef.instance as DetailViewHolder;
    this.dvViewHolder = instance;
    this.dvViewHolder.CurrentObject = this.CurrentObject;
    instance.ds = this.ds;
    var OptionKeys = Object.keys(cmpHolder.ControlOption);
    OptionKeys.forEach((key) => {
      instance[key] = cmpHolder.ControlOption[key];
    });
    instance.onView_Initialzied = () => {
      this.AppForm = instance.appForm;
      return Promise.resolve();
    };
    this._container.insert(cmpRef.hostView);
  }

  Close(): void {
    this.actModal.close(false);
  }

  AcceptChanges(): void {
    var validations = this.AppForm.getValidations();
    if (validations.length > 0) return;
    this.actModal.close(this.CurrentObject);
  }
}

@Injectable({
  providedIn: "root"
})
export class FormHelperDialogService {


  constructor(private modalSvc: NgbModal) { }

  open(CurrentObject: any, title: string, details?: DetailView_Detail[], options?:any): Promise<any> {
    var cmp = this.modalSvc.open(FormHelperViewComponent, {
      windowClass: "form-helper-view-dialog",
      size: "md",
      centered: true,
      backdrop: "static",
    });

    var cmpDV = cmp.componentInstance as FormHelperViewComponent;
    cmpDV.CurrentObject = CurrentObject;
    cmpDV.title = title;
    cmpDV.DetailView_Details = details;

    if(!isNullOrUndefined(options)){

      if(!isNullOrUndefined(options['PositiveButton_Label'])){
  
        cmpDV.PositiveButton_Label = options['PositiveButton_Label'];
      }
    }

    return cmp.result;
  }


  openFormHelper(CurrentObject: any, title: string, details?: DetailView_Detail[], options?:any): Promise<FormHelperViewComponent> {
    var cmp = this.modalSvc.open(FormHelperViewComponent, {
      windowClass: "form-helper-view-dialog",
      size: "md",
      centered: true,
      backdrop: "static",
    });

    var cmpDV = cmp.componentInstance as FormHelperViewComponent;
    cmpDV.CurrentObject = CurrentObject;
    cmpDV.title = title;
    cmpDV.DetailView_Details = details;

    if(!isNullOrUndefined(options)){

      if(!isNullOrUndefined(options['PositiveButton_Label'])){
  
        cmpDV.PositiveButton_Label = options['PositiveButton_Label'];
      }
    }
     
    return new Promise<FormHelperViewComponent>(async (resolve, reject) => {

      var result = await cmp.result;

      if(result == false){

        reject();
      }else{

        resolve(cmpDV);
      }

    });
  }

}

export function generateFormHtmlText(detailView: DetailViewModel, cmpHolder?: any, isFilterMode?: boolean, opt?: IGenerateHtmlOpt): string {
  if (cmpHolder == undefined || cmpHolder == null) cmpHolder = {};
  var roots = new TS.List(detailView.Details).where(s => s.ID_Tab == null && s.ID_Section == null)
    .orderBy(d => d.SeqNo).select<ViewContainerModel>((d: DetailView_Detail) => {
      return {
        Oid: d.Oid,
        Name: d.Name,
        Caption: d.Caption,
        IsShowLabel: d.IsShowLabel,
        ControlType: d.ID_ControlType,
        Type: d.ID_ControlType == ControlTypeEnum.Tab ? ViewContainerModelTypeEnum.Tab : (d.ID_ControlType == ControlTypeEnum.Section ? ViewContainerModelTypeEnum.Section : ViewContainerModelTypeEnum.Control),
        GroupIndex: d.GroupIndex,
        ColCount: d.ColCount,
        ColSpan: d.ColSpan,
        SeqNo: d.SeqNo,
        Childs: []
      }
    }).toArray();

  //console.log("Roots",roots);

  var initChilds = (details: ViewContainerModel[]): void => {
    details.forEach(det => {
      var field = det.ControlType == ControlTypeEnum.Section ? "ID_Section" : "ID_Tab";
      det.Childs = new TS.List(detailView.Details).where(d => d[field] == det.Oid &&
        (d.ID_ControlType == ControlTypeEnum.Tab || d.ID_ControlType == ControlTypeEnum.Section))
        .select<ViewContainerModel>(
          (detail: DetailView_Detail) => {
            return {
              Oid: detail.Oid,
              Name: detail.Name,
              Caption: detail.Caption,
              ControlType: detail.ID_ControlType,
              GroupIndex: detail.GroupIndex,
              Type: detail.ID_ControlType == ControlTypeEnum.Tab ? ViewContainerModelTypeEnum.Tab : ViewContainerModelTypeEnum.Section,
              ColCount: detail.ColCount,
              IsShowLabel: detail.IsShowLabel,
              ColSpan: detail.ColSpan,
              SeqNo: detail.SeqNo,
              Childs: []
            }
          }
        ).toArray();
      if (det.Childs.length > 0) initChilds(det.Childs);
    });
  };
  initChilds(roots);

  var initControlsHtml = (det: ViewContainerModel): string => {
    var controls = new TS.List(detailView.Details).where(
      (d: any) => [ControlTypeEnum.Tab, ControlTypeEnum.Section].includes(d.ID_ControlType) == false &&
        d[det.ControlType == ControlTypeEnum.Tab ? "ID_Tab" : "ID_Section"] === det.Oid).orderBy(d => d.SeqNo).toArray();
    if (controls.length == 0) return "";
    var str = "";
    str += getControlsHtml(controls, det.ControlType == ControlTypeEnum.Tab);
    return str;
  }

  var getControlsHtml = (controls: DetailView_Detail[], isTab: boolean): string => {
    var str = "";
    controls.forEach(async d => {
      if (d.ID_ControlType == ControlTypeEnum.Tab) {
        if (d.ColSpan == null) d.ColSpan = 12;
      }
      if (d.ColSpan == null) d.ColSpan = 3;
      if (d.ID_ControlType == ControlTypeEnum.ListView) {
        // if ( isNullOrUndefined(d.Height) == true ) d.Height = 100;
        // if  (d.Height == 0 ) d.Height = 100;
        d.Height = 100;
      }
      var html = `<div ${d.ID_ControlType !== ControlTypeEnum.ListView && isNullOrUndefined(d.Oid) !== true ? `id="${d.Oid}"` : ""}   style="margin-bottom:5px;${d.ID_ControlType == ControlTypeEnum.ListView ? "max-height:1500px" : ""}"
          class="${isTab == true && d.ID_ControlType !== ControlTypeEnum.ListView && isFilterMode !== true ? `ui-g-${d.ColSpan}` : ""} oid-control"  
          ${d.Height > 0 && d.ID_ControlType == ControlTypeEnum.ListView ? `[style.height.px]=${d.Height}` : ""}>`;

      var controlName = null;
      var controlOption: BaseAppControlOption = null;
      if (d.ID_ControlType == null) {
        if (d.ID_PropertyType == 5) d.ID_ControlType = ControlTypeEnum.DatePicker;
        if (d.ID_PropertyType == PropertyTypeEnum.Bit) d.ID_ControlType = ControlTypeEnum.CheckBox;
        if (d.ID_PropertyType == PropertyTypeEnum.String) d.ID_ControlType = ControlTypeEnum.TextBox;
        if (d.ID_PropertyType == PropertyTypeEnum.Decimal || d.ID_PropertyType == PropertyTypeEnum.Int) {
          d.ID_ControlType = ControlTypeEnum.NumberBox;
        }
        if ( d.ID_PropertyType == PropertyTypeEnum.Date ) d.ID_ControlType = ControlTypeEnum.DatePicker;
        if ( d.ID_PropertyType == PropertyTypeEnum.DateTime ) d.ID_ControlType = ControlTypeEnum.DateTimePicker;
      }

      if (isNullOrUndefined(d.DisplayProperty) !== true && d.IsReadOnly == true) {
        d.ID_ControlType = ControlTypeEnum.TextBox;
        d.Name = d.DisplayProperty;
      }

      if ( isFilterMode == true && d.ID_PropertyType == PropertyTypeEnum.Bit ) {
        d.ID_ControlType = ControlTypeEnum.MultipleListBox;
      }

      switch (d.ID_ControlType) {
        case ControlTypeEnum.ListView:
          controlName = 'app-data-table';
          var gridOption: DataTableOption = new DataTableOption();
          gridOption.showFilter = false;
          gridOption.multiSelect = true;
          gridOption.Oid_ListView = d.ID_ListView;
          if (d.ID_ModelProperty) {
            gridOption.allowEditing = true;
            gridOption.newRowEditing = true;
          }
          // gridOption.actionRowItems = [
          //   {
          //     icon : "red-text mdi mdi-delete-circle",
          //     command: () => { 

          //     }
          //   }
          // ];
          gridOption.onContextMenu_Show = new EventEmitter<IContextMenuEvent>();
          gridOption.onContextMenu_Show.subscribe((evt: IContextMenuEvent) => {
            var grid = evt.grid;
            var itemSelecteds = grid.getItemsSelected();
            //console.log("Item Selected",itemSelecteds);
          });
          controlOption = gridOption;
          break;
        case ControlTypeEnum.TextBox:
        default:
          controlName = 'app-text-box';
          break;
        case ControlTypeEnum.NumberBox:
          controlName = 'app-number-box';
          break;
        case ControlTypeEnum.SelectBox:
          controlName = 'app-select-box';
          var selectBoxOption: IAppSelectBoxOption = {}
          var items = detailView.SelectBoxOptions ? detailView.SelectBoxOptions[d.Name] : null;
          if (items) {
            selectBoxOption.items = items;
          }
          if (d.DataSource) selectBoxOption.sourceKey = d.DataSource;
          //alert(selectBoxOption.sourceKey);
          if (d.DataSourceKey) selectBoxOption.propertyKey = d.DataSourceKey;
          controlOption = selectBoxOption;
          break;
        case ControlTypeEnum.ListBox:
          controlName = 'app-lookup-box';
          var appLookUpBox: IAppLookupBoxOption = {
            Oid_ListView: d.ID_ListView,
            apiUrl: `Model/GetList/${d.ID_ListView}`,
            sourceKey: d.DataSource,
            Oid_Model: d.ID_ModelProperty
          };
          if (d.DataSourceKey) appLookUpBox.sourceKey = d.DataSourceKey;

          //if (d.DisplayProperty) appLookUpBox.p = d.DataSourceKey;
          controlOption = appLookUpBox;
          break;
        case ControlTypeEnum.DateTimePicker:
        case ControlTypeEnum.DatePicker:
          controlName = 'app-date-box';

          break;
        case ControlTypeEnum.CheckBox:
          controlName = 'app-check-box';
          break;
        case ControlTypeEnum.TextArea:
          controlName = 'app-text-area';
          if (d.ID_LabelLocation == null) d.ID_LabelLocation = LabelLocaltionEnum.Top;
          break;
        case ControlTypeEnum.ImageBox:
          controlName = 'app-image-box';
          if (d.ID_LabelLocation == null) d.ID_LabelLocation = LabelLocaltionEnum.Top;
          break;
        case ControlTypeEnum.MultipleListBox:
          controlName = 'app-multiplelist-box';
          var selectBoxOption: IAppMultipleListBoxOption = {}
          var items = detailView.SelectBoxOptions ? detailView.SelectBoxOptions[d.Name] : null;
          if (items) {
            selectBoxOption.items = items;
          }
          if (d.DataSource) selectBoxOption.sourceKey = d.DataSource;
          //alert(selectBoxOption.sourceKey);
          if (d.DataSourceKey) selectBoxOption.propertyKey = d.DataSourceKey;
          controlOption = selectBoxOption;
          d.ID_LabelLocation = LabelLocaltionEnum.Top;
          if ( isFilterMode == true && d.ID_PropertyType == PropertyTypeEnum.Bit ) {
            selectBoxOption.items = [{
              ID: 1,
              Name : "True",
            },{
              ID: 0,
              Name : "False"
            }];
          }
          break;
        case ControlTypeEnum.MultipleSelectBox:
          controlName = 'app-multipleselect-box';
          var selectBoxOption: IAppMultipleSelectBoxOption = {}
          var items = detailView.SelectBoxOptions ? detailView.SelectBoxOptions[d.Name] : null;
          if (items) {
            selectBoxOption.items = items;
          }
          if (d.DataSource) selectBoxOption.sourceKey = d.DataSource;
          //alert(selectBoxOption.sourceKey);
          if (d.DataSourceKey) selectBoxOption.propertyKey = d.DataSourceKey;
          controlOption = selectBoxOption;
          d.ID_LabelLocation = LabelLocaltionEnum.Top;
          if ( isFilterMode == true && d.ID_PropertyType == PropertyTypeEnum.Bit ) {
            selectBoxOption.items = [{
              ID: 1,
              Name : "True",
            },{
              ID: 0,
              Name : "False"
            }];
          }
          break;
      };

      if (opt?.onControlOption_Initialized) {
        opt?.onControlOption_Initialized(d.Name, controlOption);
      }

      if (d.ID_LabelLocation == null) d.ID_LabelLocation = LabelLocaltionEnum.Left;

      if (controlName != null) {
        var optionName = null;
        if (controlOption != null) {
          optionName = d.Name + "_Option";
          //cmpHolder[optionName] = controlOption;
          cmpHolder.ControlOption[optionName] = controlOption;
        }
        var name = `name="${d.Name}"`
        var oid = `oid_Control="${d.Oid}"`;
        var required = d.IsRequired == true ? `[required]="true"` : ''
        var caption = `caption="${d.Caption != null ? d.Caption : d.Name}"`;
        var readOnly = d.IsReadOnly == true ? `[readOnly]="true"` : '';
        var isDisabled = d.IsDisabled == true ? `[disabled]="true"` : '';
        var labelLocation = null;
        var showTime: string = '';
        var filterMode: string = isFilterMode == true ? '[filterMode]="true"' : '';
        if (d.ID_ControlType == ControlTypeEnum.DatePicker) showTime = `[show-time]="false"`;
        switch (d.ID_LabelLocation) {
          case LabelLocaltionEnum.Right:
            labelLocation = "labelPosition='right'";
            break;
          case LabelLocaltionEnum.Top:
            labelLocation = "labelPosition='top'";
            break;
        }
        //<div>-->Value{{CurrentObject.${d.Name}}}</div>
        html += `<${controlName} [(ngModel)]="CurrentObject.${d.Name}" ${name} ${caption} ${required} ${readOnly} ${isDisabled} ${oid}
                  ${optionName != null ? `[option]="${optionName}"` : ""}
                  ${labelLocation != null ? labelLocation : ''} ${showTime} ${filterMode}></${controlName}>`;
        //console.log(html);
      };


      html += `</div>`;
      if ( isFilterMode ==  true ) {
        html = `<p-accordionTab id="${d.Name}" [transitionOptions]="'0ms'" header="${d.Caption}">${html}</p-accordionTab>`;
      }

      if (d.ID_ControlType == ControlTypeEnum.ListView) {
        // <p-fieldset legend="${d.Caption ?? ''}" class="detail-field-set ${d.Caption && d.IsShowLabel == true ? `with-caption` : "no-caption"}">
        html = `
                <div class="detail-grid oid-control" id="${d.Oid}">
                  <p-toolbar>
                    ${ d.Caption && d.IsShowLabel == true ?
            `
                        <div class="ui-toolbar-group-left">
                          <div style="padding:10px">${d.Caption && d.IsShowLabel == true ? d.Caption : ""}</div>
                        </div>
                        <div class="ui-toolbar-group-right">
                              <p-menubar #${d.Name}Menu [model]="${d.Name}_MenuItems" [hidden]="${d.Name}_MenuItems == undefined || ${d.Name}_MenuItems?.length == 0" class="detail-menubar"></p-menubar>
                        </div>
                     ` : `
                        <div class="ui-toolbar-group-left">
                              <p-menubar #${d.Name}Menu [model]="${d.Name}_MenuItems" [hidden]="${d.Name}_MenuItems == undefined || ${d.Name}_MenuItems?.length == 0" class="detail-menubar"></p-menubar>
                        </div>
                        <div class="ui-toolbar-group-right">
                        </div>
                    `}
                  </p-toolbar>
                  ${html}
                </div>`
        if (isTab == true) html = `<div  ${d.ID_ControlType === ControlTypeEnum.ListView ? `id="${d.Oid}"` : ""} class="ui-g-${d.ColSpan} oid-control">${html}</div>`
      };
      str += html;
    });
    //if (isTab == true) return `<div class="p-grid">${str}</div>`
    return str;
  }

  var initHtml = (details: ViewContainerModel[], colCount: number): string => {
    var str: string = "";
    var tempGroups = new TS.List(details).groupBy((d) => d.Type + "_" + (d.GroupIndex ? d.GroupIndex : -1)).select((d) => {
      return {
        type: Number.parseInt(d.key.split("_")[0]), //,
        group: Number.parseInt(d.key.split("_")[1]),
        childs: d.value.toArray()
      }
    }).orderBy(d => d.group).toArray();
    //console.log("groups",tempGroups);
    tempGroups.forEach(g => {
      var childs = new TS.List(g.childs).orderBy(d => d.SeqNo).toArray();
      switch (g.type) {
        case ViewContainerModelTypeEnum.Tab:
          //tabViews.forEach((element) => {
          str += "<p-tabView>";
          childs.forEach(d => {
            // leftIcon="pi pi-calendar"
            str += `<p-tabPanel header="${d.Caption}" id="${d.Oid}" class="oid-control tab-panel-custom">`
            if (d.Childs.length > 0) {
              str +=
                `<div class="ui-g">
                        ${initHtml(d.Childs, colCount)}
                        ${initControlsHtml(d)}
                      </div>`
            } else {
              str += ('<div class="p-grid" [style.padding.px]="10">' + initControlsHtml(d) + '</div>');
            }
            str += `</p-tabPanel>`
          });
          str += "</p-tabView>";
          //});
          break;
        case ViewContainerModelTypeEnum.Section:
          str += `<div class="ui-g-12">`
          childs.forEach(element => {
            //console.log(element,element.ColSpan);
            str += `<div class="ui-g-${element.ColSpan} ui-g-nopad">`;
            str += `<div id="${element.Oid}" class="oid-control" [style.padding.px]="5">`;
            str += `<p-fieldset legend="${element.Caption}" class="${element.Caption && element.IsShowLabel == true ? `with-caption` : "no-caption"}">`;
            if (element.Childs.length > 0) {
              str += `<div class="p-grid">${initHtml(element.Childs, element.ColCount)}</div>`;
            }
            str += initControlsHtml(element);
            str += `</p-fieldset>`;
            str += `</div>`;
            str += "</div>";
          });
          str += "</div>";
          break;
        case ViewContainerModelTypeEnum.Control:
          if (childs.length > 0) {
            var Oids = new TS.List(childs).select(d => d.Oid).toArray();
            var childControls = new TS.List(detailView.Details).where(d => Oids.includes(d.Oid) == true).toArray();

            if (isFilterMode == true) {
              str += `<p-accordion #filterAccordioView  [multiple]="true">`;
            }

            str += getControlsHtml(childControls, true);

            if (isFilterMode == true) {
              str += `</p-accordion>`;
            }
          }
          break;
      }
    });
    // var tabViews = new TS.List(details).where(d => d.ControlType == ControlTypeEnum.Tab)
    //   .orderBy(d => d.SeqNo).groupBy((d) => d.GroupIndex).orderBy(d => d.key).toArray();
    // var sections = new TS.List(details).where(d => d.ControlType == ControlTypeEnum.Section).orderBy(d => d.SeqNo).toArray();
    // var controls = new TS.List(detailView.Details).where(d =>
    //   new TS.List(details).where(det => [ControlTypeEnum.Tab, ControlTypeEnum.Section].includes(det.ControlType) == false)
    //     .select(det => det.Oid).toArray().includes(d.Oid)
    // ).toArray();
    return str;
  };

  var _html  = initHtml(roots,12);
  var html = '<app-form #appForm  [CurrentObject]="CurrentObject" [style.margin-bottom.px]="50">' + _html + '</app-form>';
  return html;
}


class ViewContainerModel {
  Oid: string;
  Name: string;
  Caption: string;
  ControlType: ControlTypeEnum;
  GroupIndex: number;
  ColCount: number;
  IsShowLabel: boolean;
  ColSpan: number;
  SeqNo: number;
  Type: ViewContainerModelTypeEnum;
  Childs: ViewContainerModel[];
}

enum ViewContainerModelTypeEnum {
  Section = 1,
  Tab = 2,
  Control = 3
}

