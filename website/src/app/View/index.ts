import { MenuItem } from 'primeng/api/menuitem';

export interface ViewMenuItem extends MenuItem {

    IsDisabled?: () => boolean;
    IsVisible?: () => boolean;
  
  }