
import '@angular/compiler';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { FormHelperViewComponent, FormHelperDialogService } from './../FormHelperView/FormHelperView.component';
import { IPagingOption } from './../../../utils/service/data.service';
import { Guid } from 'guid-typescript';
import { Model, DetailView_Detail, APP_MODEL, ListView_Detail, PropertyType, DetailView, APP_DETAILVIEW, PatientAppointment, AppointmentSchedule } from './../../../bo/APP_MODELS';
import { ViewTypeEnum, ListViewModel, PropertyTypeEnum, ControlTypeEnum, UserGroupEnum } from 'src/utils/controls/class_helper';
import { ViewMenuItem } from './../index';
import { ICellEvent, LabelLocaltionEnum } from './../../../utils/controls/class_helper';
import { AppDataTableComponent, DataTableOption, DataTableColumn, IContextMenuEvent } from 'src/utils/controls/appDataTable/appDataTable.component';
import { MenuItem } from 'primeng/api/menuitem';
import { Component, OnInit, Injector, NgModuleRef, ChangeDetectorRef, ViewChild, ViewContainerRef, Injectable, Input, EventEmitter, isDevMode, Output, ErrorHandler } from '@angular/core';
import { DataService } from 'src/utils/service/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DynamicTemplateService, DetailViewHolder } from 'src/app/AppServices/dynamic-temaplate.svc';
import { MessageBoxService } from 'src/utils/controls/appModal/appModal.component';
import { REGISTRY_VC_MODEL } from 'src/bo/VC_Model.decorator';
import { ListView, APP_LISTVIEW } from 'src/bo/APP_MODELS';
import { ListViewModalService } from './ListView.service';
import { HttpClient } from '@angular/common/http';
import { VC_BaseView } from 'src/bo/VC/Base/VC_BaseView';
import { isNullOrUndefined, isNull } from 'util';
import { DetailViewDialogService } from '../DetailViewDialog/DetailViewDialog.component';
import { generateFormHtmlText } from '../FormHelperView/FormHelperView.component';
import * as TS from 'linq-typescript';
import { CustomDetailViewRouteService } from 'src/utils/service/customdetailviewroute.service';
import { GlobalfxService } from 'src/utils/service/globalfx.service';
import { UserAuthenticationService } from 'src/app/AppServices/UserAuthentication.service';

@Component({
  selector: 'app-list-view',
  templateUrl: './ListView.component.html',
  styleUrls: ['./ListView.component.less']
})
export class ListViewComponent implements OnInit, ErrorHandler {

  @ViewChild('vc', { read: ViewContainerRef })
  _container: ViewContainerRef;

  @ViewChild('vcFilter', { read: ViewContainerRef })
  _containerFilterView: ViewContainerRef;

  userTOken: any = {};

  constructor(
    private _injector: Injector, 
    private _m: NgModuleRef<any>, 
    private ds: DataService,
    private router: Router, 
    private route: ActivatedRoute, 
    private tpl: DynamicTemplateService,
    private changeDetector: ChangeDetectorRef, 
    private msgBox: MessageBoxService, 
    private lvModalSvc: ListViewModalService,
    private httpClient: HttpClient, 
    private dvModalSvc: DetailViewDialogService, 
    private dvFormSvc: FormHelperDialogService,
    private cs:CrypterService,
    private customDetailViewRoute: CustomDetailViewRouteService,
    protected globalFx: GlobalfxService,
    protected userAuthSvc: UserAuthenticationService,
  ) { 


    this.userTOken = this.userAuthSvc.getDecodedToken();
  }

  async ngOnInit() {

    await this.customDetailViewRoute.load();
  }

  handleError(error) {

    console.log(error);
  }

  vcModels: VC_BaseView[] = [];

  @Input()
  showHeader: boolean = true;

  menuItems: ViewMenuItem[] = [];

  @Input()
  lvParams?: any = null;

  @Input()
  ID_ListView: string = null;

  gridOption: DataTableOption = {
    caption: "-"
  };

  primaryKey: string = "ID";

  @Output('Initialized')
  onGridOption_Initialized: EventEmitter<DataTableOption> = new EventEmitter();

  appDataTable: AppDataTableComponent;

  breadCrumbMenuns: MenuItem[] = [];
  home: MenuItem = { icon: 'pi pi-home', routerLink: '/Home' };

  CurrentObject: any = {}
  filterControls: DetailView_Detail[] = [];
  showFilterBox: boolean = false;
  dvViewHolder: DetailViewHolder = null;

  openCustomDetailView = null;

  ngAfterViewInit(): void {
    if (this.ID_ListView) {
      this.initView(this.ID_ListView);
    } else {
      this.route.paramMap.subscribe(async (params) => {
        var Oid = atob(params.get("Oid_ListView"));
        this.initView(Oid);
      });
    }
}

  toggeFilterBox(): void {
    this.showFilterBox = !this.showFilterBox;
  }

  async clearFilter(): Promise<void> {
    var list = this.dvViewHolder.appForm.getFilterFormValue().length;
    if (await this.msgBox.confirm("Are you sure you want to clear?", this.gridOption.caption, "Yes", "No") == true) {
      this.dvViewHolder.CurrentObject = {};
      this.dvViewHolder.appForm.clearFilter();
      if ( list == 0 ) return;
      setTimeout(() => {
        this.appDataTable.reload(true);
      }, 500);
    }
  }

  searchFilter(): void {
    var validations = this.dvViewHolder.appForm.getValidations();
    if (validations.length > 0) {
      this.msgBox.showValidationsBox(validations);
      return;
    };
    this.appDataTable.reload(true);
  }

  openDetailView(ModelName: string, ID_CurrentObject: string): void {

    var Oid_DetailView = this.gridOption.Oid_DetailView;
    var isExist = this.customDetailViewRoute.IsExist(Oid_DetailView);
    
    if(isExist){

      this.customDetailViewRoute.navigate(Oid_DetailView, ID_CurrentObject, {

        Oid_DetailView: Oid_DetailView,
        ID_CurrentObject: ID_CurrentObject
      });
    }else{

      if(Oid_DetailView.toLowerCase() == APP_DETAILVIEW.PATIENTAPPOINTMENT_DETAILVIEW.toLowerCase()){

        this.gotoPatient(ID_CurrentObject);
      }else{

        this.router.navigate([`/Main/dv/${ModelName}/${btoa(this.gridOption.Oid_DetailView)}/${ID_CurrentObject}`], {
          queryParams: {
            lv: btoa(this.ID_ListView)
          }
        });
      }

    }
  }

  async gotoPatient(ID_CurrentObject){

    var sql = this.cs.encrypt(`SELECT ID, ID_Patient FROM vAppointmentSchedule WHERE ID = ${ID_CurrentObject}`);   
    var records = await this.ds.query<AppointmentSchedule>(sql);

    if(records.length > 0){

      var routerLink = [];
      var config = {

        ID_AppointmentSchedule: records[0].ID,
      };

      routerLink = [`/Main`, 'Patient', records[0].ID_Patient];

      this.globalFx.customNavigate(routerLink, config);
    }
  }

  async initView(Oid: string): Promise<any> {
    this.filterControls = [];
    this.showFilterBox = false;
    this.breadCrumbMenuns = [];
    this.vcModels = [];
    this._container.clear();
    this._containerFilterView.clear();
    this.ID_ListView = Oid;
    var listView: ListViewModel = null;
    if (Guid.isGuid(this.ID_ListView) != true) this.ID_ListView = null;
    if (this.ID_ListView) {
      var listViews = await this.httpClient.get("assets/listview/_Index.json").toPromise<any>();
      if (Object.keys(listViews).includes(this.ID_ListView.toUpperCase()) && isDevMode() != true) {
        listView = await this.httpClient.get(`assets/listview/${listViews[this.ID_ListView.toUpperCase()]}.json`).toPromise<any>();//;.then((listView: ListViewModel) => {
      } else {
        listView = await this.ds.get(`Model/ListView/${this.ID_ListView}`);//;.then((listView: ListViewModel) => {
      }
    } else {
      listView = {
        Oid: null,
        Model: "",
        Name: "",
      }
    }
    if (listView == null) return;
    var ModelName = listView.Model;
    var view_name = listView.Name;
    this.primaryKey = listView.PrimaryKey;
    //console.log(listView);
    document.title = "VeCS - Vet. Cloud System - " + listView.Caption;
    var vcClasses = REGISTRY_VC_MODEL.get(view_name);
    var AllViews = REGISTRY_VC_MODEL.get("*");

    if (AllViews) {
      if (vcClasses == undefined) vcClasses = [];
      vcClasses = vcClasses.concat(AllViews);
    };
    if (vcClasses) {
      vcClasses.forEach(c => {
        var newInstance = Object.create(c.prototype) as VC_BaseView;// as VC_BaseDetailView;
        newInstance.ViewType = ViewTypeEnum.ListView;
        newInstance.ViewParams = this.lvParams;
        newInstance.msgBox = this.msgBox;
        newInstance.dataService = this.ds;
        newInstance.lvModal = this.lvModalSvc;
        newInstance.dvModal = this.dvModalSvc;
        newInstance.dvFormSvc = this.dvFormSvc;
        this.vcModels.push(newInstance);
      });
    };

    this.menuItems = [
      {
        label: "Refresh",
        icon: "green-text pi pi-fw pi-refresh",
        command: () => {
          this.appDataTable.reloadData(true);
        }
      },
    ];

    if (listView.ID_DetailView) {
      this.menuItems.unshift({
        label: "New",
        icon: "pi pi-fw pi-file-o",
        command: (evt) => {
          if (this.gridOption.Oid_DetailView) {

            // this.router.navigate([`dv/${ModelName}/${btoa(this.gridOption.Oid_DetailView)}/-1`]);
            this.openDetailView(ModelName, '-1');
          }
        }
      });
    };

    //
    // FILTER
    //

    var _filterControls = new TS.List(listView.Details).where(d => d.IsActive == true && d.IsFilter == true).orderBy(d => d.VisibleIndex).toArray();
    _filterControls.forEach(fd => { 
      var Name = fd.Name;
      if ( fd.ID_FilterControlType == ControlTypeEnum.TextBox && isNullOrUndefined(fd.DisplayProperty) != null ) {
        Name = fd.DisplayProperty;
      }
      this.filterControls.push(
        {
          Name: Name,
          Caption : fd.Caption ? fd.Caption : fd.Name,
          ID_ControlType : fd.ID_FilterControlType,
          DataSource : this.cs.encrypt(fd["$DataSource"]),
          ID_PropertyType : fd.ID_PropertyType
        }
      )
    });

    if (this.vcModels.length > 0) {
      this.vcModels.forEach(async d => {
        await d.onFilterControls_Initialized(this.filterControls);
      })
    };
    this.filterControls.forEach(d => {
      d.ColSpan = 12;
      d.ID_LabelLocation = LabelLocaltionEnum.Top;
    });
    var cmpHolder = this as any;
    cmpHolder.ControlOption = {};
    if (this.filterControls?.length > 0) {
     //this.showFilterBox = true;
      var html = generateFormHtmlText({
        Oid: this.ID_ListView,
        Details: this.filterControls
      }, cmpHolder, true);
      var cmpRef = await this.tpl.createAndCompileComponents(html, this._injector, this._m);//.then(cmpRef => {
      var instance = cmpRef.instance as DetailViewHolder;
      instance.CurrentObject = this.CurrentObject;
      this.dvViewHolder = instance;
      var OptionKeys = Object.keys(cmpHolder.ControlOption);
      OptionKeys.forEach(key => {
        instance[key] = cmpHolder.ControlOption[key];
      });
      this._containerFilterView.insert(cmpRef.hostView);

      instance.onView_Initialzied = () => {

        if(isNullOrUndefined(this.filterControls)) this.filterControls = [];
        instance.appForm.details = this.filterControls;

        if(!isNullOrUndefined(instance.appForm['controls'])){

          instance.appForm.controls.forEach(c => {
            c.onEnterKey.subscribe((evt) => {
              this.searchFilter();
            });
          });
        } 
      }
    }



    //
    // END FILTER
    //
    this.changeDetector.detectChanges();
    this.gridOption = {
      Oid_ListView: this.ID_ListView,
      apiUrl: `Model/GetList/${this.ID_ListView}`,
      caption: "-",
      showFilter: false,
      onContextMenu_Show: new EventEmitter<IContextMenuEvent>(),
      onCell_DoubleClick: new EventEmitter<ICellEvent>(),
      onCell_EnterKey: new EventEmitter<ICellEvent>(),
      Oid_DetailView: listView.ID_DetailView,
      actionRowItems: [],
      onBefore_LoadData: new EventEmitter<IPagingOption>()
      // actionRowItems: [
      //   {
      //     class: 'btn-info',
      //     icon: "fa fa-folder-open",
      //     url: (rowKey) => {
      //       return `dv/${ModelName}/${btoa(this.gridOption.Oid_DetailView)}/${rowKey.ID}`
      //     },
      //   },
      //   {
      //     class: 'btn-success',
      //     icon: "fa fa-comment",
      //     command: () => { 


      //     }
      //   }
      // ]
    };

    this.gridOption.onBefore_LoadData.subscribe((opt: IPagingOption) => {

      if(isNullOrUndefined(this.filterControls)) this.filterControls = [];

      if (this.filterControls.length > 0) {
        if (this.dvViewHolder != null) {
          // alert('xxxx');
          var filterValues = this.dvViewHolder.appForm.getFilterFormValue();
          opt.filterFormValue = JSON.stringify(filterValues);
          //console.log("filter values-->", opt.filterFormValue);
        }
      }
    });

    await this.onGridOption_Initialized.emit(this.gridOption);
    if (isNullOrUndefined(this.gridOption.Oid_DetailView) != true) {
      this.gridOption.actionRowItems.push(
        {
          class: 'btn-info',
          icon: "fa fa-folder-open",
          // url: (rowKey) => {
          //   return ['Admin', 'dv', ModelName, btoa(this.gridOption.Oid_DetailView),  rowKey.ID];
          //   // `/dv/${ModelName}/${btoa(this.gridOption.Oid_DetailView)}/${rowKey.ID}`;
          // },
          command: (evt: any, rowKey: any) => {
            this.openDetailView(ModelName, rowKey[this.primaryKey]);
          }
        },
      );
    }

    this.gridOption.onCell_DoubleClick.subscribe((evt: ICellEvent) => {
      this.openDetailView(ModelName, evt.key[this.primaryKey]);

    });
    this.gridOption.onCell_EnterKey.subscribe((evt: ICellEvent) => {
      this.openDetailView(ModelName, evt.key[this.primaryKey]);
    });

    this.gridOption.onContextMenu_Show.subscribe((e: IContextMenuEvent) => {

      if(this.userTOken.ID_UserGroup != UserGroupEnum.System) return;

      e.menus.unshift({
        label: "Reload View",
        icon: "green-text pi pi-fw pi-refresh",
        command: async () => {
         await this.initView(this.ID_ListView);
         this.msgBox.success("Reload view successfully");
        }
      });

      if (e.isHeader == true && e.column) {
        if (e.column.oid) {
          e.menus.push({
            label: "Edit Column " + e.column.dataField,
            icon: "pi pi-fw pi-table",
            command: () => {
              this.lvModalSvc.open(APP_LISTVIEW.LISTVIEW_DETAIL_LISTVIEW, {
                Oid_ListView: this.ID_ListView,
                Oid: e.column.oid
              }, {
                height: 350,
                title: `${view_name} Edit Columns...`
              });
            }
          });
        }
      }

      e.menus.push({
        label: "Edit Columns",
        icon: "pi pi-fw pi-table",
        command: () => {
          this.lvModalSvc.open(APP_LISTVIEW.LISTVIEW_DETAIL_LISTVIEW, {
            Oid_ListView: this.ID_ListView
          }, {
            title: `${view_name} Edit Columns...`
          });
        }
      });
      if (e.isHeader == true && e.column) {
        if (e.column.oid) {
          e.menus.push({
            label: e.column.dataField + " filter settings",
            icon: "pi pi-fw pi-table",
            command: async () => {
              var res = await this.dvFormSvc.open({
                IsFilter: e.column.isFilter,
                ID_FilterControlType: e.column.filterEditorType,
                DataSource: e.column.dataSource,
                ID_PropertyType : e.column.id_propertyType,
                DisplayProperty: e.column.displayDataField,
              }, e.column.dataField + " filter settings", [
                {
                  Name: "IsFilter",
                  Caption: "Filter",
                  ID_PropertyType: PropertyTypeEnum.Bit
                },
                {
                  Name: "ID_FilterControlType",
                  Caption: "Control Type",
                  ID_PropertyType: PropertyTypeEnum.Int,
                  ID_ControlType: ControlTypeEnum.SelectBox,
                  DataSource: this.cs.encrypt(`
                    SELECT ID, Name
                    FROM dbo._tControlType tct
                    WHERE ID IN(1, 3, 6, 18, 19);`)
                },
                {
                  Name: "ID_PropertyType",
                  Caption: "Property Type",
                  ID_PropertyType: PropertyTypeEnum.Int,
                  ID_ControlType: ControlTypeEnum.SelectBox,
                  DataSource: this.cs.encrypt("SELECT ID, Name FROM _tPropertyType"),
                  IsLoadData: true,
                },
                {
                  Name: "DataSource",
                  Caption: "Data Source",
                  ID_PropertyType: PropertyTypeEnum.String,
                  ID_ControlType: ControlTypeEnum.TextArea
                },
                {
                  Name : "DisplayProperty",
                  Caption : "Display Property",
                  IsReadOnly : true
                }
              ]);
              if (res == false) return;
              await this.ds.saveObject(APP_MODEL.LISTVIEW_DETAIL, {
                Oid: e.column.oid,
                DataSource: res.DataSource,
                ID_FilterControlType: res.ID_FilterControlType,
                IsFilter: res.IsFilter
              });
              await this.initView(this.ID_ListView);
              this.msgBox.success("Column filter settings updated");
            }
          })
        }
      }

    });
    this.vcModels.forEach((c: VC_BaseView) => {
      //c.menuItems = this.menuItems;
      c.onDataTableOption_Initialized(this.gridOption);
    });
    this.gridOption.onView_Initialized = (grid: AppDataTableComponent) => {
      grid.focus();
      this.appDataTable = grid;
      //console.log('grid-->', grid);
      this.vcModels.forEach(async (c: VC_BaseView) => {
        c.menuItems = this.menuItems;
        c.appDataTable = grid;
        c.ngAfterViewInit();
        this.menuItems = c.menuItems;
        this.changeDetector.detectChanges();
      });
      //console.log(this.menuItems);
      return Promise.resolve();
    };
    var html = `<app-data-table [option]="gridOption"></app-data-table>`;
    var v = await this.tpl.createAndCompileComponents(html, this._injector, this._m);
    v.instance.gridOption = this.gridOption;
    //console.log("INSTANCE-->", v.instance);
    this._container.insert(v.hostView);
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.

  }

}



export class ListView_DTO extends ListView {

  Model?: string;

}