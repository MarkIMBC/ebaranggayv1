import { ElementRef } from '@angular/core';
import { CrypterService } from 'src/utils/service/Crypter.service';
import { MessageBoxService } from './../../../utils/controls/appModal/appModal.component';
import { ListViewComponent } from './ListView.component';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, ViewChild, Injectable } from '@angular/core';
import { DataTableOption, AppDataTableComponent } from 'src/utils/controls/appDataTable/appDataTable.component';
import * as TS from 'linq-typescript';
import { isNullOrUndefined } from 'util';

@Component({
  templateUrl: './ListViewDialog.component.html'
})
export class ListViewComponentDialog {

  constructor(public actModal: NgbActiveModal, private crypterSvc: CrypterService) { }

  title: string = "Title";

  @ViewChild(`listView`)
  listView: ListViewComponent

  lvParams?: any = null;
  ID_ListView: string = null;
  height:number = 650;

  positiveButtonText:string = 'OK';

  onListView_Initialized(dt: DataTableOption) : void { }
  
  PostiveButtonClick() : void {}

  dismiss() : void {
    this.actModal.close();
  }

  ngAfterViewInit(): void { }

}


@Injectable({
  providedIn: "root"
})
export class ListViewModalService {

  constructor(private modalSvc: NgbModal,private msgBox: MessageBoxService, private crypterSvc: CrypterService) { }

  open(ID_ListView, params?: any, opt?:ILookupOption): Promise<void> {
    var cmp = this.modalSvc.open(ListViewComponentDialog, {
      windowClass: "list-view-dialog " + ( isNullOrUndefined(opt?.width) != true ? "custom-width-dialog" : "" ),
      size: "lg",
      centered: true,
      backdrop: "static"
    });
    if ( isNullOrUndefined(opt?.width) !== true ) {
      var parent = $(((cmp as any)._windowCmptRef.instance._elRef as ElementRef).nativeElement);
      //console.log("modaldialog",parent);
      setTimeout(() => {
        var modalDialog = parent.find('.modal-dialog');
        modalDialog.css("cssText",`width:${opt.width}px!important`);
        modalDialog.css("cssText",`max-width:${opt.width}px!important`) 
        setTimeout(() => {
          parent.css("cssText", "opacity:1!important");
        });
      });
    }
    var cmpLv = cmp.componentInstance as ListViewComponentDialog;
    cmpLv.height = opt.height ? opt.height : 450;
    cmpLv.ID_ListView = ID_ListView;
    cmpLv.title = opt?.title ? opt.title : "App";
    cmpLv.lvParams = params;
    return cmp.result;
  }

  ListLookUp<T>(opt : ILookupOption) : Promise<T[]> { 
    return new Promise<T[]>((res, rej) => { 
      var cmp = this.modalSvc.open(ListViewComponentDialog, {
        windowClass: "list-view-dialog " + ( isNullOrUndefined(opt.width) != true ? "custom-width-dialog" : "" ),
        size: "lg",
        centered: true,
        backdrop: "static"
      });

      if ( isNullOrUndefined(opt.width) !== true ) {
        var parent = $(((cmp as any)._windowCmptRef.instance._elRef as ElementRef).nativeElement);
        //console.log("modaldialog",parent);
        setTimeout(() => {
          var modalDialog = parent.find('.modal-dialog');
          modalDialog.css("cssText",`width:${opt.width}px!important`);
          modalDialog.css("cssText",`max-width:${opt.width}px!important`) 
          setTimeout(() => {
            parent.css("cssText", "opacity:1!important");
          });
        });
      }

      if(isNullOrUndefined(opt.positiveButtonText) != true){

        if(opt.positiveButtonText.length > 0) cmpLv.positiveButtonText = opt.positiveButtonText;
      }

      var cmpLv = cmp.componentInstance as ListViewComponentDialog;
      cmpLv.height = opt.height ? opt.height : 450;
      cmpLv.positiveButtonText = opt.positiveButtonText;
      cmpLv.ID_ListView = opt.ID_ListView ? opt.ID_ListView : null;

      cmpLv.onListView_Initialized = ((gridOption:DataTableOption)=>{

        gridOption.sourceKey = this.crypterSvc.encrypt(opt.sql);
        gridOption.multiSelect = true;
        gridOption.showFilter = true;
        if ( opt.onGridOption_Initialized ) opt.onGridOption_Initialized(gridOption);
      });

      cmpLv.PostiveButtonClick = () => { 

        var itemsSelected = cmpLv.listView.appDataTable.getItemsSelected();

        if ( itemsSelected.length > 0 ) {
          var items = new TS.List(itemsSelected).select(s=>s.value as T).toArray();
          cmpLv.actModal.dismiss();
          res(items);
        } else { 
            this.msgBox.showValidationBox("No Item(s) selected.");
        } 
      };

      cmpLv.dismiss = () => {

        cmpLv.actModal.dismiss();
        res([]);
      }

      cmpLv.ID_ListView = opt.ID_ListView;
      cmpLv.title = opt.title;
    });
  }

}

export class ILookupOption {
  title: string;
  ID_ListView?: string;
  sql?:string;
  width?:number;
  height?:number;
  positiveButtonText?: string;
  onGridOption_Initialized?: (gridOption: DataTableOption) => void = null; 
}
