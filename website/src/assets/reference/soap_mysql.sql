SELECT
    a.soap_id,
    a.vet_id,
    a.pet_id,
    a.done,
    a.date,
    b.name,
    c.subjective AS subj,
    d.heart_rate,
    d.respiratory_rate,
    d.weight,
    d.weight_unit,
    d.length,
    d.bcs,
    d.temperature,
    d.temperature_degrees,
    e.diagnosis,
    e.notes,
    e.test_result,
    e.final_diagnosis,
    e.prognosis,
    e.category,
    f.treatment,
    f.remarks,
    f.date_return,
    f.reason,
    f.priority,
    g.prescribed,
    g.prescribed_id,
	a.status
FROM
    soap AS a
LEFT JOIN users AS b
ON
    a.vet_id = b.user_id
LEFT JOIN subjective AS c
ON
    a.soap_id = c.soap_id
LEFT JOIN objectives AS d
ON
    a.soap_id = d.soap_id
LEFT JOIN assessment AS e
ON
    a.soap_id = e.soap_id
LEFT JOIN plan AS f
ON
    a.soap_id = f.soap_id
LEFT JOIN prescribed AS g
ON
    a.soap_id = g.soap_id
WHERE
	a.soap_id IS NOT NULL
ORDER BY
    a.soap_id
DESC