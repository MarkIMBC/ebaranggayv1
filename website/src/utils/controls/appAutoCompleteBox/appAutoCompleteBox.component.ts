import { IMenuItem } from '../class_helper';
import { FilterCriteriaType } from '../appControlContainer/appControlContainer.component';
import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef, EventEmitter, Output } from '@angular/core';
import { BaseAppControl, BaseAppControlOption } from '../_base/baseAppControl';
import { AppControlContainerComponent } from '..';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as $ from 'jquery';
import { GlobalfxService } from 'src/utils/service/globalfx.service';

@Component({
  selector: "app-auto-complete-box",
  templateUrl: "./appAutoCompleteBox.component.html",
  styleUrls: ["./appAutoCompleteBox.component.less"],
  providers: [
    { provide: BaseAppControl, useExisting: AppAutoCompleteBoxComponent },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: AppAutoCompleteBoxComponent,
      multi: true
    }
  ]
})
export class AppAutoCompleteBoxComponent extends BaseAppControl<string> {

  @Input()
  option: BaseAppControlOption = null;

  @ViewChild("appContainer", { static: false })
  _appContainer: AppControlContainerComponent;

  @Output() onValueChanged = new EventEmitter<any>();

  @Input()
  dataList: string[];

  filterCriteriaItems: IMenuItem[] = [
    {
      id: FilterCriteriaType.Like,
      label: 'Like'
    },
    {
      id: FilterCriteriaType.NotLike,
      label: 'Not Like'
    },
    {
      id: FilterCriteriaType.StartWith,
      label: 'Start With'
    },
    {
      id: FilterCriteriaType.EndWith,
      label: 'End With'
    },
    {
      id: FilterCriteriaType.Equal,
      label: 'Equals'
    },
    {
      id: FilterCriteriaType.NotEqual,
      label: 'Not Equals'
    },
  ];

  idAttribute: string = "";
  
  globalFx: GlobalfxService;

  constructor(el: ElementRef, ch: ChangeDetectorRef, _globalFx: GlobalfxService) {
    
    super(el, ch);

    this.globalFx = _globalFx;

    this.idAttribute = this.globalFx.generateRamdomCharacters(10);
  }

  focus() : void {
    
    $(this.input.nativeElement).focus();
  }

  onTextBox_ValueChange(_value): void {

    this.emitValueChange(_value);
    this.onControl_ValueChanged();

    this.onValueChanged.emit(_value);
  }

  isInvalid() : boolean {
    var required = this.isRequired();
    return this.isDirty == true && required == true && this.isEmptyString() == true;
  }

  afterLoad(): void { 

    if(!this.input.nativeElement != undefined && !this.input.nativeElement != null){

      $(this.input.nativeElement).attr("list", this.idAttribute);
    }
  }
}

export class IAppTextBoxOption extends BaseAppControlOption {
  autoCompleteApiUri?: string;
}

function IsNullOrUndefined(nativeElement: any) {
  throw new Error('Function not implemented.');
}

