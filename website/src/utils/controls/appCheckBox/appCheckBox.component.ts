import { Checkbox } from 'primeng/checkbox';
import { Component, OnInit, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { BaseAppControl, BaseAppControlOption } from '../_base/baseAppControl';
import { Guid } from 'guid-typescript';
import * as $ from "jquery";
import { NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-check-box',
  templateUrl: './appCheckBox.component.html',
  styleUrls: ['./appCheckBox.component.less'],
  providers: [{
    provide: BaseAppControl, useExisting: AppCheckBoxComponent
  }, {
    provide: NG_VALUE_ACCESSOR,
    useExisting: AppCheckBoxComponent,
    multi: true
  }]
})
export class AppCheckBoxComponent extends BaseAppControl<boolean> {

  @ViewChild('checkBox')
  checkBox: Checkbox;

  constructor(el: ElementRef, ch :ChangeDetectorRef) {
    super(el, ch);

  }

  focus() : void {
    this.checkBox.focused = true;
    var chcBox = $(this.el.nativeElement).find('.ui-chkbox-box');
    chcBox.attr("tabindex", 1);
    chcBox.focus();
  }

  writeValue(value: any): void { 
    this.value = value;
  }

  onCheckBox_ValueChange($event): void { 
    this.onControl_ValueChanged();
    this.emitValueChange(this.value);
  }

  ngAfterViewInit(): void {
    var chcBox = $(this.el.nativeElement).find('.ui-chkbox-box');
    chcBox.keydown((evt:any) => {
      var keyCode = evt.keyCode;
      if ( keyCode == 32 ) {
        //space;
        evt.stopPropagation();
        evt.preventDefault();

        this.checkBox.checked = !this.checkBox.checked;
        this.value = this.checkBox.checked;
        this.emitValueChange(this.value);
      }
    });
    this.checkBox.onBlur = (): void => {
      this.onBlur(null);
      chcBox.removeAttr("tabindex");
    }
  }


}
