import { CrypterService } from 'src/utils/service/Crypter.service';
import { DataService } from './../../service/data.service';
import { OverlayPanel } from 'primeng/overlaypanel';
import { DataTableOption, AppDataTableComponent } from 'src/utils/controls/appDataTable/appDataTable.component';
import { Component, OnInit, ElementRef, ViewChild, KeyValueDiffers, KeyValueDiffer, SimpleChanges, ChangeDetectorRef, EventEmitter } from '@angular/core';
import { DataTableColumn } from '../appDataTable/appDataTable.component';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseAppControl, BaseAppControlOption } from '../_base/baseAppControl';
import { ICellEvent } from '../class_helper';
import { NULL_EXPR } from '@angular/compiler/src/output/output_ast';
import * as $ from 'jquery';
import { KeyCode } from '@ng-select/ng-select/lib/ng-select.types';

@Component({
  selector: "app-pick-list",
  templateUrl: "./appPickListBox.component.html",
  styleUrls: ["./appPickListBox.component.less"],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: AppPickListBoxComponent,
      multi: true
    },
    {
      provide: BaseAppControl,
      useExisting : AppPickListBoxComponent
    }
  ]
})
export class AppPickListBoxComponent extends BaseAppControl<any> {

  @ViewChild("op")
  op: OverlayPanel;

  @ViewChild('displayInput')
  displayInput: ElementRef;

  @ViewChild('dataTable')
  grid: AppDataTableComponent;

  gridOption: DataTableOption;
  private differ: KeyValueDiffer<string, any>;

  propertyKey?:string= "ID";
  displayKey?: string = "Name";

  itemSelected: any = null;
  showClearButton: boolean = true;

  constructor(
    el: ElementRef,
    differs: KeyValueDiffers,
    private changeDetector: ChangeDetectorRef,
    private ds: DataService,
    private crypterSvc: CrypterService
  ) {
    super(el, changeDetector);
    this.differ = differs.find({}).create();
  }

  ngOnChanges(ch: SimpleChanges): void {
    super.ngOnChanges(ch);
    //if (ch.option.firstChange == true) {
      var opt = ch.option.currentValue as IAppPickListOption;
      if ( opt == undefined ) return;
      this.propertyKey = opt.propertyKey;
      this.displayKey = opt.displayKey;
    //}
  }

  async writeValue(val: any) { 
    console.log("Write Value " + this.name, val);
    if (this.itemSelected == null) {
      var itemSelected = null;
      if (val) {
        itemSelected = await this.loadObjectValue(val);
      }
      this.itemSelected = itemSelected;
    } 
    this.value = val;
  }

  timer = null;
  onTextBox_ValueChange(event): void { 
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      if ( this.grid ) {
        this.grid.reloadData(true, this.value);
      }
    }, 800);    
  }

  focus() : void { 
    if ( this.itemSelected != null ) {
      $(this.displayInput.nativeElement).focus();
      //$(this.displayInput.nativeElement).blur(this.onBlur);
    } else {
      super.focus();
    }
  }

  onBlur($event: any): void {}

  onFocus($event: any): void {
    super.onFocus($event);
    this.op.show($event);
  }

  onShow($event): void {
    //this.gridOption.autoLoad = true;
    setTimeout(() => {
      this.gridOption.autoLoad = true
      this.grid.reload(true);
    }, 500);
  }
  
  onHide($event: any): void {}

  ngAfterContentChecked() : void {
    this.changeDetector.detectChanges();
  }

  handleEnteryKey: boolean = false;

  onKeyDown(evt:KeyboardEvent): void {
    super.onKeyDown(evt);

    if ( evt.keyCode == 8 ) { //Backspace
      this.clearItemSelected();
      return;
    }

    // if ( evt.keyCode == 27 ) {
    //   this.op.hide();
    //   $(this.displayInput.nativeElement).focus();
    //   return;
    // }
    //evt.keyCode == 13 
    if ( evt.keyCode == 40 || evt.keyCode == 38  ) {
      if ( this.op.overlayVisible == false ) {
        this.op.show(evt);
        return;
      } 
    }
    // if ( this.itemSelected ) {
    //   if (evt.keyCode !== 13) {
    //     evt.stopPropagation();
    //     evt.preventDefault();
    //   }
    //return;
    //}
    if (evt.keyCode == 13) {
      if (this.itemSelected != null) {
        if (this.handleEnteryKey == true) return;
      }
    }

    if ( this.op.overlayVisible == false ) {
      if ( evt.keyCode == 13 ){
        return;
      }
      return;
    }; 
    this.grid.onKeyDown(evt);
  }

  ngAfterViewInit(): void {
    var opt = this.option as IAppPickListOption;
    this.propertyKey = opt.propertyKey;
    this.displayKey = opt.displayKey;
    this.gridOption = {
      apiUrl: opt.apiUrl,
      columns: opt.columns,
      sourceKey: opt.sourceKey,
      propertyKey : opt.propertyKey,
      Oid_ListView: opt.Oid_ListView,
  	  autoLoad: false,
      Oid_Model : opt.Oid_Model,
      onCell_DoubleClick : new EventEmitter<ICellEvent>(),
      onCell_EnterKey : new EventEmitter<ICellEvent>()
    };

    if ( this.gridOption ) {
        this.gridOption.onCell_DoubleClick.subscribe((evt: ICellEvent) => {
          this.onItem_Selected(evt);

        });
        this.gridOption.onCell_EnterKey.subscribe((evt: ICellEvent) => {
          this.onItem_Selected(evt);
        });
    }
    this.changeDetector.detectChanges();
  }

  onItem_Selected(cell: ICellEvent) : void  {
    this.itemSelected = cell.key;
    this.value = this.itemSelected[this.propertyKey];
    this.emitValueChange(this.value);
    this.onControl_ValueChanged();
    this.op.hide();
    setTimeout(() => {
      $(this.displayInput.nativeElement).focus();
    }, 100);
  }

  clearItemSelected() : void {
    this.itemSelected = null;
    this.emitValueChange(null);
    this.value = null;
    this.isDirty = true;
    this.onControl_ValueChanged();
    this.grid.clearSelection();
    setTimeout(() => {
      this.focus();
    }, 500);
  }

  getDisplayText(): string {
    if ( this.itemSelected == null ) return "";
    return this.itemSelected[this.displayKey];
  }

  async loadObjectValue(val: string): Promise<any> {
    var opt = this.option as IAppPickListOption;
    // if (opt.Oid_ListView == null || opt.Oid_ListView == undefined) opt.Oid_ListView = "xxxxxxx"
    var apiUrl = `Model/GetList/${opt.Oid_ListView}`;
    var result = await this.ds.GetPagingList(apiUrl, {
      SQL: opt.sourceKey,
      Value: val + "",
    });
    return (result.data as any[])[0];
  }

}

export class IAppPickListOption extends BaseAppControlOption {
  columns?: DataTableColumn[] | any[];
  apiUrl?: string;
  Oid_ListView?: string;
  sourceKey?:string;
  Oid_Model?:string;
  propertyKey?:string= "ID";
  displayKey?: string = "Name";
}
