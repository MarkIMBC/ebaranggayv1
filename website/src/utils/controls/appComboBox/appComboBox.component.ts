import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { BaseAppControl, BaseAppControlOption } from '../_base/baseAppControl';
import { NgbDropdown, NgbDropdownConfig, NgbDropdownMenu } from '@ng-bootstrap/ng-bootstrap';
import { AppDataGridComponent } from '../appDataGrid/appDataGrid.component';
import { Guid } from 'guid-typescript';
import * as $ from 'jquery';

@Component({
  selector: 'app-combo-box',
  templateUrl: './appComboBox.component.html',
  styleUrls: ['./appComboBox.component.less'],
  providers: [NgbDropdownConfig, {
    provide: BaseAppControl, useExisting: AppComboBoxComponent
  }]
})
export class AppComboBoxComponent extends BaseAppControl<number> {

  constructor(el: ElementRef, config: NgbDropdownConfig, ch :ChangeDetectorRef) {
    super(el, ch);
    config.container = "body";
    config.autoClose = false;
  }

  dropdownId: string = "id" + Guid.create().toString().substring(0, 8);

  @ViewChild('dropDown')
  private dropDown: NgbDropdown;

  @ViewChild('dataGrid')
  private dataGrid: AppDataGridComponent;

  private isGridInit: boolean = false;

  toggleDropdown($event?: Event): void {
    if ($event) $event.stopPropagation();
    this.dropDown.open();
    if (this.dropDown.isOpen() == false) return;
    this.open();
    //alert('xxxx')
  }

  open(): void {
    var dpElement = $(`body #${this.dropdownId}`);
    var dropDownEl = dpElement.parent().parent();
    dropDownEl.css({ "z-index": 5000 });
    var width = Math.floor($(this.input.nativeElement).width() * 1.55);
    dpElement.css({ "width": width });
    setTimeout(() => {
      this.dataGrid.updateDimension();
    });
    // $(document).mouseup()
    // $(document).mouseup((e: JQuery.MouseUpEvent) => {
    //   var container = dropDownEl;
    //   if ($(this.dataGrid.element.nativeElement).has(e.target).length > 0) return;
    //   if ($(this.el.nativeElement).has(e.target).length > 0) return;
    //   if (!container.is(e.target) && container.has(e.target).length === 0) {
    //     this.dropDown.toggle();
    //   }
    // });
  }

  onFocus($evt: Event): void {
    super.onFocus($evt);
    this.dropDown.open();
    this.open();
  }

  ngAfterViewInit(): void {
    // this.dropDown.container = "body";
    this.dropDown.autoClose = "outside";
    // $(this.dataGrid.element.nativeElement).click((evt: Event) => {
    //   evt.stopImmediatePropagation();
    //   evt.stopPropagation();
    //   evt.preventDefault();
    // });
  }
}

export class IAppComboBoxOption extends BaseAppControlOption {

}