import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { Component, Input, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { BaseAppControl, BaseAppControlOption } from '..';
import { InputTextarea } from 'primeng/inputtextarea';
import * as $ from "jquery";
import { NgxQrcodeElementTypes, NgxQrcodeErrorCorrectionLevels } from '@techiediaries/ngx-qrcode';

@Component({
  selector: 'app-qrcode-image',
  templateUrl: './appQRCodeImage.component.html',
  styleUrls: ['./appQRCodeImage.component.less'],
  providers: [
    {
      provide: BaseAppControl,
      useExisting: AppQRCodeImageComponent
    },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: AppQRCodeImageComponent,
      multi: true
    }
  ]
})
export class AppQRCodeImageComponent extends BaseAppControl<string> {
  

  @Input()
  option: BaseAppControlOption = null;

  elementType = NgxQrcodeElementTypes.URL;
  correctionLevel = NgxQrcodeErrorCorrectionLevels.HIGH;

  constructor(el: ElementRef, ch :ChangeDetectorRef) {
    super(el, ch);

  }

  ngAfterViewInit(): void {

  }
}

export class IAppTextAreaOption extends BaseAppControlOption {

}
