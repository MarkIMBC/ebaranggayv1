import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CSSDropdownComponent } from './cssdropdown.component';

describe('CSSDropdownComponent', () => {
  let component: CSSDropdownComponent;
  let fixture: ComponentFixture<CSSDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CSSDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CSSDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
