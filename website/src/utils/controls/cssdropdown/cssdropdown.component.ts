import { Component, OnInit, Input } from "@angular/core";
import { MenuItem } from "primeng/api";

@Component({
  selector: "cssdropdown",
  templateUrl: "./cssdropdown.component.html",
  styleUrls: ["./cssdropdown.component.less"],
})
export class CSSDropdownComponent implements OnInit {
  @Input()
  label: string = "";

  @Input()
  icon: string = "";

  @Input()
  model: MenuItem[] = [];

  constructor() {}

  ngOnInit(): void {}

  IsVisible: boolean = false;

  onClick() {
    this.IsVisible = !this.IsVisible;
  }

  menu_onClick(menuitem: MenuItem) {

    menuitem.command();
  }

  focusOutFunction() {
    var hide = () => {
      this.IsVisible = false;
    };

    setTimeout(function () {
      hide();
    }, 150);
  }
}
