import { FilterCriteriaType } from './../appControlContainer/appControlContainer.component';
import { DEBUG, IMenuItem } from './../class_helper';
import { MessageBoxService } from './../appModal/appModal.component';
import { Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { isNullOrUndefined } from 'util';
import { Calendar } from 'primeng/calendar';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { BaseAppControl, BaseAppControlOption } from '../_base/baseAppControl';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as $ from "jquery";

@Component({
  selector: 'app-date-box',
  templateUrl: './appDateBox.component.html',
  styleUrls: ['./appDateBox.component.less'],
  //changeDetection: ChangeDetectionStrategy.Default,
  providers: [
   
    {
      provide: BaseAppControl,
      useExisting: AppDateBoxComponent,
    }
    ,
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: AppDateBoxComponent,
      multi: true
    },
  ]
})
export class AppDateBoxComponent extends BaseAppControl<Date> {

  @Input()
  maxDate: Date = null;

  @Input() 
  minDate: Date = null;

  constructor(el: ElementRef, ch: ChangeDetectorRef) {
    super(el, ch);

  }

  filterCriteriaItems: IMenuItem[] = [
    {
      id: FilterCriteriaType.Between,
      label: 'Between'
    },
    {
      id: FilterCriteriaType.Equal,
      label: 'Equals'
    },
    {
      id: FilterCriteriaType.NotEqual,
      label: 'Not Equals'
    },
    {
      id: FilterCriteriaType.GreaterThan,
      label: 'Greater than'
    },
    {
      id: FilterCriteriaType.LessThan,
      label: 'Less than'
    },
    {
      id: FilterCriteriaType.GreaterThanEqual,
      label: 'Greater than Equal'
    },
    {
      id: FilterCriteriaType.LessThanEqual,
      label: 'Less than Equal'
    },
    {
      id: FilterCriteriaType.Contains,
      label: "Contains"
    },{
      id: FilterCriteriaType.NotContains,
      label: "Not Contains"
    }];

  onFilterCriteria_Changed(item: IMenuItem): void {
    if ( this.filterMode !== true ) return;
    this.value = null;
    if ( item.id == FilterCriteriaType.Between ) {
      this.calendar.selectionMode = "range";
      // this.calendar.readonlyInput = true;
      this.calendar.numberOfMonths = 2;
    } else if ( item.id == FilterCriteriaType.Contains || item.id == FilterCriteriaType.NotContains ) {
      this.calendar.selectionMode = "multiple"
      // this.calendar.readonlyInput = true;
      this.calendar.numberOfMonths = 2;
    } else {
      this.calendar.numberOfMonths = 1;
      this.calendar.selectionMode = "single";
      this.calendar.readonlyInput = false;
    }
  }

  @ViewChild('calendar')
  calendar: Calendar;

  @Input('show-time')
  showTime: boolean = true;

  writeValue(_value: any): void {
    var DateValue = null;
    if (_value) {
      var val = new Date(_value);
      if (val.toString() !== "Invalid Date") {
        DateValue = val;
      }
    };
    this.ch.detectChanges();
    this.value = DateValue;
  }

  onCalendar_ValueChange(value: any): void {

    this.emitValueChange(value);
    this.onControl_ValueChanged();
  }

  isDisabled(): boolean {
    if (this.readOnly == true) return true;
    return this.disabled;
  }

  ngAfterViewInit(): void {

  }

  onInputCalendarBlur(event): void {
    if (isNullOrUndefined(this.value) == true) {
      this.calendar.inputFieldValue = null;
    }
  }

  IconClick(): void {
    this.calendar.toggle();
  }

}
