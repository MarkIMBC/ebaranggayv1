import { Component, OnInit, ElementRef, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { BaseAppControl, BaseAppControlOption } from '../_base/baseAppControl';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as $ from 'jquery';
import { IMenuItem } from '../class_helper';
import { FilterCriteriaType } from '../appControlContainer/appControlContainer.component';

@Component({
  selector: 'app-number-box',
  templateUrl: './appNumberBox.component.html',
  styleUrls: ['./appNumberBox.component.less'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: AppNumberBoxComponent,
      multi: true
    },
    {
      provide: BaseAppControl,
      useExisting: AppNumberBoxComponent
    }
  ]
})
export class AppNumberBoxComponent extends BaseAppControl<number> {

  @Output() onChange = new EventEmitter<number>();

  filterCriteriaItems: IMenuItem[] = [
    {
      id: FilterCriteriaType.Equal,
      label: 'Equals'
    },
    {
      id: FilterCriteriaType.NotEqual,
      label: 'Not Equals'
    },
    {
      id: FilterCriteriaType.GreaterThan,
      label: 'Greater than'
    },
    {
      id: FilterCriteriaType.LessThan,
      label: 'Less than'
    },
    {
      id: FilterCriteriaType.GreaterThanEqual,
      label: 'Greater than Equal'
    },
    {
      id: FilterCriteriaType.LessThanEqual,
      label: 'Less than Equal'
    }
  ];

  constructor(el: ElementRef,   ch :ChangeDetectorRef) {
    super(el, ch);
  }

  focus(): void {
    $(this.input.nativeElement).focus();
  }

  onNumberBox_ValueChange(_value: any): void {
    
    if ( Number.parseFloat(this.value + "") !== Number.parseFloat(_value) ) {
      this.onControl_ValueChanged()
    };
    this.emitValueChange(Number.parseFloat(_value));

    this.onChange.emit(_value);
  }

}
