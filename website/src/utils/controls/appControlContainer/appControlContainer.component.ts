import { EventEmitter } from '@angular/core';
import { SplitButtonModule, SplitButton } from 'primeng/splitbutton';
import { SimpleChanges, ViewChild, Output } from '@angular/core';
import { IMenuItem } from './../class_helper';
import { Component, OnInit, Input, ContentChild, QueryList, ViewChildren, ContentChildren } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { View } from 'src/bo/APP_MODELS';

@Component({
  selector: 'app-control',
  templateUrl: './appControlContainer.component.html',
  styleUrls: ['./appControlContainer.component.less']
})
export class AppControlContainerComponent implements OnInit {

  toolTipMessage: string = "This is required."
  disabledTooltip: boolean = true;

  filterMode: boolean = false;

  hideRequiredMessage: boolean = true;

  @Input()
  label?: string;

  @Input('cssClass')
  cssClass: any = null;

  @Input()
  clearButton: boolean = false;

  @Input()
  labelPosition: LabelPositionEnum = LabelPositionEnum.left;

  @Input()
  filterItems: IMenuItem[] = [];

  @ViewChild('filterCriteriaButton')
  filterCriteriaButton: SplitButton;

  filterCriteria:IMenuItem = null;
  
  showLabel: boolean = true;

  @Output()
  onFilterCriteria_Changed = new EventEmitter<IMenuItem>();

  constructor(private http: HttpClient) { }

  ngOnInit() { }

  getClass(): any {
    if (this.cssClass != null) {
      return this.cssClass;
    }
    return {};
  }

  getLabelPositionClass(): any {
    
  }

  onClearButtonClick(): void { }

  ngOnChanges(ch: SimpleChanges): void {

  }

  ngAfterViewInit(): void {
    if (this.filterItems.length > 0) {
      this.filterItems.push({
        id: FilterCriteriaType.Exclude,
        label : "Exclude"
      })
      this.filterItems.forEach(f => {
        switch (f.id) {
          case FilterCriteriaType.Equal:// = '1',
            f.icon = 'mdi mdi-equal'
            break;
          case FilterCriteriaType.NotEqual:// = '2',
            f.icon = 'mdi mdi-not-equal-variant'
            break;
          case FilterCriteriaType.Contains:// = '3',
            f.icon = 'mdi mdi-contain'
            break;
          case FilterCriteriaType.NotContains:// = '4',
            f.icon = 'mdi mdi-not-equal'
            break;
          case FilterCriteriaType.Like:// = '5',
            f.icon = 'mdi mdi-not-alphabetical'
            break;
          case FilterCriteriaType.NotLike:// = '6',
            f.icon = 'mdi mdi-not-equal-variant'
            break;
          case FilterCriteriaType.StartWith:// = '7',
            f.icon = 'mdi mdi-contain-start'
            break;
          case FilterCriteriaType.EndWith:// = '8',
            f.icon = 'mdi mdi-contain-end'
            break;
          case FilterCriteriaType.GreaterThan:// = '9',
            f.icon = 'mdi mdi-greater-than'
            break;
          case FilterCriteriaType.LessThan:// = '10'
            f.icon = 'mdi mdi-less-than'
            break;
        }
        f.command = () => {
          this.onFilterCriteria_Changed.emit(f);
          this.filterCriteria = f;
        }
      });
      this.filterCriteria = this.filterItems[0];
      //alert('--->' + this.label);
      setTimeout(() => {
      this.onFilterCriteria_Changed.emit(this.filterCriteria);
      }, 500);
      //this.filterCriteriaButton.icon = this.filterItems[0].icon;
    }
  }



}

export enum LabelPositionEnum {
  left = 'left', top = 'top', right = 'right', bottom = 'bottom'
}

export enum LabelAlignment {
  top = 'top', middle = 'middle', bottom = 'bottom'
}

export enum FilterCriteriaType {
  Exclude = '0',
  Equal = '1',
  NotEqual = '2',
  Contains = '3',
  NotContains = '4',
  Like = '5',
  NotLike = '6',
  StartWith = '7',
  EndWith = '8',
  GreaterThan = '9',
  LessThan = '10',
  GreaterThanEqual = '11',
  LessThanEqual = '12',
  Between = '13'
}
