import { Component, OnInit, ElementRef, ChangeDetectorRef, SimpleChanges } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseAppControl, BaseAppControlOption } from '../_base/baseAppControl';
import { DataService } from 'src/utils/service/data.service';
import * as TS from 'linq-typescript';
import { IMenuItem } from '../class_helper';
import { FilterCriteriaType } from '../appControlContainer/appControlContainer.component';

@Component({
  selector: 'app-multipleselect-box',
  templateUrl: './appMultipleSelectBox.component.html',
  styleUrls: ['./appMultipleSelectBox.component.less'],
  providers: [
    {
      provide: BaseAppControl,
      useExisting: AppMultipleSelectBoxComponent,
    },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: AppMultipleSelectBoxComponent,
      multi: true
    }
  ]
})
export class AppMultipleSelectBoxComponent extends BaseAppControl<number> {

  constructor(el: ElementRef, private ds: DataService, private changeDetector: ChangeDetectorRef) {
    super(el, changeDetector);
  }

  selecteds: any[] = [];
  displayKey?: string = "Name";
  propertyKey?: string = "ID";
  sourceKey?:string = null;
  items: any[] = [];
  showToggleAll?: boolean = true;
  multiple?: boolean = true;
  checkbox?: boolean = true;


   filterCriteriaItems: IMenuItem[] = [
    // {
    //   id: FilterCriteriaType.Equal,
    //   label: 'Equals'
    // },
    // {
    //   id: FilterCriteriaType.NotEqual,
    //   label: 'Not Equals'
    // },
    {
      id: FilterCriteriaType.Contains,
      label: 'Contains'
    },
    {
      id: FilterCriteriaType.NotContains,
      label: 'Not Contains'
    },
    // {
    //   id: FilterCriteriaType.StartWith,
    //   label: 'Start With'
    // },
    // {
    //   id: FilterCriteriaType.EndWith,
    //   label: 'End With'
    // }
  ];

  ngOnInit() {
  }

  async writeValue(val: any[]) {
    if (val) {
      if (this.items.length == 0) {
        this.items = await this.getListSource();
      }
      val = new TS.List(this.items).where(d => val.includes(d => d[this.propertyKey] == true)).toArray();
    } else {
      val = [];
    }
    this.selecteds = val;
  }

  private async getListSource(): Promise<any[]> {
    var list = await this.ds.query(this.sourceKey);
    return list;
  }

  isInvalid(): boolean { 
    var required = this.isRequired();
    return this.isDirty == true && required == true && !(this.selecteds?.length > 0);
  }


  async ngOnChanges(ch: SimpleChanges): Promise<void> {
    super.ngOnChanges(ch);
    if (ch.option) {
      var option = ch.option.currentValue as IAppMultipleSelectBoxOption;
      if (option == undefined) return;
      var items = option.items;
      this.propertyKey = option.propertyKey ? this.propertyKey : "ID";
      this.displayKey = option.displayKey ? this.displayKey : "Name";
      if (items) {
        this.items = items;
      }
      if (this.items.length == 0) {
        if (option.sourceKey) {
          this.sourceKey = option.sourceKey;
          var list = await this.ds.query(option.sourceKey);
          this.items = list;
        };
      }
    }
  }

  onNgModelValue_Changed(evt: any[]): void {
    if (evt?.length > 0) {
      var values = new TS.List(evt).select(d => d[this.propertyKey]).toArray();
      this.emitValueChange(values);
    } else {
      this.emitValueChange(evt);
    }
    this.onControl_ValueChanged();
  }

}



export class IAppMultipleSelectBoxOption extends BaseAppControlOption {
  items?: any[] = [];
  propertyKey?: string = "ID";
  displayKey?: string = "Name";
  sourceKey?: string = null;
}
