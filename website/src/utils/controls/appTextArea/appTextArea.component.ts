import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { Component, Input, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { BaseAppControl, BaseAppControlOption } from '..';
import { InputTextarea } from 'primeng/inputtextarea';
import * as $ from "jquery";

@Component({
  selector: 'app-text-area',
  templateUrl: './appTextArea.component.html',
  styleUrls: ['./appTextArea.component.less'],
  providers: [
    {
      provide: BaseAppControl,
      useExisting: AppTextAreaComponent
    },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: AppTextAreaComponent,
      multi: true
    }
  ]
})
export class AppTextAreaComponent extends BaseAppControl<string> {

  @Input()
  option: BaseAppControlOption = null;

  @Input()
  height: number = 150;

  @ViewChild('textArea')
  textArea: InputTextarea;

  constructor(el: ElementRef, ch :ChangeDetectorRef) {
    super(el, ch);

  }

  onTextArea_ValueChange(_value: any): void {
    this.onControl_ValueChanged();
    this.emitValueChange(_value);
  };

  ngAfterViewInit(): void {

  }
}

export class IAppTextAreaOption extends BaseAppControlOption {

}
