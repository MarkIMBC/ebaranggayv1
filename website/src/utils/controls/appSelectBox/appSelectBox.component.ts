import { OverlayPanel } from 'primeng/overlaypanel';
import { KeyCode } from '@ng-select/ng-select/lib/ng-select.types';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { DataService } from './../../service/data.service';
import { AppListBoxComponent } from './../appListBox/appListBox.component';
import { Component, OnInit, ViewChild, ElementRef, SimpleChanges, ChangeDetectorRef, EventEmitter, Output } from '@angular/core';
import { BaseAppControl, BaseAppControlOption, IValueChanged } from '../_base/baseAppControl';
import { Guid } from 'guid-typescript';
import { NgbDropdown, NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { Dropdown } from 'primeng/dropdown';
import { DEBUG } from '../class_helper';
import { AutoComplete } from 'primeng/autocomplete';
import * as $ from 'jquery';

@Component({
  selector: "app-select-box",
  templateUrl: "./appSelectBox.component.html",
  styleUrls: ["./appSelectBox.component.less"],
  providers:[
    {
      provide: BaseAppControl,
      useExisting: AppSelectBoxComponent,
    },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: AppSelectBoxComponent,
      multi: true
    }
  ]
})
export class AppSelectBoxComponent extends BaseAppControl<number> {

  displayKey?: string = "Name";
  propertyKey?: string = "ID";
  sourceKey?: string = null;
  items: any[] = [];
  itemSelected : any = null;

  @ViewChild('dropDown')
  dropdown: Dropdown

  @Output() onValueChanged = new EventEmitter<any>();

  constructor(el: ElementRef, private ds: DataService, private changeDetector: ChangeDetectorRef) {
    super(el, changeDetector);
    $(el.nativeElement).keydown((evt) => {
     if ( evt.keyCode == 8 ) {
       this.itemSelected = null;
       this.value = null;
     }
    });
  }

  focus(): void {
    this.dropdown.focus();
  }

  async writeValue(val: any): Promise<void> { 
    var _value = null;
    if ( val ) {
      if ( this.items.length == 0 ) {
        this.items = await this.getListSource();
      }
      var item = this.items.find(d=>d[this.propertyKey] == val);
      if ( item ) {
        _value = item;
      }
    } 
    this.value = val;
    this.itemSelected = _value;
  }

  onDropdown_ValueChange(value: any): void {
    this.value = value ? value[this.propertyKey] : null;
    this.itemSelected = value;
    if ( value == null ) {
      setTimeout(() => {
        this.focus();
      }, 100);
    };
    this.emitValueChange(this.value);
    this.onControl_ValueChanged();

    this.onValueChanged.emit(this.itemSelected);
  }


  ngOnChanges(ch: SimpleChanges): void {
    super.ngOnChanges(ch);
    if (ch.option ) {
      var option = ch.option.currentValue as IAppSelectBoxOption;
      if ( option == undefined ) return;
      var items = option.items;
      this.propertyKey = option.propertyKey ? this.propertyKey : "ID";
      this.displayKey = option.displayKey ? this.displayKey : "Name";
      // if ( this.displayKey ) {
      //   this.dropdown.optionLabel = this.displayKey;
      // }
      if ( items ) {
        this.items = items;
      }

      if ( this.items.length == 0 ) {
        if ( option.sourceKey ) {
          this.sourceKey = option.sourceKey;
        };
      }
    }
  }

  ngAfterViewInit(): void {
    this.dropdown.optionLabel = this.displayKey;
    this.dropdown.dataKey = this.propertyKey;
    //alert('xxxxx');
    this.changeDetector.detectChanges();
    this.dropdown.emptyFilterMessage = "";
    // this.dropdown.onItemClick = (evt) => { 
    //   setTimeout(() => {
    //     this.focus();
    //   }, 100);
    // };
    this.dropdown.onShow.subscribe(async () => {
      this.dropdown.overlay.onkeydown = (evt) => {
        if (evt.keyCode == 27 || evt.keyCode == 13) { //Escape
          setTimeout(() => {
              this.focus();
          }, 100);
        }
      }
      if (this.items.length > 0) {
        this.dropdown.options = this.items;
        return;
      };
      if ( this.sourceKey == null ) return;
      var items = await this.getListSource();
      this.dropdown.options = items;
    });
  }

  private async getListSource(): Promise<any[]> { 
    var list = await  this.ds.query(this.sourceKey); 
    return list;
  }

}

export class IAppSelectBoxOption extends BaseAppControlOption {
  items?: any[] = [];
  propertyKey?: string = "ID";
  displayKey?: string = "Name";
  sourceKey?: string = null;
}
