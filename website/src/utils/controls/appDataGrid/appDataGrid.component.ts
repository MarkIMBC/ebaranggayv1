import { Component, ElementRef, ViewChild, Input,ViewContainerRef, ComponentFactoryResolver, SimpleChanges} from "@angular/core";
import { DomSanitizer, SafeStyle } from "@angular/platform-browser";
import {  BaseAppControlOption,  BaseAppControl, IEnterKey } from "../_base/baseAppControl";
import { HttpClient } from "@angular/common/http";
import * as $ from "jquery";
import * as TS from "linq-typescript";
import { GhostElementCreatedEvent, DragMoveEvent, DragEndEvent } from "angular-draggable-droppable";
import { Type } from "@angular/compiler";
import { AppTextBoxComponent } from "../appTextBox/appTextBox.component";
import { DoCheck, KeyValueDiffers, KeyValueDiffer, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: "app-data-grid",
  templateUrl: "./appDataGrid.component.html",
  styleUrls: ["./appDataGrid.component.less"],
  providers: [{
    provide: BaseAppControl, useExisting: AppDataGridComponent
  }]
})
export class AppDataGridComponent extends BaseAppControl<any[]> {
  newRowObject: any = {};
  items: any[] = [];
  columns: DataGridColumn[] = null;
  fixedLeftColumns: DataGridColumn[] = null;
  fixedRightColumns: DataGridColumn[] = null;
  isEditing: boolean = false;
  //
  //
  //
  backWrap: JQuery<EventTarget>;
  currentCell: JQuery<EventTarget>;
  colResizer: JQuery<EventTarget>;
  //currentRow: JQuery<EventTarget>;

  @ViewChild("holder")
  holder: ElementRef;

  @ViewChild("scrollViewBar")
  scrollViewBar: ElementRef;

  @Input()
  allowDelete: boolean = false;

  @Input()
  option?: DataGridOption;

  readonly checkBoxColWidth: number = 35;
  readonly deleteColWidth: number = 50;
  readonly scrollYWidth: number = 17;
  differ: KeyValueDiffer<string, any>;

  constructor(
    private http: HttpClient,
    public element: ElementRef,
    private sanitization: DomSanitizer,
    private viewContainerRef: ViewContainerRef,
    private componentFactoryResolver: ComponentFactoryResolver,
    private differs: KeyValueDiffers,
    ch :ChangeDetectorRef
  ) {
    super(element,ch);
    this.differ = this.differs.find({}).create();
  }

  //
  //
  //

  ngDoCheck(): void {
    const change = this.differ.diff(this.option);
    if (change) {
      change.forEachChangedItem(item => {
        if (item.key == "columns") {
          this.columns = item.currentValue;
          this.initColumns();
        }
      });
    }
  }

  ngOnChanges(ch: SimpleChanges): void {
    if (ch.option.firstChange == true) {
      this.option = ch.option.currentValue;
      this.columns = this.option.columns;
      this.updateDimension();
    }
  }

  reload(): void {
    var opt = this.option as DataGridOption;
    //if (!opt.apiUrl) throw Error(`${opt.name} api url cannot be null`);
    this.http
      .get("assets/mock.json")
      .toPromise()
      .then((d: any[]) => {
        console.log({ d });
        this.items = d;
        this.updateDimension();
      });
  }

  initColumns(): void {
    if (this.items != null && this.columns === null) {
      if (this.items.length == 0) return;
      var data = this.items[0];
      var keys = Object.keys(data);
      var cols: DataGridColumn[] = [];
      keys.forEach((key: string, index: number) => {
        var dataType = typeof data[key];
        var col: DataGridColumn = {
          dataField: key,
          caption: key,
          fixed: false
        };
        if ([0, 1].includes(index)) {
          col.fixed = true; //Sample Only
          if (index == 0) {
            col.width = 50;
          }
        }
        // if ([keys.length - 1, keys.length - 2].includes(index)) {
        //   col.fixed = true;
        //   col.fixedRight = true;
        //   col.width = 80;
        // }
        //console.log(col.dataField);
        if (index !== 0) col.editable = true;
        //console.log(dataType);
        switch (dataType) {
          case "string":
            col.dataType = DataTypeEnum.String;
            break;
          default:
            col.dataType = DataTypeEnum.String;
            break;
        }
        cols.push(col);
      });
      // for (var i = 0; i < 1; i++) {
      //   cols.push({
      //     dataField: "col" + i,
      //     width: 80,
      //     caption: "Sample",
      //     fixed: true,
      //     fixedRight: true
      //   });
      // }

      // for (var i = 0; i < 1; i++) {
      //   cols.push({
      //     dataField: "colxxx" + i,
      //     width: 80,
      //     caption: "Sample Col"
      //   });
      // }

      var colsWithoutWidths = cols.filter(c => c.width == null);
      var colWithWidths = cols.filter(c => c.width != null);
      var totalDefineWidths = new TS.List(colWithWidths).sum(
        d => d.width as number
      );
      var _colWidth: number = 0;
      if (colWithWidths.length > 0) {
        var parent = $(this.element.nativeElement).parent();
        var width = parent.width() - 17;
        if (width > 0) {
          if (width > totalDefineWidths) {
            _colWidth = Math.floor(
              (width - totalDefineWidths) / colsWithoutWidths.length
            );
          }
        } else {
          _colWidth = 150;
        }
      }

      if (colsWithoutWidths.length > 0) {
        colsWithoutWidths.forEach((c: DataGridColumn) => {
          // if (c.dataField == "ip_address") {
          //   console.log(c.dataField, c);
          // }
          // //c.width = this.sanitization.bypassSecurityTrustStyle("100"); //Sample
          // console.log(_colWidth);
          c.width = _colWidth; // this.sanitization.bypassSecurityTrustStyle(_colWidth);
        });
      }

      var fixedLefts = cols.filter(
        f => f.fixed == true && f.fixedRight !== true
      );
      var fixedRights = cols.filter(
        f => f.fixed == true && f.fixedRight === true
      );
      var seqNo: number = 0;
      fixedLefts.forEach((col: DataGridColumn) => {
        col.seqNo = seqNo++;
      });
      cols.forEach((col: DataGridColumn) => {
        if (col.fixed == true) return;
        col.seqNo = seqNo++;
      });
      fixedRights.forEach((col: DataGridColumn) => {
        col.seqNo = seqNo++;
      });
      console.log(seqNo, cols.length);
      this.columns = cols; //.filter(f => f.fixedRight !== true);
      this.fixedLeftColumns = fixedLefts;
      this.fixedRightColumns = fixedRights;
    }
    setTimeout(() => {
      this.updateDimension();
    });
  }

  updateDimension(): void {
    var parent = $(this.element.nativeElement).parent();
    var width = parent.width();
    var height = parent.height();
    var footerHeight = 30;
    var headerContent = $(this.element.nativeElement).find(
      ".table-header .table-container"
    );
    var backwrap = $(this.element.nativeElement).find(".backwrap");
    this.backWrap = backwrap;
    var verticallScroll = $(this.element.nativeElement).find(
      ".vertical-scroll"
    );
    var headerHeight = $(headerContent[0]).height();
    //headerContent.css({ position: "absolute", "padding-right": 17 });
    headerContent.css({
      position: "absolute"
    });
    var _bodyCss: any = {
      top: headerHeight + "px",
      position: "relative"
    };
    if (height) {
      //  _bodyCss.height = height - headerHeight + 20 + "px";
      _bodyCss.height = height - headerHeight;
    }

    verticallScroll.css({
      height: _bodyCss.height, // + headerHeight,
      width: "100%",
      "z-index": 0,
      position: "absolute",
      //top: headerHeight * -1
      top: 0
    });
    var body = $(this.element.nativeElement).find(".table-body");
    var footer = body.find(".table-footer");
    body.css(_bodyCss);
    var tableFixed = body.find(".body.table-container.fixed");
    var tableContent = body.find(".body.table-container.content");
    var bodyHeight = height - headerHeight + 5 - 1 - footerHeight - 20;
    var tbl = tableContent.find("table");
    //alert(tbl.height());
    backwrap.css({
      height: tbl.height(),
      width: tbl.width() + 100 //- 17
    });
    //var tableContentWidth = verticallScroll.width() - 17;
    var tableContentWidth = verticallScroll.width() - 17;
    tableContent.css({
      height: bodyHeight,
      width: tableContentWidth,
      overflow: "hidden"
    });
    tableFixed.css({
      height: bodyHeight,
      overflow: "hidden"
    });
    footer.css({
      top: tableFixed.height(),
      width: tableContentWidth
    });
    var footerTable = footer.find(".footer.table-container");
    footerTable.css({
      height: footerHeight + "px",
      width: "100%",
      overflow: "hidden"
    });
    footerTable.find("td").css({ height: "50" });
    var _headers = $(this.element.nativeElement).find(
      ".table-header .table-container"
    );
    var _scrollView = $(this.scrollViewBar.nativeElement);
    body.bind("mousewheel", function(e: any) {
      e.preventDefault();
      e.stopPropagation();
      if (e.originalEvent.wheelDelta / 120 > 0) {
        var scrollTop = _scrollView.scrollTop();
        _scrollView.scrollTop(scrollTop - 50);
      } else {
        var scrollTop = _scrollView.scrollTop();
        _scrollView.scrollTop(scrollTop + 50);
      }
    });
    _scrollView.scroll(() => {
      const scrollTop = _scrollView.scrollTop();
      const scrollLeft = _scrollView.scrollLeft();
      _headers.scrollLeft(scrollLeft);
      footerTable.scrollLeft(scrollLeft);
      tableFixed.scrollTop(scrollTop);
      tableContent.scrollTop(scrollTop);
      tableContent.scrollLeft(scrollLeft);
    });
    _scrollView.scrollTop(tbl.height());
    this.setCellHeightByRowClassName("filter-row");
    this.setCellHeightByRowClassName("edit-row");
    if (!this.colResizer) {
      this.colResizer = $(this.element.nativeElement).find(
        ".resize-column-indicator"
      );
    }
    this.colResizer.height(height);
  }

  private setCellHeightByRowClassName(
    className: string,
    newHeight?: number
  ): void {
    const cells: any[] = $(this.element.nativeElement)
      .find(`.${className} td`)
      .toArray();
    const rowCells = new TS.List(cells); //TS.List(cells);
    if (!newHeight) {
      const maxHeight = rowCells.max((d: any) => $(d).outerHeight());
      rowCells.toArray().forEach(cell => {
        $(cell).css({
          height: maxHeight
        });
      });
    } else {
      rowCells.toArray().forEach(cell => {
        $(cell).css({
          height: newHeight
        });
      });
    }
  }

  onControlEnterKey(opt: IEnterKey): void {
    let _newRowObject = JSON.parse(JSON.stringify(this.newRowObject));
    this.items.push(_newRowObject);
    this.newRowObject = {};
    var body = $(this.element.nativeElement).find(".table-body");
    var tableContent = body.find(".body.table-container.content");
    var tbl = tableContent.find("table");
    var newHeight = tbl.height();
    this.backWrap.css({
      height: newHeight,
      width: tbl.width()
    });
    $(this.scrollViewBar.nativeElement).scrollTop(newHeight);
  }

  ngAfterViewInit(): void {
    //this.reload();
  }

  onKeyDown(evt: KeyboardEvent): void {
    if (evt.keyCode === 27) {
      //EscapeKey
      if (this.currentCell) this.currentCell.removeClass("jf-focused");
      $(this.element.nativeElement)
        .find("tr.jf-focused")
        .removeClass("jf-focused");
      this.currentCell = null;
      // if (this.currentRow) this.currentRow.removeClass("jf-focused");
      // this.currentRow = null;
    }
    if ((evt.keyCode >= 37 && evt.keyCode <= 40) || evt.keyCode === 13) {
      if (this.currentCell != null) {
        var classes = this.currentCell.attr("class").split(" ");
        var cellClass = new TS.List(classes)
          .where((d: string) => d.includes("rc-"))
          .toArray();
        if (cellClass.length > 0) {
          var rc = cellClass[0].replace(/rc-/g, "").split("-");
          var row = parseInt(rc[0]);
          var col = parseInt(rc[1]);

          switch (evt.keyCode) {
            case 37: //left
              if (col == 0) return;
              col--;
              break;
            case 38: //up
              if (row != 0) {
                $(this.element.nativeElement)
                  .find("tr.jf-focused")
                  .removeClass("jf-focused");
                row--;
                var rows = $(this.element.nativeElement).find(".r-" + row);
                rows.addClass("jf-focused");
              }
              break;
            case 39: //right
              if (col == this.columns.length - 1) return;
              col++;
              break;
            case 40: //down
              if (row !== this.items.length - 1) {
                $(this.element.nativeElement)
                  .find("tr.jf-focused")
                  .removeClass("jf-focused");
                row++;
                var rows = $(this.element.nativeElement).find(".r-" + row);
                rows.addClass("jf-focused");
              }
              break;
          }
          this.currentCell.removeClass("jf-focused");
          var container = $(this.element.nativeElement);
          var nextCell = container.find(`.rc-${row}-${col}`);
          this.currentCell = $(nextCell[0]);
          this.currentCell.addClass("jf-focused");
          var scrollView = $(this.scrollViewBar.nativeElement);

          switch (evt.keyCode) {
            case 38:
            case 40:
              var top = scrollView.offset().top;
              var bottom = scrollView.offset().top + scrollView.outerHeight();
              var cellTop = this.currentCell.offset().top;
              if (evt.keyCode == 38) {
                if (!(cellTop >= top && cellTop <= bottom)) {
                  var scrollTop = scrollView.scrollTop() - 100;
                  scrollView.scrollTop(scrollTop);
                }
              } else if (evt.keyCode == 40) {
                if (row == this.items.length - 1) {
                  scrollView.scrollTop(this.backWrap.outerHeight());
                  return;
                }
                var cellBottom = cellTop + this.currentCell.outerHeight();
                if (!(cellBottom >= top && cellBottom <= bottom)) {
                  var scrollTop = scrollView.scrollTop() + 100;
                  scrollView.scrollTop(scrollTop);
                }
              }
              break;
            case 37:
            case 39:
              var left = scrollView.offset().left;
              var right = scrollView.offset().left + scrollView.outerWidth();
              var tableFixedRightWidth =
                container.find(".table-container.fixed-right").width() + 17;
              var tableFixedLeftWidth = container
                .find(".table-container.fixed-left")
                .width();
              var boundaryLeft = left + tableFixedLeftWidth;
              var boundaryRight = right - tableFixedRightWidth;
              var column = this.columns[col];
              if (evt.keyCode == 39) {
                if (column.fixed != true) {
                  var cellRight =
                    this.currentCell.offset().left +
                    this.currentCell.outerWidth();
                  if (cellRight > boundaryRight) {
                    scrollView.scrollLeft(
                      scrollView.scrollLeft() + this.currentCell.outerWidth()
                    );
                  }
                }
              } else if (evt.keyCode == 37) {
                if (column.fixed != true) {
                  var cellLeft = this.currentCell.offset().left;
                  if (cellLeft < boundaryLeft) {
                    scrollView.scrollLeft(
                      scrollView.scrollLeft() - this.currentCell.outerWidth()
                    );
                  }
                }
              }

              break;
            //
            //
            //
            case 13: //EnterKey
              this.initCellEditing(col, row);
              break;
          }
        }
      }
    }
    super.onKeyDown(evt);
  }

  initCellEditing(col: number, row: number): void {
    var dataGridColumn = this.columns[col];
    var dataRow = this.items[row];
    var allowEdit = (allowEdit = dataGridColumn.allowEdit
      ? dataGridColumn.allowEdit instanceof Function
        ? dataGridColumn.allowEdit(dataRow)
        : dataGridColumn.allowEdit
      : true);
    if (allowEdit === false) return;
    var type: EditorTypeEnum = EditorTypeEnum.TextBox;
    if (!dataGridColumn.editorType) {
      switch (dataGridColumn.dataType) {
        case DataTypeEnum.String:
          type = EditorTypeEnum.TextBox;
          break;
        case DataTypeEnum.Integer:
          type = EditorTypeEnum.NumberBox;
          break;
        case DataTypeEnum.Decimal:
          type = EditorTypeEnum.NumberBox;
          break;
        case DataTypeEnum.Date:
          type = EditorTypeEnum.DateBox;
          break;
        case DataTypeEnum.DateTime:
          type = EditorTypeEnum.DateBox;
          break;
        case DataTypeEnum.Boolean:
          type = EditorTypeEnum.CheckBox;
          break;
      }
    } else {
      type = dataGridColumn.editorType;
    }
    //
    //
    //
    var controlType: any = null;
    switch (type) {
      case EditorTypeEnum.TextBox:
        controlType = AppTextBoxComponent;
        break;
    }
    if (!controlType) return;
    //this.viewContainerRef.clear();
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
      controlType
    );
    var componentInstance = this.viewContainerRef.createComponent(
      componentFactory
    );
    var currentValue = this.items[row][dataGridColumn.dataField];
    var baseAppControl = componentInstance.instance as BaseAppControl<any>;
    baseAppControl.name = "";
    baseAppControl.value = currentValue;
    baseAppControl.showLabel = false;
    // var _html = $(this.currentCell.html());
    // _html.addClass("hide");
    //console.log(_html);
    this.currentCell.toggleClass("edit-cell");
    var lastRowHeight = $(this.element.nativeElement)
      .find(".r-" + row)
      .height();
    this.isEditing = true;
    setTimeout(() => {
      $(componentInstance.location.nativeElement).keydown((evt: any) => {
        evt.stopPropagation();
        var keyCode: number = evt.keyCode;
        switch (keyCode) {
          case 13: //Enter
            //this.currentCell.html(_html);
            this.items[row][dataGridColumn.dataField] = baseAppControl.value;
            this.viewContainerRef.clear();
            this.setCellHeightByRowClassName("r-" + row, lastRowHeight);
            this.focus();
            this.currentCell.toggleClass("edit-cell");
            // _html.css({ "display": "block" });
            break;
          case 27: // Escape
            break;
          case 9: //Tab
            break;
        }
      });
      this.currentCell.append(componentInstance.location.nativeElement);
      baseAppControl.focus();
      this.setCellHeightByRowClassName("r-" + row);
    });
  }

  onSelectColumnClick($evt: MouseEvent, item: any, rowIndex: number): void {}

  onCellClick(
    evt: MouseEvent,
    dataGridColumn: DataGridColumn,
    colIndex: number
  ): void {
    if (this.currentCell) this.currentCell.removeClass("jf-focused");
    this.currentCell = $(evt.target).parent();
    this.currentCell.addClass("jf-focused");
    var classes = this.currentCell.attr("class").split(" ");
    var cellClass = new TS.List(classes)
      .where((d: string) => d.includes("rc-"))
      .toArray();
    if (cellClass.length > 0) {
      var rc = cellClass[0].replace(/rc-/g, "").split("-");
      var row = parseInt(rc[0]);
      var col = parseInt(rc[1]);
      $(this.element.nativeElement)
        .find("tr.jf-focused")
        .removeClass("jf-focused");
      var rows = $(this.element.nativeElement).find(".r-" + row);
      rows.addClass("jf-focused");
    }
  }

  onColumnHeaderClick(evt: MouseEvent, col: DataGridColumn): void {}

  onRowClick(evt: MouseEvent, rowIndex?: number): void {}

  onDeleteRow(evt: MouseEvent, item: any): void {
    alert("Delete");
  }

  focus(): void {
    $(this.holder.nativeElement).focus();
  }

  onColumn_ResizeStart($event: any, col: DataGridColumn): void {
    this.colResizer.css({
      display: "block"
    });
  }

  onColumn_ResizeDragging($event: DragMoveEvent, col: DataGridColumn): void {
    var x = $event.x;
    if (x) {
      this.colResizer.css({
        left: this.DragPositionX + x
      });
    }
  }

  onColumn_ResizeEnd($event: DragEndEvent, col: DataGridColumn): void {
    this.DragPositionX = null;
    this.colResizer.css({
      display: "none",
      left: 0
    });
    (col.width as number) += $event.x;
    //if (col.fixedRight == true) return;
    var backwrap = $(this.element.nativeElement).find(".backwrap");
    var _width = $($(this.element.nativeElement).find("table")[0]).width();
    backwrap.css({
      width: _width + 100
    });
  }

  private DragPositionX: number = null;
  onGhost_ElementCreated($event: GhostElementCreatedEvent): void {
    var leftPositionParent = $(this.element.nativeElement).offset().left;
    this.DragPositionX = $event.clientX - leftPositionParent;
  }

  onDrop($event: any): void {}

  getCellClass(
    col: DataGridColumn,
    rowIndex: number,
    cellIndex: number,
    isFixed: boolean
  ): any {
    var _class = {};
    _class[`rc-${rowIndex}-${col.seqNo}`] =
      isFixed == false ? col.fixed != true : col.fixed == true;
    return _class;
  }

  getRowClass(data: any, rowIndex: number, isFixed: boolean): any {
    var _class = {};
    _class["r-" + rowIndex] = true;
    return _class;
  }

  getCssClass(): any {
    var classes = super.getCssClass();
    classes["is-editing"] = this.isEditing === true;
    return classes;
  }
}

export enum DataTypeEnum {
  String = "string",
  Integer = "integer",
  Decimal = "decimal",
  Date = "date",
  DateTime = "dateTime",
  Boolean = "boolean"
}

export enum EditorTypeEnum {
  TextBox = 1,
  NumberBox = 2,
  CheckBox = 3,
  DateBox = 4,
  SelectBox = 5,
  ComboBox = 6
}

export class DataGridColumn {
  dataField: string;
  caption?: string;
  dataType?: DataTypeEnum = DataTypeEnum.String;
  fixed?: boolean = false;
  fixedRight?: boolean = false;
  width?: SafeStyle | number = null;
  editable?: boolean = false;
  seqNo?: number;
  allowEdit?: (data: any) => boolean | boolean;
  editorType?: EditorTypeEnum;
}

class DataGridOption extends BaseAppControlOption {
  columns?: DataGridColumn[] | any[];
  apiUrl?: string;
}

export interface ContextMenu {
  text: string;
  icon?: string;
  onClick?: () => void;
}
