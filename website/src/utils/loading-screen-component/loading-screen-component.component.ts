import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'loading-screen',
  templateUrl: './loading-screen-component.component.html',
  styleUrls: ['./loading-screen-component.component.less'],
})
export class LoadingScreenComponentComponent implements OnInit {

  @Input() isLoading: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

}
