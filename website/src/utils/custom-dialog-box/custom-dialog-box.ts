import { Position } from '../../bo/APP_MODELS';
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'custom-dialog-box',
  templateUrl: './custom-dialog-box.html',
  styleUrls: ['./custom-dialog-box.less']
})
export class CustomDialogBoxComponent implements OnInit {

  @ViewChild('mymodal')
  modalComponent: NgbModal

  @Input()
  childTemplate: TemplateRef<any>;
  @Input()
  title: string = "";
  @Input()
  size: string = "lg";
  
  @Input()
  captionPositiveButton: string = "OK";
  @Input()
  captionNegativeButton: string = "Cancel";
  
  @Input()
  closeOnPositiveButton: boolean = true;
  @Input()
  closeOnNegativeButton: boolean = true;
  @Input()
  IsShowBtnPositive: boolean = true;
  @Input() 
  IsShowBtnNegative: boolean = true;

  @Output() onLoad = new EventEmitter();
  @Output() onClosed = new EventEmitter();
  @Output() onPositiveButtonClick = new EventEmitter();
  @Output() onNegativeButtonClick = new EventEmitter();


  modal: any;


  constructor(protected modalService: NgbModal) { }

  ngOnInit(): void {

  }

  async open<T>(): Promise<T[]> {

    var promise = new Promise<T[]>(async (resolve, reject) => {

      var cmp = this.modalService.open(this.modalComponent, {
        ariaLabelledBy: 'modal-basic-title',
        backdrop: "static",
        size: this.size,
        centered: true,
      }).result.then((result) => {

        this.onClosed.emit();
        resolve(result);
      }, (result) => {

        
        this.onClosed.emit();
        resolve(result);
      });
      

      await this.onLoad.emit();
      
      this.formatDialogBox(cmp);
    });

    return promise
  }

  private formatDialogBox(cmp) {

    var parent = $(((cmp as any)._windowCmptRef as ElementRef));

    setTimeout(() => {

      var modalDialog = parent.find('.modal-dialog');
      var dialogBody = $('.modal-body')

      modalDialog.css("width", `100%!important`);

      setTimeout(() => {

        parent.css("cssText", "opacity:1!important");
      });
    });
  }

  btnPositive_OnClick(modal){

    this.onPositiveButtonClick.emit(modal);

    if(this.closeOnPositiveButton == true) modal.close();
  }

  btnNegative_OnClick(modal){

    this.onNegativeButtonClick.emit(modal);
    if(this.closeOnNegativeButton == true) modal.close();
  }

  close(reason: any){

    this.modalService.dismissAll(reason);
  }

}
