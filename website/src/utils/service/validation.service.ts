import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { FilterCriteriaType } from '../controls/appControlContainer/appControlContainer.component';
import { isNullOrUndefined } from 'util';
@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  constructor() { }

  validateEmail(email : string){

    var emailPattern =/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    return emailPattern.test(email);
  }

  validatePHMobileNumber(email : string){

    var pattern = /^(09|\+639|639)\d{9}$/gm;
    var result = pattern.test(email);

    return result;
  }

  validateDateTimeRange(dateStart: Date, dateEnd: Date, isAllowSameDate?: boolean){

    var now = moment(dateEnd);
    var end = moment(dateStart);
    var duration = moment.duration(now.diff(end));
    var milliseconds = duration.asMilliseconds();

    return milliseconds >= (isAllowSameDate != true ? 1 : 0);
  }

  validateDate(dateStart: Date, dateEnd?: Date){

    var validate = {
      isValid: true,
      message: ''
    }

    if(!moment(dateStart).isValid() && validate.isValid){

      validate.isValid = false;
      validate.message = "Invalid Date Start."
    }

    if(!isNullOrUndefined(dateEnd)){

      if(!moment(dateEnd).isValid() && validate.isValid){

        validate.isValid = false;
        validate.message = "Invalid Date End."
      }

      if(validate.isValid){

        if(dateStart > dateEnd){

          validate.isValid = false;
          validate.message = "Invalid Date Range."
        }
  
      }
    }

    return validate;
  }

}


 