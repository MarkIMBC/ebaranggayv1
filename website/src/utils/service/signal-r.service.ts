import { Injectable } from '@angular/core';
import * as signalR from "@aspnet/signalr";
import { ChartModel } from '../../utils/controls/class_helper';
import { DataService } from 'src/utils/service/data.service';
import { isNullOrUndefined } from 'util';


@Injectable({
  providedIn: 'root'
})
export class SignalRService {
  public data: ChartModel[];

  public hubConnection: signalR.HubConnection;
  public connectionId : string;

  constructor(
    private ds: DataService) { }

  public startConnection = async (user) => {

    var config = await this.ds.loadConfig();

    this.hubConnection = new signalR.HubConnectionBuilder()
                        .configureLogging(signalR.LogLevel.Debug)
                        .withUrl(`${config.chatUrl}`, {
                          skipNegotiation: true,
                          transport: signalR.HttpTransportType.WebSockets
                        })
                        .build();
    try {

      await this.hubConnection
          .start()
          .then(() => console.log('Connection started'))
          .then(() => this.getConnectionId(user))
          .catch(err => {

            setTimeout(function() {
           
  
            }, 5000);
          });

    } catch (err) {

        //console.log(err);
    }
  }

  public getConnectionId = (user) => {
    this.hubConnection.invoke('getconnectionid').then(
      
      (data) => {

          //console.log('connectionId', data);
          this.connectionId = data;
          this.hubConnection.send("RegisterGroup", data, user);
        }
    ); 
  }
 
  public sentMessage = async (sender: string, recipient: string, message: string) => {
    
    this.hubConnection.send("NewMessage",sender, recipient, message);
  }

  private _ConnetToChatController = async () => {

    var result:any = await this.ds.post("chathub", null);

    if(isNullOrUndefined(result)) throw 'Unable to connect on chat hub controller.'; 
    if(isNullOrUndefined(result.success)) throw 'Unable to connect on chat hub controller.'; 
  }
}