
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import * as $ from "jquery";

@Injectable({
  providedIn: "root"
})
export class PopupService {
  private popups: any[] = [];

  popupContainer: JQuery<HTMLElement> = null;

  add(popup: any) {
    // add Popup to array of active Popups
    this.popups.push(popup);
    if (this.popupContainer == null) {
      this.popupContainer = $('<div class="popup-container"></div>');
      var body = $(document.body).find('app-root');
      body.append(this.popupContainer);
    }
  }

  hasId(id: string): boolean {
    var _popup = this.popups.find(d => d.id === id);
    if (_popup) return true;
    return false;
  }

  remove(id: string) {
    // remove Popup from array of active Popups
    this.popups = this.popups.filter(x => x.id !== id);
  }

  open(id: string) {
    // open Popup specified by id
    let popup: any = this.popups.filter(x => x.id === id)[0];
    popup.open();
    $(this.popupContainer).css({
      "display": 'block'
    });
  }

  close(id: string) {
    // close Popup specified by id
    let popup: any = this.popups.filter(x => x.id === id)[0];
    popup.close();
    //
    //
    //
    var popups = $(this.popupContainer).find('.popup.open');
    if (popups.length == 0) {
      $(this.popupContainer).css({
        "display": 'none'
      });
    }
  }
}
