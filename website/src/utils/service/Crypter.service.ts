import { DataService } from 'src/utils/service/data.service';
import { MenuItem } from 'primeng/api/menuitem';
import { Injectable, Input, Directive } from "@angular/core";
import sha256 from 'crypto-js/sha256';
import hmacSHA512 from 'crypto-js/hmac-sha512';
import Base64 from 'crypto-js/enc-base64';
import * as CryptoJS from 'crypto-js';
import { isNullOrUndefined } from 'util';

export const CrypterKey : string = "sql123$%^abc123$";
                                 // 8080808080808080
@Injectable({
  providedIn: "root"
})
export class CrypterService {

    encrypt(str, isEncrypted?:boolean) : string {
      if (isEncrypted == true) return str;
        var key = CryptoJS.enc.Utf8.parse(CrypterKey);
        var iv = CryptoJS.enc.Utf8.parse(CrypterKey);

        let encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(str), key,
            {
                keySize: 128 / 2,
                iv: iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            });
        return encrypted.toString();
      }

      decrypt(str) : string {

        if (isNullOrUndefined(str) == true) return null;
        if (str.length == 0) return null;

        var key = CryptoJS.enc.Utf8.parse(CrypterKey);
        var iv = CryptoJS.enc.Utf8.parse(CrypterKey);

        const plainText = CryptoJS.AES.decrypt(str, key, {
          keySize: 128 / 2,
          iv: iv,
          mode: CryptoJS.mode.CBC,
          padding: CryptoJS.pad.Pkcs7
        });

        return  plainText.toString(CryptoJS.enc.Utf8);
      }
}   