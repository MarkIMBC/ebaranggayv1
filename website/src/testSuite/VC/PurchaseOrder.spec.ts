// import { AppPickListBoxComponent } from '../../utils/controls/appPickListBox/appPickListBox.component';
// import { VC_PurchaseOrder_DetailView } from '../../bo/VC/Transaction/VC_PurchaseOrder';
// import { APP_DETAILVIEW, PurchaseOrder, PurchaseOrder_Detail, BusinessPartner } from '../../bo/APP_MODELS';
// import { DetailViewComponent } from '../../app/View/DetailView/DetailView.component';
// import { ComponentFixture, TestBed, async } from '@angular/core/testing';
// import { InitTestBedModule } from '..';

// describe(`Purchase Order Create New Data`, () => {

//   let component: DetailViewComponent;
//   let fixture: ComponentFixture<DetailViewComponent>;
//   let PurchaseOrder: PurchaseOrder;
//   let VC_PurchaseOrder : VC_PurchaseOrder_DetailView;
//   beforeEach(async(() => {
//     InitTestBedModule();
//   }));

//   afterEach(() => {
//     spyOn(component, 'ngOnDestroy').and.callFake(() => { });
//     fixture.destroy();
//   });

//   beforeEach(async () => {
//     fixture = TestBed.createComponent(DetailViewComponent);
//     component = fixture.componentInstance;
//     spyOn(component, 'ngAfterViewInit').and.callFake(() => { });
//     component.ID_DetailView = null;
//     fixture.detectChanges();
//     await component.initView(APP_DETAILVIEW.PURCHASEORDER_DETAILVIEW, "-1");
//     PurchaseOrder = component.CurrentObject;
//     PurchaseOrder.PurchaseOrder_Detail = [];
//     VC_PurchaseOrder = component.vcModels.find(d => d.ClassName == "VC_PurchaseOrder_DetailView") as VC_PurchaseOrder_DetailView;
//     fixture.detectChanges();
//   });

//   it("Should successfully initilized the view", async () => {
//     var ID : number = PurchaseOrder.ID;
//     expect(component.CurrentObject).toBeTruthy();
//     expect(ID).toEqual(-1);
//   });

//   it('should compute details correctly', async () => { 
//     var ItemA : PurchaseOrder_Detail = {
//       ID : -1,
//       ID_Item : 1,
//       Quantity : 1,
//       UnitPrice : 200
//     }
//     PurchaseOrder.PurchaseOrder_Detail.push(ItemA);
//     // fixture.detectChanges();
//     await fixture.whenStable();
//     //await fixture.whenRenderingDone();
//     VC_PurchaseOrder.compute();
//     expect(ItemA.Subtotal).toEqual(200);
//     expect(VC_PurchaseOrder.detailGrid.gridItems.length).toEqual(1);
//   });

//   it('should successfully load business partner', async () => {
//     PurchaseOrder.ID_BusinessPartner = 1; //primarty
//     fixture.detectChanges();
//     await fixture.whenStable();
//     var LookupBusinessPartner = component.getControl<AppPickListBoxComponent>("ID_BusinessPartner");
//     var Supplier: BusinessPartner = LookupBusinessPartner.itemSelected;
//     expect(Supplier).toBeTruthy();
//     expect(Supplier.Name).toEqual("Meat");
//     expect(Supplier.Code).toEqual("Hello");
//   });

// });

