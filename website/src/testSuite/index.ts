import { DetailViewComponent } from '../app/View/DetailView/DetailView.component';
import { DynamicTemplateService, RUNTIME_COMPILER_PROVIDERS } from '../app/AppServices/dynamic-temaplate.svc';
import { SharedModule } from '../utils/shared.module';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { DataService } from 'src/utils/service/data.service';
import { ListViewComponent } from '../app/View/ListView/ListView.component';
import { TestBed } from '@angular/core/testing';
import { appInit } from '../app/app.module';

export function InitTestBedModule(): Promise<any> {
    DataService.API_URL = "http://localhost:5000/api/";
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000000;
    return TestBed.configureTestingModule({
      declarations: [
        ListViewComponent,
        DetailViewComponent,
      ],
      imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        RouterModule.forRoot([]),
        SharedModule,
      ],
      providers: [
        DataService,
        DynamicTemplateService,
        {
          provide: APP_INITIALIZER,
          useFactory: appInit,
          multi: true,
          deps: [DataService]
        },
        ...RUNTIME_COMPILER_PROVIDERS
      ]
    }
    ).compileComponents();
  };
  