GO

Use db_ebarangayv1_batangas_laurel

GO
CREATE OR
ALTER FUNCTION [dbo].fGetAPILink()
RETURNS VARCHAR(MAX)
AS
  BEGIN
      DECLARE @result VARCHAR(MAX) = '';

      SET @result = 'https://laurelbatangasapi.ebarangaysoftware.com'

      return @result
  END
 
GO

EXEC _pRefreshAllViews

GO

Update tCompany
SET    Name = 'Municipality of Laurel',
       Code = 'laurel',
       IsActive = 1
WHERE  ID = 1

Update tCompany
SET    IsActive = 0
WHERE  ID > 1

Update vUser
SET    Password = LEFT(NEWID(), 4)
where  ID_Company = 1