SELECT
    _client.client_id,
    _client.name,
    _pets.pet_id,
    _pets.pet_name,
    _pets.`birthdate`,
    `pet_gender`,
    `species`,
    `breed`,
    `neutered`,
    IF(`deceased` = 1, "Yes", "No") deceased,
    `color`,
    `microchip`,
    `remarks`
FROM
    `pets` _pets
INNER JOIN `clients` _client ON
    _pets.client_id = _client.client_id
WHERE
    1