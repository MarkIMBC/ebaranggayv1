Declare @ID_Compay INT = 40

SELECT Code,
       LTRIM(RTRIM(Name)) Name,
       Address,
       Email,
       ContactNumber,
       ContactNumber2
FROM   vClient
WHERE  ID_Company = @ID_Compay
ORDER  BY LTRIM(RTRIM(Name))

SELECT Client.Code                                            ClientCode,
       LTRIM(RTRIM(ISNULL(patient.Name_Client, '')))          Client,
       CASE
         WHEN patient.Old_patient_id IS NOT NULL THEN '(from Legacy)'
         ELSE ISNULL(patient.Code, '')
       END                                                    PatientCode,
       patient.Name                                           [Pet Name],
       ISNULL(patient.Species, '')                            [Breed - Species],
       ISNULL(patient.Name_Gender, '')                        Gender,
       ISNULL(FORMAT(patient.DateBirth, 'yyyy-MM-dd'), '')    DateBirth,
       CASE
         WHEN ISNULL(patient.IsNeutered, 0) = 1 THEN 'YES'
         ELSE 'NO'
       END                                                    Neutered,
       CASE
         WHEN ISNULL(patient.IsDeceased, 0) = 1 THEN 'YES'
         ELSE 'NO'
       END                                                    Deceased,
       ISNULL(FORMAT(patient.DateDeceased, 'yyyy-MM-dd'), '') DateDeceased,
       ISNULL(patient.AnimalWellness, '')                     AnimalWellness
FROM   vPatient patient
       INNER JOIN TClient client
               on client.ID = patient.ID_Client
WHERE  patient.ID_Company = @ID_Compay
ORDER  BY LTRIM(RTRIM(Name_Client)),
          patient.Name 
