GO

CREATE OR
ALTER PROC ___pSetManualSMSDateSent(@Page           INT,
                                    @Count          INT,
                                    @DateSentString VARCHAR(MAX),
                                    @PrefixCode     VARCHAR(MAX))
as
  BEGIN
      DECLARE @IDs_Reference typIntList
      DECLARE @ID_Reference INT

      SET @Page = @Page - 1

      INSERT @IDs_Reference
      select ID_REFERENCE
      FROm   tSample1
      ORDER  BY Rowindex
      OFFSET @Count * @Page ROWS FETCH FIRST @Count ROWS ONLY;

      select ROWIndex,
             Name_Company,
             Name_Client,
             ContactNumber_Client,
             Message,
             Code,
             ID_REFERENCE
      FROm   tSample1 hed
             inner join @IDs_Reference ids
                     on ids.ID = hed.ID_Reference
      where  Code LIKE @PrefixCode + '%'
             and ( ( LEN(ContactNumber_Client) = 11 )
                    OR ( LEN(ContactNumber_Client) = 12
                         AND ContactNumber_Client LIKE '63%' ) )

      -- AND RowIndex NOT IN ( 238, 250 )
      DECLARE import_cursor CURSOR FOR
        select ID_REFERENCE
        FROm   tSample1 hed
               inner join @IDs_Reference ids
                       on ids.ID = hed.ID_Reference

      OPEN import_cursor

      FETCH NEXT FROM import_cursor INTO @ID_Reference

      WHILE @@FETCH_STATUS = 0
        BEGIN
            DECLARE @DateSentString2 VARCHAR(MAX) = @DateSentString

            SET @DateSentString2 = REPLACE(@DateSentString2, 'mm', FORMAT(FLOOR(RAND()*(60- 0)) + 0, '#0'))
            SET @DateSentString2 = REPLACE(@DateSentString2, 'ss', FORMAT(FLOOR(RAND()*(60- 0)) + 0, '#0'))
            SET @DateSentString2 = REPLACE(@DateSentString2, 'ml', FORMAT(FLOOR(RAND()*(999- 0)) + 0, '#0'))

            IF( @PrefixCode = 'SOAP' )
              BEGIN
                  Update tPatient_SOAP_Plan
                  SET    DateSent = @DateSentString2,
                         IsSentSMS = 1
                  FROM   tPatient_SOAP_Plan soapPlan
                  WHERE  ID = @ID_Reference
              END
            ELSE IF( @PrefixCode = 'VAC' )
              BEGIN
                  Update tPatient_Vaccination_Schedule
                  SET    DateSent = @DateSentString2,
                         IsSentSMS = 1
                  FROM   tPatient_Vaccination_Schedule soapPlan
                  WHERE  ID = @ID_Reference
              END

            FETCH NEXT FROM import_cursor INTO @ID_Reference
        END

      CLOSE import_cursor;

      DEALLOCATE import_cursor;
  END

GO

exec ___pSetManualSMSDateSent
  1,
  100,
  '2021-10-05 18:mm:ss.ml',
  'SOAP'

exec ___pSetManualSMSDateSent
  2,
  100,
  '2021-10-05 20:mm:ss.ml',
  'SOAP'

exec ___pSetManualSMSDateSent
  3,
  100,
  '2021-10-05 21:mm:ss.ml',
  'SOAP'

exec ___pSetManualSMSDateSent
  4,
  100,
  '2021-10-05 23:mm:ss.ml',
  'SOAP'

exec ___pSetManualSMSDateSent
  5,
  100,
  '2021-10-05 23:mm:ss.ml',
  'SOAP'

exec ___pSetManualSMSDateSent
  1,
  100,
  '2021-10-05 18:mm:ss.ml',
  'VAC'

exec ___pSetManualSMSDateSent
  2,
  100,
  '2021-10-05 20:mm:ss.ml',
  'VAC'

exec ___pSetManualSMSDateSent
  3,
  100,
  '2021-10-05 21:mm:ss.ml',
  'VAC'

exec ___pSetManualSMSDateSent
  4,
  100,
  '2021-10-05 23:mm:ss.ml',
  'VAC'

exec ___pSetManualSMSDateSent
  5,
  100,
  '2021-10-05 23:mm:ss.ml',
  'VAC' 
