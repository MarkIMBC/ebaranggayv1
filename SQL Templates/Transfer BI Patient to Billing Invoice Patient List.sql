Declare @BIPatient TABLE
  (
     ID_BillingInvoice INT,
     ID_Patient        Int,
     ID_Company        INT
  )
Declare @BIPatientName TABLE
  (
     ID_BillingInvoice INT,
     PatientNames      VARCHAR(MAX)
  )

INSERT @BIPatient
SELECT bi.ID,
       bi.ID_Patient,
       bi.ID_Company
FROm   tBillingInvoice bi
       LEFT JOIN (SELECT ID_BillingInvoice,
                         Count(*) Count
                  FROm   tBillingInvoice_Patient
                  GROUP  BY ID_BillingInvoice) biPatientCount
              on bi.ID = biPatientCount.ID_BillingInvoice
where  ISNULL(bi.ID_Patient, 0) > 0
       AND ISNULL(Count, 0) = 0

INSERT INTO [dbo].[tBillingInvoice_Patient]
            ([ID_BillingInvoice],
             [ID_Patient],
             [ID_Company],
             [IsActive],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy])
SELECT ID_BillingInvoice,
       ID_Patient,
       ID_Company,
       1,
       GETDATE(),
       GETDATE(),
       1,
       1
FROM   @BIPatient

INSERT @BIPatientName
SELECT ID,
       CASE
         WHEN LEN(PatientNames) > 0 THEN LEFT(PatientNames, LEN(PatientNames) - 1)
         ELSE ''
       END
FROM   (SELECT bi.ID,
               bi.ID_Patient,
               (SELECT ST1.Name_Patient + ', ' AS [text()]
                FROM   dbo.vBillingInvoice_Patient ST1
                WHERE  ST1.ID_BillingInvoice = bi.ID
                ORDER  BY ST1.Name_Patient
                FOR XML PATH ('')) PatientNames
        FROm   tBillingInvoice bi
               LEFT JOIN (SELECT ID_BillingInvoice,
                                 Count(*) Count
                          FROm   tBillingInvoice_Patient
                          GROUP  BY ID_BillingInvoice) biPatientCount
                      on bi.ID = biPatientCount.ID_BillingInvoice) tbl

Update tBillingInvoice
SET    PatientNames = biPatientNames.PatientNames
FROM   tBillingInvoice bi
       inner join @BIPatientName biPatientNames
               ON bi.ID = biPatientNames.ID_BillingInvoice 
