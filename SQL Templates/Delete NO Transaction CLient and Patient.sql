DECLARE @ID_Company_PetValleyAnimalClinic_TaguigTuktukanBranch INT = 113
DECLARE @IDs_Client typIntList
DECLARE @IDs_Patient typIntList

INSERT @IDs_Client
SELECT DISTINCT ID_Client
FROm   tBillingInvoice
where  ID_Company = @ID_Company_PetValleyAnimalClinic_TaguigTuktukanBranch 

INSERT @IDs_Client
SELECT DISTINCT ID_Client
FROm   vPatient_SOAP
where  ID_Company = @ID_Company_PetValleyAnimalClinic_TaguigTuktukanBranch 

INSERT @IDs_Client
SELECT DISTINCT ID_Client
FROm   tPatient_Confinement
where  ID_Company = @ID_Company_PetValleyAnimalClinic_TaguigTuktukanBranch 

INSERT @IDs_Client
SELECT DISTINCT ID_Client
FROm   tPatientAppointment
where  ID_Company = @ID_Company_PetValleyAnimalClinic_TaguigTuktukanBranch 

/* Patient*/
INSERT @IDs_Patient
SELECT DISTINCT ID_Patient
FROm   tBillingInvoice
where  ID_Company = @ID_Company_PetValleyAnimalClinic_TaguigTuktukanBranch 

INSERT @IDs_Patient
SELECT DISTINCT ID_Patient
FROm   vPatient_SOAP
where  ID_Company = @ID_Company_PetValleyAnimalClinic_TaguigTuktukanBranch 

INSERT @IDs_Patient
SELECT DISTINCT biPatient.ID_Patient
FROm   tBillingInvoice biHed
       inner join tBillingInvoice_Patient biPatient
               on biHed.ID = biPatient.ID_BillingInvoice
where  biHed.ID_Company = @ID_Company_PetValleyAnimalClinic_TaguigTuktukanBranch 

INSERT @IDs_Patient
SELECT DISTINCT ID_Patient
FROm   tPatient_Confinement
where  ID_Company = @ID_Company_PetValleyAnimalClinic_TaguigTuktukanBranch 

INSERT @IDs_Patient
SELECT DISTINCT ID_Patient
FROm   tPatientAppointment
where  ID_Company = @ID_Company_PetValleyAnimalClinic_TaguigTuktukanBranch 

/* DELETEING */
DELETE FROM @IDs_Client
WHERE  ID IS NULL

DELETE FROM @IDs_Patient
WHERE  ID IS NULL

Update tClient
SET    IsActive = 0
WHERE  ID NOT IN (SELECT DISTINCT ID
                  FROM   @IDs_Client)
       and ID_Company = @ID_Company_PetValleyAnimalClinic_TaguigTuktukanBranch 
       AND Comment like '%Import%'
       AND IsActive = 1

Update tPatient
SET    IsActive = 0
WHERE  ID NOT IN (SELECT DISTINCT ID
                  FROM   @IDs_Patient)
       and ID_Company = @ID_Company_PetValleyAnimalClinic_TaguigTuktukanBranch 
       AND Comment like '%Import%'
       AND IsActive = 1 



Update tClient set IsActive  = 0 where DateCreated = '2021-07-29 04:17:21.210' and Comment LIKE '%Import%'
Update tClient set IsActive  = 0 where DateCreated = '2021-07-29 04:17:18.237' and Comment LIKE '%Import%'
Update tPatient set IsActive  = 0 where DateCreated = '2021-07-29 04:17:21.553' and Comment LIKE '%Import%'


SELECT * FROm tPatient where ID_Company  = @ID_Company_PetValleyAnimalClinic_TaguigTuktukanBranch 
SELECT * FROm tClient where ID_Company  = @ID_Company_PetValleyAnimalClinic_TaguigTuktukanBranch 