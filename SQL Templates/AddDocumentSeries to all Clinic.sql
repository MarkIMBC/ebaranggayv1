    INSERT dbo.tDocumentSeries                                                                                                      
    (  
        Code,  
        Name,  
        IsActive,  
        Comment,  
        DateCreated,  
        DateModified,  
        ID_CreatedBy,  
        ID_LastModifiedBy,  
        ID_Model,  
        Counter,  
        Prefix,  
        IsAppendCurrentDate,  
        DigitCount,  
        ID_Company  
    )  
   SELECT  docSeries.Code,  
           docSeries.Name,  
           docSeries.IsActive,  
           docSeries.Comment,  
           docSeries.DateCreated,  
           docSeries.DateModified,  
           docSeries.ID_CreatedBy,  
           docSeries.ID_LastModifiedBy,  
           docSeries.ID_Model,  
           1,  
           docSeries.Prefix,  
           docSeries.IsAppendCurrentDate,  
           5,  
           company.ID  
    FROM dbo.tDocumentSeries  docSeries, tCompany company
    WHERE docSeries.ID_Company = 1 AND docSeries.ID = 733 And company.ID <> 1;  