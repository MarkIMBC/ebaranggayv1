SELECT
    _clients.name ClientName,
    _pets.pet_name PetName,
    _soap.soap_id,
    _soap.date,
    _users.name vetName,
    _schedule.start ScheduleStart,
    _schedule.start ScheduleEnd
FROM
    `soap` _soap
LEFT JOIN `pets` _pets ON
    _soap.pet_id = _pets.pet_id
LEFT JOIN `clients` _clients ON
    _clients.client_id = _pets.client_id
INNER JOIN `appointment` _appointments ON
    _soap.appointment_id = _appointments.appointment_id
LEFT JOIN `schedule` _schedule ON
    _schedule.sched_id = _appointments.schedule_id
LEFT JOIN `users` _users ON
    _users.user_id = _appointments.vet_id
ORDER BY
    _clients.name,
    _pets.pet_name,
    _soap.soap_id,
    _soap.date