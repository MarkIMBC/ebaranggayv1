DECLARE @Name_Religion VARCHAR(MAX) = 'Protestante (UCCP)'
DECLARE @name VARCHAR(50) -- database name   
DECLARE @dateStart DateTime
DECLARE @dateEnd DateTime
DECLARE @table TABLE
  (
     ID           INT,
     Name         VARCHAR(MAX),
     IsActive     BIT,
     DateCreated  DateTime,
     DatabaseName VARCHAR(MAX)
  )
DECLARE cursor12231 CURSOR FOR
  SELECT name
  FROM   MASTER.dbo.sysdatabases
  WHERE  ( name LIKE 'db_ebarangayv1_%' )

OPEN cursor12231

FETCH NEXT FROM cursor12231 INTO @name

WHILE @@FETCH_STATUS = 0
  BEGIN
      SET @dateStart = GETDATE();

      BEGIN TRY
          INSERT @table
          exec( 'exec ' + @name +'.dbo.pInsertReligion '''+ @Name_Religion+''', 1')

          SET @dateEnd = GETDATE();
      END TRY
      BEGIN CATCH
          DECLARE @msg VARCHAR(MAX) = ERROR_MESSAGE()
          SET @dateEnd = GETDATE();
          SELECT @msg
      END CATCH

      FETCH NEXT FROM cursor12231 INTO @name
  END

CLOSE cursor12231

DEALLOCATE cursor12231

SELECT Name,
       Count(*)
FROM   @table
GROUP  BY Name
Order  by Name 

GO

DECLARE @name VARCHAR(50) -- database name   
DECLARE @dateStart DateTime
DECLARE @dateEnd DateTime
DECLARE @table TABLE
  (
     ID           INT,
     Name         VARCHAR(MAX),
     IsActive     BIT,
     DateCreated  DateTime,
     DatabaseName VARCHAR(MAX)
  )
DECLARE cursor12231 CURSOR FOR
  SELECT name
  FROM   MASTER.dbo.sysdatabases
  WHERE  ( name LIKE 'db_ebarangayv1_%' )

OPEN cursor12231

FETCH NEXT FROM cursor12231 INTO @name

WHILE @@FETCH_STATUS = 0
  BEGIN
      SET @dateStart = GETDATE();

      BEGIN TRY
          INSERT @table
          exec( 'SELECT ID, Name, IsActive, DateCreated, '''+ @name +''' DatabaseName FROM ' + @name +'.dbo.tReligion')

          SET @dateEnd = GETDATE();
      END TRY
      BEGIN CATCH
          DECLARE @msg VARCHAR(MAX) = ERROR_MESSAGE()
          SET @dateEnd = GETDATE();
          SELECT @msg
      END CATCH

      FETCH NEXT FROM cursor12231 INTO @name
  END

CLOSE cursor12231

DEALLOCATE cursor12231

SELECT Name,
       Count(*)
FROM   @table
GROUP  BY Name
Order  by Name 

