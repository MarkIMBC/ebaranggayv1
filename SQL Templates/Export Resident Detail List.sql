DEclare @GUID_Company VARCHAR(MAX) = '83A8DFD8-8B0C-402E-82BD-8E7175BABE59'

IF(SELECT Count(*)
   FROM   vCompanyActive
   WHERE  Guid = @GUID_Company
          AND IsActive = 1) <> 1
  BEGIN ;
      THROW 51000, 'Company does not exist.', 1;
  END

SELECT *
FROm   vCompanyActive
WHERE  GUID = @GUID_Company

DECLARE @ID_Company INT

SELECT @ID_Company = ID
FROM   tCompany
WHERE  Guid = @GUID_Company

----------------------------------------------------------------------------------------
SELECT Name,
Address,
       Name_Gender Gender,
       ISNULL(ContactNumber, '') ContactNumber,
	   DateBirth,
       CASE
         WHEN ISNULL(IsDeceased, 0) = 1 THEN 'Yes'
         ELSE 'No'
       END Deceased,
       CASE
         WHEN ISNULL(IsPermanent, 0) = 1 THEN 'Yes'
         ELSE 'No'
       END Permanent,
       CASE
         WHEN ISNULL(IsMigrante, 0) = 1 THEN 'Yes'
         ELSE 'No'
       END Migrant,
	   Name_CivilStatus CivilStatus, Name_EducationalLevel EducationalLevel, Name_HomeOwnershipStatus HomeOwnershipStatus,
	   Name_OccupationalStatus, Occupation
FROM   vResident
WHERE  ID_Company = @ID_Company
       AND IsActive = 1 
	   Order by Name 
