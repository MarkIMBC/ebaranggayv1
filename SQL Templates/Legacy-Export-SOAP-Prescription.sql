SELECT
    _clients.name ClientName,
    _pets.pet_name PetName,
    _soap.soap_id,
    _soap.date,
    _users.name vetName,
    _prescribed.datetime,
    _prescribed.prescribed
FROM
    `soap` _soap
LEFT JOIN `prescribed` _prescribed ON
    _prescribed.soap_id = _soap.soap_id
LEFT JOIN `pets` _pets ON
    _soap.pet_id = _pets.pet_id
LEFT JOIN `clients` _clients ON
    _clients.client_id = _pets.client_id
  LEFT JOIN `users` _users ON
    _users.user_id = _prescribed.author_id
ORDER BY
    _clients.name,
    _pets.pet_name,
    _soap.soap_id,
    _soap.date,
    _prescribed.datetime