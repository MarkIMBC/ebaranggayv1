DECLARE @Source_ID_Company INT = 23
Declare @adjustInventory typReceiveInventory

INSERT @adjustInventory
       ([Code],
        [ID_Item],
        [Quantity],
        [UnitPrice],
        [DateExpired],
        [BatchNo],
        [ID_FilingStatus],
        [ID_Company],
        [Comment],
        [IsAddInventory])
SELECT 'Reset Inventory',
       ID,
       CurrentInventoryCount,
       0.00,
       OtherInfo_DateExpiration,
       NULL,
       3,
       @Source_ID_Company,
       'Reset Inventory on 2021-09-05 10:50 pm',
       CASE WHEN CurrentInventoryCount > 0 then 0 ELSE 1 END
FROM   tItem
where  ID_Company = @Source_ID_Company
       AND ISNULL(CurrentInventoryCount, 0) > 0

Declare @ID_UserSession INT

select TOP 1 @ID_UserSession = ID
FRom   tUsersession
where  ID_User = 1
ORDER  BY ID DESC

exec pReceiveInventory
  @adjustInventory,
  @ID_UserSession
