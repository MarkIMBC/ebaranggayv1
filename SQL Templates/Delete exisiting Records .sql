DELETE FROM tAuditTrail_Detail
DELETE FROM tAuditTrail
DELETE FROM tInventoryTrail

DELETE FROM tPatient_SOAP_Treatment
DELETE FROM tPatient_SOAP_Prescription
DELETE FROM tPatient_SOAP_Plan
DELETE FROM tPatient_SOAP_SMSStatus
DELETE FROM tPatient_SOAP_RegularConsoltation
DELETE FROM tPatient_SOAP
DELETE FROM [dbo].[tPatientWaitingList_Logs]
DELETE FROM [dbo].[tPatientWaitingList]
DELETE FROM [dbo].[tPatientAppointment]
DELETE FROM [dbo].[tUserSession]
DELETE FROM [dbo].[tUserSession] where ID_User IN (
SELECT ID FROM vUser WHERE ID_Company NOT IN ( 1)
)

DELETE FROM [dbo].[tPatient_Confinement_ItemsServices]
DELETE FROM [dbo].[tPatient_Confinement_Patient]
DELETE FROM [dbo].[tPatient_Confinement]

DELETE FROM [dbo].[tFileUpload]
DELETE FROM [dbo].[tAppointmentStatusLog]


DELETE FROM [dbo].[tPurchaseOrder_Detail]
DELETE FROM [dbo].[tPurchaseOrder]

DELETE FROM [dbo].[tReceivingReport_Detail]
DELETE FROM [dbo].[tReceivingReport]


DELETE FROM [dbo].tDocumentSeries where ID_Company NOT IN (1)

Update tEmployee SET ID_LastModifiedBy = 10, ID_CreatedBy = 10
DELETE FROM [dbo].tEmployee where ID_Company NOT IN (1)

Update [vUser] SET ID_Employee = 1 WHERE ID_Company NOT IN (1)
DELETE FROM [dbo].[tUser] where ID IN (
SELECT ID FROM vUser WHERE ID_Company NOT IN ( 1)
)
DELETE FROM [dbo].[tUser] where ID IN (
SELECT ID FROM vUser WHERE ID_Employee IS NULL
)

DELETE FROM tPatient_Wellness_Schedule_SMSStatus
DELETE FROM tPatient_Wellness_Schedule
DELETE FROM tPatient_Wellness_Detail
DELETE FROM tPatient_Wellness

DELETE FROM [dbo].[_tDatabaseMemoryLog]

DELETE FROM tPaymentTransaction
DELETE FROM tBillingInvoice_Patient
DELETE FROM tBillingInvoice_Detail
DELETE FROM tBillingInvoice


DELETE FROM [tClientWithdraw ]
DELETE FROM [tClientWithdraw ]
DELETE FROM tClient_CreditLogs

DELETE FROM tPatient_DentalExamination
DELETE FROM tPatient_History
DELETE FROM tPatient
DELETE FROM tClient
