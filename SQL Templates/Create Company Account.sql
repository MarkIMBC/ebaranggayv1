Declare @CompanyName VARCHAR(MAX) ='Ven Veterinary Clinic - Training'
Declare @CompanyCode VARCHAR(MAX) = 'venvettrain'
Declare @MaxSMSCountPerDay INT = 25
Declare @ID_Company INT = 0

IF(SELECT COUNT(*)
   FROm   tCOmpany
   WHERE  Name = @CompanyName) = 0
  BEGIN
      exec pCreateNewCompanyAccess
        @CompanyName,
        @CompanyCode
  END

SELECT @ID_Company = ID
FROm   tCompany
where  Name = @CompanyName

Update tCompany Set GUID = NEWID() WHERE ID = @ID_Company AND GUID IS NULL
Update tCompany Set ReceptionPortalGuid = NEWID() WHERE ID = @ID_Company AND ReceptionPortalGuid IS NULL

exec [dbo].[pAdd_SMSPatientSOAP_Company]
  @ID_Company

exec [dbo].[pAdd_Company_SMSSetting]
  @ID_Company,
  @MaxSMSCountPerDay

exec pAddCompany_Subscription
  @ID_Company,
  '2021-01-01',
  '9999-12-31'

SELECT ID_Company,
       Name_Company,
       Name_Employee,
       ID ID_User,
       Username,
       Password
FROM   vUser
WHERE  IsActive = 1
Order  by Name_Company,
          Name_Employee 
