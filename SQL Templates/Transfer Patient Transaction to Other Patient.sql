DECLARE @ID_Patient_Source INT = 360665
DECLARE @ID_Patient_Destination INT = 359627
DECLARE @ID_Client_Destination INT = 0

SELECT @ID_Client_Destination = ID_Client
FROM   tPatient
WHERE  ID = @ID_Patient_Destination

UPDATE tPatient_SOAP
SET    ID_Patient = @ID_Patient_Destination 
WHERE ID_Patient = @ID_Patient_Source

--UPDATE tPatient_Confinement
--SET    ID_Client = @ID_Client_Destination,
--       ID_Patient = @ID_Patient_Destination 
--WHERE ID_Patient = @ID_Patient_Source

UPDATE tVeterinaryHealthCertificate 
SET    ID_Client = @ID_Client_Destination,
       ID_Patient = @ID_Patient_Destination 
WHERE ID_Patient = @ID_Patient_Source

UPDATE tPatient_Wellness 
SET    ID_Client = @ID_Client_Destination,
       ID_Patient = @ID_Patient_Destination 
WHERE ID_Patient = @ID_Patient_Source

UPDATE tPatient_Vaccination
SET    ID_Client = @ID_Client_Destination,
       ID_Patient = @ID_Patient_Destination 
WHERE ID_Patient = @ID_Patient_Source


