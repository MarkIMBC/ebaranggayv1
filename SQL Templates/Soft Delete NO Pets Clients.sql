Declare @ID_COmpany INT = 164
DECLARE @IDs_CLient typIntList
DECLARE @ClientPetCounts TABLE
  (
     ID_Client INT,
     Count     INt
  )

INSERT @ClientPetCounts
SELECT ID_Client,
       Count(*)
FROm   tPatient
where  ID_Company = @ID_COmpany
       and IsActive = 1
GROUP  BY ID_Client

INSERT @IDs_CLient
select ID
FROm   tClient client
       LEFT JOIN @ClientPetCounts petCount
              on client.ID = petCount.ID_Client
where  ID_Company = @ID_COmpany
       and ISNULL(Count, 0) = 0

Update tClient
set    IsActive = 0
FROm   tClient client
       inner join @IDs_CLient ids
               on client.ID = ids.ID 
