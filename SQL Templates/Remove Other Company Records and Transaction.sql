/* REMOVE Other Company Data */
DECLARE @ID_COMPANY INT = 157

DELETE FROM tClient
WHERE  ID_Company NOT IN ( @ID_COMPANY )

DELETE FROM tPatient_DentalExamination
WHERE  ID_Patient NOT IN (SELECT ID
                          FROM   tPatient
                          WHERE  ID_Company NOT IN ( @ID_COMPANY ))

Update tPatient
SET    Name = '',
       Code = ''
WHERE  ID_Company NOT IN ( @ID_COMPANY )

DELETE FROM tPatient_SOAP_Prescription
WHERE  ID_Patient_SOAP NOT IN (SELECT ID
                               FROM   tPatient_SOAP
                               WHERE  ID_Company NOT IN ( @ID_COMPANY ))

DELETE FROM tPatient_SOAP_Plan
WHERE  ID_Patient_SOAP NOT IN (SELECT ID
                               FROM   tPatient_SOAP
                               WHERE  ID_Company NOT IN ( @ID_COMPANY ))

DELETE FROM tPatient_SOAP
WHERE  ID_Company NOT IN ( @ID_COMPANY )

DELETE FROM tPatient_Confinement_ItemsServices
WHERE  ID_Patient_Confinement NOT IN (SELECT ID
                                      FROM   tPatient_Confinement
                                      WHERE  ID_Company NOT IN ( @ID_COMPANY ))

DELETE FROM tPatient_Confinement
WHERE  ID_Company NOT IN ( @ID_COMPANY )

DELETE FROM tBillingInvoice_Patient
WHERE  ID_BillingInvoice NOT IN (SELECT ID
                                 FROM   tBillingInvoice
                                 WHERE  ID_Company NOT IN ( @ID_COMPANY ))

DELETE FROM tBillingInvoice_Detail
WHERE  ID_BillingInvoice NOT IN (SELECT ID
                                 FROM   tBillingInvoice
                                 WHERE  ID_Company NOT IN ( @ID_COMPANY ))

DELETE FROM tBillingInvoice
WHERE  ID_Company NOT IN ( @ID_COMPANY )

Update tCompany
SET    Name = LEFT(NEWID(), 4),
       Code = LEFT(NEWID(), 4),
       ContactNumber = '',
       Email = '',
       Address = '',
       ImageHeaderFilename = '',
       ImageLogoFilename = ''
WHERE  ID NOT IN ( 157 )

Update tEmployee
SET    Name = LEFT(NEWID(), 4),
       PRCLicenseNumber = '',
       TINNumber = '',
       ContactNumber = '',
       FirstName = LEFT(NEWID(), 4),
       MiddleName = LEFT(NEWID(), 4),
       LastName = LEFT(NEWID(), 4),
       PTR = '',
       S2 = '',
       FullAddress = '',
       Email = ''
WHERE  ID_Company NOT IN ( 157 )

Update tUser
SET    Password = LEFT(NEWID(), 4),
       UserName = LEFT(NEWID(), 4),
       Name = LEFT(NEWID(), 4)
WHERE  ID_Employee IN (SELECT ID
                       FROM   tEmployee
                       where  ID_Company NOT IN ( 157 ))

Update tSupplier
SET    Address = '',
       Name = '',
       TINNumber = '',
       ContactDetail = ''
WHERE  ID_Company NOT IN ( @ID_COMPANY )

TRUNCATE TABLE tAuditTrail_Detail;

DELETE FROM tAuditTrail

GO 
