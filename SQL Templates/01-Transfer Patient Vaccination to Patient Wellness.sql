DECLARE @ID_Patient_Vaccination INT
DECLARE import_cursor CURSOR FOR
  SELECT ID
  FROM   tPatient_Vaccination
  WHERE  ID NOT IN (SELECT ID_Patient_Vaccination
                    FROM   tPatient_Wellness
                    where  ID_Patient_Vaccination IS NOT NULL)
  oRDER  BY ID

OPEN import_cursor

FETCH NEXT FROM import_cursor INTO @ID_Patient_Vaccination

WHILE @@FETCH_STATUS = 0
  BEGIN
      DECLARE @ID_Patient_Wellness INT = 0

      INSERT INTO [dbo].[tPatient_Wellness]
                  ([Code],
                   [Name],
                   [IsActive],
                   [ID_Company],
                   [Comment],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [Date],
                   [ID_Client],
                   [ID_Patient],
                   [ID_FilingStatus],
                   [DateCanceled],
                   [ID_CanceledBy],
                   [ID_Patient_SOAP],
                   [AttendingPhysician],
                   [AttendingPhysician_ID_Employee],
                   ID_Patient_Vaccination)
      SELECT [Code],
             [Name],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [Date],
             [ID_Client],
             [ID_Patient],
             [ID_FilingStatus],
             [DateCanceled],
             [ID_CanceledBy],
             [ID_Patient_SOAP],
             [AttendingPhysician],
             [AttendingPhysician_ID_Employee],
             ID
      FROM   [dbo].[tPatient_Vaccination]
      WHERE  ID = @ID_Patient_Vaccination

      SET @ID_Patient_Wellness = @@IDENTITY

      INSERT INTO [dbo].[tPatient_Wellness_Schedule]
                  ([Code],
                   [Name],
                   [IsActive],
                   [ID_Company],
                   [Comment],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [ID_Patient_Wellness],
                   [Date],
                   [DateSent],
                   [IsSentSMS],
                   ID_Patient_Vaccination_Schedule)
      SELECT [Code],
             [Name],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             @ID_Patient_Wellness,
             [Date],
             [DateSent],
             [IsSentSMS],
             ID
      FROM   [dbo].[tPatient_Vaccination_Schedule]
      WHERE  ID_Patient_Vaccination = @ID_Patient_Vaccination

      INSERT INTO [dbo].[tPatient_Wellness_Detail]
                  ([IsActive],
                   [ID_Company],
                   [Comment],
                   [DateCreated],
                   [DateModified],
                   [ID_CreatedBy],
                   [ID_LastModifiedBy],
                   [ID_Patient_Wellness],
                   [UnitCost],
                   [UnitPrice],
                   [DateExpiration],
                   [ID_Item],
                   [ID_Patient_Vaccination_Detail])
      SELECT [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             @ID_Patient_Wellness,
             [UnitCost],
             [UnitPrice],
             [DateExpiration],
             [ID_Item],
             ID
      FROM   [dbo].[tPatient_Vaccination]
      WHERE  ID = @ID_Patient_Vaccination

      exec pModel_AfterSaved_Patient_Wellness
        @ID_Patient_Wellness,
        1

      FETCH NEXT FROM import_cursor INTO @ID_Patient_Vaccination
  END

CLOSE import_cursor;

DEALLOCATE import_cursor; 
