DECLARE @IDs_User TYPINTLIST
DECLARE @DateStart DATE = '2021-10-01'
DECLARE @DateEnd DATE = '2021-10-31'

INSERT @IDs_User
SELECT ID
FROM   vUser
WHERE  ID_Company = 149

SELECT _audit.Date,
       ISNULL(_model.Caption, _model.Name) Model,
       _user.Name_Employee Name,
       _audit.Description
FROM   vAuditTrail _audit
       INNER JOIN @IDs_User idsUser
               ON _audit.ID_User = idsUser.ID
       INNER JOIN _tModel _model
               ON _model.Oid = _audit.ID_Model
       INNER JOIN vUser _user
               ON _user.ID = idsUser.ID
WHERE  CONVERT(DATE, _audit.Date) BETWEEN @DateStart AND @DateEnd
ORDER  BY _audit.Date 
