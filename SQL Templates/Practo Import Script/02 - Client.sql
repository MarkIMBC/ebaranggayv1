/*
	@ID_Company_VetSaverSanIsidroAntipoloBranch 
	PetValleySorsogon-Patients
	PetValleySorsogon-ClinicalNotes
*/
DECLARE @forImportPatient TABLE
  (
     CustomCode_Patient VARCHAR(MAX),
     Name_Patient       VARCHAR(MAX),
     ContactNumber      VARCHAR(MAX),
     ContactNumber2     VARCHAR(MAX),
     EmailAddress       VARCHAR(MAX),
     ID_Gender          INT,
     Address_Client     VARCHAR(MAX),
     DateBirth          DateTime,
     Species            VARCHAR(MAX),
     Comment            VARCHAR(MAX)
  )
DECLARE @forImportClient TABLE
  (
     Name_Client    VARCHAR(MAX),
     ContactNumber  VARCHAR(MAX),
     ContactNumber2 VARCHAR(MAX),
     EmailAddress   VARCHAR(MAX),
     Address_Client VARCHAR(MAX)
  )

INSERT @forImportPatient
SELECT [Patient Number],
       [Patient Name],
       [Mobile Number],
       [Contact Number]
       + CASE
           WHEN LEN([Secondary Mobile]) > 0 THEN ' ' + [Secondary Mobile]
           ELSE ''
         END,
       [Email Address],
       CASE
         WHEN [Gender] = 'M' THEN 1
         ELSE
           CASE
             WHEN [Gender] = 'F' THEN 2
             ELSE NULL
           END
       END,
       [Address]
       + CASE
           WHEN LEN([Locality]) > 0 THEN ' '
           ELSE ''
         END
       + [Locality]
       + CASE
           WHEN LEN([City]) > 0 THEN ' '
           ELSE ''
         END
       + [City],
       CASE
         WHEN LEN([Date of Birth]) > 0 THEN TRY_CONVERT(DateTime, [Date of Birth])
         ELSE NULL
       END,
       [Groups],
       + CASE
           WHEN LEN([Remarks]) > 0 THEN 'Remarks' + CHAR(13) + CHAR(10) + ' ' + [Remarks]
           ELSE ''
         END
       +
       + CASE
           WHEN LEN([Medical History]) > 0 THEN CHAR(13) + CHAR(10) + 'Medical History'
                                                + CHAR(13) + CHAR(10) + ' ' + [Medical History]
           ELSE ''
         END
       + CASE
           WHEN LEN([Patient Notes]) > 0 THEN CHAR(13) + CHAR(10) + 'Patient Notes' + CHAR(13)
                                              + CHAR(10) + ' ' + [Patient Notes]
           ELSE ''
         END
FROM   (SELECT dbo.fGetRemovedBegEndSQuatationString([Patient Number])                                        [Patient Number],
               REPLACE(REPLACE(dbo.fGetRemovedBegEndSQuatationString([Patient Name]), '-', ' - '), '  ', ' ') [Patient Name],
               dbo.fGetRemovedBegEndSQuatationString([Mobile Number])                                         [Mobile Number],
               dbo.fGetRemovedBegEndSQuatationString([Contact Number])                                        [Contact Number],
               dbo.fGetRemovedBegEndSQuatationString([Email Address])                                         [Email Address],
               dbo.fGetRemovedBegEndSQuatationString([Secondary Mobile])                                      [Secondary Mobile],
               dbo.fGetRemovedBegEndSQuatationString([Gender])                                                [Gender],
               dbo.fGetRemovedBegEndSQuatationString([Address])                                               [Address],
               dbo.fGetRemovedBegEndSQuatationString([Locality])                                              [Locality],
               dbo.fGetRemovedBegEndSQuatationString([City])                                                  [City],
               dbo.fGetRemovedBegEndSQuatationString([National Id])                                           [National Id],
               dbo.fGetRemovedBegEndSQuatationString([Date of Birth])                                         [Date of Birth],
               dbo.fGetRemovedBegEndSQuatationString([Remarks])                                               [Remarks],
               dbo.fGetRemovedBegEndSQuatationString([Medical History])                                       [Medical History],
               dbo.fGetRemovedBegEndSQuatationString([Groups])                                                [Groups],
               dbo.fGetRemovedBegEndSQuatationString([Patient Notes])                                         [Patient Notes]
        FROM   [dbo].vetsaveers_clientpatient_practo) tbl

DECLARE @ID_Company_VetSaverSanIsidroAntipoloBranch INT = 154

INSERT @forImportClient
select FirstName,
       _forImport.ContactNumber,
       _forImport.ContactNumber2,
       _forImport.EmailAddress,
       _forImport.Address_Client
FROM   tPatient _patient
       INNER JOIN @forImportPatient _forImport
               ON _patient.CustomCode = _forImport.CustomCode_Patient
                  and _patient.Name = _forImport.Name_Patient
where  ID_Company = @ID_Company_VetSaverSanIsidroAntipoloBranch
 
ORDER  By FirstName

INSERT dbo.tClient
       (ID_Company,
        Name,
        Address,
        ContactNumber,
        ContactNumber2,
        Email,
        IsActive,
        DateCreated,
        DateModified,
        ID_CreatedBy,
        ID_LastModifiedBy,
        Comment)
SELECT @ID_Company_VetSaverSanIsidroAntipoloBranch,
       Name_Client,
       MAX(Address_Client),
       REPLACE(MAX(ContactNumber), '+63', '0'),
       MAX(ContactNumber2),
       MAX(EmailAddress),
       1,
       GETDATE(),
       GETDATE(),
       1,
       1,
       'Imported '
       + FORMAT(GETDATE(), 'MM/dd/yyyy hh:mm tt')
FROM   @forImportClient
WHERE  LEN(Name_Client) > 0
GROUP  BY Name_Client
order  BY Name_Client

Update tPatient
SET    ID_Client = client.ID
FROM   tPatient patient
       inner join (SELECT MAX(ID) ID,
                          Name,
                          ID_Company
                   FROM   tClient
                   WHERE  IsActive = 1
                   GROUP  BY Name,
                             ID_Company) client
               on client.Name = patient.FirstName
where  client.ID_Company = @ID_Company_VetSaverSanIsidroAntipoloBranch
       AND patient.ID_Company = @ID_Company_VetSaverSanIsidroAntipoloBranch
       and patient.IsActive = 1

/*DElete alll Client without  pets*/
DECLARE @IDs_Client typIntList

INSERT @IDs_Client
SELECT DISTINCT client . ID
FROM   tClient client
       Left join tPatient patient
              on patient.ID_Client = client.ID
where  client.ID_Company = @ID_Company_VetSaverSanIsidroAntipoloBranch
       and patient.ID IS NULL

DELETE FROM tClient
where  ID IN (SELECT ID
              FROM   @IDs_Client)
       and ID_Company = @ID_Company_VetSaverSanIsidroAntipoloBranch

SELECT *
FROM   tClient
where  ID_Company = @ID_Company_VetSaverSanIsidroAntipoloBranch
Order  By Name
/*


DECLARE @ID_Company_VetSaverSanIsidroAntipoloBranch INT = 154

DELETE
FROM   tClient
where  ID_Company = @ID_Company_VetSaverSanIsidroAntipoloBranch  

*/
