DECLARE @IDs_Company TYPINTLIST

INSERT @IDs_Company
SELECT DISTINCT ID_Company
FROM   tEmployee
WHERE  LastName LIKE 'Receptionportal'

DECLARE @IDs_Company_nonExisting TYPINTLIST

INSERT @IDs_Company_nonExisting
SELECT DISTINCT ID
FROM   tCompany
WHERE  ID NOT IN (SELECT ID
                  FROM   @IDs_Company)

INSERT INTO [dbo].[tEmployee]
            ([Code],
             [IsActive],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [DateCreated],
             [DateModified],
             [ImageFile],
             [ID_Department],
             [ID_EmployeePosition],
             [ID_Position],
             [LastName],
             [FirstName],
             [MiddleName],
             [ID_Gender],
             [ID_EmployeeStatus],
             [FullAddress],
             [Email],
             [ContactNumber],
             [Name],
             [ID_Company],
             [Comment],
             [IsSystemUsed],
             [PRCLicenseNumber],
             [PTR],
             [S2],
             [TINNumber],
             [DatePRCExpiration])
SELECT [Code],
       [IsActive],
       [ID_CreatedBy],
       [ID_LastModifiedBy],
       [DateCreated],
       [DateModified],
       [ImageFile],
       [ID_Department],
       [ID_EmployeePosition],
       [ID_Position],
       [LastName],
       [FirstName],
       [MiddleName],
       [ID_Gender],
       [ID_EmployeeStatus],
       [FullAddress],
       [Email],
       [ContactNumber],
       [Name],
       IDsCompany.ID,
       [Comment],
       [IsSystemUsed],
       [PRCLicenseNumber],
       [PTR],
       [S2],
       [TINNumber],
       [DatePRCExpiration]
FROM   tEmployee,
       @IDs_Company_nonExisting IDsCompany
WHERE  ID_Company = 1
       AND LastName = 'Receptionportal'

DECLARE @ID_Employee INT
DECLARE @ID_Company INT
DECLARE @FirstName VARCHAR(MAX) = ''
DECLARE cursor_product CURSOR FOR
  SELECT employee.ID,
         ID_Company,
         FirstName
  FROM   tEmployee employee
         LEFT JOIN tUser _user
                ON employee.ID = _user.ID_Employee
  WHERE  _user.ID IS NULL
         AND LastName = 'Receptionportal'

OPEN cursor_product;

FETCH NEXT FROM cursor_product INTO @ID_Employee, @ID_Company, @FirstName

WHILE @@FETCH_STATUS = 0
  BEGIN
      EXEC pCreateUser
        @ID_Company,
        @ID_Employee,
        @FirstName

      FETCH NEXT FROM cursor_product INTO @ID_Employee, @ID_Company, @FirstName
  END;

CLOSE cursor_product;

DEALLOCATE cursor_product; 
