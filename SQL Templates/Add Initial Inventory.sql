DECLARE @ID_Company INT = 135
Declare @adjustInventory typReceiveInventory

INSERT @adjustInventory
       ([Code],
        [ID_Item],
        [Quantity],
        [UnitPrice],
        [DateExpired],
        [BatchNo],
        [ID_FilingStatus],
        [ID_Company],
        [Comment],
        [IsAddInventory])
SELECT 'Initial Inventory Adjustment',
       ID,
       10,
       0.00,
       NULL,
       NULL,
       3,
       @ID_Company,
       'Imported on ' + FORMAT(GETDATE(),'yyyy-MM-dd hh:mm:ss tt'),
       1
FROM   titem
WHERE  ISNULL(CurrentInventoryCount, 0) = 0
       and ID_itemType = 2
       and ID_COmpany = @ID_Company

exec pReceiveInventory
  @adjustInventory,
  24284 
