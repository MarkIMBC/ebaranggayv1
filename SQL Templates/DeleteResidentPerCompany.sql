DECLARE @IDs_Resident typINtList

INSERT @IDs_Resident
SELECT ID
FROM   tResident
WHERE  ID_Company = 1

DELETE FROM tResident_Program
WHERE  ID_Resident IN (SELECT ID
                       FROM   @IDs_Resident)

DELETE FROM tResident_Family
WHERE  ID_Resident IN (SELECT ID
                       FROM   @IDs_Resident)

DELETE FROM tResident_HouseholdNumberLog
WHERE  ID_Resident IN (SELECT ID
                       FROM   @IDs_Resident)

DELETE FROM tResident_MedicalRecord
WHERE  ID_Resident IN (SELECT ID
                       FROM   @IDs_Resident)

DELETE FROM tResident
WHERE  ID IN (SELECT ID
              FROM   @IDs_Resident) 
			   