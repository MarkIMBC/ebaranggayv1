DECLARE @Name_Client   VARCHAR(MAX)= '',
        @Name_Patient  VARCHAR(MAX)= '',
        @ContactNumber VARCHAR(MAX)= ''
DEclare @ID_Company INT = 153
DEclare @ClientPatients TABLE
  (
     Name_Client   VARCHAR(MAX),
     Name_Patient  VARCHAR(MAX),
     ContactNumber VARCHAR(MAX)
  )
DECLARE import_cursor CURSOR FOR
  select [Owner's Name],
         [Pet's Name (Pet 1/Pet 2)],
         [Contact Number]
  FROM   Petlink_Makati_Client_Patient_Records_1423

OPEN import_cursor

FETCH NEXT FROM import_cursor INTO @Name_Client, @Name_Patient, @ContactNumber

WHILE @@FETCH_STATUS = 0
  BEGIN
      INSERT @ClientPatients
             (Name_Client,
              Name_Patient,
              ContactNumber)
      SELECT @Name_Client,
             Part,
             @ContactNumber
      FROM   dbo.fGetSplitString(@Name_Patient, '/')

      FETCH NEXT FROM import_cursor INTO @Name_Client, @Name_Patient, @ContactNumber
  END

CLOSE import_cursor;

DEALLOCATE import_cursor;