GO

DECLARE @DateStart DATE = '2022-10-01'
DECLARE @DateEND DATE = GETDATE()
DECLARE @Table as TAble
  (
     ID_Company   INT,
     DateModified Datetime
  )

/****************************************************************************************************************/
INSERT @Table
SELECT head.ID_Company,
       ISNULL(head.DateModified, head.DateCreated)
FROm   tResident head
       inner join tCompany c
               on head.ID_Company = c.ID
WHERE  CONVERT(Date, ISNULL(head.DateModified, head.DateCreated)) BETWEEN @DateStart AND @DateEND

INSERT @Table
SELECT _user.ID_Company,
       ISNULL(head.DateCreated, head.DateCreated)
FROm   tUserSession head
       INNER JOIN vUser _user
               on _user.ID = head.ID_User
       inner join tCompany c
               on _user.ID_Company = c.ID
WHERE  CONVERT(Date, ISNULL(head.DateCreated, head.DateCreated)) BETWEEN @DateStart AND @DateEND

/****************************************************************************************************************/
DECLARE @Dates Table
  (
     Date Date
  )

INSERT @Dates
SELECT Date
FROM   dbo.fGetDatesByDateCoverage(@DateStart, @DateEnd)

IF OBJECT_ID('tempdb..##DateCoverage') IS NOT NULL
  DROP TABLE ##DateCoverage

IF OBJECT_ID('tempdb..##UsageDates') IS NOT NULL
  DROP TABLE ##UsageDates

SELECT *
INTO   ##UsageDates
FROM   @Table

SELECT *
INTO   ##DateCoverage
FROM   @Dates

/*Company Usage Count Per Day*/
DECLARE @CompanyUsageCountPerDay TABLE
  (
     ID_Company      INT,
     Name_Company    Varchar(MAX),
     Date            Date,
     ClinicUsedCount Int
  )
DECLARE @dateStringsColumns VARCHAR(MAX) = ''
DECLARE @dateStrings VARCHAR(MAX) = ''
DECLARE @sql VARCHAR(MAX) = ''

SELECT @dateStringsColumns = @dateStringsColumns + 'ISNULL(['
                             + FORMAT(Date, 'yyyy-MM-dd') + '], '''') ['
                             + FORMAT(Date, 'yyyy-MM-dd (ddd)') + '] , '
FROM   @Dates

SELECT @dateStrings = @dateStrings + '[' + FORMAT(Date, 'yyyy-MM-dd')
                      + '], '
FROM   @Dates

SET @dateStringsColumns = LEFT(@dateStringsColumns, LEN(@dateStringsColumns) - 1)
SET @dateStrings = LEFT(@dateStrings, LEN(@dateStrings) - 1)

SELECT @dateStrings

SET @sql = '
SELECT  REPLACE(DB_NAME(), ''db_ebarangayv1_'', '''') DbName,  Name_Company, '
           + @dateStringsColumns
           + ', '''' Count
FROM   (SELECT Distinct c.ID                          ID_Company,
                        c.IsActive,
                        c.Name                        Name_Company,
                        COnvert(date, t.DateModified) DateUsage,
                        ''Yes''                         IsUsed
        FROM   ##UsageDates t
               Inner join vCompanyActive c
                       on c.ID = t.ID_Company
               RIGHT JOIN ##DateCoverage dateDaily
                       on COnvert(date, t.DateModified) = COnvert(date, dateDaily.Date)) AS SourceTable
       PIVOT ( MAX(IsUsed)
             FOR DateUsage IN ( '
           + @dateStrings + ') ) AS PivotTable
		   WHERE Name_Company IS NOT NULL
Order  by IsActive DESC,
          Name_Company '

exec (@sql) 
