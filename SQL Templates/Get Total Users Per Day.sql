
SELECT Date,
       Name_Company,
       Count(*) UserCount,
       Sum(AuditTrailCount)   AuditTrailCount
FROM   (select ID_Company,
               Name_Company,
               CONVERT(DATE, Date) Date,
               _user.ID            ID_User,
               _user.Name          Name_User,
               COUNT(*)            AuditTrailCount
        FROm   tAuditTrail _audit
               inner join vUser _user
                       on _audit.ID_User = _user.ID
        WHERE  CONVERT(DATE, Date) =  CONVERT(DATE, GETDATE())
        GROUP  BY ID_Company,
                  Name_Company,
                  _user.ID,
                  _user.Name,
                  CONVERT(DATE, Date)) tbl
GROUP  BY Date,
          Name_Company

GO

select ID_Company,
               Name_Company,
               CONVERT(DATE, Date) Date,
			    MAX( Date) MaxDate,
               _user.ID            ID_User,
               _user.Name          Name_User,
               COUNT(*)            AuditTrailCount
        FROm   tAuditTrail _audit
               inner join vUser _user
                       on _audit.ID_User = _user.ID
        WHERE  CONVERT(DATE, Date) =  CONVERT(DATE, GETDATE())
        GROUP  BY ID_Company,
                  Name_Company,
                  _user.ID,
                  _user.Name,
                  CONVERT(DATE, Date)

				 ORder by MaxDate DESC

				 
SELECT Date,
       Name_Company,
       Count(*) UserCount,
       Sum(AuditTrailCount)   AuditTrailCount
FROM   (select ID_Company,
               Name_Company,
               CONVERT(DATE, Date) Date,
               _user.ID            ID_User,
               _user.Name          Name_User,
               COUNT(*)            AuditTrailCount
        FROm   tAuditTrail _audit
               inner join vUser _user
                       on _audit.ID_User = _user.ID
        WHERE  CONVERT(DATE, Date) =  CONVERT(DATE, GETDATE())
        GROUP  BY ID_Company,
                  Name_Company,
                  _user.ID,
                  _user.Name,
                  CONVERT(DATE, Date)) tbl
GROUP  BY Date,
          Name_Company

