DECLARE @Source_ID_Company INT = 100
DECLARE @Destination_ID_Company INT = 102

INSERT INTO [dbo].[tItem]
            ([Code],
             [Name],
             [IsActive],
             [ID_Company],
             [Comment],
             [DateCreated],
             [DateModified],
             [ID_CreatedBy],
             [ID_LastModifiedBy],
             [ID_ItemType],
             [ID_ItemCategory],
             [MinInventoryCount],
             [MaxInventoryCount],
             [UnitCost],
             [UnitPrice],
             [CurrentInventoryCount],
             [Old_item_id],
             [Old_procedure_id],
             [OtherInfo_DateExpiration],
             [ID_InventoryStatus])
SELECT [Code],
       [Name],
       [IsActive],
       @Destination_ID_Company,
       [Comment],
       GETDATE(),
       GETDATE(),
       [ID_CreatedBy],
       [ID_LastModifiedBy],
       [ID_ItemType],
       [ID_ItemCategory],
       [MinInventoryCount],
       [MaxInventoryCount],
       [UnitCost],
       [UnitPrice],
       [CurrentInventoryCount],
       [Old_item_id],
       [Old_procedure_id],
       [OtherInfo_DateExpiration],
       [ID_InventoryStatus]
FROM   [dbo].[tItem]
WHERE  ID_Company = @Source_ID_Company
       AND ID_ItemType = 1

GO 
