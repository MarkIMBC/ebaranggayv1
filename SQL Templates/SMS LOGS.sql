Declare @table Table
  (
     ID_Patient_SOAP      INT,
     ID_Patient_SOAP_Plan INT,
     ID_SOAP_SMSStatus    INT
  )

INSERT @table
select soapPlan.ID_Patient_SOAP,
       soapPlan.ID,
       MAX(soapSMS.ID) ID_SOAP_SMSStatus
FROM   tPatient_SOAP hed
       inner join tPatient_SOAP_Plan soapPlan
               on hed.ID = soapPlan.ID_Patient_SOAP
       Left JOIN tPatient_SOAP_SMSStatus soapSMS
              ON soapSMS.ID_Patient_SOAP = soapPlan.ID
where  hed.ID_Company = 44
       and CONVERT(Date, soapPlan.DateReturn) BETWEEN '2021-10-01' AND '2021-10-15'
GROUP  BY soapPlan.ID_Patient_SOAP,
          soapPlan.ID,
          soapPlan.IsSentSMS

SELECT DISTINCT client.Name,
                client.ContactNumber, c.Name
FROM   @table hed
       inner JOIN vPatient_SOAP soap
               on soap.ID = hed.ID_Patient_SOAP
       inner join vPatient_SOAP_Plan soapPlan
               on soapPlan.ID = hed.ID_Patient_SOAP_Plan
       LEFT JOIN vPatient_SOAP_SMSStatus soapSMS
               ON soapSMS.ID = hed.ID_SOAP_SMSStatus
       INNER JOIN tCompany c
               on c.ID = soap.ID_Company
       inner join tClient client
               on client.ID = soap.ID_Client
       LEFT join tITextMoAPISMSStatus smsStatus
              on soapSMS.iTextMo_Status = smsStatus.Code
ORDER  BY client.name 
